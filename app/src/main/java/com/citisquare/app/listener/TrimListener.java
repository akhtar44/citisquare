package com.citisquare.app.listener;

/**
 * Created by hlink16 on 12/1/16.
 */
public interface TrimListener {
    public void onTrimmingDone(String url);
}
