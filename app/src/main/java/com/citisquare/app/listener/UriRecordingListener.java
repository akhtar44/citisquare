package com.citisquare.app.listener;

import android.net.Uri;

/**
 * Created by hlink56 on 24/12/16.
 */

public interface UriRecordingListener {
    public void setFileImageName(Uri name, String from);
    public void setFileVideo(String file);

}


