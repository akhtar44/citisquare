package com.citisquare.app.camera.core;

import android.hardware.Camera;

import java.util.List;

/**
 * Created by hlink21 on 24/6/16.
 */

public class CameraSettingInitializer {

    CameraSettings cameraSettings;
    Camera mCamera;


    public CameraSettingInitializer() {
        cameraSettings = new CameraSettings();
    }

    public CameraSettings provideSetting() {
        setBackCameraParameters();
        setFrontCameraParameters();
        return cameraSettings;
    }

    private void setBackCameraParameters() {

        mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);

        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(Camera.CameraInfo.CAMERA_FACING_BACK, info);

        cameraSettings.setBackOrientation(info.orientation);
        cameraSettings.setSupportBackFocus(hasFocus(mCamera));
        cameraSettings.setSupportBackFlash(hasFlash(mCamera));
        Camera.Parameters parameters = mCamera.getParameters();
        cameraSettings.setSupportedBackPreviewSize(parameters.getSupportedPreviewSizes());
        cameraSettings.setSupportedBackPictureSize(parameters.getSupportedPictureSizes());
        cameraSettings.setSupportedBackVideoSize(parameters.getSupportedVideoSizes());
        mCamera.release();

    }

    private void setFrontCameraParameters() {

        mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(Camera.CameraInfo.CAMERA_FACING_FRONT, info);

        cameraSettings.setFrontOrientation(info.orientation);
        cameraSettings.setSupportFrontFocus(hasFocus(mCamera));
        cameraSettings.setSupportFrontFlash(hasFlash(mCamera));
        Camera.Parameters parameters = mCamera.getParameters();
        cameraSettings.setSupportedFrontPreviewSize(parameters.getSupportedPreviewSizes());
        cameraSettings.setSupportedFrontPictureSize(parameters.getSupportedPictureSizes());
        cameraSettings.setSupportedFrontVideoSize(parameters.getSupportedVideoSizes());
        mCamera.release();

    }

    private boolean hasFocus(Camera mCamera) {
        if (mCamera == null) {
            return false;
        }

        Camera.Parameters parameters = mCamera.getParameters();

        if (parameters.getFocusMode() == null) {
            return false;
        }

        List<String> supportedFlashModes = parameters.getSupportedFocusModes();
        if (supportedFlashModes == null || supportedFlashModes.isEmpty() || supportedFlashModes.size() == 1) {
            return false;
        }

        return true;
    }

    private boolean hasFlash(Camera mCamera) {
        if (mCamera == null) {
            return false;
        }

        Camera.Parameters parameters = mCamera.getParameters();

        if (parameters.getFlashMode() == null) {
            return false;
        }

        List<String> supportedFlashModes = parameters.getSupportedFlashModes();
        if (supportedFlashModes == null || supportedFlashModes.isEmpty() || supportedFlashModes.size() == 1 && supportedFlashModes.get(0).equals(Camera.Parameters.FLASH_MODE_OFF)) {
            return false;
        }

        return true;
    }
}
