package com.citisquare.app.camera.utils;

import android.app.Activity;
import android.content.Context;
import android.hardware.Camera;
import android.os.Handler;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.citisquare.app.camera.core.CameraSettings;

import java.io.IOException;
import java.util.List;

/**
 * Created by hlink21 on 17/6/16.
 */
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {

    private final List<Camera.Size> videoSizes;
    private List<Camera.Size> mSupportedPreviewSizes;

    private Camera mCamera;
    private SurfaceHolder mHolder;

    private Camera.Size mPreviewSize;
    private int currentCameraId;
    private int currentOrientation;

    private CameraSettings cameraSettings;
    boolean isFrontFacing;

    public CameraPreview(Context context, Camera camera, CameraSettings cameraSettings) {
        super(context);
        this.mCamera = camera;
        this.cameraSettings = cameraSettings;

        mSupportedPreviewSizes = cameraSettings.getSupportedBackPreviewSize();
        videoSizes = cameraSettings.getSupportedBackVideoSize();

        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        mHolder = getHolder();
        mHolder.addCallback(this);
        // deprecated setting, but required on Android versions prior to 3.0
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

        try {

            if (mCamera != null) {
                mCamera.setPreviewDisplay(holder);
                setCameraDisplayOrientation((Activity) getContext(), Camera.CameraInfo.CAMERA_FACING_BACK, mCamera);
                mCamera.startPreview();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

        if (mHolder.getSurface() == null) {
            // preview surface does not exist
            return;
        }

        // stop preview before making changes
        try {
            mCamera.stopPreview();
        } catch (Exception e) {
            // ignore: tried to stop a non-existent preview
        }

        // set preview size and make any resize, rotate or
        // reformatting changes here

        // start preview with new account_settings
        try {
            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();

        } catch (Exception ignored) {

        }

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        releaseCameraAndPreview();
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        final int width = resolveSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        final int height = resolveSize(getSuggestedMinimumHeight(), heightMeasureSpec);
        //  setMeasuredDimension(width, height);

        if (mSupportedPreviewSizes != null) {
            mPreviewSize = getOptimalPreviewSize(mSupportedPreviewSizes, width, height);
        }

        float ratio;
        if (mPreviewSize.height >= mPreviewSize.width)
            ratio = (float) mPreviewSize.height / (float) mPreviewSize.width;
        else
            ratio = (float) mPreviewSize.width / (float) mPreviewSize.height;

        // One of these methods should be used, second method squishes preview slightly
        setMeasuredDimension(width, (int) (width * ratio));
        // setMeasuredDimension((int) (width * ratio), height);

    }

    private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio = (double) h / w;

        if (sizes == null)
            return null;

        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        for (Camera.Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
                continue;

            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }

        return optimalSize;
    }

    private Camera.Size getOptimalPictureSize(List<Camera.Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.2;

        double targetRatio = (double) w / h;
        if (sizes == null)
            return null;

        Camera.Size optimalSize = null;
        double minDiff = 0;

        int targetHeight = h;

        // Try to find an size match aspect ratio and size
        for (Camera.Size size : sizes) {
            Log.d("Camera", "Checking size " + size.width + "w " + size.height
                    + "h");
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
                continue;

            /*if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }*/

            if (size.height > minDiff) {
                optimalSize = size;
                minDiff = size.height;
                Log.d("Camera", "Taking size " + optimalSize.width + "w " + optimalSize.height
                        + "h");
            }

        }

        // Cannot find the one match the aspect ratio, ignore the
        // requirement
        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                    Log.d("Camera", "" + optimalSize.width + "w " + optimalSize.height
                            + "h");
                }
            }
        }
        return optimalSize;
    }

    public Camera getCamera() {
        return mCamera;
    }


    public void switchCamera() {

        mCamera.stopPreview();

        //NB: if you don't release the current camera before switching, you app will crash
        mCamera.release();

        //swap the id of the camera to be used

        if (currentCameraId == Camera.CameraInfo.CAMERA_FACING_BACK) {
            currentCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
            isFrontFacing = true;
        } else {
            currentCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
            isFrontFacing = false;
        }

        mCamera = Camera.open(currentCameraId);

        mSupportedPreviewSizes = mCamera.getParameters().getSupportedPreviewSizes();

        requestLayout();

        // wait for onMeasure call
        Runnable runnable = new Runnable() {
            public void run() {
                setCameraDisplayOrientation((Activity) getContext(), currentCameraId, mCamera);
                try {

                    mCamera.setPreviewDisplay(mHolder);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                mCamera.startPreview();
            }
        };

        new Handler().postDelayed(runnable, 500);

    }

    public void setCameraDisplayOrientation(Activity activity, int cameraId, Camera camera) {

        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay()
                .getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }

        Camera.Parameters parameters = camera.getParameters();
        parameters.setRotation(info.orientation);
        parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);

        // get best available picture size
        List<Camera.Size> supportedPictureSizes = parameters.getSupportedPictureSizes();
        Camera.Size optimalPreviewSize = getOptimalPictureSize(supportedPictureSizes, mPreviewSize.width, mPreviewSize.height);
        parameters.setPictureSize(optimalPreviewSize.width, optimalPreviewSize.height);

        if (!isFrontFacing && cameraSettings.isSupportBackFocus()) {
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        }

        camera.setParameters(parameters);

        Log.d(" ORIGNAL ORIENTATION ", "" + info.orientation);

        currentOrientation = info.orientation;
        Log.d(" Orientation is ", "" + +result);
        camera.setDisplayOrientation(result);
    }

    public Camera.Size getVideoSize() {

        Camera.Size optimalVideoSize;
        if (videoSizes != null)
            optimalVideoSize = getOptimalPreviewSize(videoSizes, 1280, 720);
        else
            optimalVideoSize = getOptimalPreviewSize(mSupportedPreviewSizes, 1280, 720);

        Log.d(" VIDEO SIZE ", "" + optimalVideoSize.width + " : " + optimalVideoSize.height);
        return optimalVideoSize;
    }

    public int getCurrentOrientation() {
        return currentOrientation;
    }

    public boolean isFrontFacing() {
        return isFrontFacing;
    }

    public boolean hasFlash() {
        if (mCamera == null) {
            return false;
        }

        Camera.Parameters parameters = mCamera.getParameters();

        if (parameters.getFlashMode() == null) {
            return false;
        }

        List<String> supportedFlashModes = parameters.getSupportedFlashModes();
        if (supportedFlashModes == null || supportedFlashModes.isEmpty() || supportedFlashModes.size() == 1 && supportedFlashModes.get(0).equals(Camera.Parameters.FLASH_MODE_OFF)) {
            return false;
        }

        return true;
    }

    public boolean hasFocus() {
        if (mCamera == null) {
            return false;
        }

        Camera.Parameters parameters = mCamera.getParameters();

        if (parameters.getFocusMode() == null) {
            return false;
        }

        List<String> supportedFlashModes = parameters.getSupportedFocusModes();
        if (supportedFlashModes == null || supportedFlashModes.isEmpty() || supportedFlashModes.size() == 1) {
            return false;
        }

        return true;
    }

    public void releaseCameraAndPreview() {
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
    }

    public interface cameraPreviewListener {
        void onPreviewAvailable(CameraPreview cameraPreview);
    }
}
