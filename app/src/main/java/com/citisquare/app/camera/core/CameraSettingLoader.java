package com.citisquare.app.camera.core;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.citisquare.app.utils.DataToPref;

/**
 * Created by hlink21 on 5/7/16.
 */

public class CameraSettingLoader {

    private static final String CAMERA_SETTINGS = "BstarCameraSetting";
    Context context;
    private CameraSettingInitializer cameraSettingInitializer;

    public CameraSettingLoader(CameraSettingInitializer cameraSettingInitializer, Context context) {
        this.cameraSettingInitializer = cameraSettingInitializer;
        this.context = context;
    }

    public CameraSettings getCameraSetting() {
        Gson gson = new Gson();
        String string = DataToPref.getData(context, CAMERA_SETTINGS);

        if (string != null && !string.isEmpty()) {
            try {
                // Log.e("PREF CAMERA Setting", " " + string);
                CameraSettings cameraSettings = gson.fromJson(string, CameraSettings.class);
                return cameraSettings;
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
                return null;
            }

        } else {

            CameraSettings cameraSettings = cameraSettingInitializer.provideSetting();

            String settingString = gson.toJson(cameraSettings);

            // Log.e("NEW CAMERA Setting ", " " + settingString);

            if (settingString != null && !settingString.isEmpty())
                DataToPref.setData(context, CAMERA_SETTINGS, settingString);

            return cameraSettings;
        }

    }
}
