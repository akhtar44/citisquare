package com.citisquare.app.camera.core;

import android.hardware.Camera;

import java.io.Serializable;
import java.util.List;

/**
 * Created by hlink21 on 24/6/16.
 */
public class CameraSettings implements Serializable {

    private boolean isSupportFrontFlash;
    private boolean isSupportBackFlash;
    private boolean isSupportFrontFocus;
    private boolean isSupportBackFocus;

    private int frontOrientation;
    private int backOrientation;

    private List<Camera.Size> supportedFrontPreviewSize;
    private List<Camera.Size> supportedBackPreviewSize;
    private List<Camera.Size> supportedFrontPictureSize;
    private List<Camera.Size> supportedBackPictureSize;
    private List<Camera.Size> supportedFrontVideoSize;
    private List<Camera.Size> supportedBackVideoSize;


    public boolean isSupportFrontFlash() {
        return isSupportFrontFlash;
    }

    public void setSupportFrontFlash(boolean supportFrontFlash) {
        isSupportFrontFlash = supportFrontFlash;
    }

    public boolean isSupportBackFlash() {
        return isSupportBackFlash;
    }

    public void setSupportBackFlash(boolean supportBackFlash) {
        isSupportBackFlash = supportBackFlash;
    }

    public boolean isSupportFrontFocus() {
        return isSupportFrontFocus;
    }

    public void setSupportFrontFocus(boolean supportFrontFocus) {
        isSupportFrontFocus = supportFrontFocus;
    }

    public boolean isSupportBackFocus() {
        return isSupportBackFocus;
    }

    public void setSupportBackFocus(boolean supportBackFocus) {
        isSupportBackFocus = supportBackFocus;
    }

    public int getFrontOrientation() {
        return frontOrientation;
    }

    public void setFrontOrientation(int frontOrientation) {
        this.frontOrientation = frontOrientation;
    }

    public int getBackOrientation() {
        return backOrientation;
    }

    public void setBackOrientation(int backOrientation) {
        this.backOrientation = backOrientation;
    }

    public List<Camera.Size> getSupportedFrontPreviewSize() {
        return supportedFrontPreviewSize;
    }

    public void setSupportedFrontPreviewSize(List<Camera.Size> supportedFrontPreviewSize) {
        this.supportedFrontPreviewSize = supportedFrontPreviewSize;
    }

    public List<Camera.Size> getSupportedBackPreviewSize() {
        return supportedBackPreviewSize;
    }

    public void setSupportedBackPreviewSize(List<Camera.Size> supportedBackPreviewSize) {
        this.supportedBackPreviewSize = supportedBackPreviewSize;
    }

    public List<Camera.Size> getSupportedFrontPictureSize() {
        return supportedFrontPictureSize;
    }

    public void setSupportedFrontPictureSize(List<Camera.Size> supportedFrontPictureSize) {
        this.supportedFrontPictureSize = supportedFrontPictureSize;
    }

    public List<Camera.Size> getSupportedBackPictureSize() {
        return supportedBackPictureSize;
    }

    public void setSupportedBackPictureSize(List<Camera.Size> supportedBackPictureSize) {
        this.supportedBackPictureSize = supportedBackPictureSize;
    }

    public List<Camera.Size> getSupportedFrontVideoSize() {
        return supportedFrontVideoSize;
    }

    public void setSupportedFrontVideoSize(List<Camera.Size> supportedFrontVideoSize) {
        this.supportedFrontVideoSize = supportedFrontVideoSize;
    }

    public List<Camera.Size> getSupportedBackVideoSize() {
        return supportedBackVideoSize;
    }

    public void setSupportedBackVideoSize(List<Camera.Size> supportedBackVideoSize) {
        this.supportedBackVideoSize = supportedBackVideoSize;
    }

    @Override
    public String toString() {
        return "SupportFrontFlash :" + isSupportFrontFlash + "\n"
                + "SupportBackFlash :" + isSupportBackFlash + "\n"
                + "SupportFrontFocus :" + isSupportFrontFocus + "\n"
                + "SupportBackFocus :" + isSupportBackFocus + "\n"
                + "Front Orientation " + frontOrientation + "\n"
                + "Back Orientation " + backOrientation + "\n"
                + "supportedBackPreviewSize :" + supportedBackPreviewSize.size() + "\n"
                + "supportedFrontPreviewSize :" + supportedFrontPreviewSize.size() + "\n"
                + "SupportedBackPictureSize :" + supportedBackPictureSize.size() + "\n"
                + "SupportedFrontPictureSize :" + supportedFrontPictureSize.size()
                ;
    }
}
