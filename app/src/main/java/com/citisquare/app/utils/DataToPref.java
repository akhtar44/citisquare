package com.citisquare.app.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by hlink16 on 11/6/15.
 */

// isNeededManuallySupport, isAvailable, user
public class DataToPref {
    private static final String pref = "CitiSquarePref";


    public static void setBoolean(Context context, String key, boolean isTrue) {
        try {
            SharedPreferences sharedPreferences = context.getSharedPreferences(pref, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(key, isTrue);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setData(Context context, String value) {
        Debugger.e("DATA ARE SET:::" + value);
        setData(context, "user", value);
    }

    public static String getData(Context context) {
        return DataToPref.getData(context, "user");
    }

    public static void setData(Context context, String key, String value) {
        try {
            SharedPreferences sharedPreferences = context.getSharedPreferences(pref, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(key, value);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void remove(Context context, String key) {
        try {
            SharedPreferences sharedPreferences = context.getSharedPreferences(pref, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.remove(key);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getData(Context context, String key) {

        String userObjectString = null;
        try {
            SharedPreferences sharedPreferences = context.getSharedPreferences(pref, Context.MODE_PRIVATE);
            userObjectString = sharedPreferences.getString(key, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userObjectString;
    }

    public static void setUserInfo(Context context, String datum) {
        setData(context, "user", datum);
        isAvailable(context);
    }

    public static void setNotificationStatus(Context context, String status) {
        setData(context, "notificationStatus", status);
    }

    public static String getNotificationStatus(Context context) {
        String data = getData(context, "notificationStatus");
        return data;
    }

    /*public static User getUserInfo(Context context) {
        User user = new User();
        if (getData(context, "isAvailable").equals("true")) {
            String userObjectString = getData(context, "user");
            try {
                user = new Gson().fromJson(userObjectString, User.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return user;

    }*/

    public static void isAvailable(Context context) {
        setData(context, "isAvailable", "true");
    }


    public static void clear(Context context) {
        try {
            String deviceId = getData(context, "deviceID");
            SharedPreferences sharedPreferences = context.getSharedPreferences(pref, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.clear();
            editor.commit();
            setData(context, "deviceID", deviceId);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
