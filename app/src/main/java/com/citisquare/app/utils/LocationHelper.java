package com.citisquare.app.utils;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/**
 * Created by hlink56 on 31/8/16.
 */
public class LocationHelper implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    Context context;
    ProvideLocation provideLocation;
    boolean isLocationOn, isProvider;
    private GoogleApiClient mGoogleApiClient;
    private int GooglePlayServicestatus;
    private LocationRequest locationRequest;
    private boolean islocationupdated;
    private Fragment fragment;

    public LocationHelper(Fragment fragment, ProvideLocation provideLocation) {
        this.fragment = fragment;
        this.context = fragment.getContext();
        this.provideLocation = provideLocation;
        islocationupdated = false;

        if (isLocationEnabled()) {
            if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(context) == ConnectionResult.SUCCESS) {

                mGoogleApiClient = new GoogleApiClient.Builder(context)
                        .addApi(LocationServices.API)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this)
                        .build();

                if (!mGoogleApiClient.isConnected() || !mGoogleApiClient.isConnecting()) {
                    mGoogleApiClient.connect();
                }
            } else {
                Log.e("", "unable to connect to google play services.");
            }
        } else {
            checkLocationErrorWithDialog();
            provideLocation.onError();
        }
    }

    public void checkLocationErrorWithDialog() {
        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            isLocationOn = gps_enabled;
        } catch (Exception ex) {
        }
        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            isProvider = network_enabled;
        } catch (Exception ex) {
        }
        if (!gps_enabled && !network_enabled) {
            // notify user

            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setCancelable(false);
            dialog.setMessage("To re-enable, please go to account_settings and turn on location service for Quickinfo app.");
            dialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {

    /*                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        BaseActivity.this.startActivity(myIntent);*/
                    paramDialogInterface.dismiss();
                    fragment.startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 1);
                    paramDialogInterface.dismiss();
                    //get gps
                }
            });
            dialog.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    paramDialogInterface.dismiss();
                    checkLocationErrorWithDialog();
                }
            });
            dialog.show();

        }
    }

    public void isNewLocationAvailable(Location location) {
        if (location != null) {
            // DebugLog.e("Latitude and Longitude are Set" + String.valueOf(location.getLatitude()) + "," + String.valueOf(location.getLongitude()));
            stopLocationUpdates();
            provideLocation.currentLocation(location);
        } else {

        }
    }


    public void disConnectFusedApi() {
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        Debugger.e("", "onConnected");

        locationRequest = LocationRequest.create();
        locationRequest.setInterval(1000); // milliseconds
        locationRequest.setFastestInterval(1000);
        // the fastest rate in milliseconds at which your app can handle location updates
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setMaxWaitTime(10);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, locationRequest, this);


      /*  final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!islocationupdated) {
                    provideLocation.onError();
                    islocationupdated = true;
                    stopLocationUpdates();
                }
            }
        }, 3000);*/


    }


    @Override
    public void onConnectionSuspended(int i) {
        provideLocation.onError();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        provideLocation.onError();
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            Log.e("", "position: " + location.getLatitude() + ", " + location.getLongitude() + " accuracy: " + location.getAccuracy());

            // we have our desired accuracy of 500 meters so lets quit this service,
            // onDestroy will be called and stop our location uodates
            /*if (location.getAccuracy() < 10.0f) {
                stopLocationUpdates();
                sendLocationDataToWebsite(location);
            }*/

            stopLocationUpdates();
            if (!islocationupdated) {
                isNewLocationAvailable(location);
                islocationupdated = true;
            }
        }


    }

    boolean isLatestPlayserviceinstalled() {
        GooglePlayServicestatus = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context.getApplicationContext());
        if (GooglePlayServicestatus == ConnectionResult.SUCCESS) {
            return true;
        } else {
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(GooglePlayServicestatus, (Activity) context, requestCode);
            dialog.show();
            return false;
        }
    }

    public boolean isLocationEnabled() {
        int m_locationMode = 0;
        String m_locationProviders;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                m_locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException m_e) {
                m_e.printStackTrace();
            }
            return m_locationMode != Settings.Secure.LOCATION_MODE_OFF;
        } else {
            m_locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(m_locationProviders);
        }
    }

    public void startLocationUpdates() {
        LocationRequest mLocationRequest = LocationRequest.create();
        if (mGoogleApiClient.isConnected())
            /*if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }*/
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        //LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    public void stopLocationUpdates() {
        try {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface ProvideLocation {
        void currentLocation(Location location);

        void onError();
    }
}