package com.citisquare.app.utils;

import android.app.Activity;
import android.support.v4.view.ViewPager;

/**
 * Created by sanket on 7/12/16.
 */

public class AutoRotate {
    private Runnable runnable = null;
    private ViewPager pager;
    private final Activity activity;
    private boolean isRunning;
    private Thread thread;

    public AutoRotate(Activity activity, ViewPager pager) {
        this.activity = activity;
        this.pager = pager;
    }

    public void rotate() {

        runnable = new Runnable() {
            @Override
            public void run() {
                while (isRunning) {
                    try {
                        Thread.sleep(2000);
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                change();
                            }
                        });

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        thread = new Thread(runnable);
        thread.start();

    }

    public void onResume() {
        if (!isRunning)
            rotate();
        isRunning=true;

    }

    public void onPause() {
        isRunning=false;
    }

    private void change() {
        if (pager.getCurrentItem() == pager.getAdapter().getCount()-1)
            pager.setCurrentItem(0);
        else
            pager.setCurrentItem(pager.getCurrentItem() + 1);
    }
}
