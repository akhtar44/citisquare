package com.citisquare.app.utils;

import android.Manifest;

/**
 * Created by hlink56 on 12/2/16.
 */
public class Constant {
    public static final boolean ENABLE_DEBUG = true;
    public static final String TAG = "CitiSquare";
    public static String SNAP_DIRECTORY = "CitiSquare";

    public static String DEFAULT_THEMECOLOR = "#67c3ea";
    public static String THEMECOLOR = "#67c3ea";
    public final static String UN_CHECK_COLOR = "#9d9d9d";

    public static String SOCKET_TIMEOUT = "java.net.SocketTimeoutException";
    public static String INVALID_HOSTNAME = "java.net.UnknownHostException";
    public static String CONNECTION_GONE = "java.net.ConnectException";



    public static String SD_TIME_FORMAT = "dd-MM-yyyy , h:mm a";

    public static String[] PERMISSIONS_CAMERA = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    public static String[] PERMISSIONS_LOCATION = {Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};

    public static String[] PERMISSIONS_PHONE_STATE = {Manifest.permission.READ_PHONE_STATE};
}
