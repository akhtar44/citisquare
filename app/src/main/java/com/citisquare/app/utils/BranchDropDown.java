package com.citisquare.app.utils;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.BranchDropDownAdapter;
import com.citisquare.app.interfaces.ItemEventListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 19/8/16.
 */
public class BranchDropDown {
    PopupWindow popupWindow;
    View view;
    boolean popUpVisible = false;
    int position;
    ItemEventListener<Integer> itemEventListener;
    @BindView(R.id.cityList)
    ListView categoryListview;
    TextView textView;
    Context context;
    ArrayList<String> categoryArrayList;
    BranchDropDownAdapter cityDropDownAdapter;
    int height;
    int[] location = new int[2];
    boolean flag;


    public BranchDropDown(Context context, int height, int[] location, ArrayList<String> categoryArrayList
            , boolean flag, ItemEventListener<Integer> integerItemEventListener) {
        view = View.inflate(context, R.layout.city_dropdown, null);
        ButterKnife.bind(this, view);
        itemEventListener = integerItemEventListener;
        this.context = context;
        this.flag = flag;
        categoryListview.setTextFilterEnabled(true);
        this.categoryArrayList = categoryArrayList;
        this.location = location;
        this.height = height;
        popupWindow = new PopupWindow(view, LinearLayout.LayoutParams.MATCH_PARENT, (int) context.getResources().getDimension(R.dimen.dp_180));
    }


    public void setAdapter() {
        categoryListview.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        cityDropDownAdapter = new BranchDropDownAdapter(context, R.layout.raw_branch_drop_down, categoryArrayList, flag);
        categoryListview.setAdapter(cityDropDownAdapter);

        popupWindow.setContentView(view);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0));
        popupWindow.setFocusable(true);
        if (popupWindow.isFocusable()) {
            popUpVisible = false;
        }
        categoryListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                itemEventListener.onItemEventFired(position, 0, "");
                if (flag)
                    ((MainActivity) context).setBranchPosition(position);
                dismiss();
            }
        });
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int pos) {
        position = pos;
    }

    public void show() {
        popUpVisible = true;
        setAdapter();
        //popupWindow.showAsDropDown(view);
        popupWindow.showAtLocation(view, Gravity.TOP, location[0],
                location[1] + height);
    }


    public void dismiss() {
        popUpVisible = false;
        popupWindow.dismiss();
    }

    public boolean isVisible() {
        return popUpVisible;
    }
}
