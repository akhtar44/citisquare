package com.citisquare.app.utils;

/**
 * Created by h-link9 on 02/07/2016.
*/

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;


public class BackstackUtils {
    /**
     * getlast backstack tag from stack
     * @param context
     * @return
     */
    public static String getlastBackStackTag(AppCompatActivity context){
        if(context.getSupportFragmentManager().getBackStackEntryCount()!=0){
            FragmentManager.BackStackEntry backEntry=context.getSupportFragmentManager().getBackStackEntryAt(context.getSupportFragmentManager().getBackStackEntryCount() - 1);
            return backEntry.getName();}else{
            return "";
        }
    }

    /**
     * clear all entry from backstack
     * @param context
     */
    public static void clearBackStack(AppCompatActivity context) {
        context.getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    /**
     * check if tag is from collection of tag
     * @param ctag tag who need to be compared
     * @param tags list of tags
     * @return true or false
     */
    public static boolean compareTags(String ctag, String...tags) {
        for (String tag : tags) {
            if (tag.equalsIgnoreCase(ctag)){
                return true;
            }
        }
        return false;

    }

    /**
     * get last backstack n entru
     * @param context
     * @param i i th entry from last whould be retrive
     * @return tag of that entry
     */
    public static String getlastNBackStackTag(AppCompatActivity context, int i) {
        try {
            if (context.getSupportFragmentManager().getBackStackEntryCount() != 0) {
                FragmentManager.BackStackEntry backEntry = context.getSupportFragmentManager().getBackStackEntryAt(context.getSupportFragmentManager().getBackStackEntryCount() - (1 + i));
                return backEntry.getName();
            } else {
                return "";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * print backstract with tag
     * @param context
     */
        public static void printBackStack(AppCompatActivity context){
            FragmentManager fm =context.getSupportFragmentManager();
            for(int entry = 0; entry < fm.getBackStackEntryCount(); entry++){
            }
        }
    }

