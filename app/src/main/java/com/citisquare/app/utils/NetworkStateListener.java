package com.citisquare.app.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.citisquare.app.interfaces.ConnectionListener;


/**
 * Created by hlink16 on 16/6/15.
 */
public class NetworkStateListener extends BroadcastReceiver {

    private ConnectionListener callBack;

    public NetworkStateListener(ConnectionListener callBack) {
        this.callBack = callBack;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        int flag = NetworkStatus.getConnectivityStatus(context);
        Debugger.e(this.getClass().getName(), "::: NETWORK CONNECTION IS :::: " + flag);
        if (flag == 0)
            callBack.onConnectionOFF();
        else
            callBack.onConnectionON();
    }
}
