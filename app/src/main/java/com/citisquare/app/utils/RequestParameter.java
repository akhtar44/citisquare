package com.citisquare.app.utils;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by hlink56 on 1/8/16.
 */
public class RequestParameter {
    public HashMap<String, String> getHashMapForVisitorSignup(HashMap<String, String> hashMapSignupVisitor) {
        hashMapSignupVisitor.put("device_type", "A");
        return hashMapSignupVisitor;
    }

    public HashMap<String, String> getHashMapForSellerSignup(HashMap<String, String> hashMapSignupSeller) {
        hashMapSignupSeller.put("device_type", "A");

        return hashMapSignupSeller;
    }

    public HashMap<String, String> getHashMapLogin(HashMap<String, String> hashMapLogin) {
        hashMapLogin.put("device_type", "A");
        return hashMapLogin;
    }

    public HashMap<String, String> getHashMapPostJob(HashMap<String, String> hashMap) {
        return hashMap;
    }

    public HashMap<String, String> getGalleryImageUpload(File image) {
        HashMap<String, String> hashMapGallery = new HashMap<>();
        hashMapGallery.put("image", image.getAbsolutePath());
        return hashMapGallery;
    }

    public HashMap<String, String> getPostOnTimeLine(HashMap<String, String> hashMap) {
        return hashMap;
    }

    public HashMap<String, String> getHashMapOffer(HashMap<String, String> hashMapOffer) {
        return hashMapOffer;
    }

    public HashMap<String, String> getHashMapForNewsApi(HashMap<String, String> hashMapNewsApi) {
        return hashMapNewsApi;
    }

    public HashMap<String, String> getHashMapForDoFollow(HashMap<String, String> doFollowMap) {
        return doFollowMap;
    }

    public HashMap<String, String> getHashMapLike(HashMap<String, String> likeHashMap) {
        return likeHashMap;
    }

    public HashMap<String, String> getHashMapForComment(HashMap<String, String> hashMapForComment) {
        return hashMapForComment;
    }

    public HashMap<String, String> getCommentSendHashMap(HashMap<String, String> commentSendHashMap) {
        return commentSendHashMap;
    }

    public HashMap<String, String> getJobList(HashMap<String, String> jobHashMap) {
        return jobHashMap;
    }

    public HashMap<String, String> getHashMapForForgotPassword(HashMap<String, String> forgotPassword) {
        return forgotPassword;
    }

    public HashMap<String, String> getHashMapApplyJob(HashMap<String, String> hashMapApplyJob) {
        return hashMapApplyJob;
    }

    public HashMap<String, String> getHashMapForSendQuery(HashMap<String, String> hashMapSendQuery) {
        return hashMapSendQuery;
    }

    public HashMap<String, String> getSellerList(HashMap<String, String> sellerHashMap) {
        return sellerHashMap;
    }

    public HashMap<String, String> getHashMapForEditSeller(HashMap<String, String> hashMapSellerEdit) {
        hashMapSellerEdit.put("device_type", "A");
        return hashMapSellerEdit;
    }

    public HashMap<String, String> getHashMapVisitorEdit(HashMap<String, String> hashMapVisitorEdit) {
        hashMapVisitorEdit.put("device_type", "A");
        return hashMapVisitorEdit;
    }

    public HashMap<String, String> getHashMapForMapSellerDetails(HashMap<String, String> hashMapForSellerDetails) {
        return hashMapForSellerDetails;
    }

    public HashMap<String, String> getHashMapForNotification(HashMap<String, String> notificationMap) {
        return notificationMap;
    }

    public HashMap<String, String> getHashMapForDetails(HashMap<String, String> mapDetails) {
        return mapDetails;
    }

    public Map<String, String> getPostOffer(Map<String, String> postOffer) {
        return postOffer;
    }

    public HashMap<String, String> getHashMapForAddBranch(HashMap<String, String> addBranch) {
        return addBranch;
    }

    public HashMap<String, String> getHashMapForEditBranch(HashMap<String, String> hashMapEdit) {
        return hashMapEdit;
    }
}
