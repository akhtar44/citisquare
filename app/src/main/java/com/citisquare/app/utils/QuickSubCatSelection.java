package com.citisquare.app.utils;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.citisquare.app.R;
import com.citisquare.app.adapter.SubCategoryAdapter;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.pojo.response.SubCategory;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 3/10/16.
 */

public class QuickSubCatSelection {
    PopupWindow popupWindow;
    View view;
    boolean popUpVisible = false;
    int position;
    ItemEventListener<Integer> itemEventListener;
    @BindView(R.id.categoryListview)
    ListView categoryListview;
    TextView textView;
    Context context;
    List<SubCategory> categoryArrayList;
    SubCategoryAdapter categoryDropDownAdapter;
    int height;
    int[] location = new int[2];


    public QuickSubCatSelection(Context context, int height, int[] location, List<SubCategory> categoryArrayList, ItemEventListener<Integer> integerItemEventListener) {
        view = View.inflate(context, R.layout.categories_dropdown, null);
        ButterKnife.bind(this, view);
        itemEventListener = integerItemEventListener;
        this.context = context;
        categoryListview.setTextFilterEnabled(true);
        this.categoryArrayList = categoryArrayList;
        this.location = location;
        this.height = height;
        popupWindow = new PopupWindow(view, LinearLayout.LayoutParams.MATCH_PARENT, (int) context.getResources().getDimension(R.dimen.dp_180));
    }


    public void setAdapter() {
        categoryListview.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        categoryDropDownAdapter = new SubCategoryAdapter(context, R.layout.raw_category_dropdown, categoryArrayList);
        categoryListview.setAdapter(categoryDropDownAdapter);

        popupWindow.setContentView(view);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0));
        popupWindow.setFocusable(true);
        if (popupWindow.isFocusable()) {
            popUpVisible = false;
        }
        categoryListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                itemEventListener.onItemEventFired(position, 0, "");
                dismiss();
            }
        });
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int pos) {
        position = pos;
    }

    public void show() {
        popUpVisible = true;
        setAdapter();
        popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);
        /*popupWindow.showAtLocation(view, Gravity.CENTER, location[0],
                location[1] + height + 10);*/
    }


    public void dismiss() {
        popUpVisible = false;
        popupWindow.dismiss();
    }

    public boolean isVisible() {
        return popUpVisible;
    }
}
