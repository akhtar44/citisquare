package com.citisquare.app.utils;

import android.content.Context;
import android.support.v4.app.FragmentManager;

import com.citisquare.app.fragments.BaseFragment;
import com.citisquare.app.listener.OnBackPressListener;

/**
 * Created by shahabuddin on 6/6/14.
 */
public class BackPressImpl implements OnBackPressListener {

    private BaseFragment parentFragment;
    private Context context;

    public BackPressImpl(BaseFragment parentFragment) {
        this.parentFragment = parentFragment;
    }

    @Override
    public boolean onBackPressed() {

        if (parentFragment == null) return false;


        int childCount = parentFragment.getChildFragmentManager().getBackStackEntryCount();

        if (childCount == 0) {
            // it has no child Fragment
            // can not handle the onBackPressed task by itself
            return false;

        } else {
            // get the child Fragment
            parentFragment.setHeaderTitle();
            FragmentManager childFragmentManager = parentFragment.getChildFragmentManager();
            OnBackPressListener childFragment = (OnBackPressListener) childFragmentManager.getFragments().get(0);

            // propagate onBackPressed method call to the child Fragment
            if (!childFragment.onBackPressed()) {
                // child Fragment was unable to handle the task
                // It could happen when the child Fragment is last last leaf of a chain
                // removing the child Fragment from stack
                childFragmentManager.popBackStackImmediate();

            }

            // either this Fragment or its child handled the task
            // either way we are successful and done here
            return true;
        }
    }
}
