package com.citisquare.app.utils;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.model.KeyvaluePair;
import com.citisquare.app.pojo.Filter;
import com.citisquare.app.pojo.response.CategoryData;
import com.citisquare.app.pojo.response.CategoryWrapper;
import com.citisquare.app.pojo.response.Country;
import com.citisquare.app.pojo.response.CountryWrapper;

import java.util.ArrayList;
import java.util.List;

import serviceCalling.builder.KeyValuePair;
import serviceCalling.helper.GetRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink16 on 25/5/16.
 */
public class InitializeMenu {
    private List<KeyvaluePair> keyvaluePairList;

    private Drawable drawable;
    private ArrayList<CategoryData> categoryDatas;
    private ArrayList<Filter> filtersList;
    private ArrayList<String> cityList;
    private ArrayList<Integer> titleList;
    private ArrayList<String> nowShowing;
    private ArrayList<Country> countryDatas;
    private ArrayList<com.citisquare.app.pojo.response.City> cityDatas;
    private ArrayList<String> resontoConnect;
    private ArrayList<String> sellerType;


    public void init(Context context) {
        countryDatas = new ArrayList<>();
        categoryDatas = new ArrayList<>();
        cityDatas = new ArrayList<>();
        toSetCountry(context);
        setCategory(context);
    }

    public void toSetCountry(final Context context) {
        new GetRequestHelper<CountryWrapper>().pingToRequest(Constants.COUNTRY, new ArrayList<KeyValuePair>(), ((MainActivity) context).getHeader()
                , new CountryWrapper(), new TaskCompleteListener<CountryWrapper>() {
                    @Override
                    public void onSuccess(CountryWrapper countryWrapper) {
                        if (countryWrapper.getStatus() == 1) {
                            // ((MainActivity) getActivity()).messageAlert(login.getMessage());
                            countryDatas.clear();
                            countryDatas.addAll(countryWrapper.getData().getCountry());
                            cityDatas.addAll(countryWrapper.getData().getCity());
                            ((MainActivity) context).callCity();
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, CountryWrapper countryWrapper) {

                    }
                });
    }

    public ArrayList<Country> toGetCountryCode() {
        if (countryDatas != null)
            return countryDatas;
        else
            return new ArrayList<Country>();
    }

    public ArrayList<com.citisquare.app.pojo.response.City> toGetCityCode() {
        if (cityDatas != null)
            return cityDatas;
        else
            return new ArrayList<>();
    }

    public void setCategory(final Context context) {
        new GetRequestHelper<CategoryWrapper>().pingToRequest(Constants.CATEGORY
                , new ArrayList<KeyValuePair>()
                , ((MainActivity) context).getHeader()
                , new CategoryWrapper(), new TaskCompleteListener<CategoryWrapper>() {
                    @Override
                    public void onSuccess(CategoryWrapper categoryData) {
                        if (categoryData.getStatus() == 1) {
                            categoryDatas.clear();
                            categoryDatas.addAll(categoryData.getData());
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, CategoryWrapper categoryData) {

                    }
                });
    }

    public ArrayList<CategoryData> toGetCategory() {
        /*Debugger.e("BEFORE RETURN:::::" + categoryDatas.toString());*/
        if (categoryDatas != null)
            return categoryDatas;
        else
            return new ArrayList<>();
    }


    public List<KeyvaluePair> toGetMenuList(Context context) {
        drawable = new BitmapDrawable();
        keyvaluePairList = new ArrayList<>();
        if (((MainActivity) context).getUserForWholeApp() != null) {
            if (((MainActivity) context).getUserForWholeApp().equals(Enum.setUser.GUEST)) {
                keyvaluePairList.add(new KeyvaluePair(context.getResources().getDrawable(R.drawable.home), context.getString(R.string.HOME), 0));
                keyvaluePairList.add(new KeyvaluePair(context.getResources().getDrawable(R.drawable.settings), context.getString(R.string.SETTINGS), 0));
                keyvaluePairList.add(new KeyvaluePair(context.getResources().getDrawable(R.drawable.contactus), context.getString(R.string.contact_us), 0));
                keyvaluePairList.add(new KeyvaluePair(context.getResources().getDrawable(R.drawable.subscription), context.getString(R.string.subscriptionPlan), 0));
                keyvaluePairList.add(new KeyvaluePair(context.getResources().getDrawable(R.drawable.sign_in), context.getString(R.string.login), 0));
            } else if (((MainActivity) context).getUserForWholeApp().equals(Enum.setUser.VISITOR)) {
                keyvaluePairList.add(new KeyvaluePair(context.getResources().getDrawable(R.drawable.home), context.getString(R.string.HOME), 0));
                keyvaluePairList.add(new KeyvaluePair(context.getResources().getDrawable(R.drawable.notification_slice), context.getString(R.string.NOTIFICATIONS), 0));
                keyvaluePairList.add(new KeyvaluePair(context.getResources().getDrawable(R.drawable.message), context.getString(R.string.MESSAGES), 0));
                keyvaluePairList.add(new KeyvaluePair(context.getResources().getDrawable(R.drawable.settings), context.getString(R.string.SETTINGS), 0));
                keyvaluePairList.add(new KeyvaluePair(context.getResources().getDrawable(R.drawable.contactus), context.getString(R.string.contact_us), 0));
                keyvaluePairList.add(new KeyvaluePair(context.getResources().getDrawable(R.drawable.settings), context.getString(R.string.accountsettings), 0));
                keyvaluePairList.add(new KeyvaluePair(context.getResources().getDrawable(R.drawable.logout), context.getString(R.string.LOGOUT), 0));
            } else {
                keyvaluePairList.add(new KeyvaluePair(context.getResources().getDrawable(R.drawable.home), context.getString(R.string.HOME), 0));
                keyvaluePairList.add(new KeyvaluePair(context.getResources().getDrawable(R.drawable.notification_slice), context.getString(R.string.NOTIFICATIONS), 0));
                keyvaluePairList.add(new KeyvaluePair(context.getResources().getDrawable(R.drawable.message), context.getString(R.string.MESSAGES), 0));
                keyvaluePairList.add(new KeyvaluePair(context.getResources().getDrawable(R.drawable.settings), context.getString(R.string.SETTINGS), 0));
                keyvaluePairList.add(new KeyvaluePair(context.getResources().getDrawable(R.drawable.subscription), context.getString(R.string.subscriptionPlan), 0));
                keyvaluePairList.add(new KeyvaluePair(context.getResources().getDrawable(R.drawable.contactus), context.getString(R.string.contact_us), 0));
                keyvaluePairList.add(new KeyvaluePair(context.getResources().getDrawable(R.drawable.settings), context.getString(R.string.accountsettings), 0));
                keyvaluePairList.add(new KeyvaluePair(context.getResources().getDrawable(R.drawable.megaphone), context.getString(R.string.advertisingWithUs), 0));
                keyvaluePairList.add(new KeyvaluePair(context.getResources().getDrawable(R.drawable.logout), context.getString(R.string.LOGOUT), 0));
            }
        } else {
            Debugger.e("CALL OTHER 2");
            keyvaluePairList.add(new KeyvaluePair(context.getResources().getDrawable(R.drawable.logout), context.getString(R.string.LOGOUT), 0));
        }
        return keyvaluePairList;
    }


    public ArrayList<Filter> toGetFilter(Context context) {
        filtersList = new ArrayList<>();
        filtersList.add(new Filter(context.getString(R.string.nearby)));
        filtersList.add(new Filter(context.getString(R.string.qiverifieddrop)));
        //filtersList.add(new Filter(context.getString(R.string.delivery)));
        filtersList.add(new Filter(context.getString(R.string.followersDrop)));
        filtersList.add(new Filter(context.getString(R.string.ratings)));

        return filtersList;
    }

    public ArrayList<Filter> toGetFilterJob(Context context) {
        filtersList = new ArrayList<>();
        filtersList.add(new Filter(context.getString(R.string.saudis_jobs_drop)));
        filtersList.add(new Filter(context.getString(R.string.non_saudis_jobs_drop)));
        return filtersList;
    }

    public ArrayList<String> toGetSubMenu(Context context) {
        nowShowing = new ArrayList<String>();
        nowShowing.add(context.getString(R.string.postOnTimeline));
        //nowShowing.add(context.getString(R.string.postAds));
        nowShowing.add(context.getString(R.string.postTodaysOffer));
        nowShowing.add(context.getString(R.string.postaslider));
        return nowShowing;
    }


    public ArrayList<String> toGetBranchList() {
        cityList = new ArrayList<>();
        cityList.add("No Branch");
        cityList.add("0 - 1 Branch ($200)");
        cityList.add("1 - 5 Branch ($300)");
        cityList.add("5 - 20 Branch ($400)");
        return cityList;
    }

    public ArrayList<String> toGetAdvertiseMent(Context context) {
        cityList = new ArrayList<>();
        cityList.add(context.getString(R.string.title_mr));
        cityList.add(context.getString(R.string.title_mrs));
        cityList.add(context.getString(R.string.title_miss));
        cityList.add(context.getString(R.string.title_dr));
        return cityList;
    }

    public ArrayList<String> getResontoConnect(Context context) {
        resontoConnect = new ArrayList<>();
        resontoConnect.add(context.getString(R.string.reason_to_connect_complains));
        resontoConnect.add(context.getString(R.string.reason_to_connect_advertise_with_us));
        resontoConnect.add(context.getString(R.string.reason_to_connect_suggestion));
        return resontoConnect;
    }

    public ArrayList<String> getSellerType() {
        sellerType = new ArrayList<>();
        sellerType.add("30 days trial");
        sellerType.add("Silver");
        sellerType.add("Gold");
        sellerType.add("Platinum");
        return sellerType;
    }
}
