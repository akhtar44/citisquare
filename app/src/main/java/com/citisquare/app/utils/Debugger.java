package com.citisquare.app.utils;

import android.util.Log;

/**
 * Created by hlink16 on 15/9/15.
 */
public class Debugger {

    public static void i(String tag, String message) {
        if (Constant.ENABLE_DEBUG) {
            Log.i(tag, message);
        }
    }

    public static void i(String message) {
        Debugger.i(Constant.TAG, message);
    }

    public static void e(String tag, String message) {
        if (Constant.ENABLE_DEBUG) {
            Log.e(tag, message);
        }
    }

    public static void e(String message) {
        if (Constant.ENABLE_DEBUG) {
            Debugger.e(Constant.TAG, message);
        }
    }

    public static void e(String tag, String message, Exception e) {
        if (Constant.ENABLE_DEBUG) {
            Log.e(tag, message);
            e.printStackTrace();
        }
    }

}
