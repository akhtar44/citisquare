package com.citisquare.app.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.URLSpan;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.citisquare.app.R;
import com.citisquare.app.SessionStore;
import com.citisquare.app.activity.LogInActivity;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.model.SpinnerModel;
import com.citisquare.app.model.UserKeyDetailsModel;
import com.citisquare.app.spinnersearchable.SearchableSpinner;
import com.google.gson.Gson;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utility {

    private static final Utility instance = new Utility();
    private static final String TAG = "Utility";
    private static Gson gson = new Gson();
    public static String[] thumbColumns = {MediaStore.Video.Thumbnails.DATA};
    public static String[] mediaColumns = {MediaStore.Video.Media._ID};

    private static int screenWidth = 0;
    private static int screenHeight = 0;

    private Utility() {
    }

    public static Utility getInstance() {
        return instance;
    }

    public static Gson getGson() {
        return gson;
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int getScreenHeight(Context c) {
        if (screenHeight == 0) {
            WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            screenHeight = size.y;
        }

        return screenHeight;
    }

    public static int getScreenWidth(Context c) {
        if (screenWidth == 0) {
            WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            screenWidth = size.x;
        }

        return screenWidth;
    }

    public static boolean isAndroid5() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }


    public static void stripUnderlines(TextView textView) {
        Spannable s = new SpannableString(textView.getText());
        URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
        for (URLSpan span : spans) {
            int start = s.getSpanStart(span);
            int end = s.getSpanEnd(span);
            s.removeSpan(span);
            s.setSpan(span, start, end, 0);
        }
        textView.setText(s);
    }

    public static String getPathFromUri(Uri uri, Context context) {


        String[] columns = {MediaStore.Images.Media.DATA,
                MediaStore.Images.Media.MIME_TYPE};

        Cursor cursor = context.getContentResolver().query(uri, columns, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
            int pathColumnIndex = cursor.getColumnIndex(columns[0]);
            String contentPath = cursor.getString(pathColumnIndex);
            cursor.close();
            return contentPath;
        }

        return null;
    }


    public static File saveFile(byte[] data) {
        File file = null;
        try {

            file = new File(Environment.getExternalStorageDirectory() + "/cs", "citiSquare" + System.currentTimeMillis() + "_.jpeg");
            file.getParentFile().mkdirs();
            OutputStream imageFileOS = new FileOutputStream(file);
            imageFileOS.write(data);
            imageFileOS.flush();
            imageFileOS.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    public static String createNewVideoFile() {
        File file = new File(Environment.getExternalStorageDirectory() + File.separator + "cs", "snap_" + System.currentTimeMillis() + ".mp4");
        file.getParentFile().mkdirs();
        return file.getPath();
    }

    public static File saveImage(Bitmap bitmap) {
        try {

            File file = new File(Environment.getExternalStorageDirectory() + "/cs", "thumb_" + System.currentTimeMillis() + "_.jpeg");
            file.getParentFile().mkdirs();
            BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file));
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            return file;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Bitmap resizeBitMapImage(String filePath, int targetWidth, int targetHeight) {
        Bitmap bitMapImage = null;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(filePath, options);
            double sampleSize = 0;
            Boolean scaleByHeight = Math.abs(options.outHeight - targetHeight) >= Math.abs(options.outWidth
                    - targetWidth);
            if (options.outHeight * options.outWidth * 2 >= 1638) {
                sampleSize = scaleByHeight ? options.outHeight / targetHeight : options.outWidth / targetWidth;
                sampleSize = (int) Math.pow(2d, Math.floor(Math.log(sampleSize) / Math.log(2d)));
            }
            options.inJustDecodeBounds = false;
            options.inTempStorage = new byte[128];
            while (true) {
                try {
                    options.inSampleSize = (int) sampleSize;
                    bitMapImage = BitmapFactory.decodeFile(filePath, options);
                    break;
                } catch (Exception ex) {
                    ex.printStackTrace();
                    try {
                        sampleSize = sampleSize * 2;
                    } catch (Exception ex1) {
                        ex1.printStackTrace();
                    }
                }
            }
        } catch (Exception ex) {

            ex.printStackTrace();
        }
        return bitMapImage;
    }

    public static String compressImage(String filePath) {

        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 1920.0f;//1280.0f;//816.0f;
        float maxWidth = 1080.0f;//852.0f;//612.0f;

        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Debugger.e("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Debugger.e("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Debugger.e("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Debugger.e("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }


    public static String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "cs/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }


    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }


    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri     The Uri to query.
     * @author paulburke
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public boolean IsInternetConnected(Context context) {
        boolean isConnected = false;
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context
                    .getSystemService(context.CONNECTIVITY_SERVICE);
            if (connectivityManager != null) {
                NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
                if (info != null) {
                    for (int i = 0; i < info.length; i++) {
                        if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                            isConnected = true;
                        }
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return isConnected;
    }

    public static void ShowToast(Context context, String message) {
        try {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public static void showAlertDialogSingleButton(final Context context, String message) {
        try {
            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(message)
                    .setCancelable(false)
                    .setNegativeButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog,
                                            @SuppressWarnings("unused") final int id) {
                            dialog.cancel();
                        }
                    });
            final AlertDialog alert = builder.create();
            alert.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static int MergeColors(int backgroundColor, int foregroundColor) {
        final byte ALPHA_CHANNEL = 24;
        final byte RED_CHANNEL = 16;
        final byte GREEN_CHANNEL = 8;
        final byte BLUE_CHANNEL = 0;
        int a = 0, r = 0, g = 0, b = 0;
        try {
            final double ap1 = (double) (backgroundColor >> ALPHA_CHANNEL & 0xff) / 255d;
            final double ap2 = (double) (foregroundColor >> ALPHA_CHANNEL & 0xff) / 255d;
            final double ap = ap2 + (ap1 * (1 - ap2));

            final double amount1 = (ap1 * (1 - ap2)) / ap;
            final double amount2 = amount1 / ap;

            a = ((int) (ap * 255d)) & 0xff;

            r = ((int) (((float) (backgroundColor >> RED_CHANNEL & 0xff) * amount1) +
                    ((float) (foregroundColor >> RED_CHANNEL & 0xff) * amount2))) & 0xff;
            g = ((int) (((float) (backgroundColor >> GREEN_CHANNEL & 0xff) * amount1) +
                    ((float) (foregroundColor >> GREEN_CHANNEL & 0xff) * amount2))) & 0xff;
            b = ((int) (((float) (backgroundColor & 0xff) * amount1) +
                    ((float) (foregroundColor & 0xff) * amount2))) & 0xff;

        } catch (Exception ex) {
            ex.printStackTrace();
        }


        return a << ALPHA_CHANNEL | r << RED_CHANNEL | g << GREEN_CHANNEL | b << BLUE_CHANNEL;
    }

    public void SupportActionBar(Activity activity, android.support.v7.app.ActionBar actionBar, String color_primary, TextView toolbarTitleTV, String title, boolean isHomeActivity) {
        String color_dark;
        try {
            color_dark = Constant.THEMECOLOR;

            actionBar.setTitle("");

            if (isHomeActivity) {
                try {
                    final Drawable upArrow = activity.getResources().getDrawable(R.drawable.hamburger24);
                    upArrow.setColorFilter(activity.getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
                    actionBar.setHomeAsUpIndicator(upArrow);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                //toolbarTitleTV.setTextSize(activity.getResources().getDimension(R.dimen.toolbar_title_10sp));
                try {
                    final Drawable upArrow = activity.getResources().getDrawable(R.drawable.arrow_left_white_24dp);
                    upArrow.setColorFilter(activity.getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
                    actionBar.setHomeAsUpIndicator(upArrow);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setElevation(0);
            toolbarTitleTV.setText(title);

            try {
                actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(color_primary)));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                int color = Utility.MergeColors(Color.parseColor(color_primary), Color.parseColor("#33000000"));
                color_dark = String.format("#%06X", (0xFFFFFF & color));
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = activity.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(Color.parseColor(color_dark));
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public String SaveImageGallery(Context context, Bitmap bitmap, String filename, String imgActualPath) {
        File file = null, galleryDirectory, directory;
        String root;
        byte[] byteArrayData;
        Bitmap localBitMap;
        FileOutputStream fos;
        boolean isGallery = false;
        try {
            root = Environment.getExternalStorageDirectory().toString();
            directory = new File(root + "/" + context.getString(R.string.app_name));
            if (!directory.exists())
                directory.mkdirs();

            galleryDirectory = directory.getParentFile();

            galleryDirectory = new File(directory + "/" + context.getString(R.string.app_name) + " Images");


            if (!galleryDirectory.exists())
                galleryDirectory.mkdirs();

            if (filename != null) {
                if (filename.toString().endsWith(".jpg")
                        || filename.toString().endsWith(".png")
                        || filename.toString().endsWith(".jpeg")) {

                    file = new File(galleryDirectory, filename);
                    isGallery = true;
                }
            }
            if (file != null && file.exists())
                file.delete();
            try {
                if (file != null) {
                    fos = new FileOutputStream(file, false);
                    if (isGallery) {
                        if (imgActualPath != null) {
                            localBitMap = BitmapFactory.decodeFile(imgActualPath);
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            localBitMap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                            byteArrayData = stream.toByteArray();
                            fos.write(byteArrayData);
                        } else
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 95, fos);
                    }
                    fos.flush();
                    fos.close();
                    return galleryDirectory + "/" + filename;
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            } catch (OutOfMemoryError ex) {
                ex.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: handle exception
        }
        return "";
    }

    public static ProgressDialog getProgressDialog(Context context, String message) {
        ProgressDialog progressDialog = null;
        try {
            progressDialog = new ProgressDialog(context, R.style.DialogStyle);
            progressDialog.setMessage(message);//context.getString(R.string.loading)
            //progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            progressDialog.setCancelable(false);
            // progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            // show it

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return progressDialog;
    }

    public void searchSpinnerItems(SearchableSpinner searchableSpinner, String strText, ArrayList<SpinnerModel> spinnerList) {
        final List<SpinnerModel> filteredList = new ArrayList<>();
        try {
            for (int i = 0; i < spinnerList.size(); i++) {

                final String text = spinnerList.get(i).getTitle().toLowerCase();

                if (text.toLowerCase().startsWith(strText)) {
                    filteredList.add(spinnerList.get(i));
                }
            }
            searchableSpinner.updateList(filteredList);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void dismissDialog(ProgressDialog progressDialog) {
        try {
            if (progressDialog != null)
                progressDialog.dismiss();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void showDialog(ProgressDialog progressDialog) {
        try {
            if (progressDialog != null)
                progressDialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void saveUserDetails(Context context, UserKeyDetailsModel userKeyDetailsModel) {
        try {

            if (!StringUtils.isBlank(userKeyDetailsModel.getUserGuid()))
                SessionStore.saveInformationMode(context, SessionStore.USER_ID, userKeyDetailsModel.getUserGuid());
            if (!StringUtils.isBlank(userKeyDetailsModel.getUsername()))
                SessionStore.saveInformationMode(context, SessionStore.USER_NAME, userKeyDetailsModel.getUsername());
            if (!StringUtils.isBlank(userKeyDetailsModel.getEmail()))
                SessionStore.saveInformationMode(context, SessionStore.EMAIL, userKeyDetailsModel.getEmail());
            if (!StringUtils.isBlank(userKeyDetailsModel.getPhoneNumber()))
                SessionStore.saveInformationMode(context, SessionStore.PHONE_NUMBER, userKeyDetailsModel.getPhoneNumber());
            if (!StringUtils.isBlank(userKeyDetailsModel.getProfile_image()))
                SessionStore.saveInformationMode(context, SessionStore.PROFILE_PIC, userKeyDetailsModel.getProfile_image());

            if (!StringUtils.isBlank(userKeyDetailsModel.getBranch_name()))
                SessionStore.saveInformationMode(context, SessionStore.BRANCH_NAME, userKeyDetailsModel.getBranch_name());
            if (!StringUtils.isBlank(userKeyDetailsModel.getUser_session_id()))
                SessionStore.saveInformationMode(context, SessionStore.USER_SESSION_ID, userKeyDetailsModel.getUser_session_id());
            if (!StringUtils.isBlank(userKeyDetailsModel.getCategory_count()))
                SessionStore.saveInformationMode(context, SessionStore.CATEGORY_COUNT, userKeyDetailsModel.getCategory_count());
            if (!StringUtils.isBlank(userKeyDetailsModel.getDevice_token()))
                SessionStore.saveInformationMode(context, SessionStore.DEVICE_TOKEN, userKeyDetailsModel.getDevice_token());
            if (!StringUtils.isBlank(userKeyDetailsModel.getLanguage()))
                SessionStore.saveInformationMode(context, SessionStore.LANGUAGE, userKeyDetailsModel.getLanguage());

            if (!StringUtils.isBlank(userKeyDetailsModel.getBuilding_no()))
                SessionStore.saveInformationMode(context, SessionStore.BUILDING_NO, userKeyDetailsModel.getBuilding_no());
            if (!StringUtils.isBlank(userKeyDetailsModel.getCity()))
                SessionStore.saveInformationMode(context, SessionStore.CITY, userKeyDetailsModel.getCity());
            if (!StringUtils.isBlank(userKeyDetailsModel.getCountry()))
                SessionStore.saveInformationMode(context, SessionStore.COUNTRY, userKeyDetailsModel.getCountry());
            if (!StringUtils.isBlank(userKeyDetailsModel.getStreet()))
                SessionStore.saveInformationMode(context, SessionStore.STREET, userKeyDetailsModel.getStreet());
            if (!StringUtils.isBlank(userKeyDetailsModel.getZipcode()))
                SessionStore.saveInformationMode(context, SessionStore.ZIPCODE, userKeyDetailsModel.getZipcode());

            if (!StringUtils.isBlank(userKeyDetailsModel.getFirst_name()))
                SessionStore.saveInformationMode(context, SessionStore.FIRST_NAME, userKeyDetailsModel.getFirst_name());
            if (!StringUtils.isBlank(userKeyDetailsModel.getLast_name()))
                SessionStore.saveInformationMode(context, SessionStore.LAST_NAME, userKeyDetailsModel.getLast_name());


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void minimizeActivity(Context context) {
        try {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public static void onErrorResponseMessage(Context context, VolleyError volleyError) {
        String message = "", json;
        JSONObject jsonObject;
        try {
            NetworkResponse response = volleyError.networkResponse;

            if (response != null && response.data != null) {
                switch (response.statusCode) {
                    case 400:
                        json = new String(response.data);
                        Log.i("onErrorResponMessage()", "response.statusCode - 400 - " + json);
                        jsonObject = new JSONObject(json);
                        message = jsonObject.getString("message");
                        if (message != null)
                            showAlertDialogSingleButton(context, message);
                        break;
                    default:
                        try {
                            Log.i("onErrorResponMessage()", "response.statusCode - " + response.statusCode);
                            json = new String(response.data);
                            //jsonObject = new JSONObject(json);
                            Log.i("onErrorResponMessage()", "response.statusCode - " + json);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        Utility.showVolleyAlertDialogSingleButton(context, volleyError);
                        break;
                }
                //Additional cases
            } else {
                Utility.showVolleyAlertDialogSingleButton(context, volleyError);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            Log.i("onErrorResponMessage()", "Exception - " + ex.getMessage());
        }
    }

    public static void showVolleyAlertDialogSingleButton(final Context context, VolleyError error) {
        String message = "";
        try {

            if (error instanceof TimeoutError) {
                message = context.getString(R.string.internet_timeout);
                showAlertDialogSingleButton(context, message);
            } else if (error instanceof AuthFailureError) {
                message = context.getString(R.string.internet_authFailureError);
                showAlertDialogSingleButton(context, message);
            } else if (error instanceof ServerError) {
                message = context.getString(R.string.internet_server_error);
                showAlertDialogSingleButton(context, message);
            } else if (error instanceof NetworkError) {
                message = context.getString(R.string.internet_gone);
                showAlertDialogSingleButton(context, message);
            } else if (error instanceof ParseError) {
                message = context.getString(R.string.internet_parse_error);
                showAlertDialogSingleButton(context, message);
            } else if (error instanceof ParseError) {
                message = context.getString(R.string.internet_parse_error);
                showAlertDialogSingleButton(context, message);
            } else {
                message = error.getMessage().toString();
                showNetwrokErrorMessage(context, message);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void showNetwrokErrorMessage(Context context, String exception) {
        try {
            if (exception.contains(Constant.SOCKET_TIMEOUT)) {
                String messageBody = context.getString(R.string.internet_slow);
                showAlertDialogSingleButton(context, messageBody);
            } else if (exception.contains(Constant.INVALID_HOSTNAME) || exception.contains(Constant.CONNECTION_GONE)) {
                String messageBody = context.getString(R.string.internet_gone);
                showAlertDialogSingleButton(context, messageBody);
            } else
                showAlertDialogSingleButton(context, exception);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public static UserKeyDetailsModel getUserKeyDetails(Context context) {
        UserKeyDetailsModel userKeyDetailsModel = new UserKeyDetailsModel();
        try {
            userKeyDetailsModel.setUserGuid(SessionStore.getInformationMode(context, SessionStore.USER_ID, ""));
            userKeyDetailsModel.setFirst_name(SessionStore.getInformationMode(context, SessionStore.FIRST_NAME, ""));
            userKeyDetailsModel.setLast_name(SessionStore.getInformationMode(context, SessionStore.LAST_NAME, ""));
            userKeyDetailsModel.setProfilePicture(SessionStore.getInformationMode(context, SessionStore.PROFILE_PIC, ""));
            userKeyDetailsModel.setThemeColorCode(SessionStore.getInformationMode(context, SessionStore.THEME_COLOR, ""));
            userKeyDetailsModel.setIsDefaultTheme(SessionStore.getBoolean(context, SessionStore.IS_DEFAULT, false));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return userKeyDetailsModel;
    }

    public static UserKeyDetailsModel getUserFullDetails(Context context) {
        UserKeyDetailsModel userKeyDetailsModel = new UserKeyDetailsModel();
        try {
            userKeyDetailsModel.setUserGuid(SessionStore.getInformationMode(context, SessionStore.USER_ID, ""));
            userKeyDetailsModel.setUsername(SessionStore.getInformationMode(context, SessionStore.USER_NAME, ""));
            userKeyDetailsModel.setPhoneNumber(SessionStore.getInformationMode(context, SessionStore.PHONE_NUMBER, ""));
            userKeyDetailsModel.setEmail(SessionStore.getInformationMode(context, SessionStore.EMAIL, ""));
            userKeyDetailsModel.setProfilePicture(SessionStore.getInformationMode(context, SessionStore.PROFILE_PIC, ""));

            userKeyDetailsModel.setFirst_name(SessionStore.getInformationMode(context, SessionStore.FIRST_NAME, ""));
            userKeyDetailsModel.setLast_name(SessionStore.getInformationMode(context, SessionStore.LAST_NAME, ""));

            userKeyDetailsModel.setThemeColorCode(SessionStore.getInformationMode(context, SessionStore.THEME_COLOR, ""));
            userKeyDetailsModel.setIsDefaultTheme(SessionStore.getBoolean(context, SessionStore.IS_DEFAULT, false));

            userKeyDetailsModel.setBranch_name(SessionStore.getInformationMode(context, SessionStore.BRANCH_NAME, ""));
            userKeyDetailsModel.setUser_session_id(SessionStore.getInformationMode(context, SessionStore.USER_SESSION_ID, ""));
            userKeyDetailsModel.setCategory_count(SessionStore.getInformationMode(context, SessionStore.CATEGORY_COUNT, ""));
            userKeyDetailsModel.setLanguage(SessionStore.getInformationMode(context, SessionStore.LANGUAGE, ""));
            userKeyDetailsModel.setDevice_token(SessionStore.getInformationMode(context, SessionStore.DEVICE_TOKEN, ""));

            userKeyDetailsModel.setBuilding_no(SessionStore.getInformationMode(context, SessionStore.BUILDING_NO, ""));
            userKeyDetailsModel.setCity(SessionStore.getInformationMode(context, SessionStore.CITY, ""));
            userKeyDetailsModel.setStreet(SessionStore.getInformationMode(context, SessionStore.STREET, ""));
            userKeyDetailsModel.setCountry(SessionStore.getInformationMode(context, SessionStore.COUNTRY, ""));
            userKeyDetailsModel.setZipcode(SessionStore.getInformationMode(context, SessionStore.ZIPCODE, ""));


        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return userKeyDetailsModel;
    }

    public boolean isValidEmail(String mail) {
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,256})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(mail);
        return matcher.matches();
    }

    public static HashMap<String, String> getHeaderOnlyContentType() {
        HashMap<String, String> header = new HashMap<>();
        try {
            //header.put("Content-Type", "application/json");
            //header.put("Accept", "application/json");
            header.put("API-KEY", Constants.X_API_KEY);

        } catch (Exception ex) {

        }
        return header;
    }

    public static void hideKeyboard(Activity activity) {
        try {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            //Find the currently focused view, so we can grab the correct window token from it.
            View view = activity.getCurrentFocus();
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            if (view == null) {
                view = new View(activity);
            }
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}