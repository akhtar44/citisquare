package com.citisquare.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SessionStore {

	private static String SHARE_PREFERENCE_NAME = "citiSquareSharePref";
	public static String PHONE_NUMBER = "PhoneNumber";
	public static String USER_ID = "userId";
	public static String USER_NAME = "userName";
	public static String EMAIL = "email";
	public static String PROFILE_PIC = "profielPic";
	public static String IS_FIRST_LOAD = "IsFirstLoad";
	public static String NAMAZ_LIST = "NamazList";

	public static String THEME_COLOR = "ThemeColor";
	public static String IS_DEFAULT = "IsDefault";

	public static String BRANCH_NAME = "branch_name";
	public static String USER_SESSION_ID = "user_session_id";
	public static String CATEGORY_COUNT = "category_count";
	public static String LANGUAGE = "language";
	public static String DEVICE_TOKEN = "device_token";

	public static String BUILDING_NO = "building_no";
	public static String STREET = "street";
	public static String CITY = "city";
	public static String COUNTRY = "country";
	public static String ZIPCODE = "zipcode";
	public static String FIRST_NAME = "first_name";
	public static String LAST_NAME = "last_name";


	public static void saveInformationMode(Context context, String key, String value) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				SHARE_PREFERENCE_NAME, Context.MODE_PRIVATE);
		Editor editor = sharedPreferences.edit();
		editor.putString(key, value);
		editor.commit();
	}

	public static String getInformationMode(Context context, String key,
			String defaultValue) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				SHARE_PREFERENCE_NAME, Context.MODE_PRIVATE);
		return sharedPreferences.getString(key, defaultValue);
	}

	public static void saveBoolean(Context context, String key, boolean value) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				SHARE_PREFERENCE_NAME, Context.MODE_PRIVATE);
		Editor editor = sharedPreferences.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}

	public static boolean getBoolean(Context context, String key,
									 boolean defaultValue) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				SHARE_PREFERENCE_NAME, Context.MODE_PRIVATE);
		return sharedPreferences.getBoolean(key, defaultValue);
	}

	public static String getSharePrefFileName() {
		return SHARE_PREFERENCE_NAME;
	}

}
