package com.citisquare.app.application;

import android.app.Application;
import android.content.Context;
import android.net.Uri;
import android.support.multidex.MultiDex;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import java.util.Locale;

import serviceCalling.builder.RequestBuilder;

import static com.android.volley.VolleyLog.TAG;

/**
 * Created by hlink56 on 26/2/16.
 */
public class QuickInfo extends Application {
    public static QuickInfo quickInfo;
    public static RequestBuilder requestBuilder;
    private RequestQueue mRequestQueue;

    public static synchronized QuickInfo getInstance() {
        return quickInfo;
    }

    public static RequestBuilder getInstanceBuilder() {
        return requestBuilder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //MultiDex.install(this);
        getApplicationContext().getResources().getConfiguration().setLayoutDirection(Locale.ENGLISH);
        quickInfo = this;
        requestBuilder = new RequestBuilder();
        requestBuilder.toIntializeObjects();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static RequestBuilder getRequestBuilder() {
        return requestBuilder;
    }

    public static Picasso providePicasso() {
        return new Picasso.Builder(getInstance())
                //.memoryCache(new LruCache(10*1024))
                .listener(new Picasso.Listener() {
                    @Override
                    public void onImageLoadFailed(Picasso picasso, Uri uri, Exception e) {

                    }
                })
                .build();
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }
}
