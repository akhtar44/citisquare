package com.citisquare.app.fragments;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.PlaceAutocompleteAdapter;
import com.citisquare.app.controls.CustomAutoComplete;
import com.citisquare.app.controls.CustomEditText;
import com.citisquare.app.controls.CustomRadioButton;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.dialog.CountryDialog;
import com.citisquare.app.dialog.ImageChooserDialog;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ImageChooserListener;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.interfaces.Navigation;
import com.citisquare.app.pojo.addresslocation.Address;
import com.citisquare.app.pojo.response.City;
import com.citisquare.app.pojo.response.Country;
import com.citisquare.app.pojo.response.LoginData;
import com.citisquare.app.pojo.response.LoginWrapper;
import com.citisquare.app.utils.BranchDropDown;
import com.citisquare.app.utils.ColorFilterTransformation;
import com.citisquare.app.utils.Debugger;
import com.citisquare.app.utils.InitializeMenu;
import com.citisquare.app.utils.RequestParameter;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import serviceCalling.builder.KeyValuePair;
import serviceCalling.builder.ServiceParameter;
import serviceCalling.helper.GetRequestHelper;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 29/12/16.
 */

public class SellerEditFragment extends BaseFragment {
    File editProfileImage;
    File timeLineImage;
    ArrayList<String> titleList;
    AdvertisementDropDown advertisementDropDown;
    CityDropDownSeller cityDropDown;
    City city;
    Country country;
    ArrayList<Country> countries;
    CountryCodeDropDown countryCodeDropDown;
    BranchDropDown branchDropDown;

    ArrayList<String> branchList;
    @BindView(R.id.editProfileSellerCoverImage)
    ImageView editProfileSellerCoverImage;
    @BindView(R.id.editProfileSellerImage)
    ImageView editProfileSellerImage;
    @BindView(R.id.changePasswordSeller)
    CustomTextView changePasswordSeller;
    @BindView(R.id.editProfileSellerCoverImageChoose)
    CustomTextView editProfileSellerCoverImageChoose;
    @BindView(R.id.editLayoutSeller)
    FrameLayout editLayoutSeller;
    @BindView(R.id.sellerProfileTitle)
    CustomTextView sellerProfileTitle;
    @BindView(R.id.sellerProfileFirstname)
    CustomEditText sellerProfileFirstname;
    @BindView(R.id.editTextSellerProfileLastName)
    CustomEditText editTextSellerProfileLastName;
    @BindView(R.id.editTextSellerProfileUsername)
    CustomEditText editTextSellerProfileUsername;
    @BindView(R.id.editTextSellerProfileEmail)
    CustomEditText editTextSellerProfileEmail;
    @BindView(R.id.editTextSellerProfileBuildingNumber)
    CustomEditText editTextSellerProfileBuildingNumber;
    @BindView(R.id.editTextSellerProfileStreet)
    CustomAutoComplete editTextSellerProfileStreet;
    @BindView(R.id.editTextSellerProfileCompanyNameArabic)
    CustomEditText editTextSellerProfileCompanyNameArabic;
    @BindView(R.id.editTextSellerZipCode)
    CustomAutoComplete editTextSellerZipCode;
    @BindView(R.id.editTextSellerProfileCity)
    CustomTextView editTextSellerProfileCity;
    @BindView(R.id.editTextSellerProfileCountry)
    CustomTextView editTextSellerProfileCountry;
    @BindView(R.id.editTextSellerProfileCountryCode)
    CustomTextView editTextSellerProfileCountryCode;
    @BindView(R.id.editTextSellerProfileCellPhoneNumber)
    CustomEditText editTextSellerProfileCellPhoneNumber;
    @BindView(R.id.editTextSellerProfileCountryCodeForPhone)
    CustomTextView editTextSellerProfileCountryCodeForPhone;
    @BindView(R.id.editTextSellerProfilePhoneNumber)
    CustomEditText editTextSellerProfilePhoneNumber;
    @BindView(R.id.editTextSellerProfileBio)
    CustomEditText editTextSellerProfileBio;
    @BindView(R.id.editTextSellerProfileCompanyName)
    CustomEditText editTextSellerProfileCompanyName;
    @BindView(R.id.editTextBusinessCompanyAlternateEmail)
    CustomEditText editTextBusinessCompanyAlternateEmail;
    @BindView(R.id.editTextSellerProfileWebsite)
    CustomEditText editTextSellerProfileWebsite;
    LoginWrapper loginWrapper;
    LoginData loginData;
    ArrayList<City> cityList;
    ArrayList<Country> countryList;
    Double lat, lng;
    @BindView(R.id.editTextSellerProfileBusinessHour)
    CustomEditText editTextSellerProfileBusinessHour;
    @BindView(R.id.englishRadioButtonSellerEditProfile)
    RadioButton englishRadioButtonSellerEditProfile;
    @BindView(R.id.arabicRadioButtonSellerEditProfile)
    RadioButton arabicRadioButtonSellerEditProfile;
    @BindView(R.id.preferredLanguageRadioSellerEditProfile)
    RadioGroup preferredLanguageRadioSellerEditProfile;

    @BindView(R.id.locationImageViewSellerEditProfile)
    ImageView locationImageViewSellerEditProfile;
    private Pattern pattern;
    private Matcher matcher;
    private int branchId = 0;
    private PlaceAutocompleteAdapter placeAdapter;
    private String languageString = "";
    private LatLng locationGlobal;
    private Integer title = -1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_edit_seller_profile, container, false);
        ButterKnife.bind(this, view);
        pattern = Pattern.compile(Constants.englishOnly);

        preferredLanguageRadioSellerEditProfile.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int radio = preferredLanguageRadioSellerEditProfile.getCheckedRadioButtonId();
                switch (radio) {
                    case R.id.englishRadioButtonSellerEditProfile:
                        languageString = String.valueOf(Constants.forLanguage.ENGLISH);
                        break;
                    case R.id.arabicRadioButtonSellerEditProfile:
                        languageString = String.valueOf(Constants.forLanguage.ARABIC);
                        break;
                }
            }
        });

        cityList = ((MainActivity) getActivity()).cityArrayList();
        countryList = ((MainActivity) getActivity()).countryDatas();
        loginWrapper = ((MainActivity) getActivity()).getData(getContext());

        pattern = Pattern.compile(Constants.englishOnly);

        if (loginWrapper != null) {
            loginData = loginWrapper.getData();
            sellerProfileTitle.setText(loginData.getTitle());
            sellerProfileFirstname.setText(loginData.getFirstName());
            editTextSellerProfileLastName.setText(loginData.getLastName());
            editTextSellerProfileUsername.setText(loginData.getUsername());
            editTextSellerProfileEmail.setText(loginData.getEmail());
            editTextSellerProfileBuildingNumber.setText(loginData.getBuildingNo());
            editTextSellerProfileStreet.setText(loginData.getStreet());
            editTextSellerZipCode.setText(loginData.getZipcode());
            editTextSellerProfileCountryCode.setText(loginData.getCellCountryCode());
            editTextSellerProfileCellPhoneNumber.setText(loginData.getCellPhoneno());
            editTextSellerProfileCountryCodeForPhone.setText(loginData.getCellCountryCode());
            editTextSellerProfilePhoneNumber.setText(loginData.getPhoneNo());
            editTextSellerProfileBio.setText(loginData.getBio());
            editTextSellerProfileCompanyName.setText(loginData.getCompanyName());
            editTextSellerProfileCompanyNameArabic.setText(loginData.getCompanyNameAr());
            editTextBusinessCompanyAlternateEmail.setText(loginData.getAlternateEmail());
            editTextSellerProfileWebsite.setText(loginData.getWebsite());
            editTextSellerProfileBusinessHour.setText(loginData.getBusinessHours());
            if(loginData.getLatitude()!=null)
            {
                LatLng loc = new LatLng(Double.parseDouble(loginData.getLatitude()), Double.parseDouble(loginData.getLongitude()));
                locationGlobal = loc;
                getGoogleMapThumbnail(loc);
            }

            if (loginData.getLanguage().equalsIgnoreCase(String.valueOf(Constants.forLanguage.ENGLISH))) {
                englishRadioButtonSellerEditProfile.setChecked(true);
            } else {
                arabicRadioButtonSellerEditProfile.setChecked(true);
            }

            ((MainActivity) getActivity()).setCircularImage(editProfileSellerImage, loginData.getProfileImageThumb());

            for (int i = 0; i < cityList.size(); i++) {
                if (cityList.get(i).getId().equals(loginData.getCity())) {
                    editTextSellerProfileCity.setText(cityList.get(i).getName());
                    String cityGson = new Gson().toJson(cityList.get(i));
                    city = new Gson().fromJson(cityGson, City.class);
                    break;
                }
            }

            for (int i = 0; i < countryList.size(); i++) {
                if (countryList.get(i).getId().equals(loginData.getCountry())) {
                    editTextSellerProfileCountry.setText(countryList.get(i).getCountry());
                    String countryGson = new Gson().toJson(countryList.get(i));
                    country = new Gson().fromJson(countryGson, Country.class);
                    break;
                }
            }
                if (loginData.getCoverImage()!=null) {
                    Picasso.with(getActivity()).load(loginData.getCoverImage()).transform(new ColorFilterTransformation(getResources().getColor(R.color.transparent_blue))).into(editProfileSellerCoverImage);
                }
                else {
                    editProfileSellerCoverImage.setColorFilter(getResources().getColor(R.color.transparent_blue));
                }

        }


        placeAdapter = new PlaceAutocompleteAdapter(getContext(), R.layout.raw_autocomplete_text);
        editTextSellerProfileStreet.setAdapter(placeAdapter);

        editTextSellerProfileStreet.setThreshold(1);

        editTextSellerProfileBio.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.editTextSellerProfileBio) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        return view;
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle(getString(R.string.title_editProfile), Enum.setNevigationIcon.BACK_SAVE, true);
    }

    public void editSellerProfile() {
        if (locationGlobal != null) {
            lat = locationGlobal.latitude;
            lng = locationGlobal.longitude;
        } else {
            lat = 0d;
            lng = 0d;
        }
        sallerEdit(lat, lng);
    }

    public void getLocation() {
        new GetRequestHelper<Address>().pingToRequest("http://maps.googleapis.com/maps/api/geocode/json?address=" +
                        editTextSellerProfileStreet.getText().toString().replace("\"", "") + "&sensor=true"
                , new ArrayList<KeyValuePair>()
                , new ServiceParameter()
                , new Address()
                , new TaskCompleteListener<Address>() {
                    @Override
                    public void onSuccess(Address mObject) {
                        try {
                            Debugger.e("GET LOCATION:::" + mObject);
                            if (mObject.getStatus().equalsIgnoreCase("ok")) {
                                lat = mObject.getResults().get(0).getGeometry().getLocation().getLat();
                                lng = mObject.getResults().get(0).getGeometry().getLocation().getLng();
                                sallerEdit(lat, lng);
                            } else {
                                ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterValidLocation));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, Address address) {

                    }
                });
    }


    public void sallerEdit(Double lat, Double lng) {
        if (getActivity() != null)
            ((MainActivity) getActivity()).startLoader();

        List<KeyValuePair> keyValuePair = new ArrayList<>();
        if (timeLineImage != null)
            keyValuePair.add(new KeyValuePair("cover_image", timeLineImage.getAbsolutePath()));

        if (editProfileImage != null)
            keyValuePair.add(new KeyValuePair("profile_image", editProfileImage.getAbsolutePath()));


        new PostRequestHelper<LoginWrapper>().pingToRequest(Constants.SELLER_EDIT
                , "image/*"
                , keyValuePair
                , new RequestParameter().getHashMapForEditSeller(getHashMapSeller(lat, lng))
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new LoginWrapper()
                , new TaskCompleteListener<LoginWrapper>() {
                    @Override
                    public void onSuccess(LoginWrapper loginWrapper) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        if (getActivity() != null && loginWrapper != null) {
                            if (loginWrapper.getStatus() == 1) {
                                ((MainActivity) getActivity()).setData(getContext(), new Gson().toJson(loginWrapper));
                                ((MainActivity) getActivity()).messageAlert(loginWrapper.getMessage());
                                //getActivity().onBackPressed();
                            } else {
                                ((MainActivity) getActivity()).messageAlert(loginWrapper.getMessage());
                            }
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, LoginWrapper loginWrapper) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        if(loginWrapper!=null){
                            ((MainActivity)getActivity()).messageAlert(loginWrapper.getMessage());
                        }
                    }
                });
    }
     /*first_name,username,email,phone_no,building_no,street,city,country,country_code,device_type,device_token,latitude,longitude,branch_name
        website,bio,cell_country_code,cell_phoneno,cover_image,profile_image*/


    @NonNull
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static String getStringByLocal(Context context, int id, String locale) {
        Configuration configuration = new Configuration(context.getResources().getConfiguration());
        configuration.setLocale(new Locale(locale));
        return context.createConfigurationContext(configuration).getResources().getString(id);
    }

    public HashMap<String, String> getHashMapSeller(Double lat, Double lng) {
        /*
        title,first_name,username,email,building_no,street,zipcode,city,country,cell_country_code,
        cell_phoneno,device_type,device_token,latitude,longitude,
        company_name,alternate_email,website,bio,country_code,phone_no,cover_image,profile_image*/
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("title", sellerProfileTitle.getText().toString().trim());
        hashMap.put("first_name", sellerProfileFirstname.getText().toString().trim());
        hashMap.put("last_name", editTextSellerProfileLastName.getText().toString().trim());
        hashMap.put("username", editTextSellerProfileUsername.getText().toString().trim());
        hashMap.put("email", editTextSellerProfileEmail.getText().toString().trim());
        hashMap.put("building_no", editTextSellerProfileBuildingNumber.getText().toString().trim());
        hashMap.put("street", editTextSellerProfileStreet.getText().toString().trim());
        hashMap.put("zipcode", editTextSellerZipCode.getText().toString().trim());
        hashMap.put("city", city.getId());
        hashMap.put("country", country.getId());
        //hashMap.put("company_name_ar", editTextSellerProfileCompanyNameArabic.getText().toString().trim());
        hashMap.put("cell_country_code", country.getDialCode());
        hashMap.put("cell_phoneno", editTextSellerProfileCellPhoneNumber.getText().toString().trim());

        if (editTextSellerProfilePhoneNumber.getText().length() > 0) {
            hashMap.put("phone_no", editTextSellerProfilePhoneNumber.getText().toString().trim());
            hashMap.put("country_code", country.getDialCode());
        }

        hashMap.put("device_token", ((MainActivity) getActivity()).getDeviceId());
        hashMap.put("latitude", String.valueOf(lat));
        hashMap.put("longitude", String.valueOf(lng));
        hashMap.put("company_name", editTextSellerProfileCompanyName.getText().toString().trim());
        hashMap.put("alternate_email", editTextBusinessCompanyAlternateEmail.getText().toString().trim());
        hashMap.put("website", editTextSellerProfileWebsite.getText().toString().trim());
        hashMap.put("bio", editTextSellerProfileBio.getText().toString().trim());
        hashMap.put("business_hours", editTextSellerProfileBusinessHour.getText().toString().trim());
        hashMap.put("language", languageString);

        return hashMap;
    }

    public boolean isValid() {

        if (editTextSellerProfileCompanyName.getText().toString().trim().length() == 0) {
            //((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterCompanyName));
            editTextSellerProfileCompanyName.requestFocus();
            return false;
        }


        if (!validWebsite(editTextSellerProfileWebsite.getText().toString())) {
            editTextSellerProfileWebsite.requestFocus();
            return false;
        }


        if (sellerProfileTitle.getText().toString().equals(getString(R.string.title))) {
            sellerProfileTitle.setText(getText(R.string.title));
            //((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterTitle));
            sellerProfileTitle.requestFocus();
            return false;
        }


        if (sellerProfileFirstname.getText().toString().trim().length() == 0) {
            //((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterFirstName));
            sellerProfileFirstname.requestFocus();
            return false;
        }

        if (editTextSellerProfileLastName.getText().toString().trim().length() == 0) {
            //((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterLastName));
            editTextSellerProfileLastName.requestFocus();
            return false;
        }

        if (editTextSellerProfileUsername.getText().toString().trim().length() == 0) {
            //((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterUserName));
            editTextSellerProfileUsername.requestFocus();
            return false;
        }

        if (!validEmail(editTextSellerProfileEmail.getText().toString())) {
            editTextSellerProfileEmail.requestFocus();
            return false;
        }


        if (!alternateEmailValid(editTextBusinessCompanyAlternateEmail.getText().toString())) {
            editTextBusinessCompanyAlternateEmail.requestFocus();
            return false;
        }


        if (editTextSellerProfileCellPhoneNumber.getText().toString().trim().length() == 0) {
            //((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterMobileNumber));
            editTextSellerProfileCellPhoneNumber.requestFocus();
            return false;
        }


        if (editTextSellerProfileCellPhoneNumber.getText().toString().trim().length() < 9) {
            //((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterValidMobileNumber));
            editTextSellerProfileCellPhoneNumber.requestFocus();
            return false;
        } else if (editTextSellerProfilePhoneNumber.getText().toString().trim().length() > 0) {

            if (editTextSellerProfilePhoneNumber.getText().toString().length() < 9) {
              //  ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterValidPhoneNumber));
                editTextSellerProfilePhoneNumber.requestFocus();
                return false;
            }
        }


        if (editTextSellerProfileStreet.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterStreet));
            editTextSellerProfileStreet.requestFocus();
            return false;
        }

        if (editTextSellerZipCode.getText().toString().trim().length() == 0) {
            //((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterZipCode));
            editTextSellerZipCode.requestFocus();
            return false;
        }


        if (city == null) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterCity));
            editTextSellerProfileCity.requestFocus();
            return false;
        }


        if (country == null) {
         //   ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterCountry));
            editTextSellerProfileCountry.requestFocus();
            return false;
        }

        return true;
    }

    public boolean validWebsite(String webSite) {
        int length = webSite.trim().length();

        Pattern pattern = Pattern.compile(Navigation.WEBSITE_REGEX);
        Matcher matcher = pattern.matcher(webSite);
        if (length > 0) {
            if (!(webSite.charAt(0) + "").matches("\\p{L}") || !matcher.matches()) {
                ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterValidWEbsite));
                return false;
            }
        }
        return true;
    }


    private boolean alternateEmailValid(String email) {
        int length = email.trim().length();

        // Patterns.WEB_URL.matcher(potentialUrl).matches()
        Pattern pattern = Pattern.compile(Navigation.EMAIL_REGEX);
        Matcher matcher = pattern.matcher(email);
        if (length > 0) {
            if (!(email.charAt(0) + "").matches("\\p{L}") || !matcher.matches()) {
                ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterValidEmailId));
                return false;
            }
        }
        return true;
    }


    public boolean validEmail(String email) {
        int length = email.trim().length();
        if (length <= 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterEmail));
            return false;
        }
        // Patterns.WEB_URL.matcher(potentialUrl).matches()
        Pattern pattern = Pattern.compile(Navigation.EMAIL_REGEX);
        Matcher matcher = pattern.matcher(email);
        if (!(email.charAt(0) + "").matches("\\p{L}") || !matcher.matches()) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterValidEmailId));
            return false;
        }
        return true;
    }


    @OnClick({R.id.editProfileSellerCoverImageChoose, R.id.editProfileSellerImage, R.id.changePasswordSeller, R.id.sellerProfileTitle, R.id.editTextSellerProfileCity, R.id.editTextSellerProfileCountryCode})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.editProfileSellerCoverImageChoose:
                sellerProfileTimelineImage();
                break;
            case R.id.editProfileSellerImage:
                sellerProfileImage();
                break;
            case R.id.changePasswordSeller:
                ((MainActivity) getActivity()).changePasswordDialog();
                break;
            case R.id.sellerProfileTitle:
                title();
                break;
            case R.id.editTextSellerProfileCity:
                cityDataLoad();
                break;
            case R.id.editTextSellerProfileCountryCode:
                countrySelectDilog();
                break;


        }
    }

    /*private void selectBranchDropDown() {
        ((MainActivity) getActivity()).hideKeyboard();
        int[] location = new int[2];
        editTextSellerProfileSelectBranch.getLocationOnScreen(location);
        branchList = new InitializeMenu().toGetBranchList();
        branchDropDown = new BranchDropDown(getContext(), editTextSellerProfileSelectBranch.getHeight(), location, branchList, true, new ItemEventListener<Integer>() {
            @Override
            public void onItemEventFired(Integer integer, Integer t2) {
                editTextSellerProfileSelectBranch.setText(branchList.get(integer));
                branchId = integer + 1;

            }
        });
        branchDropDown.show();
    }*/


    private void countrySelectDilog() {
        CountryDialog.openCountryCodeDialog(getContext(), new ItemEventListener<String>() {
            @Override
            public void onItemEventFired(String s, String t2, String actualPos) {
                Debugger.e(s + " " + t2);
                country = new Gson().fromJson(t2, Country.class);
                editTextSellerProfileCountry.setText(country.getCountry());
                editTextSellerProfileCountryCode.setText(country.getDialCode());
                editTextSellerProfileCountryCodeForPhone.setText(country.getDialCode());

            }
        });
    }

    private void cityDataLoad() {
        ((MainActivity) getActivity()).hideKeyboard();
        int[] location = new int[2];
        editTextSellerProfileCity.getLocationOnScreen(location);
        final ArrayList<City> cityList = ((MainActivity) getActivity()).cityArrayList();
        cityDropDown = new CityDropDownSeller(getContext(), editTextSellerProfileCity.getHeight(), location, cityList, new ItemEventListener<String>() {
            @Override
            public void onItemEventFired(String integer, String t2, String actualPos) {
                city = new Gson().fromJson(t2, City.class);
                if (Locale.getDefault().getDisplayLanguage().equals("العربية"))
                    editTextSellerProfileCity.setText(city.getArName());
                else
                    editTextSellerProfileCity.setText(city.getName());

            }
        });
        cityDropDown.show();
    }

    private void title() {
        ((MainActivity) getActivity()).hideKeyboard();
        int[] location = new int[2];
        sellerProfileTitle.getLocationOnScreen(location);
        titleList = new InitializeMenu().toGetAdvertiseMent(getContext());
        advertisementDropDown = new AdvertisementDropDown(getContext(), sellerProfileTitle.getHeight(), location, titleList, new ItemEventListener<Integer>() {
            @Override
            public void onItemEventFired(Integer integer, Integer t2, String actualPos) {
                sellerProfileTitle.setText(titleList.get(integer));
                title = integer;
//                        branchId = integer + 1;
            }
        });
        advertisementDropDown.show();
    }

    private void sellerProfileTimelineImage() {
        ImageChooserDialog imageChooserDialog = new ImageChooserDialog();
        imageChooserDialog.show(getActivity().getSupportFragmentManager(), "imageChooserDialog");
        imageChooserDialog.setCallback(new ImageChooserListener() {

                                           @Override
                                           public void getResultFromCamera(String result) {
                                               timeLineImage = new File(result);
                                               new Handler().postDelayed(new Runnable() {
                                                   @Override
                                                   public void run() {
                                                       Picasso.with(getActivity()).load(timeLineImage).transform(new ColorFilterTransformation(getResources().getColor(R.color.transparent_blue))).into(editProfileSellerCoverImage);
                                                   }
                                               }, 50);

                                           }

                                           @Override
                                           public void getResultFromGallery(String result) {
                                               if (result != null) {
                                                   if (!(result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("jpeg") ||
                                                           result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("jpg") ||
                                                           result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("png") ||
                                                           result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("bmp"))) {
                                                       //isInvalidImage = true;
                                                       ((MainActivity) getActivity()).messageAlert(getString(R.string.message_thisImageIsNotSupported));

                                                   } else {
                                                       timeLineImage = new File(result);
                                                       new Handler().postDelayed(new Runnable() {
                                                           @Override
                                                           public void run() {
                                                               Picasso.with(getActivity()).load(timeLineImage)
                                                                       .transform(new ColorFilterTransformation(getResources().getColor(R.color.transparent_blue)))
                                                                       .into(editProfileSellerCoverImage);
                                                           }
                                                       }, 50);
                                                   }
                                               }
                                           }

                                           @Override
                                           public void nothingSet() {

                                           }
                                       }

        );
    }

    private void sellerProfileImage() {
        ImageChooserDialog imageChooserDialog = new ImageChooserDialog();
        imageChooserDialog.show(getActivity().getSupportFragmentManager(), "imageChooserDialog");
        imageChooserDialog.setCallback(new ImageChooserListener() {
            @Override
            public void getResultFromCamera(String result) {
                editProfileImage = new File(result);
                ((MainActivity) getActivity()).setCircularImage(editProfileImage, editProfileSellerImage);
            }

            @Override
            public void getResultFromGallery(String result) {
                if (result != null) {
                    if (!(result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("jpeg") ||
                            result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("jpg") ||
                            result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("png") ||
                            result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("bmp"))) {
                        //isInvalidImage = true;
                        ((MainActivity) getActivity()).messageAlert(getString(R.string.message_thisImageIsNotSupported));

                    } else {
                        editProfileImage = new File(result);
                        ((MainActivity) getActivity()).setCircularImage(editProfileImage, editProfileSellerImage);
                        //layoutAddItemImageViewAdd.setBackgroundColor(getResources().getColor(R.color.backColor));
                        //isInvalidImage = false;
                    }
                }
            }

            @Override
            public void nothingSet() {

            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick(R.id.locationImageViewSellerEditProfile)
    public void selectLocationClicked() {
        MakeYourBusinessLocationFragment locationFragment = new MakeYourBusinessLocationFragment();
        ((MainActivity) getActivity()).setFragment(locationFragment, true, "");
        locationFragment.setOnLocationSelectionListener(new MakeYourBusinessLocationFragment.OnLocationSelectionListener() {
            @Override
            public void locationSelect(LatLng location) {
                if (getActivity() != null) {
                    getActivity().onBackPressed();

                    getGoogleMapThumbnail(location);
                }
            }
        });
    }

    public void getGoogleMapThumbnail(LatLng place) {
        final String STATIC_MAP_URL = "https://maps.googleapis.com/maps/api/staticmap?";
        locationGlobal = place;
        String location = place.latitude + "," + place.longitude;
        if (!location.isEmpty()) {
            try {
                final StringBuilder stringBuilder = new StringBuilder(STATIC_MAP_URL);
                stringBuilder.append("zoom=" + "9");
                stringBuilder.append("&size=" + "600x400");
                stringBuilder.append("&maptype=" + "roadmap");
                stringBuilder.append("&markers=" + location);
                //stringBuilder.append("&key=" + STATIC_MAP_API_KEY);

                Log.e("MainURL", "onCreate: " + stringBuilder.toString() + "autocomplete adddress:" + location);
                locationImageViewSellerEditProfile.setVisibility(View.VISIBLE);
                Glide.with(getContext()).load(stringBuilder.toString()).asBitmap().into(locationImageViewSellerEditProfile);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        } else {
            Toast.makeText(getContext(), "can not find location", Toast.LENGTH_SHORT).show();
        }
    }
}
