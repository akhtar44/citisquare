package com.citisquare.app.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.ParentHomeAdapter;
import com.citisquare.app.adapter.SearchAdapter;
import com.citisquare.app.controls.CustomAutoComplete;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.pojo.response.Like;
import com.citisquare.app.pojo.response.OfferData;
import com.citisquare.app.pojo.response.OfferWrapper;
import com.citisquare.app.pojo.response.SearchData;
import com.citisquare.app.pojo.response.SearchWrapper;
import com.citisquare.app.utils.Debugger;
import com.citisquare.app.utils.RequestParameter;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import serviceCalling.builder.KeyValuePair;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 19/7/16.
 */
public class ParentHomeFragment extends BaseFragment {

    ParentHomeAdapter newsAdapter;
    ArrayList<OfferData> quickInfoDatas;

    List<String> list;
    @BindView(R.id.newsRecyclerView)
    RecyclerView newsRecyclerView;
    @BindView(R.id.relative_empty)
    RelativeLayout relative_empty;
    @BindView(R.id.txt_noinfo)
    TextView txt_noinfo;
    GridLayoutManager gridLayoutManager;
    boolean isPagination;
    int pageno;
    @BindView(R.id.searchTextInHomeScreen)
    CustomAutoComplete searchTextInHomeScreen;
    ProgressDialog pdialogue;
    SearchAdapter searchAdapter;
    List<SearchData> searchDatas;

    private Pattern pattern;
    private Matcher matcher;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.news_profile_layout, container, false);
        ButterKnife.bind(this, view);
        searchTextInHomeScreen.setVisibility(View.VISIBLE);
        searchDatas = new ArrayList<>();
        quickInfoDatas = new ArrayList<>();
        searchAdapter = new SearchAdapter(getContext(), R.layout.raw_search_listing, searchDatas);
        pattern = Pattern.compile(Constants.englishOnly);
        searchTextInHomeScreen.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                try {
                    SearchData searchData = searchDatas.get(position);
                    if (searchData != null) {
                        searchTextInHomeScreen.setText("");
                        if (searchData.getIsFlag().equalsIgnoreCase("U")) {
                            ProfileFragment profileFragment = new ProfileFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("profile", "other");
                            ((MainActivity) getActivity()).setProfileUserId(searchData.getId());
                            profileFragment.setArguments(bundle);
                            ((MainActivity) getActivity()).setSelectionPostion(0);
                            ((MainActivity) getActivity()).setFragment(profileFragment, false, "");
                        } else if (searchData.getIsFlag().equalsIgnoreCase("SC")) {
                            ((MainActivity) getActivity()).hideKeyboard();
                            Bundle bundle = new Bundle();
                            CategorySellerFragment categorySellerFragment = new CategorySellerFragment();
                            bundle.putString("subCat", searchData.getId());
                            bundle.putString("category", searchData.getParentId());

                            categorySellerFragment.setArguments(bundle);
                            childFragmentReplacement(categorySellerFragment, "child");
                        }
                    }
                } catch (Exception e) {

                }
            }
        });

        searchTextInHomeScreen.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                callSearchSellerCategoryApi(s);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });

        searchTextInHomeScreen.setAdapter(searchAdapter);
        newsAdapter = new ParentHomeAdapter(getContext(), quickInfoDatas, new ItemEventListener<String>() {
            @Override
            public void onItemEventFired(String pos, String userId, final String actualPos) {
                switch (pos) {
                    case "map":
                        ((MainActivity) getActivity()).setProfileUserId(userId);
                        ((MainActivity) getActivity()).setFragment(new OtherMapPathFragment(), false, "");
                        break;
                    case "share":
                        OfferData offerData = new Gson().fromJson(userId, OfferData.class);
                        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                        sharingIntent.putExtra(Intent.EXTRA_TEXT, offerData.getDescription() + "  " + offerData.getUrl());
                        sharingIntent.setType("text/plain");
                        startActivity(Intent.createChooser(sharingIntent, "Share application using"));
                        break;
                    case "Child":
                        DetailsHomeFragment detailsHomeFragment = new DetailsHomeFragment();
                        Bundle bundleDetails = new Bundle();
                        bundleDetails.putString("data", userId);
                        bundleDetails.putString("details", "Home");
                        detailsHomeFragment.setArguments(bundleDetails);
                        ((MainActivity) getActivity()).setFragment(detailsHomeFragment, false, "");
                        //((MainActivity) getActivity()).setFragment(new DetailsHomeFragment());
                        break;
                    case "comment":
                        if (((MainActivity) getActivity()).getUserForWholeApp().equals(Enum.setUser.GUEST)) {
                            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_Guest));
                        } else {
                            CommentFragment commentFragment = new CommentFragment();

                            commentFragment.setCommentListener(new CommentFragment.CommentListener() {
                                @Override
                                public void commentCount(int count) {
                                    quickInfoDatas.get(Integer.parseInt(actualPos)).setCommentCount(count);
                                    newsAdapter.notifyDataSetChanged();
                                }
                            });
                            Bundle bundleComment = new Bundle();
                            bundleComment.putString("postId", userId);
                            bundleComment.putString("FROM", "T");
                            commentFragment.setArguments(bundleComment);
                            ((MainActivity) getActivity()).setFragment(commentFragment, false, "");
                        }
                        break;
                    case "otherProfile":
                        Debugger.e("USER ID" + userId);
                        if (userId.equals(((MainActivity) getActivity()).userId())) {
                            ProfileFragment profileFragment = new ProfileFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("profile", "own");
                            ((MainActivity) getActivity()).setProfileUserId(userId);
                            profileFragment.setArguments(bundle);
                            ((MainActivity) getActivity()).setSelectionPostion(0);
                            ((MainActivity) getActivity()).setFragment(profileFragment, false, "");
                        } else {
                            ProfileFragment profileFragment = new ProfileFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("profile", "other");
                            ((MainActivity) getActivity()).setProfileUserId(userId);
                            profileFragment.setArguments(bundle);
                            ((MainActivity) getActivity()).setSelectionPostion(0);
                            ((MainActivity) getActivity()).setFragment(profileFragment, false, "");
                            //childFragmentReplacement(profileFragment, "child");
                        }
                        break;
                    case "video":
                        PlayVideoFragment playVideoFragment = new PlayVideoFragment();
                        Bundle playBundle = new Bundle();
                        playBundle.putString("video", userId);
                        playVideoFragment.setArguments(playBundle);
                        ((MainActivity) getActivity()).setFragment(playVideoFragment, false, "");
                        //childFragmentReplacement(playVideoFragment, "child");
                        break;
                    case "Follow":
                        doFollowApiCall(userId, "follow");
                        break;
                    case "Unfollow":
                        doFollowApiCall(userId, "unfollow");
                        break;
                    case "Like":
                        likeApiCall(userId);
                        break;

                }
            }
        });

        gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        newsRecyclerView.setLayoutManager(gridLayoutManager);
        newsRecyclerView.setAdapter(newsAdapter);

        newsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (isPagination) {
                    int visibleItemCount = recyclerView.getChildCount();
                    int totalItemCount = gridLayoutManager.getItemCount();
                    int firstVisibleItemPosition = gridLayoutManager.findFirstVisibleItemPosition();
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount) {
                        isPagination = false;

                        if (((MainActivity) getActivity()).getUserForWholeApp().equals(Enum.setUser.GUEST)) {
                            newTimeLineListApiCall(++pageno);
                        } else {
                            newsListApiCall(++pageno);
                        }
                    }
                }
            }
        });

        return view;
    }

    private void callSearchSellerCategoryApi(CharSequence s) {
        new PostRequestHelper<SearchWrapper>().pingToRequest(Constants.ALL_SEARCH_DETAIL,
                "",
                new ArrayList<KeyValuePair>(),
                getHashMapForSearchSellerAndCategory(s),
                ((MainActivity) getActivity()).getHeader(),
                new SearchWrapper(),
                new TaskCompleteListener<SearchWrapper>() {
                    @Override
                    public void onSuccess(SearchWrapper mObject) {
                        searchDatas.clear();
                        searchDatas.addAll(mObject.getData());
                        searchAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, SearchWrapper searchWrapper) {

                    }
                });
    }

    private HashMap<String, String> getHashMapForSearchSellerAndCategory(CharSequence search) {
        HashMap<String, String> hashMapForSellerAndCategory = new HashMap<>();
        hashMapForSellerAndCategory.put("city_id", ((MainActivity) getActivity()).getTopCitySelectionId());
        hashMapForSellerAndCategory.put("search", String.valueOf(search));
        return hashMapForSellerAndCategory;
    }

    public void likeApiCall(String postId) {
        new PostRequestHelper<Like>().pingToRequest(Constants.LIKE
                , null
                , null
                , new RequestParameter().getHashMapLike(getLikeHashMap(postId))
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new Like()
                , new TaskCompleteListener<Like>() {
                    @Override
                    public void onSuccess(Like mObject) {
                        if (mObject.getStatus() == 1)
                            Debugger.e("LIKE");
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, Like like) {
                        if (like != null) {
                            if (like.getStatus() == -1)
                                ((MainActivity) getActivity()).invalidToken();
                        }
                    }
                });
    }

    public void doFollowApiCall(final String userId, final String status) {
        new PostRequestHelper<Like>().pingToRequest(Constants.DO_FOLLOW
                , null
                , null
                , new RequestParameter().getHashMapForDoFollow(getDoFollowMap(userId, status))
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new Like()
                , new TaskCompleteListener<Like>() {
                    @Override
                    public void onSuccess(Like mObject) {
                        if (mObject.getStatus() == 1)
                            follow(userId, status);
                        Debugger.e("FOLLOW SUCCESSFULLY");
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, Like like) {
                        if (like != null) {
                            if (like.getStatus() == -1)
                                ((MainActivity) getActivity()).invalidToken();
                        }
                    }
                });
    }

    public void follow(String userId, String status) {
        for (int i = 0; i < quickInfoDatas.size(); i++) {
            if (quickInfoDatas.get(i).getUserId().equals(userId)) {
                if (status.equals("follow"))
                    quickInfoDatas.get(i).setIsFollowing(1);
                else
                    quickInfoDatas.get(i).setIsFollowing(0);
            }
        }
        newsAdapter.notifyDataSetChanged();
    }

    public HashMap<String, String> getLikeHashMap(String postId) {
        HashMap<String, String> likeHashMap = new HashMap<>();
        likeHashMap.put("flag", "T");
        likeHashMap.put("post_id", postId);
        likeHashMap.put("status", "1");
        return likeHashMap;
    }

    public HashMap<String, String> getDoFollowMap(String userId, String status) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("follower_user_id", userId);
        if (status.equals("follow"))
            hashMap.put("status", "1");
        else
            hashMap.put("status", "0");

        return hashMap;
    }

    public void callNewsApi() {
        if (((MainActivity) getActivity()).getUserForWholeApp().equals(Enum.setUser.GUEST)) {
            pageno = 1;
            newTimeLineListApiCall(pageno);
        } else {
            pageno = 1;
            //newsListApiCall(pageno);
            newTimeLineListApiCall(pageno);
        }
    }

    private void newTimeLineListApiCall(final int pageno) {
        pdialogue = ProgressDialog.show(getActivity(), null, getString(R.string.loading));
        pdialogue.setCancelable(false);
        pdialogue.setCanceledOnTouchOutside(false);
        new PostRequestHelper<OfferWrapper>().pingToRequest(Constants.NEWTIMELINE_LIST
                , null
                , null
                , new RequestParameter().getHashMapForNewsApi(getHashMapForNewTimeLine(pageno))
                , ((MainActivity) getActivity()).getHeader()
                , new OfferWrapper()
                , new TaskCompleteListener<OfferWrapper>() {
                    @Override
                    public void onSuccess(OfferWrapper offerWrapper) {

                        if (pageno == 1)
                            quickInfoDatas.clear();

                        if (offerWrapper.getStatus() == 1) {
                            isPagination = true;
                            //newsRecyclerView.setVisibility(View.VISIBLE);
                            setNewsData(offerWrapper.getData());
                            TabFragment.hideRefreshIcon(false);
                        } else {
                            isPagination = false;
                            if (pageno == 1)
                               // ((MainActivity) getActivity()).messageAlert(offerWrapper.getMessage());
                            Toast.makeText(getActivity(), offerWrapper.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                      
                    }
                     

                    @Override
                    public void onFailure(ExceptionType exceptions, OfferWrapper offerWrapper) {
                        isPagination = false;
                        if (pageno == 1)
                            quickInfoDatas.clear();
                        if(offerWrapper!=null){
                            if (offerWrapper.getStatus() == 0) {
                                isPagination = true;
                                setNewsData(offerWrapper.getData());
                            }
                        }
                        //newsRecyclerView.setVisibility(View.GONE);
                        //txt_noinfo.setVisibility(View.VISIBLE);
                        //pdialogue.dismiss();
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        setPage();

                        newsAdapter.notifyDataSetChanged();
                    }
                });
        pdialogue.dismiss();
    }

    private HashMap<String, String> getHashMapForNewTimeLine(int pageno) {
        HashMap<String, String> hashMapNews = new HashMap<>();
        String search = "";
        BaseFragment baseFragment = (BaseFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.placeholder);
        if (baseFragment instanceof TabFragment) {
            search = ((TabFragment) baseFragment).getSearchData();
        }

        hashMapNews.put("page", pageno + "");
        hashMapNews.put("search", search);
        hashMapNews.put("city_id", ((MainActivity) getActivity()).getTopCitySelectionId());
        return hashMapNews;
    }


    public void newsListApiCall(final int pageno) {
        pdialogue = ProgressDialog.show(getActivity(), null, getString(R.string.loading));
        pdialogue.setCancelable(false);
        pdialogue.setCanceledOnTouchOutside(false);
        new PostRequestHelper<OfferWrapper>().pingToRequest(Constants.TIMELINE_LIST
                , null
                , null
                , new RequestParameter().getHashMapForNewsApi(getHashMapNews(pageno))
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new OfferWrapper()
                , new TaskCompleteListener<OfferWrapper>() {
                    @Override
                    public void onSuccess(OfferWrapper offerWrapper) {

                        if (pageno == 1)
                            quickInfoDatas.clear();


                        if (offerWrapper.getStatus() == 1) {
                            isPagination = true;
                            setNewsData(offerWrapper.getData());

                        } else {
                            isPagination = false;
                            if (pageno == 1)
                               // ((MainActivity) getActivity()).messageAlert(offerWrapper.getMessage());
                            Toast.makeText(getActivity(),offerWrapper.getMessage(),Toast.LENGTH_LONG).show();
                        }

                        //pdialogue.dismiss();
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, OfferWrapper offerWrapper) {
                        isPagination = false;
                        if (pageno == 1)
                            quickInfoDatas.clear();
                        if (offerWrapper==null){
                            if (offerWrapper.getStatus() == 0) {
                                isPagination = true;
                                setNewsData(offerWrapper.getData());
                            }
                        }

                        //newsRecyclerView.setVisibility(View.GONE);
                       // txt_noinfo.setVisibility(View.VISIBLE);
                        //pdialogue.dismiss();
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        setPage();

                        newsAdapter.notifyDataSetChanged();
                    }
                });
        pdialogue.dismiss();
    }

    public void setPage() {
        pageno = 1;
    }

    public HashMap<String, String> getHashMapNews(int pageno) {
        String search = "";
        BaseFragment baseFragment = (BaseFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.placeholder);
        if (baseFragment instanceof TabFragment) {
            search = ((TabFragment) baseFragment).getSearchData();
        }
        HashMap<String, String> hashMapNews = new HashMap<>();
        hashMapNews.put("user_id", ((MainActivity) getActivity()).getData(getContext()).getData().getId());
        hashMapNews.put("page", pageno + "");
        hashMapNews.put("search", search);
        hashMapNews.put("type", "NF");
        hashMapNews.put("city_id", ((MainActivity) getActivity()).getTopCitySelectionId());
        return hashMapNews;
    }

    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (((MainActivity) getActivity()).getUserForWholeApp() != null) {
            callNewsApi();
        }

    }

    public void setNewsData(List<OfferData> offerDatas) {
        quickInfoDatas.addAll(offerDatas);
        if(quickInfoDatas.size()==0){
            relative_empty.setVisibility(View.VISIBLE);
        }
        else
            relative_empty.setVisibility(View.GONE);
        newsAdapter.notifyDataSetChanged();
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle("default", Enum.setNevigationIcon.MENU_CITY, false);
    }

    public void RefreshHomescreenOnSwipe() {
        try {
            callNewsApi();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}