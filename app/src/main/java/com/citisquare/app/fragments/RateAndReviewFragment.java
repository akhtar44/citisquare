package com.citisquare.app.fragments;

import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatRatingBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.Rendering;
import com.citisquare.app.pojo.response.Like;
import com.citisquare.app.pojo.response.RateAndReviewWrapper;
import com.citisquare.app.pojo.response.RateReviewCount;
import com.citisquare.app.pojo.response.RateUserDetail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import serviceCalling.builder.KeyValuePair;
import serviceCalling.helper.GetRequestHelper;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 31/1/17.
 */

public class RateAndReviewFragment extends BaseFragment {
    @BindView(R.id.ratingAverage)
    CustomTextView ratingAverage;
    @BindView(R.id.customTextViewAverageUsingRating)
    CustomTextView customTextViewAverageUsingRating;
    @BindView(R.id.topRatingView)
    RelativeLayout topRatingView;
    @BindView(R.id.fiveProgressBar)
    ProgressBar fiveProgressBar;
    @BindView(R.id.fiveCountTextView)
    CustomTextView fiveCountTextView;
    @BindView(R.id.fourProgressBar)
    ProgressBar fourProgressBar;
    @BindView(R.id.fourCountTextView)
    CustomTextView fourCountTextView;
    @BindView(R.id.threeProgressBar)
    ProgressBar threeProgressBar;
    @BindView(R.id.threeCountTextView)
    CustomTextView threeCountTextView;
    @BindView(R.id.twoProgressBar)
    ProgressBar twoProgressBar;
    @BindView(R.id.twoStarCountTextView)
    CustomTextView twoStarCountTextView;
    @BindView(R.id.oneProgressBar)
    ProgressBar oneProgressBar;
    @BindView(R.id.oneStarCountTextView)
    CustomTextView oneStarCountTextView;

    @BindView(R.id.averateRating)
    AppCompatRatingBar averateRating;
    RateUserDetail userDetail;
    @BindView(R.id.fiveRatingLinearLayout)
    LinearLayout fiveRatingLinearLayout;
    @BindView(R.id.fourRatingLinearLayout)
    LinearLayout fourRatingLinearLayout;
    @BindView(R.id.threeRatingLinearLayout)
    LinearLayout threeRatingLinearLayout;
    @BindView(R.id.twoRatingLinearLayout)
    LinearLayout twoRatingLinearLayout;
    @BindView(R.id.oneRatingLinearLayout)
    LinearLayout oneRatingLinearLayout;
    private int page;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.rate_review_frag, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        page = 1;
        rateAndReviewApiCall(page);
    }

    public void deletePreviewRateAndAddNew() {
        if (userDetail != null) {
            if (userDetail.getIsRating().equals("1")) {
                ((MainActivity) getActivity()).alertTwoAction(getString(R.string.write_rate_delete), new Rendering() {
                    @Override
                    public void response(boolean isCofirm) {
                        if (isCofirm)
                            deleteReview();
                    }
                });
            } else {
                ((MainActivity) getActivity()).setFragment(new WriteReviewFragment(), true, "");
            }
        }
    }

    private void deleteReview() {
        if (getActivity() != null) {
            ((MainActivity) getActivity()).startLoader();
        }

        List<KeyValuePair> keyValuePairs = new ArrayList<>();
        keyValuePairs.add(new KeyValuePair("user_id", ((MainActivity) getActivity()).getProfileUserId()));

        new GetRequestHelper<Like>().pingToRequest(Constants.DELETE_REVIEW,
                keyValuePairs,
                ((MainActivity) getActivity()).getParamWholeParameters(),
                new Like(),
                new TaskCompleteListener<Like>() {
                    @Override
                    public void onSuccess(Like like) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();
                        }

                        if (like.getStatus() == 1) {
                            ((MainActivity) getActivity()).setFragment(new WriteReviewFragment(), false, "");
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, Like like) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();
                        }

                        if (like != null) {
                            if (like.getStatus() == -1) {
                                ((MainActivity) getActivity()).invalidToken();
                            }
                        }
                    }
                });
    }


    public void callApi() {
        page = 1;
        rateAndReviewApiCall(page);
    }

    private void rateAndReviewApiCall(int pageNo) {
        if (getActivity() != null && pageNo == 1)
            ((MainActivity) getActivity()).startLoader();

        new PostRequestHelper<RateAndReviewWrapper>().pingToRequest(Constants.RATE_LISTING
                , null
                , null
                , getHashMapRateReview(pageNo + "")
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new RateAndReviewWrapper()
                , new TaskCompleteListener<RateAndReviewWrapper>() {
                    @Override
                    public void onSuccess(RateAndReviewWrapper rateAndReviewWrapper) {

                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        /*if (page == 1) {
                            rateReviewDatas.clear();
                        }*/

                        if (rateAndReviewWrapper.getStatus() == 1) {
                            setData(rateAndReviewWrapper);
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, RateAndReviewWrapper rateAndReviewWrapper) {

                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();
                    }
                });
    }


    private void setData(RateAndReviewWrapper rateAndReviewWrapper) {

        userDetail = rateAndReviewWrapper.getUserDetail();

        RateReviewCount rateReviewCount = rateAndReviewWrapper.getCount();
        setProgressAnimate(oneProgressBar, Integer.parseInt(rateReviewCount.getOneCount()), Integer.parseInt(rateReviewCount.getTotal()));
        setProgressAnimate(twoProgressBar, Integer.parseInt(rateReviewCount.getTwoCount()), Integer.parseInt(rateReviewCount.getTotal()));
        setProgressAnimate(threeProgressBar, Integer.parseInt(rateReviewCount.getThreeCount()), Integer.parseInt(rateReviewCount.getTotal()));
        setProgressAnimate(fourProgressBar, Integer.parseInt(rateReviewCount.getFourCount()), Integer.parseInt(rateReviewCount.getTotal()));
        setProgressAnimate(fiveProgressBar, Integer.parseInt(rateReviewCount.getFiveCount()), Integer.parseInt(rateReviewCount.getTotal()));

        if (userDetail.getRate()==null){
            averateRating.setRating((float) 0.0);
        }
        else
        averateRating.setRating(Float.parseFloat(userDetail.getRate()));
        ratingAverage.setText(userDetail.getRate());
        oneStarCountTextView.setText(rateReviewCount.getOneCount());
        twoStarCountTextView.setText(rateReviewCount.getTwoCount());
        threeCountTextView.setText(rateReviewCount.getThreeCount());
        fourCountTextView.setText(rateReviewCount.getFourCount());
        fiveCountTextView.setText(rateReviewCount.getFiveCount());
        // notifyDataSet(rateAndReviewWrapper.getData());
    }


    private void setProgressAnimate(ProgressBar pb, int rate, int total) {
        int progressTo = 0;
        if (rate > 0) {
            progressTo = getProgress(rate, total);
        }
        ObjectAnimator animation = ObjectAnimator.ofInt(pb, "progress", pb.getProgress(), progressTo);
        animation.setDuration(500);
        animation.setInterpolator(new DecelerateInterpolator());
        animation.start();
    }

    public int getProgress(int rating, int total) {
        int rate = 0;
        rate += (int) ((rating * 100) / total);
        return rate;
    }


    private HashMap<String, String> getHashMapRateReview(String pageNo) {
        HashMap<String, String> stringStringHashMap = new HashMap<>();
        stringStringHashMap.put("page", pageNo);
        stringStringHashMap.put("rate", "1");
        stringStringHashMap.put("user_id", ((MainActivity) getActivity()).getProfileUserId());
        return stringStringHashMap;
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle(getString(R.string.reviews), Enum.setNevigationIcon.BACK_REVIEW, true);
    }


    public void moveToRateAndReviewScreen(String rate) {
        if (getActivity() != null) {
            RateAndReviewListingFragment rateAndReviewListingFragment = new RateAndReviewListingFragment();
            Bundle bundleForRateAndReview = new Bundle();
            bundleForRateAndReview.putString("rate", rate);
            rateAndReviewListingFragment.setArguments(bundleForRateAndReview);
            ((MainActivity) getActivity()).setFragment(rateAndReviewListingFragment, true, "");
        }
    }

    @OnClick({R.id.fiveRatingLinearLayout, R.id.fourRatingLinearLayout, R.id.threeRatingLinearLayout, R.id.twoRatingLinearLayout, R.id.oneRatingLinearLayout})
    public void onClick(View view) {
        /*switch (view.getId()) {
            case R.id.fiveRatingLinearLayout:
                moveToRateAndReviewScreen("5");
                break;
            case R.id.fourRatingLinearLayout:
                moveToRateAndReviewScreen("4");
                break;
            case R.id.threeRatingLinearLayout:
                moveToRateAndReviewScreen("3");
                break;
            case R.id.twoRatingLinearLayout:
                moveToRateAndReviewScreen("2");
                break;
            case R.id.oneRatingLinearLayout:
                moveToRateAndReviewScreen("1");
                break;
        }*/
    }
}