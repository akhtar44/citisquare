package com.citisquare.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.payfort.fort.android.sdk.base.FortSdk;
import com.payfort.sdk.android.dependancies.models.FortRequest;
import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.BranchPlanAdapter;
import com.citisquare.app.async.JsonCallingRest;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.interfaces.PaymentCallBack;
import com.citisquare.app.interfaces.TaskComplateListener;
import com.citisquare.app.pojo.response.BranchPlanData;
import com.citisquare.app.pojo.response.BranchPlanWrapper;
import com.citisquare.app.pojo.response.FortSdkResponse;
import com.citisquare.app.pojo.response.LoginData;
import com.citisquare.app.pojo.response.LoginWrapper;
import com.citisquare.app.pojo.response.PaymentDataFailure;
import com.citisquare.app.utils.Debugger;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import serviceCalling.builder.KeyValuePair;
import serviceCalling.helper.GetRequestHelper;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

import static com.citisquare.app.interfaces.FortConstants.ACCESSCODE;
import static com.citisquare.app.interfaces.FortConstants.MERCHANT_IDENTIFIER;
import static com.citisquare.app.interfaces.FortConstants.REQUEST_PHASE;
import static com.citisquare.app.interfaces.FortConstants.TESTING_URL_ON_FAIL;

/**
 * Created by hlink56 on 30/1/17.
 */

public class BranchPlanFragment extends BaseFragment implements PaymentCallBack {


    @BindView(R.id.recyclerBranchPlan)
    RecyclerView recyclerBranchPlan;
    BranchPlanAdapter branchPlanAdapter;

    GridLayoutManager gridLayoutManager;
    List<BranchPlanData> branchPlanDatas;
    LoginData loginData;
    LoginWrapper loginWrapper;
    private String language = "";
    private String device_id = "";
    private String branchId = "";
    private String merchant_id = "";

    public static String encode(String base) {

        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.branch_list_frag, container, false);
        ButterKnife.bind(this, view);
        branchPlanDatas = new ArrayList<>();

        branchPlanAdapter = new BranchPlanAdapter(getContext(), branchPlanDatas, new ItemEventListener<String>() {
            @Override
            public void onItemEventFired(String s, @Nullable String t2, String actualPos) {
                branchId = t2;
                wsCallGetSdkToken(s);
            }
        });
        gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerBranchPlan.setLayoutManager(gridLayoutManager);
        recyclerBranchPlan.setAdapter(branchPlanAdapter);
        loginWrapper = ((MainActivity) getActivity()).getData(getContext());
        loginData = loginWrapper.getData();
        ((MainActivity) getActivity()).setPaymentCallBack(this);

        if (Locale.getDefault().getDisplayLanguage().equals("العربية"))
            language = "ar";
        else
            language = "en";

        device_id = FortSdk.getDeviceId(getContext());

        callApiOfPlanListing();
        return view;
    }

    private void wsCallGetSdkToken(final String amount) {
        if (getActivity() != null) {
            ((MainActivity) getActivity()).startLoader();
        }
        List<KeyValuePair> keyValuePairs = new ArrayList<>();
        keyValuePairs.add(new KeyValuePair("device_id", device_id));

        new GetRequestHelper<FortSdkResponse>().pingToRequest(Constants.GETSDK_TOKEN
                , keyValuePairs
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new FortSdkResponse()
                , new TaskCompleteListener<FortSdkResponse>() {
                    @Override
                    public void onSuccess(FortSdkResponse fortSdkResponse) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();

                            if (fortSdkResponse != null) {
                                if (!fortSdkResponse.getSdkToken().equalsIgnoreCase("")) {
                                    next(fortSdkResponse.getSdkToken(), amount);
                                } else {
                                    if (fortSdkResponse.getResponseCode() != null)
                                        ((MainActivity) getActivity()).messageAlert(fortSdkResponse.getResponseMessage());
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, FortSdkResponse subscriptionPlanWrapper) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();
                        }
                    }
                });
    }

    private void next(String sdkToken, String amount) {
        try {
            merchant_id = "" + System.currentTimeMillis();
            FortRequest fortRequest = new FortRequest();
            Double aDouble = Double.parseDouble(amount);
            aDouble = aDouble * 100;
            long d = (new Double(aDouble)).longValue();
            Map<String, String> hashMapString = new HashMap<String, String>();
            hashMapString.put("amount", String.valueOf(d));
            hashMapString.put("command", "PURCHASE");
            hashMapString.put("currency", "SAR");
            hashMapString.put("customer_email", loginData.getEmail());
            hashMapString.put("customer_name", loginData.getFirstName() + loginData.getLastName());
            hashMapString.put("language", language);
            hashMapString.put("merchant_reference", merchant_id);
            hashMapString.put("sdk_token", sdkToken);
            fortRequest.setShowResponsePage(true);
            ((MainActivity) getActivity()).payment(hashMapString);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    private void callApiOfPlanListing() {
        if (getActivity() != null) {
            ((MainActivity) getActivity()).startLoader();
        }

        new GetRequestHelper<BranchPlanWrapper>().pingToRequest(Constants.BRANCH_PLAN_LIST,
                new ArrayList<KeyValuePair>(),
                ((MainActivity) getActivity()).getHeader(),
                new BranchPlanWrapper(),
                new TaskCompleteListener<BranchPlanWrapper>() {
                    @Override
                    public void onSuccess(BranchPlanWrapper branchPlanWrapper) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();

                            if (branchPlanWrapper.getStatus() == 1) {
                                notifyBrnachPlanAdapter(branchPlanWrapper.getData());
                            }
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, BranchPlanWrapper branchPlanWrapper) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();


                            if (branchPlanWrapper != null) {
                                if (branchPlanWrapper.getStatus() == -1)
                                    ((MainActivity) getActivity()).invalidToken();
                            }
                        }
                    }
                });

    }

    private void notifyBrnachPlanAdapter(List<BranchPlanData> planDatas) {
        branchPlanDatas.clear();
        List<BranchPlanData> datasToSend = new ArrayList<>();
        for (BranchPlanData planData : planDatas) {
            if (Integer.parseInt(planData.getId()) > Integer.parseInt(((MainActivity) getActivity()).getData(getContext()).getData().getBranchId())) {
                datasToSend.add(planData);
            }
        }
        branchPlanDatas.addAll(datasToSend);
        branchPlanAdapter.notifyDataSetChanged();
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle(getString(R.string.branch_plan), Enum.setNevigationIcon.BACK, true);
    }

    @Override
    public void onSucess(Map<String, String> map, Map<String, String> map1) {
        branchSuccessPaymentCallApi(map1.get("fort_id"));
    }


    @Override
    public void onFailureTransaction(Map<String, String> map, Map<String, String> map1) {
        //  getFortIDForFailure();
    }

    @Override
    public void onCancel(Map<String, String> map, Map<String, String> map1) {

    }

    public void getFortIDForFailure() {
        String s = encode(REQUEST_PHASE + "access_code=" + ACCESSCODE + "language=" + language + "merchant_identifier=" + MERCHANT_IDENTIFIER + "merchant_reference=" + merchant_id + "query_command=CHECK_STATUS" + REQUEST_PHASE);
        JSONObject stringStringHashMap = new JSONObject();
        try {
            stringStringHashMap.put("query_command", "CHECK_STATUS");
            stringStringHashMap.put("access_code", ACCESSCODE);
            stringStringHashMap.put("merchant_identifier", MERCHANT_IDENTIFIER);
            stringStringHashMap.put("language", language);
            stringStringHashMap.put("merchant_reference", merchant_id);
            stringStringHashMap.put("signature", s);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (stringStringHashMap.length() > 0) {
            new JsonCallingRest(new TaskComplateListener<String>() {
                @Override
                public void onSuccess(String data) {
                    try {
                        PaymentDataFailure paymentDataFailure = new Gson().fromJson(data, PaymentDataFailure.class);
                        if (paymentDataFailure != null) {
                            if (paymentDataFailure.getResponseCode().equalsIgnoreCase("12000")) {
                                branchSuccessPaymentCallApi(paymentDataFailure.getFortId());
                            }
                            if (paymentDataFailure.getResponseCode().equalsIgnoreCase("11036")) {
                                branchSuccessPaymentCallApi(paymentDataFailure.getFortId());
                            }
                        }
                        Debugger.e("get data for fort_id======================" + data);
                    } catch (JsonSyntaxException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure() {

                }
            }).execute(String.valueOf(stringStringHashMap), TESTING_URL_ON_FAIL);
        }
    }

    private void branchSuccessPaymentCallApi(String fortId) {
        if (getActivity() != null)
            ((MainActivity) getActivity()).startLoader();


        new PostRequestHelper<LoginWrapper>().pingToRequest(Constants.UPDATE_BRANCH
                , null
                , null
                , paymentHashMap(fortId)
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new LoginWrapper()
                , new TaskCompleteListener<LoginWrapper>() {
                    @Override
                    public void onSuccess(LoginWrapper loginWrapper) {

                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();

                            if (loginWrapper.getStatus() == 1) {
                                ((MainActivity) getActivity()).setData(getContext(), new Gson().toJson(loginWrapper));
                                /*DataToPref.setData(getContext(), new Gson().toJson(loginWrapper));*/
                                moveToAddBranchFagment();
                            }
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, LoginWrapper loginWrapper) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        if (loginWrapper != null) {
                            if (loginWrapper.getStatus() == -1)
                                ((MainActivity) getActivity()).invalidToken();
                        }
                    }
                });
    }


    private void moveToAddBranchFagment() {

        ((MainActivity) getActivity()).moveToAddBranch("");

    }

    public HashMap<String, String> paymentHashMap(String fortId) {
        HashMap<String, String> paymentHashMap = new HashMap<>();
        paymentHashMap.put("branch_id", branchId);
        paymentHashMap.put("transaction_id", fortId);
        return paymentHashMap;
    }

}
