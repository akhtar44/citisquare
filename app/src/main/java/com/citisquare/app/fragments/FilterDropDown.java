package com.citisquare.app.fragments;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.FilterDropDownAdapter;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.pojo.Filter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 2/6/16.
 */
public class FilterDropDown {
    PopupWindow popupWindow;
    View view;
    boolean popUpVisible = false;
    int position;
    ItemEventListener<Integer> itemEventListener;

    TextView textView;
    Context context;
    ArrayList<Filter> categoryArrayList;
    FilterDropDownAdapter categoryDropDownAdapter;
    int height;
    int[] location = new int[2];
    @BindView(R.id.filterDropDownList)
    ListView filterDropDownList;


    public FilterDropDown(Context context, int height, int[] location, ArrayList<Filter> categoryArrayList, ItemEventListener<Integer> integerItemEventListener) {
        view = View.inflate(context, R.layout.filter_dropdown, null);
        ButterKnife.bind(this, view);
        itemEventListener = integerItemEventListener;
        this.context = context;
        filterDropDownList.setTextFilterEnabled(true);
        this.categoryArrayList = categoryArrayList;
        this.location = location;
        this.height = height;
        popupWindow = new PopupWindow(view, LinearLayout.LayoutParams.MATCH_PARENT, (int) context.getResources().getDimension(R.dimen.dp_180));
    }


    public void setAdapter() {
        filterDropDownList.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        categoryDropDownAdapter = new FilterDropDownAdapter(context, R.layout.raw_filter_dropdown, categoryArrayList);
        filterDropDownList.setAdapter(categoryDropDownAdapter);

        popupWindow.setContentView(view);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0));
        popupWindow.setFocusable(true);
        if (popupWindow.isFocusable()) {
            popUpVisible = false;
        }
        filterDropDownList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                itemEventListener.onItemEventFired(position, 0, "");
                ((MainActivity)context).setFilterPostion(position);
                dismiss();
            }
        });
    }

    public void setPosition(int pos) {
        position = pos;
    }

    public int getPosition() {
        return position;
    }

    public void show() {
        popUpVisible = true;
        setAdapter();

        popupWindow.showAtLocation(view, Gravity.TOP, location[0],
                location[1] + height + 10);
    }


    public void dismiss() {
        popUpVisible = false;
        popupWindow.dismiss();
    }

    public boolean isVisible() {
        return popUpVisible;
    }
}
