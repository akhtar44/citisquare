package com.citisquare.app.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import com.google.gson.Gson;
import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.pojo.response.CityInfoData;
import com.citisquare.app.utils.Debugger;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 28/9/16.
 */
public class WebViewCityInfoFragment extends BaseFragment {

    @BindView(R.id.layout_about_us_webview)
    WebView webView;
    @BindView(R.id.framelayoutPlaceholder)
    FrameLayout framelayoutPlaceholder;

    Bundle bundle;
    CityInfoData cityInfoData;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.about_layout, container, false);
        ButterKnife.bind(this, view);

        bundle = getArguments();
        if (bundle != null) {
            cityInfoData = new Gson().fromJson(bundle.getString("webview"), CityInfoData.class);

            if (cityInfoData != null) {
                Debugger.e("CITY DATA ?:::: " + cityInfoData.toString());
                String data;
                if (Locale.getDefault().getDisplayLanguage().equals("العربية")) {
                    data = cityInfoData.getArContent();
                } else {
                    data = cityInfoData.getContent();
                }
                String mime = "text/html";
                String encoding = "utf-8";
                WebSettings webSettings = webView.getSettings();
                webSettings.setJavaScriptEnabled(true);
                //webView.setWebViewClient(new myWebClient());
                webView.loadDataWithBaseURL(null, data, mime, encoding, null);
            }
        }

        return view;
    }

    @Override
    public void setHeaderTitle() {
        if (cityInfoData != null) {
            ((MainActivity) getActivity()).setTitle(cityInfoData.getTitle(), Enum.setNevigationIcon.BACK, true);
        }
    }

    public class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            String mime = "text/html";
            String encoding = "utf-8";

            view.loadDataWithBaseURL(null, url, mime, encoding, null);

            Debugger.e("url +++++++ " + url);
            return true;

        }
    }
}
