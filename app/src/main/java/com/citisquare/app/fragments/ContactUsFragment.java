package com.citisquare.app.fragments;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.ContactUsAdapter;
import com.citisquare.app.controls.CustomButton;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.pojo.ContactUsData;
import com.citisquare.app.utils.Debugger;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 27/12/16.
 */

public class ContactUsFragment extends BaseFragment {

    private final int REQUEST_CALL_PERMISSION = 88;
    @BindView(R.id.recyclerViewContactUs)
    RecyclerView recyclerViewContactUs;
    @BindView(R.id.sendQuery)
    CustomButton sendQuery;
    List<ContactUsData> contactUsDatas;
    GridLayoutManager gridLayoutManager;
    ContactUsAdapter contactUsAdapter;
    String phoneNumber = "";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_contact_us, container, false);
        ButterKnife.bind(this, view);

        contactUsDatas = new ArrayList<>();
        sendQuery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFragment(new SendQueryFragment(), false, "");
            }
        });

        return view;
    }
    public void setFragment(BaseFragment baseFragment, boolean flag, String tag) {
        BaseFragment fragment = (BaseFragment) getActivity().getSupportFragmentManager().
                findFragmentById(R.id.placeholder);
        if (fragment instanceof TabFragment)
            ((TabFragment) fragment).setFragment(baseFragment, flag, tag);
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle(getString(R.string.contact_us), Enum.setNevigationIcon.BACK, false);
    }

    public void showCallMessage(String number) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                checkForCallPermission(number);
            else
                call(number);
        } catch (Exception e) {
            e.printStackTrace();
            Debugger.e(":::::::ERROR WHILE DIALING NUMBER:::::");
        }
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        contactUsDatas.add(new ContactUsData("Saudi Geophysical", "Al Khobar-Karawan Tower. Al Khobar- 31952 Post Box: 1587", "08:00 Hours To 17:00 Hours", "+966 13 8879090", "00966 13 8879090", "Shahid@ citisquare.com", "www.citisquare.com"));
        contactUsDatas.add(new ContactUsData("Rafa Gulf Contracting", "Karwaan Tower A-Second Floor-King Faisal Road - Al Khobar 31952 Post Box 1587", "08:00 Hours To 17:00 Hours", "+966 13 8879090", "00966 13 8879090", "Khalid@citisquare.com", "www.citisquare.com"));
        /*contactUsDatas.add(new ContactUsData("RMS Advertising Agency", "", "08:00 AM  To 05:00 PM", "00966138995594", "00966138995594", "info@rmsadv.com.sa", "www.rmsadv.com.sa"));//Al Khobar-Prince Bander Street. Suwekat Cross 15
        contactUsDatas.add(new ContactUsData("Compu Band High Technology Service", "", "08:00 To 12: 30 PM  & 04:00 PM To 09:00 PM", "8996635", "8971568", "goldeneye@hotmail.com", "www.compuband.net"));//King Abudllah Bin Abdul Aziz Road (Dahran Road) Dossary Towers Mall Ground Floor Show Room No 102
        contactUsDatas.add(new ContactUsData("Advance Systems & Technology Co. Ltd (atm)", "", "8:00 AM To 1:00 PM & 4:00 Pm To 7:00 Pm", " 00966 13-8290260", "00966 13-8290260", "", "www.AST-IT.com"));//SHC Building 1st Floor, Behind Iskan Park-Post Box 9303 Dammam- 31413
        contactUsDatas.add(new ContactUsData("Al Kuhaimi Metal Industries ltd", "", "", " +966-13-8472777", "+966-13-8472591", "info@alkuhaimi.com", "www.alkuhaimi.com"));//P.O. Box# 545, Dammam 31421, Saudi Arabia.
*/
        contactUsAdapter = new ContactUsAdapter(getContext(), contactUsDatas, new ItemEventListener<String>() {
            @Override
            public void onItemEventFired(String s, @Nullable String t2, String actualPos) {
                switch (s) {
                    case "phone":
                        phoneNumber = t2;
                        showCallMessage(t2);
                        break;
                }
            }
        });

        gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerViewContactUs.setLayoutManager(gridLayoutManager);
        recyclerViewContactUs.setAdapter(contactUsAdapter);

    }

    public void checkForCallPermission(String number) {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL_PERMISSION);
            //call(number);
        } else {
            try {
                call(number);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void call(final String phone) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle("" + getString(R.string.app_name));
        alertDialogBuilder
                .setMessage("" + Html.fromHtml(phone))
                .setPositiveButton("CALL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Intent.ACTION_CALL);
                        intent.setData(Uri.parse("tel:" + phone));
                        startActivity(intent);
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Debugger.e("HOME:::::::::ONPERMISSIONRESULT CALLED");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CALL_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    call(phoneNumber);
                    return;
                } else {
                    //  getActivity().finish();
                }
                break;
        }
    }
}
