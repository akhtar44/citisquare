package com.citisquare.app.fragments;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.CategoryDropDownAdapter;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.pojo.response.CategoryData;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 1/6/16.
 */
public class CategoryDropDown {

    PopupWindow popupWindow;
    View view;
    boolean popUpVisible = false;
    int position;
    ItemEventListener<Integer> itemEventListener;
    @BindView(R.id.categoryListview)
    ListView categoryListview;
    Context context;
    ArrayList<CategoryData> categoryArrayList;
    CategoryDropDownAdapter categoryDropDownAdapter;
    int height;
    int[] location = new int[2];


    public CategoryDropDown(Context context, int height, int[] location, ArrayList<CategoryData> categoryArrayList, ItemEventListener<Integer> integerItemEventListener) {
        view = View.inflate(context, R.layout.categories_dropdown, null);
        ButterKnife.bind(this, view);
        itemEventListener = integerItemEventListener;
        this.context = context;
        categoryListview.setTextFilterEnabled(true);
        this.categoryArrayList = categoryArrayList;
        this.location = location;
        this.height = height;
        popupWindow = new PopupWindow(view, LinearLayout.LayoutParams.MATCH_PARENT,(int) context.getResources().getDimension(R.dimen.dp_180));
    }


    public void setAdapter() {
        categoryListview.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        categoryDropDownAdapter = new CategoryDropDownAdapter(context, R.layout.raw_category_dropdown, categoryArrayList, false);
        categoryListview.setAdapter(categoryDropDownAdapter);

        popupWindow.setContentView(view);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0));
        popupWindow.setFocusable(true);
        if (popupWindow.isFocusable()) {
            popUpVisible = false;
        }
        categoryListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                itemEventListener.onItemEventFired(position, 0, "");
                ((MainActivity) context).setPositionCategory(position);
                dismiss();
            }
        });
    }

    public void setPosition(int pos) {
        position = pos;
    }

    public int getPosition() {
        return position;
    }

    public void show() {
        popUpVisible = true;
        setAdapter();

        popupWindow.showAtLocation(view, Gravity.TOP, location[0],
                location[1] + height + 10);
    }


    public void dismiss() {
        popUpVisible = false;
        popupWindow.dismiss();
    }

    public boolean isVisible() {
        return popUpVisible;
    }
}