package com.citisquare.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.InboxAdapter;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.RenderingString;
import com.citisquare.app.pojo.response.InboxData;
import com.citisquare.app.pojo.response.InboxWrapper;

import java.util.ArrayList;
import java.util.List;

import serviceCalling.builder.KeyValuePair;
import serviceCalling.helper.GetRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 31/5/16.
 */
public class InboxFragment extends BaseFragment {
    RecyclerView recyclerView;
    List<InboxData> inboxList;
    GridLayoutManager gridLayoutManager;
    InboxAdapter inboxAdapter;
    int pageno = 1;

    boolean isPagination;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.inbox_layout, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.inboxRecyclerView);
        pageno = 1;
        inboxList = new ArrayList<>();

        inboxAdapter = new InboxAdapter(getContext(), inboxList, new RenderingString() {
            @Override
            public void renderString(String data) {

            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (isPagination) {
                    int visibleItemCount = recyclerView.getChildCount();
                    int totalItemCount = gridLayoutManager.getItemCount();
                    int firstVisibleItemPosition = gridLayoutManager.findFirstVisibleItemPosition();
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount) {
                        isPagination = false;
                        memberList(++pageno);
                    }
                }
            }
        });

        //memberList(pageno);
        return view;
    }

    public void chatMessage(String message, String chatId) {
        for (InboxData inboxData : inboxList) {
            if (inboxData.getChatId().equals(chatId)) {
                int count = Integer.parseInt(inboxData.getCount());
                inboxData.setCount(++count + "");
            }
        }
        if (inboxAdapter != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    inboxAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    public void memberList(final int pageno) {
        if (getActivity() != null && pageno == 1)
            ((MainActivity) getActivity()).startLoader();

        List<KeyValuePair> keyvaluePairs = new ArrayList<>();
        keyvaluePairs.add(new KeyValuePair("page", pageno + ""));

        new GetRequestHelper<InboxWrapper>().pingToRequest(Constants.MESSAGE_LIST
                , keyvaluePairs
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new InboxWrapper()
                , new TaskCompleteListener<InboxWrapper>() {
                    @Override
                    public void onSuccess(InboxWrapper inboxWrapper) {
                        isPagination = true;
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        if (pageno == 1)
                            inboxList.clear();

                        if (inboxWrapper.getStatus() == 1)
                            setInboxAdapater(inboxWrapper.getData());
                        else {
                            if (pageno == 1)
                                ((MainActivity) getActivity()).messageAlert(inboxWrapper.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, InboxWrapper inboxWrapper) {
                        isPagination = false;
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        if (inboxWrapper != null) {
                            if (exceptions == ExceptionType.ParameterException) {
                                if (pageno == 1)
                                    ((MainActivity) getActivity()).messageAlert(inboxWrapper.getMessage());
                            }
                        }
                       /* if (inboxWrapper.getStatus() == -1) {
                            ((MainActivity) getActivity()).invalidToken();
                        }*/
                    }
                });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        pageno = 1;
        memberList(pageno);
    }

    @Override
    public void onResume() {
        super.onResume();
        pageno = 1;
        inboxAdapter = new InboxAdapter(getContext(), inboxList, new RenderingString() {
            @Override
            public void renderString(String data) {
                int i = Integer.parseInt(data);
                ChatFragment chatFragment = new ChatFragment();
                Bundle bundle = new Bundle();
                bundle.putString("chatLayout", "chatLayout");
                bundle.putSerializable("chat", inboxList.get(i));
                chatFragment.setArguments(bundle);
                ((MainActivity) getActivity()).setFragment(chatFragment, true, "");
            }
        });
        /*memberList(pageno);*/
        gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(inboxAdapter);

    }


    public void setInboxAdapater(List<InboxData> inboxAdapater) {
        inboxList.addAll(inboxAdapater);
        inboxAdapter.notifyDataSetChanged();
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle(getString(R.string.message), Enum.setNevigationIcon.MENU_COMPOSE, false);
    }
}
