package com.citisquare.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.controls.CustomButton;
import com.citisquare.app.controls.CustomEditText;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.pojo.response.Like;
import com.citisquare.app.pojo.response.LoginData;
import com.citisquare.app.pojo.response.LoginWrapper;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import serviceCalling.builder.KeyValuePair;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 31/1/17.
 */

public class WriteReviewFragment extends BaseFragment {

    @BindView(R.id.writeReviewImage)
    ImageView writeReviewImage;
    @BindView(R.id.editTextTitle)
    CustomEditText editTextTitle;
    @BindView(R.id.editTextWriteReview)
    CustomEditText editTextWriteReview;
    @BindView(R.id.submitRateAndReview)
    CustomButton submitRateAndReview;
    @BindView(R.id.writeReviewUploadUserName)
    CustomTextView writeReviewUploadUserName;
    @BindView(R.id.reviewRating)
    RatingBar reviewRating;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.write_review_frag, container, false);
        ButterKnife.bind(this, view);

        LoginWrapper loginWrapper = ((MainActivity) getActivity()).getData(getContext());
        LoginData loginData = loginWrapper.getData();

        if (loginData != null) {
            ((MainActivity) getActivity()).setCircularImage(writeReviewImage, loginData.getProfileImageThumb());
            String name = loginData.getFirstName() + " " + loginData.getLastName();
            writeReviewUploadUserName.setText(name);
        }

        reviewRating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if (rating < 1.0)
                    ratingBar.setRating(1.0f);
            }
        });

        editTextWriteReview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.editTextWriteReview) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        return view;
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle(getString(R.string.write_review), Enum.setNevigationIcon.BACK, true);
    }

    public boolean isValid() {
        if (editTextTitle.getText().toString().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterTitle));
            return false;
        }

        return true;
    }

    @OnClick(R.id.submitRateAndReview)
    public void onClick() {
        if (isValid()) {
            callWriteAndReviewApiCall();
        }
    }

    private void callWriteAndReviewApiCall() {
        if (getActivity() != null) {
            ((MainActivity) getActivity()).startLoader();
        }

        new PostRequestHelper<Like>().pingToRequest(Constants.RATE_REVIEW
                , null
                , new ArrayList<KeyValuePair>()
                , getHashMapForRating()
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new Like()
                , new TaskCompleteListener<Like>() {
                    @Override
                    public void onSuccess(Like like) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();
                        }
                        if (like.getStatus() == 1) {
                            ((MainActivity) getActivity()).messageAlert(like.getMessage(), "");
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, Like like) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();
                        }
                        if(like!=null){
                            if(like.getStatus()==-1){
                                ((MainActivity)getActivity()).invalidToken();
                            }
                        }
                    }
                });
    }


    public HashMap<String, String> getHashMapForRating() {
        HashMap<String, String> hashMapForRating = new HashMap<>();
        hashMapForRating.put("title", editTextTitle.getText().toString().trim());
        hashMapForRating.put("rate", reviewRating.getRating() + "");
        hashMapForRating.put("rated_user_id", ((MainActivity) getActivity()).getProfileUserId());
        hashMapForRating.put("review", editTextWriteReview.getText().toString().trim());

        return hashMapForRating;
    }
}

