package com.citisquare.app.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.SearchAdapter;
import com.citisquare.app.controls.MapWrapperLayout;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.LocationCallback;
import com.citisquare.app.listener.OnInfoWindowElemTouchListener;
import com.citisquare.app.pojo.response.ParentMapData;
import com.citisquare.app.pojo.response.ParentMapWrapper;
import com.citisquare.app.pojo.response.SearchData;
import com.citisquare.app.utils.CircleTransformation;
import com.citisquare.app.utils.Debugger;
import com.citisquare.app.utils.LocationHelper;
import com.citisquare.app.utils.RequestParameter;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink16 on 27/5/16.
 */
public class ParentMapFragment extends BaseFragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    public OnInfoWindowElemTouchListener infoButtonListenerMail;
    public OnInfoWindowElemTouchListener infoButtonLocation;
    @BindView(R.id.map)
    MapView mapView;
    @BindView(R.id.framelayoutPlaceholder)
    FrameLayout framelayoutPlaceholder;
    @BindView(R.id.mapWrapperLayout)
    MapWrapperLayout mapWrapperLayout;
    ParentMapWrapper parentMapWrapper;
    HashMap<LatLng, String> latLngStringHashMap;
    boolean not_first_time_showing_info_window = false;
    private GoogleMap googleMap;
    private LocationCallback locationCallback;
    private LocationHelper locationHelper;
    ProgressDialog pdialogue;
    SearchAdapter searchAdapter;
    List<SearchData> searchDatas;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;
        try {
            MapsInitializer.initialize(getActivity());
            view = inflater.inflate(R.layout.map_layout, container, false);
            ButterKnife.bind(this, view);
            mapView.onCreate(savedInstanceState);
            latLngStringHashMap = new HashMap<>();
            BaseFragment baseFragment = (BaseFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.placeholder);
            if (baseFragment instanceof TabFragment) {
                ((TabFragment) baseFragment).setSearchVisibility(Enum.setSearch.ONLYSEARCHHIDE);
            }
            if (mapView != null) {
                mapView.getMapAsync(this);
            }
            mapWrapperLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) getActivity()).hideKeyboard();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        //markerList = new ArrayList<>();
        ButterKnife.bind(this, view);

        return view;
    }


    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle("default", Enum.setNevigationIcon.MENU_CITY, false);
    }

    public void startLocation() {
        startlocationlisting(new LocationCallback() {
            @Override
            public void onLocationError() {

            }

            @Override
            public void onLocationRecive(Location location) {
                if (getActivity() != null) {
                    ((MainActivity) getActivity()).setLatLng(new LatLng(location.getLatitude(), location.getLongitude()));
                    try {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                parentMapApiCall();
                            }
                        }, 200);

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    LoadDefaultMap();

                }
            }
        });
    }

    public void LoadDefaultMap() {
        LatLng location;
        if (googleMap != null)
            googleMap.clear();

        if (getActivity() != null)
            location = ((MainActivity) getActivity()).getLatLng();
        else
            location = new LatLng(0, 0);

        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                location, 9));
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(location)      // Sets the center of the map to location user
                .zoom(15)// Sets the zoom
                .build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        drawMarker(location, "CURRENT", "", R.drawable.current_location);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        if (googleMap != null) {
            this.googleMap = googleMap;
            setUpMap();
        }
    }

    public void parentMapApiCall() {

        new PostRequestHelper<ParentMapWrapper>().pingToRequest(Constants.MAP_SELLER_DETAIL
                , null
                , null
                , new RequestParameter().getHashMapForMapSellerDetails(getHashMapForSellerDetails())
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new ParentMapWrapper()
                , new TaskCompleteListener<ParentMapWrapper>() {
                    @Override
                    public void onSuccess(ParentMapWrapper parentMapWrapper) {
                        if (getActivity() != null) {
                            if (parentMapWrapper.getStatus() == 1) {
                                BaseFragment baseFragment = (BaseFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.placeholder);
                                if (baseFragment instanceof TabFragment) {
                                    ((TabFragment) baseFragment).setVisibleHurryUp(false);
                                }
                                loadMapData(parentMapWrapper);
                            } else {
                                //((MainActivity) getActivity()).messageAlert(parentMapWrapper.getMessage());
                                Toast.makeText(getActivity(),parentMapWrapper.getMessage(),Toast.LENGTH_LONG).show();
                                loadMapData(parentMapWrapper);
                            }
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, ParentMapWrapper parentMapWrapper) {
                        if (getActivity() != null && parentMapWrapper != null)
                            //((MainActivity) getActivity()).messageAlert(parentMapWrapper.getMessage());
                        Toast.makeText(getActivity(),parentMapWrapper.getMessage(),Toast.LENGTH_LONG).show();
                        loadMapData(parentMapWrapper);
                    }
                });
    }

    public void loadMapData(ParentMapWrapper parentMapWrapper) {
        //this.parentMapWrapper = parentMapWrapper;
        if (parentMapWrapper != null) {
            this.parentMapWrapper = parentMapWrapper;
            /*LatLng location;
            if (googleMap != null)
                googleMap.clear();

            if (getActivity() != null)
                location = ((MainActivity) getActivity()).getLatLng();
            else
                location = new LatLng(0, 0);

            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    location, 9));
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(location)      // Sets the center of the map to location user
                    .zoom(9)// Sets the zoom
                    .build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            drawMarker(location, "CURRENT", "", R.drawable.current_location);*/
            if (parentMapWrapper.getData() != null) {
                List<ParentMapData> parentMapData = parentMapWrapper.getData();
                latLngStringHashMap.clear();
                for (int i = 0; i < parentMapData.size(); i++) {
                    if (!(parentMapData.get(i).getLatitude().equals("") && parentMapData.get(i).getLongitude().equals(""))) {
                        LatLng latLng = new LatLng(Double.parseDouble(parentMapData.get(i).getLatitude()), Double.parseDouble(parentMapData.get(i).getLongitude()));
                       // drawMarker(latLng, "", "", R.drawable.job).setVisible(true);
                        if (parentMapData.get(i).getIsVisible().equalsIgnoreCase("0")) {
                            drawMarker(latLng, "", "", R.drawable.job).setVisible(false);
                        } else
                            drawMarker(latLng, "", "", R.drawable.job).setVisible(true);
                        latLngStringHashMap.put(latLng, new Gson().toJson(parentMapData.get(i)));
                    }
                }
            }
        } else {
            if (googleMap != null)
                googleMap.clear();
        }
    }

    /*city_id,latitude,longitude	search,cat_id,flag,job_type*/

    private HashMap<String, String> getHashMapForSellerDetails() {
        HashMap<String, String> hashMapSellerDetails = new HashMap<>();
        String jobType = "";
        String search = "";
        String flag = "";
        String catID = "";
        hashMapSellerDetails.put("city_id", ((MainActivity) getActivity()).getTopCitySelectionId());
        if (getActivity() != null) {
            hashMapSellerDetails.put("latitude", String.valueOf(((MainActivity) getActivity()).getLatLng().latitude));
            hashMapSellerDetails.put("longitude", String.valueOf(((MainActivity) getActivity()).getLatLng().longitude));
        } else {
            hashMapSellerDetails.put("latitude", "0");
            hashMapSellerDetails.put("longitude", "0");
        }


        BaseFragment baseFragment = (BaseFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.placeholder);
        if (baseFragment != null) {
            if (baseFragment instanceof TabFragment) {
                search = ((TabFragment) baseFragment).getSearchData();
                catID = ((TabFragment) baseFragment).getCategoryId();

                if (((TabFragment) baseFragment).getFilterData().equals(getString(R.string.saudis_jobs_drop))) {
                    jobType = "S";
                    flag = "";
                } else if (((TabFragment) baseFragment).getFilterData().equals(getString(R.string.non_saudis_jobs_drop))) {
                    jobType = "NS";
                    flag = "";
                } else if (((TabFragment) baseFragment).getFilterData().equals(getString(R.string.nearby))) {
                    flag = "nearby";
                    jobType = "";
                } else if (((TabFragment) baseFragment).getFilterData().equals(getString(R.string.delivery))) {
                    flag = "Delivery";
                    jobType = "";
                } else if (((TabFragment) baseFragment).getFilterData().equals(getString(R.string.followersDrop))) {
                    flag = "followers";
                    jobType = "";
                } else if (((TabFragment) baseFragment).getFilterData().equals(getString(R.string.followingDrop))) {
                    flag = "following";
                    jobType = "";
                } else if (((TabFragment) baseFragment).getFilterData().equals(getString(R.string.qiverifieddrop))) {
                    flag = "verified";
                    jobType = "";
                } else if (((TabFragment) baseFragment).getFilterData().equals(getString(R.string.ratings))) {
                    flag = "rating";
                    jobType = "";
                }
            }
        }

        hashMapSellerDetails.put("cat_id", catID);
        hashMapSellerDetails.put("search", search);
        hashMapSellerDetails.put("flag", flag);
        hashMapSellerDetails.put("job_type", jobType);
        return hashMapSellerDetails;
    }


    public Marker drawMarker(LatLng place, String title, String content, int icon) {
        MarkerOptions markerOptions = new MarkerOptions()
                .position(place)
                .infoWindowAnchor(0.5f, -0.0f)
                .title(title)
                .snippet(content)
                .icon(BitmapDescriptorFactory.fromResource(icon));

        Marker mMarker = googleMap.addMarker(markerOptions);


        return mMarker;
    }


    public void setUpMap() {
        /*if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Debugger.e("setUpMap method: permission require................");
            return;
        }*/
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.getUiSettings().setAllGesturesEnabled(true);
        googleMap.setOnMarkerClickListener(this);

        //googleMap.setInfoWindowAdapter(new InfoWindowAdapter(getContext()));
        googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(final Marker marker) {
                View view = LayoutInflater.from(getContext()).inflate(R.layout.map_tip_popup, null);
                final ViewHolder holder = new ViewHolder(view);
                String userId = "";
                //holder.jobPost.setText(parentMapWrapper.getData().get(marker.getPosition()).getFirstName());
                if (latLngStringHashMap != null) {
                    final ParentMapData parentMapData = new Gson().fromJson(latLngStringHashMap.get(marker.getPosition()), ParentMapData.class);
                    if (parentMapData != null) {

                        userId = parentMapData.getId();
                        holder.jobPost.setText(parentMapData.getCompanyName());

                        Picasso.with(getContext()).load(parentMapData.getProfileImageThumb())
                                .resize(50, 50)
                                .transform(new CircleTransformation())
                                .into(holder.jobImageView, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        getInfoContents(marker);
                                    }

                                    @Override
                                    public void onError() {

                                    }
                                });
                        holder.locationTab.setText(parentMapData.getCityName());
                    }
                }

                mapWrapperLayout.setMarkerWithInfoWindow(marker, view);
                if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) ==
                        Configuration.SCREENLAYOUT_SIZE_XLARGE) {
                    // on a x-large screen device ...
                    Debugger.e("extra large screen");
                    mapWrapperLayout.init(googleMap, getPixelsFromDp(getActivity(), 35 + 50));
                } else if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) ==
                        Configuration.SCREENLAYOUT_SIZE_LARGE) {
                    // on a large screen device ...
                    Debugger.e("large screen");
                    mapWrapperLayout.init(googleMap, getPixelsFromDp(getActivity(), 35 + 50));
                } else {
                    Debugger.e("normal screen");
                    mapWrapperLayout.init(googleMap, getPixelsFromDp(getActivity(), 35 + 20));
                }

                final String finalUserId = userId;
                ParentMapFragment.this.infoButtonListenerMail = new OnInfoWindowElemTouchListener(holder.tipViewDetails, 0, 0) {
                    @Override
                    protected void onClickConfirmed(View v, Marker m) {
                        ProfileFragment profileFragment = new ProfileFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("profile", "other");
                        ((MainActivity) getActivity()).setProfileUserId(finalUserId);
                        profileFragment.setArguments(bundle);
                        ((MainActivity) getActivity()).setSelectionPostion(0);
                        ((MainActivity) getActivity()).setFragment(profileFragment, false, "");
                    }
                };
                ParentMapFragment.this.infoButtonLocation = new OnInfoWindowElemTouchListener(holder.locationTab, 0, 0) {
                    @Override
                    protected void onClickConfirmed(View v, Marker m) {
                        ((MainActivity) getActivity()).setFragment(new OtherMapPathFragment(), false, "");
                    }
                };
                holder.tipViewDetails.setOnTouchListener(infoButtonListenerMail);
                holder.locationTab.setOnTouchListener(infoButtonLocation);
                return view;
            }

            @Override
            public View getInfoContents(Marker marker) {
                if (marker != null &&
                        marker.isInfoWindowShown()) {
                    marker.hideInfoWindow();
                    marker.showInfoWindow();
                }
                return null;
            }
        });
    }


    @Override
    public void onResume() {
        if (getActivity() != null) {
            BaseFragment fragement = (BaseFragment) getActivity().getSupportFragmentManager().
                    findFragmentById(R.id.placeholder);
            if (fragement != null) {
                if (fragement instanceof TabFragment) {
                    int i = ((TabFragment) fragement).pager.getCurrentItem();
                    if (i == 3) {
                        startlocationlisting(new LocationCallback() {
                            @Override
                            public void onLocationError() {

                            }

                            @Override
                            public void onLocationRecive(Location location) {
                                if (getActivity() != null) {
                                    ((MainActivity) getActivity()).setLatLng(new LatLng(location.getLatitude(), location.getLongitude()));
                                    try {
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                parentMapApiCall();
                                            }
                                        }, 200);

                                    } catch (Exception ex) {
                                        ex.printStackTrace();
                                    }
                                    LoadDefaultMap();
                                }
                            }
                        });
                    }
                }
            }
        }
        if (mapView != null) {
            mapView.onResume();

        }

        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mapView != null)
            mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        if (mapView != null)
            mapView.onLowMemory();
        super.onLowMemory();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        // googleMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(marker.getPosition().latitude + (double) 90 / Math.pow(2, 9), marker.getPosition().longitude)));
        //new LatLng(arg0.getPosition().latitude + (double)90/Math.pow(2, zoom), arg0.getPosition().longitude), zoom
        if (marker.getTitle().equals("CURRENT")) {

        } else {
            marker.showInfoWindow();
        }
        return true;
    }

    public int getPixelsFromDp(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    public void startlocationlisting(LocationCallback locationCallback) {
        this.locationCallback = locationCallback;
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    ) {
                requestPermissions(Constants.ACCESS_FINE_LOCATION, Constants.LOCATIONREQUESTCODE);
            } else {
                startLisning();
            }

        } else startLisning();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Debugger.e("PERMISSION ::" + requestCode);
        switch (requestCode) {
            case Constants.LOCATIONREQUESTCODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLisning();
                } else {

                }
                return;
            }
        }
    }

    private void startLisning() {
        locationHelper = new LocationHelper(this, new LocationHelper.ProvideLocation() {
            @Override
            public void currentLocation(final Location location) {
                locationCallback.onLocationRecive(location);
            }

            @Override
            public void onError() {
                locationCallback.onLocationError();
            }
        });
    }

    public void stoplocationlsting() {
        if (locationHelper != null) {
            locationHelper.stopLocationUpdates();
        }
    }

    public void loadMapselection(Location latlong) {

    }

    public class ViewHolder {
        @BindView(R.id.jobImageView)
        ImageView jobImageView;
        @BindView(R.id.jobPost)
        TextView jobPost;
        @BindView(R.id.locationTab)
        TextView locationTab;


        @BindView(R.id.tipViewDetails)
        TextView tipViewDetails;

        public ViewHolder(View v) {
            ButterKnife.bind(this, v);

        }
    }

    private class InfoWindowRefresher implements Callback {
        private Marker markerToRefresh;

        private InfoWindowRefresher(Marker markerToRefresh) {
            this.markerToRefresh = markerToRefresh;
        }

        @Override
        public void onSuccess() {
            markerToRefresh.showInfoWindow();
        }

        @Override
        public void onError() {
        }
    }
}