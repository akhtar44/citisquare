package com.citisquare.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.ChatAdapter;
import com.citisquare.app.controls.CustomEditText;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.dialog.ImageChooserDialog;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ImageChooserListener;
import com.citisquare.app.pojo.response.ChatData;
import com.citisquare.app.pojo.response.ChatWrapper;
import com.citisquare.app.pojo.response.InboxData;
import com.citisquare.app.pojo.response.LoginWrapper;
import com.citisquare.app.pojo.response.ProfileData;
import com.citisquare.app.pojo.response.PushNotification;
import com.citisquare.app.pojo.response.SendChatWrapper;
import com.citisquare.app.utils.Debugger;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import serviceCalling.builder.KeyValuePair;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 26/7/16.
 */
public class ChatFragment extends BaseFragment {

    private static boolean isToggle = false;
    public ChatWrapper chatWrapper;
    @BindView(R.id.recyclerViewChat)
    RecyclerView layoutChatRecyclerView;
    @BindView(R.id.chatMessage)
    CustomEditText chatMessage;
    @BindView(R.id.sendChat)
    CustomTextView sendChat;
    ChatAdapter chatAdapter;
    List<ChatData> chatDatas;
    Bundle bundle;
    int pageNo = 0;
    InboxData inboxData;
    String bundleString = "";
    boolean isPagination;
    int totalPage;
    GridLayoutManager gridLayoutManager;
    ProfileData profileData;
    @BindView(R.id.backGroundChatImage)
    ImageView backGroundChatImage;
    @BindView(R.id.uploadChatBackgroundImage)
    ImageView uploadChatBackgroundImage;


    private Pattern pattern;
    private Matcher matcher;
    private File image;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_chat, container, false);
        ButterKnife.bind(this, view);
        chatDatas = new ArrayList<>();


        pattern = Pattern.compile(Constants.englishOnly);
        /*int maxHeight = getResources().getDisplayMetrics().heightPixels;
        maxHeight -= sendChat.getHeight();
        backGroundChatImage.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, maxHeight));*/
        gridLayoutManager = new GridLayoutManager(getActivity(), 1);

        pageNo = 0;
        bundle = getArguments();
        if (bundle != null) {
            if (bundle.getString("chatLayout", "").equals("chatLayout")) {
                inboxData = (InboxData) bundle.getSerializable("chat");
                bundleString = "chatLayout";
                Debugger.e("INBOX LAYOUT");
                chatHistory(pageNo);
            } else if (bundle.getSerializable("otherProfile") != null) {
                profileData = (ProfileData) bundle.getSerializable("otherProfile");
                bundleString = "profile";
                Debugger.e("OTHER PROFILE");
                chatHistory(pageNo);
            }
            if (bundleString.equals("chatLayout")) {
                ((MainActivity) getActivity()).setCircularProfileImage(inboxData.getProfileImageThumb());
                ((MainActivity) getActivity()).setTitle(inboxData.getFirstName() + " " + inboxData.getLastName(), Enum.setNevigationIcon.BACK_PROFILE, true);
            } else if (bundleString.equals("profile")) {
                ((MainActivity) getActivity()).setCircularProfileImage(profileData.getProfileImageThumb());
                ((MainActivity) getActivity()).setTitle(profileData.getFirstName() + " " + profileData.getLastName(), Enum.setNevigationIcon.BACK_PROFILE, true);
            }
        }

        chatAdapter = new ChatAdapter(getActivity(), chatDatas);
        gridLayoutManager.setReverseLayout(true);
        layoutChatRecyclerView.setLayoutManager(gridLayoutManager);
        layoutChatRecyclerView.setAdapter(chatAdapter);
        layoutChatRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (isPagination) {
                    if (pageNo < totalPage) {
                        int visibleItemCount = recyclerView.getChildCount();
                        int totalItemCount = gridLayoutManager.getItemCount();
                        int firstVisibleItemPosition = gridLayoutManager.findFirstVisibleItemPosition();
                        if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount) {
                            isPagination = false;
                            chatHistory(++pageNo);
                        }
                    }
                }
            }
        });

        return view;
    }

    public boolean chat(PushNotification message) {
        if (chatWrapper != null) {
            if (chatWrapper.getData().get(0).getChatId().equals(message.getChatId())) {
                Debugger.e(":::MESSAGE UPDATE:::");

                HashMap<String, String> hashMapListChatHistory = new HashMap<>();
                hashMapListChatHistory.put("select_user_id", message.getUserId());
                hashMapListChatHistory.put("chat_id", message.getChatId());
                hashMapListChatHistory.put("page", "0");
                hashMapListChatHistory.put("init", "9999");

                new PostRequestHelper<ChatWrapper>().pingToRequest(Constants.CHAT_LIST
                        , null
                        , null
                        , hashMapListChatHistory
                        , ((MainActivity) getActivity()).getParamWholeParameters()
                        , new ChatWrapper()
                        , new TaskCompleteListener<ChatWrapper>() {
                            @Override
                            public void onSuccess(ChatWrapper chatHistoryWrapper) {
                                if (chatHistoryWrapper.getStatus() == 1) {
                                    if (chatHistoryWrapper.getData().get(0) != null) {
                                        chatDatas.add(0, chatHistoryWrapper.getData().get(0));
                                        chatAdapter.notifyDataSetChanged();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(ExceptionType exceptions, ChatWrapper chatWrapper) {

                            }
                        });
                return false;
            } else {
                Debugger.e("RETURN TRUE...");
                return true;
            }
        } else {
            return false;
        }
    }

    public void chatHistory(final int pageNo) {
        if (pageNo == 0) {
            if (getActivity() != null)
                ((MainActivity) getActivity()).startLoader();
        }

        HashMap<String, String> hashMapListChatHistory = new HashMap<>();

        Debugger.e("PAGE NO ::: " + pageNo);
        if (bundleString.equals("chatLayout"))
            hashMapListChatHistory.put("select_user_id", inboxData.getUserId());

        if (bundleString.equals("profile"))
            hashMapListChatHistory.put("select_user_id", profileData.getUserId());

        if (pageNo > 0) {
            hashMapListChatHistory.put("datetime", ((MainActivity) getActivity()).getChatDateTime());
            hashMapListChatHistory.put("page", pageNo + "");
        } else if (pageNo == 0) {
            hashMapListChatHistory.put("init", "9999");
        }

        new PostRequestHelper<ChatWrapper>().pingToRequest(Constants.CHAT_LIST
                , null
                , null
                , hashMapListChatHistory
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new ChatWrapper()
                , new TaskCompleteListener<ChatWrapper>() {
                    @Override
                    public void onSuccess(ChatWrapper chatWrapper) {
                        isPagination = true;
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();

                            if (chatWrapper.getStatus() == 1) {
                                Debugger.e("PAGE ::" + chatWrapper.getPage());

                                //Debugger.e("DATE  TIME::"+chatWrapper.getDatetime());
                                if (pageNo == 0) {
                                    if (chatWrapper.getChatRoomImage().equals("")) {
                                        Picasso.with(getContext()).load(R.drawable.splash).into(backGroundChatImage);
                                    } else {
                                        Picasso.with(getContext()).load(chatWrapper.getChatRoomImage()).placeholder(R.drawable.splash).into(backGroundChatImage);
                                    }
                                    //((MainActivity) getActivity()).setImage(chatWrapper.getChatRoomImage(), backGroundChatImage);
                                    ((MainActivity) getActivity()).setChatDateTime(chatWrapper.getDatetime());
                                    totalPage = chatWrapper.getPage();
                                }

                                setChatAdapter(chatWrapper);
                            }
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, ChatWrapper chatWrapper) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();

                            if (chatWrapper != null) {
                                if (chatWrapper.getStatus() == -1)
                                    ((MainActivity) getActivity()).invalidToken();
                            }

                            if (exceptions == ExceptionType.ParameterException) {
                                Debugger.e(":::EXCEPTION:::" + chatWrapper.getMessage());
                                ((MainActivity) getActivity()).messageAlert(chatWrapper.getMessage());
                            }
                        }
                    }
                });
    }

    public void setChatAdapter(ChatWrapper chatWrapper) {
        this.chatWrapper = chatWrapper;
        this.chatDatas.addAll(chatWrapper.getData());
        chatAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.sendChat)
    public void sendChatClicked() {
        if (chatMessage.getText().toString().trim().length() <= 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterMessage));
            chatMessage.requestFocus();
        } else {
            sendChat();
        }
    }

    public void sendChat() {
        ((MainActivity) getActivity()).hideKeyboard();
        HashMap<String, String> hashMapSendMessage = new HashMap<>();

        if (bundleString.equals("chatLayout")) {
            hashMapSendMessage.put("chat_id", inboxData.getChatId());
            hashMapSendMessage.put("receiver_id", inboxData.getUserId());
        } else if (bundleString.equals("profile")) {
            hashMapSendMessage.put("receiver_id", profileData.getUserId());
        }
        final String s = chatMessage.getText().toString().trim();
        hashMapSendMessage.put("message", s);

        chatMessage.setText("");

        new PostRequestHelper<SendChatWrapper>().pingToRequest(Constants.SEND_MESSAGE
                , null
                , null
                , hashMapSendMessage
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new SendChatWrapper()
                , new TaskCompleteListener<SendChatWrapper>() {
                    @Override
                    public void onSuccess(SendChatWrapper sendChatWrapper) {
                        if (sendChatWrapper.getStatus() == 1) {
                            if (getActivity() != null) {
                                chatAdapter.setText(sendChatWrapper.getData().get(0).getChatId()
                                        , ((MainActivity) getActivity()).getData(getContext()).getData().getId()
                                        , s, sendChatWrapper.getData().get(0).getInsertdate());


                                chatAdapter.notifyDataSetChanged();
                                layoutChatRecyclerView.smoothScrollToPosition(0);
                            }
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, SendChatWrapper sendChatWrapper) {
                    }
                });
    }

    @Override
    public void setHeaderTitle() {
        //((MainActivity) getActivity()).setTitle("Peter Methews", Enum.setNevigationIcon.BACK_PROFILE, true);
        if (bundleString.equals("chatLayout")) {
            ((MainActivity) getActivity()).setCircularProfileImage(inboxData.getProfileImageThumb());
            ((MainActivity) getActivity()).setTitle(inboxData.getFirstName() + " " + inboxData.getLastName(), Enum.setNevigationIcon.BACK_PROFILE, true);
        } else if (bundleString.equals("profile")) {
            ((MainActivity) getActivity()).setCircularProfileImage(profileData.getProfileImageThumb());
            ((MainActivity) getActivity()).setTitle(profileData.getFirstName() + " " + profileData.getLastName(), Enum.setNevigationIcon.BACK_PROFILE, true);
        }
    }

    @OnClick(R.id.uploadChatBackgroundImage)
    public void uploadImageChatClicked() {
        ImageChooserDialog imageChooserDialog = new ImageChooserDialog();
        imageChooserDialog.show(getActivity().getSupportFragmentManager(), "imageChooserDialog");
        imageChooserDialog.setCallback(new ImageChooserListener() {
            @Override
            public void getResultFromCamera(String result) {
                image = new File(result);
                uploadChatBackgroundImage(image);
                // ((MainActivity) getActivity()).setCircularImage(image, imageViewBusinessImage);
            }

            @Override
            public void getResultFromGallery(String result) {
                if (result != null) {
                    if (!(result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("jpeg") ||
                            result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("jpg") ||
                            result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("png") ||
                            result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("bmp"))) {
                        //isInvalidImage = true;
                        ((MainActivity) getActivity()).messageAlert(getString(R.string.message_thisImageIsNotSupported));

                    } else {
                        image = new File(result);
                        uploadChatBackgroundImage(image);

                        //((MainActivity) getActivity()).setCircularImage(image, imageViewBusinessImage);
                        //layoutAddItemImageViewAdd.setBackgroundColor(getResources().getColor(R.color.backColor));
                        //isInvalidImage = false;
                    }
                }
            }

            @Override
            public void nothingSet() {

            }
        });
    }

    public void uploadChatBackgroundImage(final File image) {
        if (getActivity() != null)
            ((MainActivity) getActivity()).startLoader();

        ArrayList<KeyValuePair> keyValuePairs = new ArrayList<>();
        keyValuePairs.add(new KeyValuePair("image", image.getAbsolutePath()));
        //new RequestParameter().getGalleryImageUpload(image)
        new PostRequestHelper<LoginWrapper>().pingToRequest(Constants.UPLOAD_CHAT_IMAGE
                , "image/*"
                , keyValuePairs
                , getHashMapForUploadImage()
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new LoginWrapper()
                , new TaskCompleteListener<LoginWrapper>() {
                    @Override
                    public void onSuccess(LoginWrapper loginWrapper) {
                        if (getActivity() != null) {

                            ((MainActivity) getActivity()).stopLoader();

                            if (loginWrapper.getStatus() == 1) {
                                //((MainActivity) getActivity()).messageAlert(loginWrapper.getMessage());
                                ((MainActivity) getActivity()).setImage(image, backGroundChatImage);
                                // callGalleyApi();
                            }
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, LoginWrapper loginWrapper) {
                        if (getActivity() != null) {

                            ((MainActivity) getActivity()).stopLoader();
                            if (loginWrapper != null) {
                                if (loginWrapper.getStatus() == -1)
                                    ((MainActivity) getActivity()).invalidToken();

                            }
                        }
                    }
                });
    }

    private HashMap<String, String> getHashMapForUploadImage() {
        HashMap<String, String> hashmapForUploadImage = new HashMap<>();
        hashmapForUploadImage.put("chat_room_id", chatWrapper.getChatId());
        return hashmapForUploadImage;
    }
}