package com.citisquare.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.controls.CustomButton;
import com.citisquare.app.controls.CustomEditText;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.interfaces.Navigation;
import com.citisquare.app.pojo.response.City;
import com.citisquare.app.pojo.response.Country;
import com.citisquare.app.pojo.response.Like;
import com.citisquare.app.utils.InitializeMenu;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 28/12/16.
 */

public class AdvertisementFragment extends BaseFragment {
    @BindView(R.id.advertiseTitle)
    CustomTextView advertiseTitle;
    @BindView(R.id.advertiseFirstName)
    CustomEditText advertiseFirstName;
    @BindView(R.id.advertiseLastName)
    CustomEditText advertiseLastName;
    @BindView(R.id.advertiseEmail)
    CustomEditText advertiseEmail;
    @BindView(R.id.advertiseCity)
    TextView advertiseCity;
    @BindView(R.id.advertiseCountryCode)
    CustomTextView advertiseCountryCode;
    @BindView(R.id.advertisePhoneNumber)
    CustomEditText advertisePhoneNumber;
    @BindView(R.id.advertiseCompanyName)
    CustomEditText advertiseCompanyName;
    @BindView(R.id.advertiseMessage)
    CustomEditText advertiseMessage;
    @BindView(R.id.advertisePost)
    CustomButton advertisePost;

    ArrayList<String> titleList;
    ArrayList<Country> countries;
    AdvertisementDropDown advertisementDropDown;
    CountryCodeDropDown countryCodeDropDown;
    CityDropDownSeller cityDropDown;
    City city;
    Country country;
    private Pattern pattern;
    private Matcher matcher;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_advertisement, container, false);
        ButterKnife.bind(this, view);
        titleList = new InitializeMenu().toGetAdvertiseMent(getContext());
        countries = ((MainActivity) getActivity()).countryDatas();

        for (Country countryLoop : countries) {
            if (countryLoop.getDialCode().equals("+966")) {
                country = countryLoop;
                advertiseCountryCode.setText(countryLoop.getDialCode());
            }
        }
        pattern = Pattern.compile(Constants.englishOnly);

        advertiseMessage.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.advertiseMessage) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        return view;
    }


    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle(getString(R.string.advertisingWithUs), Enum.setNevigationIcon.MENU, false);
    }

    @OnClick({R.id.advertiseTitle, R.id.advertiseCountryCode, R.id.advertisePost, R.id.advertiseCity})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.advertiseTitle:
                ((MainActivity) getActivity()).hideKeyboard();
                int[] location = new int[2];
                advertiseTitle.getLocationOnScreen(location);
                advertisementDropDown = new AdvertisementDropDown(getContext()
                        , advertiseTitle.getHeight()
                        , location
                        , titleList
                        , new ItemEventListener<Integer>() {
                    @Override
                    public void onItemEventFired(Integer integer, Integer t2, String actualPos) {
                        advertiseTitle.setText(titleList.get(integer));

//                        branchId = integer + 1;

                    }
                });
                advertisementDropDown.show();
                break;
            case R.id.advertiseCountryCode:
                ((MainActivity) getActivity()).hideKeyboard();
                int[] locationContry = new int[2];
                advertiseCountryCode.getLocationOnScreen(locationContry);
                countryCodeDropDown = new CountryCodeDropDown(getContext(), advertiseCountryCode.getHeight(), locationContry, countries, new ItemEventListener<Country>() {
                    @Override
                    public void onItemEventFired(Country integer, Country t2, String actualPos) {
                        country = integer;
                        advertiseCountryCode.setText(integer.getDialCode());
//                        branchId = integer + 1;
                    }
                });
                countryCodeDropDown.show();
                break;
            case R.id.advertiseCity:
                ((MainActivity) getActivity()).hideKeyboard();
                int[] locationCity = new int[2];
                advertiseCity.getLocationOnScreen(locationCity);
                final ArrayList<City> cityList = ((MainActivity) getActivity()).cityArrayList();
                cityDropDown = new CityDropDownSeller(getContext(), advertiseCity.getHeight(), locationCity, cityList, new ItemEventListener<String>() {
                    @Override
                    public void onItemEventFired(String integer, String t2, String actualPos) {
                        city = new Gson().fromJson(t2, City.class);

                        if (Locale.getDefault().getDisplayLanguage().equals("العربية"))
                            advertiseCity.setText(city.getArName());
                        else
                            advertiseCity.setText(city.getName());


                    }
                });
                cityDropDown.show();
                break;
            case R.id.advertisePost:
                if (isValid()) {
                    advertiseMentCallApi();
                    //advertisePostSuccess();
                    //((MainActivity) getActivity()).messageAlert(getString(R.string.message_advertisePostedSuccessfully));
                    //sellerSignup();
                    //   getLocationAndPayment();
                    //((MainActivity) getActivity()).replaceParentFragment(new TabFragment(), "parent");
                }

                break;
        }
    }

    private void advertiseMentCallApi() {

        if (getActivity() != null) {
            ((MainActivity) getActivity()).hideKeyboard();
            ((MainActivity) getActivity()).startLoader();
        }
        new PostRequestHelper<Like>().pingToRequest(Constants.ADVERTISEMENT
                , null
                , null
                , getHashMapForAdvertisement()
                , ((MainActivity) getActivity()).getParamWholeParameters(), new Like(), new TaskCompleteListener<Like>() {
                    @Override
                    public void onSuccess(Like mObject) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();
                        }
                        if (mObject.getStatus() == 1) {
                            ((MainActivity) getActivity()).messageAlert(mObject.getMessage());
                            advertiseClear();
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, Like like) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();
                        }
                        if (like != null) {
                            if (like.getStatus() == -1)
                                ((MainActivity) getActivity()).invalidToken();
                        }

                    }
                });
    }

    private void advertiseClear() {
        advertiseTitle.setText(getString(R.string.title));
        advertiseFirstName.setText("");
        advertiseLastName.setText("");
        advertiseEmail.setText("");
        advertiseCity.setText(getString(R.string.city));
        advertiseCountryCode.setText("+966");
        advertisePhoneNumber.setText("");
        advertiseMessage.setText("");
        advertiseCompanyName.setText("");
    }

    private HashMap<String, String> getHashMapForAdvertisement() {
        HashMap<String, String> hashMapForadvertisement = new HashMap<>();
        hashMapForadvertisement.put("title", advertiseTitle.getText().toString());
        hashMapForadvertisement.put("first_name", advertiseFirstName.getText().toString().trim());
        hashMapForadvertisement.put("last_name", advertiseLastName.getText().toString().trim());
        hashMapForadvertisement.put("email", advertiseEmail.getText().toString().trim());
        hashMapForadvertisement.put("city_id", city.getId());
        hashMapForadvertisement.put("country_code", country.getDialCode());
        hashMapForadvertisement.put("phone_no", advertisePhoneNumber.getText().toString());
        hashMapForadvertisement.put("message", advertiseMessage.getText().toString().trim());
        hashMapForadvertisement.put("company_name", advertiseCompanyName.getText().toString().trim());
        return hashMapForadvertisement;
    }

    private void advertisePostSuccess() {
        advertiseTitle.setText(getString(R.string.title));
        advertiseFirstName.setText("");
        advertiseLastName.setText("");
        advertiseEmail.setText("");
        advertiseCountryCode.setText(getString(R.string.countryCode));
        advertisePhoneNumber.setText("");
        advertiseCity.setText(getString(R.string.city));
        advertiseCompanyName.setText("");
        advertiseMessage.setText("");
    }

    public boolean isValid() {
        if (advertiseTitle.getText().toString().equals(getString(R.string.title))) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterTitle));
            advertiseTitle.requestFocus();
            return false;
        }

        if (advertiseFirstName.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterFirstName));
            advertiseFirstName.requestFocus();
            return false;
        }

        if (advertiseLastName.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterLastName));
            advertiseLastName.requestFocus();
            return false;
        }

        if (!validEmail(advertiseEmail.getText().toString())) {
            advertiseEmail.requestFocus();
            return false;
        }

        if (advertiseCity.getText().toString().equals(getString(R.string.city))) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterCity));
            advertiseCity.requestFocus();
            return false;
        }


        if (advertiseCountryCode.getText().toString().equals(getString(R.string.countryCode))) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseSelectCountryCode));
            advertiseCountryCode.requestFocus();
            return false;
        }

        if (advertisePhoneNumber.getText().toString().equals("")) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterPhoneNumber));
            advertisePhoneNumber.requestFocus();
            return false;
        }

        if (advertisePhoneNumber.getText().toString().length() < 9) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterValidPhoneNumber));
            advertisePhoneNumber.requestFocus();
            return false;
        }

        if (advertiseMessage.getText().toString().equals("")) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterMessage));
            advertiseMessage.requestFocus();
            return false;
        }
        return true;
    }

    public boolean validEmail(String email) {
        int length = email.trim().length();
        if (length <= 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterEmail));
            return false;
        }
        // Patterns.WEB_URL.matcher(potentialUrl).matches()
        Pattern pattern = Pattern.compile(Navigation.EMAIL_REGEX);
        Matcher matcher = pattern.matcher(email);
        if (!(email.charAt(0) + "").matches("\\p{L}") || !matcher.matches()) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterValidEmailId));
            return false;
        }
        return true;
    }

}