package com.citisquare.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.CommentAdapter;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.pojo.response.CommentData;
import com.citisquare.app.pojo.response.CommentWrapper;
import com.citisquare.app.pojo.response.LoginData;
import com.citisquare.app.pojo.response.LoginWrapper;
import com.citisquare.app.pojo.response.PostComment;
import com.citisquare.app.utils.RequestParameter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 1/6/16.
 */
public class CommentFragment extends BaseFragment {

    @BindView(R.id.commentRecycler)
    RecyclerView commentRecycler;
    @BindView(R.id.editPostComment)
    EditText editPostComment;

    List<CommentData> commentList;
    CommentAdapter commentAdapter;
    GridLayoutManager gridLayoutManager;
    @BindView(R.id.commentAdd)
    TextView commentAdd;

    Bundle bundleComment;
    String postId = "";
    String flag = "";
    boolean isPagination;
    int pageNo = 1;
    String s = "";

    int commentCount = 0;


    private Pattern pattern;
    private Matcher matcher;
    private CommentListener commentListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.comment_layout, container, false);
        ButterKnife.bind(this, view);

        pattern = Pattern.compile(Constants.englishOnly);

        bundleComment = getArguments();
        if (bundleComment != null) {
            postId = bundleComment.getString("postId");
            flag = bundleComment.getString("FROM");
        }
        commentList = new ArrayList<>();
        commentAdapter = new CommentAdapter(getContext(), commentList);
        gridLayoutManager = new GridLayoutManager(getActivity(), 1);

        gridLayoutManager.setReverseLayout(true);

        commentRecycler.setLayoutManager(gridLayoutManager);
        commentRecycler.setAdapter(commentAdapter);
        commentRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (isPagination) {
                    int visibleItemCount = recyclerView.getChildCount();
                    int totalItemCount = gridLayoutManager.getItemCount();
                    int firstVisibleItemPosition = gridLayoutManager.findFirstVisibleItemPosition();
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount) {
                        isPagination = false;
                        commentListApiCall(++pageNo);
                    }
                }
            }
        });

        commentListApiCall(pageNo);

        return view;
    }


    public void commentListApiCall(final int pageNo) {
        if (getActivity() != null && pageNo == 1)
            ((MainActivity) getActivity()).startLoader();

        new PostRequestHelper<CommentWrapper>().pingToRequest(Constants.COMMENT_LISTING
                , null
                , null
                , new RequestParameter().getHashMapForComment(getHashMapForComment(pageNo))
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new CommentWrapper()
                , new TaskCompleteListener<CommentWrapper>() {
                    @Override
                    public void onSuccess(CommentWrapper commentWrapper) {

                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        if (pageNo == 1) {
                            commentList.clear();
                        }

                        if (commentWrapper.getStatus() == 1) {
                            commentCount = commentWrapper.getCommentCount();
                            isPagination = true;
                            setCommentAdapter(commentWrapper.getData());
                        } else {
                            isPagination = false;
                            if (pageNo == 1)
                                ((MainActivity) getActivity()).messageAlert(commentWrapper.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, CommentWrapper commentWrapper) {
                        isPagination = false;
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();
                        if (commentWrapper != null && commentWrapper.getStatus() == -1)
                            ((MainActivity) getActivity()).invalidToken();

                        if (exceptions == ExceptionType.ParameterException) {
                            if (pageNo == 1)
                                ((MainActivity) getActivity()).messageAlert(commentWrapper.getMessage());
                        }
                    }
                });
    }

    public void setCommentAdapter(List<com.citisquare.app.pojo.response.CommentData> commentDatas) {
        commentList.addAll(commentDatas);
        commentAdapter.notifyDataSetChanged();
    }

    public HashMap<String, String> getHashMapForComment(int pageNo) {
        HashMap<String, String> hashMapComment = new HashMap<>();
        hashMapComment.put("post_id", postId);
        hashMapComment.put("flag", flag);
        hashMapComment.put("page", String.valueOf(pageNo));
        return hashMapComment;
    }

    @OnClick(R.id.commentAdd)
    public void commentClicked() {
        //((MainActivity) getActivity()).hideKeyboard();
        if (editPostComment.getText().toString().trim().length() <= 0) {
            //((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterComment));
            editPostComment.requestFocus();
        } else {

            s = editPostComment.getText().toString().trim();
            postCommentApiCall();
        }/*{
            s = editPostComment.getText().toString().trim();
            postCommentApiCall();
        }*/
    }

    public void postCommentApiCall() {
        if (getActivity() != null) {
            ((MainActivity) getActivity()).startLoader();
        }
        final String commentString = editPostComment.getText().toString().trim();
        editPostComment.setText("");
        new PostRequestHelper<PostComment>().pingToRequest(Constants.COMMENT
                , null
                , null
                , new RequestParameter().getCommentSendHashMap(getCommentSendHashMap())
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new PostComment()
                , new TaskCompleteListener<PostComment>() {
                    @Override
                    public void onSuccess(PostComment comment) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();
                        }

                        editPostComment.setText("");
                        if (comment.getStatus() == 1) {
                            commentCount = comment.getCommentCount();
                            LoginWrapper loginWrapper;
                            LoginData loginData;
                            if (getActivity() != null) {
                                loginWrapper = ((MainActivity) getActivity()).getData(getContext());
                                loginData = loginWrapper.getData();
                                if (loginData != null) {

                                    commentList.add(0, new CommentData(""
                                            , ""
                                            , loginData.getId()
                                            , commentString
                                            , "1"
                                            , "0"
                                            , loginData.getUsername()
                                            , loginData.getProfileImage(), loginData.getProfileImageThumb()));
                                }
                                commentAdapter.notifyDataSetChanged();
                                commentRecycler.smoothScrollToPosition(0);
                            }
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, PostComment commentWrapper) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        if (commentWrapper.getStatus() == -1)
                            ((MainActivity) getActivity()).invalidToken();
                    }
                });
    }

    @Override
    public void onStop() {
        super.onStop();
        if (commentListener != null)
            commentListener.commentCount(commentCount);
    }

    private HashMap<String, String> getCommentSendHashMap() {
        HashMap<String, String> hashMapComment = new HashMap<>();
        hashMapComment.put("post_id", postId);
        hashMapComment.put("comment", s);
        hashMapComment.put("flag", flag);
        return hashMapComment;
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle(getString(R.string.title_comment), Enum.setNevigationIcon.BACK, true);
    }

    public void setCommentListener(CommentListener commentListener) {
        this.commentListener = commentListener;
    }

    public void setCount(int counter) {
        commentCount = counter;
    }

    public interface CommentListener {
        void commentCount(int count);
    }
}
