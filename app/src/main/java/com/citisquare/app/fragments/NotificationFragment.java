package com.citisquare.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.NotificationAdapter;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.pojo.response.NotificationData;
import com.citisquare.app.pojo.response.NotificationWrapper;
import com.citisquare.app.utils.RequestParameter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 4/6/16.
 */
public class NotificationFragment extends BaseFragment {

    @BindView(R.id.notificationRecyclerView)
    RecyclerView notificationRecyclerView;

    GridLayoutManager gridLayoutManager;
    NotificationAdapter notificationAdapter;
    List<NotificationData> notificationList;
    boolean isPagination;
    NotificationData notificationData;
    int pageno = 1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.notification_layout, container, false);
        ButterKnife.bind(this, view);

        notificationList = new ArrayList<>();

        pageno = 1;
        notificationAdapter = new NotificationAdapter(getContext(), notificationList, new ItemEventListener<String>() {
            @Override
            public void onItemEventFired(String s, String t2, String actualPos) {
                notificationData = new Gson().fromJson(s, NotificationData.class);
                if (notificationData.getPostId().equals("0")) {
                    String userId = notificationData.getUserId();
                    /*MOVE TO PROFILE*/
                    ProfileFragment profileFragment = new ProfileFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("profile", "other");
                    ((MainActivity) getActivity()).setProfileUserId(userId);
                    profileFragment.setArguments(bundle);
                    ((MainActivity) getActivity()).setSelectionPostion(0);
                    ((MainActivity) getActivity()).setFragment(profileFragment, false, "");
                    //childFragmentReplacement(profileFragment, "child");

                } else {
                    /*DETAILS PROFILE*/
                    DetailsHomeFragment detailsHomeFragment = new DetailsHomeFragment();
                    Bundle bundleDetails = new Bundle();
                    bundleDetails.putString("details", "notification");
                    bundleDetails.putString("Notification", s);
                    detailsHomeFragment.setArguments(bundleDetails);
                    ((MainActivity) getActivity()).setFragment(detailsHomeFragment, false, "");
                }
            }
        });
        gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        notificationRecyclerView.setLayoutManager(gridLayoutManager);
        notificationRecyclerView.setAdapter(notificationAdapter);

        notificationRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (isPagination) {
                    int visibleItemCount = recyclerView.getChildCount();
                    int totalItemCount = gridLayoutManager.getItemCount();
                    int firstVisibleItemPosition = gridLayoutManager.findFirstVisibleItemPosition();
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount) {
                        isPagination = false;
                        notificationListing(++pageno);
                    }
                }
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        pageno = 1;
        notificationListing(pageno);
    }

    public void notificationListing(final int pageno) {
        if (getActivity() != null) {
            ((MainActivity) getActivity()).startLoader();
        }
        new PostRequestHelper<NotificationWrapper>().pingToRequest(Constants.NOTIFICATION_LIST
                , null
                , null
                , new RequestParameter().getHashMapForNotification(getNotificationMap(pageno))
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new NotificationWrapper()
                , new TaskCompleteListener<NotificationWrapper>() {
                    @Override
                    public void onSuccess(NotificationWrapper notificationWrapper) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();
                        }

                        if (pageno == 1)
                            notificationList.clear();

                        if (notificationWrapper.getStatus() == 1) {
                            isPagination = true;
                            setNotificationAdapter(notificationWrapper.getData());
                        } else if (notificationWrapper.getStatus() == 0) {
                            isPagination = false;
                            if (pageno == 1)
                                ((MainActivity) getActivity()).messageAlert(notificationWrapper.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, NotificationWrapper notificationWrapper) {
                        isPagination = false;

                        if (pageno == 1) {
                            ((MainActivity) getActivity()).messageAlert(notificationWrapper.getMessage());
                        }
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();
                        }
                        if (notificationWrapper != null) {
                            if (notificationWrapper.getStatus() == -1) {
                                ((MainActivity) getActivity()).invalidToken();
                            }
                        }
                    }
                });
    }

    private void setNotificationAdapter(List<NotificationData> data) {
        notificationList.addAll(data);
        notificationAdapter.notifyDataSetChanged();
    }

    private HashMap<String, String> getNotificationMap(int pageno) {
        HashMap<String, String> notificationHashMap = new HashMap<>();
        notificationHashMap.put("page", pageno + "");
        return notificationHashMap;
    }


    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle(getString(R.string.title_notifications), Enum.setNevigationIcon.BACK, false);
    }
}
