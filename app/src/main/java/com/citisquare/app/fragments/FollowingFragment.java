package com.citisquare.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.FollowingAdapter;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.pojo.response.Like;
import com.citisquare.app.pojo.response.ProfileFollowingData;
import com.citisquare.app.pojo.response.ProfileFollowingWrapper;
import com.citisquare.app.utils.Debugger;
import com.citisquare.app.utils.RequestParameter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import serviceCalling.builder.KeyValuePair;
import serviceCalling.helper.GetRequestHelper;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * d
 * Created by hlink56 on 1/6/16.
 */
public class FollowingFragment extends BaseFragment {

    @BindView(R.id.followingRecyclerView)
    RecyclerView followingRecyclerView;

    FollowingAdapter followingAdapter;
    List<ProfileFollowingData> followingList;
    GridLayoutManager gridLayoutManager;
    int pageNo = 1;

    boolean isPagination;
    View view;
    Bundle bundle;
    String stringProfile;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.following_layout, container, false);
        ButterKnife.bind(this, view);

        bundle = getArguments();
        if (bundle != null) {
            if (bundle.getString("Profile") != null)
                stringProfile = bundle.getString("Profile");
        }
        followingList = new ArrayList<>();
        pageNo = 1;
        followingAdapter = new FollowingAdapter(getContext(), followingList, new ItemEventListener<String>() {
            @Override
            public void onItemEventFired(String s, String userId, String actualPos) {
                switch (s) {
                    case "Follow":
                        doFollowApiCall(userId, "follow");
                        break;
                    case "Unfollow":
                        doFollowApiCall(userId, "unfollow");
                        break;
                    case "otherprofile":
                        if (userId.equals(((MainActivity) getActivity()).userId())) {
                            ProfileFragment profileFragment = new ProfileFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("profile", "own");
                            ((MainActivity) getActivity()).setProfileUserId(userId);
                            profileFragment.setArguments(bundle);
                            ((MainActivity) getActivity()).setSelectionPostion(0);
                            ((MainActivity) getActivity()).setFragment(profileFragment, false, "");
                        } else {
                            ProfileFragment profileFragment = new ProfileFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("profile", "other");
                            ((MainActivity) getActivity()).setProfileUserId(userId);
                            profileFragment.setArguments(bundle);
                            ((MainActivity) getActivity()).setSelectionPostion(0);
                            ((MainActivity) getActivity()).setFragment(profileFragment, false, "");
                            //childFragmentReplacement(profileFragment, "child");
                        }
                        break;
                }
            }
        });
        gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        followingRecyclerView.setLayoutManager(gridLayoutManager);
        followingRecyclerView.setAdapter(followingAdapter);
        followingRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (isPagination) {
                    int visibleItemCount = recyclerView.getChildCount();
                    int totalItemCount = gridLayoutManager.getItemCount();
                    int firstVisibleItemPosition = gridLayoutManager.findFirstVisibleItemPosition();
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount) {
                        isPagination = false;
                        followingApiCall(++pageNo);
                    }
                }
            }
        });


        return view;
    }

    public void followingApi() {
        pageNo = 1;
        followingApiCall(pageNo);
    }

    public void doFollowApiCall(final String userId, final String status) {
        new PostRequestHelper<Like>().pingToRequest(Constants.DO_FOLLOW
                , null
                , null
                , new RequestParameter().getHashMapForDoFollow(getDoFollowMap(userId, status))
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new Like()
                , new TaskCompleteListener<Like>() {
                    @Override
                    public void onSuccess(Like mObject) {
                        if (mObject.getStatus() == 1)
                            Debugger.e("FOLLOW SUCCESSFULLY");
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, Like like) {
                        if(like!=null) {
                            if (like.getStatus() == -1)
                                ((MainActivity) getActivity()).invalidToken();
                        }
                    }
                });
    }

    public HashMap<String, String> getDoFollowMap(String userId, String status) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("follower_user_id", userId);
        if (status.equals("follow"))
            hashMap.put("status", "1");
        else
            hashMap.put("status", "0");

        return hashMap;
    }

    public void followingApiCall(final int pageNo) {
        ArrayList<KeyValuePair> keyValuePairs = new ArrayList<>();
        keyValuePairs.add(new KeyValuePair("page", pageNo + ""));
        keyValuePairs.add(new KeyValuePair("user_id", ((MainActivity) getActivity()).getProfileUserId()));


        new GetRequestHelper<ProfileFollowingWrapper>().pingToRequest(Constants.FOLLOWING_LIST
                , keyValuePairs
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new ProfileFollowingWrapper()
                , new TaskCompleteListener<ProfileFollowingWrapper>() {
                    @Override
                    public void onSuccess(ProfileFollowingWrapper mObject) {

                        if (pageNo == 1) {
                            followingList.clear();
                        }
                        if (mObject.getStatus() == 1) {
                            setFollowingAdapter(mObject.getData());
                        } else if (mObject.getStatus() == 0) {
                            ((MainActivity) getActivity()).messageAlert(mObject.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, ProfileFollowingWrapper followingWrapper) {

                        if (getActivity() != null && followingWrapper != null) {
                            if (pageNo == 1) {
                                ((MainActivity) getActivity()).messageAlert(followingWrapper.getMessage());
                            }
                            if(followingWrapper!=null) {
                                if (followingWrapper.getStatus() == -1)
                                    ((MainActivity) getActivity()).invalidToken();
                            }
                        }
                        setPage();
                    }
                });
    }

    public void setPage() {
        pageNo = 1;
    }

    public void setFollowingAdapter(List<ProfileFollowingData> followingList) {
        this.followingList.addAll(followingList);
        followingAdapter.notifyDataSetChanged();
    }


    @Override
    public void setHeaderTitle() {

    }
}
