package com.citisquare.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.interfaces.Enum;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by hlink56 on 19/1/17.
 */

public class AboutUsFragment extends BaseFragment {


    @BindView(R.id.termsAndCondition)
    CustomTextView termsAndCondition;
    @BindView(R.id.privacyPolicy)
    CustomTextView privacyPolicy;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.about_us_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle(getString(R.string.ABOUTUS), Enum.setNevigationIcon.BACK, true);
    }

    @OnClick({R.id.termsAndCondition, R.id.privacyPolicy})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.termsAndCondition:
                WebViewFragment webViewFragment = new WebViewFragment();
                Bundle bundle = new Bundle();
                bundle.putString("from", "1");
                webViewFragment.setArguments(bundle);
                ((MainActivity) getActivity()).setFragment(webViewFragment, false, "");
                break;
            case R.id.privacyPolicy:
                WebViewFragment webViewFrag = new WebViewFragment();
                Bundle bundleWebView = new Bundle();
                bundleWebView.putString("from", "0");
                webViewFrag.setArguments(bundleWebView);
                ((MainActivity) getActivity()).setFragment(webViewFrag, false, "");
                break;
        }
    }
}
