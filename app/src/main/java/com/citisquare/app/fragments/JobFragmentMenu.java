package com.citisquare.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.JobsFrimMenuAdapter;
import com.citisquare.app.controls.CustomEditText;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.dialog.JobApplyDialog;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.pojo.Filter;
import com.citisquare.app.pojo.JobMenuData;
import com.citisquare.app.pojo.JobMenuWrapper;
import com.citisquare.app.utils.InitializeMenu;
import com.citisquare.app.utils.RequestParameter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 21/4/17.
 */

public class JobFragmentMenu extends BaseFragment {
    @BindView(R.id.searchText)
    CustomEditText searchText;
    @BindView(R.id.filterSearchDropDown)
    CustomTextView filterSearchDropDown;
    @BindView(R.id.mainJobsRecycler)
    RecyclerView mainJobsRecycler;
    @BindView(R.id.framelayoutPlaceholder)
    FrameLayout framelayoutPlaceholder;
    private JobsFrimMenuAdapter jobsAdapter;
    private GridLayoutManager gridLayoutManager;
    private boolean isPagination;
    private int pageno = 1;

    FilterDropDown filterDropDown;
    List<JobMenuData> jobList;
    private ArrayList<Filter> filtersListJob;

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle("default", Enum.setNevigationIcon.MENU_CITY, false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.job_from_menu, container, false);
        ButterKnife.bind(this, view);

        jobList = new ArrayList<>();
        filtersListJob = new InitializeMenu().toGetFilterJob(getContext());

        jobsAdapter = new JobsFrimMenuAdapter(getActivity(), jobList, new ItemEventListener<String>() {
            @Override
            public void onItemEventFired(String s, String t2, String actualPos) {
                switch (t2) {
                    case "JOBAPPLY":
                        JobApplyDialog jobApplyDialog = new JobApplyDialog();
                        Bundle bundle = new Bundle();
                        bundle.putString("jobData", s);
                        jobApplyDialog.setArguments(bundle);
                        jobApplyDialog.show(getActivity().getSupportFragmentManager(), jobApplyDialog.getClass().getName());
                        break;
                    case "profile":
                        if (s.equals(((MainActivity) getActivity()).userId())) {
                            ProfileFragment profileFragment = new ProfileFragment();
                            Bundle bundleProfile = new Bundle();
                            bundleProfile.putString("profile", "own");
                            ((MainActivity) getActivity()).setProfileUserId(s);
                            profileFragment.setArguments(bundleProfile);
                            ((MainActivity) getActivity()).setSelectionPostion(0);
                            ((MainActivity) getActivity()).setFragment(profileFragment, false, "");
                            //childFragmentReplacement(profileFragment, "child");
                        } else {
                            ProfileFragment profileFragment = new ProfileFragment();
                            Bundle bundleProfile = new Bundle();
                            bundleProfile.putString("profile", "other");
                            ((MainActivity) getActivity()).setProfileUserId(s);
                            profileFragment.setArguments(bundleProfile);
                            ((MainActivity) getActivity()).setSelectionPostion(0);
                            ((MainActivity) getActivity()).setFragment(profileFragment, false, "");
                            // childFragmentReplacement(profileFragment, "child");
                        }

                       /* ProfileFragment profileFragment = new ProfileFragment();
                        Bundle bundleProfile = new Bundle();
                        bundleProfile.putString("profile", "other");
                        profileFragment.setArguments(bundleProfile);
                        ((MainActivity) getActivity()).setSelectionPostion(0);
                        //((MainActivity) getActivity()).setFragment(profileFragment);
                        childFragmentReplacement(profileFragment, "child");*/
                        break;
                    case "map":
                        ((MainActivity) getActivity()).setFragment(new OtherMapPathFragment(), false, "");
                        break;
                }
            }
        });

        gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        mainJobsRecycler.setLayoutManager(gridLayoutManager);
        mainJobsRecycler.setAdapter(jobsAdapter);

        mainJobsRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (isPagination) {
                    int visibleItemCount = recyclerView.getChildCount();
                    int totalItemCount = gridLayoutManager.getItemCount();
                    int firstVisibleItemPosition = gridLayoutManager.findFirstVisibleItemPosition();
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount) {
                        isPagination = false;
                        callJobApi(++pageno);
                    }
                }
            }
        });
        callJobApi(pageno);

        return view;
    }

    private void callJobApi(int page) {
        if (getActivity() != null && pageno == 1)
            ((MainActivity) getActivity()).startLoader();

        new PostRequestHelper<JobMenuWrapper>().pingToRequest(Constants.JOB_FROM_MENU_LISTING
                , null
                , null
                , new RequestParameter().getJobList(getJobHashMap(pageno))
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new JobMenuWrapper()
                , new TaskCompleteListener<JobMenuWrapper>() {
                    @Override
                    public void onSuccess(JobMenuWrapper jobListWrapper) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        if (pageno == 1)
                            jobList.clear();

                        if (jobListWrapper.getStatus() == 1) {
                            isPagination = true;
                            setJobAdapter(jobListWrapper.getData());
                        } else if (jobListWrapper.getStatus() == 0) {
                            isPagination = false;
                            jobList.clear();
                            setJobAdapter(jobListWrapper.getData());
                            if (pageno == 1)
                                ((MainActivity) getActivity()).messageAlert(jobListWrapper.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, JobMenuWrapper jobListWrapper) {
                        isPagination = false;
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();

                            if (jobListWrapper != null) {
                                if (pageno == 1) {
                                    jobList.clear();
                                    jobsAdapter.notifyDataSetChanged();
                                }
                                if (jobListWrapper.getStatus() == -1)
                                    ((MainActivity) getActivity()).invalidToken();


                                if (exceptions == ExceptionType.ParameterException) {
                                    isPagination = false;
                                    if (pageno == 1)
                                        ((MainActivity) getActivity()).messageAlert(jobListWrapper.getMessage());
                                }
                            }
                        }
                    }
                });
    }

    @OnClick(R.id.filterSearchDropDown)
    public void searchFilter() {
        int location[] = new int[2];
        ((MainActivity) getActivity()).hideKeyboard();
        filterSearchDropDown.getLocationOnScreen(location);

        filterDropDown = new FilterDropDown(getContext(), filterSearchDropDown.getHeight(),
                location, filtersListJob, new ItemEventListener<Integer>() {
            @Override
            public void onItemEventFired(Integer integer, Integer t2, String actualPos) {
                filterSearchDropDown.setText(filtersListJob.get(integer).getName());
                apiCall();
            }
        });
        filterDropDown.show();
    }

    public void apiCall() {
        pageno = 1;
        callJobApi(pageno);
    }


    public void setJobAdapter(List<JobMenuData> jobAdapter) {
        jobList.addAll(jobAdapter);
        jobsAdapter.notifyDataSetChanged();
    }

    public HashMap<String, String> getJobHashMap(int pageno) {
        HashMap<String, String> hashMap = new HashMap<>();
        String filter = "";

        if (filterSearchDropDown.getText().toString().equals(getString(R.string.saudis_jobs_drop)))
            filter = "S";
        else if (filterSearchDropDown.getText().toString().equals(getString(R.string.non_saudis_jobs_drop)))
            filter = "NS";

        /*cat_id,city_id,page	subcat_id,job_type,search*/
        hashMap.put("job_type", filter);
        hashMap.put("search", searchText.getText().toString().trim());
        hashMap.put("city_id", ((MainActivity) getActivity()).getTopCitySelectionId());
        hashMap.put("page", String.valueOf(pageno));
        return hashMap;
    }
}