package com.citisquare.app.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.PlaceAutocompleteAdapter;
import com.citisquare.app.controls.CustomEditText;
import com.citisquare.app.dialog.CountryDialog;
import com.citisquare.app.dialog.ImageChooserDialog;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ImageChooserListener;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.interfaces.Navigation;
import com.citisquare.app.pojo.addresslocation.Address;
import com.citisquare.app.pojo.response.City;
import com.citisquare.app.pojo.response.Country;
import com.citisquare.app.pojo.response.LoginData;
import com.citisquare.app.pojo.response.LoginWrapper;
import com.citisquare.app.utils.BranchDropDown;
import com.citisquare.app.utils.ColorFilterTransformation;
import com.citisquare.app.utils.Debugger;
import com.citisquare.app.utils.InitializeMenu;
import com.citisquare.app.utils.RequestParameter;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import serviceCalling.builder.KeyValuePair;
import serviceCalling.builder.ServiceParameter;
import serviceCalling.helper.GetRequestHelper;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 8/6/16.
 */
public class MyInfoEditModeFragment extends BaseFragment {



    File timeLineImage;
    File editImage;


    CityDropDownSeller cityDropDown;
    ArrayList<City> cityList;
    ArrayList<Country> countryList;

    @BindView(R.id.myInfoBackTimelineImage)
    ImageView myInfoBackTimelineImage;
    @BindView(R.id.editProfileImage)
    ImageView editProfileImage;
    @BindView(R.id.timeLineProfileCamera)
    TextView timeLineProfileCamera;
    @BindView(R.id.editLayout)
    FrameLayout editLayout;
    @BindView(R.id.myinfoEditEmail)
    EditText myinfoEditEmail;
    @BindView(R.id.myinfoEditWebsite)
    EditText myinfoEditWebsite;
    @BindView(R.id.myinfoEditBuildingNumber)
    EditText myinfoEditBuildingNumber;
    @BindView(R.id.myinfoEditStreet)
    AutoCompleteTextView myinfoEditStreet;
    @BindView(R.id.myinfoEditCity)
    TextView myinfoEditCity;
    @BindView(R.id.myinfoEditCountry)
    TextView myinfoEditCountry;
    @BindView(R.id.myinfoEditCountryCode)
    TextView myinfoEditCountryCode;
    @BindView(R.id.myinfoEditPhoneNumber)
    EditText myinfoEditPhoneNumber;
    @BindView(R.id.myinfoEditBio)
    EditText myinfoEditBio;
    City city;
    @BindView(R.id.tagCellPhoneEdit)
    TextView tagCellPhoneEdit;
    @BindView(R.id.myinfoEditCellPhoneCountryCode)
    TextView myinfoEditCellPhoneCountryCode;
    @BindView(R.id.myinfoEditCellPhoneNumber)
    EditText myinfoEditCellPhoneNumber;
    @BindView(R.id.myinfoEditBranch)
    TextView myinfoEditBranch;
    LoginWrapper loginWrapper;
    LoginData loginData;
    @BindView(R.id.myinfotextWebsite)
    TextView myinfotextWebsite;
    @BindView(R.id.myInfoTextBranch)
    TextView myInfoTextBranch;
    @BindView(R.id.myInfotextPhone)
    TextView myInfotextPhone;
    @BindView(R.id.myInfoLayoutPhone)
    LinearLayout myInfoLayoutPhone;
    @BindView(R.id.myinfoTextName)
    TextView myinfoTextName;
    @BindView(R.id.myinfoEditName)
    EditText myinfoEditName;
    @BindView(R.id.myinfoTextFirstname)
    TextView myinfoTextFirstname;
    @BindView(R.id.myinfoEditFirstName)
    EditText myinfoEditFirstName;
    @BindView(R.id.myinfoTextLastname)
    TextView myinfoTextLastname;
    @BindView(R.id.myinfoEditLastName)
    EditText myinfoEditLastName;
    @BindView(R.id.myinfoTextUsername)
    TextView myinfoTextUsername;
    @BindView(R.id.myinfoEdituserName)
    EditText myinfoEdituserName;
    @BindView(R.id.myinfoEditPassword)
    CustomEditText myinfoEditPassword;

    BranchDropDown branchDropDown;
    Country country;
    ArrayList<String> branchList;
    Double lat, lng;

    private Pattern pattern;
    private Matcher matcher;
    private PlaceAutocompleteAdapter placeAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.myinfo_edit_mode, container, false);
        ButterKnife.bind(this, view);


        cityList = ((MainActivity) getActivity()).cityArrayList();
        countryList = ((MainActivity) getActivity()).countryDatas();
        branchList = new InitializeMenu().toGetBranchList();
        loginWrapper = ((MainActivity) getActivity()).getData(getContext());

        pattern = Pattern.compile(Constants.englishOnly);

        if (Locale.getDefault().getDisplayLanguage().equals("العربية")) {
            myinfoEditPassword.setGravity(Gravity.RIGHT);
        } else {
            myinfoEditPassword.setGravity(Gravity.LEFT);
        }

        if (loginWrapper != null) {
            loginData = loginWrapper.getData();
            if (loginData.getRole().equalsIgnoreCase("S")) {
                myinfoEditFirstName.setVisibility(View.GONE);
                myinfoTextFirstname.setVisibility(View.GONE);
                myinfoEditLastName.setVisibility(View.GONE);
                myinfoTextLastname.setVisibility(View.GONE);

                myinfoEditWebsite.setVisibility(View.VISIBLE);
                myinfotextWebsite.setVisibility(View.VISIBLE);
                myinfoEditBranch.setVisibility(View.VISIBLE);
                myInfoTextBranch.setVisibility(View.VISIBLE);
                myInfoLayoutPhone.setVisibility(View.VISIBLE);
                myInfotextPhone.setVisibility(View.VISIBLE);
                myinfoTextName.setVisibility(View.VISIBLE);
                myinfoEditName.setVisibility(View.VISIBLE);

                myinfoEditName.setText(loginData.getFirstName());
                myinfoEditWebsite.setText(loginData.getWebsite());
                myinfoEditBranch.setText(loginData.getBranchName());
                myinfoEditPhoneNumber.setText(loginData.getPhoneNo());
                myinfoEditCountryCode.setText(loginData.getCountryCode());
                myinfoEditCellPhoneCountryCode.setText(loginData.getCountryCode());

                myinfoEditCellPhoneCountryCode.setText(loginData.getCellCountryCode());
                myinfoEditCellPhoneNumber.setText(loginData.getCellPhoneno());

                /*myinfoEditCellPhoneCountryCode.setText(s);
                myinfoEditCountryCode.setText(s);*/
            } else if (loginData.getRole().equalsIgnoreCase("V")) {
                myinfoEditWebsite.setVisibility(View.GONE);
                myinfotextWebsite.setVisibility(View.GONE);
                myinfoEditBranch.setVisibility(View.GONE);
                myInfoTextBranch.setVisibility(View.GONE);
                myInfoLayoutPhone.setVisibility(View.GONE);
                myInfotextPhone.setVisibility(View.GONE);
                myinfoTextName.setVisibility(View.GONE);
                myinfoEditName.setVisibility(View.GONE);

                myinfoEditFirstName.setVisibility(View.VISIBLE);
                myinfoTextFirstname.setVisibility(View.VISIBLE);
                myinfoEditLastName.setVisibility(View.VISIBLE);
                myinfoTextLastname.setVisibility(View.VISIBLE);

                myinfoEditFirstName.setText(loginData.getFirstName());
                myinfoEditLastName.setText(loginData.getLastName());


                myinfoEditCellPhoneNumber.setText(loginData.getPhoneNo());
                myinfoEditCellPhoneCountryCode.setText(loginData.getCountryCode());
            }
            /*USERNAME, EMAIL ,BUILDING NO, STREET ,CITY, COUNTRY ,CELL PHONE ,BIO*/
            myinfoEdituserName.setText(loginData.getUsername());
            myinfoEditEmail.setText(loginData.getEmail());
            myinfoEditBuildingNumber.setText(loginData.getBuildingNo());


            for (int i = 0; i < cityList.size(); i++) {
                if (cityList.get(i).getId().equals(loginData.getCity())) {
                    myinfoEditCity.setText(cityList.get(i).getName());
                    String cityGson = new Gson().toJson(cityList.get(i));
                    city = new Gson().fromJson(cityGson, City.class);
                    break;
                }
            }

            for (int i = 0; i < countryList.size(); i++) {
                if (countryList.get(i).getId().equals(loginData.getCountry())) {
                    myinfoEditCountry.setText(countryList.get(i).getCountry());
                    String countryGson = new Gson().toJson(countryList.get(i));
                    country = new Gson().fromJson(countryGson, Country.class);
                    break;
                }
            }

            myinfoEditStreet.setText(loginData.getStreet());

            myinfoEditBio.setText(loginData.getBio());

            ((MainActivity) getActivity()).setCircularImage(editProfileImage, loginData.getProfileImageThumb());
            if (!loginData.getCoverImage().equalsIgnoreCase(""))
                Picasso.with(getActivity()).load(loginData.getCoverImage()).transform(new ColorFilterTransformation(getResources().getColor(R.color.transparent_blue))).into(myInfoBackTimelineImage);
            //((MainActivity) getActivity()).setImage(loginData.getCoverImage(), myInfoBackTimelineImage);
        }
        //codePicker = new CountryCodePicker(getContext());
        myinfoEditBio.setMovementMethod(new ScrollingMovementMethod());

     /*myinfoEditStreet.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    ((MainActivity) getActivity()).hideKeyboard();
                    if (myinfoEditStreet.getText().length() <= 1)
                        myInfoEditCountryClicked();

                }
                return true;
            }
        });*/


        /*myinfoEditStreet.setAdapter(new GooglePlacesAutocompleteAdapter(getContext()
                , myinfoEditStreet
                , R.layout.raw_autocomplete_text));*/
        placeAdapter = new PlaceAutocompleteAdapter(getContext(), R.layout.raw_autocomplete_text);
        myinfoEditStreet.setAdapter(placeAdapter);

        myinfoEditStreet.setThreshold(1);

        myinfoEditBio.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.myinfoEditBio) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        return view;
    }


    @OnClick(R.id.myinfoEditCity)
    public void myInfoEditCity() {
        ((MainActivity) getActivity()).hideKeyboard();
        int[] location = new int[2];
        myinfoEditCity.getLocationOnScreen(location);
        cityDropDown = new CityDropDownSeller(getContext(), myinfoEditCity.getHeight(), location, cityList, new ItemEventListener<String>() {
            @Override
            public void onItemEventFired(String integer, String t2, String actualPos) {
                city = new Gson().fromJson(t2, City.class);
                if (Locale.getDefault().getDisplayLanguage().equals("العربية"))
                    myinfoEditCity.setText(city.getArName());
                else
                    myinfoEditCity.setText(city.getName());


            }
        });
        cityDropDown.show();
    }

    @OnClick(R.id.myinfoEditBranch)
    public void myinfoBranchClicked() {
        ((MainActivity) getActivity()).hideKeyboard();
        int[] location = new int[2];
        myinfoEditBranch.getLocationOnScreen(location);
        branchDropDown = new BranchDropDown(getContext(), myinfoEditBranch.getHeight(), location, branchList, true, new ItemEventListener<Integer>() {
            @Override
            public void onItemEventFired(Integer integer, Integer t2, String actualPos) {
                myinfoEditBranch.setText(branchList.get(integer));
            }
        });
        branchDropDown.show();
    }

    @OnClick(R.id.myinfoEditCountry)
    public void myInfoEditCountryClicked() {

        CountryDialog.openCountryCodeDialog(getContext(), new ItemEventListener<String>() {
            @Override
            public void onItemEventFired(String s, String t2, String actualPos) {
                Debugger.e(s + " " + t2);
                country = new Gson().fromJson(t2, Country.class);
                Debugger.e("COUNTRY :::" + country.toString());
                myinfoEditCountry.setText(country.getCountry());
                myinfoEditCellPhoneCountryCode.setText(s);
                myinfoEditCountryCode.setText(s);
            }
        });
    }


    public boolean isValid() {
        if (loginData.getRole().equalsIgnoreCase("S")) {
            if (myinfoEditName.getText().toString().trim().length() == 0) {
                ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterName));
                myinfoEditName.requestFocus();
                return false;
            }


            if (myinfoEditPhoneNumber.getText().toString().trim().length() > 0) {
                if (myinfoEditPhoneNumber.getText().toString().length() < 9) {
                    ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterValidPhoneNumber));
                    myinfoEditPhoneNumber.requestFocus();
                    return false;
                }
            }

        } else if (loginData.getRole().equalsIgnoreCase("V")) {
            if (myinfoEditFirstName.getText().toString().trim().length() == 0) {
                ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterFirstName));
                myinfoEditFirstName.requestFocus();
                return false;
            }


            if (myinfoEditLastName.getText().toString().trim().length() == 0) {
                ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterLastName));
                myinfoEditLastName.requestFocus();
                return false;
            }

        }

        /*USERNAME, EMAIL ,BUILDING NO, STREET ,CITY, COUNTRY ,CELL PHONE ,BIO*/
        if (!validEmail(myinfoEditEmail.getText().toString())) {
            myinfoEditEmail.requestFocus();
            return false;
        }

        if (!validWebsite(myinfoEditWebsite.getText().toString())) {
            myinfoEditWebsite.requestFocus();
            return false;
        }

        if (myinfoEditBuildingNumber.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterBuildingNumber));
            myinfoEditBuildingNumber.requestFocus();
            return false;
        }


        if (myinfoEditStreet.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterStreet));
            myinfoEditStreet.requestFocus();
            return false;
        }

        if (myinfoEditCity.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterCity));
            return false;
        }

        if (myinfoEditCountry.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterCountry));
            return false;
        }


        if (myinfoEditCellPhoneNumber.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterMobileNumber));
            myinfoEditCellPhoneNumber.requestFocus();
            return false;
        }


        if (myinfoEditCellPhoneNumber.getText().toString().length() < 9) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterValidMobileNumber));
            myinfoEditCellPhoneNumber.requestFocus();
            return false;
        }
        return true;
    }


    @OnClick(R.id.timeLineProfileCamera)
    public void timeLineCameraImageClicked() {
        ImageChooserDialog imageChooserDialog = new ImageChooserDialog();
        imageChooserDialog.show(getActivity().getSupportFragmentManager(), "imageChooserDialog");
        imageChooserDialog.setCallback(new ImageChooserListener() {

                                           @Override
                                           public void getResultFromCamera(String result) {
                                               timeLineImage = new File(result);
                                               new Handler().postDelayed(new Runnable() {
                                                   @Override
                                                   public void run() {
                                                       Picasso.with(getActivity()).load(timeLineImage).transform(new ColorFilterTransformation(getResources().getColor(R.color.transparent_blue))).into(myInfoBackTimelineImage);
                                                   }
                                               }, 50);

                                           }

                                           @Override
                                           public void getResultFromGallery(String result) {
                                               if (result != null) {
                                                   if (!(result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("jpeg") ||
                                                           result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("jpg") ||
                                                           result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("png") ||
                                                           result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("bmp"))) {
                                                       //isInvalidImage = true;
                                                       ((MainActivity) getActivity()).messageAlert(getString(R.string.message_thisImageIsNotSupported));

                                                   } else {
                                                       timeLineImage = new File(result);
                                                       new Handler().postDelayed(new Runnable() {
                                                           @Override
                                                           public void run() {
                                                               Picasso.with(getActivity()).load(timeLineImage).transform(new ColorFilterTransformation(getResources().getColor(R.color.transparent_blue))).into(myInfoBackTimelineImage);

                                                           }
                                                       }, 50);
                                                   }
                                               }
                                           }

                                           @Override
                                           public void nothingSet() {

                                           }
                                       }

        );
    }

    @OnClick(R.id.editProfileImage)
    public void editProfileImageClicked() {
        ImageChooserDialog imageChooserDialog = new ImageChooserDialog();
        imageChooserDialog.show(getActivity().getSupportFragmentManager(), "imageChooserDialog");
        imageChooserDialog.setCallback(new ImageChooserListener() {
            @Override
            public void getResultFromCamera(String result) {
                editImage = new File(result);
                ((MainActivity) getActivity()).setCircularImage(editImage, editProfileImage);
            }

            @Override
            public void getResultFromGallery(String result) {
                if (result != null) {
                    if (!(result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("jpeg") ||
                            result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("jpg") ||
                            result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("png") ||
                            result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("bmp"))) {
                        //isInvalidImage = true;
                        ((MainActivity) getActivity()).messageAlert(getString(R.string.message_thisImageIsNotSupported));

                    } else {
                        editImage = new File(result);
                        ((MainActivity) getActivity()).setCircularImage(editImage, editProfileImage);
                        //layoutAddItemImageViewAdd.setBackgroundColor(getResources().getColor(R.color.backColor));
                        //isInvalidImage = false;
                    }
                }
            }

            @Override
            public void nothingSet() {

            }
        });
    }


    public boolean validWebsite(String webSite) {
        int length = webSite.trim().length();
       /* if (length <= 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterWebsite));
            return false;
        }*/
        if (length > 0) {
            matcher = pattern.matcher(webSite);

                Pattern pattern = Pattern.compile(Navigation.WEBSITE_REGEX);
                Matcher matcher = pattern.matcher(webSite);
                if (!(webSite.charAt(0) + "").matches("\\p{L}") || !matcher.matches()) {
                    ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterValidWEbsite));
                    return false;
                }
        }
        return true;
    }


    public boolean validEmail(String email) {
        int length = email.trim().length();
        if (length <= 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterEmail));
            return false;
        }

        // Patterns.WEB_URL.matcher(potentialUrl).matches()
        Pattern pattern = Pattern.compile(Navigation.EMAIL_REGEX);
        Matcher matcher = pattern.matcher(email);
        if (!(email.charAt(0) + "").matches("\\p{L}") || !matcher.matches()) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterValidEmailId));
            return false;
        }
        return true;
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle(getString(R.string.title_editProfile), Enum.setNevigationIcon.BACK_SAVE, true);
    }

    public void editProfile() {
        if (myinfoEditStreet.getText().toString().equals(loginData.getStreet())) {
            Double lat, lng;
            if (loginData.getLatitude() != null) {
                lat = Double.parseDouble(loginData.getLatitude());
                lng = Double.parseDouble(loginData.getLongitude());
            } else {
                lat = 0d;
                lng = 0d;
            }
            callApi(lat, lng);
        } else {
            getLocation();
        }
    }


    public void getLocation() {
        new GetRequestHelper<Address>().pingToRequest("http://maps.googleapis.com/maps/api/geocode/json?address=" +
                        myinfoEditStreet.getText().toString().replace("\"","") + "&sensor=true"
                , new ArrayList<KeyValuePair>()
                , new ServiceParameter()
                , new Address()
                , new TaskCompleteListener<Address>() {
                    @Override
                    public void onSuccess(Address mObject) {
                        try {
                            Debugger.e("GET LOCATION:::" + mObject);
                            if (mObject.getStatus().equalsIgnoreCase("ok")) {
                                lat = mObject.getResults().get(0).getGeometry().getLocation().getLat();
                                lng = mObject.getResults().get(0).getGeometry().getLocation().getLng();
                                callApi(lat, lng);
                            } else {
                                ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterValidLocation));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, Address address) {

                    }
                });
    }

    public void callApi(Double lat, Double lng) {
        if (((MainActivity) getActivity()).getData(getContext()).getData().getRole().equalsIgnoreCase("S")) {
            sallerEdit(lat, lng);
        } else if (((MainActivity) getActivity()).getData(getContext()).getData().getRole().equalsIgnoreCase("V")) {
            visitorEdit(lat, lng);
        }
    }


    public void sallerEdit(Double lat, Double lng) {
        if (getActivity() != null)
            ((MainActivity) getActivity()).startLoader();

        List<KeyValuePair> keyValuePair = new ArrayList<>();
        if (timeLineImage != null)
            keyValuePair.add(new KeyValuePair("cover_image", timeLineImage.getAbsolutePath()));

        if (editImage != null)
            keyValuePair.add(new KeyValuePair("profile_image", editImage.getAbsolutePath()));


        new PostRequestHelper<LoginWrapper>().pingToRequest(Constants.SELLER_EDIT
                , "image/*"
                , keyValuePair
                , new RequestParameter().getHashMapForEditSeller(getHashMapSeller(lat, lng))
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new LoginWrapper()
                , new TaskCompleteListener<LoginWrapper>() {
                    @Override
                    public void onSuccess(LoginWrapper loginWrapper) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        if (loginWrapper.getStatus() == 1) {
                            ((MainActivity) getActivity()).setData(getContext(), new Gson().toJson(loginWrapper));
                            ((MainActivity) getActivity()).messageAlert(loginWrapper.getMessage());
                            getActivity().onBackPressed();
                        } else {
                            ((MainActivity) getActivity()).messageAlert(loginWrapper.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, LoginWrapper loginWrapper) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();
                    }
                });
    }
     /*first_name,username,email,phone_no,building_no,street,city,country,country_code,device_type,device_token,latitude,longitude,branch_name
        website,bio,cell_country_code,cell_phoneno,cover_image,profile_image*/


    public HashMap<String, String> getHashMapSeller(Double lat, Double lng) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("first_name", myinfoEditName.getText().toString().trim());
        hashMap.put("username", myinfoEdituserName.getText().toString().trim());
        hashMap.put("email", myinfoEditEmail.getText().toString().trim());
        hashMap.put("cell_phoneno", myinfoEditCellPhoneNumber.getText().toString().trim());
        hashMap.put("cell_country_code", myinfoEditCellPhoneCountryCode.getText().toString());
        if (myinfoEditCellPhoneNumber.getText().length() > 0) {
            hashMap.put("phone_no", myinfoEditPhoneNumber.getText().toString().trim());
            hashMap.put("country_code", myinfoEditCountryCode.getText().toString().trim());
        }
        hashMap.put("building_no", myinfoEditBuildingNumber.getText().toString().trim());
        hashMap.put("street", myinfoEditStreet.getText().toString().trim());
        hashMap.put("city", city.getId());
        hashMap.put("country", country.getId());
        hashMap.put("device_token", ((MainActivity) getActivity()).getDeviceId());
        hashMap.put("latitude", String.valueOf(lat));
        hashMap.put("longitude", String.valueOf(lng));
        hashMap.put("branch_name", myinfoEditBranch.getText().toString());
        hashMap.put("website", myinfoEditWebsite.getText().toString().trim());
        hashMap.put("bio", myinfoEditBio.getText().toString().trim());


        return hashMap;
    }

    public void visitorEdit(Double lat, Double lng) {
        if (getActivity() != null)
            ((MainActivity) getActivity()).startLoader();

        List<KeyValuePair> keyValuePairs = new ArrayList<>();
        if (timeLineImage != null)
            keyValuePairs.add(new KeyValuePair("cover_image", timeLineImage.getAbsolutePath()));

        if (editImage != null)
            keyValuePairs.add(new KeyValuePair("profile_image", editImage.getAbsolutePath()));

        new PostRequestHelper<LoginWrapper>().pingToRequest(Constants.VISITOR_EDIT
                , "image/*"
                , keyValuePairs
                , new RequestParameter().getHashMapVisitorEdit(getHashMapVisitorEdit(lat, lng))
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new LoginWrapper()
                , new TaskCompleteListener<LoginWrapper>() {
                    @Override
                    public void onSuccess(LoginWrapper mObject) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        if (mObject.getStatus() == 1) {
                            ((MainActivity) getActivity()).messageAlert(mObject.getMessage());
                            ((MainActivity) getActivity()).setData(getContext(), new Gson().toJson(mObject));


                            getActivity().onBackPressed();
                        } else {
                            ((MainActivity) getActivity()).messageAlert(mObject.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, LoginWrapper loginWrapper) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();
                    }
                });
    }

    /*first_name,last_name,username,email,phone_no,building_no,street,city,country,country_code,device_type,device_token,latitude,longitude
* bio,cover_image,profile_image	*/
    public HashMap<String, String> getHashMapVisitorEdit(Double lat, Double lng) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("first_name", myinfoEditFirstName.getText().toString().trim());
        hashMap.put("last_name", myinfoEditLastName.getText().toString().trim());
        hashMap.put("username", myinfoEdituserName.getText().toString().trim());
        hashMap.put("email", myinfoEditEmail.getText().toString().trim());
        hashMap.put("password", myinfoEditPassword.getText().toString().trim());
        hashMap.put("phone_no", myinfoEditCellPhoneNumber.getText().toString().trim());
        hashMap.put("building_no", myinfoEditBuildingNumber.getText().toString().trim());
        hashMap.put("street", myinfoEditStreet.getText().toString().trim());
        hashMap.put("city", city.getId());
        hashMap.put("country", country.getId());
        hashMap.put("country_code", myinfoEditCellPhoneCountryCode.getText().toString());
        hashMap.put("device_token", ((MainActivity) getActivity()).getDeviceId());
        hashMap.put("latitude", String.valueOf(lat));
        hashMap.put("longitude", String.valueOf(lng));
        hashMap.put("bio", myinfoEditBio.getText().toString().trim());

        return hashMap;
    }


}
