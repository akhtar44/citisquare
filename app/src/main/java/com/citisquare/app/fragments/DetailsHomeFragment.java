package com.citisquare.app.fragments;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.controls.CustomButton;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.Rendering;
import com.citisquare.app.pojo.response.DetailsData;
import com.citisquare.app.pojo.response.DetailsWrapper;
import com.citisquare.app.pojo.response.Like;
import com.citisquare.app.pojo.response.LoginData;
import com.citisquare.app.pojo.response.NotificationData;
import com.citisquare.app.pojo.response.OfferData;
import com.citisquare.app.utils.Debugger;
import com.citisquare.app.utils.RequestParameter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 6/6/16.
 */
public class DetailsHomeFragment extends BaseFragment {

    OfferData offerData;
    String bundleFlag;
    DetailsData detailsData;
    Bundle bundle;
    NotificationData notificationData;
    @BindView(R.id.quickInfoDetilsUserLogo)
    ImageView quickInfoDetilsUserLogo;
    @BindView(R.id.homeDetailsUsername)
    CustomTextView homeDetailsUsername;
    @BindView(R.id.homeDetailsLocation)
    CustomTextView homeDetailsLocation;
    @BindView(R.id.homeDetailsDate)
    CustomTextView homeDetailsDate;
    @BindView(R.id.followDetailsQuickInfo)
    CustomButton followDetailsQuickInfo;
    @BindView(R.id.topQuickLayout)
    LinearLayout topQuickLayout;
    @BindView(R.id.quickImage)
    ImageView quickImage;
    @BindView(R.id.videoDetails)
    ImageView videoDetails;
    @BindView(R.id.quickInfoDescription)
    CustomTextView quickInfoDescription;
    @BindView(R.id.commentDetailsQuickInfo)
    TextView commentDetailsQuickInfo;
    @BindView(R.id.likeQuickInfo)
    CustomTextView likeDetailsQuickInfo;
    @BindView(R.id.shareDetailsQuickInfo)
    CustomTextView shareDetailsQuickInfo;

    @BindView(R.id.likeImageDetails)
    ImageView likeImageDetails;
    @BindView(R.id.relativeLayoutLike)
    RelativeLayout relativeLayoutLike;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.quickinfo_details, container, false);
        ButterKnife.bind(this, view);
        bundle = getArguments();


        if (bundle != null) {
            if (bundle.getString("details", "").equals("News")) {
                followDetailsQuickInfo.setVisibility(View.GONE);
                bundleFlag = "T";
            } else if (bundle.getString("details", "").equals("Offer")) {
                followDetailsQuickInfo.setVisibility(View.VISIBLE);
                bundleFlag = "O";
            } else if (bundle.getString("details", "").equals("Home")) {
                followDetailsQuickInfo.setVisibility(View.VISIBLE);
                bundleFlag = "T";
            } else if (bundle.getString("details", "").equals("notification")) {
                if (bundle.getString("Notification") != null) {
                    notificationData = new Gson().fromJson(bundle.getString("Notification"), NotificationData.class);
                    bundleFlag = notificationData.getType();
                    callDetailsApi();
                }
            }

            if (bundle.getString("data") != null) {
                offerData = new Gson().fromJson(bundle.getString("data"), OfferData.class);
                callDetailsApi();
            }

        } else {
            followDetailsQuickInfo.setVisibility(View.VISIBLE);
        }
        return view;
    }

    public void callDetailsApi() {
        if (getActivity() != null) {
            ((MainActivity) getActivity()).startLoader();
        }

        new PostRequestHelper<DetailsWrapper>().pingToRequest(Constants.OFFER_DETAILS
                , null
                , null
                , new RequestParameter().getHashMapForDetails(getMapDetails())
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new DetailsWrapper()
                , new TaskCompleteListener<DetailsWrapper>() {
                    @Override
                    public void onSuccess(DetailsWrapper detailsWrapper) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();
                        }
                        if (detailsWrapper.getStatus() == 1) {
                            setData(detailsWrapper.getData());
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, DetailsWrapper detailsWrapper) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();
                        }
                    }
                });

    }

    @OnClick(R.id.homeDetailsLocation)
    public void homeDetailsLocationClicked() {
        if (getActivity() != null) {
            ((MainActivity) getActivity()).setProfileUserId(detailsData.getUserId());
            ((MainActivity) getActivity()).setFragment(new OtherMapPathFragment(), false, "");
        }
    }

    @OnClick(R.id.videoDetails)
    public void videoDetailsClicked() {
        if (detailsData != null) {
            PlayVideoFragment playVideoFragment = new PlayVideoFragment();
            Bundle playBundle = new Bundle();
            playBundle.putString("video", detailsData.getMediaName());
            playVideoFragment.setArguments(playBundle);
            ((MainActivity) getActivity()).setFragment(playVideoFragment, false, "");
        }
    }

    private void setData(DetailsData data) {
        detailsData = data;
        ((MainActivity) getActivity()).setCircularImage(quickInfoDetilsUserLogo, data.getProfileImageThumb());

        // String userName = data.getFirstName() + data.getLastName();
        homeDetailsUsername.setText(data.getCompanyName());
        homeDetailsLocation.setText(data.getCityName());

        DateFormat utcFormat = new SimpleDateFormat(Constants.WS_DATE);
        utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date date = null;
        try {
            date = utcFormat.parse(data.getInsertdate());
        } catch (Exception e) {
            e.printStackTrace();
        }

        DateFormat pstFormat = new SimpleDateFormat(Constants.DATE_DISPLAY);
        pstFormat.setTimeZone(TimeZone.getDefault());
        homeDetailsDate.setText(pstFormat.format(date));


        if (data.getMediaType().equalsIgnoreCase("")) {
            quickImage.setVisibility(View.GONE);
            videoDetails.setVisibility(View.GONE);
        } else if (data.getMediaType().equalsIgnoreCase("V")) {
            quickImage.setVisibility(View.VISIBLE);
            videoDetails.setVisibility(View.VISIBLE);
        } else if (data.getMediaType().equalsIgnoreCase("P")) {
            quickImage.setVisibility(View.VISIBLE);
            videoDetails.setVisibility(View.INVISIBLE);
        }
        ((MainActivity) getActivity()).setImage(data.getMediaNameThumb(), quickImage);

        quickInfoDescription.setText(data.getDescription());

        likeDetailsQuickInfo.setText(String.valueOf(data.getLikeCount()));


        commentDetailsQuickInfo.setText(String.valueOf(data.getCommentCount()));


        if (data.getUserId().equals(((MainActivity) getActivity()).getData(getContext()).getData().getId())) {
            followDetailsQuickInfo.setVisibility(View.GONE);
        } else {
            followDetailsQuickInfo.setVisibility(View.VISIBLE);
            if (detailsData.getIsFollowing() == 1) {
                followDetailsQuickInfo.setText(getContext().getString(R.string.following));
                followDetailsQuickInfo.setTextColor(Color.WHITE);
                followDetailsQuickInfo.setBackgroundColor(getContext().getResources().getColor(R.color.colorPrimary));
            } else {
                followDetailsQuickInfo.setText(getContext().getString(R.string.FOLLOW));
                followDetailsQuickInfo.setTextColor(getContext().getResources().getColor(R.color.colorPrimary));
                followDetailsQuickInfo.setBackground(getContext().getResources().getDrawable(R.drawable.border_blue));
            }
        }
    }

    private HashMap<String, String> getMapDetails() {
        HashMap<String, String> hashMapDetails = new HashMap<>();
        if (offerData != null) {
            hashMapDetails.put("user_id", offerData.getUserId());
            hashMapDetails.put("post_id", offerData.getId());
        } else if (notificationData != null) {
            LoginData get = ((MainActivity) getActivity()).getData(getContext()).getData();
            hashMapDetails.put("user_id", get.getId());
            hashMapDetails.put("post_id", notificationData.getPostId());
        }
        hashMapDetails.put("flag", bundleFlag);
        hashMapDetails.put("city_id", ((MainActivity) getActivity()).getTopCitySelectionId());
        return hashMapDetails;
    }

    @OnClick(R.id.followDetailsQuickInfo)
    public void quickInfoDetailsClicked() {

        if (((MainActivity) getActivity()).getUserForWholeApp().equals(Enum.setUser.GUEST)) {
            ((MainActivity) getActivity()).messageAlert(getContext().getString(R.string.message_Guest));
        } else if (((MainActivity) getActivity()).getUserForWholeApp().equals(Enum.setUser.SELLER_BASIC)) {
            ((MainActivity) getActivity()).messageAlert(getContext().getString(R.string.message_visitor));
        } else {

            if (detailsData.getIsFollowing() == 1) {
                ((MainActivity) getActivity()).alertTwoAction(getContext().getString(R.string.message_DoYouWantToUnFollow), new Rendering() {
                    @Override
                    public void response(boolean isCofirm) {
                        if (isCofirm) {
                            detailsData.setIsFollowing(0);
                            followDetailsQuickInfo.setText(getContext().getString(R.string.FOLLOW));
                            followDetailsQuickInfo.setTextColor(getContext().getResources().getColor(R.color.colorPrimary));
                            followDetailsQuickInfo.setBackground(getContext().getResources().getDrawable(R.drawable.border_blue));
                            doFollowApiCall(detailsData.getUserId(), "unfollow");
                        }
                    }
                });
            } else {
                detailsData.setIsFollowing(1);
                followDetailsQuickInfo.setTextColor(Color.WHITE);
                followDetailsQuickInfo.setBackgroundColor(getContext().getResources().getColor(R.color.colorPrimary));
                doFollowApiCall(detailsData.getUserId(), "follow");
            }
        }
    }

    public void doFollowApiCall(final String userId, final String status) {
        new PostRequestHelper<Like>().pingToRequest(Constants.DO_FOLLOW
                , null
                , null
                , new RequestParameter().getHashMapForDoFollow(getDoFollowMap(userId, status))
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new Like()
                , new TaskCompleteListener<Like>() {
                    @Override
                    public void onSuccess(Like mObject) {
                        if (mObject.getStatus() == 1)
                            Debugger.e("FOLLOW SUCCESSFULLY");
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, Like like) {
                        if (like != null) {
                            if (like.getStatus() == -1)
                                ((MainActivity) getActivity()).invalidToken();
                        }
                    }
                });
    }

    public HashMap<String, String> getDoFollowMap(String userId, String status) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("follower_user_id", userId);
        if (status.equals("follow"))
            hashMap.put("status", "1");
        else
            hashMap.put("status", "0");

        return hashMap;
    }

    @OnClick(R.id.topQuickLayout)
    public void topQuickLayout() {
        if (detailsData.getUserId().equals(((MainActivity) getActivity()).userId())) {
            ProfileFragment profileFragment = new ProfileFragment();
            Bundle bundle = new Bundle();
            bundle.putString("profile", "own");
            ((MainActivity) getActivity()).setProfileUserId(detailsData.getUserId());
            profileFragment.setArguments(bundle);
            ((MainActivity) getActivity()).setSelectionPostion(0);
            ((MainActivity) getActivity()).setFragment(profileFragment, false, "");
        } else {
            ProfileFragment profileFragment = new ProfileFragment();
            Bundle bundle = new Bundle();
            bundle.putString("profile", "other");
            ((MainActivity) getActivity()).setProfileUserId(detailsData.getUserId());
            profileFragment.setArguments(bundle);
            ((MainActivity) getActivity()).setSelectionPostion(0);
            ((MainActivity) getActivity()).setFragment(profileFragment, false, "");
            //childFragmentReplacement(profileFragment, "child");
        }
    }


    @OnClick(R.id.shareDetailsQuickInfo)
    public void shareButtonClicked() {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, detailsData.getDescription() + "  " + detailsData.getUrl());
        sharingIntent.setType("text/plain");
        startActivity(Intent.createChooser(sharingIntent, "Share application using"));
    }

    @OnClick(R.id.relativeLayoutLike)
    public void likeClicked() {
        if (((MainActivity) getActivity()).getUserForWholeApp().equals(Enum.setUser.GUEST)) {
            ((MainActivity) getActivity()).messageAlert(getContext().getString(R.string.message_Guest));
        } else {
            if (detailsData.getIsLike() == 0) {

                Animator zoomAnimation = (AnimatorSet) AnimatorInflater.loadAnimator(getContext(), R.animator.zoom_in_out);
                zoomAnimation.setTarget(likeImageDetails);
                zoomAnimation.start();

                detailsData.setIsLike(1);
                detailsData.setLikeCount(detailsData.getLikeCount() + 1);
                likeDetailsQuickInfo.setText(detailsData.getLikeCount() + "");
                likeApiCall(detailsData.getId());
            }
        }
    }

    @OnClick(R.id.commentDetailsQuickInfo)
    public void commentClicked() {
        if (((MainActivity) getActivity()).getUserForWholeApp().equals(Enum.setUser.GUEST)) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_Guest));
        } else {
            CommentFragment commentFragment = new CommentFragment();

            commentFragment.setCommentListener(new CommentFragment.CommentListener() {
                @Override
                public void commentCount(int count) {
                    detailsData.setCommentCount(count);
                    commentDetailsQuickInfo.setText(String.valueOf(detailsData.getCommentCount()));
                }
            });

            Bundle bundleComment = new Bundle();
            bundleComment.putString("postId", detailsData.getId());
            bundleComment.putString("FROM", bundleFlag);
            commentFragment.setArguments(bundleComment);
            ((MainActivity) getActivity()).setFragment(commentFragment, false, "");
        }
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle(getString(R.string.title_moreDetails), Enum.setNevigationIcon.BACK, true);
    }

    public void likeApiCall(String postId) {
        new PostRequestHelper<Like>().pingToRequest(Constants.LIKE
                , null
                , null
                , new RequestParameter().getHashMapLike(getLikeHashMap(postId))
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new Like()
                , new TaskCompleteListener<Like>() {
                    @Override
                    public void onSuccess(Like mObject) {
                        if (mObject.getStatus() == 1)
                            Debugger.e("LIKE");
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, Like like) {
                        if (like != null) {
                            if (like.getStatus() == -1)
                                ((MainActivity) getActivity()).invalidToken();
                        }
                    }
                });
    }

    public HashMap<String, String> getLikeHashMap(String postId) {
        HashMap<String, String> likeHashMap = new HashMap<>();
        likeHashMap.put("post_id", postId);
        likeHashMap.put("flag", bundleFlag);
        likeHashMap.put("status", "1");
        return likeHashMap;
    }

}
