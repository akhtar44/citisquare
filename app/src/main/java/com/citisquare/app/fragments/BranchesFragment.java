package com.citisquare.app.fragments;

import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.BranchAdapter;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.interfaces.LocationCallback;
import com.citisquare.app.pojo.response.AddBranchData;
import com.citisquare.app.pojo.response.BranchData;
import com.citisquare.app.pojo.response.BranchWrapper;
import com.citisquare.app.pojo.response.LoginWrapper;
import com.citisquare.app.utils.DataToPref;
import com.citisquare.app.utils.LocationHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import serviceCalling.builder.KeyValuePair;
import serviceCalling.helper.GetRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 16/8/16.
 */
public class BranchesFragment extends BaseFragment {
    @BindView(R.id.branchRecyclerView)
    RecyclerView branchRecyclerView;

    BranchAdapter branchAdapter;
    ArrayList<BranchData> branchDatas;
    int pageNo = 1;
    boolean isPagination;
    Bundle bundle;
    GridLayoutManager gridLayoutManager;

    private LocationCallback locationCallback;
    private LocationHelper locationHelper;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_branches, container, false);
        ButterKnife.bind(this, view);

        bundle = getArguments();
        branchDatas = new ArrayList<>();

        branchAdapter = new BranchAdapter(getContext(), branchDatas, new ItemEventListener<String>() {
            @Override
            public void onItemEventFired(String pos, final String t2, String actualPos) {
                switch (pos) {
                    case "LOCATION":
                        ((MainActivity) getActivity()).setFragment(new OtherMapPathFragment(), false, "");
                        break;
                    case "EDIT":
                        AddBranchFragment addBranchFragment = new AddBranchFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("Branch", "EDIT");
                        bundle.putString("data", t2);
                        addBranchFragment.setArguments(bundle);
                        ((MainActivity) getActivity()).setFragment(addBranchFragment, true, "");
                        break;
                    case "DELETE":
                        deleteBranch(new Gson().fromJson(t2, AddBranchData.class));
                        break;
                }
            }
        });

        gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        branchRecyclerView.setLayoutManager(gridLayoutManager);
        branchRecyclerView.setAdapter(branchAdapter);

        /*branchRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (isPagination) {
                    int visibleItemCount = recyclerView.getChildCount();
                    int totalItemCount = gridLayoutManager.getItemCount();
                    int firstVisibleItemPosition = gridLayoutManager.findFirstVisibleItemPosition();
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount) {
                        isPagination = false;
                        //              branchApiCall(++pageNo);
                    }
                }
            }
        });*/


        //branchApiCall(pageNo);

        return view;
    }


    private void startLisning() {
        locationHelper = new LocationHelper(this, new LocationHelper.ProvideLocation() {
            @Override
            public void currentLocation(final Location location) {
                locationCallback.onLocationRecive(location);
            }

            @Override
            public void onError() {
                locationCallback.onLocationError();
            }
        });
    }

    private void deleteBranch(AddBranchData addBranchData) {
        if (getActivity() != null) {
            ((MainActivity) getActivity()).startLoader();
        }

        if (addBranchData != null) {
            List<KeyValuePair> keyValuePairsBranch = new ArrayList<>();
            keyValuePairsBranch.add(new KeyValuePair("branch_id", addBranchData.getId()));


            new GetRequestHelper<LoginWrapper>().pingToRequest(Constants.DELETE_BRANCH
                    , keyValuePairsBranch
                    , ((MainActivity) getActivity()).getParamWholeParameters()
                    , new LoginWrapper()
                    , new TaskCompleteListener<LoginWrapper>() {
                        @Override
                        public void onSuccess(LoginWrapper mObject) {

                            if (mObject.getStatus() == 1) {
                                setPage();
                            }
                        }

                        @Override
                        public void onFailure(ExceptionType exceptions, LoginWrapper loginWrapper) {

                        }
                    });
        }
    }

    private void setPage() {
        pageNo = 1;
        branchApiCall(pageNo);
    }


    public void branchApiCall(final int pageNo) {
        if (getActivity() != null)
            ((MainActivity) getActivity()).startLoader();

        final List<KeyValuePair> branchKeyValuePair = new ArrayList<>();
        branchKeyValuePair.add(new KeyValuePair("user_id", ((MainActivity) getActivity()).userId()));

        //new GetRequestHelper().pingToRequest(Constants.BRANCH_LIST, "image/*", branchKeyValuePair ,((MainActivity)getActivity()).getHeader(),new BranchWrapper(),new TaskCompleteListener<>());
        new GetRequestHelper<BranchWrapper>().pingToRequest(Constants.BRANCH_LIST
                , branchKeyValuePair
                , ((MainActivity) getActivity()).getHeader()
                , new BranchWrapper()
                , new TaskCompleteListener<BranchWrapper>() {
                    @Override
                    public void onSuccess(BranchWrapper branchWrapper) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();
                            isPagination = true;
                            DataToPref.setData(getContext(), "branch_price", branchWrapper.getPrice());


                            if (pageNo == 1)
                                branchDatas.clear();

                            if (branchWrapper.getStatus() == 1)
                                setBranchAdapter(branchWrapper.getData());
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, BranchWrapper branchWrapper) {
                        if (getActivity() != null) {
                            isPagination = false;
                            ((MainActivity) getActivity()).stopLoader();


                            if (branchWrapper != null) {
                                DataToPref.setData(getContext(), "branch_price", branchWrapper.getPrice());
                                if (pageNo == 1)
                                    branchDatas.clear();
                                branchAdapter.notifyDataSetChanged();

                            /*if (exceptions == ExceptionType.ParameterException) {
                                if (pageNo == 1)
                                    ((MainActivity) getActivity()).messageAlert(branchWrapper.getMessage());
                            }*/
                            }
                        }
                    }
                });
    }

    public void setBranchAdapter(List<BranchData> branchList) {
        branchDatas.addAll(branchList);
        branchAdapter.notifyDataSetChanged();
    }


    @Override
    public void onResume() {
        super.onResume();
        pageNo = 1;
        branchApiCall(pageNo);
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle(getString(R.string.branches), Enum.setNevigationIcon.BACK_BRANCH, true);
    }
}