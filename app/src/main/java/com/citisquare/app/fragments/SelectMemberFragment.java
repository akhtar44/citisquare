package com.citisquare.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.SelectMemberAdapter;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.pojo.response.FollowerData;
import com.citisquare.app.pojo.response.FollowersWrapper;
import com.citisquare.app.utils.Debugger;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import serviceCalling.builder.KeyValuePair;
import serviceCalling.helper.GetRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 16/7/16.
 */
public class SelectMemberFragment extends BaseFragment {

    @BindView(R.id.selectAll)
    TextView selectAll;
    @BindView(R.id.selectMemberListView)
    ListView selectMemberListView;

    SelectMemberAdapter selectMemberAdapter;
    List<FollowerData> selectMembersList;
    boolean click = true;
    int pageno = 1;
    Bundle bundle;
    boolean isPagination;
    String message = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_select_members, container, false);
        ButterKnife.bind(this, view);
        pageno = 1;
        selectMembersList = new ArrayList<>();


        selectMemberAdapter = new SelectMemberAdapter(getContext(), R.layout.raw_selectmember, selectMembersList);
        selectMemberListView.setAdapter(selectMemberAdapter);
        bundle = getArguments();
        if (bundle != null) {
            if (bundle.getString("message", "") != null) {
                message = bundle.getString("message");
            }
        }

        selectMemberListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (isPagination) {
                    if ((visibleItemCount + firstVisibleItem) >= totalItemCount) {
                        isPagination = false;
                        followersListing(++pageno);
                    }
                }
            }
        });

        followersListing(pageno);
        return view;
    }


    public String getMessage() {
        return message;
    }

    public void followersListing(final int pageno) {
        if (getActivity() != null && pageno == 1)
            ((MainActivity) getActivity()).startLoader();

        List<KeyValuePair> keyvaluePairs = new ArrayList<>();
        keyvaluePairs.add(new KeyValuePair("page", pageno + ""));

        new GetRequestHelper<FollowersWrapper>().pingToRequest(Constants.MEMBER_LIST
                , keyvaluePairs
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new FollowersWrapper()
                , new TaskCompleteListener<FollowersWrapper>() {
                    @Override
                    public void onSuccess(FollowersWrapper followersWrapper) {
                        isPagination = true;
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        if (pageno == 1)
                            selectMembersList.clear();

                        if (followersWrapper.getStatus() == 1)
                            setFollowersData(followersWrapper.getData());
                        else
                            ((MainActivity) getActivity()).messageAlert(followersWrapper.getMessage());
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, FollowersWrapper followersWrapper) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        if (followersWrapper != null) {
                            if (followersWrapper.getStatus() == -1)
                                ((MainActivity) getActivity()).invalidToken();

                            if (exceptions == ExceptionType.ParameterException) {
                                if (pageno == 1)
                                    ((MainActivity) getActivity()).messageAlert(followersWrapper.getMessage());
                            }
                        }
                    }
                });
    }


    public void setFollowersData(List<FollowerData> followersData) {
        selectMembersList.addAll(followersData);
        selectMemberAdapter.notifyDataSetChanged();
    }

    public ArrayList<String> selectedMembers() {
        Debugger.e("SELECTED MEMBER:::" + selectMemberAdapter.getReceiverId().toString());
        return selectMemberAdapter.getReceiverId();
    }


    @OnClick(R.id.selectAll)
    public void selectAllMemberClicked() {
        if (click) {
            Debugger.e("TRUE");
            for (int i = 0; i < selectMembersList.size(); i++) {
                selectMembersList.get(i).setSelect(true);
            }
            selectMemberAdapter.notifyDataSetChanged();
            click = false;
        } else {
            Debugger.e("FALSE");
            selectMemberAdapter.setCountZero();
            for (int i = 0; i < selectMembersList.size(); i++) {
                selectMembersList.get(i).setSelect(false);
            }
            selectMemberAdapter.notifyDataSetChanged();
            click = true;
        }
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle(getString(R.string.title_selectMembers), Enum.setNevigationIcon.BACK_SEND, true);
    }
}
 /*selectMembersList.add(new SelectMember("John Methews"));
        selectMembersList.add(new SelectMember("John Methews"));
        selectMembersList.add(new SelectMember("John Methews"));
        selectMembersList.add(new SelectMember("John Methews"));
        selectMembersList.add(new SelectMember("John Methews"));
        selectMembersList.add(new SelectMember("John Methews"));
        selectMembersList.add(new SelectMember("John Methews"));
        selectMembersList.add(new SelectMember("John Methews"));
        selectMembersList.add(new SelectMember("John Methews"));

        selectMembersList.add(new SelectMember("John Methews"));
        selectMembersList.add(new SelectMember("John Methews"));
        selectMembersList.add(new SelectMember("John Methews"));
        selectMembersList.add(new SelectMember("John Methews"));
        selectMembersList.add(new SelectMember("John Methews"));
        selectMembersList.add(new SelectMember("John Methews"));
        selectMembersList.add(new SelectMember("John Methews"));
        selectMembersList.add(new SelectMember("John Methews"));
        selectMembersList.add(new SelectMember("John Methews"));

        selectMembersList.add(new SelectMember("John Methews"));
        selectMembersList.add(new SelectMember("John Methews"));
        selectMembersList.add(new SelectMember("John Methews"));
        selectMembersList.add(new SelectMember("John Methews"));
        selectMembersList.add(new SelectMember("John Methews"));
        selectMembersList.add(new SelectMember("John Methews"));
        selectMembersList.add(new SelectMember("John Methews"));
        selectMembersList.add(new SelectMember("John Methews"));
        selectMembersList.add(new SelectMember("John Methews"));*/

