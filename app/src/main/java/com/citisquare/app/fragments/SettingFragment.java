package com.citisquare.app.fragments;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.gson.Gson;
import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.pojo.CheckForUpdateData;
import com.citisquare.app.pojo.CheckForUpdateWrapper;
import com.citisquare.app.pojo.response.LoginData;
import com.citisquare.app.pojo.response.LoginWrapper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import serviceCalling.builder.KeyValuePair;
import serviceCalling.helper.GetRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 3/8/16.
 */
public class SettingFragment extends BaseFragment {

    @BindView(R.id.aboutUs)
    CustomTextView aboutUs;
    @BindView(R.id.notificationSettingImage)
    ImageView notificationSettingImage;
    @BindView(R.id.checkForUpdate)
    CustomTextView checkForUpdate;
    @BindView(R.id.aboutTheApp)
    CustomTextView aboutTheApp;
    @BindView(R.id.notificationLayout)
    LinearLayout notificationLayout;
    @BindView(R.id.shareTheApp)
    CustomTextView shareTheApp;
    private boolean notificationFlag;
    Intent intent;
    private AdView mBannerAd;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_setting, container, false);
        if (getActivity() != null)
            ((MainActivity) getActivity()).hideKeyboard();
        ButterKnife.bind(this, view);

        if (((MainActivity) getActivity()).getUserForWholeApp().equals(Enum.setUser.GUEST)) {
            notificationLayout.setVisibility(View.GONE);
        } else {
            notificationLayout.setVisibility(View.VISIBLE);
        }
        LoginWrapper loginWrapper = ((MainActivity) getActivity()).getData(getContext());
        LoginData loginData = loginWrapper.getData();
        if (loginData.getNotification() != null && loginData.getNotification().equalsIgnoreCase("1")) {
            notificationFlag = true;
            notificationSettingImage.setImageResource(R.drawable.on_switch);

        } else {
            notificationFlag = false;
            notificationSettingImage.setImageResource(R.drawable.offswitch);
        }
        MobileAds.initialize(getContext(), getContext().getString(R.string.admob_app_id));
        mBannerAd = (AdView) view.findViewById(R.id.adView);
        AdRequest request = new AdRequest.Builder().build();
      /*  AdRequest request = new AdRequest.Builder()
                .addTestDevice("5328A8875CFDE5D08E631C14615FDECC")  // An example device ID
                .build();*/
        mBannerAd.loadAd(request);
        return view;
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle("SETTINGS", Enum.setNevigationIcon.BACK, false);
    }

    @OnClick({R.id.aboutUs, R.id.notificationSettingImage, R.id.checkForUpdate, R.id.aboutTheApp, R.id.shareTheApp})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.aboutUs:
                openAboutUsFragment();
                break;
            case R.id.notificationSettingImage:

                if (notificationFlag) {
                    notificationFlag = false;
                    notificationSettingImage.setImageResource(R.drawable.offswitch);
                } else {
                    notificationFlag = true;
                    notificationSettingImage.setImageResource(R.drawable.on_switch);
                }
                notificationOnOffApiCall(notificationFlag);

                break;
            case R.id.checkForUpdate:
                checkForUpdateClicked();
                break;
            case R.id.aboutTheApp:
                aboutTheAppClicked();
                break;
            case R.id.shareTheApp:
                String PackageName = getActivity().getPackageName();
                intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT, "");
                intent.putExtra(Intent.EXTRA_TEXT, "I have been using CitiSquare app and its the best app out there. https://play.google.com/store/apps/details?id=" + PackageName);
                startActivity(Intent.createChooser(intent, "Share via"));
                break;
        }
    }

    private void aboutTheAppClicked() {
        ((MainActivity) getActivity()).setFragment(new AboutTheAppFragment(), false, "");

    }

    private void checkForUpdateClicked() {
        if (getActivity() != null) {
            ((MainActivity) getActivity()).startLoader();
        }
        new GetRequestHelper<CheckForUpdateWrapper>().pingToRequest(Constants.CHECK_FOR_UPDATE
                , new ArrayList<KeyValuePair>()
                , ((MainActivity) getActivity()).getHeader()
                , new CheckForUpdateWrapper()
                , new TaskCompleteListener<CheckForUpdateWrapper>() {
                    @Override
                    public void onSuccess(CheckForUpdateWrapper checkForUpdateWrapper) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        if (checkForUpdateWrapper.getStatus() == 1) {

                            setDataForMethod(checkForUpdateWrapper.getData());

                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, CheckForUpdateWrapper checkForUpdateWrapper) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();
                    }
                });
    }

    private void setDataForMethod(List<CheckForUpdateData> checkForUpdateData) {
        try {
            PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            int verCode = pInfo.versionCode;
            for (CheckForUpdateData forUpdateData : checkForUpdateData) {
                if (forUpdateData.getDeviceType().equalsIgnoreCase("A")) {
                    int updatedVersionCode = Integer.parseInt(forUpdateData.getVersion());
                    if (updatedVersionCode > verCode) {
                        moveToUpdate(forUpdateData);
                    } else {
                        ((MainActivity) getActivity()).messageAlert(getString(R.string.yourApplicationIsUpdated));
                    }
                }
            }

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void moveToUpdate(CheckForUpdateData forUpdateData) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(forUpdateData.getLink()));
        startActivity(i);
    }

    private void openAboutUsFragment() {
        ((MainActivity) getActivity()).setFragment(new AboutUsFragment(), false, "");
    }

    private void notificationOnOffApiCall(boolean notify) {
        if (getActivity() != null) {
            ((MainActivity) getActivity()).startLoader();
        }
        List<KeyValuePair> keyValuePairs = new ArrayList<>();
        keyValuePairs.add(new KeyValuePair("flag", notify ? "1" : "0"));
        new GetRequestHelper<LoginWrapper>().pingToRequest(Constants.NOTIFICATIO_ON_OFF
                , keyValuePairs
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new LoginWrapper()
                , new TaskCompleteListener<LoginWrapper>() {
                    @Override
                    public void onSuccess(LoginWrapper mObject) {

                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        if (mObject.getStatus() == 1) {
                            ((MainActivity) getActivity()).messageAlert(mObject.getMessage());
                            ((MainActivity) getActivity()).setData(getContext(), new Gson().toJson(mObject));
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, LoginWrapper like) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();
                    }
                });
    }

    @OnClick(R.id.shareTheApp)
    public void onClick() {
        /*Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.hyper.ZombieAbomination&hl=en");
        sharingIntent.setType("text/plain");
        startActivity(Intent.createChooser(sharingIntent, "Share application using"));*/
    }
}
