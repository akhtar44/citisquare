package com.citisquare.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.controls.CustomButton;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.dialog.CategorySelectionDialog;
import com.citisquare.app.dialog.SubCategorySelectionDialog;
import com.citisquare.app.interfaces.CategoryListener;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.SubCategoryListener;
import com.citisquare.app.pojo.response.CategoryData;
import com.citisquare.app.pojo.response.LoginData;
import com.citisquare.app.pojo.response.LoginWrapper;
import com.citisquare.app.pojo.response.SubCategory;
import com.citisquare.app.utils.Debugger;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 5/1/17.
 */

public class CategorySelectionFragment extends BaseFragment {

    @BindView(R.id.categorySelectionSelectCategory)
    CustomTextView categorySelectionSelectCategory;
    @BindView(R.id.categorySelecttionSubCategoryselection)
    CustomTextView categorySelecttionSubCategoryselection;
    @BindView(R.id.submitCategorySelection)
    CustomButton submitCategorySelection;

    Map<String, String> map2;
    String categoryId = "";
    String subCategoryId = "";
    LoginWrapper loginWrapper;
    LoginData loginData;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_category_selection, container, false);
        ButterKnife.bind(this, view);
        map2 = new HashMap<String, String>();
        loginWrapper = ((MainActivity) getActivity()).getData(getContext());
        loginData = loginWrapper.getData();
        return view;
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle(getString(R.string.SelectCategory), Enum.setNevigationIcon.ALLHIDE, true);
    }


    @OnClick(R.id.categorySelecttionSubCategoryselection)
    public void subCategorySelection() {
        if (categorySelectionSelectCategory.getText().toString().equals(getString(R.string.select_categor))) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseSelectFirstCategory));
        } else {
            SubCategorySelectionDialog.openCategorySelectionDialog(getContext(), categoryId, new SubCategoryListener() {
                @Override
                public void onSubCategoryListener(List<SubCategory> subCategories) {
                    String s = "";
                    subCategoryId = "";
                    for (SubCategory categoryData : subCategories) {
                        if (categoryData.isSelected()) {
                            if (Locale.getDefault().getDisplayLanguage().equals("العربية")) {
                                s += categoryData.getArName() + ",";
                            } else {
                                s += categoryData.getName() + ",";
                            }
                            subCategoryId += categoryData.getId() + ",";
                        }
                    }
                    if (subCategoryId.equalsIgnoreCase("")) {
                        categorySelecttionSubCategoryselection.setText(getString(R.string.select_sub_categor));
                    } else {
                        categorySelecttionSubCategoryselection.setText(removeLastCharacter(s));
                    }
                }
            });
        }
    }

    public String removeLastCharacter(String str) {
        if (str != null && str.length() > 0 && str.charAt(str.length() - 1) == ',') {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }

    @Override
    public void onResume() {
        super.onResume();
        Debugger.e("Resume is call");
    }

    @OnClick(R.id.categorySelectionSelectCategory)
    public void categorySelection() {
        CategorySelectionDialog.openCategorySelectionDialog(getContext(), Integer.parseInt(loginData.getCategoryCount()), "", new CategoryListener() {
            @Override
            public void onCategorySelected(List<CategoryData> categoryDatas) {
                String s = "";
                categorySelecttionSubCategoryselection.setText(getString(R.string.select_sub_categor));
                categoryId = "";
                subCategoryId = "";

                for (CategoryData categoryData : categoryDatas) {
                    if (categoryData.isSelected()) {
                        if (Locale.getDefault().getDisplayLanguage().equals("العربية")) {
                            s += categoryData.getArName() + ",";
                        } else {
                            s += categoryData.getName() + ",";
                        }
                        categoryId += categoryData.getId() + ",";
                    }
                }
                if (categoryId.equalsIgnoreCase("")) {
                    categorySelectionSelectCategory.setText(getString(R.string.select_categor));
                } else {
                    categorySelectionSelectCategory.setText(removeLastCharacter(s));
                }
            }
        });
    }


    @OnClick(R.id.submitCategorySelection)
    public void submitCategoryselectionClicked() {
        if (getActivity() != null) {
            ((MainActivity) getActivity()).startLoader();
        }

        new PostRequestHelper<LoginWrapper>().pingToRequest(Constants.ADD_SELLER_CATEGORY
                , null
                , null
                , getHashMapForCategory()
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new LoginWrapper()
                , new TaskCompleteListener<LoginWrapper>() {
                    @Override
                    public void onSuccess(LoginWrapper mObject) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();
                            ((MainActivity) getActivity()).replaceParentFragment(new TabFragment(), "parent");
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, LoginWrapper loginWrapper) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();
                        }
                    }
                });
    }

    private HashMap<String, String> getHashMapForCategory() {
        HashMap<String, String> categoryHashMap = new HashMap<>();
        categoryHashMap.put("category_id", categoryId);
        categoryHashMap.put("subcat_id", subCategoryId);
        return categoryHashMap;
    }
}