package com.citisquare.app.fragments;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.citisquare.app.R;
import com.citisquare.app.adapter.CityDropDownAdapter;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.pojo.response.City;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 16/7/16.
 */
public class CityDropDownSeller {
    PopupWindow popupWindow;
    View view;
    boolean popUpVisible = false;
    int position;
    ItemEventListener<String> itemEventListener;
    @BindView(R.id.cityList)
    ListView categoryListview;
    TextView textView;
    Context context;
    ArrayList<City> categoryArrayList;
    CityDropDownAdapter cityDropDownAdapter;
    int height;
    int[] location = new int[2];


    public CityDropDownSeller(Context context, int height, int[] location, ArrayList<City> categoryArrayList, ItemEventListener<String> integerItemEventListener) {
        view = View.inflate(context, R.layout.city_dropdown, null);
        ButterKnife.bind(this, view);
        itemEventListener = integerItemEventListener;
        this.context = context;
        categoryListview.setTextFilterEnabled(true);
        this.categoryArrayList = categoryArrayList;
        this.location = location;
        this.height = height;
        popupWindow = new PopupWindow(view, LinearLayout.LayoutParams.MATCH_PARENT, (int) context.getResources().getDimension(R.dimen.dp_180));
    }


    public void setAdapter() {
        categoryListview.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        cityDropDownAdapter = new CityDropDownAdapter(context, R.layout.raw_category_dropdown, categoryArrayList);
        categoryListview.setAdapter(cityDropDownAdapter);

        popupWindow.setContentView(view);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0));
        popupWindow.setFocusable(true);
        if (popupWindow.isFocusable()) {
            popUpVisible = false;
        }
        categoryListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String city = new Gson().toJson(categoryArrayList.get(position));
                itemEventListener.onItemEventFired(position + "", city, "");
                dismiss();
            }
        });
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int pos) {
        position = pos;
    }

    public void show() {
        popUpVisible = true;
        setAdapter();
        //popupWindow.showAsDropDown(view);
        popupWindow.showAtLocation(view, Gravity.TOP, location[0],
                location[1] + height);
    }


    public void dismiss() {
        popUpVisible = false;
        popupWindow.dismiss();
    }

    public boolean isVisible() {
        return popUpVisible;
    }
}