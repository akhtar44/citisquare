package com.citisquare.app.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.camera.utils.Utils;
import com.citisquare.app.dialog.PickerDialog;
import com.citisquare.app.dialog.VideoTrimDialog;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ImagePickerDialogListener;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.listener.TrimListener;
import com.citisquare.app.listener.UriRecordingListener;
import com.citisquare.app.pojo.response.City;
import com.citisquare.app.pojo.response.LoginWrapper;
import com.citisquare.app.utils.Debugger;
import com.citisquare.app.utils.FileUtils;
import com.citisquare.app.utils.RequestParameter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import serviceCalling.builder.KeyValuePair;
import serviceCalling.helper.PostVideoHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 4/6/16.
 */
public class PostAdsFragment extends BaseFragment {
    @BindView(R.id.imageViewTimeLine)
    ImageView imageViewTimeLine;
    @BindView(R.id.timelineSelectCity)
    TextView timelineSelectCity;
    @BindView(R.id.editTextDescriptionOfferTimeLine)
    EditText editTextDescriptionOfferTimeLine;
    @BindView(R.id.postOnTimeLineButtonSubmit)
    Button buttonSubmit;
    File image;
    @BindView(R.id.topLayout)
    FrameLayout topLayout;
    CityDropDown cityDropDown;
    ArrayList<City> cityList;
    int seconds;
    String mediaType = "";
    String cityId = "";
    private String pathVideo;
    private File fileImage;


    private Pattern pattern;
    private Matcher matcher;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.post_on_time_line, container, false);
        ButterKnife.bind(this, view);

        cityList = ((MainActivity) getActivity()).cityArrayList();
        pattern = Pattern.compile(Constants.englishOnly);

        editTextDescriptionOfferTimeLine.setMovementMethod(new ScrollingMovementMethod());

        editTextDescriptionOfferTimeLine.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.editTextDescriptionOfferTimeLine) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        return view;
    }

    @OnClick(R.id.timelineSelectCity)
    public void postOnTimeLineSubmit() {
        /*View view = getActivity().getLayoutInflater().inflate(R.layout.bottomsheet, null);
        view.findViewById(R.id.fakeShadow).setVisibility(View.GONE);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        TextView textView = (TextView) view.findViewById(R.id.dilogHeading);
        textView.setText("Select City");
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));


        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getActivity());
        ArrayList<String> strings = new ArrayList<>();
        for (City city : cityList) {
            strings.add(city.getName());
        }
        BottomSheetAdapter<City> cityBottomSheetAdapter = new BottomSheetAdapter<>(getContext(), mBottomSheetDialog,cityList, strings, new ItemEventListener<City>() {
            @Override
            public void onItemEventFired(City city, @Nullable City t2) {
                cityId = city.getId();
                timelineSelectCity.setText(city.getName());
            }
        });
        recyclerView.setAdapter(cityBottomSheetAdapter);
        mBottomSheetDialog.setContentView(view);
        BottomSheetBehavior mDialogBehavior = BottomSheetBehavior.from((View) view.getParent());

        mBottomSheetDialog.show();*/

        /*showBottomSheetDialog(getActivity(), cityList, "Select City", new ItemEventListener<City>() {
            @Override
            public void onItemEventFired(City city, @Nullable City t2) {

            }
        });*/
        int[] location = new int[2];
        timelineSelectCity.getLocationOnScreen(location);
        cityDropDown = new CityDropDown(getContext(), timelineSelectCity.getHeight(), location, cityList, new ItemEventListener<Integer>() {
            @Override
            public void onItemEventFired(Integer integer, Integer t2, String actualPos) {
                cityId = cityList.get(integer).getId();
                if (Locale.getDefault().getDisplayLanguage().equals("العربية"))
                    timelineSelectCity.setText(cityList.get(integer).getArName());
                else
                    timelineSelectCity.setText(cityList.get(integer).getName());


            }
        });
        cityDropDown.show();
    }

    @OnClick(R.id.postOnTimeLineButtonSubmit)
    public void buttonSubmitClicked() {
        if (isValid()) {
            ((MainActivity) getActivity()).hideKeyboard();
            postAds();
            /*clearPostAds();
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_postAdsSuccessfully));*/
        }
    }

    private void postAds() {
        if (getActivity() != null)
            ((MainActivity) getActivity()).startLoader();
        /*media_type : P->Photo, V->Video
        media_name : You should pass selected media in app.*/

        ArrayList<KeyValuePair> keyValuePair = new ArrayList<>();
        ArrayList<KeyValuePair> keyValuePairVideo = new ArrayList<>();
        if (!mediaType.equals("")) {
            if (mediaType.equalsIgnoreCase("V")) {
                keyValuePairVideo.add(new KeyValuePair("media_name", pathVideo));
                keyValuePair.add(new KeyValuePair("video_thumb", fileImage.getAbsolutePath()));
            } else {
                keyValuePair.add(new KeyValuePair("media_name", image.getAbsolutePath()));
            }
        }

        new PostVideoHelper<LoginWrapper>().pingToRequest(Constants.TIMELINE_ADD
                , "image/*"
                , "video/*"
                , keyValuePair
                , keyValuePairVideo
                , new RequestParameter().getPostOnTimeLine(getPostOnTimeLine())
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new LoginWrapper()
                , new TaskCompleteListener<LoginWrapper>() {
                    @Override
                    public void onSuccess(LoginWrapper mObject) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();
                        Debugger.e(mObject.getMessage());

                        if (mObject.getStatus() == 1) {
                            ((MainActivity) getActivity()).messageAlert(mObject.getMessage());
                            clearPostAds();
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, LoginWrapper loginWrapper) {
                        if (getActivity() != null) {

                            if (getActivity() != null)
                                ((MainActivity) getActivity()).stopLoader();

                            if (loginWrapper != null) {
                                if (loginWrapper.getStatus() == -1)
                                    ((MainActivity) getActivity()).invalidToken();
                            }

                            if (loginWrapper.getStatus() == 0)
                                ((MainActivity) getActivity()).messageAlert(loginWrapper.getMessage());

                        }
                    }
                });
    }

    public HashMap<String, String> getPostOnTimeLine() {
        HashMap<String, String> hashMapPostTimeLine = new HashMap<>();
        hashMapPostTimeLine.put("city_id", cityId);
        hashMapPostTimeLine.put("description", editTextDescriptionOfferTimeLine.getText().toString().trim());
        if (mediaType.equalsIgnoreCase("V")) {
            hashMapPostTimeLine.put("media_type", "V");
        } else if (mediaType.equalsIgnoreCase("P")) {
            hashMapPostTimeLine.put("media_type", "P");
        } else {
            hashMapPostTimeLine.put("media_type", "");
        }
        hashMapPostTimeLine.put("type", "A");
        return hashMapPostTimeLine;
    }


    public void clearPostAds() {
        timelineSelectCity.setText(R.string.select_city);
        editTextDescriptionOfferTimeLine.setText("");
        imageViewTimeLine.setImageResource(0);
        image = new File("");
        pathVideo = "";
        fileImage = new File("");
        mediaType = "";
        topLayout.setVisibility(View.VISIBLE);
    }

    public boolean isValid() {
        if (cityId.equals("")) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseSelectCity));
            return false;
        }
        if (editTextDescriptionOfferTimeLine.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseWriteOfferDescription));
            return false;
        }
        return true;
    }


    @OnClick(R.id.imageViewTimeLine)
    public void imageSetForTimeLineClicked() {
        PickerDialog videoChooserDialog = new PickerDialog();
        videoChooserDialog.setCallback(new ImagePickerDialogListener() {
            @Override
            public void getResultFromCamera(String result, String from) {

                CameraFragment cameraFragment = new CameraFragment();
                cameraFragment.setRecoringListener(getContext(), new UriRecordingListener() {
                    @Override
                    public void setFileImageName(Uri name, String from) {
                        methodUriConvert(name);
                        String filePath = Utils.getPathFromUri(name, getContext());

                        if (filePath != null)
                            image = new File(filePath);

                        mediaType = "P";

                    }

                    @Override
                    public void setFileVideo(String file) {
                        videoToImage(file);
                        mediaType = "V";
                    }
                });
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();

                fragmentTransaction.add(R.id.placeholder, cameraFragment, PostAdsFragment.class.getName());
                fragmentTransaction.addToBackStack(CameraFragment.class.getName());
                fragmentTransaction.commitAllowingStateLoss();

            }

            @Override
            public void getResultFromGallery(Intent result, String from) {
                if (from.equals("video")) {
                    toCheckVideoLength(result);
                    Debugger.e("VIDEO URL PATH IS :::::::::::::::: " + pathVideo);
                    mediaType = "V";
                } else if (from.equals("image")) {
                    Uri selectedImage = result.getData();
                    String path = FileUtils.getPath(getActivity(), selectedImage);
                    if (path != null)
                        image = new File(path);
                    ((MainActivity) getActivity()).picasso.load(image).into(imageViewTimeLine);
                    topLayout.setVisibility(View.INVISIBLE);
                    mediaType = "P";
                }
            }

            @Override
            public void nothingSet() {
            }
        });
        videoChooserDialog.show(getActivity().getSupportFragmentManager(), PickerDialog.class.getName());
    }


    public void tosetFramming(Intent data) {
        VideoTrimDialog videoTrimDialog = new VideoTrimDialog();
        Bundle bundle = new Bundle();
        bundle.putString("key", FileUtils.getPath(getActivity(), data.getData()));
        videoTrimDialog.setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogTheme);
        videoTrimDialog.setArguments(bundle);

        videoTrimDialog.setCallback(new TrimListener() {
            @Override
            public void onTrimmingDone(String url) {
                Debugger.e("URL IS CALL BACK FROM VIDEO TRIM......" + url);
                videoToImage(url);
            }
        });
        videoTrimDialog.show(getActivity().getSupportFragmentManager(), VideoTrimDialog.class.getName());
    }

    public void toCheckVideoLength(Intent data) {
        try {
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            retriever.setDataSource(getActivity().getApplication(), data.getData());
            MediaPlayer mp = MediaPlayer.create(getActivity().getBaseContext(), data.getData());
            int millis = mp.getDuration();
            Debugger.e("Millis:::>" + millis);
            Uri videoUri = data.getData();
            pathVideo = FileUtils.getPath(getActivity(), videoUri);
            seconds = (millis / 1000);
            if (seconds > 15) {
                tosetFramming(data);
            } else {
                videoToImage(pathVideo);
            }
            Debugger.e("Seconds:::>" + seconds);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void videoToImage(String path) {
        try {
//            Bitmap bitmap = getVidioThumbnail(path);
            Debugger.e("video to image full path is ::::::::::::: " + path);
            Uri uri = Uri.parse(path);
            Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MINI_KIND);

            /*MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            retriever.setDataSource(getActivity().getApplication(), uri);
            Bitmap bitmap = retriever.getFrameAtTime(1000, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);*/

            setImageToView(bitmap);
            this.pathVideo = path;
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            String videoImage = "video_image" + System.currentTimeMillis() + ".jpeg";
            bitmap.compress(Bitmap.CompressFormat.JPEG, 85, byteArrayOutputStream);
            File outputDir = getActivity().getCacheDir();
            fileImage = new File(outputDir, "img_" + System.currentTimeMillis() + ".jpg");
            // fileImage.createNewFile();

            FileOutputStream fileOutputStream = new FileOutputStream(fileImage);
            fileOutputStream.write(byteArrayOutputStream.toByteArray());
            fileOutputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            Debugger.e("EXCEPTION ::: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void setImageToView(final Bitmap bitmap) {
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Debugger.e("CALLL IMAGE SET >>>>");
                    topLayout.setVisibility(View.INVISIBLE);
                    imageViewTimeLine.setImageBitmap(bitmap);
                }
            });
        }
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle(getString(R.string.title_postAd), Enum.setNevigationIcon.MENU, false);
    }

    public void methodUriConvert(Uri image) {

        String pathFromUri = Utils.getPathFromUri(image, getContext());

        int roatation = 0;

        if (pathFromUri != null) {
            try {
                ExifInterface exifInterface = new ExifInterface(pathFromUri);
                int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);

                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        roatation = 90;
                        // imageView.setRotation(90);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        roatation = 180;
                        // imageView.setRotation(180);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        roatation = 270;
                        //imageView.setRotation(270);
                        break;
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (roatation != 0) {
            Matrix matrix = new Matrix();
            matrix.preRotate(roatation);
            BitmapFactory.Options options = new BitmapFactory.Options();
            Bitmap bitmap = BitmapFactory.decodeFile(pathFromUri, options);
            Bitmap rotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            imageViewTimeLine.setImageBitmap(rotated);
            //((MainActivity) getActivity()).setImage(image, imageViewTimeLine);

            //imageView.setImageBitmap(rotated);
            //Picasso.with(getContext()).load(imageUri).into(imageView);
        } else
            imageViewTimeLine.setImageURI(image);
        //
        //
        // imageView.se(imageUri);

        topLayout.setVisibility(View.INVISIBLE);
    }

}
