package com.citisquare.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.PlaceAutocompleteAdapter;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.pojo.response.City;
import com.citisquare.app.pojo.response.LoginWrapper;
import com.citisquare.app.pojo.response.SubCategory;
import com.citisquare.app.utils.Debugger;
import com.citisquare.app.utils.RequestParameter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 2/6/16.
 */
public class PostJobFragment extends BaseFragment {


    @BindView(R.id.editTextJobTitle)
    EditText editTextJobTitle;
    @BindView(R.id.editTextExperienceReq)
    EditText editTextExperienceReq;
    @BindView(R.id.editTextJobLocation)
    AutoCompleteTextView editTextJobLocation;
    @BindView(R.id.postJobSubmit)
    Button postJobSubmit;
    @BindView(R.id.jobDescription)
    EditText jobDescription;
    @BindView(R.id.postJobCity)
    TextView postJobCity;
    @BindView(R.id.saudis_jobs)
    RadioButton saudisJobs;
    @BindView(R.id.non_saudis_jobs)
    RadioButton nonSaudisJobs;
    @BindView(R.id.radioJobSelection)
    RadioGroup radioJobSelection;
    @BindView(R.id.postJobSubCategory)
    TextView postJobSubCategory;

    CityDropDown cityDropDown;
    ArrayList<City> cityList;

    SubCategoryDropDown subCategoryDropDown;
    List<SubCategory> subCatList;
    String jobString = "S";
    String cityId = "";
    String subCatId = "";

    private Pattern pattern;
    private Matcher matcher;
    private PlaceAutocompleteAdapter placeAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.post_job, container, false);
        ButterKnife.bind(this, view);
        cityList = ((MainActivity) getActivity()).cityArrayList();
        if (((MainActivity) getActivity()).getCategoryList() != null) {
            subCatList = ((MainActivity) getActivity()).getCategoryList().get(0).getSubCategory();
        }


        pattern = Pattern.compile(Constants.englishOnly);

        placeAdapter = new PlaceAutocompleteAdapter(getContext(), R.layout.raw_autocomplete_text);
        editTextJobLocation.setAdapter(placeAdapter);

        editTextJobLocation.setThreshold(1);

        jobDescription.setMovementMethod(new ScrollingMovementMethod());

        jobDescription.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.jobDescription) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        radioJobSelection.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int radio = radioJobSelection.getCheckedRadioButtonId();
                switch (radio) {
                    case R.id.saudis_jobs:
                        jobString = "S";
                        break;
                    case R.id.non_saudis_jobs:
                        jobString = "NS";
                        break;
                }
            }
        });


        return view;
    }

    @OnClick(R.id.postJobSubmit)
    public void jobButton() {
        if (isValid()) {
            ((MainActivity) getActivity()).hideKeyboard();
            postJobAdd();
            /*clearWhole();
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_jobPostedSuccesfully));*/
        }
    }

    public void postJobAdd() {
        if (getActivity() != null)
            ((MainActivity) getActivity()).startLoader();

        new PostRequestHelper<LoginWrapper>().pingToRequest(Constants.POST_ADD
                , null
                , null
                , new RequestParameter().getHashMapPostJob(postDetails())
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new LoginWrapper()
                , new TaskCompleteListener<LoginWrapper>() {
                    @Override
                    public void onSuccess(LoginWrapper login) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();


                        clearWhole();

                        if (login.getStatus() == 1)
                            ((MainActivity) getActivity()).messageAlert(login.getMessage());
                        else
                            ((MainActivity) getActivity()).messageAlert(login.getMessage());
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, LoginWrapper loginWrapper) {
//                        Debugger.e("SUCCESS FULLY ADD:::" + loginWrapper.getMessage() + "EXTENSION::" + exceptions.toString());
                    }
                });
    }

    public HashMap<String, String> postDetails() {

        /*job_type,title,experience,location,city_id,cat_id,subcat_id,description*/
        HashMap<String, String> hashMapPostJob = new HashMap<>();
        hashMapPostJob.put("job_type", jobString);
        hashMapPostJob.put("title", editTextJobTitle.getText().toString().trim());
        hashMapPostJob.put("experience", editTextExperienceReq.getText().toString().trim());
        hashMapPostJob.put("location", editTextJobLocation.getText().toString());
        hashMapPostJob.put("city_id", cityId + "");
        hashMapPostJob.put("cat_id", 1 + "");
        hashMapPostJob.put("subcat_id", subCatId + "");
        hashMapPostJob.put("description", jobDescription.getText().toString().trim());
        return hashMapPostJob;
    }


    public void clearWhole() {
        editTextJobTitle.setText("");
        editTextExperienceReq.setText("");
        editTextJobLocation.setText("");
        postJobCity.setText(getString(R.string.selectCity));
        postJobSubCategory.setText(getString(R.string.select_sub_categor));
        jobDescription.setText("");
    }

    public boolean isValid() {
        if (editTextJobTitle.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterJobTitle));
            editTextJobTitle.requestFocus();
            return false;
        }

        if (editTextExperienceReq.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterExperience));
            editTextExperienceReq.requestFocus();
            return false;
        }

        if (editTextJobLocation.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterJobLocation));
            editTextJobLocation.requestFocus();
            return false;
        }


        if (cityId.equals("")) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseSelectCity));
            return false;
        }

        if (subCatId.equals("")) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseSelectSubCategory));
            return false;
        }
        if (jobDescription.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseWriteOfferDescription));
            jobDescription.requestFocus();
            return false;
        }

        return true;
    }

    @OnClick(R.id.postJobCity)
    public void todayOfferSelectCity() {
        ((MainActivity) getActivity()).hideKeyboard();
        int[] location = new int[2];
        postJobCity.getLocationOnScreen(location);
        cityDropDown = new CityDropDown(getContext(), postJobCity.getHeight(), location, cityList, new ItemEventListener<Integer>() {
            @Override
            public void onItemEventFired(Integer integer, Integer t2, String actualPos) {
                if (Locale.getDefault().getDisplayLanguage().equals("العربية"))
                    postJobCity.setText(cityList.get(integer).getArName());
                else
                    postJobCity.setText(cityList.get(integer).getName());

                cityId = cityList.get(integer).getId();
            }
        });
        cityDropDown.show();
    }

    @OnClick(R.id.postJobSubCategory)
    public void postJobSubCategoryClicked() {
        ((MainActivity) getActivity()).hideKeyboard();
        int[] location = new int[2];
        postJobSubCategory.getLocationOnScreen(location);
        subCategoryDropDown = new SubCategoryDropDown(getContext()
                , postJobSubCategory.getHeight()
                , location
                , (ArrayList<SubCategory>) subCatList
                , new ItemEventListener<Integer>() {
            @Override
            public void onItemEventFired(Integer integer, Integer t2, String actualPos) {
                if (Locale.getDefault().getDisplayLanguage().equals("العربية")) {
                    postJobSubCategory.setText(subCatList.get(integer).getArName());
                } else {
                    postJobSubCategory.setText(subCatList.get(integer).getName());
                }

                subCatId = subCatList.get(integer).

                        getId();
            }
        });
        subCategoryDropDown.show();
    }


    @Override
    public void setHeaderTitle() {
        Debugger.e("Post Job title");
        ((MainActivity) getActivity()).setTitle(getString(R.string.title_postAJob), Enum.setNevigationIcon.MENU, false);
    }
}
