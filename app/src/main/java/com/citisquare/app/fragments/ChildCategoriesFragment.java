package com.citisquare.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.ChildCategoryAdapter;
import com.citisquare.app.adapter.QuickInfoAdapter;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.interfaces.Result;
import com.citisquare.app.pojo.response.CategoryData;
import com.citisquare.app.pojo.response.OfferData;
import com.citisquare.app.pojo.response.SliderData;
import com.citisquare.app.pojo.response.SliderWrapper;
import com.citisquare.app.pojo.response.SubCategory;
import com.citisquare.app.utils.Debugger;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import serviceCalling.builder.KeyValuePair;
import serviceCalling.helper.GetRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink16 on 27/5/16.
 */
public class ChildCategoriesFragment extends BaseFragment {
    ChildCategoryAdapter categoryAdapter;
    QuickInfoAdapter quickInfoAdapter;
    ArrayList<OfferData> quickInfoDatas = new ArrayList<>();
    @BindView(R.id.childCategoryRecyclerView)
    RecyclerView childCategoryRecyclerView;
    @BindView(R.id.relative_empty)
    RelativeLayout relative_empty;
    @BindView(R.id.categorySliderRecyclerView)
    RecyclerView categorySliderRecyclerView;
    Bundle bundle;
    CategoryData categoryData;
    private List<SliderData> sliderData;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.child_category_layout, container, false);
        ButterKnife.bind(this, view);
        bundle = getArguments();
        if (bundle != null) {
            if (bundle.getSerializable("categoryData") != null) {
                categoryData = (CategoryData) bundle.getSerializable("categoryData");
            }
        }

        if (categoryData != null) {
            Debugger.e(categoryData.toString());
        } else {
            Debugger.e("<<NULL>>>>__|");
        }
        BaseFragment baseFragment = (BaseFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.placeholder);
        if (baseFragment instanceof TabFragment) {
            ((TabFragment) baseFragment).setSearchVisibility(Enum.setSearch.SEARCH);
        }
        sliderData = new ArrayList<>();
        categoryAdapter = new ChildCategoryAdapter(getContext(), (ArrayList<SubCategory>) categoryData.getSubCategory(), new Result<SubCategory>() {
            @Override
            public void onSuccess(SubCategory subCategory) {
                String pos="";
                BaseFragment baseFragmentTab = (BaseFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.placeholder);
                if (baseFragmentTab != null) {
                    if (baseFragmentTab instanceof TabFragment) {
                        ((TabFragment) baseFragmentTab).clearFilter();
                    }
                }
                ((MainActivity) getActivity()).hideKeyboard();
                if (categoryData.getId().equalsIgnoreCase("1")) {
                    Bundle bundle = new Bundle();
                    bundle.putString("data", subCategory.getId());
                    ParentJobFragment parentJobFragment = new ParentJobFragment();
                    parentJobFragment.setArguments(bundle);
                    //((MainActivity) getActivity()).setFragment(parentJobFragment);
                    childFragmentReplacement(parentJobFragment, "child");
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putString("data", subCategory.getId());
                    CategorySellerFragment categorySellerFragment = new CategorySellerFragment();
                    categorySellerFragment.setArguments(bundle);
                    //((MainActivity) getActivity()).setFragment(categorySellerFragment);
                    childFragmentReplacement(categorySellerFragment, "child");
                }
            }
        });
        setSliderAdapter();

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        childCategoryRecyclerView.setLayoutManager(gridLayoutManager);
        childCategoryRecyclerView.setAdapter(categoryAdapter);
        callApiOfSlider(categoryData.getId());
        // subCategory();
        return view;
    }

    private void callApiOfSlider(String id) {
        ArrayList<KeyValuePair> keyValuePairs = new ArrayList<>();
        keyValuePairs.add(new KeyValuePair("city_id", ((MainActivity) getActivity()).getTopCitySelectionId()));
        keyValuePairs.add(new KeyValuePair("cat_id", id));


        new GetRequestHelper<SliderWrapper>().pingToRequest(Constants.CATEGORY_SLIDER_IMAGE, keyValuePairs, ((MainActivity) getActivity()).getHeader(), new SliderWrapper(), new TaskCompleteListener<SliderWrapper>() {
            @Override
            public void onSuccess(SliderWrapper sliderWrapper) {
                if (sliderWrapper.getStatus() == 1) {
                    setSliderData(sliderWrapper.getData());
                }
            }

            @Override
            public void onFailure(ExceptionType exceptions, SliderWrapper sliderWrapper) {

            }
        });
    }

    private void setSliderData(List<SliderData> data) {
        sliderData.clear();
        sliderData.addAll(data);
        quickInfoAdapter.notifySlider();
    }

    private void setSliderAdapter() {
        try {
            quickInfoAdapter = new QuickInfoAdapter(getContext(), getChildFragmentManager(), quickInfoDatas, new ItemEventListener<String>() {
                @Override
                public void onItemEventFired(String pos, String userId, final String actualPos) {

                }
            }, sliderData,true);

            GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1);
            categorySliderRecyclerView.setLayoutManager(gridLayoutManager);
            categorySliderRecyclerView.setAdapter(quickInfoAdapter);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void getChildCount() {
        Debugger.e(":::::IN THE DATA::::" + getChildFragmentManager().getBackStackEntryCount());
    }

    public void subCategory() {
        List<KeyValuePair> keyValuePair = new ArrayList<>();
        keyValuePair.add(new KeyValuePair("cat_id", categoryData.getId()));

    }


    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle(categoryData.getName(), Enum.setNevigationIcon.BACK, true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}