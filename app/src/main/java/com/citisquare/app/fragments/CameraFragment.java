package com.citisquare.app.fragments;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.Camera;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.camera.core.CameraSettingInitializer;
import com.citisquare.app.camera.core.CameraSettingLoader;
import com.citisquare.app.camera.core.CameraSettings;
import com.citisquare.app.camera.utils.CameraPreview;
import com.citisquare.app.camera.utils.Utils;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.listener.UriRecordingListener;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 24/12/16.
 */

public class CameraFragment extends BaseFragment implements Camera.PictureCallback {


    @BindView(R.id.videoview)
    FrameLayout videoview;
    @BindView(R.id.mybutton)
    View mybutton;
    @BindView(R.id.buttonSubmit)
    CustomTextView buttonSubmit;
    @BindView(R.id.textViewSeconds)
    CustomTextView textViewSeconds;
    @BindView(R.id.myPhotoCapture)
    View myPhotoCapture;
    @BindView(R.id.photoSubmit)
    CustomTextView photoSubmit;
    String fileName;
    Camera camera;
    CameraPreview mPreview;
    boolean recording, isRecorded;
    CountDownTimer countDownTimer;
    Context context;
    private MediaRecorder mMediaRecorder;
    private Camera.Size optimalVideoSize;
    private CameraSettings cameraSettings;
    private UriRecordingListener callback;

    private String VIDEO_PATH_NAME = "/mnt/sdcard/VGA_30fps_512vbrate.mp4";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.video_recording, container, false);
        ButterKnife.bind(this, view);

        CameraSettingLoader cameraSettingLoader = new CameraSettingLoader(new CameraSettingInitializer(), getContext());
        try {
            cameraSettings = cameraSettingLoader.getCameraSetting();
        } catch (RuntimeException e) {
            Toast.makeText(getContext(), "Fail to connect to camera", Toast.LENGTH_LONG).show();
            getActivity().onBackPressed();
            //getString(R.string.app_name),"Fail to connect to camera").show(getChildFragmentManager(),"abcd");

        }


        fileName = Utils.createNewVideoFile();

        myPhotoCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!recording) {
                    if (buttonSubmit.getText().toString().equalsIgnoreCase(getContext().getString(R.string.start))) {
                        mybutton.setEnabled(false);
                        if (camera != null)
                            camera.takePicture(null, null, CameraFragment.this);
                    }
                }

            }
        });

        mybutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    onButtonClicked();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });


        return view;
    }


    /*@Override
    public void onStart() {
        Window w = getActivity().getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        w.setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        super.onStart();
    }*/


    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(MediaStore.Images.Media.ORIENTATION, mPreview.getCurrentOrientation());

            Uri imageFileUri = getActivity()
                    .getContentResolver()
                    .insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);


            if (imageFileUri != null) {
                OutputStream imageFileOS = getActivity()
                        .getContentResolver()
                        .openOutputStream(imageFileUri);
/*
                Window w = getActivity().getWindow();
                w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                w.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);*/


                if (imageFileOS != null) {
                    imageFileOS.write(data);
                    imageFileOS.flush();
                    imageFileOS.close();
                    callback.setFileImageName(imageFileUri, "image");
                    getActivity().onBackPressed();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            File newFile = Utils.saveFile(data);
            Uri uri = Uri.fromFile(newFile);
            getActivity().sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri));
            callback.setFileImageName(uri, "image");
            getActivity().onBackPressed();
        }
    }


    public void setRecoringListener(Context context, UriRecordingListener callback) {
        this.callback = callback;
        this.context = context;
    }

    private void onButtonClicked() throws IOException {
        myPhotoCapture.setClickable(false);
        photoSubmit.setClickable(false);
        myPhotoCapture.setBackground(getResources().getDrawable(R.drawable.circle_gray));
        photoSubmit.setTextColor(Color.BLACK);
        if (recording) {
            if (countDownTimer != null) {
                // stop recording and release camera
                //stopRecording();
                //releaseMediaRecorder(); // release the MediaRecorder object
                recording = false;
                countDownTimer.onFinish();
                //Exit after saved
                //  finish();


               /* if (callback != null && isRecorded) {
                    if (getActivity() != null) {
                        callback.setFileVideo(VIDEO_PATH_NAME);
                        getActivity().onBackPressed();
                    }
                }
*/
            }

        } else {

            //Release Camera before MediaRecorder start
            initRecorder();

            mMediaRecorder.start();
            tocountDownStart();
            recording = true;
            isRecorded = true;
            buttonSubmit.setText(R.string.stop);
        }
    }

    public void tocountDownStart() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                countDownTimer = new CountDownTimer(14000, 1000) {
                    @Override
                    public void onTick(long l) {
                        textViewSeconds.setText("" + l / 1000);
                    }

                    @Override
                    public void onFinish() {
                        try {
                            myPhotoCapture.setClickable(true);
                            photoSubmit.setClickable(true);
                           /* Window w = getActivity().getWindow();
                            w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                            w.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);*/

                            stopRecording();
                            if (callback != null && isRecorded) {
                                if (getActivity() != null) {
                                    callback.setFileVideo(VIDEO_PATH_NAME);
                                    getActivity().onBackPressed();
                                }
                            }

                            //textViewSeconds.setText("");
                            //textViewSeconds.setVisibility(View.GONE);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }.start();
            }
        }, 800);
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).actionBarHide();
    }


    @Override
    public void onStart() {
        /*Window w = getActivity().getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        super.onStart();
    }

    @Override
    public void onStop() {
        /*Window w = getActivity().getWindow();
        w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        w.clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);*/
        stopRecording();
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        camera = Camera.open();
        mPreview = new CameraPreview(getContext(), camera, cameraSettings);
        videoview.removeAllViews();
        videoview.addView(mPreview);
    }


    private void initRecorder() throws IOException {
        // It is very important to unlock the camera before doing setCamera
        // or it will results in a black preview

        if (mPreview.getCamera() != null) {

            if (!mPreview.isFrontFacing() && cameraSettings.isSupportBackFocus()) {
                Camera.Parameters parameters = mPreview.getCamera().getParameters();

                List<String> supportedFocusModes = parameters.getSupportedFocusModes();
                if (supportedFocusModes != null && !supportedFocusModes.isEmpty()) {
                    if (supportedFocusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO))
                        parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
                }
                mPreview.getCamera().setParameters(parameters);
            }

            mPreview.getCamera().unlock();

            if (mMediaRecorder != null)
                mMediaRecorder.reset();

            mMediaRecorder = new MediaRecorder();
            // mMediaRecorder.setPreviewDisplay(surface);
            mMediaRecorder.setCamera(mPreview.getCamera());

            mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
            mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

            mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
            mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
            mMediaRecorder.setVideoEncodingBitRate(2000000);

            Log.e("Orientation ", " " + mPreview.getCurrentOrientation());
            mMediaRecorder.setOrientationHint(mPreview.getCurrentOrientation());

            optimalVideoSize = mPreview.getVideoSize();
            mMediaRecorder.setVideoSize(optimalVideoSize.width, optimalVideoSize.height);
            VIDEO_PATH_NAME = Utils.createNewVideoFile();
            mMediaRecorder.setOutputFile(VIDEO_PATH_NAME);

            mMediaRecorder.setMaxDuration(15000); // Set max duration 40 sec.

            try {
                mMediaRecorder.prepare();
            } catch (IllegalStateException e) {
                // This is thrown if the previous calls are not called with the
                // proper order
                e.printStackTrace();
            }
        }
    }

    private void stopRecording() {
        if (mMediaRecorder != null) {
            try {
                myPhotoCapture.setClickable(true);
                mMediaRecorder.stop();
                mMediaRecorder.release();
                mMediaRecorder = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}
