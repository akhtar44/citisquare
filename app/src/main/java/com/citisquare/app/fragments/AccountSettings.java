package com.citisquare.app.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.Rendering;
import com.citisquare.app.pojo.response.Like;
import com.citisquare.app.pojo.response.LoginData;
import com.citisquare.app.pojo.response.LoginWrapper;
import com.google.android.gms.analytics.GoogleAnalytics;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.ButterKnife;
import serviceCalling.builder.KeyValuePair;
import serviceCalling.helper.GetRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

public class AccountSettings extends BaseFragment {

    MainActivity mainActivity = new MainActivity();
    CheckBox cb_share_data, cb_email_notification;
    RadioButton rb_one_day, rb_8_hours, rb_1_hour;
    Switch toggle_All_notification;
    LinearLayout layout_cb_allnotification;
    private static final String MyPREFERENCES = null;
    SharedPreferences app_preference = null;
    RadioButton rb_english, rb_arabic;
    CheckBox cb_email, cb_mobileno, cb_location;
    String getLanguage = null;
    TextView txt_subscription, txt_deleteacccount;
    Bundle bundle;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.account_settings, container, false);
       // View view = inflater.inflate(R.layout.about_layout, container, false);
        if (getActivity() != null)
            ((MainActivity) getActivity()).hideKeyboard();
        ButterKnife.bind(this, view);

        rb_english = (RadioButton) view.findViewById(R.id.rb_english);
        rb_arabic = (RadioButton) view.findViewById(R.id.rb_arabic);
        cb_email = (CheckBox) view.findViewById(R.id.cb_email);
        cb_location = (CheckBox) view.findViewById(R.id.cb_location);
        cb_mobileno = (CheckBox) view.findViewById(R.id.cb_mobilenumber);
        toggle_All_notification = (Switch) view.findViewById(R.id.cb_allNotification);
        layout_cb_allnotification = (LinearLayout) view.findViewById(R.id.cb_fullLayout);
        rb_one_day = (RadioButton) view.findViewById(R.id.cb_notification_allDays);
        rb_8_hours = (RadioButton) view.findViewById(R.id.cb_notification_8Hours);
        rb_1_hour = (RadioButton) view.findViewById(R.id.cb_notification_1Hour);
        txt_subscription = (TextView) view.findViewById(R.id.subscriptiontextview);
        txt_deleteacccount = (TextView) view.findViewById(R.id.deleteacccounttextview);
        cb_share_data = (CheckBox) view.findViewById(R.id.cb_share_data);
        cb_email_notification = (CheckBox) view.findViewById(R.id.cb_email_notification);

        app_preference = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = app_preference.edit();

        LoginWrapper loginWrapper = ((MainActivity) getActivity()).getData(getContext());
        final LoginData loginData = loginWrapper.getData();

        getLanguage = app_preference.getString("language", "English");
        if (getLanguage.equalsIgnoreCase("Arabic")) {
            rb_arabic.setChecked(true);
        } else {
            rb_english.setChecked(true);
        }
        cb_share_data.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

            }
        });
        cb_email_notification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

            }
        });
        txt_deleteacccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).alertTwoAction(getString(R.string.message_areYouSureYouWantDelete), new Rendering() {
                    @Override
                    public void response(boolean isCofirm) {
                        if (isCofirm) {
                            deleteAccount(loginData.getId());
                        }
                    }
                });
            }
        });
        txt_subscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                subcriptionDetails(loginData.getId());
            }
        });
        if(cb_email.isChecked())
        {

        }
        if(cb_location.isChecked())
        {

        }
        if(cb_mobileno.isChecked())
        {

        }
        return view;
    }

    private void subcriptionDetails(String userID) {
        List<KeyValuePair> keyValuePairs = new ArrayList<>();
        //keyValuePairs.add(new KeyValuePair("user_id", userID));

        new GetRequestHelper<Like>().pingToRequest(Constants.SUBSCRIPTION_DETAILS,
                keyValuePairs,
                ((MainActivity) getActivity()).getParamWholeParameters(),
                new Like(),
                new TaskCompleteListener<Like>() {
                    @Override
                    public void onSuccess(Like like) {
                        if (mainActivity != null) {
                            mainActivity.stopLoader();
                        }

                        if (like.getStatus() == 1) {
                            ((MainActivity) getActivity()).replaceParentFragment(new LoginFragment(), "");
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, Like like) {
                        if (mainActivity != null) {
                            mainActivity.stopLoader();
                        }

                        if (like != null) {
                            if (like.getStatus() == -1) {
                                mainActivity.invalidToken();
                            }
                        }
                    }
                });
    }

    private void deleteAccount(String userID) {

        List<KeyValuePair> keyValuePairs = new ArrayList<>();
        keyValuePairs.add(new KeyValuePair("user_id", userID));

        new GetRequestHelper<Like>().pingToRequest(Constants.DEACTIVATE_ACCOUNT,
                keyValuePairs,
                ((MainActivity) getActivity()).getParamWholeParameters(),
                new Like(),
                new TaskCompleteListener<Like>() {
                    @Override
                    public void onSuccess(Like like) {
                        if (mainActivity != null) {
                            mainActivity.stopLoader();
                        }

                        if (like.getStatus() == 1) {
                            //((MainActivity) getActivity()).setFragment(new LoginFragment(), false, "");
                            ((MainActivity) getActivity()).replaceParentFragment(new LoginFragment(), "");
                            Toast.makeText(getActivity(),"User deactivated successfully",Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, Like like) {
                        if (mainActivity != null) {
                            mainActivity.stopLoader();
                        }

                        if (like != null) {
                            if (like.getStatus() == -1) {
                                mainActivity.invalidToken();
                            }
                        }
                    }
                });
    }

    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        GoogleAnalytics.getInstance(getActivity().getBaseContext()).dispatchLocalHits();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle("ACCOUNT SETTINGS", Enum.setNevigationIcon.BACK, false);
    }

    public boolean onBackPressed() {

        saveLanguage();
        Intent intent = new Intent(getActivity(), MainActivity.class);
        startActivity(intent);

        return false;
    }

    private void saveLanguage() {
        SharedPreferences.Editor edit = app_preference.edit();
        if (rb_arabic.isChecked()) {
            Locale locale = new Locale("ar");
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getActivity().getResources().updateConfiguration(config, null);

            edit.putString("language", "Arabic");
            edit.commit();
        } else {
            Locale locale = new Locale("");
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getActivity().getResources().updateConfiguration(config, null);
            edit.putString("language", "English");
            edit.commit();
        }
    }

}