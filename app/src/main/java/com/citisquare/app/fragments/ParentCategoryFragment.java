package com.citisquare.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.NewspaperPictureSlideAdapter;
import com.citisquare.app.adapter.ParentCategoryAdapter;
import com.citisquare.app.adapter.QuickInfoAdapter;
import com.citisquare.app.adapter.SearchAdapter;
import com.citisquare.app.controls.AutoScrollViewPager;
import com.citisquare.app.controls.CustomAutoComplete;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.interfaces.Result;
import com.citisquare.app.pojo.response.CategoryData;
import com.citisquare.app.pojo.response.CategoryWrapper;
import com.citisquare.app.pojo.response.LoginWrapper;
import com.citisquare.app.pojo.response.OfferData;
import com.citisquare.app.pojo.response.SearchData;
import com.citisquare.app.pojo.response.SearchWrapper;
import com.citisquare.app.pojo.response.SliderData;
import com.citisquare.app.pojo.response.SliderWrapper;
import com.citisquare.app.utils.DataToPref;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import serviceCalling.builder.KeyValuePair;
import serviceCalling.helper.GetRequestHelper;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink16 on 27/5/16.
 */
public class ParentCategoryFragment extends BaseFragment {

    ParentCategoryAdapter categoryAdapter;
    QuickInfoAdapter quickInfoAdapter;
    ArrayList<OfferData> quickInfoDatas = new ArrayList<>();
    ArrayList<CategoryData> categories;
    @BindView(R.id.categoryRecyclerView)
    RecyclerView categoryRecyclerView;
    @BindView(R.id.categorySliderRecyclerView)
    RecyclerView categorySliderRecyclerView;
    @BindView(R.id.searchTextInCategoryScreen)
    CustomAutoComplete searchTextInCategoryScreen;
    NewspaperPictureSlideAdapter pictureSlideAdapter;
    SearchAdapter searchAdapter;
    List<SearchData> searchDatas;
    private List<SliderData> sliderData;
    FragmentManager manager;
    AutoScrollViewPager dummyAutoScroll;
    private Pattern pattern;
    private Matcher matcher;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.categories_layout, container, false);
        ButterKnife.bind(this, view);

        categories = new ArrayList<>();
        searchTextInCategoryScreen.setVisibility(View.VISIBLE);
        searchDatas = new ArrayList<>();
        searchAdapter = new SearchAdapter(getContext(), R.layout.raw_search_listing, searchDatas);
        BaseFragment baseFragment = (BaseFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.placeholder);
        if (baseFragment instanceof TabFragment) {
            ((TabFragment) baseFragment).setSearchVisibility(Enum.setSearch.NOTHING);
        }

        categorySliderRecyclerView.setNestedScrollingEnabled(false);
        categoryRecyclerView.setNestedScrollingEnabled(false);

        pattern = Pattern.compile(Constants.englishOnly);

        searchTextInCategoryScreen.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                try {
                    SearchData searchData = searchDatas.get(position);
                    if (searchData != null) {
                        searchTextInCategoryScreen.setText("");
                        if (searchData.getIsFlag().equalsIgnoreCase("U")) {
                            ProfileFragment profileFragment = new ProfileFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("profile", "other");
                            ((MainActivity) getActivity()).setProfileUserId(searchData.getId());
                            profileFragment.setArguments(bundle);
                            ((MainActivity) getActivity()).setSelectionPostion(0);
                            ((MainActivity) getActivity()).setFragment(profileFragment, false, "");
                        } else if (searchData.getIsFlag().equalsIgnoreCase("SC")) {
                            ((MainActivity) getActivity()).hideKeyboard();
                            Bundle bundle = new Bundle();
                            CategorySellerFragment categorySellerFragment = new CategorySellerFragment();
                            bundle.putString("subCat", searchData.getId());
                            bundle.putString("category", searchData.getParentId());
                            categorySellerFragment.setArguments(bundle);
                            childFragmentReplacement(categorySellerFragment, "child");
                        }
                    }
                } catch (Exception e) {

                }
            }
        });

        searchTextInCategoryScreen.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.toString().length() >= 2) {
                    callSearchSellerCategoryApi(s);
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });

        searchTextInCategoryScreen.setAdapter(searchAdapter);
        sliderData = new ArrayList<>();
        categoryAdapter = new ParentCategoryAdapter(getContext(), categories, new Result<CategoryData>() {
            @Override
            public void onSuccess(CategoryData categoryData) {
                ((MainActivity) getActivity()).hideKeyboard();
                ChildCategoriesFragment childCategoriesFragment = new ChildCategoriesFragment();
                Bundle bundleId = new Bundle();
                DataToPref.setData(getContext(), "CATEGORY", new Gson().toJson(categoryData));
                bundleId.putSerializable("categoryData", categoryData);
                childCategoriesFragment.setArguments(bundleId);
                //((MainActivity)getActivity()).setFragment(childCategoriesFragment);
                childFragmentReplacement(childCategoriesFragment, "child");
            }
        });


        setSliderAdapter();

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        categoryRecyclerView.setLayoutManager(gridLayoutManager);
        categoryRecyclerView.setAdapter(categoryAdapter);

        callApiOfSlider();
        //categoryWSCalling();
        return view;
    }

    private void setSliderAdapter() {
        try {
            quickInfoAdapter = new QuickInfoAdapter(getContext(), getChildFragmentManager(), quickInfoDatas, new ItemEventListener<String>() {
                @Override
                public void onItemEventFired(String pos, String userId, final String actualPos) {

                }
            }, sliderData, true);

            GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1);
            categorySliderRecyclerView.setLayoutManager(gridLayoutManager);
            categorySliderRecyclerView.setAdapter(quickInfoAdapter);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void callApiOfSlider() {
        ArrayList<KeyValuePair> keyValuePairs = new ArrayList<>();
        keyValuePairs.add(new KeyValuePair("city_id", ((MainActivity) getActivity()).getTopCitySelectionId()));

        new GetRequestHelper<SliderWrapper>().pingToRequest(Constants.SLIDER_IMAGE, keyValuePairs, ((MainActivity) getActivity()).getHeader(), new SliderWrapper(), new TaskCompleteListener<SliderWrapper>() {
            @Override
            public void onSuccess(SliderWrapper sliderWrapper) {
                if (sliderWrapper.getStatus() == 1) {
                    setSliderData(sliderWrapper.getData());
                }
            }

            @Override
            public void onFailure(ExceptionType exceptions, SliderWrapper sliderWrapper) {
                Log.i("ParentCategoryFragment", " callApiOfSlider(): onFailure exceptions:-" + exceptions.toString());
            }
        });
    }

    private void setSliderData(List<SliderData> data) {
        sliderData.clear();
        sliderData.addAll(data);
        quickInfoAdapter.notifySlider();
    }


    private void callSearchSellerCategoryApi(CharSequence s) {
        new PostRequestHelper<SearchWrapper>().pingToRequest(Constants.ALL_SEARCH_DETAIL,
                "",
                new ArrayList<KeyValuePair>(),
                getHashMapForSearchSellerAndCategory(s),
                ((MainActivity) getActivity()).getHeader(),
                new SearchWrapper(),
                new TaskCompleteListener<SearchWrapper>() {
                    @Override
                    public void onSuccess(SearchWrapper mObject) {
                        searchDatas.clear();
                        searchDatas.addAll(mObject.getData());
                        searchAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, SearchWrapper searchWrapper) {

                    }
                });
    }

    private HashMap<String, String> getHashMapForSearchSellerAndCategory(CharSequence search) {
        HashMap<String, String> hashMapForSellerAndCategory = new HashMap<>();
        hashMapForSellerAndCategory.put("city_id", ((MainActivity) getActivity()).getTopCitySelectionId());
        hashMapForSellerAndCategory.put("search", String.valueOf(search));
        return hashMapForSellerAndCategory;
    }

    private void sellerStatusApiCall() {

        new GetRequestHelper<LoginWrapper>().pingToRequest(Constants.SELLER_STATUS, new ArrayList<KeyValuePair>(), ((MainActivity) getActivity()).getParamWholeParameters(), new LoginWrapper(), new TaskCompleteListener<LoginWrapper>() {
            @Override
            public void onSuccess(LoginWrapper mObject) {
                if (mObject != null) {
                    if (mObject.getStatus() == 1) {
                        SubscriptionPlanFragment subscriptionPlanFragment = new SubscriptionPlanFragment();
                        Bundle bundleSubscription = new Bundle();
                        bundleSubscription.putString("from", "status_check");
                        subscriptionPlanFragment.setArguments(bundleSubscription);
                        ((MainActivity) getActivity()).setFragment(subscriptionPlanFragment, false, "");
                    } else if (mObject.getStatus() == 2) {
                        ((MainActivity) getActivity()).setFragment(new CategorySelectionFragment(), false, "");
                    }
                }
            }

            @Override
            public void onFailure(ExceptionType exceptions, LoginWrapper like) {
            }
        });
    }

    public void categoryWSCalling() {/*
        if (getActivity() != null)
            ((MainActivity) getActivity()).startLoader();*/

        new GetRequestHelper<CategoryWrapper>().pingToRequest(Constants.CATEGORY
                , new ArrayList<KeyValuePair>()
                , ((MainActivity) getActivity()).getHeader()
                , new CategoryWrapper()
                , new TaskCompleteListener<CategoryWrapper>() {
                    @Override
                    public void onSuccess(CategoryWrapper categoryWrapper) {/*
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();*/

                        if (categoryWrapper.getStatus() == 1) {
                            setAdapter(categoryWrapper.getData());
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, CategoryWrapper categoryData) {
                        Log.i("ParentCategoryFragment", " categoryWSCalling() : onFailure exceptions:-" + exceptions.toString());
                    }
                });
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (((MainActivity) getActivity()).getUserForWholeApp() != null) {
            if (((MainActivity) getActivity()).getUserForWholeApp().equals(Enum.setUser.SELLER_BASIC) || ((MainActivity) getActivity()).getUserForWholeApp().equals(Enum.setUser.SELLER_PREMIUM)) {
                sellerStatusApiCall();
            }
        }
        categoryWSCalling();
    }

    public void setAdapter(List<CategoryData> categoryDatas) {
        categories.clear();
        categories.addAll(categoryDatas);
        categoryAdapter.notifyDataSetChanged();
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle("default", Enum.setNevigationIcon.MENU_CITY, false);
    }
}