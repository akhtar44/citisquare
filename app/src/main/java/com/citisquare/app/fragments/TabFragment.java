package com.citisquare.app.fragments;


import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Rect;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.ViewPagerAdapter;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.interfaces.LocationCallback;
import com.citisquare.app.listener.OnBackPressListener;
import com.citisquare.app.pojo.Filter;
import com.citisquare.app.pojo.response.CategoryData;
import com.citisquare.app.pojo.response.SendWrapper;
import com.citisquare.app.utils.DataToPref;
import com.citisquare.app.utils.Debugger;
import com.citisquare.app.utils.InitializeMenu;
import com.citisquare.app.utils.LocationHelper;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import customTabWidget.TabWidget;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

import static com.citisquare.app.R.id.linearLayoutHome;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabFragment extends BaseFragment implements View.OnClickListener, ViewPager.OnPageChangeListener {


    static boolean flag = true;

    public ViewPagerAdapter adapter;
    @BindView(R.id.searchText)
    EditText searchText;
    @BindView(R.id.categorySearchDropDown)
    TextView categorySearchDropDown;
    @BindView(R.id.filterSearchDropDown)
    TextView filterSearchDropDown;
    CategoryDropDown categoryDropDown;
    FilterDropDown filterDropDown;
    @BindView(R.id.vp_pages)
    ViewPager vppages;
    @BindView(R.id.framelayoutContainer)
    FrameLayout framelayoutContainer;
    @BindView(R.id.searchTopFragment)
    FrameLayout searchTopFragment;
    @BindView(R.id.bottom_Tab)
    LinearLayout bottomTab;
    @BindView(R.id.viewMargin)
    View viewMargin;
    ArrayList<CategoryData> categoryList;
    int pos;
    int lastVisible;
    ArrayList<Filter> filtersList;
    ArrayList<Filter> filtersListJob;
    LatLng latLng;
    String catId = "";
    private TabWidget textViewOne, textViewTwo, linearLayoutThree, textViewFour, textViewFive;
    private LocationCallback locationCallback;
    private LocationHelper locationHelper;
    private static SwipeRefreshLayout swipeContainer;
    private Pattern pattern;
    private Matcher matcher;
    public ViewPager pager;
    private AdView mBannerAd;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_carousel, container, false);
        ButterKnife.bind(this, rootView);
        pattern = Pattern.compile(Constants.englishOnly);


        rootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                rootView.getWindowVisibleDisplayFrame(r);
                int screenHeight = rootView.getRootView().getHeight();

                // r.bottom is the position above soft keypad or device button.
                // if keypad is shown, the r.bottom is smaller than that before.
                int keypadHeight = screenHeight - r.bottom;


                if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                    // keyboard is opened
                    viewMargin.setVisibility(View.GONE);
                    bottomTab.setVisibility(View.GONE);
                } else {
                    BaseFragment fragment = (BaseFragment) getChildFragmentManager().findFragmentById(R.id.framelayoutContainer);
                    if (fragment instanceof CommentFragment || fragment instanceof ChatFragment
                            || fragment instanceof PlayVideoFragment || fragment instanceof CategorySelectionFragment
                            || fragment instanceof SubscriptionPlanFragment) {
                        viewMargin.setVisibility(View.GONE);
                        bottomTab.setVisibility(View.GONE);
                    } else {
                        viewMargin.setVisibility(View.VISIBLE);
                        bottomTab.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        textViewOne = (TabWidget) rootView.findViewById(R.id.textViewUnderLineTextOne);
        textViewTwo = (TabWidget) rootView.findViewById(R.id.textViewUnderLineTextTwo);
        linearLayoutThree = (TabWidget) rootView.findViewById(linearLayoutHome);
        textViewFour = (TabWidget) rootView.findViewById(R.id.textViewUnderLineTextFour);
        textViewFive = (TabWidget) rootView.findViewById(R.id.textViewUnderLineTextFive);
        swipeContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeContainer);
        //indicator = (TabPageIndicator) rootView.findViewById(R.id.tpi_header);

        pager = (ViewPager) rootView.findViewById(R.id.vp_pages);
        pager.setOffscreenPageLimit(-1);
        textViewOne.setOnClickListener(this);
        textViewTwo.setOnClickListener(this);
        linearLayoutThree.setOnClickListener(this);
        textViewFour.setOnClickListener(this);
        textViewFive.setOnClickListener(this);
        textViewFive.setVisibility(View.GONE);

// TODO: Add adView to your view hierarchy.

        categoryList = ((MainActivity) getActivity()).getCategoryList();
        filtersList = new InitializeMenu().toGetFilter(getContext());
        filtersListJob = new InitializeMenu().toGetFilterJob(getContext());

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code to refresh the list here.
                //isRefresh = true;
                int index = -1;
                Fragment currentFragment;
                try {
                    index = pager.getCurrentItem();
                    adapter = ((ViewPagerAdapter) pager.getAdapter());
                    currentFragment = adapter.getItem(index);
                    if (currentFragment instanceof ParentHomeFragment) {
                        ParentHomeFragment pendingFrag = (ParentHomeFragment) currentFragment;
                        pendingFrag.RefreshHomescreenOnSwipe();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                // GetOrdersByDate();

            }
        });


        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_purple);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                swipeContainer.setEnabled(true);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
                enableDisableSwipeRefresh(arg0 == ViewPager.SCROLL_STATE_IDLE);
            }
        });

        searchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                ((MainActivity) getActivity()).hideKeyboard();
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    BaseFragment baseFragmentQuickParent = (BaseFragment) adapter.getRegisteredFragment(pager.getCurrentItem());
                    if (baseFragmentQuickParent instanceof ParentHomeFragment) {
                        //   ((ParentHomeFragment) baseFragmentQuickParent).callNewsApi();
                    } else if (baseFragmentQuickParent instanceof ParentToDaysPostFragment) {
                        ((ParentToDaysPostFragment) baseFragmentQuickParent).callQuickOfferApi();
                    } else if (baseFragmentQuickParent instanceof ParentMapFragment) {
                        ((ParentMapFragment) baseFragmentQuickParent).startLocation();
                    } else {
                        BaseFragment baseFragment = (BaseFragment) adapter.getRegisteredFragment(pager.getCurrentItem()).getChildFragmentManager().findFragmentByTag("child");
                        if (baseFragment != null) {
                            if (baseFragment instanceof ChildCategoriesFragment) {

                                final BaseFragment fragment = (BaseFragment) baseFragment.getChildFragmentManager().findFragmentByTag("child");
                                if (fragment instanceof CategorySellerFragment)
                                    ((CategorySellerFragment) fragment).getSellerApiCall();
                                else {
                                    Debugger.e("CHILD CATEGORIES FRAGMENT:::");
                                    if (!DataToPref.getData(getContext(), "CATEGORY").equals("")) {
                                        CategoryData categoryData = new Gson().fromJson(DataToPref.getData(getContext(), "CATEGORY"), CategoryData.class);
                                        if (categoryData.getId().equalsIgnoreCase("1")) {
                                            Bundle bundle = new Bundle();
                                            ParentJobFragment parentJobFragment = new ParentJobFragment();
                                            parentJobFragment.setArguments(bundle);
                                            baseFragment.childFragmentReplacement(parentJobFragment, "child");
                                        } else {
                                            Bundle bundle = new Bundle();
                                            CategorySellerFragment categorySellerFragment = new CategorySellerFragment();
                                            categorySellerFragment.setArguments(bundle);
                                            baseFragment.childFragmentReplacement(categorySellerFragment, "child");
                                        }
                                    } else {
                                        BaseFragment bs = (BaseFragment) adapter.getRegisteredFragment(pager.getCurrentItem());
                                        if (bs != null) {
                                            if (bs instanceof ParentCategoryFragment) {
                                                DataToPref.remove(getContext(), "CATEGORY");
                                                Bundle bundle = new Bundle();
                                                CategorySellerFragment categorySellerFragment = new CategorySellerFragment();
                                                categorySellerFragment.setArguments(bundle);
                                                bs.childFragmentReplacement(categorySellerFragment, "child");
                                            }
                                        }
                                    }
                                }
                            } else if (baseFragment instanceof ParentToDaysPostFragment) {
                                Debugger.e("HELLO IT IS CALLED >> >>> >> >> > >>");
                            }
                        } else {
                            Debugger.e("CHILD ELSE CATEGORIES FRAGMENT:::");
                            BaseFragment bs = (BaseFragment) adapter.getRegisteredFragment(pager.getCurrentItem());
                            if (bs != null) {
                                if (bs instanceof ParentCategoryFragment) {
                                    DataToPref.remove(getContext(), "CATEGORY");
                                    Bundle bundle = new Bundle();
                                    CategorySellerFragment categorySellerFragment = new CategorySellerFragment();
                                    categorySellerFragment.setArguments(bundle);
                                    bs.childFragmentReplacement(categorySellerFragment, "child");
                                }
                            }
                        }
                        return true;
                    }
                    return true;

                }
                return false;
            }
        });
        MobileAds.initialize(getContext(), getContext().getString(R.string.admob_app_id));
        mBannerAd = (AdView) rootView.findViewById(R.id.adView);
        AdRequest request = new AdRequest.Builder().build();
        /*AdRequest request = new AdRequest.Builder()
                .addTestDevice("5328A8875CFDE5D08E631C14615FDECC")  // An example device ID
                .build();*/
        mBannerAd.loadAd(request);
        return rootView;
    }

    private void enableDisableSwipeRefresh(boolean enable) {
        if (swipeContainer != null) {
            swipeContainer.setEnabled(enable);
        }
    }


    public void apiCall() {
        Location latlong = null;
        BaseFragment container = (BaseFragment) getChildFragmentManager().findFragmentById(R.id.framelayoutContainer);
        if (container != null) {
            if (container instanceof JobFragmentMenu) {
                ((JobFragmentMenu) container).apiCall();
            }
        }

        BaseFragment baseFragmentParent = (BaseFragment) adapter.getRegisteredFragment(pager.getCurrentItem());
        if (baseFragmentParent != null) {
            if (baseFragmentParent instanceof ParentCityInfoFragment) {
                ((ParentCityInfoFragment) baseFragmentParent).callCityInfo();
                ((ParentCityInfoFragment) baseFragmentParent).yahooWeatherApi();
            } else if (baseFragmentParent instanceof ParentToDaysPostFragment) {
                ((ParentToDaysPostFragment) baseFragmentParent).callQuickOfferApi();
            } else if (baseFragmentParent instanceof ParentHomeFragment) {
                BaseFragment baseFragmentHome = (BaseFragment) adapter.getRegisteredFragment(pager.getCurrentItem()).getChildFragmentManager().findFragmentByTag("child");
                if (baseFragmentHome != null) {
                    if (baseFragmentHome instanceof CategorySellerFragment)
                        ((CategorySellerFragment) baseFragmentHome).getSellerApiCall();
                    else if (baseFragmentHome instanceof ParentJobFragment) {
                        ((ParentJobFragment) baseFragmentHome).apiCall();
                    }
                } else
                    ((ParentHomeFragment) baseFragmentParent).callNewsApi();
            } else if (baseFragmentParent instanceof ParentMapFragment) {
                //((ParentMapFragment) baseFragmentParent).loadMapselection(latlong);
                ((ParentMapFragment) baseFragmentParent).startLocation();
            } else {
                BaseFragment baseFragment = (BaseFragment) adapter.getRegisteredFragment(pager.getCurrentItem()).getChildFragmentManager().findFragmentByTag("child");
                if (baseFragment != null) {
                    if (baseFragment instanceof CategorySellerFragment) {
                        ((CategorySellerFragment) baseFragment).getSellerApiCall();
                    }
                    if (baseFragment instanceof ChildCategoriesFragment) {
                        final BaseFragment fragment = (BaseFragment) baseFragment.getChildFragmentManager().findFragmentByTag("child");
                        if (fragment != null) {
                            if (fragment instanceof CategorySellerFragment)
                                ((CategorySellerFragment) fragment).getSellerApiCall();
                            else if (fragment instanceof ParentJobFragment) {
                                ((ParentJobFragment) fragment).apiCall();
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).actionBarShow();
    }


    public String getSearchData() {
        if (getActivity() != null) {
            if (searchText != null) {
                if (searchText.getText().toString().trim().length() > 0)
                    return searchText.getText().toString().trim();
                else
                    return "";
            }
        }
        return "";
    }

    public void clearSearch() {
        searchText.setText("");
    }


    public String getFilterData() {
        if (filterSearchDropDown.getText().toString().length() > 0)
            return filterSearchDropDown.getText().toString();
        else
            return "";
    }

    public String getCategoryId() {
        return catId;
    }

    public void clearFilter() {
        ((MainActivity) getActivity()).setFilterPostion(-1);
        filterSearchDropDown.setText(getString(R.string.filter));
    }

    public void setSearchVisibility(Enum.setSearch search) {
        if (searchTopFragment != null && searchText != null && categorySearchDropDown != null && filterSearchDropDown != null) {
            switch (search) {
                case SEARCH:
                    searchTopFragment.setVisibility(View.VISIBLE);
                    searchText.setVisibility(View.VISIBLE);
                    categorySearchDropDown.setVisibility(View.GONE);
                    filterSearchDropDown.setVisibility(View.GONE);
                    break;
                case SEARCH_CATEGORY:
                    searchTopFragment.setVisibility(View.VISIBLE);
                    searchText.setVisibility(View.VISIBLE);
                    categorySearchDropDown.setVisibility(View.VISIBLE);
                    filterSearchDropDown.setVisibility(View.GONE);
                    break;
                case SEARCH_FILTER:
                    searchTopFragment.setVisibility(View.VISIBLE);
                    searchText.setVisibility(View.VISIBLE);
                    categorySearchDropDown.setVisibility(View.GONE);
                    filterSearchDropDown.setVisibility(View.VISIBLE);
                    break;
                case WHOLE_SEARCH:
                    searchTopFragment.setVisibility(View.VISIBLE);
                    searchText.setVisibility(View.VISIBLE);
                    categorySearchDropDown.setVisibility(View.VISIBLE);
                    filterSearchDropDown.setVisibility(View.VISIBLE);
                    break;
                case NOTHING:
                    searchTopFragment.setVisibility(View.GONE);
                    searchText.setVisibility(View.GONE);
                    categorySearchDropDown.setVisibility(View.GONE);
                    filterSearchDropDown.setVisibility(View.GONE);
                    break;
                case ONLYSEARCHHIDE:
                    searchTopFragment.setVisibility(View.VISIBLE);
                    searchText.setVisibility(View.VISIBLE);
                    categorySearchDropDown.setVisibility(View.VISIBLE);
                    filterSearchDropDown.setVisibility(View.VISIBLE);
                    break;
            }
        }
    }

    public void searchFilter() {
        if (flag) {
            flag = false;
            searchText.setVisibility(View.VISIBLE);
            categorySearchDropDown.setVisibility(View.GONE);
            filterSearchDropDown.setVisibility(View.VISIBLE);
        }
    }


    @OnClick(R.id.categorySearchDropDown)
    public void categoryDropDownClicked() {
        ((MainActivity) getActivity()).hideKeyboard();
        int location[] = new int[2];

        categorySearchDropDown.getLocationOnScreen(location);
        categoryDropDown = new CategoryDropDown(getContext(), categorySearchDropDown.getHeight(),
                location, categoryList, new ItemEventListener<Integer>() {
            @Override
            public void onItemEventFired(Integer integer, Integer t2, String actualPos) {
                final BaseFragment baseFragmentQuickParent = (BaseFragment) adapter.getRegisteredFragment(pager.getCurrentItem());
                if (baseFragmentQuickParent instanceof ParentMapFragment) {
                    if (Locale.getDefault().getDisplayLanguage().equals("العربية")) {
                        categorySearchDropDown.setText(categoryList.get(integer).getArName());
                    } else {
                        categorySearchDropDown.setText(categoryList.get(integer).getName());
                    }
                    catId = categoryList.get(integer).getId();
                    ((MainActivity) getActivity()).setFilterPostion(-1);
                    filterSearchDropDown.setText(getContext().getString(R.string.filter));
                    ((ParentMapFragment) baseFragmentQuickParent).startLocation();
                } else {
                    if (Locale.getDefault().getDisplayLanguage().equals("العربية")) {
                        categorySearchDropDown.setText(categoryList.get(integer).getArName());
                    } else {
                        categorySearchDropDown.setText(categoryList.get(integer).getName());
                    }
                    catId = categoryList.get(integer).getId();
                    ((MainActivity) getActivity()).setFilterPostion(-1);
                    filterSearchDropDown.setText(getContext().getString(R.string.filter));
                }
            }
        });
        categoryDropDown.show();
    }

    @OnClick(R.id.filterSearchDropDown)
    public void filterDropDown() {
        int location[] = new int[2];
        ((MainActivity) getActivity()).hideKeyboard();
        filterSearchDropDown.getLocationOnScreen(location);
        final BaseFragment baseFragmentQuickParent = (BaseFragment) adapter.getRegisteredFragment(pager.getCurrentItem());
        if (baseFragmentQuickParent instanceof ParentMapFragment) {

            if (categorySearchDropDown.getText().toString().equalsIgnoreCase("JOB")) {
                filterDropDown = new FilterDropDown(getContext(), filterSearchDropDown.getHeight(),
                        location, filtersListJob, new ItemEventListener<Integer>() {
                    @Override
                    public void onItemEventFired(Integer integer, Integer t2, String actualPos) {
                        filterSearchDropDown.setText(filtersListJob.get(integer).getName());
                        ((ParentMapFragment) baseFragmentQuickParent).startLocation();
                    }
                });
            } else {
                filterDropDown = new FilterDropDown(getContext(), filterSearchDropDown.getHeight(),
                        location, filtersList, new ItemEventListener<Integer>() {
                    @Override
                    public void onItemEventFired(Integer integer, Integer t2, String actualPos) {
                        filterSearchDropDown.setText(filtersList.get(integer).getName());
                        ((ParentMapFragment) baseFragmentQuickParent).startLocation();
                    }
                });
            }
        } else {
            final BaseFragment baseFragment = (BaseFragment) adapter.getRegisteredFragment(pager.getCurrentItem()).getChildFragmentManager().findFragmentByTag("child");
            if (baseFragment != null) {
                final BaseFragment fragment = (BaseFragment) baseFragment.getChildFragmentManager().findFragmentByTag("child");
                if (fragment != null) {
                    if (fragment instanceof ParentJobFragment) {
                        filterDropDown = new FilterDropDown(getContext(), filterSearchDropDown.getHeight(),
                                location, filtersListJob, new ItemEventListener<Integer>() {
                            @Override
                            public void onItemEventFired(Integer integer, Integer t2, String actualPos) {
                                filterSearchDropDown.setText(filtersListJob.get(integer).getName());
                                ((ParentJobFragment) fragment).apiCall();
                            }
                        });
                    } else {
                        filterDropDown = new FilterDropDown(getContext(), filterSearchDropDown.getHeight(),
                                location, filtersList, new ItemEventListener<Integer>() {
                            @Override
                            public void onItemEventFired(Integer integer, Integer t2, String actualPos) {
                                filterSearchDropDown.setText(filtersList.get(integer).getName());
                                Debugger.e("TOP ONE");
                                if (filtersList.get(integer).getName().equals("Near By")) {
                                    Debugger.e("NEW ONE SET LAST");
                                    startlocationlisting(new LocationCallback() {
                                        @Override
                                        public void onLocationError() {

                                        }

                                        @Override
                                        public void onLocationRecive(Location location) {
                                            if (getActivity() != null)
                                                ((MainActivity) getActivity()).setLatLng(new LatLng(location.getLatitude(), location.getLongitude()));
                                            if (fragment instanceof CategorySellerFragment)
                                                ((CategorySellerFragment) fragment).getSellerApiCall();
                                        }
                                    });

                                } else if (fragment instanceof CategorySellerFragment)
                                    ((CategorySellerFragment) fragment).getSellerApiCall();
                            }
                        });
                    }
                } else {
                    filterDropDown = new FilterDropDown(getContext(), filterSearchDropDown.getHeight(),
                            location, filtersList, new ItemEventListener<Integer>() {
                        @Override
                        public void onItemEventFired(Integer integer, Integer t2, String actualPos) {
                            final BaseFragment fragment = (BaseFragment) baseFragment.getChildFragmentManager().findFragmentByTag("child");
                            filterSearchDropDown.setText(filtersList.get(integer).getName());
                            if (filtersList.get(integer).getName().equals("Near By")) {
                                Debugger.e("FIRST NEAR BY CALL");
                                startlocationlisting(new LocationCallback() {
                                    @Override
                                    public void onLocationError() {

                                    }

                                    @Override
                                    public void onLocationRecive(Location location) {
                                        ((MainActivity) getActivity()).setLatLng(new LatLng(location.getLatitude(), location.getLongitude()));
                                        if (fragment instanceof CategorySellerFragment)
                                            ((CategorySellerFragment) fragment).getSellerApiCall();
                                        else if (baseFragment instanceof CategorySellerFragment)
                                            ((CategorySellerFragment) baseFragment).getSellerApiCall();
                                    }
                                });

                            } else if (fragment instanceof CategorySellerFragment)
                                ((CategorySellerFragment) fragment).getSellerApiCall();
                            else if (baseFragment instanceof CategorySellerFragment) {
                                ((CategorySellerFragment) baseFragment).getSellerApiCall();
                            }
                        }
                    });
                }
            } else {
                filterDropDown = new FilterDropDown(getContext(), filterSearchDropDown.getHeight(),
                        location, filtersList, new ItemEventListener<Integer>() {
                    @Override
                    public void onItemEventFired(Integer integer, Integer t2, String actualPos) {
                        filterSearchDropDown.setText(filtersList.get(integer).getName());
                        if (filtersList.get(integer).getName().equals("Near By")) {
                            Debugger.e("SECOND NEAR BY CALL");
                        }
                    }
                });
            }
        }

        filterDropDown.show();
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Note that we are passing childFragmentManager, not FragmentManager
        adapter = new ViewPagerAdapter(getResources(), getChildFragmentManager(), getContext());
        pager.setAdapter(adapter);
        //indicator.setViewPager(pager);
        pager.addOnPageChangeListener(this);
        setSearchVisibility(Enum.setSearch.NOTHING);
        onPageSelected(0);
    }

    /**
     * Retrieve the currently visible Tab Fragment and propagate the onBackPressed callback
     *
     * @return true = if this fragment and/or one of its associates Fragment can handle the backPress
     */


    public boolean onBackPressed() {
        // currently visible tab Fragment
        Debugger.e("...TAB BACK IS CALLED...");

        ((MainActivity) getActivity()).hideKeyboard();
        BaseFragment fragmentBase = (BaseFragment) adapter.getRegisteredFragment(pager.getCurrentItem());
        if (fragmentBase != null) {
            fragmentBase.setHeaderTitle();
        }
        setSearchVisibility(Enum.setSearch.SEARCH);

        BaseFragment baseFragmentProfile = (BaseFragment) getChildFragmentManager().findFragmentById(R.id.framelayoutContainer);

        if (fragmentBase != null) {
            if (fragmentBase instanceof ParentHomeFragment || fragmentBase instanceof ParentCategoryFragment)
                setSearchVisibility(Enum.setSearch.NOTHING);
            /*if(fragmentBase instanceof ChildCategoriesFragment)
                setSearchVisibility(Enum.setSearch.SEARCH);*/

            BaseFragment fragmentOut = (BaseFragment) adapter.getRegisteredFragment(pager.getCurrentItem());

            if (fragmentOut != null) {
                Debugger.e("1 CALL" + fragmentOut.getClass().getName());
                BaseFragment headerChildOut = (BaseFragment) fragmentOut.getChildFragmentManager().findFragmentByTag("child");
                if (headerChildOut != null) {
                    BaseFragment headerChildSecondOut = (BaseFragment) headerChildOut.getChildFragmentManager().findFragmentByTag("child");
                    if (headerChildSecondOut != null) {
                        if (headerChildSecondOut instanceof CategorySellerFragment) {
                            setSearchVisibility(Enum.setSearch.SEARCH);
                        } else if (headerChildSecondOut instanceof ParentJobFragment) {
                            setSearchVisibility(Enum.setSearch.SEARCH);
                        }
                    }
                }
            }
        }


        if (baseFragmentProfile != null) {

            if (baseFragmentProfile instanceof CategorySelectionFragment) {
                getActivity().finish();
                return false;
            }
/*
            if (baseFragmentProfile instanceof WriteReviewFragment || baseFragmentProfile instanceof RateAndReviewListingFragment) {
                List<Fragment> fragments = getChildFragmentManager().getFragments();
                if (fragments.size() > 0 && fragments.get(fragments.size() - 2) instanceof RateAndReviewFragment) {
                    ((RateAndReviewFragment) fragments.get(fragments.size() - 2)).callApi();
                }
            }*/

            BaseFragment fragment = (BaseFragment) adapter.getRegisteredFragment(pager.getCurrentItem());

            if (fragment != null) {
                Debugger.e("1 CALL" + fragment.getClass().getName());

                fragment.setHeaderTitle();

                BaseFragment headerChild = (BaseFragment) fragment.getChildFragmentManager().findFragmentByTag("child");
                if (headerChild != null) {
                    Debugger.e("2 CALL" + headerChild.getClass().getName());
                    setSearchVisibility(Enum.setSearch.SEARCH);

                    BaseFragment headerChildSecond = (BaseFragment) headerChild.getChildFragmentManager().findFragmentByTag("child");
                    if (headerChild instanceof ChildCategoriesFragment) {
                        setSearchVisibility(Enum.setSearch.SEARCH);
                        //headerChildSecond.setHeaderTitle();
                    }

                    if (headerChildSecond != null) {
                        if (headerChildSecond instanceof CategorySellerFragment) {
                            Debugger.e("3 SELLER" + headerChildSecond.getClass().getName());
                            setSearchVisibility(Enum.setSearch.SEARCH_FILTER);
                            headerChildSecond.setHeaderTitle();
                        } else if (headerChildSecond instanceof ParentJobFragment) {
                            Debugger.e("3 JOB" + headerChildSecond.getClass().getName());
                            setSearchVisibility(Enum.setSearch.SEARCH_FILTER);
                            headerChildSecond.setHeaderTitle();
                        }
                    }
                }
            }
        }

        viewMargin.setVisibility(View.VISIBLE);
        bottomTab.setVisibility(View.VISIBLE);

        if (pager.getVisibility() == View.INVISIBLE) {
            searchTopFragment.setVisibility(View.GONE);
            //  searchTop.setVisibility(View.VISIBLE);
            BaseFragment baseFragmentTitle = (BaseFragment) getChildFragmentManager().findFragmentById(R.id.framelayoutContainer);


            if (baseFragmentTitle != null) {
                if (baseFragmentProfile instanceof AddBranchFragment) {
                    //  baseFragmentProfile.setHeaderTitle();

                    if (getChildFragmentManager().getBackStackEntryCount() != 0)
                        getChildFragmentManager().popBackStackImmediate();
                } else {
                    if (getChildFragmentManager().getBackStackEntryCount() != 0)
                        getChildFragmentManager().popBackStackImmediate();

                    BaseFragment baseFragment = (BaseFragment) getChildFragmentManager().findFragmentById(R.id.framelayoutContainer);
                    if (baseFragment != null)
                        baseFragment.setHeaderTitle();
                }
            }
            Debugger.e("CHILD COUNTER :::::::::: " + getChildFragmentManager().getBackStackEntryCount());


            if (getChildFragmentManager().getBackStackEntryCount() == 0) {
                pager.setVisibility(View.VISIBLE);
                viewMargin.setVisibility(View.VISIBLE);
                searchTopFragment.setVisibility(View.VISIBLE);
                bottomTab.setVisibility(View.VISIBLE);
                framelayoutContainer.setVisibility(View.GONE);

                BaseFragment fragmentBasepager = (BaseFragment) adapter.getRegisteredFragment(pager.getCurrentItem());
                if (fragmentBasepager != null) {
                    if (fragmentBasepager instanceof ParentCityInfoFragment) {
                        searchTopFragment.setVisibility(View.GONE);
                    } else if (fragmentBasepager instanceof ParentMapFragment) {
                        setSearchTitle(3);
                    }
                }
                return true;
            }
           /* BaseFragment baseFragment = (BaseFragment) getChildFragmentManager().findFragmentById(R.id.framelayoutContainer);
            baseFragment.setHeaderTitle();*/

            return true;
        }


        OnBackPressListener currentFragment = (OnBackPressListener) adapter.getRegisteredFragment(pager.getCurrentItem());

        if (currentFragment != null) {
            return currentFragment.onBackPressed();
        }

        // this Fragment couldn't handle the onBackPressed call
        return false;
    }


    public void setSearchTitle(int pos) {
        switch (pos) {
            case 0:
                setSearchVisibility(Enum.setSearch.NOTHING);
                break;
            case 1:
                setSearchVisibility(Enum.setSearch.NOTHING);
                break;
            case 2:
                setSearchVisibility(Enum.setSearch.NOTHING);
                break;
            case 3:
                setSearchVisibility(Enum.setSearch.WHOLE_SEARCH);
                break;
            case 4:
                setSearchVisibility(Enum.setSearch.NOTHING);
                break;
        }
    }

    public void setFragment(BaseFragment fragment, boolean flag, String tag) {

        Debugger.e("FRAGMENT NAME IS ::::::::::: " + fragment.getClass().getName());
        if (fragment instanceof CommentFragment || fragment instanceof ChatFragment || fragment instanceof PlayVideoFragment || fragment instanceof CategorySelectionFragment || fragment instanceof SubscriptionPlanFragment) {
            Debugger.e("CALL ");
            viewMargin.setVisibility(View.GONE);
            bottomTab.setVisibility(View.GONE);
        } else {
            viewMargin.setVisibility(View.VISIBLE);
            bottomTab.setVisibility(View.VISIBLE);
        }

        framelayoutContainer.setVisibility(View.VISIBLE);
        pager.setVisibility(View.INVISIBLE);
        setSearchVisibility(Enum.setSearch.NOTHING);


        if (fragment instanceof WebViewFragment || fragment instanceof MakeYourBusinessLocationFragment || fragment instanceof RateAndReviewListingFragment || fragment instanceof WriteReviewFragment ||
                fragment instanceof RateAndReviewDetailFragment || fragment instanceof RateAndReviewFragment ||
                fragment instanceof BranchPlanFragment || fragment instanceof AboutTheAppFragment || fragment instanceof AboutUsFragment ||
                fragment instanceof SellerEditFragment || fragment instanceof VisitorProfileEditFragment || fragment instanceof OtherMapPathFragment
                || fragment instanceof CommentFragment || fragment instanceof ProfileFragment || fragment instanceof AddBranchFragment || fragment instanceof BranchesFragment
                || fragment instanceof SendMessageFragment || fragment instanceof PlayVideoFragment || fragment instanceof ChatFragment
                || fragment instanceof SelectMemberFragment || fragment instanceof DetailsHomeFragment) {
        } else {
            //getChildFragmentManager().popBackStack(fragment.getClass().getName(), 1);
            getChildFragmentManager().popBackStackImmediate();
        }

        if (tag.equals("profileOwn")) {
            getChildFragmentManager().popBackStackImmediate();
        }

        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();

        if (flag) {
            fragmentTransaction.replace(R.id.framelayoutContainer, fragment, fragment.getClass().getName())
                    .addToBackStack(fragment.getClass().getName())
                    .commitAllowingStateLoss();
        } else {
            fragmentTransaction.add(R.id.framelayoutContainer, fragment, fragment.getClass().getName())
                    .addToBackStack(fragment.getClass().getName())
                    .commitAllowingStateLoss();
        }
    }

    public void writeRateAndReview() {
        BaseFragment fragment = (BaseFragment) getChildFragmentManager().
                findFragmentById(R.id.framelayoutContainer);
        if (fragment != null) {
            if (fragment instanceof RateAndReviewFragment) {
                ((RateAndReviewFragment) fragment).deletePreviewRateAndAddNew();
            }
        }
    }


    public void myInfoDataIsValid() {
        BaseFragment fragment = (BaseFragment) getChildFragmentManager().
                findFragmentById(R.id.framelayoutContainer);

        if (fragment instanceof SellerEditFragment) {
            ((SellerEditFragment) fragment).editSellerProfile();
            /*if (((SellerEditFragment) fragment).isValid()) {
                ((SellerEditFragment) fragment).editSellerProfile();
            }*/
        }

        if (fragment instanceof VisitorProfileEditFragment) {
            if (((VisitorProfileEditFragment) fragment).isValid()) {
                ((VisitorProfileEditFragment) fragment).editVisitorProfile();
            }
        }

        /*if (fragment instanceof MyInfoEditModeFragment) {
            if (((MyInfoEditModeFragment) fragment).isValid()) {
                ((MyInfoEditModeFragment) fragment).editProfile();
            }
        }*/
    }

    public boolean selectUserSend() {
        BaseFragment fragment = (BaseFragment) getChildFragmentManager().
                findFragmentById(R.id.framelayoutContainer);

        if (fragment instanceof SelectMemberFragment) {
            if (((SelectMemberFragment) fragment).selectedMembers().size() > 0) {
                sendGroupMessage(((SelectMemberFragment) fragment).selectedMembers(), ((SelectMemberFragment) fragment).getMessage());

                return true;
            }
        }
        return false;
    }


    public void sendGroupMessage(ArrayList<String> strings, String msg) {
        if (getActivity() != null)
            ((MainActivity) getActivity()).startLoader();

        HashMap<String, String> hashMap = new HashMap<>();
        String id = "";
        for (int i = 0; i < strings.size(); i++) {
            id += strings.get(i) + ",";
        }
        hashMap.put("receiver_id", id);
        hashMap.put("message", msg);
        new PostRequestHelper<SendWrapper>().pingToRequest(Constants.SEND_MESSAGE
                , null
                , null
                , hashMap
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new SendWrapper()
                , new TaskCompleteListener<SendWrapper>() {
                    @Override
                    public void onSuccess(SendWrapper mObject) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();
                        if (mObject.getStatus() == 1) {
                            onBackPressed();
                            onBackPressed();
                            Debugger.e("ALL RECEIVERS ARE::::" + mObject.toString());
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, SendWrapper sendWrapper) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();
                        if (sendWrapper != null) {
                            if (sendWrapper.getStatus() == -1)
                                ((MainActivity) getActivity()).invalidToken();
                        }

                    }
                });
    }


    @Override
    public void onClick(View view) {

        if (getChildFragmentManager().getBackStackEntryCount() != 0) {
            getChildFragmentManager().popBackStackImmediate(null, 0);
        }
        switch (view.getId()) {
            case R.id.textViewUnderLineTextOne:
                setSearchVisibility(Enum.setSearch.NOTHING);
                onPageSelected(0);
                break;
            case R.id.textViewUnderLineTextTwo:

                ((MainActivity) getActivity()).setSelectionPostion(1);
                //clearBackStack(parentCategoryFragment);
                // removeChildFragment();
                Debugger.e("......................TAB ONE IS SELECTED..............");
                onPageSelected(1);
                removeChildFragment();
                setSearchVisibility(Enum.setSearch.NOTHING);
                pager.getAdapter().notifyDataSetChanged();

                break;
            case linearLayoutHome:
                setSearchVisibility(Enum.setSearch.NOTHING);
                ((MainActivity) getActivity()).setSelectionPostion(0);
                onPageSelected(2);
                break;
            case R.id.textViewUnderLineTextFour:
                setSearchVisibility(Enum.setSearch.WHOLE_SEARCH);
                onPageSelected(3);
                break;
            case R.id.textViewUnderLineTextFive:
                setSearchVisibility(Enum.setSearch.WHOLE_SEARCH);
                onPageSelected(4);
                break;
        }
    }

    public void removeChildFragment() {
        if (adapter != null && adapter.getItem(1).getChildFragmentManager().getBackStackEntryCount() != 0)
            adapter.getItem(1).getChildFragmentManager().popBackStackImmediate();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

        if (getActivity() != null) {
            getChildFragmentManager().popBackStackImmediate();
        }

        clearSearch();
        if (position == 0) {
            if (getActivity() != null) {
                ((MainActivity) getActivity()).stopLoader();
                ((MainActivity) getActivity()).setSelectionPostion(0);
            }
            BaseFragment fragment = (BaseFragment) adapter.getRegisteredFragment(pager.getCurrentItem());
            if (fragment != null) {
                if (fragment instanceof ParentHomeFragment) {
                    ((ParentHomeFragment) fragment).callNewsApi();
                }
            }

        }
        if (position == 1) {
            //removeChildFragment();
            if (getActivity() != null) {
                ((MainActivity) getActivity()).stopLoader();
                ((MainActivity) getActivity()).setSelectionPostion(1);
            }


            BaseFragment fragment = (BaseFragment) adapter.getRegisteredFragment(pager.getCurrentItem());
            if (fragment != null) {
                if (fragment instanceof ParentCategoryFragment) {
                    ((ParentCategoryFragment) fragment).categoryWSCalling();
                    swipeContainer.setEnabled(false);
                }
            }
        }


        if (position == 2) {
            BaseFragment fragment = (BaseFragment) adapter.getRegisteredFragment(pager.getCurrentItem());
            if (fragment != null) {
                if (fragment instanceof ParentToDaysPostFragment) {
                    ((ParentToDaysPostFragment) fragment).callQuickOfferApi();
                    swipeContainer.setEnabled(false);
                    searchTopFragment.setVisibility(View.GONE);
                }
            }
        }

        if (position == 3) {
            BaseFragment fragment = (BaseFragment) adapter.getRegisteredFragment(pager.getCurrentItem());
            if (fragment != null) {
                if (fragment instanceof ParentMapFragment) {
                    ((ParentMapFragment) fragment).startLocation();
                    searchTopFragment.setVisibility(View.VISIBLE);
                    swipeContainer.setEnabled(false);
                }
            }
        }

        if (position == 4) {
            BaseFragment fragment = (BaseFragment) adapter.getRegisteredFragment(pager.getCurrentItem());
            if (fragment != null) {
                if (fragment instanceof ParentCityInfoFragment) {
                    ((ParentCityInfoFragment) fragment).callCityInfo();
                    ((ParentCityInfoFragment) fragment).yahooWeatherApi();
                }
            }
        }
        setHeaderCustom();
        Debugger.e("POSITION ::::" + position);
        setSearchTitle(position);
        ((MainActivity) getActivity()).hideKeyboard();
        viewMargin.setVisibility(View.VISIBLE);
        bottomTab.setVisibility(View.VISIBLE);
        framelayoutContainer.setVisibility(View.INVISIBLE);
        pager.setVisibility(View.VISIBLE);
        //searchTopFragment.setVisibility(View.VISIBLE);
        pos = position;
        pager.setCurrentItem(position);
        setSelection(position);
    }

    public void setHeaderCustom() {
        BaseFragment fragment = (BaseFragment) adapter.getRegisteredFragment(pager.getCurrentItem());
        if (fragment != null) {
            BaseFragment headerChild = (BaseFragment) fragment.getChildFragmentManager().findFragmentByTag("child");
            if (headerChild != null) {
                BaseFragment headerChildSecond = (BaseFragment) headerChild.getChildFragmentManager().findFragmentByTag("child");
                if (headerChildSecond != null)
                    headerChildSecond.setHeaderTitle();
                else
                    headerChild.setHeaderTitle();
            } else {
                fragment.setHeaderTitle();
            }
        }
    }


    public void setSelection(int position) {

        setHeaderTitle();
        Debugger.e("SET SELECTION IN TAB ::" + position);
        textViewOne.changeColor(Color.WHITE, R.drawable.unselect_tab);
        textViewTwo.changeColor(Color.WHITE, R.drawable.unselect_tab);
        linearLayoutThree.changeColor(Color.WHITE, R.drawable.unselect_tab);
        textViewFour.changeColor(Color.WHITE, R.drawable.unselect_tab);
        textViewFive.changeColor(Color.WHITE, R.drawable.unselect_tab);

        if (position == 0) {
            textViewOne.selectedTab(getContext().getResources().getColor(R.color.dark_blue)
                    , R.drawable.select_tab);
            linearLayoutThree.visibleBlink(false);
        } else if (position == 1) {
            textViewTwo.selectedTab(getContext().getResources().getColor(R.color.dark_blue)
                    , R.drawable.select_tab);
            linearLayoutThree.visibleBlink(false);
        } else if (position == 2) {
            linearLayoutThree.selectedTab(getContext().getResources().getColor(R.color.dark_blue)
                    , R.drawable.select_tab);
            linearLayoutThree.visibleBlink(false);
        } else if (position == 3) {
            textViewFour.selectedTab(getContext().getResources().getColor(R.color.dark_blue)
                    , R.drawable.select_tab);
            linearLayoutThree.visibleBlink(false);
        } else if (position == 4) {
            textViewFive.selectedTab(getContext().getResources().getColor(R.color.dark_blue)
                    , R.drawable.select_tab);
            linearLayoutThree.visibleBlink(false);
        }

    }


    /*GPS LOCATION*/
    public void startlocationlisting(LocationCallback locationCallback) {
        this.locationCallback = locationCallback;
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(Constants.ACCESS_FINE_LOCATION, Constants.LOCATIONREQUESTCODE);
            } else {
                startLisning();
            }
        } else startLisning();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Debugger.e("PERMISSION ::" + requestCode);
        switch (requestCode) {
            case Constants.LOCATIONREQUESTCODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLisning();
                } else {

                }
                return;
            }
        }
    }

    private void startLisning() {
        locationHelper = new LocationHelper(this, new LocationHelper.ProvideLocation() {
            @Override
            public void currentLocation(final Location location) {
                locationCallback.onLocationRecive(location);
            }

            @Override
            public void onError() {
                locationCallback.onLocationError();
            }
        });
    }

    public void stoplocationlsting() {
        if (locationHelper != null) {
            locationHelper.stopLocationUpdates();
        }
    }


    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public void setVisibleHurryUp(boolean b) {
        linearLayoutThree.setHurryUp(b);
    }

    public static void hideRefreshIcon(boolean enableRefresh) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                TabFragment.swipeContainer.setRefreshing(enableRefresh);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}