package com.citisquare.app.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.controls.CustomButton;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.LocationCallback;
import com.citisquare.app.pojo.addresslocation.Address;
import com.citisquare.app.pojo.response.LoginData;
import com.citisquare.app.pojo.response.LoginWrapper;
import com.citisquare.app.utils.DataToPref;
import com.citisquare.app.utils.Debugger;
import com.citisquare.app.utils.LocationHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import serviceCalling.builder.KeyValuePair;
import serviceCalling.builder.ServiceParameter;
import serviceCalling.helper.GetRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 23/3/17.
 */

public class MakeYourBusinessLocationFragment extends BaseFragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    @BindView(R.id.mapForLocation)
    MapView mapView;
    @BindView(R.id.locationMapButtonSelectlocation)
    CustomButton locationMapButtonSelectlocation;
    private View view;
    private GoogleMap googleMap;
    private Polyline polyline;
    private LocationCallback locationCallback;
    private LocationHelper locationHelper;
    LatLng global;
    OnLocationSelectionListener onLocationSelectionListener;

    public OnLocationSelectionListener getOnLocationSelectionListener() {
        return onLocationSelectionListener;
    }

    public void setOnLocationSelectionListener(OnLocationSelectionListener onLocationSelectionListener) {
        this.onLocationSelectionListener = onLocationSelectionListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            global = new LatLng(0.0d, 0.0d);
            MapsInitializer.initialize(getActivity());
            view = inflater.inflate(R.layout.locationlayout, container, false);
            ButterKnife.bind(this, view);

            mapView.onCreate(savedInstanceState);

            if (mapView != null) {
                mapView.getMapAsync(this);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        //markerList = new ArrayList<>();
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle(getString(R.string.title_setBusinessLocation), Enum.setNevigationIcon.BACK, true);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        if (googleMap != null) {
            this.googleMap = googleMap;
            setUpMap();
            final LatLngBounds.Builder builder = new LatLngBounds.Builder();

            if (DataToPref.getData(getContext(), "makeYourLocation").length() > 0) {
                String[] latlong = DataToPref.getData(getContext(), "makeYourLocation").split(",");
                double latitude = Double.parseDouble(latlong[0]);
                double longitude = Double.parseDouble(latlong[1]);

                LatLng latLng = new LatLng(latitude, longitude);
                setMarkerLocation(googleMap, latLng);
            } else if (((MainActivity) getActivity()).getData(getContext()) != null) {
                LoginWrapper loginWrapper = ((MainActivity) getActivity()).getData(getContext());
                LoginData loginData = loginWrapper.getData();

                LatLng locationLatlng = new LatLng(Double.parseDouble(loginData.getLatitude()), Double.parseDouble(loginData.getLongitude()));

                setMarkerLocation(googleMap, locationLatlng);
            }

            googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    googleMap.clear();
                    googleMap.addMarker(new MarkerOptions()
                            .position(latLng)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.current_location))
                            .title("CityInfo"));
                    global = latLng;

                }
            });
        }
    }

    private void setMarkerLocation(GoogleMap googleMap, LatLng locationLatlng) {
        global = locationLatlng;
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                locationLatlng, 11));
        googleMap.addMarker(new MarkerOptions()
                .position(locationLatlng)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.current_location))
                .title("CityInfo"));

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(locationLatlng)      // Sets the center of the map to location user
                .zoom(13)// Sets the zoom

                .build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    public void setUpMap() {
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);

        googleMap.setOnMarkerClickListener(this);
    }

    public void startLocation() {
        startlocationlisting(new LocationCallback() {
            @Override
            public void onLocationError() {

            }

            @Override
            public void onLocationRecive(Location location) {
                if (getActivity() != null) {
                    ((MainActivity) getActivity()).setLatLng(new LatLng(location.getLatitude(), location.getLongitude()));
                    LatLng locationLatlng = new LatLng(location.getLatitude(), location.getLongitude());

                    setMarkerLocation(googleMap, locationLatlng);
                }
            }
        });
    }

    public void startlocationlisting(LocationCallback locationCallback) {
        this.locationCallback = locationCallback;
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    ) {
                requestPermissions(Constants.ACCESS_FINE_LOCATION, Constants.LOCATIONREQUESTCODE);
            } else {
                startLisning();
            }

        } else startLisning();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Debugger.e("PERMISSION ::" + requestCode);
        switch (requestCode) {
            case Constants.LOCATIONREQUESTCODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLisning();
                } else {

                }
                return;
            }
        }
    }

    private void startLisning() {
        locationHelper = new LocationHelper(this, new LocationHelper.ProvideLocation() {
            @Override
            public void currentLocation(final Location location) {
                locationCallback.onLocationRecive(location);
            }

            @Override
            public void onError() {
                locationCallback.onLocationError();
            }
        });
    }


    @Override
    public void onResume() {

        if (mapView != null)
            mapView.onResume();

        if (DataToPref.getData(getContext(), "makeYourLocation").length() > 0) {

        } else if (((MainActivity) getActivity()).getData(getContext()) != null) {


        } else {
            startLocation();
        }

        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mapView != null)
            mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        if (mapView != null)
            mapView.onLowMemory();
        super.onLowMemory();
    }

    public String getAdressFromLatlong(LatLng latLng) {
        // DebugLog.e("latlong is::::"+ latLng);

        Geocoder geocoder;
        if (latLng.latitude != 0.0 && latLng.longitude != 0.0) {
            List<android.location.Address> addresses = null;
            geocoder = new Geocoder(getActivity(), Locale.getDefault());
            try {
                addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (addresses != null && !addresses.isEmpty()) {
                String area = "";
                if (addresses.get(0).getAddressLine(0) != null) {
                    area = addresses.get(0).getAddressLine(0);
                    if (addresses.get(0).getAddressLine(1) != null) {
                        area = area + " " + addresses.get(0).getAddressLine(1);

                    }
                }
                return area;
                //editTextEnterAddress.setText(area);
                //Once address is select return to add item detail fragment
            } /*else {
                textViewSelectAddress.setText("We are not serving in this area");
            }*/
        }
        return "";
    }

    public void getLocation(final LatLng latlng) {
        if (getActivity() != null)
            ((MainActivity) getActivity()).startLoader();

        if (getAdressFromLatlong(latlng).equals("")) {
            if (getActivity() != null) {
                ((MainActivity) getActivity()).stopLoader();

                //((MainActivity) getActivity()).messageAlert(getString(R.string.message_pelaseSelectValidLocation));
            }
        } else {
            new GetRequestHelper<Address>().pingToRequest("http://maps.googleapis.com/maps/api/geocode/json?address=" +
                            getAdressFromLatlong(latlng) + "&sensor=true"
                    , new ArrayList<KeyValuePair>()
                    , new ServiceParameter()
                    , new Address()
                    , new TaskCompleteListener<Address>() {
                        @Override
                        public void onSuccess(Address mObject) {
                            try {
                                Debugger.e("GET LOCATION:::" + mObject);
                                if (mObject.getStatus().equalsIgnoreCase("ok")) {
                                /*lat = mObject.getResults().get(0).getGeometry().getLocationAndPayment().getLat();
                                lng = mObject.getResults().get(0).getGeometry().getLocationAndPayment().getLng();
                                sellerSignup(lat, lng);*/
                                    if (getActivity() != null) {
                                        if (onLocationSelectionListener != null) {
                                            if (getActivity() != null) {

                                                DataToPref.setData(getContext(), "makeYourLocation", latlng.latitude + "," + latlng.longitude);
                                                ((MainActivity) getActivity()).stopLoader();
                                                onLocationSelectionListener.locationSelect(latlng);
                                                //getActivity().onBackPressed();
                                            }
                                        }
                                    }
                                } else {
                                    ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pelaseSelectValidLocation));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(ExceptionType exceptions, Address address) {

                        }
                    });
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    @OnClick(R.id.locationMapButtonSelectlocation)
    public void onViewClicked() {
        getLocation(global);
        //getActivity().onBackPressed();
    }


    public interface OnLocationSelectionListener {
        void locationSelect(LatLng location);
    }

}