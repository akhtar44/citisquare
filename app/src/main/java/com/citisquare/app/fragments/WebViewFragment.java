package com.citisquare.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.pojo.WebViewData;
import com.citisquare.app.pojo.WebViewWrapper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import serviceCalling.builder.KeyValuePair;
import serviceCalling.helper.GetRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 19/1/17.
 */

public class WebViewFragment extends BaseFragment {

    @BindView(R.id.layout_about_us_webview)
    WebView layoutAboutUsWebview;
    @BindView(R.id.framelayoutPlaceholder)
    FrameLayout framelayoutPlaceholder;

    Bundle bundle;
    String bundleString;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.about_layout, container, false);
        ButterKnife.bind(this, view);

        bundle = getArguments();
        if (bundle != null) {
            bundleString = bundle.getString("from");
        }
        WebSettings webSettings = layoutAboutUsWebview.getSettings();
        webSettings.setJavaScriptEnabled(true);

        webViewCmsApicall();
        return view;
    }

    private void webViewCmsApicall() {
        if (getActivity() != null) {
            ((MainActivity) getActivity()).startLoader();
        }
        List<KeyValuePair> keyValuePairs = new ArrayList<>();
        keyValuePairs.add(new KeyValuePair("type", bundleString.equals("1") ? "T" : "P"));
        new GetRequestHelper<WebViewWrapper>().pingToRequest(Constants.CMS_API_WEB_VIEW, keyValuePairs, ((MainActivity) getActivity()).getHeader(), new WebViewWrapper(), new TaskCompleteListener<WebViewWrapper>() {
            @Override
            public void onSuccess(WebViewWrapper webViewWrapper) {
                if (getActivity() != null) {
                    ((MainActivity) getActivity()).stopLoader();
                }
                if (webViewWrapper.getStatus() == 1) {
                    setViewData(webViewWrapper.getData());
                }
            }

            @Override
            public void onFailure(ExceptionType exceptions, WebViewWrapper webViewWrapper) {
                if (getActivity() != null) {
                    ((MainActivity) getActivity()).stopLoader();
                }
            }
        });
    }

    private void setViewData(WebViewData data) {
        String mime = "text/html";
        String encoding = "utf-8";

        //webView.setWebViewClient(new myWebClient());
        layoutAboutUsWebview.loadDataWithBaseURL(null, data.getContent(), mime, encoding, null);
    }

    @Override
    public void setHeaderTitle() {
        if (bundleString.equalsIgnoreCase("1")) {
            ((MainActivity) getActivity()).setTitle(getString(R.string.termsAndCondition), Enum.setNevigationIcon.BACK, true);
        } else {
            ((MainActivity) getActivity()).setTitle(getString(R.string.privacyPolicy), Enum.setNevigationIcon.BACK, true);
        }
    }
}
