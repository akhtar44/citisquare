package com.citisquare.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.ParentCityInfoAdapter;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.pojo.response.CityInfoData;
import com.citisquare.app.pojo.response.CityInfoWrapper;
import com.citisquare.app.pojo.yahoo.Condition;
import com.citisquare.app.pojo.yahoo.YahooWeather;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import serviceCalling.builder.KeyValuePair;
import serviceCalling.helper.GetRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink16 on 27/5/16.
 */
public class ParentCityInfoFragment extends BaseFragment {


    List<CityInfoData> cityList;
    GridLayoutManager gridLayoutManager;
    ParentCityInfoAdapter parentCityInfoAdapter;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.cityHeaderCityName)
    CustomTextView cityHeaderCityName;
    @BindView(R.id.cityHeaderCountryName)
    CustomTextView cityHeaderCountryName;
    @BindView(R.id.imageWeather)
    ImageView imageWeather;
    @BindView(R.id.cityHeaderTemprature)
    CustomTextView cityHeaderTemprature;
    @BindView(R.id.cityHeaderDate)
    CustomTextView cityHeaderDate;
    @BindView(R.id.cityRecyclerView)
    RecyclerView cityRecyclerView;
    @BindView(R.id.framelayoutPlaceholder)
    RelativeLayout framelayoutPlaceholder;
    @BindView(R.id.progressInCityInfo)
    ProgressBar progressInCityInfo;
    @BindView(R.id.tempSymbol)
    ImageView tempSymbol;

    public static String encodeString(String base) {
        try {
            return URLEncoder.encode(base, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "";
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.city_layout, container, false);
        ButterKnife.bind(this, view);
        //ButterKnife.bind(this, view);
        //getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        cityList = new ArrayList<>();

/*      cityList.add(new City("PLACES", ""));
        cityList.add(new City("EVENTS AND EXHIBITIONS", ""));
        cityList.add(new City("CITY LIFE", ""));
        cityList.add(new City("TOURISM", ""));*/

        parentCityInfoAdapter = new ParentCityInfoAdapter(getContext(), cityList, new ItemEventListener<String>() {
            @Override
            public void onItemEventFired(String s, String t2, String actualPos) {
                switch (s) {
                    case "webview":
                        WebViewCityInfoFragment webViewCityInfoFragment = new WebViewCityInfoFragment();
                        Bundle bundleWebView = new Bundle();
                        bundleWebView.putString("webview", t2);
                        webViewCityInfoFragment.setArguments(bundleWebView);
                        ((MainActivity) getActivity()).setFragment(webViewCityInfoFragment, false, "");
                        break;
                }
            }
        });

        if (!((MainActivity) getActivity()).getCityName().equals(""))
            cityHeaderCityName.setText(((MainActivity) getActivity()).getCityName());

        gridLayoutManager = new GridLayoutManager(getActivity(), 2);

        cityRecyclerView.setLayoutManager(gridLayoutManager);
        cityRecyclerView.setAdapter(parentCityInfoAdapter);


      /*  callCityInfo();

        yahooWeatherApi();*/
        //textViewLogin.setText("CITY FRAGMENT:::::");


        return view;
    }

    public void yahooWeatherApi() {
        if (!((MainActivity) getActivity()).getCityName().equals("")) {
            cityHeaderCityName.setText(((MainActivity) getActivity()).getCityName());
        }
        progressInCityInfo.setVisibility(View.VISIBLE);
        tempSymbol.setVisibility(View.INVISIBLE);
        imageWeather.setVisibility(View.INVISIBLE);
        cityHeaderTemprature.setVisibility(View.INVISIBLE);

        String query = "select * from weather.forecast where woeid in (select woeid from geo.places(1) where text='" + ((MainActivity) getActivity()).getCityName() + "') and u ='C'";
        final long cacheBuster = System.currentTimeMillis() / 1200 / 1000;
        String URL = "http://query.yahooapis.com/v1/public/yql?q=" + encodeString(query) + "&format=json&_nocache=" + cacheBuster;

        new GetRequestHelper<YahooWeather>().pingToRequest(URL
                , new ArrayList<KeyValuePair>()
                , ((MainActivity) getActivity()).getHeader()
                , new YahooWeather()
                , new TaskCompleteListener<YahooWeather>() {
                    @Override
                    public void onSuccess(YahooWeather yahooWeather) {
                        if (yahooWeather.getQuery().getResults() != null) {
                            progressInCityInfo.setVisibility(View.INVISIBLE);
                            tempSymbol.setVisibility(View.VISIBLE);
                            imageWeather.setVisibility(View.VISIBLE);
                            cityHeaderTemprature.setVisibility(View.VISIBLE);
                            Condition condition = yahooWeather.getQuery().getResults().getChannel().getItem().getCondition();
                            cityHeaderTemprature.setText(condition.getTemp());
                            if (getContext() != null)
                                Glide.with(getContext()).load("http://l.yimg.com/a/i/us/we/52/" + condition.getCode() + ".gif").into(imageWeather);
                            cityHeaderDate.setText(condition.getDate());
                            /*DateFormat utcFormat = new SimpleDateFormat(Constants.YAHOO_DATE);
                            utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                            Date date = null;
                            try {
                                date = utcFormat.parse(condition.getDate());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            DateFormat pstFormat = new SimpleDateFormat(Constants.DATE_DISPLAY);
                            pstFormat.setTimeZone(TimeZone.getDefault());
                            cityHeaderDate.setText(pstFormat.format(date));*/
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, YahooWeather cityInfoWrapper) {

                    }
                });
    }

    /* http://192.168.1.185/Jaimin/quick_info/api/v1/service/city_info/city_id/1*/
    public void callCityInfo() {
        List<KeyValuePair> keyValuePairs = new ArrayList<>();
        keyValuePairs.add(new KeyValuePair("city_id", ((MainActivity) getActivity()).getTopCitySelectionId()));

        new GetRequestHelper<CityInfoWrapper>().pingToRequest(Constants.CITY_INFO
                , keyValuePairs
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new CityInfoWrapper()
                , new TaskCompleteListener<CityInfoWrapper>() {
                    @Override
                    public void onSuccess(CityInfoWrapper mObject) {
                        if (mObject.getStatus() == 1) {
                            setCityInfoAdapter(mObject.getData());
                        } else {
                            cityList.clear();
                            parentCityInfoAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, CityInfoWrapper cityInfoWrapper) {

                    }
                });
    }


    public void setCityInfoAdapter(List<CityInfoData> cityInfoDatas) {
        cityList.clear();
        cityList.addAll(cityInfoDatas);
        parentCityInfoAdapter.notifyDataSetChanged();
    }


    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle("default", Enum.setNevigationIcon.MENU_CITY, false);
    }
}
