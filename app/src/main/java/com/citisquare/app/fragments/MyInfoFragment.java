package com.citisquare.app.fragments;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.pojo.response.City;
import com.citisquare.app.pojo.response.Country;
import com.citisquare.app.pojo.response.LoginData;
import com.citisquare.app.pojo.response.LoginWrapper;
import com.citisquare.app.pojo.response.ProfileData;
import com.citisquare.app.pojo.response.ProfileWrapper;
import com.citisquare.app.utils.Debugger;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by hlink56 on 1/6/16.
 */
public class MyInfoFragment extends BaseFragment {


    private static final int REQUEST_CALL_PHONE_PERMISSION = 98;
    private final int REQUEST_CALL_PERMISSION = 88;

    Bundle bundle;
    String stringData = "";
    ProfileWrapper profileWrapper;

    ProfileData profileData;
    ArrayList<City> cityList;
    ArrayList<Country> countryList;
    LoginWrapper loginWrapper;
    LoginData loginData;
    String phoneNumber;
    String cellPhoneNumber;


    String serviceListToDisplay = "";
    @BindView(R.id.myinfoHolderFirstname)
    CustomTextView myinfoHolderFirstname;
    @BindView(R.id.myonfoFirstName)
    CustomTextView myonfoFirstName;
    @BindView(R.id.myinfoHolderlasstname)
    CustomTextView myinfoHolderlasstname;
    @BindView(R.id.myonfoLastName)
    CustomTextView myonfoLastName;
    @BindView(R.id.myinfoHolderUsername)
    CustomTextView myinfoHolderUsername;
    @BindView(R.id.myonfoUserName)
    CustomTextView myonfoUserName;
    @BindView(R.id.myinfoHolderPhone)
    CustomTextView myinfoHolderPhone;
    @BindView(R.id.myonfoPhoneNumber)
    CustomTextView myonfoPhoneNumber;
    @BindView(R.id.myonfoEmail)
    CustomTextView myonfoEmail;
    @BindView(R.id.myinfoBuildingNumber)
    CustomTextView myinfoBuildingNumber;
    @BindView(R.id.myinfoStreet)
    CustomTextView myinfoStreet;
    @BindView(R.id.myinfoCity)
    CustomTextView myinfoCity;
    @BindView(R.id.myinfoCountry)
    CustomTextView myinfoCountry;
    @BindView(R.id.myinfoZipcode)
    CustomTextView myinfoZipcode;
    @BindView(R.id.myonfoBio)
    CustomTextView myonfoBio;
    @BindView(R.id.myinfoCompnayName)
    CustomTextView myinfoCompanyName;
    @BindView(R.id.myinfoCompanyNameTextView)
    CustomTextView myInfoCompanyNameDisplay;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.myinfo, container, false);
        ButterKnife.bind(this, view);
        cityList = ((MainActivity) getActivity()).cityArrayList();
        countryList = ((MainActivity) getActivity()).countryDatas();


        bundle = getArguments();
        if (bundle != null) {
            stringData = bundle.getString("profile", "");
            if (stringData.equals("own")) {
                loginWrapper = ((MainActivity) getActivity()).getData(getContext());
                if (loginWrapper != null) {
                    loginData = loginWrapper.getData();

                    if(loginData.getRole().equals("S")){
                        myinfoCompanyName.setVisibility(View.VISIBLE);
                        myInfoCompanyNameDisplay.setVisibility(View.VISIBLE);
                    }else {
                        myinfoCompanyName.setVisibility(View.GONE);
                        myInfoCompanyNameDisplay.setVisibility(View.GONE);
                    }

                    myonfoFirstName.setText(loginData.getFirstName());
                    myonfoLastName.setText(loginData.getLastName());
                    cellPhoneNumber = loginData.getCountryCode() + " " + loginData.getPhoneNo();
                    myonfoPhoneNumber.setText(underLine(cellPhoneNumber));
                    myonfoUserName.setText(loginData.getUsername());
                    myonfoEmail.setText(loginData.getEmail());
                    myinfoZipcode.setText(loginData.getZipcode());
                    myinfoBuildingNumber.setText(loginData.getBuildingNo());
                    myinfoStreet.setText(loginData.getStreet());

                    myonfoBio.setText(loginData.getBio());
                    for (int i = 0; i < cityList.size(); i++) {
                        if (cityList.get(i).getId().equals(loginData.getCity())) {
                            if (Locale.getDefault().getDisplayLanguage().equals("العربية")) {
                                myinfoCity.setText(cityList.get(i).getArName());
                            } else {
                                myinfoCity.setText(cityList.get(i).getName());
                            }
                            break;
                        }
                    }

                    for (int i = 0; i < countryList.size(); i++) {
                        if (countryList.get(i).getId().equals(loginData.getCountry())) {
                            myinfoCountry.setText(countryList.get(i).getCountry());
                            break;
                        }
                    }
                }
            } else if (stringData.equals("other")) {
                profileWrapper = (ProfileWrapper) bundle.getSerializable("otherData");
                if (profileWrapper != null && getActivity() != null) {
                    profileData = profileWrapper.getData();

                    if(profileData.getRole().equals("S")){
                        //myInfoCompanyNameDisplay.setText(profileData.getco);
                        myinfoCompanyName.setVisibility(View.VISIBLE);
                        myInfoCompanyNameDisplay.setVisibility(View.VISIBLE);
                    }else {
                        myinfoCompanyName.setVisibility(View.GONE);
                        myInfoCompanyNameDisplay.setVisibility(View.GONE);
                    }
                    myonfoFirstName.setText(profileData.getFirstName());
                    myonfoLastName.setText(profileData.getLastName());
                    myonfoUserName.setText(profileData.getUsername());
                    myonfoEmail.setText(profileData.getEmail());
                    myinfoBuildingNumber.setText(profileData.getBuildingNo());
                    myinfoStreet.setText(profileData.getStreet());
                    myinfoZipcode.setText(profileData.getZipcode());

                    cellPhoneNumber = profileData.getCellCountryCode() + " " + profileData.getCellPhoneno();

                    myonfoBio.setText(profileData.getBio());
                    for (int i = 0; i < cityList.size(); i++) {
                        if (cityList.get(i).getId().equals(profileData.getCity())) {
                            if (Locale.getDefault().getDisplayLanguage().equals("العربية")) {
                                myinfoCity.setText(cityList.get(i).getArName());
                            } else {
                                myinfoCity.setText(cityList.get(i).getName());
                            }
                            break;
                        }
                    }

                    for (int i = 0; i < countryList.size(); i++) {
                        if (countryList.get(i).getId().equals(profileData.getCountry())) {
                            myinfoCountry.setText(countryList.get(i).getCountry());
                            break;
                        }
                    }
                }
            }
        }
        return view;
    }


    @OnClick(R.id.myonfoPhoneNumber)
    public void phoneNumberClicked() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL_PERMISSION);
            //call(number);
        } else {
            try {
                call(phoneNumber);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void setHeaderTitle() {

    }

    public SpannableString underLine(String data) {
        SpannableString content = new SpannableString(data);
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        return content;
    }

    public void call(final String phone) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle("" + getString(R.string.app_name));
        alertDialogBuilder
                .setMessage("" + Html.fromHtml(phone))
                .setPositiveButton("CALL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Intent.ACTION_CALL);
                        intent.setData(Uri.parse("tel:" + phone));
                        startActivity(intent);
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Debugger.e("HOME:::::::::ONPERMISSIONRESULT CALLED");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CALL_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    call(phoneNumber);
                    return;
                } else {
                    //  getActivity().finish();
                }
                break;
            case REQUEST_CALL_PHONE_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    call(cellPhoneNumber);
                    return;
                } else {
                    //  getActivity().finish();
                }
                break;
        }
    }
}
