package com.citisquare.app.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.citisquare.app.R;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.Navigation;
import com.citisquare.app.listener.OnBackPressListener;
import com.citisquare.app.utils.BackPressImpl;
import com.citisquare.app.utils.Debugger;

/**
 * Created by hlink16 on 25/5/16.
 */
public abstract class BaseFragment extends Fragment implements OnBackPressListener {
    Navigation navigation;
    private ProgressDialog progressDialog;


    /*public static <T> void showBottomSheetDialog(Activity activity, List<T> list, String title, final ItemEventListener<T> itemEventListener) {

        View view = activity.getLayoutInflater().inflate(R.layout.bottomsheet, null);
        view.findViewById(R.id.fakeShadow).setVisibility(View.GONE);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        TextView textView = (TextView) view.findViewById(R.id.dilogHeading);
        textView.setText(title);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity.getApplicationContext()));


        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(activity);
        ArrayList<String> strings = new ArrayList<>();

        BottomSheetAdapter<T> bottomSheetAdapter = new BottomSheetAdapter<>(activity, mBottomSheetDialog, list, strings, new ItemEventListener<T>() {
            @Override
            public void onItemEventFired(T t, @Nullable T t2) {
                itemEventListener.onItemEventFired(t, t2);
            }
        });

        recyclerView.setAdapter(bottomSheetAdapter);
        mBottomSheetDialog.setContentView(view);
        BottomSheetBehavior mDialogBehavior = BottomSheetBehavior.from((View) view.getParent());

        mBottomSheetDialog.show();
    }*/


    /*public static <T> void showBottomSheetMultipleSelection(Activity activity, List<T> list, String title, final ItemEventListener<T> itemEventListener) {

        View view = activity.getLayoutInflater().inflate(R.layout.bottomsheet_multiple, null);
        view.findViewById(R.id.fakeShadowMultiple).setVisibility(View.GONE);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerViewMultiple);
        TextView textView = (TextView) view.findViewById(R.id.dilogHeadingMultiple);
        textView.setText(title);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity.getApplicationContext()));


        final BottomSheetDialog mBottomSheetDialogMultiple = new BottomSheetDialog(activity);
        ArrayList<String> strings = new ArrayList<>();
        Button button = (Button) view.findViewById(R.id.doneMultiple);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialogMultiple.dismiss();
            }
        });

        BottomSheetMultipleAdapter<T> bottomSheetAdapter = new BottomSheetMultipleAdapter<>(activity, mBottomSheetDialogMultiple,
                list, strings, new ItemEventListener<T>() {
            @Override
            public void onItemEventFired(T t, @Nullable T t2) {
                itemEventListener.onItemEventFired(t, t2);
            }
        });

        recyclerView.setAdapter(bottomSheetAdapter);
        mBottomSheetDialogMultiple.setContentView(view);
        BottomSheetBehavior mDialogBehavior = BottomSheetBehavior.from((View) view.getParent());

        mBottomSheetDialogMultiple.show();
    }*/
/*  @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        navigation.onFragmentChange(getTag());
    }*/

    public abstract void setHeaderTitle();

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        navigation = (Navigation) getActivity();
        progressDialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage("Loading...");

        setHeaderTitle();
    }

    public void childFragmentReplacement(BaseFragment fragment, String tag) {
        Debugger.e(":::::Fragment" + fragment.getClass().getName() + ":::::");
        if (fragment instanceof ProfileFragment) {
            BaseFragment baseFragmentTab = (BaseFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.placeholder);
            if (baseFragmentTab != null) {
                if (baseFragmentTab instanceof TabFragment) {
                    ((TabFragment) baseFragmentTab).setSearchVisibility(Enum.setSearch.NOTHING);
                }
            }
        }


        getChildFragmentManager().beginTransaction()
                .add(R.id.framelayoutPlaceholder, fragment, tag)
                .addToBackStack(fragment.getClass().getName())
                .commitAllowingStateLoss();

        //.addToBackStack(fragment.getClass().getName()).
    }

    /*Loader start stop*/
    public void startLoader() {
//        getProgressDialog.show();
        if (progressDialog != null) {
            progressDialog.show();
        }
    }

    public void stopLoader() {
        //   getProgressDialog.dismiss();
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public boolean onBackPressed() {
        //setHeaderTitle();
        return new BackPressImpl(this).onBackPressed();
        //   return true;
    }

    public void clearBackStack(Fragment fragment) {
        /* Debugger.e(":::::Fragment" + fragment.getClass().getName() + ":::::");
        getChildFragmentManager().beginTransaction()
                .add(R.id.framelayoutPlaceholder, fragment)
                .addToBackStack("child")
                .commitAllowingStateLoss();*/


        getChildFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        getActivity().getSupportFragmentManager().beginTransaction()
                .add(R.id.framelayoutPlaceholder, fragment)
                .addToBackStack(fragment.getClass().getName())
                .commitAllowingStateLoss();
    }


}