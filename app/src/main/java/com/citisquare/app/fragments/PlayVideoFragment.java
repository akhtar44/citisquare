package com.citisquare.app.fragments;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.VideoView;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.utils.Debugger;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 1/9/16.
 */
public class PlayVideoFragment extends BaseFragment {

    Bundle playBundle;
    String playUrl;
    @BindView(R.id.play_video_view)
    VideoView playVideoView;
    Uri mVideoURI;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_video_play, container, false);
        ButterKnife.bind(this, view);
        playBundle = getArguments();


        if (playBundle != null) {
            if (playBundle.getString("video") != null) {
                playUrl = playBundle.getString("video");
                Debugger.e("PLAY URL :::" + playUrl);
                Uri myUri = Uri.parse(playUrl);
                Debugger.e("PLAY URL :::" + myUri.toString());
                playVideo(myUri);
            }
        }
        return view;
    }

    public void playVideo(Uri uri) {
        if (getActivity() != null)
            ((MainActivity) getActivity()).startLoader();


        try {

            final MediaController mediacontroller = new MediaController(getActivity());
            mediacontroller.setAnchorView(playVideoView);
            // Get the URL from String VideoURL
            playVideoView.setMediaController(mediacontroller);
            playVideoView.setVideoURI(uri);
            playVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                // Close the progress bar and play the video
                public void onPrepared(MediaPlayer mp) {
                    ((MainActivity) getActivity()).stopLoader();
                    playVideoView.requestFocus();
                    playVideoView.start();
                }
            });

            playVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    if (getActivity() != null) {
                        ((MainActivity)getActivity()).onBackPressed();
                    }
                }
            });

        } catch (Exception e) {
            if (getActivity() != null)
                ((MainActivity) getActivity()).stopLoader();
            Debugger.e("Error" + e.getMessage());
            if (getActivity() != null)
                ((MainActivity) getActivity()).onBackPressed();

            e.printStackTrace();
        }
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).actionBarHide();
    }
}
