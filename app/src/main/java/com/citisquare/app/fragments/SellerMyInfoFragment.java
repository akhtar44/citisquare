package com.citisquare.app.fragments;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.pojo.response.CategoryData;
import com.citisquare.app.pojo.response.City;
import com.citisquare.app.pojo.response.Country;
import com.citisquare.app.pojo.response.LoginData;
import com.citisquare.app.pojo.response.LoginWrapper;
import com.citisquare.app.pojo.response.ProfileData;
import com.citisquare.app.pojo.response.ProfileWrapper;
import com.citisquare.app.pojo.response.SubCategory;
import com.citisquare.app.utils.Debugger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by hlink56 on 14/3/17.
 */

public class SellerMyInfoFragment extends BaseFragment {

    private static final int REQUEST_CALL_PHONE_PERMISSION = 98;
    private final int REQUEST_CALL_PERMISSION = 88;

    @BindView(R.id.sellerMyInfoEmail)
    CustomTextView sellerMyInfoEmail;
    @BindView(R.id.myinfoHolderPhone)
    CustomTextView myinfoHolderPhone;
    @BindView(R.id.sellerMyInfoPhoneNumber)
    CustomTextView sellerMyInfoPhoneNumber;
    @BindView(R.id.myinfoHolderCellPhone)
    CustomTextView myinfoHolderCellPhone;
    @BindView(R.id.myinfoCellPhone)
    CustomTextView myinfoCellPhone;
    @BindView(R.id.sellerInfoHolderWebsite)
    CustomTextView sellerInfoHolderWebsite;
    @BindView(R.id.sellerInfowebsite)
    CustomTextView sellerInfowebsite;
    @BindView(R.id.sellerInfoStreet)
    CustomTextView sellerInfoStreet;
    @BindView(R.id.sellerInfoCity)
    CustomTextView sellerInfoCity;
    @BindView(R.id.sellerInfoZipcode)
    CustomTextView sellerInfoZipcode;
    @BindView(R.id.sellerInfoBio)
    CustomTextView sellerInfoBio;
    @BindView(R.id.sellerInfoServiceTexitview)
    CustomTextView sellerInfoServiceTexitview;
    @BindView(R.id.serviceList)
    CustomTextView serviceList;
    @BindView(R.id.businessHoursellerInfoTextView)
    CustomTextView businessHoursellerInfoTextView;
    @BindView(R.id.businessHoutsellerInfoDetails)
    CustomTextView businessHoutsellerInfoDetails;
    @BindView(R.id.sellerMyInfoCompanyName)
    CustomTextView sellerMyInfoCompanyName;
    String phoneNumber;
    String cellPhoneNumber;
    ArrayList<City> cityList;
    ArrayList<Country> countryList;
    private String serviceListToDisplay = "";
    private Bundle bundle;
    private String stringData;
    private LoginWrapper loginWrapper;
    private ProfileWrapper profileWrapper;

    @Override
    public void setHeaderTitle() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.seller_myinfo, container, false);
        ButterKnife.bind(this, view);
        cityList = ((MainActivity) getActivity()).cityArrayList();
        countryList = ((MainActivity) getActivity()).countryDatas();
        bundle = getArguments();
        if (bundle != null) {
            stringData = bundle.getString("profile", "");
            if (stringData.equals("own")) {
                loginWrapper = ((MainActivity) getActivity()).getData(getContext());
                LoginData loginData = loginWrapper.getData();
                sellerMyInfoEmail.setText(loginData.getEmail());
                sellerMyInfoCompanyName.setText(loginData.getCompanyName() + "\n" + loginData.getCompanyNameAr());
                cellPhoneNumber = loginData.getCellCountryCode() + " " + loginData.getCellPhoneno();
                phoneNumber = loginData.getCountryCode() + " " + loginData.getPhoneNo();
                sellerMyInfoPhoneNumber.setText(phoneNumber);
                myinfoCellPhone.setText(underLine(cellPhoneNumber));
                sellerInfoStreet.setText(loginData.getStreet());
                sellerInfowebsite.setText(loginData.getWebsite());
                for (int i = 0; i < cityList.size(); i++) {
                    if (cityList.get(i).getId().equals(loginData.getCity())) {
                        if (Locale.getDefault().getDisplayLanguage().equals("العربية")) {
                            sellerInfoCity.setText(cityList.get(i).getArName());
                        } else {
                            sellerInfoCity.setText(cityList.get(i).getName());
                        }
                        break;
                    }
                }


                sellerInfoZipcode.setText(loginData.getZipcode());
                sellerInfoBio.setText(loginData.getBio());
                getCateGoryList(Arrays.asList(loginData.getCategoryId().split(",")), Arrays.asList(loginData.getSubCatId().split(",")));

            } else if (stringData.equals("other")) {
                profileWrapper = (ProfileWrapper) bundle.getSerializable("otherData");
                ProfileData profileData = profileWrapper.getData();
                sellerMyInfoEmail.setText(profileData.getEmail());
                sellerMyInfoCompanyName.setText(profileData.getCompanyName() + "\n" + profileData.getCompanyNameAr());
                cellPhoneNumber = profileData.getCellCountryCode() + " " + profileData.getCellPhoneno();
                phoneNumber = profileData.getCountryCode() + " " + profileData.getPhoneNo();
                sellerMyInfoPhoneNumber.setText(phoneNumber);
                myinfoCellPhone.setText(underLine(cellPhoneNumber));
                sellerInfoStreet.setText(profileData.getStreet());
                sellerInfowebsite.setText(profileData.getWebsite());

                for (int i = 0; i < cityList.size(); i++) {
                    if (cityList.get(i).getId().equals(profileData.getCity())) {

                        if (Locale.getDefault().getDisplayLanguage().equals("العربية")) {
                            sellerInfoCity.setText(cityList.get(i).getArName());
                        } else {
                            sellerInfoCity.setText(cityList.get(i).getName());
                        }

                        break;
                    }
                }

                sellerInfoZipcode.setText(profileData.getZipcode());
                sellerInfoBio.setText(profileData.getBio());
                getCateGoryList(Arrays.asList(profileData.getCategoryId().split(",")), Arrays.asList(profileData.getSubCatId().split(",")));
            }
        }


        return view;
    }

    public String removeLastCharacter(String str) {
        if (str != null && str.length() > 0 && str.charAt(str.length() - 1) == ',') {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }

    private void getCateGoryList(List<String> categories, List<String> subCategory) {
        if (categories.size() > 0) {
            if (((MainActivity) getActivity()).getCategoryList() != null) {
                for (CategoryData categoryData : ((MainActivity) getActivity()).getCategoryList()) {
                    for (String category : categories) {
                        if (categoryData.getId().equals(category)) {
                            if (Locale.getDefault().getDisplayLanguage().equals("العربية")) {
                                serviceListToDisplay += categoryData.getArName() + " ,";
                            } else {
                                serviceListToDisplay += categoryData.getName() + " ,";
                            }
                            if (subCategory.size() > 0) {
                                getSubCategoryList(categoryData.getId(), subCategory);
                            }
                        }
                    }
                }
            }
        }
        serviceList.setText(removeLastCharacter(serviceListToDisplay));
    }

    private void getSubCategoryList(String id, List<String> subCategory) {
        if (getActivity() != null) {
            for (SubCategory category : ((MainActivity) getActivity()).getSubCategoryList(id)) {
                for (String s : subCategory) {
                    if (category.getId().equals(s)) {
                        if (Locale.getDefault().getDisplayLanguage().equals("العربية"))
                            serviceListToDisplay += category.getArName() + " ,";
                        else
                            serviceListToDisplay += category.getName() + " ,";
                    }
                }
            }
        }
    }

    public SpannableString underLine(String data) {
        SpannableString content = new SpannableString(data);
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        return content;
    }

    public void call(final String phone) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle("" + getString(R.string.app_name));
        alertDialogBuilder
                .setMessage("" + Html.fromHtml(phone))
                .setPositiveButton("CALL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Intent.ACTION_CALL);
                        intent.setData(Uri.parse("tel:" + phone));
                        startActivity(intent);
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @OnClick(R.id.myinfoCellPhone)
    public void cellPhoneNumberCalled() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL_PHONE_PERMISSION);
            //call(number);
        } else {
            try {
                call(cellPhoneNumber);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @OnClick(R.id.sellerMyInfoPhoneNumber)
    public void phoneNumberClicked() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL_PERMISSION);
            //call(number);
        } else {
            try {
                call(phoneNumber);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Debugger.e("HOME:::::::::ONPERMISSIONRESULT CALLED");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CALL_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    call(phoneNumber);
                    return;
                }
                break;
            case REQUEST_CALL_PHONE_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    call(cellPhoneNumber);
                    return;
                }
                break;
        }
    }
}
