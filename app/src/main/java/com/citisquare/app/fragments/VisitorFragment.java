package com.citisquare.app.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.PlaceAutocompleteAdapter;
import com.citisquare.app.controls.CustomAutoComplete;
import com.citisquare.app.controls.CustomEditText;
import com.citisquare.app.controls.CustomRadioButton;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.controls.GPSTracker;
import com.citisquare.app.dialog.CountryDialog;
import com.citisquare.app.dialog.ImageChooserDialog;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ImageChooserListener;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.interfaces.Navigation;
import com.citisquare.app.pojo.addresslocation.Address;
import com.citisquare.app.pojo.response.City;
import com.citisquare.app.pojo.response.Country;
import com.citisquare.app.pojo.response.LoginWrapper;
import com.citisquare.app.utils.Debugger;
import com.citisquare.app.utils.InitializeMenu;
import com.citisquare.app.utils.RequestParameter;
import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import serviceCalling.builder.KeyValuePair;
import serviceCalling.builder.ServiceParameter;
import serviceCalling.helper.GetRequestHelper;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by hlink56 on 1/6/16.
 */
public class VisitorFragment extends BaseFragment {
    GPSTracker gps;
    private static final String TODO = null ;
    @BindView(R.id.imageViewProfileImage)
    ImageView imageViewProfileImage;
    @BindView(R.id.editTextLastName)
    EditText editTextLastName;
    @BindView(R.id.editTextUsername)
    EditText editTextUsername;
    @BindView(R.id.editTextProfileEmail)
    EditText editTextProfileEmail;
    @BindView(R.id.profilePassword)
    EditText profilePassword;
    @BindView(R.id.profileConfirmPassword)
    EditText profileConfirmPassword;
    @BindView(R.id.editTextProfilebuildingNumber)
    EditText editTextProfilebuildingNumber;
    @BindView(R.id.profileEditTextStreet)
    AutoCompleteTextView profileEditTextStreet;
    @BindView(R.id.editTextProfileCity)
    TextView editTextProfileCity;
    @BindView(R.id.editTextProfileCountryCode)
    TextView editTextProfileCountryCode;
    @BindView(R.id.editTextProfilePhoneNumber)
    EditText editTextProfilePhoneNumber;
    @BindView(R.id.sign_profile_buttonSubmit)
    Button buttonSubmit;
    File image;
    @BindView(R.id.editTextProfileCountry)
    TextView editTextProfileCountry;
    Country country;
    City city;
    CityDropDownSeller cityDropDown;
    Double lat, lng;
    @BindView(R.id.visitorTitle)
    CustomTextView visitorTitle;
    @BindView(R.id.visitorFirstname)
    CustomEditText visitorFirstname;
    @BindView(R.id.visitorZipcode)
    CustomAutoComplete visitorZipcode;

    CountryCodeDropDown countryCodeDropDown;
    ArrayList<Country> countries;
    AdvertisementDropDown advertisementDropDown;
    ArrayList<String> titleList;
    @BindView(R.id.visitor_checkButton)
    ImageView visitorCheckButton;
    @BindView(R.id.visitor_textView_termsAndCondition)
    CustomTextView visitorTextViewTermsAndCondition;
    @BindView(R.id.englishRadioButtonVisitorSignup)
    CustomRadioButton englishRadioButtonVisitorSignup;
    @BindView(R.id.arabicRadioButtonVisitorSignup)
    CustomRadioButton arabicRadioButtonVisitorSignup;
    @BindView(R.id.preferredLanguageRadioVisitorSignup)
    RadioGroup preferredLanguageRadioVisitorSignup;
    private Pattern pattern;
    private Pattern passwordPattern;
    private Matcher matcher;
    private Matcher password;
    private PlaceAutocompleteAdapter placeAdapter;
    private String languageString = String.valueOf(Constants.forLanguage.ARABIC);
    private Integer title = -1;
    MainActivity mainActivity = new MainActivity();
    private static int SELECT_FILE_CAMERA = 1;
    private static int SELECT_FILE_GALLERY = 2;
    public static int SELECT_PLACE_PICKER = 3;
    Geocoder geocoder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.signup_profile, container, false);
        //getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //ShowLocationMap();
        geocoder = new Geocoder(getActivity(), Locale.getDefault());
        ButterKnife.bind(this, view);
        titleList = new InitializeMenu().toGetAdvertiseMent(getContext());
        countries = ((MainActivity) getActivity()).countryDatas();
        if (countries != null) {
            for (Country countryLoop : countries) {
                if (countryLoop.getDialCode().equals("+966")) {
                    country = countryLoop;
                    editTextProfileCountry.setText(country.getCountry());
                    editTextProfileCountryCode.setText(country.getDialCode());
                }
            }
        }
        pattern = Pattern.compile(Constants.englishOnly);
        passwordPattern = Pattern.compile(Constants.passwordRegex);
        placeAdapter = new PlaceAutocompleteAdapter(getContext(), R.layout.raw_autocomplete_text);
        profileEditTextStreet.setAdapter(placeAdapter);
        profileEditTextStreet.setThreshold(1);


        preferredLanguageRadioVisitorSignup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int radio = preferredLanguageRadioVisitorSignup.getCheckedRadioButtonId();
                switch (radio) {
                    case R.id.englishRadioButtonVisitorSignup:
                        languageString = String.valueOf(Constants.forLanguage.ENGLISH);
                        break;
                    case R.id.arabicRadioButtonVisitorSignup:
                        languageString = String.valueOf(Constants.forLanguage.ARABIC);
                        break;
                }
            }
        });
        if (Locale.getDefault().getDisplayLanguage().equals("العربية")) {
            profilePassword.setGravity(Gravity.RIGHT);
            profileConfirmPassword.setGravity(Gravity.RIGHT);
        } else {
            profileConfirmPassword.setGravity(Gravity.LEFT);
            profileConfirmPassword.setGravity(Gravity.LEFT);
        }
        return view;
    }

    public void AlertMessageNoGPS(final Activity activity) {
        try {
            final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setMessage(activity.getString(R.string.gpsDisableEnableText))
                    .setCancelable(false)
                    .setPositiveButton(activity.getString(R.string.statusYesBtn),
                            new DialogInterface.OnClickListener() {
                                public void onClick(
                                        @SuppressWarnings("unused") final DialogInterface dialog,
                                        @SuppressWarnings("unused") final int id) {
                                    activity.startActivity(new Intent(
                                            Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                }
                            })
                    .setNegativeButton(activity.getString(R.string.statusNoBtn), new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog,
                                            @SuppressWarnings("unused") final int id) {
                            dialog.cancel();
                        }
                    });
            final AlertDialog alert = builder.create();
            alert.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public boolean IsGPSOn(Activity activity) {
        boolean isON = false;
        try {
            LocationManager locationManager = (LocationManager) activity.getSystemService(LOCATION_SERVICE);
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                isON = true;
            } else {
                isON = false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return isON;
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).actionBarShow();
        ((MainActivity) getActivity()).setTitle(getString(R.string.title_signup), Enum.setNevigationIcon.BACK, true);
    }

    @OnClick(R.id.visitor_checkButton)
    public void checkButtonClicked() {
        if (visitorCheckButton.getDrawable().getConstantState().equals(getResources().getDrawable(R.drawable.uncheck).getConstantState()))
            visitorCheckButton.setImageDrawable(getResources().getDrawable(R.drawable.check));
        else
            visitorCheckButton.setImageDrawable(getResources().getDrawable(R.drawable.uncheck));
    }


    @OnClick(R.id.visitor_textView_termsAndCondition)
    public void termsAndConditionClicked() {
        WebViewFragment webViewFragment = new WebViewFragment();
        Bundle bundle = new Bundle();
        bundle.putString("from", "1");
        webViewFragment.setArguments(bundle);
        ((MainActivity) getActivity()).replaceParentFragment(webViewFragment, "");
    }


    public void getLocation() {
        new GetRequestHelper<Address>().pingToRequest("http://maps.googleapis.com/maps/api/geocode/json?address=" +
                        profileEditTextStreet.getText().toString().replace("\"", "") + "&sensor=true"
                , new ArrayList<KeyValuePair>()
                , new ServiceParameter()
                , new Address()
                , new TaskCompleteListener<Address>() {
                    @Override
                    public void onSuccess(Address mObject) {
                        try {
                            Debugger.e("GET LOCATION:::" + mObject);
                            if (mObject.getStatus().equalsIgnoreCase("ok")) {
                                lat = mObject.getResults().get(0).getGeometry().getLocation().getLat();
                                lng = mObject.getResults().get(0).getGeometry().getLocation().getLng();
                                //visitoSignup(lat, lng);
                            } else {
                                ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterValidLocation));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, Address address) {

                    }
                });
    }


    public void visitoSignup() {
        if (getActivity() != null)
            ((MainActivity) getActivity()).startLoader();

        List<KeyValuePair> keyValuePair = new ArrayList<>();
        if (image != null)
            keyValuePair.add(new KeyValuePair("profile_image", image.getAbsolutePath()));


        new PostRequestHelper<LoginWrapper>().pingToRequest(Constants.VISITOR_SIGNUP, "image/*"
                , keyValuePair, new RequestParameter().getHashMapForVisitorSignup(getHashMapForVisitorSignup(lat, lng))
                , ((MainActivity) getActivity()).getHeader()
                , new LoginWrapper(), new TaskCompleteListener<LoginWrapper>() {
                    @Override
                    public void onSuccess(LoginWrapper login) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        if (login.getStatus() == 1) {
                            //((MainActivity) getActivity()).messageAlert(login.getMessage());
                            //((MainActivity) getActivity()).onBackPressed();
                           /* ((MainActivity) getActivity()).setData(getContext(), new Gson().toJson(login));
                            ((MainActivity) getActivity()).setUserSession(getContext(), login.getData().getUserSessionId());
                            ((MainActivity) getActivity()).replaceParentFragment(new TabFragment(), "parent");*/
                            //Toast.makeText(getActivity(), R.string.sign_up_successful, Toast.LENGTH_LONG).show();
                            //((MainActivity) getActivity()).messageAlert(login.getMessage());
                            //((MainActivity) getActivity()).onBackPressed();
                            ((MainActivity) getActivity()).replaceParentFragment(new LoginFragment(), "parent");
                            Toast.makeText(getActivity(), R.string.sign_up_successful, Toast.LENGTH_LONG).show();
                        } else
                            ((MainActivity) getActivity()).messageAlert(login.getMessage());
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, LoginWrapper signupSellerWrapper) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        if (signupSellerWrapper != null) {
                            ((MainActivity) getActivity()).messageAlert(signupSellerWrapper.getMessage());
                        }
                    }
                });
    }


    public HashMap<String, String> getHashMapForVisitorSignup(Double lat, Double lng) {
        /*title,first_name,last_name,username,email,password,building_no,street,zipcode,city,country
        ,country_code,phone_no,device_type,device_token,latitude,longitude	profile_image*/

        HashMap<String, String> hashmapVisitorSignup = new HashMap<>();
        /*if (visitorTitle.getText().toString().equalsIgnoreCase(getString(R.string.title))) {
            hashmapVisitorSignup.put("title", "");
        } else {
            hashmapVisitorSignup.put("title", visitorTitle.getText().toString());
        }*/
        //hashmapVisitorSignup.put("first_name", visitorFirstname.getText().toString());
        //hashmapVisitorSignup.put("last_name", editTextLastName.getText().toString());
         hashmapVisitorSignup.put("username", editTextUsername.getText().toString());
        hashmapVisitorSignup.put("email", editTextProfileEmail.getText().toString());
        hashmapVisitorSignup.put("password", profilePassword.getText().toString());
        //hashmapVisitorSignup.put("building_no", editTextProfilebuildingNumber.getText().toString());
        //hashmapVisitorSignup.put("street", profileEditTextStreet.getText().toString());
        ///hashmapVisitorSignup.put("zipcode", visitorZipcode.getText().toString());
       // hashmapVisitorSignup.put("city", city.getId());
        // hashmapVisitorSignup.put("country", country.getId());
        //hashmapVisitorSignup.put("device_token", ((MainActivity) getActivity()).getDeviceId());
        //hashmapVisitorSignup.put("country_code", country.getDialCode());
        //hashmapVisitorSignup.put("phone_no", editTextProfilePhoneNumber.getText().toString());
        //hashmapVisitorSignup.put("latitude", lat + "");
        //hashmapVisitorSignup.put("longitude", lng + "");

        //hashmapVisitorSignup.put("language", languageString);

        return hashmapVisitorSignup;
    }

    @OnClick(R.id.imageViewProfileImage)
    public void imageProfileImage() {
        ImageChooserDialog imageChooserDialog = new ImageChooserDialog();
        imageChooserDialog.show(getActivity().getSupportFragmentManager(), "imageChooserDialog");
        imageChooserDialog.setCallback(new ImageChooserListener() {
            @Override
            public void getResultFromCamera(String result) {
                image = new File(result);
                ((MainActivity) getActivity()).setCircularImage(image, imageViewProfileImage);
            }

            @Override
            public void getResultFromGallery(String result) {
                if (result != null) {
                    if (!(result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("jpeg") ||
                            result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("jpg") ||
                            result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("png") ||
                            result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("bmp"))) {
                        //isInvalidImage = true;
                        ((MainActivity) getActivity()).messageAlert(getString(R.string.message_thisImageIsNotSupported));

                    } else {
                        image = new File(result);
                        ((MainActivity) getActivity()).setCircularImage(image, imageViewProfileImage);
                        //layoutAddItemImageViewAdd.setBackgroundColor(getResources().getColor(R.color.backColor));
                        //isInvalidImage = false;
                    }
                }
            }

            @Override
            public void nothingSet() {

            }
        });
    }

    @OnClick(R.id.editTextProfileCountry)
    public void countrySelectionClicked() {
        CountryDialog.openCountryCodeDialog(getContext(), new ItemEventListener<String>() {
            @Override
            public void onItemEventFired(String s, String t2, String actualPos) {
                Debugger.e(s + " " + t2);
                country = new Gson().fromJson(t2, Country.class);
                Debugger.e("COUNTRY :::" + country.toString());
                editTextProfileCountry.setText(country.getCountry());
                editTextProfileCountryCode.setText(country.getDialCode());
                // editTextProfileCountryCode.setText(s);
            }
        });
    }

    @OnClick(R.id.sign_profile_buttonSubmit)
    public void signupProfileButtonSubmit() {
        if (isValid()) {
            visitoSignup();
            //((MainActivity) getActivity()).messageAlert("SIGNUP SUCCESSFULLY");
             //getLocation();
            //GetCurrentLocation();
            ((MainActivity) getActivity()).replaceParentFragment(new TabFragment(), "parent");
        }
    }

    private void GetCurrentLocation() {
        gps = new GPSTracker(getActivity());

        // check if GPS enabled
        if(gps.canGetLocation()){

            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();

            // \n is for new line
            Toast.makeText(getActivity(), "Your Location is - \nLat: "
                    + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
            //visitoSignup(latitude,longitude);
        }else{
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in account_settings
            gps.showSettingsAlert();
        }
    }


    public boolean isValid() {
        /*if (visitorFirstname.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterFirstName));
            visitorFirstname.requestFocus();
            return false;
        }

        if (editTextLastName.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterLastName));
            editTextLastName.requestFocus();
            return false;
        }*/

        if (!validEmail(editTextProfileEmail.getText().toString())) {
            editTextProfileEmail.requestFocus();
            return false;
        }

        if (profilePassword.getText().toString().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterPassword));
            profilePassword.requestFocus();
            return false;
        } else {
            matcher = pattern.matcher(profilePassword.getText().toString());
            password = passwordPattern.matcher(profilePassword.getText().toString());
            if (matcher.find()) {
                profilePassword.setText("");
                ((MainActivity) getActivity()).messageAlert(getString(R.string.message_englishValidation));
                profilePassword.requestFocus();
                return false;
            } else if (!password.matches()) {
                profilePassword.setText("");
                ((MainActivity) getActivity()).messageAlert(getString(R.string.messagePasswordMustContains));
                profilePassword.requestFocus();
                return false;
            }
        }

        if (profilePassword.getText().toString().length() < 6) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterMinimumThreeCharacterForPassword));
            profilePassword.requestFocus();
            return false;
        } else if (profileConfirmPassword.getText().toString().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterConfirmPassword));
            profileConfirmPassword.requestFocus();
            return false;
        }


        if (profilePassword.getText().toString().compareTo(profileConfirmPassword.getText().toString()) != 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_passwordmismatch));
            profilePassword.requestFocus();
            return false;
        }

       if (editTextUsername.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterUserName));
            editTextUsername.requestFocus();
            return false;
        }
       /*if (profileEditTextStreet.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterStreet));
            profileEditTextStreet.requestFocus();
            return false;
        }

        if (visitorZipcode.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterZipCode));
            visitorZipcode.requestFocus();
            return false;
        }

        if (city == null) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterCity));
            editTextProfileCity.requestFocus();
            return false;
        }


        if (country == null) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterCountry));
            editTextProfileCountry.requestFocus();
            return false;
        }


        if (editTextProfilePhoneNumber.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterMobileNumber));
            editTextProfilePhoneNumber.requestFocus();
            return false;
        }


        if (editTextProfilePhoneNumber.getText().toString().length() < 9) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterValidMobileNumber));
            editTextProfilePhoneNumber.requestFocus();
            return false;
        }*/

        if (visitorCheckButton.getDrawable().getConstantState().equals(getResources().getDrawable(R.drawable.uncheck).getConstantState())) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseAccpetTermsAndConditions));
            return false;
        }

        return true;
    }

    @OnClick(R.id.editTextProfileCity)
    public void profileCityClicked() {

        ((MainActivity) getActivity()).hideKeyboard();
        int[] location = new int[2];
        editTextProfileCity.getLocationOnScreen(location);
        final ArrayList<City> cityList = ((MainActivity) getActivity()).cityArrayList();
        cityDropDown = new CityDropDownSeller(getContext(), editTextProfileCity.getHeight(), location, cityList, new ItemEventListener<String>() {
            @Override
            public void onItemEventFired(String integer, String t2, String actualPos) {
                city = new Gson().fromJson(t2, City.class);
                if (Locale.getDefault().getDisplayLanguage().equals("العربية"))
                    editTextProfileCity.setText(city.getArName());
                else
                    editTextProfileCity.setText(city.getName());

            }
        });
        cityDropDown.show();
    }

    public boolean validEmail(String email) {
        int length = email.trim().length();
        if (length <= 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterEmail));
            return false;
        }
        Pattern pattern = Pattern.compile(Navigation.EMAIL_REGEX);
        Matcher matcher = pattern.matcher(email);
        if (!(email.charAt(0) + "").matches("\\p{L}") || !matcher.matches()) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterValidEmailId));
            return false;
        }
        return true;
    }

    @OnClick({R.id.visitorTitle})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.visitorTitle:
                ((MainActivity) getActivity()).hideKeyboard();
                int[] location = new int[2];
                visitorTitle.getLocationOnScreen(location);
                advertisementDropDown = new AdvertisementDropDown(getContext(), visitorTitle.getHeight(), location, titleList, new ItemEventListener<Integer>() {
                    @Override
                    public void onItemEventFired(Integer integer, Integer t2, String actualPos) {
                        visitorTitle.setText(titleList.get(integer));
                        title=integer;
//                        branchId = integer + 1;
                    }
                });
                advertisementDropDown.show();

                break;

        }
    }
}