package com.citisquare.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.controls.CustomButton;
import com.citisquare.app.controls.CustomEditText;
import com.citisquare.app.interfaces.Enum;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by hlink56 on 16/7/16.
 */
public class SendMessageIndividualFragment extends BaseFragment {
    @BindView(R.id.sendMessageSelectorEditMessage)
    CustomEditText sendMessageSelectorEditMessage;
    @BindView(R.id.sendMessage)
    CustomButton sendMessage;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_send_message, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @OnClick(R.id.sendMessage)
    public void selectMembersClicked() {
        if (sendMessageSelectorEditMessage.getText().toString().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterMessage));
        } else {
            ((MainActivity) getActivity()).onBackPressed();
        }
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle(getString(R.string.title_sendMessage), Enum.setNevigationIcon.BACK, true);
    }
}
