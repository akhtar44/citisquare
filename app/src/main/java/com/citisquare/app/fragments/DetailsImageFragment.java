package com.citisquare.app.fragments;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.pojo.response.GalleryData;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 8/9/16.
 */
public class DetailsImageFragment extends DialogFragment {

    Bundle bundle;
    @BindView(R.id.layout_details_imageView)
    ImageView layoutDetailsImageView;
    GalleryData galleryData;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_details_image_full, container, false);
        getDialog().requestWindowFeature(STYLE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
        //getDialog().getWindow().setWindowAnimations(R.style.DialogNoAnimation);
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        getDialog().setCanceledOnTouchOutside(true);
        ButterKnife.bind(this, view);

        layoutDetailsImageView.invalidate();
        layoutDetailsImageView.setImageResource(0);

        bundle = getArguments();
        if (bundle != null) {
            String s = bundle.getString("data");
            if (s != null) {
                galleryData = new Gson().fromJson(s, GalleryData.class);
                if (galleryData != null)
                    ((MainActivity) getActivity()).picasso.load(galleryData.getMedia_name_thumb()).into(layoutDetailsImageView);
//                    ((MainActivity) getActivity()).setImage(galleryData.getImage(), layoutDetailsImageView);
            }
        }
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        layoutDetailsImageView.invalidate();
        layoutDetailsImageView.setImageResource(0);
    }

    @Override
    public int show(FragmentTransaction transaction, String tag) {
        return super.show(transaction, tag);
    }

}
