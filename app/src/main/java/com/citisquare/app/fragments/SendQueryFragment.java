package com.citisquare.app.fragments;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.controls.CustomButton;
import com.citisquare.app.controls.CustomEditText;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.interfaces.Navigation;
import com.citisquare.app.pojo.response.Country;
import com.citisquare.app.pojo.response.Like;
import com.citisquare.app.pojo.response.LoginData;
import com.citisquare.app.pojo.response.LoginWrapper;
import com.citisquare.app.utils.BranchDropDown;
import com.citisquare.app.utils.Debugger;
import com.citisquare.app.utils.InitializeMenu;
import com.citisquare.app.utils.RequestParameter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

import static com.citisquare.app.R.id.send_quiry_quiery;

/**
 * Created by hlink56 on 3/6/16.
 */
public class SendQueryFragment extends BaseFragment {

    private final int REQUEST_CALL_PERMISSION = 101;
    @BindString(R.string.dummyPhoneNumber)
    String phoneNo;

    @BindView(R.id.sendQueryTitle)
    CustomTextView sendQueryTitle;
    @BindView(R.id.sendQueryFirstName)
    CustomEditText sendQueryFirstName;
    @BindView(R.id.sendQueryLastName)
    CustomEditText sendQueryLastName;
    @BindView(R.id.sendQueryCountryCode)
    CustomTextView sendQueryCountryCode;
    @BindView(R.id.sendQueryPhoneNumber)
    CustomEditText sendQueryPhoneNumber;
    @BindView(R.id.sendQueryEmail)
    CustomEditText sendQueryEmail;

    @BindView(R.id.sendQueryReasonToConnect)
    CustomTextView sendQueryReasonToConnect;
    @BindView(send_quiry_quiery)
    CustomEditText sendQuiryQuiery;
    @BindView(R.id.sendQuerySubmit)
    CustomButton sendQuerySubmit;
    @BindView(R.id.sendQuiryEmailId)
    CustomTextView sendQuiryEmailId;
    @BindView(R.id.sendQuiryPhone)
    CustomTextView sendQuiryPhone;
    @BindView(R.id.framelayoutPlaceholder)
    FrameLayout framelayoutPlaceholder;
    AdvertisementDropDown advertisementDropDown;
    CountryCodeDropDown countryCodeDropDown;

    ArrayList<String> titleList;
    ArrayList<Country> countries;
    BranchDropDown branchDropDown;
    LoginData loginData;
    LoginWrapper loginWrapper;
    private Pattern pattern;
    private Matcher matcher;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.send_queries, container, false);
        ButterKnife.bind(this, view);
        titleList = new InitializeMenu().toGetAdvertiseMent(getContext());
        sendQuiryQuiery.setMovementMethod(new ScrollingMovementMethod());
        countries = ((MainActivity) getActivity()).countryDatas();
        pattern = Pattern.compile(Constants.englishOnly);

        if (((MainActivity) getActivity()).getUserForWholeApp().equals(Enum.setUser.GUEST)) {

            sendQueryTitle.setClickable(true);
            sendQueryFirstName.setEnabled(true);
            sendQueryLastName.setEnabled(true);
            sendQuiryEmailId.setEnabled(true);
            sendQueryCountryCode.setClickable(true);
            sendQueryCountryCode.setText("+966");
            sendQueryPhoneNumber.setEnabled(true);
            sendQueryEmail.setEnabled(true);
        } else {
            sendQueryTitle.setClickable(false);
            sendQueryFirstName.setEnabled(false);
            sendQueryLastName.setEnabled(false);
            sendQueryCountryCode.setClickable(false);
            sendQueryPhoneNumber.setEnabled(false);
            sendQueryEmail.setEnabled(false);

            loginWrapper = ((MainActivity) getActivity()).getData(getContext());
            loginData = loginWrapper.getData();

            sendQueryTitle.setText(loginData.getTitle());
            sendQueryFirstName.setText(loginData.getFirstName());
            sendQueryLastName.setText(loginData.getLastName());
            sendQueryCountryCode.setText(loginData.getCountryCode());
            sendQueryEmail.setText(loginData.getEmail());

            /*sendQuiryEntername.setText(name);
            sendQuiryEnterEmail.setText(loginWrapper.getData().getData());*/
            if (((MainActivity) getActivity()).getUserForWholeApp().equals(Enum.setUser.VISITOR)) {
                sendQueryCountryCode.setText(loginData.getCountryCode());
                sendQueryPhoneNumber.setText(loginWrapper.getData().getPhoneNo());
            } else {
                sendQueryCountryCode.setText(loginData.getCellCountryCode());
                sendQueryPhoneNumber.setText(loginWrapper.getData().getCellPhoneno());
            }
        }
        sendQuiryQuiery.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == send_quiry_quiery) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        return view;
    }

    /* default mail : Testing Subject messageBody: "This is a testing mail."*/

    @OnClick(R.id.sendQuiryPhone)
    public void sendQuiryPhoneClicked() {
        showCallMessage();
    }

    public void showCallMessage() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                checkForCallPermission();
            else
                call(phoneNo);
        } catch (Exception e) {
            e.printStackTrace();
            Debugger.e(":::::::ERROR WHILE DIALING NUMBER:::::");
        }
    }

    public void checkForCallPermission() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL_PERMISSION);
        } else {
            try {
                call(phoneNo);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void call(final String phone) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle("" + getString(R.string.app_name));
        alertDialogBuilder
                .setMessage("" + Html.fromHtml(phone))
                .setPositiveButton("CALL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Intent.ACTION_CALL);
                        intent.setData(Uri.parse("tel:" + phone));
                        startActivity(intent);
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Debugger.e("HOME:::::::::ONPERMISSIONRESULT CALLED");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CALL_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    call(phoneNo);
                    return;
                } else {
                    //  getActivity().finish();
                }
                break;
        }
    }


    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle(getString(R.string.send_queries), Enum.setNevigationIcon.BACK, false);
    }

    @OnClick({R.id.sendQueryTitle, R.id.sendQueryCountryCode, R.id.sendQuerySubmit, R.id.sendQueryReasonToConnect})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sendQueryReasonToConnect:
                ((MainActivity) getActivity()).hideKeyboard();
                int[] locationReson = new int[2];
                sendQueryReasonToConnect.getLocationOnScreen(locationReson);
                final ArrayList<String> resonList = new InitializeMenu().getResontoConnect(getContext());
                branchDropDown = new BranchDropDown(getContext(), sendQueryReasonToConnect.getHeight(), locationReson, resonList, false, new ItemEventListener<Integer>() {
                    @Override
                    public void onItemEventFired(Integer integer, Integer t2, String actualPos) {
                        sendQueryReasonToConnect.setText(resonList.get(integer));
                    }
                });
                branchDropDown.show();
                break;
            case R.id.sendQueryTitle:
                ((MainActivity) getActivity()).hideKeyboard();
                int[] location = new int[2];
                sendQueryTitle.getLocationOnScreen(location);
                advertisementDropDown = new AdvertisementDropDown(getContext(), sendQueryTitle.getHeight(), location, titleList, new ItemEventListener<Integer>() {
                    @Override
                    public void onItemEventFired(Integer integer, Integer t2, String actualPos) {
                        sendQueryTitle.setText(titleList.get(integer));
//                        branchId = integer + 1;

                    }
                });
                advertisementDropDown.show();
                break;
            case R.id.sendQueryCountryCode:
                ((MainActivity) getActivity()).hideKeyboard();
                int[] locationContry = new int[2];
                sendQueryCountryCode.getLocationOnScreen(locationContry);
                countryCodeDropDown = new CountryCodeDropDown(getContext(), sendQueryCountryCode.getHeight(), locationContry, countries, new ItemEventListener<Country>() {
                    @Override
                    public void onItemEventFired(Country integer, Country t2, String actualPos) {
                        sendQueryCountryCode.setText(integer.getDialCode());
//                        branchId = integer + 1;
                    }
                });
                countryCodeDropDown.show();
                break;
            case R.id.sendQuerySubmit:
                if (isValid()) {
                    sendQueryApiCall();
                }

                break;
        }
    }

    public void sendQueryApiCall() {
        if ((MainActivity) getActivity() != null)
            ((MainActivity) getActivity()).startLoader();

        new PostRequestHelper<Like>().pingToRequest(Constants.SEND_QUERY
                , null
                , null
                , new RequestParameter().getHashMapForSendQuery(getHashMapSendQuery())
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new Like()
                , new TaskCompleteListener<Like>() {
                    @Override
                    public void onSuccess(Like mObject) {
                        if ((MainActivity) getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        if (mObject.getStatus() == 1) {
                            ((MainActivity) getActivity()).messageAlert(mObject.getMessage());
                            clear();
                        } else if (mObject.getStatus() == 0)
                            ((MainActivity) getActivity()).messageAlert(mObject.getMessage());
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, Like like) {
                        if ((MainActivity) getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        if (like != null) {
                            if (like.getStatus() == -1)
                                ((MainActivity) getActivity()).invalidToken();

                            ((MainActivity) getActivity()).messageAlert(like.getMessage());
                        }
                    }
                });
    }

    public void clear() {
        if (((MainActivity) getActivity()).getUserForWholeApp().equals(Enum.setUser.GUEST)) {
            sendQueryTitle.setText(getString(R.string.title));
            sendQueryFirstName.setText("");
            sendQueryLastName.setText("");
            sendQueryCountryCode.setText("+966");
            sendQueryPhoneNumber.setText("");
            sendQueryEmail.setText("");
            sendQueryReasonToConnect.setText(getString(R.string.reasonToConnect));
            sendQuiryQuiery.setText("");
        } else {
            sendQuiryQuiery.setText("");
        }
    }

    /*name,email,phone_no,description	user_id,title,last_name,country_code,reason,subject*/

    private HashMap<String, String> getHashMapSendQuery() {
        HashMap<String, String> hashMapSendQuery = new HashMap<>();
        if (((MainActivity) getActivity()).getUserForWholeApp().equals(Enum.setUser.GUEST)) {
        } else {
            hashMapSendQuery.put("user_id", ((MainActivity) getActivity()).userId());
        }
        hashMapSendQuery.put("title", sendQueryTitle.getText().toString());
        hashMapSendQuery.put("last_name", sendQueryLastName.getText().toString());
        hashMapSendQuery.put("name", sendQueryFirstName.getText().toString().trim());
        hashMapSendQuery.put("email", sendQueryEmail.getText().toString().trim());
        hashMapSendQuery.put("country_code", sendQueryCountryCode.getText().toString());
        hashMapSendQuery.put("reason", sendQueryReasonToConnect.getText().toString());
        hashMapSendQuery.put("phone_no", sendQueryPhoneNumber.getText().toString().trim());
        hashMapSendQuery.put("description", sendQuiryQuiery.getText().toString().trim());
        return hashMapSendQuery;
    }

    public void clearFileds() {
        /*if (((MainActivity) getActivity()).getUserForWholeApp().equals(Enum.setUser.GUEST)) {
            sendQuiryEntername.setText("");
            sendQuiryEmailId.setText("");
            sendQuiryPhone.setText("");
            sendQuiryQuiery.setText("");
        } else
            sendQuiryQuiery.setText("");*/
    }


    public boolean isValid() {
        if (sendQueryTitle.getText().toString().equals(getString(R.string.title))) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterTitle));
            sendQueryTitle.requestFocus();
            return false;
        }

        if (sendQueryFirstName.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterFirstName));
            sendQueryFirstName.requestFocus();
            return false;
        }

        if (sendQueryLastName.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterLastName));
            sendQueryLastName.requestFocus();
            return false;
        }


        if (sendQueryCountryCode.getText().toString().equals(getString(R.string.countryCode))) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseSelectCountryCode));
            sendQueryCountryCode.requestFocus();
            return false;
        }

        if (sendQueryPhoneNumber.getText().toString().equals("")) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterPhoneNumber));
            sendQueryPhoneNumber.requestFocus();
            return false;
        }

        if (!validEmail(sendQueryEmail.getText().toString())) {
            sendQueryEmail.requestFocus();
            return false;
        }


        if (sendQueryReasonToConnect.getText().toString().equals(getString(R.string.reasonToConnect))) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterReasonToConnect));
            sendQueryReasonToConnect.requestFocus();
            return false;
        }


        if (sendQuiryQuiery.getText().toString().equals("")) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterMessage));
            sendQuiryQuiery.requestFocus();
            return false;
        }


        return true;
    }


    public boolean validEmail(String email) {
        int length = email.trim().length();
        if (length <= 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterEmail));
            return false;
        }
        // Patterns.WEB_URL.matcher(potentialUrl).matches()
        Pattern pattern = Pattern.compile(Navigation.EMAIL_REGEX);
        Matcher matcher = pattern.matcher(email);
        if (!(email.charAt(0) + "").matches("\\p{L}") || !matcher.matches()) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterValidEmailId));
            return false;
        }
        return true;
    }

}