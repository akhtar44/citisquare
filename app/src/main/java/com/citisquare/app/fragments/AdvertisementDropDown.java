package com.citisquare.app.fragments;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.citisquare.app.R;
import com.citisquare.app.adapter.AdvertisementAdapter;
import com.citisquare.app.interfaces.ItemEventListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 28/12/16.
 */

public class AdvertisementDropDown {
    PopupWindow popupWindow;
    View view;
    boolean popUpVisible = false;
    int position;
    ItemEventListener<Integer> itemEventListener;
    @BindView(R.id.cityList)
    ListView categoryListview;
    TextView textView;
    Context context;
    ArrayList<String> categoryArrayList;
    AdvertisementAdapter cityDropDownAdapter;
    int height;
    int[] location = new int[2];
    boolean flag;


    public AdvertisementDropDown(Context context, int height, int[] location, ArrayList<String> categoryArrayList
            , ItemEventListener<Integer> integerItemEventListener) {
        view = View.inflate(context, R.layout.advertise_drop, null);
        ButterKnife.bind(this, view);
        itemEventListener = integerItemEventListener;
        this.context = context;
        this.flag = flag;
        categoryListview.setTextFilterEnabled(true);
        this.categoryArrayList = categoryArrayList;
        this.location = location;
        this.height = height;
        popupWindow = new PopupWindow(view, (int) context.getResources().getDimension(R.dimen.dp_115), (int) context.getResources().getDimension(R.dimen.dp_180));
    }


    public void setAdapter() {
        categoryListview.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        cityDropDownAdapter = new AdvertisementAdapter(context, R.layout.raw_advertise_drop, categoryArrayList);
        categoryListview.setAdapter(cityDropDownAdapter);

        popupWindow.setContentView(view);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0));
        popupWindow.setFocusable(true);
        if (popupWindow.isFocusable()) {
            popUpVisible = false;
        }
        categoryListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                itemEventListener.onItemEventFired(position, 0, "");
                dismiss();
            }
        });
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int pos) {
        position = pos;
    }

    public void show() {
        popUpVisible = true;
        setAdapter();
        //popupWindow.showAsDropDown(view);
        popupWindow.showAtLocation(view, Gravity.LEFT | Gravity.TOP, location[0],
                location[1] + height);
    }


    public void dismiss() {
        popUpVisible = false;
        popupWindow.dismiss();
    }

    public boolean isVisible() {
        return popUpVisible;
    }
}
