package com.citisquare.app.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.QuickInfoAdapter;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.pojo.response.Like;
import com.citisquare.app.pojo.response.OfferData;
import com.citisquare.app.pojo.response.OfferWrapper;
import com.citisquare.app.pojo.response.SliderData;
import com.citisquare.app.pojo.response.SliderWrapper;
import com.citisquare.app.utils.Debugger;
import com.citisquare.app.utils.RequestParameter;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import serviceCalling.builder.KeyValuePair;
import serviceCalling.helper.GetRequestHelper;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink16 on 26/5/16.
 */
public class ParentToDaysPostFragment extends BaseFragment {

    QuickInfoAdapter quickInfoAdapter;
    ArrayList<OfferData> quickInfoDatas;
    @BindView(R.id.recyclerViewQuickInfo)
    RecyclerView recyclerViewQuickInfo;
    int pageno = 1;
    List<String> list;
    boolean isPagination;
    String search = "";
    GridLayoutManager gridLayoutManager;
    private List<SliderData> sliderData;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.quickinfo_layout, container, false);
        ButterKnife.bind(this, view);
        recyclerViewQuickInfo.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (isPagination) {
                    int visibleItemCount = recyclerView.getChildCount();
                    int totalItemCount = gridLayoutManager.getItemCount();
                    int firstVisibleItemPosition = gridLayoutManager.findFirstVisibleItemPosition();
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount) {
                        isPagination = false;
                        offerApiCalling(++pageno);
                    }
                }
            }
        });

        Log.e(this.getClass().getName(), ":::: CHILD COUNT IS :::: " + getChildFragmentManager().getBackStackEntryCount());


        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        quickInfoDatas = new ArrayList<>();
        sliderData = new ArrayList<>();
        quickInfoAdapter = new QuickInfoAdapter(getContext(), getChildFragmentManager(), quickInfoDatas, new ItemEventListener<String>() {
            @Override
            public void onItemEventFired(String pos, String userId, final String actualPos) {
                switch (pos) {
                    case "map":
                        ((MainActivity) getActivity()).setProfileUserId(userId);
                        ((MainActivity) getActivity()).setFragment(new OtherMapPathFragment(), false, "");
                        break;
                    case "share":
                        OfferData offerData = new Gson().fromJson(userId, OfferData.class);
                        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                        sharingIntent.putExtra(Intent.EXTRA_TEXT, offerData.getDescription() + "  " + offerData.getUrl());
                        sharingIntent.setType("text/plain");
                        startActivity(Intent.createChooser(sharingIntent, "Share application using"));
                        break;
                    case "Child":
                        DetailsHomeFragment detailsHomeFragment = new DetailsHomeFragment();
                        Bundle bundleDetails = new Bundle();
                        bundleDetails.putString("data", userId);
                        bundleDetails.putString("details", "Offer");
                        detailsHomeFragment.setArguments(bundleDetails);
                        ((MainActivity) getActivity()).setFragment(detailsHomeFragment, false, "");
                        //((MainActivity) getActivity()).setFragment(new DetailsHomeFragment());
                        break;
                    case "comment":
                        if (((MainActivity) getActivity()).getUserForWholeApp().equals(Enum.setUser.GUEST)) {
                            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_Guest));
                        } else {
                            CommentFragment commentFragment = new CommentFragment();

                            commentFragment.setCommentListener(new CommentFragment.CommentListener() {
                                @Override
                                public void commentCount(int count) {
                                    quickInfoDatas.get(Integer.parseInt(actualPos)).setCommentCount(count);
                                    quickInfoAdapter.notifyDataSetChanged();
                                }
                            });
                            Bundle bundleComment = new Bundle();
                            bundleComment.putString("postId", userId);
                            bundleComment.putString("FROM", "O");
                            commentFragment.setArguments(bundleComment);
                            ((MainActivity) getActivity()).setFragment(commentFragment, false, "");
                        }
                        break;
                    case "otherProfile":
                        if (userId.equals(((MainActivity) getActivity()).userId())) {
                            ProfileFragment profileFragment = new ProfileFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("profile", "own");
                            ((MainActivity) getActivity()).setProfileUserId(userId);
                            profileFragment.setArguments(bundle);
                            ((MainActivity) getActivity()).setSelectionPostion(0);
                            ((MainActivity) getActivity()).setFragment(profileFragment, false, "");
                        } else {
                            ProfileFragment profileFragment = new ProfileFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("profile", "other");
                            ((MainActivity) getActivity()).setProfileUserId(userId);
                            profileFragment.setArguments(bundle);
                            ((MainActivity) getActivity()).setSelectionPostion(0);
                            ((MainActivity) getActivity()).setFragment(profileFragment, false, "");
                            //childFragmentReplacement(profileFragment, "child");
                        }
                        break;
                    case "Follow":
                        doFollowApiCall(userId, "follow");
                        break;
                    case "Unfollow":
                        doFollowApiCall(userId, "unfollow");
                        break;
                    case "Like":
                        likeApiCall(userId);
                        break;
                    case "video":
                        PlayVideoFragment playVideoFragment = new PlayVideoFragment();
                        Bundle playBundle = new Bundle();
                        playBundle.putString("video", userId);
                        playVideoFragment.setArguments(playBundle);
                        ((MainActivity) getActivity()).setFragment(playVideoFragment, false, "");
                        //childFragmentReplacement(playVideoFragment, "child");
                        break;
                    case "category":
                        callQuickOfferApi();
                        break;
                }
            }
        }, sliderData,false);


        gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerViewQuickInfo.setLayoutManager(gridLayoutManager);
        recyclerViewQuickInfo.setAdapter(quickInfoAdapter);
        callApiOfSlider();

        //offerApiCalling(pageno);
    }

    private void callApiOfSlider() {
        ArrayList<KeyValuePair> keyValuePairs = new ArrayList<>();
        keyValuePairs.add(new KeyValuePair("city_id", ((MainActivity) getActivity()).getTopCitySelectionId()));

        new GetRequestHelper<SliderWrapper>().pingToRequest(Constants.SLIDER_IMAGE, keyValuePairs, ((MainActivity) getActivity()).getHeader(), new SliderWrapper(), new TaskCompleteListener<SliderWrapper>() {
            @Override
            public void onSuccess(SliderWrapper sliderWrapper) {
                if (sliderWrapper.getStatus() == 1) {
                    setSliderData(sliderWrapper.getData());
                }
            }

            @Override
            public void onFailure(ExceptionType exceptions, SliderWrapper sliderWrapper) {

            }
        });
    }

    private void setSliderData(List<SliderData> data) {
        sliderData.clear();
        sliderData.addAll(data);
        quickInfoAdapter.notifySlider();
    }

    public void likeApiCall(String postId) {
        new PostRequestHelper<Like>().pingToRequest(Constants.LIKE
                , null
                , null
                , new RequestParameter().getHashMapLike(getLikeHashMap(postId))
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new Like()
                , new TaskCompleteListener<Like>() {
                    @Override
                    public void onSuccess(Like mObject) {
                        if (mObject.getStatus() == 1)
                            Debugger.e("LIKE");
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, Like like) {
                        if (like != null) {
                            if (like.getStatus() == -1)
                                ((MainActivity) getActivity()).invalidToken();
                        }
                    }
                });
    }

    public void doFollowApiCall(final String userId, final String status) {
        new PostRequestHelper<Like>().pingToRequest(Constants.DO_FOLLOW
                , null
                , null
                , new RequestParameter().getHashMapForDoFollow(getDoFollowMap(userId, status))
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new Like()
                , new TaskCompleteListener<Like>() {
                    @Override
                    public void onSuccess(Like mObject) {
                        if (mObject.getStatus() == 1)
                            follow(userId, status);
                        Debugger.e("FOLLOW SUCCESSFULLY");
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, Like like) {
                        if (like != null) {
                            if (like.getStatus() == -1)
                                ((MainActivity) getActivity()).invalidToken();
                        }
                    }
                });
    }

    public void follow(String userId, String status) {
        for (int i = 0; i < quickInfoDatas.size(); i++) {
            if (quickInfoDatas.get(i).getUserId().equals(userId)) {
                if (status.equals("follow"))
                    quickInfoDatas.get(i).setIsFollowing(1);
                else
                    quickInfoDatas.get(i).setIsFollowing(0);
            }
        }
        quickInfoAdapter.notifyDataSetChanged();
    }

    public HashMap<String, String> getLikeHashMap(String postId) {
        HashMap<String, String> likeHashMap = new HashMap<>();
        likeHashMap.put("post_id", postId);
        likeHashMap.put("flag", "O");
        likeHashMap.put("status", "1");
        return likeHashMap;
    }

    public HashMap<String, String> getDoFollowMap(String userId, String status) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("follower_user_id", userId);
        if (status.equals("follow"))
            hashMap.put("status", "1");
        else
            hashMap.put("status", "0");

        return hashMap;
    }

    public void callQuickOfferApi() {
        callApiOfSlider();
        pageno = 1;

        offerApiCalling(pageno);

    }


    public void offerApiCalling(final int pageno) {
        new PostRequestHelper<OfferWrapper>().pingToRequest(Constants.OFFER_LISTING
                , null
                , null
                , new RequestParameter().getHashMapOffer(hashMapOffer(pageno))
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new OfferWrapper()
                , new TaskCompleteListener<OfferWrapper>() {
                    @Override
                    public void onSuccess(OfferWrapper offerWrapper) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();

                            if (pageno == 1)
                                quickInfoDatas.clear();

                            if (offerWrapper.getStatus() == 1) {
                                isPagination = true;
                                if (offerWrapper.getIsHurryup().equals("1")) {
                                    BaseFragment baseFragment = (BaseFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.placeholder);
                                    if (baseFragment instanceof TabFragment) {
                                        ((TabFragment) baseFragment).setVisibleHurryUp(true);
                                    }
                                } else {
                                    BaseFragment baseFragment = (BaseFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.placeholder);
                                    if (baseFragment instanceof TabFragment) {
                                        ((TabFragment) baseFragment).setVisibleHurryUp(false);
                                    }
                                }
                                setOfferAdapter(offerWrapper.getData());
                            } else if (offerWrapper.getStatus() == 0) {
                                isPagination = false;
                                if (pageno == 1)
                                    ((MainActivity) getActivity()).messageAlert(offerWrapper.getMessage());
                            }
                        }
                    }

                    public void onFailure(ExceptionType exceptions, OfferWrapper offerWrapper) {
                        isPagination = false;
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();

                            if (offerWrapper != null) {
                                if (pageno == 1)
                                    quickInfoDatas.clear();
                                setPage();

                                quickInfoAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                });
    }


    public void setPage() {
        pageno = 1;
    }

    public void setOfferAdapter(List<OfferData> offerDatas) {
        quickInfoDatas.addAll(offerDatas);
        quickInfoAdapter.notifyDataSetChanged();
    }

    public HashMap<String, String> hashMapOffer(int pageno) {
        HashMap<String, String> hashMapOffer = new HashMap<>();
        hashMapOffer.put("cat_id", quickInfoAdapter.selectedCategoryId());
        hashMapOffer.put("sub_cat_id", quickInfoAdapter.selectedSubCategoryId());
        BaseFragment baseFragment = (BaseFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.placeholder);
        if (baseFragment instanceof TabFragment) {
            search = ((TabFragment) baseFragment).getSearchData();
        }
        hashMapOffer.put("search", search);
        hashMapOffer.put("city_id", ((MainActivity) getActivity()).getTopCitySelectionId());
        hashMapOffer.put("page", pageno + "");
        return hashMapOffer;
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle("default", Enum.setNevigationIcon.MENU_CITY, false);
    }
}