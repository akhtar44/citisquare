package com.citisquare.app.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.SubscriptionPlanAdapter;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.interfaces.PaymentCallBack;
import com.citisquare.app.pojo.PayfortStatus;
import com.citisquare.app.pojo.response.FortSdkResponse;
import com.citisquare.app.pojo.response.LoginData;
import com.citisquare.app.pojo.response.LoginWrapper;
import com.citisquare.app.pojo.response.SubscriptionPlanData;
import com.citisquare.app.pojo.response.SubscriptionPlanWrapper;
import com.citisquare.app.utils.Debugger;
import com.google.gson.Gson;
import com.payfort.fort.android.sdk.base.FortSdk;
import com.payfort.sdk.android.dependancies.models.FortRequest;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import serviceCalling.builder.KeyValuePair;
import serviceCalling.helper.GetRequestHelper;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 4/1/17.
 */

public class SubscriptionPlanFragment extends BaseFragment implements PaymentCallBack {


    @BindView(R.id.subscriptionRecyclerView)
    RecyclerView subscriptionRecyclerView;

    SubscriptionPlanAdapter subscriptionPlanAdapter;
    List<SubscriptionPlanData> subscriptionPlanDatas;
    String device_id = "";
    LoginData loginData;
    LoginWrapper loginWrapper;

    Bundle bundle;
    String merchant_id = "";
    String plan_id = "";
    String bundleString = "";
    String language = "";
    private GridLayoutManager gridLayoutManager;
    private String amount;

    public static String encode(String base) {

        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_subscription_layout, container, false);

        ButterKnife.bind(this, view);
        subscriptionPlanDatas = new ArrayList<>();
        bundle = getArguments();
        if (bundle != null) {
            bundleString = bundle.getString("from");
        }

        if (Locale.getDefault().getDisplayLanguage().equals("العربية"))
            language = "ar";
        else
            language = "en";

        loginWrapper = ((MainActivity) getActivity()).getData(getContext());
        loginData = loginWrapper.getData();
        ((MainActivity) getActivity()).setPaymentCallBack(this);

        device_id = FortSdk.getDeviceId(getContext());
        Debugger.e("DEVICE ID IS::::" + device_id);
        subscriptionPlanAdapter = new SubscriptionPlanAdapter(getContext(), subscriptionPlanDatas, loginData.getSellerType(), new ItemEventListener<String>() {


            @Override
            public void onItemEventFired(String s, @Nullable String t2, String actualPos) {
                plan_id = t2;
                amount=s;
                wsCallGetSdkToken(s);
            }
        });

        gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        subscriptionRecyclerView.setLayoutManager(gridLayoutManager);
        subscriptionRecyclerView.setAdapter(subscriptionPlanAdapter);
        subscriptionPlanApiCall();

        return view;
    }

    private void wsCallGetSdkToken(final String amount) {
        if (getActivity() != null) {
            ((MainActivity) getActivity()).startLoader();
        }
        List<KeyValuePair> keyValuePairs = new ArrayList<>();
        keyValuePairs.add(new KeyValuePair("device_id", device_id));

        new GetRequestHelper<FortSdkResponse>().pingToRequest(Constants.GETSDK_TOKEN
                , keyValuePairs
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new FortSdkResponse()
                , new TaskCompleteListener<FortSdkResponse>() {
                    @Override
                    public void onSuccess(FortSdkResponse fortSdkResponse) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();
                        }
                        if (fortSdkResponse != null) {
                            if (!fortSdkResponse.getSdkToken().equalsIgnoreCase("")) {
                                next(fortSdkResponse.getSdkToken(), amount);
                            } else {
                                if (fortSdkResponse.getResponseCode() != null)
                                    ((MainActivity) getActivity()).messageAlert(fortSdkResponse.getResponseMessage());
                            }
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, FortSdkResponse subscriptionPlanWrapper) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();
                        }

                    }
                });

    }


    private void next(String sdkToken, String amount) {
        try {
            merchant_id = "" + System.currentTimeMillis();
            FortRequest fortRequest = new FortRequest();
            Double aDouble = Double.parseDouble(amount);
            aDouble = aDouble * 100;
            long d = (new Double(aDouble)).longValue();
            Map<String, String> hashMapString = new HashMap<String, String>();
            hashMapString.put("amount", String.valueOf(d));
            hashMapString.put("command", "PURCHASE");
            hashMapString.put("currency", "SAR");
            hashMapString.put("customer_email", loginData.getEmail());
            hashMapString.put("customer_name", loginData.getFirstName() + loginData.getLastName());
            hashMapString.put("language", language);
            hashMapString.put("merchant_reference", merchant_id);
            hashMapString.put("sdk_token", sdkToken);
            fortRequest.setShowResponsePage(true);
            ((MainActivity) getActivity()).payment(hashMapString);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    public void subscriptionPlanApiCall() {
        if (getActivity() != null) {
            ((MainActivity) getActivity()).startLoader();
        }

        new GetRequestHelper<SubscriptionPlanWrapper>().pingToRequest(Constants.SUBSCRIPTION_PLAN
                , new ArrayList<KeyValuePair>()
                , ((MainActivity) getActivity()).getHeader()
                , new SubscriptionPlanWrapper()
                , new TaskCompleteListener<SubscriptionPlanWrapper>() {
                    @Override
                    public void onSuccess(SubscriptionPlanWrapper subscriptionPlanWrapper) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();
                        }
                        if (subscriptionPlanWrapper.getStatus() == 1)
                            notifyAdapter(subscriptionPlanWrapper.getData());
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, SubscriptionPlanWrapper subscriptionPlanWrapper) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();
                        }
                        if (subscriptionPlanWrapper != null) {
                            if (subscriptionPlanWrapper.getStatus() == -1) {
                                ((MainActivity) getActivity()).invalidToken();
                            }
                        }
                    }
                });
    }

    private void notifyAdapter(List<SubscriptionPlanData> data) {
        subscriptionPlanDatas.clear();
        subscriptionPlanDatas.addAll(data);
        subscriptionPlanAdapter.notifyDataSetChanged();
    }


    @Override
    public void setHeaderTitle() {
        if (bundleString.equalsIgnoreCase("menu")) {
            ((MainActivity) getActivity()).setTitle(getString(R.string.subscriptionPlan), Enum.setNevigationIcon.BACK, false);
        } else {
            ((MainActivity) getActivity()).setTitle(getString(R.string.subscriptionPlan), Enum.setNevigationIcon.BACK, true);
        }
    }

    @Override
    public void onSucess(Map<String, String> map, Map<String, String> map1) {
        paymentCallApi(map1.get("fort_id"), "1");
    }

    @Override
    public void onFailureTransaction(Map<String, String> map, Map<String, String> map1) {
        if (map1.get("response_message").equalsIgnoreCase("Duplicate order number")) {
            getCheckStatus();
        } else {
            paymentCallApi("00", "0");
        }
    }

    private void getCheckStatus() {
        if (getActivity() != null)
            ((MainActivity) getActivity()).startLoader();
///check_payfortstatus/merchant_reference/12345

        ArrayList<KeyValuePair> keyValuePairs = new ArrayList<>();
        keyValuePairs.add(new KeyValuePair("merchant_reference", merchant_id));

        new GetRequestHelper<PayfortStatus>().pingToRequest(Constants.CHECK_STATUS
                , keyValuePairs
                , ((MainActivity) getActivity()).getHeader()
                , new PayfortStatus()
                , new TaskCompleteListener<PayfortStatus>() {
                    @Override
                    public void onSuccess(PayfortStatus subscriptionPlanWrapper) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();
                            if (subscriptionPlanWrapper.getStatus().equals("12") && subscriptionPlanWrapper.getTransactionCode().equals("14")) {
                                paymentCallApi(subscriptionPlanWrapper.getFortId(), "1");
                            } else {
                                wsCallGetSdkToken(amount);
                            }
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, PayfortStatus subscriptionPlanWrapper) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();
                        }
                        if (subscriptionPlanWrapper != null) {

                        }
                    }
                });
    }

    @Override
    public void onCancel(Map<String, String> map, Map<String, String> map1) {

    }




    private void paymentCallApi(String fortId, String status) {
        if (getActivity() != null)
            ((MainActivity) getActivity()).startLoader();

        new PostRequestHelper<LoginWrapper>().pingToRequest(Constants.SELLER_PLAN_PAYMENT
                , null
                , null
                , paymentHashMap(fortId, status)
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new LoginWrapper()
                , new TaskCompleteListener<LoginWrapper>() {
                    @Override
                    public void onSuccess(LoginWrapper loginWrapperComplete) {

                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();

                            if (loginWrapperComplete.getStatus() == 1) {
                                ((MainActivity) getActivity()).setData(getContext(), new Gson().toJson(loginWrapperComplete));
                                ((MainActivity) getActivity()).setFragment(new CategorySelectionFragment(), false, "");
                            }
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, LoginWrapper loginWrapperComplete) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        if (loginWrapperComplete != null) {
                            if (loginWrapperComplete.getStatus() == -1)
                                ((MainActivity) getActivity()).invalidToken();
                        }
                    }
                });
    }

    public HashMap<String, String> paymentHashMap(String fortId, String status) {
        HashMap<String, String> paymentHashMap = new HashMap<>();
        paymentHashMap.put("plan_id", plan_id);
        paymentHashMap.put("transaction_id", fortId);
        paymentHashMap.put("status", status);
        paymentHashMap.put("type", "P");

        return paymentHashMap;
    }
    public boolean onBackPressed() {
        try {
            Intent intent = new Intent(getActivity(), MainActivity.class);
            startActivity(intent);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

}
