package com.citisquare.app.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.ProfileVIewPagerAdapter;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.Rendering;
import com.citisquare.app.pojo.response.BadgeCounterWrapper;
import com.citisquare.app.pojo.response.Like;
import com.citisquare.app.pojo.response.LoginData;
import com.citisquare.app.pojo.response.ProfileData;
import com.citisquare.app.pojo.response.ProfileWrapper;
import com.citisquare.app.utils.ColorFilterTransformation;
import com.citisquare.app.utils.DataToPref;
import com.citisquare.app.utils.Debugger;
import com.citisquare.app.utils.RequestParameter;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.grantland.widget.AutofitTextView;
import serviceCalling.builder.KeyValuePair;
import serviceCalling.helper.GetRequestHelper;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 1/6/16.
 */
public class ProfileFragment extends BaseFragment implements ViewPager.OnPageChangeListener {

    public ProfileVIewPagerAdapter profileVIewPagerAdapter;
    @BindView(R.id.profileUserImage)
    ImageView profileUserImage;
    @BindView(R.id.profileUsername)
    TextView profileUsername;
    @BindView(R.id.profileQIVerified)
    TextView profileQIVerified;
    @BindView(R.id.otherProfileMap)
    TextView otherProfileMap;
    @BindView(R.id.otherProfilebio)
    TextView otherProfilebio;
    @BindView(R.id.otherProfileMessage)
    TextView otherProfileMessage;
    @BindView(R.id.otherProfileFollow)
    TextView otherProfileFollow;
    @BindView(R.id.otherProfile)
    LinearLayout otherProfile;
    @BindView(R.id.profileMap)
    AutofitTextView profileMap;
    @BindView(R.id.profileMyInfo)
    AutofitTextView profileMyInfo;
    @BindView(R.id.profileNewsFeed)
    AutofitTextView profileNewsFeed;
    @BindView(R.id.ownProfile)
    LinearLayout ownProfile;
    @BindView(R.id.framelayoutProfile)
    FrameLayout framelayoutProfile;
    @BindView(R.id.framelayoutPlaceholder)
    FrameLayout framelayoutPlaceholder;
    @BindView(R.id.profileViewPager)
    ViewPager profileViewPager;
    @BindView(R.id.profileTabNews)
    TextView profileTabNews;
    @BindView(R.id.profileTabGallery)
    TextView profileTabGallery;
    @BindView(R.id.profileTabFollowers)
    TextView profileTabFollowers;
    @BindView(R.id.profileTabFollowing)
    TextView profileTabFollowing;
    /* @BindView(R.id.editProfileImage)
     ImageView editProfileImage;*/

    @BindView(R.id.headerImageViewTimeline)
    ImageView headerImageViewTimeline;
    @BindView(R.id.imageEdit)
    ImageView imageEdit;
    @BindView(R.id.profileFragmentChange)
    FrameLayout profileFragmentChange;

    String bundleString;

    File timeLineImage;
    File editImage;
    Bundle bundle;
    View viewTab;
    @BindView(R.id.branches)
    AutofitTextView branches;
    View view;
    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;

    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    ProfileWrapper profileWrapper;
    @BindView(R.id.SubcriptionProfileTextView)
    CustomTextView SubcriptionProfileTextView;
    @BindView(R.id.defaultProfile)
    LinearLayout defaultProfile;
    @BindView(R.id.relativeLayout)
    RelativeLayout relativeLayout;
    @BindView(R.id.profileTabSelectionLayout)
    LinearLayout profileTabSelectionLayout;
    @BindView(R.id.shareProfile)
    ImageView shareProfile;
    @BindView(R.id.rateAndReviewCustomTextView)
    CustomTextView rateAndReviewCustomTextView;
    LoginData data;
    @BindView(R.id.profileUser)
    CustomTextView profileUser;
    private CountDownTimer cdt;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            view = inflater.inflate(R.layout.profile_header, container, false);
            ButterKnife.bind(this, view);
            viewTab = view.findViewById(R.id.tabbarLayout);
            bundle = getArguments();
            profileWrapper = new ProfileWrapper();
            profileViewPager = (ViewPager) view.findViewById(R.id.profileViewPager);
            data = ((MainActivity) getActivity()).getData(getContext()).getData();
            DataToPref.setData(getContext(), "makeYourLocation", "");

            if (bundle != null) {
                if (bundle.getString("profile", "").equals("own")) {
                    bundleString = "own";

                    profileVIewPagerAdapter = new ProfileVIewPagerAdapter(getResources(), getChildFragmentManager(), bundleString,getContext());
                    profileViewPager.setAdapter(profileVIewPagerAdapter);
                    profileViewPager.addOnPageChangeListener(this);

                    defaultOwnProfile();

                } else if (bundle.getString("profile", "").equals("other")) {
                    bundleString = "other";

                    profileVIewPagerAdapter = new ProfileVIewPagerAdapter(getResources(), getChildFragmentManager(), bundleString,getContext());
                    profileViewPager.setAdapter(profileVIewPagerAdapter);
                    profileViewPager.addOnPageChangeListener(this);

                    otherProfileView();

                }
            }


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (bundleString != null) {
                    if (bundleString.equals("own"))
                        defaultOwnProfile();
                    else
                        otherProfileView();
                }
            }
        }, 200);
        }catch (Exception ex)
        {
            ex.printStackTrace();
        }



        return view;
    }


    @Override
    public void setHeaderTitle() {
        if (bundleString.equals("own")) {
            Debugger.e("OWN PROFILE CALLED>>>>");
            ((MainActivity) getActivity()).setTitle(getString(R.string.title_myprofile), Enum.setNevigationIcon.MENU_SETTING, false);
        } else if (bundleString.equals("other"))
            ((MainActivity) getActivity()).setTitle(getString(R.string.title_profile), Enum.setNevigationIcon.BACK, true);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (bundle != null) {
            if (bundle.getString("profile", "").equals("own")) {
                bundleString = "own";
                defaultOwnProfile();

            } else if (bundle.getString("profile", "").equals("other")) {
                bundleString = "other";
                otherProfileView();
            }
        }


    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }


    @Override
    public void onPageSelected(int position) {
        if (getActivity() != null) {
            getChildFragmentManager().popBackStackImmediate();
        }

        if (position == 0) {
            if (getActivity() != null) {
                ((MainActivity) getActivity()).stopLoader();
                ((MainActivity) getActivity()).setSelectionPostion(0);
            }
            BaseFragment fragment = (BaseFragment) profileVIewPagerAdapter.getRegisteredFragment(profileViewPager.getCurrentItem());
            if (fragment != null) {
                if (fragment instanceof TimeLineProfileFragment) {
                    ((TimeLineProfileFragment) fragment).newsApiCall();
                }
            }
        }

        if (position == 1) {
            BaseFragment fragment = (BaseFragment) profileVIewPagerAdapter.getRegisteredFragment(profileViewPager.getCurrentItem());
            if (fragment != null) {
                if (fragment instanceof GalleryFragment) {
                    ((GalleryFragment) fragment).callGalleyApi();
                }
            }
        }
        if (position == 2) {
            BaseFragment fragment = (BaseFragment) profileVIewPagerAdapter.getRegisteredFragment(profileViewPager.getCurrentItem());
            if (fragment != null) {
                if (fragment instanceof FollowersFragment) {
                    ((FollowersFragment) fragment).followerApi();
                }
            }
        }

        if (position == 3) {
            BaseFragment fragment = (BaseFragment) profileVIewPagerAdapter.getRegisteredFragment(profileViewPager.getCurrentItem());
            if (fragment != null) {
                if (fragment instanceof FollowingFragment) {
                    ((FollowingFragment) fragment).followingApi();
                }
            }
        }
        profileViewPager.setCurrentItem(position);
        setSelection(position);
        profileViewPager.setVisibility(View.VISIBLE);
        profileFragmentChange.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @OnClick({R.id.profileTabNews, R.id.profileTabGallery, R.id.profileTabFollowers, R.id.profileTabFollowing})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.profileTabNews:
                onPageSelected(0);
                break;
            case R.id.profileTabGallery:
                onPageSelected(1);
                break;
            case R.id.profileTabFollowers:
                onPageSelected(2);
                break;
            case R.id.profileTabFollowing:
                onPageSelected(3);
                break;
        }
    }


    public void setSelection(int position) {
        profileTabNews.setTextColor(Color.WHITE);
        profileTabGallery.setTextColor(Color.WHITE);
        profileTabFollowers.setTextColor(Color.WHITE);
        profileTabFollowing.setTextColor(Color.WHITE);

        setSelectOwnProfile("bio");
        setOtherProfileSelection("bio");

        if (position == 0) {
            profileTabNews.setTextColor(getResources().getColor(R.color.selectionOfProfileTab));
        } else if (position == 1) {
            profileTabGallery.setTextColor(getResources().getColor(R.color.selectionOfProfileTab));
        } else if (position == 2) {
            profileTabFollowers.setTextColor(getResources().getColor(R.color.selectionOfProfileTab));
        } else if (position == 3) {
            profileTabFollowing.setTextColor(getResources().getColor(R.color.selectionOfProfileTab));
        }
    }


    /*COMMON CONTROL*/

    @OnClick(R.id.branches)
    public void branchesButtonClicked() {
        /*if (getActivity() != null) {
            ((MainActivity) getActivity()).messageAlert("Under development");
        }*/
        if (((MainActivity) getActivity()).getUserForWholeApp().equals(Enum.setUser.VISITOR)) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_visitor));
        } else {
            ((MainActivity) getActivity()).setFragment(new BranchesFragment(), true, "");
        }
    }

    /*SET OTHER PROFILE VIEW*/

    @OnClick(R.id.rateAndReviewCustomTextView)
    public void rateAndReviewClicked() {
        ((MainActivity) getActivity()).setFragment(new RateAndReviewFragment(), true, "");
    }

    @OnClick(R.id.otherProfileMap)
    public void otherProfileMapClicked() {
        ((MainActivity) getActivity()).setFragment(new OtherMapPathFragment(), true, "");
    }

    @OnClick(R.id.otherProfilebio)
    public void otherProfileBioClicked() {
        try {
            setOtherProfileSelection("bio");
            if (profileWrapper.getData().getRole().equalsIgnoreCase("S")) {
                SellerMyInfoFragment sellerMyInfoFragment = new SellerMyInfoFragment();
                Bundle bundleProfile = new Bundle();
                bundleProfile.putSerializable("otherData", profileWrapper);
                bundleProfile.putString("profile", bundleString);
                sellerMyInfoFragment.setArguments(bundleProfile);
                setFragment(sellerMyInfoFragment);
            } else {
                MyInfoFragment myInfoFragment = new MyInfoFragment();
                Bundle bundleProfile = new Bundle();
                bundleProfile.putSerializable("otherData", profileWrapper);
                bundleProfile.putString("profile", bundleString);
                myInfoFragment.setArguments(bundleProfile);
                setFragment(myInfoFragment);
            }
        } catch (Exception e) {

        }

    }

    @OnClick(R.id.otherProfileMessage)
    public void otherProfileMessageClicked() {
        //setOtherProfileSelection("message");
        if (((MainActivity) getActivity()).getUserForWholeApp().equals(Enum.setUser.GUEST)) {
            ((MainActivity) getActivity()).messageAlert(getActivity().getString(R.string.message_Guest));
        } else if (((MainActivity) getActivity()).getUserForWholeApp().equals(Enum.setUser.SELLER_BASIC)) {
            ((MainActivity) getActivity()).messageAlert(getActivity().getString(R.string.message_visitor));
        } else {
            ChatFragment chatFragment = new ChatFragment();
            Bundle bundleChat = new Bundle();
            bundleChat.putSerializable("otherProfile", profileWrapper.getData());
            chatFragment.setArguments(bundleChat);
            ((MainActivity) getActivity()).setFragment(chatFragment, false, "");
        }
    }

    @OnClick(R.id.otherProfileFollow)
    public void otherProfileFollowClicked() {
        if (((MainActivity) getActivity()).getUserForWholeApp().equals(Enum.setUser.GUEST)) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_Guest));
        } else if (((MainActivity) getActivity()).getUserForWholeApp().equals(Enum.setUser.SELLER_BASIC)) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_visitor));
        } else {
            if (profileWrapper != null) {
                //doFollowApiCall(profileWrapper.getData().getUserId(), "");

                if (profileWrapper.getData().getIsFollowing() == 1) {
                    ((MainActivity) getActivity()).alertTwoAction(getContext().getString(R.string.message_DoYouWantToUnFollow), new Rendering() {
                        @Override
                        public void response(boolean isCofirm) {
                            if (isCofirm) {
                                profileWrapper.getData().setIsFollowing(0);
                                otherProfileFollow.setText(getString(R.string.follow));
                                doFollowApiCall(profileWrapper.getData().getUserId(), "unfollow");
                                //itemEventListener.onItemEventFired("Unfollow", quickInfoDatas.get(position).getUserId());
                            }
                        }
                    });
                } else {
                    profileWrapper.getData().setIsFollowing(1);
                    otherProfileFollow.setText(getString(R.string.following));
                    doFollowApiCall(profileWrapper.getData().getUserId(), "follow");
                    /*quickInfoDatas.get(position).setIsFollowing(1);
                    itemEventListener.onItemEventFired("Follow", quickInfoDatas.get(position).getUserId());
                    notifyDataSetChanged();*/
                }
            }
        }
        //setOtherProfileSelection("follow");
    }

    @OnClick(R.id.shareProfile)
    public void shareProfileClicked() {
        if (profileWrapper != null) {
            ProfileData profileData = profileWrapper.getData();
            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.putExtra(Intent.EXTRA_TEXT, profileData.getCompanyName() + "  " + profileData.getUrl());
            sharingIntent.setType("text/plain");
            startActivity(Intent.createChooser(sharingIntent, "Share application using"));
        }
    }


    public void doFollowApiCall(final String userId, final String status) {
        new PostRequestHelper<Like>().pingToRequest(Constants.DO_FOLLOW
                , null
                , null
                , new RequestParameter().getHashMapForDoFollow(getDoFollowMap(userId, status))
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new Like()
                , new TaskCompleteListener<Like>() {
                    @Override
                    public void onSuccess(Like mObject) {
                        Debugger.e("FOLLOW SUCCESSFULLY");
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, Like like) {
                        if (like != null) {
                            if (like.getStatus() == -1)
                                ((MainActivity) getActivity()).invalidToken();
                        }
                    }
                });
    }


    public HashMap<String, String> getDoFollowMap(String userId, String status) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("follower_user_id", userId);
        if (status.equals("follow"))
            hashMap.put("status", "1");
        else
            hashMap.put("status", "0");

        return hashMap;
    }


    /*SET OWN PROFILE*/

    @OnClick(R.id.SubcriptionProfileTextView)
    public void subscriptionProfileClicked() {

        if (((MainActivity) getActivity()).getUserForWholeApp().equals(Enum.setUser.VISITOR)) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_visitor));
        } else {
            // setFragment(new CategorySelectionFragment(), false, "");
            ((MainActivity) getActivity()).setSelectionPostion(8);
            SubscriptionPlanFragment subscriptionPlanFragment = new SubscriptionPlanFragment();
            Bundle bundleSubscription = new Bundle();
            bundleSubscription.putString("from", "menu");
            subscriptionPlanFragment.setArguments(bundleSubscription);
            ((MainActivity) getActivity()).setFragment(subscriptionPlanFragment, false, "");
        }
    }

    @OnClick(R.id.profileMyInfo)
    public void profileMyInfoClicked() {
        setSelectOwnProfile("my_info");
        if (data != null) {
            if (data.getRole().equalsIgnoreCase("S")) {
                SellerMyInfoFragment sellerMyInfoFragment = new SellerMyInfoFragment();
                Bundle bundleProfile = new Bundle();
                bundleProfile.putString("profile", bundleString);
                sellerMyInfoFragment.setArguments(bundleProfile);
                setFragment(sellerMyInfoFragment);
            } else {
                MyInfoFragment myInfoFragment = new MyInfoFragment();
                Bundle bundleProfile = new Bundle();
                bundleProfile.putString("profile", bundleString);
                myInfoFragment.setArguments(bundleProfile);
                setFragment(myInfoFragment);
            }
        /*MyInfoFragment myInfoFragment = new MyInfoFragment();
        Bundle bundleProfile = new Bundle();
        bundleProfile.putString("profile", bundleString);
        myInfoFragment.setArguments(bundleProfile);
        setFragment(myInfoFragment);*/
        }
    }

    @OnClick(R.id.profileMap)
    public void profileMapClicked() {
        OtherMapPathFragment otherMapPathFragment = new OtherMapPathFragment();
        Bundle bundle = new Bundle();
        bundle.putString("from", "own");
        otherMapPathFragment.setArguments(bundle);
        ((MainActivity) getActivity()).setFragment(otherMapPathFragment, false, "");
        //((MainActivity) getActivity()).setFragment(new ProfileMapFragment(), false);
    }

    @OnClick(R.id.profileNewsFeed)
    public void profileNewsFeedClicked() {
        setSelectOwnProfile("news_feed");
        TimeLineProfileFragment newsProfileFragment = new TimeLineProfileFragment();
        Bundle bundleNews = new Bundle();
        bundleNews.putString("FROM", "NF");
        newsProfileFragment.setArguments(bundleNews);
        setFragment(newsProfileFragment);
    }

    public void setViewPagerSelectionNothing() {
        profileTabNews.setTextColor(Color.WHITE);
        profileTabGallery.setTextColor(Color.WHITE);
        profileTabFollowers.setTextColor(Color.WHITE);
        profileTabFollowing.setTextColor(Color.WHITE);
    }


    @OnClick(R.id.imageEdit)
    public void imageEditClicked() {
        if (((MainActivity) getActivity()).getUserForWholeApp().equals(Enum.setUser.SELLER_BASIC) || ((MainActivity) getActivity()).getUserForWholeApp().equals(Enum.setUser.SELLER_PREMIUM)) {
            ((MainActivity) getActivity()).setFragment(new SellerEditFragment(), true, "");
        } else {
            ((MainActivity) getActivity()).setFragment(new VisitorProfileEditFragment(), true, "");
        }
        //((MainActivity) getActivity()).setFragment(new MyInfoEditModeFragment(), true, "");
    }

    /*public boolean editProfileIsVisible() {
        return editLayout.getVisibility() == View.VISIBLE;
    }*/

    /*SET FRAGMENT ....*/

    public void setFragment(BaseFragment fragment) {

        getChildFragmentManager().beginTransaction().replace(R.id.profileFragmentChange, fragment, "profile")
                .commitAllowingStateLoss();
    }

    public void defaultOwnProfile() {
        try {
            if (getActivity() != null) {
                /*if (!((MainActivity) getActivity()).getUserForWholeApp().equals(Enum.setUser.GUEST)) {
                    cdt = new CountDownTimer(System.currentTimeMillis(), 30000) {
                        public void onTick(long millisUntilFinished) {
                            callMessageBadgeCounterApi();
                        }

                        public void onFinish() {
                        }
                    }.start();
                }*/

                rateAndReviewCustomTextView.setVisibility(View.INVISIBLE);

                shareProfile.setVisibility(View.INVISIBLE);
                SubcriptionProfileTextView.setVisibility(View.VISIBLE);
                if(((MainActivity) getActivity()).getData(getContext()).getData().getProfileImageThumb()!=null)
                {
                    ((MainActivity) getActivity()).setCircularImage(profileUserImage, ((MainActivity) getActivity()).getData(getContext()).getData().getProfileImageThumb());
                }

                String name = "";
                String defaultimage;
                if (!((MainActivity) getActivity()).getUserForWholeApp().equals(Enum.setUser.VISITOR)) {
                    //name = ((MainActivity) getActivity()).getData(getContext()).getData().getFirstName() + " " + ((MainActivity) getActivity()).getData(getContext()).getData().getLastName();
                    name = ((MainActivity) getActivity()).getData(getContext()).getData().getUsername();
                } else {
                    name = ((MainActivity) getActivity()).getData(getContext()).getData().getCompanyName();
                }
                defaultimage = ((MainActivity) getActivity()).getData(getContext()).getData().getProfileImage();
                profileUsername.setText(name);
                profileUser.setText(((MainActivity) getActivity()).getData(getContext()).getData().getUsername());
                profileUserImage.setVisibility(View.VISIBLE);
                viewTab.setVisibility(View.VISIBLE);
                imageEdit.setVisibility(View.GONE);
                defaultProfile.setVisibility(View.VISIBLE);
                ownProfile.setVisibility(View.VISIBLE);
                otherProfile.setVisibility(View.INVISIBLE);
                branches.setVisibility(View.VISIBLE);

                if (data != null) {
                    if (data.getIsActive().equals("2")) {
                        profileQIVerified.setVisibility(View.VISIBLE);
                    } else {
                        profileQIVerified.setVisibility(View.INVISIBLE);
                    }

                    if (data.getRole().equalsIgnoreCase("S")) {
                        onPageSelected(0);
                        branches.setVisibility(View.VISIBLE);
                        SubcriptionProfileTextView.setVisibility(View.VISIBLE);
                        profileTabGallery.setVisibility(View.VISIBLE);
                        profileTabNews.setVisibility(View.VISIBLE);
                    } else {
                        onPageSelected(2);
                        branches.setVisibility(View.INVISIBLE);
                        SubcriptionProfileTextView.setVisibility(View.INVISIBLE);
                        profileTabGallery.setVisibility(View.GONE);
                        profileTabNews.setVisibility(View.GONE);
                    }
                }
                onPageSelected(0);
                setHeaderTitle();
                if(((MainActivity) getActivity()).getData(getContext()).getData().getCoverImage()!=null)
                {
                    if (!((MainActivity) getActivity()).getData(getContext()).getData().getCoverImage().equals("")) {
                        Picasso.with(getActivity()).load(((MainActivity) getActivity()).getData(getContext()).getData().getCoverImage())
                                .transform(new ColorFilterTransformation(getResources().getColor(R.color.transparent_blue)))
                                .into(headerImageViewTimeline);
                    }
                }
            }
        }catch (Exception ex)
        {
            ex.printStackTrace();
        }


    }

    private void callMessageBadgeCounterApi() {
        if (!((MainActivity) getActivity()).getUserForWholeApp().equals(Enum.setUser.GUEST)) {
            new GetRequestHelper<BadgeCounterWrapper>().pingToRequest(Constants.BADGE_COUNTER,
                    new ArrayList<KeyValuePair>(),
                    ((MainActivity) getActivity()).getParamWholeParameters(),
                    new BadgeCounterWrapper(),
                    new TaskCompleteListener<BadgeCounterWrapper>() {
                        @Override
                        public void onSuccess(BadgeCounterWrapper badgeCounterWrapper) {
                            if (badgeCounterWrapper.getStatus() == 1 && getActivity() != null) {
                                ((MainActivity) getActivity()).setBadgedata(badgeCounterWrapper.getData());
                            }
                        }

                        @Override
                        public void onFailure(ExceptionType exceptions, BadgeCounterWrapper badgeCounterWrapper) {
                            if (badgeCounterWrapper != null) {
                                if (badgeCounterWrapper.getStatus() == -1)
                                    ((MainActivity) getActivity()).invalidToken();
                            }
                        }
                    });
        }
    }

    public void otherProfileView() {
        if (cdt != null) {
            cdt.cancel();
        }
        sellerDetailsApiCall();
        SubcriptionProfileTextView.setVisibility(View.INVISIBLE);
        viewTab.setVisibility(View.VISIBLE);
        if (((MainActivity) getActivity()).getUserForWholeApp().equals(Enum.setUser.SELLER_BASIC) || ((MainActivity) getActivity()).getUserForWholeApp().equals(Enum.setUser.SELLER_PREMIUM)) {
            shareProfile.setVisibility(View.VISIBLE);
            rateAndReviewCustomTextView.setVisibility(View.VISIBLE);
        } else {
            shareProfile.setVisibility(View.INVISIBLE);
            rateAndReviewCustomTextView.setVisibility(View.INVISIBLE);
        }
        imageEdit.setVisibility(View.GONE);
        //editLayout.setVisibility(View.INVISIBLE);
        defaultProfile.setVisibility(View.VISIBLE);
        otherProfile.setVisibility(View.VISIBLE);
        ownProfile.setVisibility(View.INVISIBLE);
        branches.setVisibility(View.INVISIBLE);
        //onPageSelected(0);
        setHeaderTitle();
    }


    public void sellerDetailsApiCall() {
        Debugger.e("CALL :::::");
        if (getActivity() != null)
            ((MainActivity) getActivity()).startLoader();

        List<KeyValuePair> profileKeyValue = new ArrayList<>();
        profileKeyValue.add(new KeyValuePair("user_id", ((MainActivity) getActivity()).getProfileUserId()));

        new GetRequestHelper<ProfileWrapper>().pingToRequest(Constants.SELLER_DETAILS
                , profileKeyValue
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new ProfileWrapper()
                , new TaskCompleteListener<ProfileWrapper>() {
                    @Override
                    public void onSuccess(ProfileWrapper profileWrapper) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        if (profileWrapper.getStatus() == 1) {
                            if (getActivity() != null)
                                setOtherProfileData(profileWrapper);
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, ProfileWrapper profileWrapper) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();
                    }
                });
    }

    public void setOtherProfileData(ProfileWrapper profileWrapper) {
        try {
            if (getActivity() != null) {
                this.profileWrapper = profileWrapper;
                ((MainActivity) getActivity()).setCircularImage(profileUserImage, profileWrapper.getData().getProfileImageThumb());
                if (profileWrapper.getData().getIsActive().equals("2")) {
                    profileQIVerified.setVisibility(View.VISIBLE);
                } else {
                    profileQIVerified.setVisibility(View.INVISIBLE);
                }
                String name;
                if (profileWrapper.getData().getRole().equalsIgnoreCase("S")) {
                    name = profileWrapper.getData().getCompanyName();
                } else {
                    name = profileWrapper.getData().getFirstName() + " " + profileWrapper.getData().getLastName();
                }
                profileUsername.setText(name);
                profileUser.setText(profileWrapper.getData().getUsername());

                if (profileWrapper.getData().getCoverImage().equalsIgnoreCase("")) {
                    headerImageViewTimeline.setColorFilter(getResources().getColor(R.color.transparent_blue));
                } else {
                    Picasso.with(getActivity()).load(profileWrapper.getData().getCoverImageThumb())
                            .transform(new ColorFilterTransformation(getResources().getColor(R.color.transparent_blue)))
                            .into(headerImageViewTimeline);
                }
                if (profileWrapper.getData().getIsFollowing() == 1) {
                    otherProfileFollow.setText(getString(R.string.following));
                }
                if (profileWrapper.getData().getRole().equalsIgnoreCase("S")) {
                    onPageSelected(0);
                    //profileViewPager.setCurrentItem(0);
                    profileTabGallery.setVisibility(View.VISIBLE);
                    profileTabNews.setVisibility(View.VISIBLE);
                    otherProfilebio.setVisibility(View.VISIBLE);
                    shareProfile.setVisibility(View.VISIBLE);
                    rateAndReviewCustomTextView.setVisibility(View.VISIBLE);
                } else {
                    onPageSelected(0);
                    profileTabGallery.setVisibility(View.GONE);
                    otherProfilebio.setVisibility(View.GONE);
                    profileTabNews.setVisibility(View.GONE);
                    shareProfile.setVisibility(View.INVISIBLE);
                    rateAndReviewCustomTextView.setVisibility(View.INVISIBLE);
                }

                if (profileWrapper.getData().getRole().equalsIgnoreCase("V") && ((MainActivity) getActivity()).getData(getContext()).getData().getRole().equalsIgnoreCase("V")) {
                    otherProfileFollow.setEnabled(false);

                } else {
                    otherProfile.setEnabled(true);
                }
            }
        }catch (Exception ex)
        {
            ex.printStackTrace();
        }

    }

    public void setSelectOwnProfile(String selection) {
        setViewPagerSelectionNothing();
        int darkBlue = getResources().getColor(R.color.dark_blue);
        int colorBack = getResources().getColor(R.color.colorPrimary);
        profileViewPager.setVisibility(View.INVISIBLE);
        profileFragmentChange.setVisibility(View.VISIBLE);
        //scroll.setVisibility(View.VISIBLE);
        int white = Color.WHITE;
        if (ownProfile.getVisibility() == View.VISIBLE) {

            switch (selection) {
                case "map":
                    profileMap.setTextColor(darkBlue);
                    profileMap.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.map_icn_sel), null, null, null);
                    profileMap.setBackground(getResources().getDrawable(R.drawable.selected_profile_drawable));

                    profileMyInfo.setTextColor(white);
                    profileMyInfo.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.myinfo_icn), null, null, null);
                    profileMyInfo.setBackground(getResources().getDrawable(R.drawable.white_border_profile));

                    profileNewsFeed.setTextColor(white);
                    profileNewsFeed.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.news), null, null, null);
                    profileNewsFeed.setBackground(getResources().getDrawable(R.drawable.white_border_profile));

                    break;
                case "my_info":
                    profileMap.setTextColor(white);
                    profileMap.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.map_marker_icn), null, null, null);
                    profileMap.setBackground(getResources().getDrawable(R.drawable.white_border_profile));

                    profileMyInfo.setTextColor(darkBlue);
                    profileMyInfo.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.myinfo_icn_sel), null, null, null);
                    profileMyInfo.setBackground(getResources().getDrawable(R.drawable.selected_profile_drawable));

                    profileNewsFeed.setTextColor(white);
                    profileNewsFeed.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.news), null, null, null);
                    profileNewsFeed.setBackground(getResources().getDrawable(R.drawable.white_border_profile));
                    break;
                case "news_feed":
                    profileMap.setTextColor(white);
                    profileMap.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.map_marker_icn), null, null, null);
                    profileMap.setBackground(getResources().getDrawable(R.drawable.white_border_profile));

                    profileMyInfo.setTextColor(white);
                    profileMyInfo.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.myinfo_icn), null, null, null);
                    profileMyInfo.setBackground(getResources().getDrawable(R.drawable.white_border_profile));

                    profileNewsFeed.setTextColor(darkBlue);
                    profileNewsFeed.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.news_sel), null, null, null);
                    profileNewsFeed.setBackground(getResources().getDrawable(R.drawable.selected_profile_drawable));
                    break;
                case "nothing":
                    profileMap.setTextColor(white);
                    profileMap.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.map_marker_icn), null, null, null);
                    profileMap.setBackground(getResources().getDrawable(R.drawable.white_border_profile));

                    profileMyInfo.setTextColor(white);
                    profileMyInfo.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.myinfo_icn), null, null, null);
                    profileMyInfo.setBackground(getResources().getDrawable(R.drawable.white_border_profile));

                    profileNewsFeed.setTextColor(white);
                    profileNewsFeed.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.news), null, null, null);
                    profileNewsFeed.setBackground(getResources().getDrawable(R.drawable.white_border_profile));
                    break;
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (cdt != null) {
            cdt.cancel();
        }
    }

    public void setOtherProfileSelection(String selection) {
        int darkBlue = getResources().getColor(R.color.dark_blue);
        int colorBack = getResources().getColor(R.color.colorPrimary);
        profileViewPager.setVisibility(View.INVISIBLE);
        profileFragmentChange.setVisibility(View.VISIBLE);
        //scroll.setVisibility(View.VISIBLE);
        int white = Color.WHITE;
        // setViewPagerSelectionNothing();

        switch (selection) {
            case "map":
                otherProfileMap.setTextColor(darkBlue);
                otherProfileMap.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.map_icn_sel), null, null, null);
                otherProfileMap.setBackground(getResources().getDrawable(R.drawable.selected_profile_drawable));

                otherProfilebio.setTextColor(white);
                otherProfilebio.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.myinfo_icn), null, null, null);
                otherProfilebio.setBackground(getResources().getDrawable(R.drawable.white_border_profile));

                otherProfileMessage.setTextColor(white);
                otherProfileMessage.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.send_msg_icn), null, null, null);
                otherProfileMessage.setBackground(getResources().getDrawable(R.drawable.white_border_profile));

                otherProfileFollow.setTextColor(white);
                otherProfileFollow.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.follow_icn), null, null, null);
                otherProfileFollow.setBackground(getResources().getDrawable(R.drawable.white_border_profile));

                break;
            case "bio":
                otherProfileMap.setTextColor(white);
                otherProfileMap.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.map_marker_icn), null, null, null);
                otherProfileMap.setBackground(getResources().getDrawable(R.drawable.white_border_profile));

                otherProfilebio.setTextColor(darkBlue);
                otherProfilebio.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.myinfo_icn_sel), null, null, null);
                otherProfilebio.setBackground(getResources().getDrawable(R.drawable.selected_profile_drawable));

                otherProfileMessage.setTextColor(white);
                otherProfileMessage.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.send_msg_icn), null, null, null);
                otherProfileMessage.setBackground(getResources().getDrawable(R.drawable.white_border_profile));

                otherProfileFollow.setTextColor(white);
                otherProfileFollow.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.follow_icn), null, null, null);
                otherProfileFollow.setBackground(getResources().getDrawable(R.drawable.white_border_profile));
                break;
            case "message":
                otherProfileMap.setTextColor(white);
                otherProfileMap.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.map_marker_icn), null, null, null);
                otherProfileMap.setBackground(getResources().getDrawable(R.drawable.white_border_profile));

                otherProfilebio.setTextColor(white);
                otherProfilebio.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.myinfo_icn), null, null, null);
                otherProfilebio.setBackground(getResources().getDrawable(R.drawable.white_border_profile));

                otherProfileMessage.setTextColor(darkBlue);
                otherProfileMessage.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.send_msg_icn_sel), null, null, null);
                otherProfileMessage.setBackground(getResources().getDrawable(R.drawable.selected_profile_drawable));

                otherProfileFollow.setTextColor(white);
                otherProfileFollow.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.follow_icn), null, null, null);
                otherProfileFollow.setBackground(getResources().getDrawable(R.drawable.white_border_profile));

                break;
            case "follow":
                otherProfileMap.setTextColor(white);
                otherProfileMap.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.map_marker_icn), null, null, null);
                otherProfileMap.setBackground(getResources().getDrawable(R.drawable.white_border_profile));

                otherProfilebio.setTextColor(white);
                otherProfilebio.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.myinfo_icn), null, null, null);
                otherProfilebio.setBackground(getResources().getDrawable(R.drawable.white_border_profile));

                otherProfileMessage.setTextColor(white);
                otherProfileMessage.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.send_msg_icn), null, null, null);
                otherProfileMessage.setBackground(getResources().getDrawable(R.drawable.white_border_profile));

                otherProfileFollow.setTextColor(darkBlue);
                otherProfileFollow.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.follow_icn_sel), null, null, null);
                otherProfileFollow.setBackground(getResources().getDrawable(R.drawable.selected_profile_drawable));
                break;
            case "nothing":
                otherProfileMap.setTextColor(white);
                otherProfileMap.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.map_marker_icn), null, null, null);
                otherProfileMap.setBackground(getResources().getDrawable(R.drawable.white_border_profile));

                otherProfilebio.setTextColor(white);
                otherProfilebio.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.myinfo_icn), null, null, null);
                otherProfilebio.setBackground(getResources().getDrawable(R.drawable.white_border_profile));

                otherProfileMessage.setTextColor(white);
                otherProfileMessage.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.send_msg_icn), null, null, null);
                otherProfileMessage.setBackground(getResources().getDrawable(R.drawable.white_border_profile));

                otherProfileFollow.setTextColor(white);
                otherProfileFollow.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.follow_icn), null, null, null);
                otherProfileFollow.setBackground(getResources().getDrawable(R.drawable.white_border_profile));
                break;
        }
    }

}
