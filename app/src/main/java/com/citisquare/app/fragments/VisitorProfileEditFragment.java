package com.citisquare.app.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.PlaceAutocompleteAdapter;
import com.citisquare.app.controls.CustomAutoComplete;
import com.citisquare.app.controls.CustomEditText;
import com.citisquare.app.controls.CustomRadioButton;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.dialog.CountryDialog;
import com.citisquare.app.dialog.ImageChooserDialog;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ImageChooserListener;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.interfaces.Navigation;
import com.citisquare.app.pojo.addresslocation.Address;
import com.citisquare.app.pojo.response.City;
import com.citisquare.app.pojo.response.Country;
import com.citisquare.app.pojo.response.LoginData;
import com.citisquare.app.pojo.response.LoginWrapper;
import com.citisquare.app.utils.ColorFilterTransformation;
import com.citisquare.app.utils.Debugger;
import com.citisquare.app.utils.InitializeMenu;
import com.citisquare.app.utils.RequestParameter;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import serviceCalling.builder.KeyValuePair;
import serviceCalling.builder.ServiceParameter;
import serviceCalling.helper.GetRequestHelper;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 29/12/16.
 */

public class VisitorProfileEditFragment extends BaseFragment {
    ArrayList<City> cityList;
    @BindView(R.id.editProfileVisitorCoverImage)
    ImageView editProfileVisitorCoverImage;
    @BindView(R.id.editProfileVisitorImage)
    ImageView editProfileVisitorImage;
    @BindView(R.id.changePasswordVisitor)
    CustomTextView changePasswordVisitor;
    @BindView(R.id.editProfileVisitorCoverImageChoose)
    CustomTextView editProfileVisitorCoverImageChoose;
    @BindView(R.id.editLayoutVisitor)
    FrameLayout editLayoutVisitor;
    @BindView(R.id.visitorProfileTitle)
    CustomTextView visitorProfileTitle;
    @BindView(R.id.visitorProfileFirstname)
    CustomEditText visitorProfileFirstname;
    @BindView(R.id.editTextVisitorProfileLastName)
    CustomEditText editTextVisitorProfileLastName;
    @BindView(R.id.editTextVisitorProfileUserName)
    CustomEditText editTextVisitorProfileUserName;
    @BindView(R.id.editTextVisitorProfileEmail)
    CustomEditText editTextVisitorProfileEmail;
    @BindView(R.id.editTextVisitorProfileBuildingNumber)
    CustomEditText editTextVisitorProfileBuildingNumber;
    @BindView(R.id.editTextVisitorProfileStreet)
    CustomAutoComplete editTextVisitorProfileStreet;
    @BindView(R.id.editTextVisitorProfileZipCode)
    CustomAutoComplete editTextVisitorProfileZipCode;
    @BindView(R.id.editTextVisitorProfileCity)
    CustomTextView editTextVisitorProfileCity;
    @BindView(R.id.editTextVisitorProfileCountry)
    CustomTextView editTextVisitorProfileCountry;
    @BindView(R.id.editTextVisitorProfileCountryCode)
    CustomTextView editTextVisitorProfileCountryCode;
    @BindView(R.id.editTextVisitorProfilePhoneNumber)
    CustomEditText editTextVisitorProfilePhoneNumber;
    @BindView(R.id.editTextVisitorProfileBio)
    CustomEditText editTextVisitorProfileBio;
    LoginWrapper loginWrapper;
    LoginData loginData;
    @BindView(R.id.englishRadioButtonVisitorEditProfile)
    CustomRadioButton englishRadioButtonVisitorEditProfile;
    @BindView(R.id.arabicRadioButtonVisitorEditProfile)
    CustomRadioButton arabicRadioButtonVisitorEditProfile;
    @BindView(R.id.preferredLanguageRadioVisitorEditProfile)
    RadioGroup preferredLanguageRadioVisitorEditProfile;
    private Pattern pattern;
    private Matcher matcher;
    private File timeLineImage;
    private File visitorEditImage;
    private AdvertisementDropDown advertisementDropDown;
    private ArrayList<String> titleList;
    private City city;
    private CityDropDownSeller cityDropDown;
    private Country country;
    private ArrayList<Country> editVisitorCountry;
    private CountryCodeDropDown countryCodeDropDown;
    private Double lat;
    private Double lng;
    private PlaceAutocompleteAdapter placeAdapter;
    private String languageString = "";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_edit_visitor_profile, container, false);
        ButterKnife.bind(this, view);
        pattern = Pattern.compile(Constants.englishOnly);
        preferredLanguageRadioVisitorEditProfile.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int radio = preferredLanguageRadioVisitorEditProfile.getCheckedRadioButtonId();
                switch (radio) {
                    case R.id.englishRadioButtonVisitorEditProfile:
                        languageString = String.valueOf(Constants.forLanguage.ENGLISH);
                        break;
                    case R.id.arabicRadioButtonVisitorEditProfile:
                        languageString = String.valueOf(Constants.forLanguage.ARABIC);
                        break;
                }
            }
        });

        cityList = ((MainActivity) getActivity()).cityArrayList();
        /*countryList = ((MainActivity) getActivity()).countryDatas();*/
        loginWrapper = ((MainActivity) getActivity()).getData(getContext());

        editVisitorCountry = ((MainActivity) getActivity()).countryDatas();

        pattern = Pattern.compile(Constants.englishOnly);

        if (loginWrapper != null) {
            loginData = loginWrapper.getData();
            visitorProfileTitle.setText(loginData.getTitle());
            visitorProfileFirstname.setText(loginData.getFirstName());
            editTextVisitorProfileLastName.setText(loginData.getLastName());
            editTextVisitorProfileUserName.setText(loginData.getUsername());
            editTextVisitorProfileEmail.setText(loginData.getEmail());
            editTextVisitorProfileBuildingNumber.setText(loginData.getBuildingNo());
            editTextVisitorProfileStreet.setText(loginData.getStreet());
            editTextVisitorProfileZipCode.setText(loginData.getZipcode());
            editTextVisitorProfileCountryCode.setText(loginData.getCountryCode());
            editTextVisitorProfilePhoneNumber.setText(loginData.getPhoneNo());
            editTextVisitorProfileBio.setText(loginData.getBio());

            if (loginData.getLanguage().equalsIgnoreCase(String.valueOf(Constants.forLanguage.ENGLISH))) {
                englishRadioButtonVisitorEditProfile.setChecked(true);
            } else {
                arabicRadioButtonVisitorEditProfile.setChecked(true);
            }

            ((MainActivity) getActivity()).setCircularImage(editProfileVisitorImage, loginData.getProfileImageThumb());

            for (int i = 0; i < cityList.size(); i++) {
                if (cityList.get(i).getId().equals(loginData.getCity())) {
                    editTextVisitorProfileCity.setText(cityList.get(i).getName());
                    String cityGson = new Gson().toJson(cityList.get(i));
                    city = new Gson().fromJson(cityGson, City.class);
                    break;
                }
            }

            for (int i = 0; i < editVisitorCountry.size(); i++) {
                if (editVisitorCountry.get(i).getId().equals(loginData.getCountry())) {
                    editTextVisitorProfileCountry.setText(editVisitorCountry.get(i).getCountry());
                    String countryGson = new Gson().toJson(editVisitorCountry.get(i));
                    country = new Gson().fromJson(countryGson, Country.class);
                    break;
                }
            }
            if (!loginData.getCoverImage().equalsIgnoreCase(""))
                Picasso.with(getActivity()).load(loginData.getCoverImage()).transform(new ColorFilterTransformation(getResources().getColor(R.color.transparent_blue))).into(editProfileVisitorCoverImage);


        }


        placeAdapter = new PlaceAutocompleteAdapter(getContext(), R.layout.raw_autocomplete_text);
        editTextVisitorProfileStreet.setAdapter(placeAdapter);

        editTextVisitorProfileStreet.setThreshold(1);


        editTextVisitorProfileBio.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.editTextVisitorProfileBio) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        return view;
    }

    public void editVisitorProfile() {
        if (editTextVisitorProfileStreet.getText().toString().equals(loginData.getStreet())) {
            Double lat, lng;
            if (loginData.getLatitude() != null) {
                lat = Double.parseDouble(loginData.getLatitude());
                lng = Double.parseDouble(loginData.getLongitude());
            } else {
                lat = 0d;
                lng = 0d;
            }
            visitorEdit(lat, lng);
        } else {
            getLocation();
        }
    }

    public void getLocation() {
        new GetRequestHelper<Address>().pingToRequest("http://maps.googleapis.com/maps/api/geocode/json?address=" +
                        editTextVisitorProfileStreet.getText().toString().replace("\"", "") + "&sensor=true"
                , new ArrayList<KeyValuePair>()
                , new ServiceParameter()
                , new Address()
                , new TaskCompleteListener<Address>() {

                    @Override
                    public void onSuccess(Address mObject) {
                        try {
                            Debugger.e("GET LOCATION:::" + mObject);
                            if (mObject.getStatus().equalsIgnoreCase("ok")) {
                                lat = mObject.getResults().get(0).getGeometry().getLocation().getLat();
                                lng = mObject.getResults().get(0).getGeometry().getLocation().getLng();
                                visitorEdit(lat, lng);
                            } else {
                                ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterValidLocation));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, Address address) {

                    }
                });
    }


    public void visitorEdit(Double lat, Double lng) {
        if (getActivity() != null)
            ((MainActivity) getActivity()).startLoader();

        List<KeyValuePair> keyValuePairs = new ArrayList<>();
        if (timeLineImage != null)
            keyValuePairs.add(new KeyValuePair("cover_image", timeLineImage.getAbsolutePath()));

        if (visitorEditImage != null)
            keyValuePairs.add(new KeyValuePair("profile_image", visitorEditImage.getAbsolutePath()));

        new PostRequestHelper<LoginWrapper>().pingToRequest(Constants.VISITOR_EDIT
                , "image/*"
                , keyValuePairs
                , new RequestParameter().getHashMapVisitorEdit(getHashMapVisitorEdit(lat, lng))
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new LoginWrapper()
                , new TaskCompleteListener<LoginWrapper>() {
                    @Override
                    public void onSuccess(LoginWrapper mObject) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();

                            if (mObject != null) {
                                if (mObject.getStatus() == 1) {
                                    //((MainActivity) getActivity()).messageAlert(mObject.getMessage());
                                    Toast.makeText(getActivity(),mObject.getMessage(),Toast.LENGTH_LONG).show();
                                    ((MainActivity) getActivity()).setData(getContext(), new Gson().toJson(mObject));
                                    //getActivity().onBackPressed();
                                } else {
                                    //((MainActivity) getActivity()).messageAlert(mObject.getMessage());
                                    Toast.makeText(getActivity(),mObject.getMessage(),Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, LoginWrapper loginWrapper) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();


                        if (loginWrapper != null) {
                            //((MainActivity) getActivity()).messageAlert(loginWrapper.getMessage());
                            Toast.makeText(getActivity(),loginWrapper.getMessage(),Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    /*title,first_name,last_name,username,email,phone_no,building_no,street,zipcode,city,country,
    country_code,device_type,device_token,latitude,longitude	bio,cover_image,profile_image	*/
    public HashMap<String, String> getHashMapVisitorEdit(Double lat, Double lng) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("title", visitorProfileTitle.getText().toString().trim());
        hashMap.put("first_name", visitorProfileFirstname.getText().toString().trim());
        hashMap.put("last_name", editTextVisitorProfileLastName.getText().toString().trim());
        hashMap.put("username", editTextVisitorProfileUserName.getText().toString().trim());
        hashMap.put("email", editTextVisitorProfileEmail.getText().toString().trim());
        hashMap.put("phone_no", editTextVisitorProfilePhoneNumber.getText().toString().trim());
        hashMap.put("building_no", editTextVisitorProfileBuildingNumber.getText().toString().trim());
        hashMap.put("street", editTextVisitorProfileStreet.getText().toString().trim());
        hashMap.put("zipcode", editTextVisitorProfileZipCode.getText().toString().trim());
        hashMap.put("city", city.getId());
        hashMap.put("country", country.getId());
        hashMap.put("country_code", editTextVisitorProfileCountryCode.getText().toString());
        hashMap.put("device_token", ((MainActivity) getActivity()).getDeviceId());
        hashMap.put("latitude", String.valueOf(lat));
        hashMap.put("longitude", String.valueOf(lng));

        hashMap.put("language", languageString);

        hashMap.put("bio", editTextVisitorProfileBio.getText().toString().trim());
        return hashMap;
    }


    public boolean isValid() {


        if (visitorProfileFirstname.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterFirstName));
            visitorProfileFirstname.requestFocus();
            return false;
        }

        if (editTextVisitorProfileLastName.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterLastName));
            editTextVisitorProfileLastName.requestFocus();
            return false;
        }

        if (editTextVisitorProfileUserName.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterUserName));
            editTextVisitorProfileUserName.requestFocus();
            return false;
        }

        if (!validEmail(editTextVisitorProfileEmail.getText().toString())) {
            editTextVisitorProfileEmail.requestFocus();
            return false;
        }


        if (editTextVisitorProfileStreet.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterStreet));
            editTextVisitorProfileStreet.requestFocus();
            return false;
        }

        if (editTextVisitorProfileZipCode.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterZipCode));
            editTextVisitorProfileZipCode.requestFocus();
            return false;
        }

        if (city == null) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterCity));
            editTextVisitorProfileCity.requestFocus();
            return false;
        }


        if (country == null) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterCountry));
            editTextVisitorProfileCountry.requestFocus();
            return false;
        }


        if (editTextVisitorProfileCountryCode.getText().toString().equals(getString(R.string.countryCode))) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterCountryCode));
            editTextVisitorProfileCountryCode.requestFocus();
            return false;
        }

        if (editTextVisitorProfilePhoneNumber.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterMobileNumber));
            editTextVisitorProfileCountryCode.requestFocus();
            return false;
        }

        if (editTextVisitorProfilePhoneNumber.getText().toString().trim().length() < 9) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterValidMobileNumber));
            editTextVisitorProfilePhoneNumber.requestFocus();
            return false;
        }

        return true;
    }


    public boolean validEmail(String email) {
        int length = email.trim().length();
        if (length <= 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterEmail));
            return false;
        }
        // Patterns.WEB_URL.matcher(potentialUrl).matches()
        Pattern pattern = Pattern.compile(Navigation.EMAIL_REGEX);
        Matcher matcher = pattern.matcher(email);
        if (!(email.charAt(0) + "").matches("\\p{L}") || !matcher.matches()) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterValidEmailId));
            return false;
        }
        return true;
    }


    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle(getString(R.string.title_editProfile), Enum.setNevigationIcon.BACK_SAVE, true);
    }

    @OnClick({R.id.editProfileVisitorImage, R.id.changePasswordVisitor, R.id.editProfileVisitorCoverImageChoose, R.id.visitorProfileTitle, R.id.editTextVisitorProfileCity, R.id.editTextVisitorProfileCountry, R.id.editTextVisitorProfileCountryCode})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.editProfileVisitorImage:
                visitorProfileImageChoose();
                break;
            case R.id.changePasswordVisitor:
                ((MainActivity) getActivity()).changePasswordDialog();
                break;
            case R.id.editProfileVisitorCoverImageChoose:
                visitorProfileTimelineImage();
                break;
            case R.id.visitorProfileTitle:
                visitorTitle();
                break;
            case R.id.editTextVisitorProfileCity:
                visitorEditProfile();
                break;
            case R.id.editTextVisitorProfileCountry:
                visitorEditProfileCountryList();
                break;
            case R.id.editTextVisitorProfileCountryCode:
                visitorCountryCodeList();
                break;
        }
    }

    private void visitorCountryCodeList() {
        ((MainActivity) getActivity()).hideKeyboard();
        int[] locationContryCode = new int[2];
        editTextVisitorProfileCountryCode.getLocationOnScreen(locationContryCode);

        countryCodeDropDown = new CountryCodeDropDown(getContext(), editTextVisitorProfileCountryCode.getHeight(), locationContryCode, editVisitorCountry, new ItemEventListener<Country>() {
            @Override
            public void onItemEventFired(Country integer, Country t2, String actualPos) {
                editTextVisitorProfileCountryCode.setText(integer.getDialCode());
//                        branchId = integer + 1;
            }
        });
        countryCodeDropDown.show();
    }

    private void visitorEditProfileCountryList() {
        CountryDialog.openCountryCodeDialog(getContext(), new ItemEventListener<String>() {
            @Override
            public void onItemEventFired(String s, String t2, String actualPos) {
                Debugger.e(s + " " + t2);
                country = new Gson().fromJson(t2, Country.class);
                editTextVisitorProfileCountry.setText(country.getCountry());
            }
        });
    }

    private void visitorEditProfile() {
        ((MainActivity) getActivity()).hideKeyboard();
        int[] location = new int[2];
        editTextVisitorProfileCity.getLocationOnScreen(location);

        cityDropDown = new CityDropDownSeller(getContext(), editTextVisitorProfileCity.getHeight(), location, cityList, new ItemEventListener<String>() {
            @Override
            public void onItemEventFired(String integer, String t2, String actualPos) {
                city = new Gson().fromJson(t2, City.class);
                if (Locale.getDefault().getDisplayLanguage().equals("العربية"))
                    editTextVisitorProfileCity.setText(city.getArName());
                else
                    editTextVisitorProfileCity.setText(city.getName());
            }
        });
        cityDropDown.show();
    }

    private void visitorTitle() {
        ((MainActivity) getActivity()).hideKeyboard();
        int[] location = new int[2];
        visitorProfileTitle.getLocationOnScreen(location);
        titleList = new InitializeMenu().toGetAdvertiseMent(getContext());
        advertisementDropDown = new AdvertisementDropDown(getContext(), visitorProfileTitle.getHeight(), location, titleList, new ItemEventListener<Integer>() {
            @Override
            public void onItemEventFired(Integer integer, Integer t2, String actualPos) {
                visitorProfileTitle.setText(titleList.get(integer));
//                        branchId = integer + 1;
            }
        });
        advertisementDropDown.show();
    }

    private void visitorProfileImageChoose() {
        ImageChooserDialog imageChooserDialog = new ImageChooserDialog();
        imageChooserDialog.show(getActivity().getSupportFragmentManager(), "imageChooserDialog");
        imageChooserDialog.setCallback(new ImageChooserListener() {
            @Override
            public void getResultFromCamera(String result) {
                visitorEditImage = new File(result);
                ((MainActivity) getActivity()).setCircularImage(visitorEditImage, editProfileVisitorImage);
            }

            @Override
            public void getResultFromGallery(String result) {
                if (result != null) {
                    if (!(result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("jpeg") ||
                            result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("jpg") ||
                            result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("png") ||
                            result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("bmp"))) {
                        //isInvalidImage = true;
                        ((MainActivity) getActivity()).messageAlert(getString(R.string.message_thisImageIsNotSupported));

                    } else {
                        visitorEditImage = new File(result);
                        ((MainActivity) getActivity()).setCircularImage(visitorEditImage, editProfileVisitorImage);
                        //layoutAddItemImageViewAdd.setBackgroundColor(getResources().getColor(R.color.backColor));
                        //isInvalidImage = false;
                    }
                }
            }

            @Override
            public void nothingSet() {

            }
        });
    }

    private void visitorProfileTimelineImage() {
        ImageChooserDialog imageChooserDialog = new ImageChooserDialog();
        imageChooserDialog.show(getActivity().getSupportFragmentManager(), "imageChooserDialog");
        imageChooserDialog.setCallback(new ImageChooserListener() {
                                           @Override
                                           public void getResultFromCamera(String result) {
                                               timeLineImage = new File(result);
                                               new Handler().postDelayed(new Runnable() {
                                                   @Override
                                                   public void run() {
                                                       Picasso.with(getActivity()).load(timeLineImage).transform(new ColorFilterTransformation(getResources().getColor(R.color.transparent_blue))).into(editProfileVisitorCoverImage);
                                                   }
                                               }, 50);

                                           }

                                           @Override
                                           public void getResultFromGallery(String result) {
                                               if (result != null) {
                                                   if (!(result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("jpeg") ||
                                                           result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("jpg") ||
                                                           result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("png") ||
                                                           result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("bmp"))) {
                                                       //isInvalidImage = true;
                                                       ((MainActivity) getActivity()).messageAlert(getString(R.string.message_thisImageIsNotSupported));

                                                   } else {
                                                       timeLineImage = new File(result);
                                                       new Handler().postDelayed(new Runnable() {
                                                           @Override
                                                           public void run() {
                                                               Picasso.with(getActivity()).load(timeLineImage).transform(new ColorFilterTransformation(getResources().getColor(R.color.transparent_blue))).into(editProfileVisitorCoverImage);
                                                           }
                                                       }, 50);
                                                   }
                                               }
                                           }

                                           @Override
                                           public void nothingSet() {

                                           }
                                       }

        );
    }

}
