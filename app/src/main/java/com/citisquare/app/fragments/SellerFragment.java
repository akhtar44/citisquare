package com.citisquare.app.fragments;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.PlaceAutocompleteAdapter;
import com.citisquare.app.controls.CustomAutoComplete;
import com.citisquare.app.controls.CustomButton;
import com.citisquare.app.controls.CustomEditText;
import com.citisquare.app.controls.CustomRadioButton;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.dialog.CategorySelectionDialog;
import com.citisquare.app.dialog.CountryDialog;
import com.citisquare.app.dialog.ImageChooserDialog;
import com.citisquare.app.dialog.SubCategorySelectionDialog;
import com.citisquare.app.interfaces.CategoryListener;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ImageChooserListener;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.interfaces.Navigation;
import com.citisquare.app.interfaces.SubCategoryListener;
import com.citisquare.app.pojo.addresslocation.Address;
import com.citisquare.app.pojo.response.CategoryData;
import com.citisquare.app.pojo.response.City;
import com.citisquare.app.pojo.response.Country;
import com.citisquare.app.pojo.response.LoginWrapper;
import com.citisquare.app.pojo.response.SubCategory;
import com.citisquare.app.utils.BranchDropDown;
import com.citisquare.app.utils.Debugger;
import com.citisquare.app.utils.InitializeMenu;
import com.citisquare.app.utils.RequestParameter;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import serviceCalling.builder.KeyValuePair;
import serviceCalling.builder.ServiceParameter;
import serviceCalling.helper.GetRequestHelper;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by hlink56 on 1/6/16.
 */
public class SellerFragment extends BaseFragment {


    File uploadAttachment;
    File image;
    ArrayList<String> pathArrayAttachMent, pathArrayImage;
    ArrayList<CategoryData> categoryDatas;
    CityDropDownSeller cityDropDown;

    Country country;
    Double lat, lng;
    City city;
    Map<String, String> map2;
    String categoryId;
    String subCategoryId;/*
    @BindView(R.id.editTextBusinessSelectBranch)
    CustomTextView editTextBusinessSelectBranch;*/
    BranchDropDown branchDropDown;
    ArrayList<String> branchList;
    MainActivity mainactivity = new MainActivity();
    int sellerVersion = 0;


    @BindView(R.id.textViewSubCategory)
    TextView textViewSubCategory;
    @BindView(R.id.imageViewBusinessImage)
    ImageView imageViewBusinessImage;
    @BindView(R.id.sellerTitle)
    CustomTextView sellerTitle;
    @BindView(R.id.sellerFirstName)
    CustomEditText sellerFirstName;
    @BindView(R.id.sellerLastName)
    CustomEditText sellerLastName;
    @BindView(R.id.editTextBusinessUserName)
    CustomEditText editTextBusinessUserName;
    @BindView(R.id.editTextBusinessEmail)
    CustomEditText editTextBusinessEmail;
    @BindView(R.id.editTextBusinessPassword)
    CustomEditText editTextBusinessPassword;
    @BindView(R.id.editTextBusinessConfirmPassword)
    CustomEditText editTextBusinessConfirmPassword;
    @BindView(R.id.editTextBusinessBuildingNumber)
    CustomEditText editTextBusinessBuildingNumber;
    @BindView(R.id.editTextBusinessStreet)
    CustomAutoComplete editTextBusinessStreet;
    @BindView(R.id.editTextBusinessZipCode)
    CustomAutoComplete editTextBusinessZipCode;
    @BindView(R.id.editTextBusinessCity)
    CustomTextView editTextBusinessCity;
    @BindView(R.id.editTextBusinessCountry)
    CustomTextView editTextBusinessCountry;
    @BindView(R.id.editTextBusinessCountryCode)
    CustomTextView editTextBusinessCountryCode;
    @BindView(R.id.editTextBussinessPhoneNum)
    CustomEditText editTextBussinessPhoneNum;
    @BindView(R.id.editTextBusinessCountryCodes)
    CustomTextView editTextBusinessCountryCodes;
    @BindView(R.id.editTextBussinessPhoneNums)
    CustomEditText editTextBussinessPhoneNums;
    @BindView(R.id.editTextSellerProfileCompanyName)
    CustomEditText editTextBusinessCompanyName;
    @BindView(R.id.editTextSellerProfileCompanyNameInArabic)
    CustomEditText editTextSellerProfileCompanyNameInArabic;
    @BindView(R.id.editTextBussinessWebsite)
    CustomEditText editTextBussinessWebsite;
    @BindView(R.id.editTextSalesid)
    CustomEditText editTextSalesid;
    @BindView(R.id.textViewCategory)
    CustomTextView textViewCategory;
    @BindView(R.id.editTextBussinessId)
    CustomTextView editTextBussinessId;
    @BindView(R.id.businessButtonSubmit)
    CustomButton businessButtonSubmit;


    CountryCodeDropDown countryCodeDropDown;
    ArrayList<Country> countries;
    AdvertisementDropDown advertisementDropDown;
    ArrayList<String> titleList;
    @BindView(R.id.editTextSellerProfileCompanyAlternateEmail)
    CustomEditText editTextSellerProfileCompanyAlternateEmail;
    @BindView(R.id.checkButton)
    ImageView checkButton;
    @BindView(R.id.textView_termsAndCondition)
    CustomTextView textViewTermsAndCondition;
    @BindView(R.id.editTextBusinessHour)
    CustomEditText editTextBusinessHour;
    @BindView(R.id.englishRadioButtonSellerSignup)
    CustomRadioButton englishRadioButtonSellerSignup;
    @BindView(R.id.arabicRadioButtonSellerSignup)
    CustomRadioButton arabicRadioButtonSellerSignup;
    @BindView(R.id.preferredLanguageRadioSellerSignup)
    RadioGroup preferredLanguageRadioSellerSignup;
    String languageString = String.valueOf(Constants.forLanguage.ARABIC);
    @BindView(R.id.textViewBussinesslocation)
    CustomTextView textViewBussinesslocation;
    @BindView(R.id.locationImageViewSellerSignup)
    ImageView locationImageViewSellerSignup;
    private int branchId = 0;
    private Pattern pattern;
    private Pattern passwordPattern;
    private Matcher matcher;
    private Matcher password;
    private PlaceAutocompleteAdapter placeAdapter;
    private LatLng locationSellerSignup;
    private Integer title = -1;
    public static int SELECT_PLACE_PICKER = 1;
    private static String TAG = "SellerFragment";
    /*List<CountryData> countryDatas;*/

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.signup_business, container, false);
        //getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        ButterKnife.bind(this, view);

        countries = ((MainActivity) getActivity()).countryDatas();
        if (countries != null) {
            for (Country countryLoop : countries) {
                if (countryLoop.getDialCode().equals("+966")) {
                    country = countryLoop;
                    editTextBusinessCountry.setText(country.getCountry());
                    editTextBusinessCountryCode.setText(country.getDialCode());
                    editTextBusinessCountryCodes.setText(country.getDialCode());
                }
            }
        }

        map2 = new HashMap<String, String>();
        pathArrayAttachMent = new ArrayList<>();
        pathArrayImage = new ArrayList<>();
        titleList = new InitializeMenu().toGetAdvertiseMent(getContext());
        //editTextBusinessSelectBranch.setText(getString(R.string.selectBranch));


        if (Locale.getDefault().getDisplayLanguage().equals("العربية")) {
            editTextBusinessPassword.setGravity(Gravity.RIGHT);
            editTextBusinessConfirmPassword.setGravity(Gravity.RIGHT);
        } else {
            editTextBusinessPassword.setGravity(Gravity.LEFT);
            editTextBusinessConfirmPassword.setGravity(Gravity.LEFT);
        }

        preferredLanguageRadioSellerSignup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int radio = preferredLanguageRadioSellerSignup.getCheckedRadioButtonId();
                switch (radio) {
                    case R.id.englishRadioButtonSellerSignup:
                        languageString = String.valueOf(Constants.forLanguage.ENGLISH);
                        break;
                    case R.id.arabicRadioButtonSellerSignup:
                        languageString = String.valueOf(Constants.forLanguage.ARABIC);
                        break;
                }
            }
        });

        pattern = Pattern.compile(Constants.englishOnly);
        passwordPattern = Pattern.compile(Constants.passwordRegex);
        //API_KEY = getString(R.string.google_maps_server_key);
        /*countryDatas = new InitializeMenu().toGetCountry(getContext());*/

        branchList = new InitializeMenu().toGetBranchList();
        placeAdapter = new PlaceAutocompleteAdapter(getContext(), R.layout.raw_autocomplete_text);
        editTextBusinessStreet.setAdapter(placeAdapter);

        editTextBusinessStreet.setThreshold(1);

        return view;
    }

    @OnClick(R.id.businessButtonSubmit)
    public void businessSubmitButtonClicked() {
        if (isValid()) {
            sellerSignup();
        }
    }

    public void getLocation() {
        new GetRequestHelper<Address>().pingToRequest("http://maps.googleapis.com/maps/api/geocode/json?address=" +
                        editTextBusinessStreet.getText().toString().replace("\"", "") + "&sensor=true"
                , new ArrayList<KeyValuePair>()
                , new ServiceParameter()
                , new Address()
                , new TaskCompleteListener<Address>() {
                    @Override
                    public void onSuccess(Address mObject) {
                        try {
                            Debugger.e("GET LOCATION:::" + mObject);
                            if (mObject.getStatus().equalsIgnoreCase("ok")) {
                                lat = mObject.getResults().get(0).getGeometry().getLocation().getLat();
                                lng = mObject.getResults().get(0).getGeometry().getLocation().getLng();
                                //sellerSignup(lat, lng);
                            } else {
                                ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterValidLocation));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, Address address) {

                    }
                });
    }

    /*
first_name,username,email,password,building_no,street,city,country,country_code,phone_no,

                    device_type,device_token

cell_country_code,cell_phoneno,business_licence_id,profile_image*/

    public void sellerSignup() {
        if (getActivity() != null)
            ((MainActivity) getActivity()).startLoader();

        List<KeyValuePair> keyValuePair = new ArrayList<>();
        if (image != null)
            keyValuePair.add(new KeyValuePair("profile_image", image.getAbsolutePath()));

        if (uploadAttachment != null)
            keyValuePair.add(new KeyValuePair("business_licence_id", uploadAttachment.getAbsolutePath()));


        new PostRequestHelper<LoginWrapper>().pingToRequest(Constants.SELLER_SIGNUP
                , "image/*"
                , keyValuePair
                , new RequestParameter().getHashMapForSellerSignup(getHashMapForSellerSignup())
                , ((MainActivity) getActivity()).getHeader()
                , new LoginWrapper()
                , new TaskCompleteListener<LoginWrapper>() {
                    @Override
                    public void onSuccess(LoginWrapper login) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        if (login.getStatus() == 1) {
                            /*Toast.makeText(getActivity(), R.string.sign_up_successful, Toast.LENGTH_LONG).show();
                            //((MainActivity) getActivity()).messageAlert(login.getMessage());
                            ((MainActivity) getActivity()).onBackPressed();*/
                            ((MainActivity) getActivity()).replaceParentFragment(new LoginFragment(), "parent");
                            Toast.makeText(getActivity(), R.string.sign_up_successful, Toast.LENGTH_LONG).show();
                        } else
                            ((MainActivity) getActivity()).messageAlert(login.getMessage());
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, LoginWrapper signupSellerWrapper) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();
                            if (signupSellerWrapper != null) {
                                ((MainActivity) getActivity()).messageAlert(signupSellerWrapper.getMessage());
                            }
                            if (exceptions == ExceptionType.ParameterException && signupSellerWrapper != null) {
                                ((MainActivity) getActivity()).messageAlert(signupSellerWrapper.getMessage());
                            }

                        }
                        ((MainActivity) getActivity()).replaceParentFragment(new LoginFragment(), "parent");
                        Toast.makeText(getActivity(), R.string.sign_up_successful, Toast.LENGTH_LONG).show();
                    }
                });

    }

    @NonNull
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static String getStringByLocal(Context context, int id, String locale) {
        Configuration configuration = new Configuration(context.getResources().getConfiguration());
        configuration.setLocale(new Locale(locale));
        return context.createConfigurationContext(configuration).getResources().getString(id);
    }

    public HashMap<String, String> getHashMapForSellerSignup() {

        HashMap<String, String> hashMapSellerSignup = new HashMap<>();
        if (sellerTitle.getText().toString().equalsIgnoreCase(getString(R.string.title))) {
            hashMapSellerSignup.put("title", "");
        } else {
            hashMapSellerSignup.put("title", sellerTitle.getText().toString().trim());
        }
        hashMapSellerSignup.put("first_name", sellerFirstName.getText().toString().trim());
        hashMapSellerSignup.put("last_name", sellerLastName.getText().toString().trim());
        hashMapSellerSignup.put("username", editTextBusinessUserName.getText().toString().trim());
        hashMapSellerSignup.put("email", editTextBusinessEmail.getText().toString().trim());
        hashMapSellerSignup.put("password", editTextBusinessPassword.getText().toString());
        hashMapSellerSignup.put("device_token", ((MainActivity) getActivity()).getDeviceId());
        hashMapSellerSignup.put("building_no", editTextBusinessBuildingNumber.getText().toString().trim());
        hashMapSellerSignup.put("street", editTextBusinessStreet.getText().toString());
        hashMapSellerSignup.put("zipcode", editTextBusinessZipCode.getText().toString().trim());
        hashMapSellerSignup.put("salesman_id", editTextSalesid.getText().toString().trim());
        hashMapSellerSignup.put("city", city.getId());
        hashMapSellerSignup.put("country", country.getId());
        hashMapSellerSignup.put("company_name_ar", editTextSellerProfileCompanyNameInArabic.getText().toString().trim());
        hashMapSellerSignup.put("cell_country_code", country.getDialCode());
        hashMapSellerSignup.put("cell_phoneno", editTextBussinessPhoneNum.getText().toString());
        hashMapSellerSignup.put("website", editTextBussinessWebsite.getText().toString().toLowerCase());
        if (locationSellerSignup != null) {
            hashMapSellerSignup.put("latitude", locationSellerSignup.latitude + "");
            hashMapSellerSignup.put("longitude", locationSellerSignup.longitude + "");
        } else {
            hashMapSellerSignup.put("latitude", 0 + "");
            hashMapSellerSignup.put("longitude", 0 + "");
        }

        hashMapSellerSignup.put("category_id", categoryId);
        hashMapSellerSignup.put("sub_cat_id", subCategoryId);
        hashMapSellerSignup.put("company_name", editTextBusinessCompanyName.getText().toString());
        hashMapSellerSignup.put("alternate_email", editTextSellerProfileCompanyAlternateEmail.getText().toString());
        hashMapSellerSignup.put("business_hours", editTextBusinessHour.getText().toString().trim());


        hashMapSellerSignup.put("language", languageString);

        if (editTextBussinessPhoneNums.getText().toString().length() > 0) {
            hashMapSellerSignup.put("country_code", country.getDialCode());
            hashMapSellerSignup.put("phone_no", editTextBussinessPhoneNums.getText().toString());
        }
        return hashMapSellerSignup;
    }

    @OnClick(R.id.editTextBusinessCountryCode)
    public void editTextBusinessCountryClicked() {
        CountryDialog.openCountryCodeDialog(getContext(), new ItemEventListener<String>() {
            @Override
            public void onItemEventFired(String s, String t2, String actualPos) {
                Debugger.e(s + " " + t2);
                country = new Gson().fromJson(t2, Country.class);
                Debugger.e("COUNTRY :::" + country.toString());
                editTextBusinessCountry.setText(country.getCountry());
                editTextBusinessCountryCode.setText(country.getDialCode());
                editTextBusinessCountryCodes.setText(country.getDialCode());
            }
        });
    }

    @OnClick(R.id.editTextBusinessCity)
    public void editTextBusinessCityClicked() {
        ((MainActivity) getActivity()).hideKeyboard();
        int[] location = new int[2];
        editTextBusinessCity.getLocationOnScreen(location);
        final ArrayList<City> cityList = ((MainActivity) getActivity()).cityArrayList();
        cityDropDown = new CityDropDownSeller(getContext(), editTextBusinessCity.getHeight(), location, cityList, new ItemEventListener<String>() {
            @Override
            public void onItemEventFired(String integer, String t2, String actualPos) {
                city = new Gson().fromJson(t2, City.class);
                editTextBusinessCity.setText(city.getName());

            }
        });
        cityDropDown.show();
    }


    @OnClick({R.id.sellerTitle})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sellerTitle:
                ((MainActivity) getActivity()).hideKeyboard();
                int[] location = new int[2];
                sellerTitle.getLocationOnScreen(location);
                advertisementDropDown = new AdvertisementDropDown(getContext(), sellerTitle.getHeight(), location, titleList, new ItemEventListener<Integer>() {
                    @Override
                    public void onItemEventFired(Integer integer, Integer t2, String actualPos) {
                        sellerTitle.setText(titleList.get(integer));
                        title = t2;
//                        branchId = integer + 1;
                    }
                });
                advertisementDropDown.show();
                break;
        }
    }


    @OnClick(R.id.checkButton)
    public void checkButtonClicked() {
        if (checkButton.getDrawable().getConstantState().equals(getResources().getDrawable(R.drawable.uncheck).getConstantState()))
            checkButton.setImageDrawable(getResources().getDrawable(R.drawable.check));
        else
            checkButton.setImageDrawable(getResources().getDrawable(R.drawable.uncheck));
    }


    @OnClick(R.id.textView_termsAndCondition)
    public void termsAndConditionClicked() {
        WebViewFragment webViewFragment = new WebViewFragment();
        Bundle bundle = new Bundle();
        bundle.putString("from", "1");
        webViewFragment.setArguments(bundle);
        ((MainActivity) getActivity()).replaceParentFragment(webViewFragment, "");
    }

    public String removeLastCharacter(String str) {
        if (str != null && str.length() > 0 && str.charAt(str.length() - 1) == ',') {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }

    @OnClick(R.id.textViewSubCategory)
    public void subCategorySelection() {
        if (textViewCategory.getText().toString().equals(getString(R.string.select_categor))) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseSelectFirstCategory));
        } else {
            SubCategorySelectionDialog.openCategorySelectionDialog(getContext(), categoryId, new SubCategoryListener() {
                @Override
                public void onSubCategoryListener(List<SubCategory> subCategories) {
                    String s = "";
                    subCategoryId = "";
                    for (SubCategory categoryData : subCategories) {
                        if (categoryData.isSelected()) {
                            if (Locale.getDefault().getDisplayLanguage().equals("العربية")) {
                                s += categoryData.getArName() + ",";
                            } else {
                                s += categoryData.getName() + ",";
                            }
                            subCategoryId += categoryData.getId() + ",";
                        }
                    }
                    if (subCategoryId.equalsIgnoreCase("")) {
                        textViewSubCategory.setText(getString(R.string.select_sub_categor));
                    } else {
                        textViewSubCategory.setText(removeLastCharacter(s));
                    }
                }
            });
        }
    }

    @OnClick(R.id.textViewCategory)
    public void categorySelection() {
        categoryId = "";
        subCategoryId = "";
        CategorySelectionDialog.openCategorySelectionDialog(getContext(), 1, "", new CategoryListener() {
            @Override
            public void onCategorySelected(List<CategoryData> categoryDatas) {
                String s = "";
                textViewSubCategory.setText(getString(R.string.select_sub_categor));

                for (CategoryData categoryData : categoryDatas) {
                    if (categoryData.isSelected()) {
                        if (Locale.getDefault().getDisplayLanguage().equals("العربية")) {
                            s += categoryData.getArName() + ",";
                        } else {
                            s += categoryData.getName() + ",";
                        }
                        categoryId += categoryData.getId() + ",";
                    }
                }
                if (categoryId.equalsIgnoreCase("")) {
                    textViewCategory.setText(getString(R.string.select_categor));
                } else {
                    textViewCategory.setText(removeLastCharacter(s));
                }
            }
        });
    }

    public void setText(String text) {
        textViewCategory.setText(text);
    }


    @OnClick(R.id.imageViewBusinessImage)
    public void businessImageClick() {
        ImageChooserDialog imageChooserDialog = new ImageChooserDialog();
        imageChooserDialog.show(getActivity().getSupportFragmentManager(), "imageChooserDialog");
        imageChooserDialog.setCallback(new ImageChooserListener() {
            @Override
            public void getResultFromCamera(String result) {
                image = new File(result);
                ((MainActivity) getActivity()).setCircularImage(image, imageViewBusinessImage);
            }

            @Override
            public void getResultFromGallery(String result) {
                if (result != null) {
                    if (!(result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("jpeg") ||
                            result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("jpg") ||
                            result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("png") ||
                            result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("bmp"))) {
                        //isInvalidImage = true;
                        ((MainActivity) getActivity()).messageAlert(getString(R.string.message_thisImageIsNotSupported));

                    } else {
                        image = new File(result);
                        ((MainActivity) getActivity()).setCircularImage(image, imageViewBusinessImage);
                        //layoutAddItemImageViewAdd.setBackgroundColor(getResources().getColor(R.color.backColor));
                        //isInvalidImage = false;
                    }
                }
            }

            @Override
            public void nothingSet() {

            }
        });
    }

    public boolean isValid() {


        if (editTextBusinessCompanyName.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterCompanyName));
            editTextBusinessCompanyName.requestFocus();
            return false;
        }


       /* if (!validWebsite(editTextBussinessWebsite.getText().toString())) {
            editTextBussinessWebsite.requestFocus();
            return false;
        }


        if (sellerTitle.getText().toString().equals(getString(R.string.title))) {
            sellerTitle.setText(getText(R.string.title));
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterTitle));
            sellerTitle.requestFocus();
            return false;
        }*/


       /* if (sellerFirstName.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterFirstName));
            sellerFirstName.requestFocus();
            return false;
        }*/

        /*if (sellerLastName.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterLastName));
            sellerLastName.requestFocus();
            return false;
        }*/
       /* if (editTextBusinessUserName.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterUserName));
            editTextBusinessUserName.requestFocus();
            return false;
        }*/
        if (locationImageViewSellerSignup.getVisibility() != View.VISIBLE) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseSelectLocation));
            return false;
        }
        if (editTextBussinessPhoneNum.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterMobileNumber));
            editTextBussinessPhoneNum.requestFocus();
            return false;
        }


        if (editTextBussinessPhoneNum.getText().toString().trim().length() < 9) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterValidMobileNumber));
            editTextBussinessPhoneNum.requestFocus();
            return false;
        } else if (editTextBussinessPhoneNums.getText().toString().trim().length() > 0) {


            if (editTextBussinessPhoneNums.getText().toString().length() < 9) {
                ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterValidPhoneNumber));
                editTextBussinessPhoneNums.requestFocus();
                return false;
            }
        }
        if (city == null) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterCity));
            editTextBusinessCity.requestFocus();
            return false;
        }

        if (textViewCategory.getText().toString().equals(getString(R.string.select_categor))) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseSelectAtLeastOneCategory));
            return false;
        }

        if (textViewSubCategory.getText().toString().equals(getString(R.string.select_sub_categor))) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseSelectAtLeastOneSubCategory));
            return false;
        }
        /*if (!validEmail(editTextBusinessEmail.getText().toString())) {
            editTextBusinessEmail.requestFocus();
            return false;
        }


        if (editTextBusinessPassword.getText().toString().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterPassword));
            editTextBusinessPassword.requestFocus();
            return false;
        } else {
            matcher = pattern.matcher(editTextBusinessPassword.getText().toString());
            password = passwordPattern.matcher(editTextBusinessPassword.getText().toString());
            if (matcher.find()) {
                editTextBusinessPassword.setText("");
                ((MainActivity) getActivity()).messageAlert(getString(R.string.message_englishValidation));
                editTextBusinessPassword.requestFocus();
                return false;
            } else if (!password.matches()) {
                editTextBusinessPassword.setText("");
                ((MainActivity) getActivity()).messageAlert(getString(R.string.messagePasswordMustContains));
                editTextBusinessPassword.requestFocus();
                return false;
            }
        }

        if (editTextBusinessPassword.getText().toString().length() < 3) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterMinimumThreeCharacterForPassword));
            editTextBusinessPassword.requestFocus();
            return false;
        }

        if (editTextBusinessConfirmPassword.getText().toString().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterConfirmPassword));
            editTextBusinessConfirmPassword.requestFocus();
            return false;
        }

        if (editTextBusinessPassword.getText().toString().compareTo(editTextBusinessConfirmPassword.getText().toString()) != 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_passwordmismatch));
            editTextBusinessPassword.requestFocus();
            return false;
        }

        if (!alternateEmailValid(editTextSellerProfileCompanyAlternateEmail.getText().toString())) {
            editTextSellerProfileCompanyAlternateEmail.requestFocus();
            return false;
        }

        if (country == null) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseSelectCountryCode));
            //editTextBusinessCountryCode.requestFocus();
            return false;
        }

        *//**//*


        if (editTextBusinessStreet.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterStreet));
            editTextBusinessStreet.requestFocus();
            return false;
        }


        if (editTextBusinessZipCode.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterZipCode));
            editTextBusinessZipCode.requestFocus();
            return false;
        }

        *//*if (editTextBussinessId.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseUploadYourBusinessId));
            editTextBussinessId.requestFocus();
            return false;
        }*//*


        if (checkButton.getDrawable().getConstantState().equals(getResources().getDrawable(R.drawable.uncheck).getConstantState())) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseAccpetTermsAndConditions));
            return false;
        }*/

        return true;
    }

    private boolean alternateEmailValid(String email) {
        int length = email.trim().length();

        // Patterns.WEB_URL.matcher(potentialUrl).matches()
        Pattern pattern = Pattern.compile(Navigation.EMAIL_REGEX);
        Matcher matcher = pattern.matcher(email);
        if (length > 0) {
            if (!(email.charAt(0) + "").matches("\\p{L}") || !matcher.matches()) {
                ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterValidEmailId));
                return false;
            }
        }
        return true;
    }

    public boolean validEmail(String email) {
        int length = email.trim().length();
        if (length <= 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterEmail));
            return false;
        }
        // Patterns.WEB_URL.matcher(potentialUrl).matches()
        Pattern pattern = Pattern.compile(Navigation.EMAIL_REGEX);
        Matcher matcher = pattern.matcher(email);
        if (!(email.charAt(0) + "").matches("\\p{L}") || !matcher.matches()) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterValidEmailId));
            return false;
        }
        return true;
    }

    public boolean validWebsite(String webSite) {
        int length = webSite.trim().length();

        Pattern pattern = Pattern.compile(Navigation.WEBSITE_REGEX);
        Matcher matcher = pattern.matcher(webSite);
        if (length > 0) {
            if (!(webSite.charAt(0) + "").matches("\\p{L}") || !matcher.matches()) {
                ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterValidWEbsite));
                return false;
            }
        }
        return true;
    }

    @OnClick(R.id.editTextBussinessId)
    public void uploadBusinessId() {
        ImageChooserDialog imageChooserDialog = new ImageChooserDialog();
        imageChooserDialog.show(getActivity().getSupportFragmentManager(), "imageChooserDialog");
        imageChooserDialog.setCallback(new ImageChooserListener() {
            @Override
            public void getResultFromCamera(String result) {
                uploadAttachment = new File(result);
                editTextBussinessId.setText(uploadAttachment.getName());
                //((MainActivity) getActivity()).setCircularImage(image, imageViewBusinessImage);
            }

            @Override
            public void getResultFromGallery(String result) {
                if (result != null) {
                    if (!(result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("jpeg") ||
                            result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("jpg") ||
                            result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("png") ||
                            result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("bmp"))) {
                        //isInvalidImage = true;
                        ((MainActivity) getActivity()).messageAlert(getString(R.string.message_thisImageIsNotSupported));

                    } else {
                        uploadAttachment = new File(result);
                        editTextBussinessId.setText(uploadAttachment.getName());
                        //((MainActivity) getActivity()).setCircularImage(image, imageViewBusinessImage);
                        //layoutAddItemImageViewAdd.setBackgroundColor(getResources().getColor(R.color.backColor));
                        //isInvalidImage = false;
                    }
                }
            }

            @Override
            public void nothingSet() {

            }
        });
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).actionBarShow();
        ((MainActivity) getActivity()).setTitle(getString(R.string.title_signup), Enum.setNevigationIcon.BACK, true);
    }

    @OnClick(R.id.textViewBussinesslocation)
    public void onClickOfSellerBusinessLocation() {
        //locationFragmentOpen();
        ShowLocationMap();
    }

    @OnClick(R.id.locationImageViewSellerSignup)
    public void onClickMapOfSellerSignup() {
        //locationFragmentOpen();
        ShowLocationMap();
    }

    private void locationFragmentOpen() {
       /* MakeYourBusinessLocationFragment locationFragment = new MakeYourBusinessLocationFragment();
        ((MainActivity) getActivity()).addParentFragment(locationFragment, "");
        locationFragment.setOnLocationSelectionListener(new MakeYourBusinessLocationFragment.OnLocationSelectionListener() {
            @Override
            public void locationSelect(LatLng location) {
                if (getActivity() != null) {
                    getActivity().onBackPressed();
                    getGoogleMapThumbnail(location);
                }
            }
        });*/


    }

    private void ShowLocationMap() {
        try {
            boolean isLive = mainactivity.isInternetConnected(getActivity());
            LocationManager locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
            if (isLive) {
                if (IsGPSOn(getActivity())) {
                    try {
                        PlacePicker.IntentBuilder intentBuilder = new PlacePicker.IntentBuilder();
                        Intent intent = intentBuilder.build(getActivity());
                        startActivityForResult(intent, SELECT_PLACE_PICKER);
                    } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } else
                    AlertMessageNoGPS(getActivity());
            } else {
                Toast.makeText(getActivity(), getActivity().getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        Bitmap bitmap = null;
        String LatLong;
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PLACE_PICKER) {
                try {
                    final Place place = PlacePicker.getPlace(data, getActivity());
                    final CharSequence name = place.getName();
                    final CharSequence address = place.getAddress() + "/" + place.getLatLng();
                    if (address != null && address.length() > 0)
                        textViewBussinesslocation.setText(address);
                    LatLong = String.valueOf(place.getLatLng());
                    UploadLatLon(LatLong);
                    Log.i(TAG, "complete address" + place.getName() + "/" + place.getAddress() + "/" + place.getLatLng());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }
    }

    private void UploadLatLon(String latLong) {
        new GetRequestHelper<Address>().pingToRequest("http://maps.googleapis.com/maps/api/geocode/json?address=" +
                        latLong + "&sensor=true"
                , new ArrayList<KeyValuePair>()
                , new ServiceParameter()
                , new Address()
                , new TaskCompleteListener<Address>() {
                    @Override
                    public void onSuccess(Address mObject) {
                        try {
                            Debugger.e("GET LOCATION:::" + mObject);
                            if (mObject.getStatus().equalsIgnoreCase("ok")) {
                                /*lat = mObject.getResults().get(0).getGeometry().getLocationAndPayment().getLat();
                                lng = mObject.getResults().get(0).getGeometry().getLocationAndPayment().getLng();
                                sellerSignup(lat, lng);*/
                                if (getActivity() != null) {

                                               /* if (onLocationSelectionListener != null) {
                                                    if (getActivity() != null) {

                                                        DataToPref.setData(getContext(), "makeYourLocation", latlng.latitude + "," + latlng.longitude);
                                                        ((MainActivity) getActivity()).stopLoader();
                                                        onLocationSelectionListener.locationSelect(latlng);
                                                        //getActivity().onBackPressed();
                                                    }
                                                }*/
                                }
                            } else {
                                ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pelaseSelectValidLocation));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, Address address) {

                    }
                });
    }

    public void AlertMessageNoGPS(final Activity activity) {
        try {
            final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setMessage(activity.getString(R.string.gpsDisableEnableText))
                    .setCancelable(false)
                    .setPositiveButton(activity.getString(R.string.statusYesBtn),
                            new DialogInterface.OnClickListener() {
                                public void onClick(
                                        @SuppressWarnings("unused") final DialogInterface dialog,
                                        @SuppressWarnings("unused") final int id) {
                                    activity.startActivity(new Intent(
                                            Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                }
                            })
                    .setNegativeButton(activity.getString(R.string.statusNoBtn), new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog,
                                            @SuppressWarnings("unused") final int id) {
                            dialog.cancel();
                        }
                    });
            final AlertDialog alert = builder.create();
            alert.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public boolean IsGPSOn(Activity activity) {
        boolean isON = false;
        try {
            LocationManager locationManager = (LocationManager) activity.getSystemService(LOCATION_SERVICE);
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                isON = true;
            } else {
                isON = false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return isON;
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void getGoogleMapThumbnail(LatLng place) {
        final String STATIC_MAP_URL = "https://maps.googleapis.com/maps/api/staticmap?";
        locationSellerSignup = place;
        String location = place.latitude + "," + place.longitude;
        if (!location.isEmpty()) {
            try {
                final StringBuilder stringBuilder = new StringBuilder(STATIC_MAP_URL);
                stringBuilder.append("zoom=" + "9");
                stringBuilder.append("&size=" + "600x400");
                stringBuilder.append("&maptype=" + "roadmap");
                stringBuilder.append("&markers=" + location);
                //stringBuilder.append("&key=" + STATIC_MAP_API_KEY);

                Log.e("MainURL", "onCreate: " + stringBuilder.toString() + "autocomplete adddress:" + location);
                locationImageViewSellerSignup.setVisibility(View.VISIBLE);
                Glide.with(getContext()).load(stringBuilder.toString()).asBitmap().into(locationImageViewSellerSignup);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            Toast.makeText(getContext(), "cannot find location", Toast.LENGTH_SHORT).show();
        }
    }
}
