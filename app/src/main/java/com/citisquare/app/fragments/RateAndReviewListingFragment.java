package com.citisquare.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.RateAndReviewAdapter;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.interfaces.Rendering;
import com.citisquare.app.pojo.response.Like;
import com.citisquare.app.pojo.response.RateAndReviewWrapper;
import com.citisquare.app.pojo.response.RateReviewData;
import com.citisquare.app.pojo.response.RateUserDetail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import serviceCalling.builder.KeyValuePair;
import serviceCalling.helper.GetRequestHelper;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;


/**
 * Created by hlink56 on 9/3/17.
 */

public class RateAndReviewListingFragment extends BaseFragment {

    @BindView(R.id.rateAndReviewRecyclerView)
    RecyclerView rateAndReviewRecyclerView;

    RateAndReviewAdapter rateAndReviewAdapter;
    List<RateReviewData> rateReviewDatas;

    RateUserDetail userDetail;
    String rate = "";
    Bundle bundle;
    private int page = 1;
    private GridLayoutManager gridLayoutManager;
    private boolean isPagination = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.rate_review_list_frag, container, false);
        ButterKnife.bind(this, view);
        rateReviewDatas = new ArrayList<>();
        bundle = getArguments();
        if (bundle != null) {
            rate = bundle.getString("rate");
        }

        rateAndReviewAdapter = new RateAndReviewAdapter(getContext(), rateReviewDatas, new ItemEventListener<String>() {
            @Override
            public void onItemEventFired(String s, @Nullable final String t2, String actualPos) {
                switch (s) {
                    case "profile":
                        ProfileFragment profileFragment = new ProfileFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("profile", "other");
                        ((MainActivity) getActivity()).setProfileUserId(t2);
                        profileFragment.setArguments(bundle);
                        ((MainActivity) getActivity()).setSelectionPostion(0);
                        ((MainActivity) getActivity()).setFragment(profileFragment, false, "");
                        break;
                    case "moveTo":
                        RateAndReviewDetailFragment rateAndReviewDetailFragment = new RateAndReviewDetailFragment();
                        Bundle rateBundle = new Bundle();
                        rateBundle.putString("data", new Gson().toJson(rateReviewDatas.get(Integer.parseInt(t2))));
                        rateAndReviewDetailFragment.setArguments(rateBundle);
                        ((MainActivity) getActivity()).setFragment(rateAndReviewDetailFragment, true, "");
                        break;
                    case "delete":
                        ((MainActivity) getActivity()).alertTwoAction(getString(R.string.message_areYouSureYouWantToDelete), new Rendering() {
                            @Override
                            public void response(boolean isCofirm) {
                                if (isCofirm) {
                                    deleteRateAndReview(t2);
                                }
                            }
                        });
                        break;
                }
            }
        });

        gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        rateAndReviewRecyclerView.setLayoutManager(gridLayoutManager);
        rateAndReviewRecyclerView.setAdapter(rateAndReviewAdapter);

        rateAndReviewRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (isPagination) {
                    int visibleItemCount = recyclerView.getChildCount();
                    int totalItemCount = gridLayoutManager.getItemCount();
                    int firstVisibleItemPosition = gridLayoutManager.findFirstVisibleItemPosition();
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount) {
                        isPagination = false;
                        rateAndReviewApiCall(++page);
                    }
                }
            }
        });


        return view;
    }


    public void deletePreviewRateAndAddNew() {
        if (userDetail != null) {
            if (userDetail.getIsRating().equals("1")) {
                ((MainActivity) getActivity()).alertTwoAction(getString(R.string.write_rate_delete), new Rendering() {
                    @Override
                    public void response(boolean isCofirm) {
                        if (isCofirm)
                            deleteReview();
                    }
                });
            } else {
                ((MainActivity) getActivity()).setFragment(new WriteReviewFragment(), false, "");
            }
        }
    }

    private void deleteReview() {
        if (getActivity() != null) {
            ((MainActivity) getActivity()).startLoader();
        }

        List<KeyValuePair> keyValuePairs = new ArrayList<>();
        keyValuePairs.add(new KeyValuePair("user_id", ((MainActivity) getActivity()).getProfileUserId()));

        new GetRequestHelper<Like>().pingToRequest(Constants.DELETE_REVIEW,
                keyValuePairs,
                ((MainActivity) getActivity()).getParamWholeParameters(),
                new Like(),
                new TaskCompleteListener<Like>() {
                    @Override
                    public void onSuccess(Like like) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();
                        }

                        if (like.getStatus() == 1) {
                            ((MainActivity) getActivity()).setFragment(new WriteReviewFragment(), false, "");
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, Like like) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();
                            if (like != null) {
                                if (like.getStatus() == -1) {
                                    ((MainActivity) getActivity()).invalidToken();
                                }
                            }
                        }
                    }
                });
    }

    private void deleteRateAndReview(String pos) {
        RateReviewData reviewData = rateReviewDatas.get(Integer.parseInt(pos));
        if (reviewData != null) {
            callApiOfDeleteRateAndReview(reviewData.getId(), pos);
        }
    }

    private void callApiOfDeleteRateAndReview(String rateId, final String pos) {
        if (getActivity() != null) {
            ((MainActivity) getActivity()).startLoader();
        }

        List<KeyValuePair> keyValuePairs = new ArrayList<>();
        keyValuePairs.add(new KeyValuePair("user_id", ((MainActivity) getActivity()).getProfileUserId()));

        new GetRequestHelper<Like>().pingToRequest(Constants.DELETE_REVIEW,
                keyValuePairs,
                ((MainActivity) getActivity()).getParamWholeParameters(),
                new Like(),
                new TaskCompleteListener<Like>() {
                    @Override
                    public void onSuccess(Like like) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();


                            if (like.getStatus() == 1) {
                                rateReviewDatas.remove(Integer.parseInt(pos));
                                rateAndReviewAdapter.notifyDataSetChanged();
                            }
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, Like like) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();
                            if (like != null) {
                                if (like.getStatus() == -1) {
                                    ((MainActivity) getActivity()).invalidToken();
                                }
                            }
                        }


                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        page = 1;
        rateAndReviewApiCall(page);
    }

    private void rateAndReviewApiCall(int pageNo) {
        if (getActivity() != null && pageNo == 1)
            ((MainActivity) getActivity()).startLoader();

        new PostRequestHelper<RateAndReviewWrapper>().pingToRequest(Constants.RATE_LISTING
                , null
                , null
                , getHashMapRateReview(pageNo + "")
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new RateAndReviewWrapper()
                , new TaskCompleteListener<RateAndReviewWrapper>() {
                    @Override
                    public void onSuccess(RateAndReviewWrapper rateAndReviewWrapper) {
                        isPagination = true;
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();

                            if (page == 1) {
                                rateReviewDatas.clear();
                            }

                            if (rateAndReviewWrapper.getStatus() == 1) {
                                setData(rateAndReviewWrapper);
                            }
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, RateAndReviewWrapper rateAndReviewWrapper) {

                        if (getActivity() != null) {
                            isPagination = false;
                            ((MainActivity) getActivity()).stopLoader();
                            if (page == 1) {
                                ((MainActivity) getActivity()).messageAlert(rateAndReviewWrapper.getMessage());
                            }
                        }
                    }
                });
    }


    private void setData(RateAndReviewWrapper rateAndReviewWrapper) {
        if (getActivity() != null) {
            if (rateAndReviewWrapper.getData().size() > 0)
                notifyDataSet(rateAndReviewWrapper.getData());
            else {
                if (page == 1)
                    ((MainActivity) getActivity()).messageAlert(rateAndReviewWrapper.getMessage());
            }
        }
    }

    private void notifyDataSet(List<RateReviewData> reviewDatas) {
        rateReviewDatas.addAll(reviewDatas);
        rateAndReviewAdapter.notifyDataSetChanged();
    }

    private HashMap<String, String> getHashMapRateReview(String pageNo) {
        HashMap<String, String> stringStringHashMap = new HashMap<>();
        stringStringHashMap.put("page", pageNo);
        stringStringHashMap.put("rate", rate);
        stringStringHashMap.put("user_id", ((MainActivity) getActivity()).getProfileUserId());
        return stringStringHashMap;
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle(getString(R.string.reviews), Enum.setNevigationIcon.BACK, true);
    }
}