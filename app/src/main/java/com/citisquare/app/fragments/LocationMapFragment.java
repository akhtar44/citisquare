package com.citisquare.app.fragments;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.async.MultipleRouteFetchTask;
import com.citisquare.app.async.RouteParserTask;
import com.citisquare.app.controls.MapWrapperLayout;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.listener.OnInfoWindowElemTouchListener;
import com.citisquare.app.utils.CircleTransformation;
import com.citisquare.app.utils.Debugger;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 20/7/16.
 */
public class LocationMapFragment extends BaseFragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    final static int ZOOM_PADDING = 250;
    public OnInfoWindowElemTouchListener infoButtonListenerMail;
    @BindView(R.id.map)
    MapView mapView;
    Bundle bundleMap;
    String bundleString = "";
    @BindView(R.id.framelayoutPlaceholder)
    FrameLayout framelayoutPlaceholder;
    @BindView(R.id.mapWrapperLayout)
    MapWrapperLayout mapWrapperLayout;
    private GoogleMap googleMap;
    private Polyline polyline;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;
        try {
            MapsInitializer.initialize(getActivity());
            view = inflater.inflate(R.layout.map_layout, container, false);
            ButterKnife.bind(this, view);
            mapView.onCreate(savedInstanceState);

            if (mapView != null) {
                mapView.getMapAsync(this);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        //markerList = new ArrayList<>();
        ButterKnife.bind(this, view);
        bundleMap = getArguments();
        if (bundleMap != null) {
            if (bundleMap.getString("MAP") != null) {
                bundleString = bundleMap.getString("MAP");
            }
        }
        return view;
    }


    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle("default", Enum.setNevigationIcon.BACK_CITY, true);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        if (googleMap != null) {
            this.googleMap = googleMap;
            setUpMap();
            final LatLngBounds.Builder builder = new LatLngBounds.Builder();

            LatLng locationStart = new LatLng(25.2048, 55.2708);
            LatLng locationEnd = new LatLng(24.2992, 54.6973);
            builder.include(locationStart);
            builder.include(locationEnd);

            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    locationStart, 11));
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(locationStart)      // Sets the center of the map to location user
                    .zoom(11)// Sets the zoom
                    .build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            drawMarker(locationStart, "CURRENT", "", R.drawable.current_location);
            drawMarker(locationEnd, "", "", R.drawable.other_point);

            drawPath(locationStart, locationEnd);
            zoomMapInitial(locationStart, builder, ZOOM_PADDING, googleMap, true);
        }
    }

    protected void zoomMapInitial(LatLng finalPlace
            , LatLngBounds.Builder bc
            , int padding
            , GoogleMap map
            , boolean isMapPadding) {
        try {
            int width = getResources().getDisplayMetrics().widthPixels;
            int height = getResources().getDisplayMetrics().heightPixels;

            if (isMapPadding) {
                int layoutHeight = framelayoutPlaceholder.getHeight();
                googleMap.setPadding(0, layoutHeight, 0, layoutHeight);
            }

            bc.include(finalPlace);
            map.animateCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), width, height, padding));

            googleMap.setPadding(0, 0, 0, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void drawPath(LatLng start, LatLng end) {
        MultipleRouteFetchTask routeFetchTask = new MultipleRouteFetchTask(googleMap, new RouteParserTask.ParseCallback() {
            @Override
            public void onRouteComplete(Polyline polyline) {
                clearPolyline(polyline);
            }
        });
        routeFetchTask.setPolyLineColor(ContextCompat.getColor(getContext(), R.color.dark_blue));
        routeFetchTask.setCurrent(start);
        routeFetchTask.setDestination(end);
        routeFetchTask.execute();
    }


    public void clearPolyline(Polyline polyline) {
        if (this.polyline != null)
            this.polyline.remove();
        this.polyline = polyline;
    }

    public Marker drawMarker(LatLng place, String title, String content, int icon) {
        MarkerOptions markerOptions = new MarkerOptions()
                .position(place)
                .infoWindowAnchor(0.5f, -0.0f)
                .title(title)
                .snippet(content)
                .icon(BitmapDescriptorFactory.fromResource(icon));

        Marker mMarker = googleMap.addMarker(markerOptions);
        return mMarker;
    }


    public void setUpMap() {
        /*if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Debugger.e("setUpMap method: permission require................");
            return;
        }*/
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.setOnMarkerClickListener(this);
        googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                View view = LayoutInflater.from(getContext()).inflate(R.layout.map_tip_popup, null);
                ViewHolder holder = new ViewHolder(view);
                mapWrapperLayout.setMarkerWithInfoWindow(marker, view);
                if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) ==
                        Configuration.SCREENLAYOUT_SIZE_XLARGE) {
                    // on a x-large screen device ...
                    Debugger.e("extra large screen");
                    mapWrapperLayout.init(googleMap, getPixelsFromDp(getActivity(), 35 + 50));
                } else if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) ==
                        Configuration.SCREENLAYOUT_SIZE_LARGE) {
                    // on a large screen device ...
                    Debugger.e("large screen");
                    mapWrapperLayout.init(googleMap, getPixelsFromDp(getActivity(), 35 + 50));
                } else {
                    Debugger.e("normal screen");
                    mapWrapperLayout.init(googleMap, getPixelsFromDp(getActivity(), 35 + 20));
                }

                LocationMapFragment.this.infoButtonListenerMail = new OnInfoWindowElemTouchListener(holder.tipViewDetails, 0, 0) {
                    @Override
                    protected void onClickConfirmed(View v, Marker m) {
                        ProfileFragment profileFragment = new ProfileFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("profile", "other");
                        profileFragment.setArguments(bundle);
                        ((MainActivity) getActivity()).setSelectionPostion(0);
                        ((MainActivity) getActivity()).setFragment(profileFragment,false, "");
                    }
                };
                holder.tipViewDetails.setOnTouchListener(infoButtonListenerMail);

                return view;
            }

            @Override
            public View getInfoContents(Marker marker) {
                return null;
            }
        });
    }

    public int getPixelsFromDp(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }


    @Override
    public void onResume() {

        if (mapView != null)
            mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mapView != null)
            mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        if (mapView != null)
            mapView.onLowMemory();
        super.onLowMemory();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        //googleMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(marker.getPosition().latitude + (double) 90 / Math.pow(2, 11), marker.getPosition().longitude)));
        //new LatLng(arg0.getPosition().latitude + (double)90/Math.pow(2, zoom), arg0.getPosition().longitude), zoom
        if (marker.getTitle().equals("CURRENT")) {

        } else {
            marker.showInfoWindow();
        }
        return true;
    }

    public class ViewHolder {
        @BindView(R.id.jobImageView)
        ImageView jobImageView;
        @BindView(R.id.jobPost)
        TextView jobPost;
        @BindView(R.id.jobCompanyName)
        TextView jobCompanyName;
        @BindView(R.id.jobExperience)
        TextView jobExperience;
        @BindView(R.id.tipViewDetails)
        TextView tipViewDetails;

        public ViewHolder(View v) {
            ButterKnife.bind(this, v);
            ((MainActivity) getActivity()).picasso.load(R.drawable.zac_afron)
                    .transform(new CircleTransformation())
                    .into(jobImageView);
        }
    }

    /*public class InfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private Context context;

        public InfoWindowAdapter(Context context) {
            this.context = context;
        }

        @Override
        public View getInfoWindow(final Marker marker) {
            View view = LayoutInflater.from(context).inflate(R.layout.map_tip_popup, null);
            ViewHolder holder = new ViewHolder(view);


            holder.bind(marker);
            return view;
        }

        @Override
        public View getInfoContents(Marker marker) {
            if (marker != null &&
                    marker.isInfoWindowShown()) {
                //marker.hideInfoWindow();
                marker.showInfoWindow();
            }
            return null;
        }

        public class ViewHolder {
            @BindView(R.id.jobImageView)
            ImageView jobImageView;
            @BindView(R.id.jobPost)
            TextView jobPost;
            @BindView(R.id.jobCompanyName)
            TextView jobCompanyName;
            @BindView(R.id.jobExperience)
            TextView jobExperience;
            @BindView(R.id.tipViewDetails)
            TextView tipViewDetails;

            public ViewHolder(View v) {
                ButterKnife.bind(this, v);
                ((MainActivity) getActivity()).picasso.load(R.drawable.zac_afron)
                        .transform(new CircleTransformation())
                        .into(jobImageView);
            }

            public void bind(final Marker marker) {
            }
        }
    }*/
}