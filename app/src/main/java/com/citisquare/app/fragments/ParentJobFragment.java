package com.citisquare.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.JobsAdapter;
import com.citisquare.app.dialog.JobApplyDialog;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.pojo.response.JobListData;
import com.citisquare.app.pojo.response.JobListWrapper;
import com.citisquare.app.utils.Debugger;
import com.citisquare.app.utils.RequestParameter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink16 on 27/5/16.
 */
public class ParentJobFragment extends BaseFragment {

    @BindView(R.id.mainJobsRecycler)
    RecyclerView mainJobsRecycler;

    List<JobListData> jobList;
    JobsAdapter jobsAdapter;
    Bundle bundle;
    String subCatId = "";
    GridLayoutManager gridLayoutManager;
    boolean isPagination;
    int pageno = 1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.jobs_main, container, false);
        ButterKnife.bind(this, view);

        pageno = 1;
        jobList = new ArrayList<>();
        bundle = getArguments();
        if (bundle != null) {
            if (bundle.getString("data") != null) {
                Debugger.e("bundle data::" + bundle.getString("data"));
                subCatId = bundle.getString("data");
            }
        }

        BaseFragment baseFragment = (BaseFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.placeholder);
        if (baseFragment instanceof TabFragment) {
            ((TabFragment) baseFragment).setSearchVisibility(Enum.setSearch.SEARCH_FILTER);
        }

        jobsAdapter = new JobsAdapter(getActivity(), jobList, new ItemEventListener<String>() {
            @Override
            public void onItemEventFired(String s, String t2, String actualPos) {
                switch (t2) {
                    case "JOBAPPLY":
                        JobApplyDialog jobApplyDialog = new JobApplyDialog();
                        Bundle bundle = new Bundle();
                        bundle.putString("jobData", s);
                        jobApplyDialog.setArguments(bundle);
                        jobApplyDialog.show(getActivity().getSupportFragmentManager(), jobApplyDialog.getClass().getName());
                        break;
                    case "profile":
                        if (s.equals(((MainActivity) getActivity()).userId())) {
                            ProfileFragment profileFragment = new ProfileFragment();
                            Bundle bundleProfile = new Bundle();
                            bundleProfile.putString("profile", "own");
                            ((MainActivity) getActivity()).setProfileUserId(s);
                            profileFragment.setArguments(bundleProfile);
                            ((MainActivity) getActivity()).setSelectionPostion(0);
                            ((MainActivity) getActivity()).setFragment(profileFragment, false, "");
                            //childFragmentReplacement(profileFragment, "child");
                        } else {
                            ProfileFragment profileFragment = new ProfileFragment();
                            Bundle bundleProfile = new Bundle();
                            bundleProfile.putString("profile", "other");
                            ((MainActivity) getActivity()).setProfileUserId(s);
                            profileFragment.setArguments(bundleProfile);
                            ((MainActivity) getActivity()).setSelectionPostion(0);
                            ((MainActivity) getActivity()).setFragment(profileFragment, false, "");
                            // childFragmentReplacement(profileFragment, "child");
                        }

                       /* ProfileFragment profileFragment = new ProfileFragment();
                        Bundle bundleProfile = new Bundle();
                        bundleProfile.putString("profile", "other");
                        profileFragment.setArguments(bundleProfile);
                        ((MainActivity) getActivity()).setSelectionPostion(0);
                        //((MainActivity) getActivity()).setFragment(profileFragment);
                        childFragmentReplacement(profileFragment, "child");*/
                        break;
                    case "map":
                        ((MainActivity) getActivity()).setFragment(new OtherMapPathFragment(), false, "");
                        break;
                }
            }
        });

        gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        mainJobsRecycler.setLayoutManager(gridLayoutManager);
        mainJobsRecycler.setAdapter(jobsAdapter);

        mainJobsRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (isPagination) {
                    int visibleItemCount = recyclerView.getChildCount();
                    int totalItemCount = gridLayoutManager.getItemCount();
                    int firstVisibleItemPosition = gridLayoutManager.findFirstVisibleItemPosition();
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount) {
                        isPagination = false;
                        callJobApi(++pageno);
                    }
                }
            }
        });
        callJobApi(pageno);
        return view;
    }

    public void getChildCount() {
        Debugger.e(":::::IN THE DATA::::" + getChildFragmentManager().getBackStackEntryCount());
    }

    public void callJobApi(final int pageno) {
        if (getActivity() != null && pageno == 1)
            ((MainActivity) getActivity()).startLoader();

        new PostRequestHelper<JobListWrapper>().pingToRequest(Constants.JOB_LISTING
                , null
                , null
                , new RequestParameter().getJobList(getJobHashMap(pageno))
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new JobListWrapper()
                , new TaskCompleteListener<JobListWrapper>() {
                    @Override
                    public void onSuccess(JobListWrapper jobListWrapper) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        if (pageno == 1)
                            jobList.clear();

                        if (jobListWrapper.getStatus() == 1) {
                            isPagination = true;
                            setJobAdapter(jobListWrapper.getData());
                        } else if (jobListWrapper.getStatus() == 0) {
                            isPagination = false;
                            jobList.clear();
                            setJobAdapter(jobListWrapper.getData());
                            if (pageno == 1)
                                ((MainActivity) getActivity()).messageAlert(jobListWrapper.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, JobListWrapper jobListWrapper) {
                        isPagination = false;
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();

                            if (jobListWrapper != null) {
                                if (pageno == 1) {
                                    jobList.clear();

                                }
                                if (jobListWrapper.getStatus() == -1)
                                    ((MainActivity) getActivity()).invalidToken();


                                if (exceptions == ExceptionType.ParameterException) {
                                    isPagination = false;
                                    if (pageno == 1)
                                        ((MainActivity) getActivity()).messageAlert(jobListWrapper.getMessage());
                                }
                            }
                        }
                    }
                });
    }


    public void setJobAdapter(List<JobListData> jobAdapter) {
        jobList.addAll(jobAdapter);
        jobsAdapter.notifyDataSetChanged();
    }

    public void apiCall() {
        pageno = 1;
        callJobApi(pageno);
    }

    public HashMap<String, String> getJobHashMap(int pageno) {
        HashMap<String, String> hashMap = new HashMap<>();
        String search = "";
        String filter = "";
        BaseFragment baseFragment = (BaseFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.placeholder);
        if (baseFragment instanceof TabFragment) {
            search = ((TabFragment) baseFragment).getSearchData();

            if (((TabFragment) baseFragment).getFilterData().equals(getString(R.string.saudis_jobs_drop)))
                filter = "S";
            else if (((TabFragment) baseFragment).getFilterData().equals(getString(R.string.non_saudis_jobs_drop)))
                filter = "NS";
        }
        /*cat_id,city_id,page	subcat_id,job_type,search*/
        hashMap.put("job_type", filter);
        hashMap.put("search", search);
        hashMap.put("subcat_id", subCatId);
        hashMap.put("cat_id", 1 + "");
        hashMap.put("city_id", ((MainActivity) getActivity()).getTopCitySelectionId());
        hashMap.put("page", String.valueOf(pageno));
        return hashMap;
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle("default", Enum.setNevigationIcon.BACK_CITY, true);
    }
}
