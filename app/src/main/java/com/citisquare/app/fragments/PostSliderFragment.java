package com.citisquare.app.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.camera.utils.Utils;
import com.citisquare.app.dialog.PickerDialog;
import com.citisquare.app.dialog.VideoTrimDialog;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ImagePickerDialogListener;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.listener.TrimListener;
import com.citisquare.app.listener.UriRecordingListener;
import com.citisquare.app.pojo.response.CategoryData;
import com.citisquare.app.pojo.response.City;
import com.citisquare.app.pojo.response.LoginWrapper;
import com.citisquare.app.pojo.response.SubCategory;
import com.citisquare.app.utils.Debugger;
import com.citisquare.app.utils.FileUtils;
import com.citisquare.app.utils.RequestParameter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import serviceCalling.builder.KeyValuePair;
import serviceCalling.helper.PostVideoHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 4/6/16.
 */
public class PostSliderFragment extends BaseFragment {
    @BindView(R.id.imageViewTodayOffer)
    ImageView imageViewTodayOffer;
    @BindView(R.id.todayOfferSelectCity)
    TextView todayOfferSelectCity;
    @BindView(R.id.editTextDescriptionOfferTodayOffer)
    EditText editTextDescriptionOfferTodayOffer;
    @BindView(R.id.todayOfferButtonSubmit)
    Button buttonSubmit;
    @BindView(R.id.todayoffertopLayout)
    FrameLayout todayoffertopLayout;
    @BindView(R.id.todayOfferSelectCategory)
    TextView todayOfferSelectCategory;
    @BindView(R.id.todayOfferSelectSubCategory)
    TextView todayOfferSelectSubCategory;
    @BindView(R.id.uploadphoto)
    TextView uploadphoto;
    CityDropDown cityDropDown;
    ArrayList<City> cityList;
    CategoryTimeDropDown categoryDropDown;
    ArrayList<CategoryData> categoryDatas;
    CategoryData selectCatData;
    SubCategory selectSubCatData;
    City seletedCity;
    SubCategoryDropDown subCategoryDropDown;
    ArrayList<SubCategory> subCateoryData;
    File image = new File("");
    int seconds;
    String mediaType = "";
    String catId = "";
    private Uri videoUri;

    private String pathVideo;
    private Pattern pattern;
    private Matcher matcher;
    private File fileImage;
    private File video;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.post_today_offer, container, false);
        ButterKnife.bind(this, view);

        cityList = ((MainActivity) getActivity()).cityArrayList();
        categoryDatas = ((MainActivity) getActivity()).getCategoryList();
        subCateoryData = new ArrayList<>();
        pattern = Pattern.compile(Constants.englishOnly);
        uploadphoto.setText("Upload Photo");


        editTextDescriptionOfferTodayOffer.setMovementMethod(new ScrollingMovementMethod());

        editTextDescriptionOfferTodayOffer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.editTextDescriptionOfferTodayOffer) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        return view;
    }


    @OnClick(R.id.imageViewTodayOffer)
    public void imageSelectClicked() {
        PickerDialog videoChooserDialog = new PickerDialog();
        videoChooserDialog.show(getActivity().getSupportFragmentManager(), PickerDialog.class.getName());
        videoChooserDialog.setCallback(new ImagePickerDialogListener() {
            @Override
            public void getResultFromCamera(String result, String from) {
                CameraFragment cameraFragment = new CameraFragment();
                cameraFragment.setRecoringListener(getContext(), new UriRecordingListener() {
                    @Override
                    public void setFileImageName(Uri name, String from) {
                        methodUriConvert(name);
                        String filePath = Utils.getPathFromUri(name, getContext());

                        if (filePath != null)
                            image = new File(filePath);

                        mediaType = "P";
                    }

                    @Override
                    public void setFileVideo(String file) {
                        videoToImage(file);
                        mediaType = "V";
                    }
                });
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();

                fragmentTransaction.add(R.id.placeholder, cameraFragment, PostAdsFragment.class.getName());
                fragmentTransaction.addToBackStack(CameraFragment.class.getName());
                fragmentTransaction.commitAllowingStateLoss();
            }

            @Override
            public void getResultFromGallery(Intent result, String from) {
                if (from.equals("video")) {
                    toCheckVideoLength(result);
                    Debugger.e("VIDEO URL PATH IS :::::::::::::::: " + pathVideo);
                    mediaType = "V";
                } else if (from.equals("image")) {
                    Uri selectedImage = result.getData();
                    String path = FileUtils.getPath(getActivity(), selectedImage);
                    if (path != null)
                        image = new File(path);
                    ((MainActivity) getActivity()).picasso.load(image).into(imageViewTodayOffer);
                    /*((MainActivity) getActivity()).setImage(image, imageViewTodayOffer);*/
                    todayoffertopLayout.setVisibility(View.INVISIBLE);
                    mediaType = "P";
                }
            }

            @Override
            public void nothingSet() {
            }
        });

    }

    public void tosetFramming(Intent data) {
        VideoTrimDialog videoTrimDialog = new VideoTrimDialog();
        Bundle bundle = new Bundle();
        bundle.putString("key", FileUtils.getPath(getActivity(), data.getData()));
        videoTrimDialog.setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogTheme);
        videoTrimDialog.setArguments(bundle);

        videoTrimDialog.setCallback(new TrimListener() {
            @Override
            public void onTrimmingDone(String url) {
                Debugger.e("URL IS CALL BACK FROM VIDEO TRIM...... " + url);
                videoToImage(url);
            }
        });
        videoTrimDialog.show(getActivity().getSupportFragmentManager(), VideoTrimDialog.class.getName());
    }

    public void toCheckVideoLength(Intent data) {
        try {
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            retriever.setDataSource(getActivity().getApplication(), data.getData());
            MediaPlayer mp = MediaPlayer.create(getActivity().getBaseContext(), data.getData());
            int millis = mp.getDuration();
            Debugger.e("Millis:::>" + millis);
            videoUri = data.getData();
            String video = FileUtils.getPath(getActivity(), videoUri);
            seconds = (millis / 1000);
            if (seconds > 15) {
                tosetFramming(data);
            } else {
                videoToImage(video);
            }
            Debugger.e("Seconds:::>" + seconds);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void videoToImage(String path) {
        try {
//            Bitmap bitmap = getVidioThumbnail(path);
            Debugger.e("video to image full path is ::::::::::::: " + path);
            Uri uri = Uri.parse(path);
            Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MINI_KIND);

            /*MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            retriever.setDataSource(getActivity().getApplication(), uri);
            Bitmap bitmap = retriever.getFrameAtTime(1000, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);*/

            setImageToView(bitmap);
            this.pathVideo = path;
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            String videoImage = "video_image" + System.currentTimeMillis() + ".jpeg";
            bitmap.compress(Bitmap.CompressFormat.JPEG, 85, byteArrayOutputStream);
            File outputDir = getActivity().getCacheDir();
            fileImage = new File(outputDir, "img_" + System.currentTimeMillis() + ".jpg");
            // fileImage.createNewFile();

            FileOutputStream fileOutputStream = new FileOutputStream(fileImage);
            fileOutputStream.write(byteArrayOutputStream.toByteArray());
            fileOutputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            Debugger.e("EXCEPTION ::: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void setImageToView(final Bitmap bitmap) {
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Debugger.e("CALLL IMAGE SET >>>>");
                    todayoffertopLayout.setVisibility(View.INVISIBLE);
                    imageViewTodayOffer.setImageBitmap(bitmap);
                }
            });
        }
    }

    public void postOffer() {
        if (getActivity() != null)
            ((MainActivity) getActivity()).startLoader();

        ArrayList<KeyValuePair> keyValuePair = new ArrayList<>();
        ArrayList<KeyValuePair> keyValuePairVideo = new ArrayList<>();
        if (!mediaType.equals("")) {
            if (mediaType.equalsIgnoreCase("V")) {
                File file = new File(pathVideo);
                if (file.exists())
                    Debugger.e("SIZE :::: " + Integer.parseInt(String.valueOf(file.length() / 1024)) + "   PATH " + file.getAbsolutePath());
                if (fileImage.exists())
                    Debugger.e("SIZE :::: FILE IMAGE " + Integer.parseInt(String.valueOf(fileImage.length() / 1024)) + "  FILE IMAGE PATH " + fileImage.getAbsolutePath());
                keyValuePairVideo.add(new KeyValuePair("media_name", pathVideo));
                keyValuePair.add(new KeyValuePair("video_thumb", fileImage.getAbsolutePath()));
            } else {
                keyValuePair.add(new KeyValuePair("media_name", image.getAbsolutePath()));
            }
        }
        new PostVideoHelper<LoginWrapper>().pingToRequest(Constants.POST_SLIDER
                , "image/*"
                , "video/*"
                , keyValuePair
                , keyValuePairVideo
                , new RequestParameter().getPostOffer(getPostOffer())
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new LoginWrapper()
                , new TaskCompleteListener<LoginWrapper>() {
                    @Override
                    public void onSuccess(LoginWrapper mObject) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();

                            if (mObject.getStatus() == 1) {
                                clearWholeData();
                                ((MainActivity) getActivity()).messageAlert(mObject.getMessage());
                            } else {
                                clearWholeData();
                                ((MainActivity) getActivity()).messageAlert(mObject.getMessage());
                            }
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, LoginWrapper loginWrapper) {
                        if (getActivity() != null) {
                            if (getActivity() != null)
                                ((MainActivity) getActivity()).stopLoader();

                            if (loginWrapper != null) {
                                if (loginWrapper.getStatus() == -1)
                                    ((MainActivity) getActivity()).invalidToken();
                                else if (loginWrapper.getStatus() == 0) {
                                    clearWholeData();
                                    ((MainActivity) getActivity()).messageAlert(loginWrapper.getMessage());
                                }
                            }
                        }
                    }
                });
    }

    private Map<String, String> getPostOffer() {
        /*cat_id,subcat_id,city_id,description,media_type	media_name,video_thumb*/

        Map<String, String> hashMapPostOffer = new HashMap<>();
        /*user_id,post_id,flag*/

        hashMapPostOffer.put("subcat_id", selectSubCatData.getId());
        hashMapPostOffer.put("city_id", seletedCity.getId());
        hashMapPostOffer.put("cat_id", catId);
        hashMapPostOffer.put("description", editTextDescriptionOfferTodayOffer.getText().toString());
        if (mediaType.equalsIgnoreCase("V")) {
            hashMapPostOffer.put("media_type", "V");
        } else if (mediaType.equalsIgnoreCase("P")) {
            hashMapPostOffer.put("media_type", "P");
        } else {
            hashMapPostOffer.put("media_type", "");
        }

        return hashMapPostOffer;
    }


    @OnClick(R.id.todayOfferSelectCity)
    public void todayOfferSelectCity() {
        ((MainActivity) getActivity()).hideKeyboard();
        int[] location = new int[2];
        todayOfferSelectCity.getLocationOnScreen(location);
        cityDropDown = new CityDropDown(getContext(), todayOfferSelectCity.getHeight(), location, cityList, new ItemEventListener<Integer>() {
            @Override
            public void onItemEventFired(Integer integer, Integer t2, String actualPos) {
                if (Locale.getDefault().getDisplayLanguage().equals("العربية"))
                    todayOfferSelectCity.setText(cityList.get(integer).getArName());
                else
                    todayOfferSelectCity.setText(cityList.get(integer).getName());

                seletedCity = cityList.get(integer);
            }
        });
        cityDropDown.show();
    }

    @OnClick(R.id.todayOfferSelectCategory)
    public void toDayOfferSelectCategoryClicked() {
        ((MainActivity) getActivity()).hideKeyboard();
        int[] location = new int[2];
        todayOfferSelectCategory.getLocationOnScreen(location);
        categoryDropDown = new CategoryTimeDropDown(getContext(), todayOfferSelectCategory.getHeight(), location, categoryDatas, new ItemEventListener<Integer>() {
            @Override
            public void onItemEventFired(Integer integer, Integer t2, String actualPos) {
                if (Locale.getDefault().getDisplayLanguage().equals("العربية")) {
                    todayOfferSelectCategory.setText(categoryDatas.get(integer).getArName());
                } else {
                    todayOfferSelectCategory.setText(categoryDatas.get(integer).getName());
                }
                todayOfferSelectSubCategory.setText(getString(R.string.select_sub_categor));
                subCateoryData = ((MainActivity) getActivity()).getSubCategoryList(categoryDatas.get(integer).getId());
                selectCatData = categoryDatas.get(integer);
                catId = categoryDatas.get(integer).getId();
            }
        });
        categoryDropDown.show();
    }

    @OnClick(R.id.todayOfferSelectSubCategory)
    public void todayOfferSelectSubCategoryClicked() {
        if (catId.equals("")) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseSelectFirstCategory));
        } else {
            ((MainActivity) getActivity()).hideKeyboard();
            int[] location = new int[2];
            todayOfferSelectSubCategory.getLocationOnScreen(location);
            subCategoryDropDown = new SubCategoryDropDown(getContext(), todayOfferSelectSubCategory.getHeight(), location, subCateoryData, new ItemEventListener<Integer>() {
                @Override
                public void onItemEventFired(Integer integer, Integer t2, String actualPos) {
                    selectSubCatData = subCateoryData.get(integer);
                    if (Locale.getDefault().getDisplayLanguage().equals("العربية")) {
                        todayOfferSelectSubCategory.setText(subCateoryData.get(integer).getArName());
                    } else {
                        todayOfferSelectSubCategory.setText(subCateoryData.get(integer).getName());
                    }

                }
            });
            subCategoryDropDown.show();
        }
    }

    @OnClick(R.id.todayOfferButtonSubmit)
    public void postSubmitButtonClicked() {
        if (isValid()) {
            ((MainActivity) getActivity()).hideKeyboard();
            postOffer();
            //((MainActivity) getActivity()).messageAlert(getString(R.string.message_postTodayOffer));
        }
    }

    public void clearWholeData() {
        imageViewTodayOffer.setImageResource(0);
        todayoffertopLayout.setVisibility(View.VISIBLE);
        todayOfferSelectCity.setText(getString(R.string.select_city));
        image = new File("");
        pathVideo = "";
        mediaType = "";
        catId = "";
        fileImage = new File("");
        editTextDescriptionOfferTodayOffer.setText("");
        todayOfferSelectCategory.setText(getString(R.string.select_categor));
        todayOfferSelectSubCategory.setText(getString(R.string.select_sub_categor));
    }

    public boolean isValid() {
        if (todayOfferSelectCity.getText().toString().equals(getString(R.string.select_city))) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseSelectCity));
            return false;
        } else if (catId.equals("")) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseSelectCategory));
            return false;
        } else if (todayOfferSelectSubCategory.getText().toString().equals(getString(R.string.select_sub_categor))) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseSelectSubCategory));
            return false;
        }

        if (editTextDescriptionOfferTodayOffer.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterOfferDescription));
            editTextDescriptionOfferTodayOffer.requestFocus();
            return false;
        }
        return true;
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle(getString(R.string.title_postTodayOffer), Enum.setNevigationIcon.MENU, false);
    }

    public void methodUriConvert(Uri image) {

        String pathFromUri = Utils.getPathFromUri(image, getContext());

        int roatation = 0;

        if (pathFromUri != null) {
            try {
                ExifInterface exifInterface = new ExifInterface(pathFromUri);
                int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);

                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        roatation = 90;
                        // imageView.setRotation(90);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        roatation = 180;
                        // imageView.setRotation(180);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        roatation = 270;
                        //imageView.setRotation(270);
                        break;
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (roatation != 0) {
            Matrix matrix = new Matrix();
            matrix.preRotate(roatation);
            BitmapFactory.Options options = new BitmapFactory.Options();
            Bitmap bitmap = BitmapFactory.decodeFile(pathFromUri, options);
            Bitmap rotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            imageViewTodayOffer.setImageBitmap(rotated);
            //((MainActivity) getActivity()).setImage(image, imageViewTimeLine);

            //imageView.setImageBitmap(rotated);
            //Picasso.with(getContext()).load(imageUri).into(imageView);
        } else
            imageViewTodayOffer.setImageURI(image);
        //
        //
        // imageView.se(imageUri);

        todayoffertopLayout.setVisibility(View.INVISIBLE);
    }
}
