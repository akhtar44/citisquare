package com.citisquare.app.fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.PlaceAutocompleteAdapter;
import com.citisquare.app.controls.CustomAutoComplete;
import com.citisquare.app.controls.CustomButton;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.interfaces.LocationCallback;
import com.citisquare.app.interfaces.PaymentCallBack;
import com.citisquare.app.pojo.PayfortStatus;
import com.citisquare.app.pojo.response.AddBranchWrapper;
import com.citisquare.app.pojo.response.BranchData;
import com.citisquare.app.pojo.response.City;
import com.citisquare.app.pojo.response.FortSdkResponse;
import com.citisquare.app.pojo.response.LoginData;
import com.citisquare.app.pojo.response.LoginWrapper;
import com.citisquare.app.utils.DataToPref;
import com.citisquare.app.utils.Debugger;
import com.citisquare.app.utils.LocationHelper;
import com.citisquare.app.utils.RequestParameter;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.payfort.fort.android.sdk.base.FortSdk;
import com.payfort.sdk.android.dependancies.models.FortRequest;

import java.io.IOException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import serviceCalling.builder.KeyValuePair;
import serviceCalling.builder.ServiceParameter;
import serviceCalling.helper.GetRequestHelper;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 16/8/16.
 */
public class AddBranchFragment extends BaseFragment implements OnMapReadyCallback, GoogleMap.InfoWindowAdapter, PaymentCallBack {


    @BindView(R.id.editTextEnterTitle)
    EditText editTextEnterTitle;
    @BindView(R.id.map)
    MapView map;
    @BindView(R.id.addBranchSubmit)
    CustomButton addBranchSubmit;
    LatLng selectedAddress;
    @BindView(R.id.editTextEnterAddress)
    CustomAutoComplete editTextEnterAddress;
    @BindView(R.id.textViewSelectCity)
    CustomTextView textViewSelectCity;
    ArrayList<City> cityList;
    CityDropDownSeller cityDropDown;
    Bundle bundle;
    String bundleString = "";
    String device_id = "";
    BranchData branchData;
    String language = "";
    City city;
    String merchant_id = "";
    LoginData loginData;
    LoginWrapper loginWrapper;

    PlaceAutocompleteAdapter placeAdapter;
    private GoogleMap googleMap;
    private LocationCallback locationCallback;
    private LocationHelper locationHelper;
    private Pattern pattern;
    private Matcher matcher;

    public static String encode(String base) {

        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;
        try {
            MapsInitializer.initialize(getActivity());
            view = inflater.inflate(R.layout.layout_add_branch, container, false);
            ButterKnife.bind(this, view);
            cityList = ((MainActivity) getActivity()).cityArrayList();
            map.onCreate(savedInstanceState);

            device_id = FortSdk.getDeviceId(getContext());
            placeAdapter = new PlaceAutocompleteAdapter(getContext(), R.layout.raw_autocomplete_text);
            editTextEnterAddress.setAdapter(placeAdapter);
            editTextEnterAddress.setThreshold(1);

            bundle = getArguments();
            if (Locale.getDefault().getDisplayLanguage().equals("العربية"))
                language = "ar";
            else
                language = "en";
            loginWrapper = ((MainActivity) getActivity()).getData(getContext());
            loginData = loginWrapper.getData();
            ((MainActivity) getActivity()).setPaymentCallBack(this);

            pattern = Pattern.compile(Constants.englishOnly);

            if (bundle != null) {
                if (bundle.getString("Branch") != null) {
                    bundleString = bundle.getString("Branch");
                }
                if (bundle.getString("data") != null) {
                    branchData = new Gson().fromJson(bundle.getString("data"), BranchData.class);
                    if (branchData != null) {
                        setData(branchData);
                    }
                }
            }

            if (map != null) {
                map.getMapAsync(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        ButterKnife.bind(this, view);
        return view;
    }

    private void setData(BranchData branchData) {
        if (branchData != null) {
            LatLng loc = new LatLng(Double.parseDouble(branchData.getLatitude()), Double.parseDouble(branchData.getLongitude()));
            drawMarker(loc, "", "", R.drawable.current_location);
            editTextEnterTitle.setText(branchData.getTitle());
            editTextEnterAddress.setText(branchData.getAddress());
            for (int i = 0; i < cityList.size(); i++) {
                if (cityList.get(i).getId().equals(branchData.getCityId())) {
                    city = cityList.get(i);
                    textViewSelectCity.setText(city.getName());
                }
            }
        }
    }

    @OnClick(R.id.addBranchSubmit)
    public void addBranchSubmitClicked() {
        if (isValid()) {
            //clearValue();
            if (bundleString.equals("ADD")) {
                wsCallGetSdkToken(DataToPref.getData(getContext(), "branch_price"));
                //getLocationAndPayment();
                /*((MainActivity) getActivity()).messageAlert("Branch added successfully", "");*/
            } else if (bundleString.equals("EDIT")) {
                if (branchData != null) {
                    if (branchData.getAddress().equals(editTextEnterAddress.getText().toString())) {
                        editBranch(Double.valueOf(branchData.getLatitude()), Double.valueOf(branchData.getLongitude()));
                    } else {
                        getLocationAndPayment("", "");
                    }
                }
            }
        }
    }


    private void editBranch(Double lat, Double lng) {
        if (getActivity() != null)
            ((MainActivity) getActivity()).startLoader();

        new PostRequestHelper<AddBranchWrapper>().pingToRequest(Constants.EDIT_BRANCH
                , null
                , null
                , new RequestParameter().getHashMapForEditBranch(getHashMapEdit(lat, lng))
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new AddBranchWrapper()
                , new TaskCompleteListener<AddBranchWrapper>() {
                    @Override
                    public void onSuccess(AddBranchWrapper addBranchWrapper) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        if (addBranchWrapper.getStatus() == 1) {
                            //((MainActivity) getActivity()).messageAlert(addBranchWrapper.getMessage(), "");
                            Toast.makeText(getActivity(),addBranchWrapper.getMessage(),Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, AddBranchWrapper addBranchWrapper) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        if (addBranchWrapper.getStatus() == -1) {
                            ((MainActivity) getActivity()).invalidToken();
                        }
                    }
                });
    }

    private HashMap<String, String> getHashMapEdit(Double lat, Double lng) {
        HashMap<String, String> hashMapEdit = new HashMap<>();
        /*branch_id,title,address,city_id,latitude,longitude*/
        if (branchData != null)
            hashMapEdit.put("branch_id", branchData.getId());

        hashMapEdit.put("address", editTextEnterAddress.getText().toString().trim());
        hashMapEdit.put("city_id", city.getId());
        hashMapEdit.put("title", editTextEnterTitle.getText().toString().trim());
        hashMapEdit.put("latitude", lat + "");
        hashMapEdit.put("longitude", lng + "");

        return hashMapEdit;
    }


    private void addBranch(Double lat, Double lng, String fortId, String status) {
        if (getActivity() != null)
            ((MainActivity) getActivity()).startLoader();

        new PostRequestHelper<AddBranchWrapper>().pingToRequest(Constants.ADD_BRANCH
                , null
                , null
                , new RequestParameter().getHashMapForAddBranch(getHashMapForAddBranch(lat, lng, fortId, status))
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new AddBranchWrapper()
                , new TaskCompleteListener<AddBranchWrapper>() {
                    @Override
                    public void onSuccess(AddBranchWrapper addBranchWrapper) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();

                            if (addBranchWrapper.getStatus() == 1) {
                                ((MainActivity) getActivity()).messageAlert(addBranchWrapper.getMessage(), "");
                                Toast.makeText(getActivity(),addBranchWrapper.getMessage(),Toast.LENGTH_LONG).show();
                                clearValue();
                            }
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, AddBranchWrapper addBranchWrapper) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();

                            if (addBranchWrapper.getStatus() == -1) {
                                ((MainActivity) getActivity()).invalidToken();
                            }
                        }
                    }
                });
    }

    private HashMap<String, String> getHashMapForAddBranch(Double lat, Double lng, String fortId, String status) {
        /*title,address,city_id,latitude,longitude*/
        HashMap<String, String> hashMapBranch = new HashMap<>();
        hashMapBranch.put("latitude", String.valueOf(lat));
        hashMapBranch.put("longitude", String.valueOf(lng));
        hashMapBranch.put("city_id", city.getId());
        hashMapBranch.put("status", status);
        hashMapBranch.put("transaction_id", fortId);
        hashMapBranch.put("title", editTextEnterTitle.getText().toString().trim());
        hashMapBranch.put("address", editTextEnterAddress.getText().toString().trim());
        return hashMapBranch;
    }

    public void clearValue() {
        editTextEnterTitle.setText("");
        textViewSelectCity.setText("");
    }


    @OnClick(R.id.textViewSelectCity)
    public void selectCityClicked() {
        ((MainActivity) getActivity()).hideKeyboard();
        int[] location = new int[2];
        textViewSelectCity.getLocationOnScreen(location);
        cityDropDown = new CityDropDownSeller(getContext(), textViewSelectCity.getHeight(), location, cityList, new ItemEventListener<String>() {
            @Override
            public void onItemEventFired(String integer, String t2, String actualPos) {
                city = new Gson().fromJson(t2, City.class);

                if (Locale.getDefault().getDisplayLanguage().equals("العربية"))
                    textViewSelectCity.setText(city.getArName());
                else
                    textViewSelectCity.setText(city.getName());


            }
        });
        cityDropDown.show();
    }

    public boolean isValid() {
        if (editTextEnterTitle.getText().toString().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterTitle));
            editTextEnterTitle.requestFocus();
            return false;
        }

        if (editTextEnterAddress.getText().toString().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterAddress));
            editTextEnterAddress.requestFocus();
            return false;
        } /*else {
            matcher = pattern.matcher(editTextEnterAddress.getText().toString());
            if (matcher.find()) {
                editTextEnterAddress.setText("");
                ((MainActivity) getActivity()).messageAlert(getString(R.string.message_englishValidation));
                editTextEnterAddress.requestFocus();
                return false;
            }
        }*/

        if (textViewSelectCity.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterCity));
            textViewSelectCity.requestFocus();
            return false;
        }

        return true;
    }

    public void getLocationAndPayment(final String fortId, final String status) {
        new GetRequestHelper<com.citisquare.app.pojo.addresslocation.Address>().pingToRequest("http://maps.googleapis.com/maps/api/geocode/json?address=" +
                        editTextEnterAddress.getText().toString().replace("\"", "") + "&sensor=true"
                , new ArrayList<KeyValuePair>()
                , new ServiceParameter()
                , new com.citisquare.app.pojo.addresslocation.Address()
                , new TaskCompleteListener<com.citisquare.app.pojo.addresslocation.Address>() {
                    @Override
                    public void onSuccess(com.citisquare.app.pojo.addresslocation.Address mObject) {
                        try {
                            Debugger.e("GET LOCATION:::" + mObject);
                            if (mObject.getStatus().equalsIgnoreCase("ok")) {
                                Double lat = mObject.getResults().get(0).getGeometry().getLocation().getLat();
                                Double lng = mObject.getResults().get(0).getGeometry().getLocation().getLng();
                                if (bundleString.equals("ADD")) {
                                    addBranch(lat, lng, fortId, status);
                                } else if (bundleString.equals("EDIT")) {
                                    editBranch(lat, lng);
                                }
                            } else {
                                ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterValidLocation));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, com.citisquare.app.pojo.addresslocation.Address address) {

                    }
                });
    }

    public void getLocationTemp() {
        new GetRequestHelper<com.citisquare.app.pojo.addresslocation.Address>().pingToRequest("http://maps.googleapis.com/maps/api/geocode/json?address=" +
                        editTextEnterAddress.getText().toString() + "&sensor=true"
                , new ArrayList<KeyValuePair>()
                , new ServiceParameter()
                , new com.citisquare.app.pojo.addresslocation.Address()
                , new TaskCompleteListener<com.citisquare.app.pojo.addresslocation.Address>() {
                    @Override
                    public void onSuccess(com.citisquare.app.pojo.addresslocation.Address mObject) {
                        try {
                            Debugger.e("GET LOCATION:::" + mObject);
                            if (mObject.getStatus().equalsIgnoreCase("ok")) {
                                Double lat = mObject.getResults().get(0).getGeometry().getLocation().getLat();
                                Double lng = mObject.getResults().get(0).getGeometry().getLocation().getLng();
                                LatLng latLngLocation = new LatLng(lat, lng);
                                ((MainActivity) getActivity()).hideKeyboard();
                                getAdressFromLatlong(latLngLocation);
                                drawMarker(latLngLocation, "", "", R.drawable.current_location);
                                editTextEnterAddress.dismissDropDown();
                            } else {
                                ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterValidLocation));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, com.citisquare.app.pojo.addresslocation.Address address) {

                    }
                });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (map != null)
            map.onResume();

        if (bundleString.equals("ADD")) {
            startlocationlisting(new LocationCallback() {
                @Override
                public void onLocationError() {

                }

                @Override
                public void onLocationRecive(Location location) {
                    setMarkerLocation(googleMap, new LatLng(location.getLatitude(), location.getLongitude()));
                }
            });
        } else {
            if (branchData != null)
                setData(branchData);
        }


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        if (map != null)
            map.onLowMemory();
        super.onLowMemory();
    }

    @Override
    public void setHeaderTitle() {
        if (bundleString.equals("ADD"))
            ((MainActivity) getActivity()).setTitle(getString(R.string.addbranch), Enum.setNevigationIcon.BACK, true);
        else if (bundleString.equals("EDIT"))
            ((MainActivity) getActivity()).setTitle(getString(R.string.edit_branch), Enum.setNevigationIcon.BACK, true);
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        if (googleMap != null) {
            this.googleMap = googleMap;
            setUpMap();

            LatLng location = null;
            if (bundleString.equals("EDIT")) {
                if (branchData != null) {
                    location = new LatLng(Double.parseDouble(branchData.getLatitude()), Double.parseDouble(branchData.getLongitude()));
                    setMarkerLocation(googleMap, location);
                }
            } else {

            }

            googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    googleMap.clear();
                    googleMap.addMarker(new MarkerOptions()
                            .position(latLng)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.current_location))
                            .title("CityInfo"));
                    getAdressFromLatlong(latLng);
                }
            });

        }
    }


    private void setMarkerLocation(GoogleMap googleMap, LatLng locationLatlng) {
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                locationLatlng, 11));
        googleMap.addMarker(new MarkerOptions()
                .position(locationLatlng)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.current_location))
                .title("CityInfo"));

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(locationLatlng)      // Sets the center of the map to location user
                .zoom(13)// Sets the zoom

                .build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    public void getAdressFromLatlong(LatLng latLng) {
        // DebugLog.e("latlong is::::"+ latLng);
        selectedAddress = latLng;
        Geocoder geocoder;
        if (latLng.latitude != 0.0 && latLng.longitude != 0.0) {
            List<Address> addresses = null;
            geocoder = new Geocoder(getActivity(), Locale.getDefault());
            try {
                addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (addresses != null && !addresses.isEmpty()) {
                String area = "";
                if (addresses.get(0).getAddressLine(0) != null) {
                    area = addresses.get(0).getAddressLine(0);
                    if (addresses.get(0).getAddressLine(1) != null) {
                        area = area + " " + addresses.get(0).getAddressLine(1);
                    }
                }
                editTextEnterAddress.setText(area);
                //Once address is select return to add item detail fragment
            } /*else {
                textViewSelectAddress.setText("We are not serving in this area");
            }*/
        }
    }

    public void setUpMap() {
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.setInfoWindowAdapter(this);
    }

    public Marker drawMarker(LatLng place, String title, String content, int icon) {
        Marker mMarker = null;
        if (googleMap != null) {
            MarkerOptions markerOptions = new MarkerOptions()
                    .position(place)
                    .infoWindowAnchor(0.5f, -0.0f)
                    .title(title)
                    .snippet(content)
                    .icon(BitmapDescriptorFactory.fromResource(icon));
            googleMap.clear();

            mMarker = googleMap.addMarker(markerOptions);

        }
        return mMarker;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }


    public void startlocationlisting(LocationCallback locationCallback) {
        this.locationCallback = locationCallback;
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    ) {
                requestPermissions(Constants.ACCESS_FINE_LOCATION, Constants.LOCATIONREQUESTCODE);
            } else {
                startLisning();
            }
        } else startLisning();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Debugger.e("PERMISSION ::" + requestCode);
        switch (requestCode) {
            case Constants.LOCATIONREQUESTCODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLisning();
                } else {

                }
                return;
            }
        }
    }

    private void startLisning() {
        locationHelper = new LocationHelper(this, new LocationHelper.ProvideLocation() {
            @Override
            public void currentLocation(final Location location) {
                locationCallback.onLocationRecive(location);
            }

            @Override
            public void onError() {
                locationCallback.onLocationError();
            }
        });
    }

    private void wsCallGetSdkToken(final String amount) {
        if (getActivity() != null) {
            ((MainActivity) getActivity()).startLoader();
        }
        List<KeyValuePair> keyValuePairs = new ArrayList<>();
        keyValuePairs.add(new KeyValuePair("device_id", device_id));

        new GetRequestHelper<FortSdkResponse>().pingToRequest(Constants.GETSDK_TOKEN
                , keyValuePairs
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new FortSdkResponse()
                , new TaskCompleteListener<FortSdkResponse>() {
                    @Override
                    public void onSuccess(FortSdkResponse fortSdkResponse) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();
                        }
                        if (fortSdkResponse != null) {
                            if (!fortSdkResponse.getSdkToken().equalsIgnoreCase("")) {
                                next(fortSdkResponse.getSdkToken(), amount);
                            } else {
                                if (fortSdkResponse.getResponseCode() != null)
                                    ((MainActivity) getActivity()).messageAlert(fortSdkResponse.getResponseMessage());

                            }
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, FortSdkResponse subscriptionPlanWrapper) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();
                        }

                    }
                });
    }

    private void next(String sdkToken, String amount) {
        try {
            merchant_id = "" + System.currentTimeMillis();
            FortRequest fortRequest = new FortRequest();
            Double aDouble = Double.parseDouble(amount);
            aDouble = aDouble * 100;
            long d = (new Double(aDouble)).longValue();
            Map<String, String> hashMapString = new HashMap<String, String>();
            hashMapString.put("amount", String.valueOf(d));
            hashMapString.put("command", "PURCHASE");
            hashMapString.put("currency", "SAR");
            hashMapString.put("customer_email", loginData.getEmail());
            hashMapString.put("customer_name", loginData.getFirstName() + loginData.getLastName());
            hashMapString.put("language", language);
            hashMapString.put("merchant_reference", merchant_id);
            hashMapString.put("sdk_token", sdkToken);
            fortRequest.setShowResponsePage(true);
            ((MainActivity) getActivity()).payment(hashMapString);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSucess(Map<String, String> map, Map<String, String> map1) {
        //  paymentCallApi(map1.get("fort_id"));
        getLocationAndPayment(map1.get("fort_id"), "1");
    }

    @Override
    public void onFailureTransaction(Map<String, String> map, Map<String, String> map1) {
        if (map1.get("response_message").equalsIgnoreCase("Duplicate order number")) {
            getCheckStatus();
        } else {
            getLocationAndPayment("000", "0");
        }
    }

    @Override
    public void onCancel(Map<String, String> map, Map<String, String> map1) {

    }

    private void getCheckStatus() {
        if (getActivity() != null)
            ((MainActivity) getActivity()).startLoader();
///check_payfortstatus/merchant_reference/12345

        ArrayList<KeyValuePair> keyValuePairs = new ArrayList<>();
        keyValuePairs.add(new KeyValuePair("merchant_reference", merchant_id));

        new GetRequestHelper<PayfortStatus>().pingToRequest(Constants.CHECK_STATUS
                , keyValuePairs
                , ((MainActivity) getActivity()).getHeader()
                , new PayfortStatus()
                , new TaskCompleteListener<PayfortStatus>() {
                    @Override
                    public void onSuccess(PayfortStatus subscriptionPlanWrapper) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();
                            if (subscriptionPlanWrapper.getStatus().equals("12") && subscriptionPlanWrapper.getTransactionCode().equals("14")) {
                                getLocationAndPayment(subscriptionPlanWrapper.getFortId(), "1");
                            } else {
                                wsCallGetSdkToken(DataToPref.getData(getContext(), "branch_price"));
                            }
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, PayfortStatus subscriptionPlanWrapper) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();
                        }
                        if (subscriptionPlanWrapper != null) {

                        }
                    }
                });
    }


}