package com.citisquare.app.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Navigation;
import com.citisquare.app.pojo.response.LoginData;
import com.citisquare.app.pojo.response.LoginWrapper;
import com.citisquare.app.utils.Debugger;
import com.citisquare.app.utils.RequestParameter;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink16 on 25/5/16.
 */
public class LoginFragment1 extends BaseFragment {
    @BindView(R.id.editTextEmail)
    EditText editTextEmail;
    @BindView(R.id.editTextPassword)
    EditText editTextPassword;
    @BindView(R.id.textViewLogin)
    TextView textViewLogin;
    @BindView(R.id.visitorsignup)
    TextView visitorsignup;
    @BindView(R.id.sellerSignup)
    TextView sellerSignup;
    @BindView(R.id.continueGuestUser)
    LinearLayout continueGuestUser;
    @BindView(R.id.logoName)
    TextView logoName;
    @BindView(R.id.loginForgotPassword)
    TextView loginForgotPassword;
    LinearLayout main_linearlayout;


    private Pattern pattern;
    private Matcher matcher;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_layout, container, false);
        main_linearlayout = (LinearLayout) view.findViewById(R.id.main_linear_layout);
        ButterKnife.bind(this, view);
        if (getActivity() != null)
            ((MainActivity) getActivity()).setLoginScreen();


        pattern = Pattern.compile(Constants.englishOnly);

        Spannable spannableQuickInfo = new SpannableString(getResources().getString(R.string.QUICK_INFO));
        spannableQuickInfo.setSpan(new ForegroundColorSpan(Color.WHITE), 5, spannableQuickInfo.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        logoName.setText(spannableQuickInfo);

        /*Spannable spannable = new SpannableString(getResources().getString(R.string.continue_as_a_guest_user));
        spannable.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.yellow_color)), 14, spannable.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        continueGuestUser.setText(spannable);*/
        return view;
    }

    @OnClick(R.id.textViewLogin)
    public void onLoginClicked() {
        if (isValid()) {
            if (getActivity() != null)
                ((MainActivity) getActivity()).hideKeyboard();

            //((MainActivity) getActivity()).replaceParentFragment(new TabFragment(), "parent");
            loginWsCall();
        }
    }


    public boolean isValid() {
        if (!validEmail(editTextEmail.getText().toString())) {
            editTextEmail.requestFocus();
            return false;
        }
        if (editTextPassword.getText().toString().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterPassword));
            editTextPassword.requestFocus();
            return false;
        }
        return true;
    }

    public void loginWsCall() {
        if (getActivity() != null)
            ((MainActivity) getActivity()).startLoader();

        new PostRequestHelper<LoginWrapper>().pingToRequest(Constants.LOGIN
                , null
                , null
                , new RequestParameter().getHashMapLogin(getHashMapParameter())
                , ((MainActivity) getActivity()).getHeader()
                , new LoginWrapper()
                , new TaskCompleteListener<LoginWrapper>() {
                    @Override
                    public void onSuccess(LoginWrapper login) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        if (login.getStatus() == 1) {
                            // ((MainActivity) getActivity()).messageAlert(login.getMessage());
                            ((MainActivity) getActivity()).replaceParentFragment(new TabFragment(), "parent");
                            ((MainActivity) getActivity()).setData(getContext(), new Gson().toJson(login));
                            ((MainActivity) getActivity()).setUserSession(getContext(), login.getData().getUserSessionId());

                        } else
                            ((MainActivity) getActivity()).messageAlert(login.getMessage());
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, LoginWrapper wrapper) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();
                        if (exceptions == ExceptionType.ParameterException) {
                            Debugger.e("EXCEPTION:::" + wrapper.getMessage());
                            ((MainActivity) getActivity()).messageAlert(wrapper.getMessage());
                        }
                    }
                });
    }

    public HashMap<String, String> getHashMapParameter() {
        HashMap<String, String> hashMapLogin = new HashMap<>();
        hashMapLogin.put("email", editTextEmail.getText().toString());
        hashMapLogin.put("password", editTextPassword.getText().toString());
        hashMapLogin.put("device_token", ((MainActivity) getActivity()).getDeviceId());
        return hashMapLogin;
    }

    @OnClick(R.id.continueGuestUser)
    public void continueGuestUserClicked() {
        LoginWrapper loginWrapper = new LoginWrapper();
        LoginData loginData = new LoginData("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "guest user", "", "", "", "", "", "", "", "", "", "", "");
        loginWrapper.setData(loginData);
        ((MainActivity) getActivity()).setData(getContext(), new Gson().toJson(loginWrapper));
        //((MainActivity) getActivity()).setUserSession(getContext(), login.getData().getUserSessionId());
        ((MainActivity) getActivity()).replaceParentFragment(new TabFragment(), "parent");
    }

    public boolean validEmail(String email) {
        int length = email.trim().length();
        if (length <= 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterEmail));
            return false;
        }
        Pattern pattern = Pattern.compile(Navigation.EMAIL_REGEX);
        Matcher matcher = pattern.matcher(email);
        if (!(email.charAt(0) + "").matches("\\p{L}") || !matcher.matches()) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterValidEmailId));
            return false;
        }
        return true;
    }


    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).actionBarHide();
        ((MainActivity) getActivity()).lockDrawer();
    }

    @OnClick(R.id.visitorsignup)
    public void visitorSignup() {
        ((MainActivity) getActivity()).visitorFragment();
    }

    @OnClick(R.id.sellerSignup)
    public void sellerSignup() {
        ((MainActivity) getActivity()).sellerFragment();
    }

    @OnClick(R.id.loginForgotPassword)
    public void forgotPasswordClicked() {
        ((MainActivity) getActivity()).forgotDialogFragment();
    }

}
