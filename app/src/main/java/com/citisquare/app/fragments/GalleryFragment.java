package com.citisquare.app.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.GalleryAdapter;
import com.citisquare.app.camera.utils.Utils;
import com.citisquare.app.dialog.PickerDialog;
import com.citisquare.app.dialog.VideoTrimDialog;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.ImagePickerDialogListener;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.listener.TrimListener;
import com.citisquare.app.listener.UriRecordingListener;
import com.citisquare.app.pojo.response.GalleryData;
import com.citisquare.app.pojo.response.GalleryWrapper;
import com.citisquare.app.pojo.response.LoginWrapper;
import com.citisquare.app.utils.Debugger;
import com.citisquare.app.utils.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import serviceCalling.builder.KeyValuePair;
import serviceCalling.helper.GetRequestHelper;
import serviceCalling.helper.PostVideoHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 1/6/16.
 */
public class GalleryFragment extends BaseFragment {

    @BindView(R.id.galleyRecyclerView)
    RecyclerView galleyRecyclerView;

    GalleryAdapter galleryAdapter;
    GridLayoutManager gridLayoutManager;
    ArrayList<com.citisquare.app.pojo.response.GalleryData> galleryDatas;
    Bundle bundle;
    File image;
    View view;
    private String mediaType = "";
    private String pathVideo;
    private File fileImage;
    private Uri videoUri;
    private int seconds;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.gallery_layout, container, false);
        ButterKnife.bind(this, view);
        galleryDatas = new ArrayList<>();

        bundle = getArguments();
        if (bundle != null) {
            if (bundle.getString("Profile", "").equals("own")) {
                galleryDatas.add(0, new com.citisquare.app.pojo.response.GalleryData("DATA"));
            }
        }

        callGalleyApi();

        galleryAdapter = new GalleryAdapter(getContext(), galleryDatas, new ItemEventListener<String>() {
            @Override
            public void onItemEventFired(String s, String t2, String actualPos) {
                if (s.equals("video")) {
                    PlayVideoFragment playVideoFragment = new PlayVideoFragment();
                    Bundle playBundle = new Bundle();
                    playBundle.putString("video", t2);
                    playVideoFragment.setArguments(playBundle);
                    ((MainActivity) getActivity()).setFragment(playVideoFragment, false, "");
                } else if (s.equals("photo")) {
                    DetailsImageFragment detailsImageFragment = new DetailsImageFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("data", t2);
                    detailsImageFragment.setArguments(bundle);
                    detailsImageFragment.show(getActivity().getSupportFragmentManager(), detailsImageFragment.getClass().getName());
                } else if (s.equals("gallery")) {
                    PickerDialog videoChooserDialog = new PickerDialog();
                    videoChooserDialog.show(getActivity().getSupportFragmentManager(), PickerDialog.class.getName());
                    videoChooserDialog.setCallback(new ImagePickerDialogListener() {
                        @Override
                        public void getResultFromCamera(String result, String from) {
                            CameraFragment cameraFragment = new CameraFragment();
                            cameraFragment.setRecoringListener(getContext(), new UriRecordingListener() {
                                @Override
                                public void setFileImageName(Uri name, String from) {
                                    //methodUriConvert(name);
                                    String filePath = Utils.getPathFromUri(name, getContext());

                                    if (filePath != null)
                                        image = new File(filePath);

                                    mediaType = "P";
                                    galleryImageAdd();
                                }

                                @Override
                                public void setFileVideo(String file) {
                                    mediaType = "V";
                                    videoToImage(file);

                                }
                            });
                            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();

                            fragmentTransaction.add(R.id.placeholder, cameraFragment, PostAdsFragment.class.getName());
                            fragmentTransaction.addToBackStack(CameraFragment.class.getName());
                            fragmentTransaction.commitAllowingStateLoss();
                        }

                        @Override
                        public void getResultFromGallery(Intent result, String from) {
                            if (from.equals("video")) {
                                mediaType = "V";
                                toCheckVideoLength(result);

                            } else if (from.equals("image")) {
                                Uri selectedImage = result.getData();
                                String path = FileUtils.getPath(getActivity(), selectedImage);
                                mediaType = "P";
                                if (path != null)
                                    image = new File(path);
                                galleryImageAdd();
                            }
                        }

                        @Override
                        public void nothingSet() {
                        }
                    });
                }
            }
        });

        gridLayoutManager = new GridLayoutManager(getActivity(), 4);
        galleyRecyclerView.setLayoutManager(gridLayoutManager);
        galleyRecyclerView.setAdapter(galleryAdapter);
        return view;
    }

    public void toCheckVideoLength(Intent data) {
        try {
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            retriever.setDataSource(getActivity().getApplication(), data.getData());
            MediaPlayer mp = MediaPlayer.create(getActivity().getBaseContext(), data.getData());
            int millis = mp.getDuration();
            Debugger.e("Millis:::>" + millis);
            videoUri = data.getData();
            String video = FileUtils.getPath(getActivity(), videoUri);
            seconds = (millis / 1000);
            if (seconds > 15) {
                tosetFramming(data);
            } else {
                videoToImage(video);
            }
            Debugger.e("Seconds:::>" + seconds);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void tosetFramming(Intent data) {
        final VideoTrimDialog videoTrimDialog = new VideoTrimDialog();
        Bundle bundle = new Bundle();
        bundle.putString("key", FileUtils.getPath(getActivity(), data.getData()));
        videoTrimDialog.setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogTheme);
        videoTrimDialog.setArguments(bundle);

        videoTrimDialog.setCallback(new TrimListener() {
            @Override
            public void onTrimmingDone(String url) {
                Debugger.e("URL IS CALL BACK FROM VIDEO TRIM...... " + url);
                videoToImage(url);
            }
        });
        videoTrimDialog.show(getActivity().getSupportFragmentManager(), VideoTrimDialog.class.getName());
    }

    public void videoToImage(String path) {
        try {
//            Bitmap bitmap = getVidioThumbnail(path);
            Debugger.e("video to image full path is ::::::::::::: " + path);
            Uri uri = Uri.parse(path);
            Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MINI_KIND);
            this.pathVideo = path;

            fileImage = Utils.saveImage(bitmap);
            // fileImage.createNewFile();

            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        galleryImageAdd();
                    }
                });
            }

        } catch (Exception e) {
            Debugger.e("EXCEPTION ::: " + e.getMessage());
            e.printStackTrace();
        }
    }


    public void galleryImageAdd() {

        if (getActivity() != null) {
            ((MainActivity) getActivity()).startLoader();
        }

        ArrayList<KeyValuePair> keyValuePair = new ArrayList<>();
        ArrayList<KeyValuePair> keyValuePairVideo = new ArrayList<>();
        if (!mediaType.equals("")) {
            if (mediaType.equalsIgnoreCase("V")) {
                File file = new File(pathVideo);
                if (file.exists())
                    Debugger.e("SIZE :::: " + Integer.parseInt(String.valueOf(file.length() / 1024)) + "   PATH " + file.getAbsolutePath());
                if (fileImage.exists())
                    Debugger.e("SIZE :::: FILE IMAGE " + Integer.parseInt(String.valueOf(fileImage.length() / 1024)) + "  FILE IMAGE PATH " + fileImage.getAbsolutePath());
                keyValuePairVideo.add(new KeyValuePair("media_name", pathVideo));
                keyValuePair.add(new KeyValuePair("video_thumb", fileImage.getAbsolutePath()));
            } else {
                keyValuePair.add(new KeyValuePair("media_name", image.getAbsolutePath()));
            }
        }


        new PostVideoHelper<LoginWrapper>().pingToRequest(Constants.GALLERY_PHOTO_ADD
                , "image/*"
                , "video/*"
                , keyValuePair
                , keyValuePairVideo
                , getHashMapVideo()
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new LoginWrapper()
                , new TaskCompleteListener<LoginWrapper>() {
                    @Override
                    public void onSuccess(LoginWrapper loginWrapper) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();
                            ((MainActivity) getActivity()).messageAlert(loginWrapper.getMessage());
                            callGalleyApi();
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, LoginWrapper loginWrapper) {
                        if (getActivity() != null && loginWrapper != null) {
                            ((MainActivity) getActivity()).stopLoader();
                            if (loginWrapper.getStatus() == -1)
                                ((MainActivity) getActivity()).invalidToken();
                        }
                    }
                });
    }

    private Map<String, String> getHashMapVideo() {
        HashMap<String, String> hashMapForGalleryAdd = new HashMap<>();
        hashMapForGalleryAdd.put("media_type", mediaType);
        return hashMapForGalleryAdd;
    }

    public void callGalleyApi() {

        List<KeyValuePair> keyValuePairs = new ArrayList<>();
        keyValuePairs.add(new KeyValuePair("user_id", ((MainActivity) getActivity()).getProfileUserId()));
        new GetRequestHelper<GalleryWrapper>().pingToRequest(Constants.GALLERY_LISTING
                , keyValuePairs
                , ((MainActivity) getActivity()).getHeader()
                , new GalleryWrapper()
                , new TaskCompleteListener<GalleryWrapper>() {
                    @Override
                    public void onSuccess(GalleryWrapper galleryWrapper) {
                        if (galleryWrapper.getStatus() == 1)
                            setGalleryAdapter(galleryWrapper.getData());
                        else
                            ((MainActivity) getActivity()).messageAlert(galleryWrapper.getMessage());
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, GalleryWrapper galleryWrapper) {

                    }
                });
    }


    public void setGalleryAdapter(List<GalleryData> galleryDatas) {
        this.galleryDatas.clear();
        if (bundle != null) {
            if (bundle.getString("Profile", "").equals("own")) {
                galleryDatas.add(0, new GalleryData("DATA"));
            }
        }
        this.galleryDatas.addAll(galleryDatas);
        galleryAdapter.notifyDataSetChanged();
    }

    @Override
    public void setHeaderTitle() {

    }
}
