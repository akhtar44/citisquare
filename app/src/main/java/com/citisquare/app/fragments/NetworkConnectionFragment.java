package com.citisquare.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.interfaces.Enum;

/**
 * Created by hlink56 on 6/7/16.
 */
public class NetworkConnectionFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.network_lost, container, false);
        ((MainActivity)getActivity()).getSupportActionBar().hide();
        return view;
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle("", Enum.setNevigationIcon.NOTHING, true);

    }
}
