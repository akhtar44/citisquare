package com.citisquare.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.citisquare.app.R.id.sendMessageSelectorEditMessage;

/**
 * Created by hlink56 on 1/6/16.
 */
public class SendMessageFragment extends BaseFragment {


    @BindView(sendMessageSelectorEditMessage)
    EditText sendMessageEditMessage;
    @BindView(R.id.selectMembers)
    TextView selectMembers;

    private Pattern pattern;
    private Matcher matcher;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_send_message_group, container, false);
        ButterKnife.bind(this, view);
        pattern = Pattern.compile(Constants.englishOnly);

        sendMessageEditMessage.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.sendMessageSelectorEditMessage) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).hideKeyboard();
            }
        });
        /*messageList = new ArrayList<>();
        messageList.add(new Message("Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s ", "15/05/2016, 12:45pm"));
        messageList.add(new Message("Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s ", "15/05/2016, 12:45pm"));
        messageList.add(new Message("Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s ", "15/05/2016, 12:45pm"));
        messageList.add(new Message("Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s ", "15/05/2016, 12:45pm"));
        messageList.add(new Message("Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s ", "15/05/2016, 12:45pm"));
        sendMessageAdapter = new SendMessageAdapter(getContext(), messageList);
        gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        sendMessageRecyclerView.setLayoutManager(gridLayoutManager);
        sendMessageRecyclerView.setAdapter(sendMessageAdapter);*/
        return view;
    }

    @OnClick(R.id.selectMembers)
    public void selectMembersClicked() {
        if (sendMessageEditMessage.getText().toString().length() == 0)
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterMessage));
        else {


            SelectMemberFragment selectMemberFragment = new SelectMemberFragment();
            Bundle bundle = new Bundle();
            bundle.putString("message", sendMessageEditMessage.getText().toString());
            selectMemberFragment.setArguments(bundle);
            ((MainActivity) getActivity()).setFragment(selectMemberFragment, true, "");
        }
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle(getString(R.string.title_sendMessage), Enum.setNevigationIcon.BACK, true);
    }
}
