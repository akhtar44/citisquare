package com.citisquare.app.fragments;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.CategorySellerAdapter;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.pojo.response.CategoryData;
import com.citisquare.app.pojo.response.Like;
import com.citisquare.app.pojo.response.SellerData;
import com.citisquare.app.pojo.response.SellerWrapper;
import com.citisquare.app.utils.DataToPref;
import com.citisquare.app.utils.Debugger;
import com.citisquare.app.utils.RequestParameter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 6/6/16.
 */
public class CategorySellerFragment extends BaseFragment {

    private final int REQUEST_CALL_PERMISSION = 1141;
    @BindView(R.id.categoryJobDetailsRecycler)
    RecyclerView categoryJobDetailsRecycler;
    @BindView(R.id.relative_empty)
    RelativeLayout relative_empty;
    CategorySellerAdapter categoryAdapter;
    ArrayList<SellerData> subCategoryLists;
    boolean isPagination;
    int pageno = 1;
    GridLayoutManager gridLayoutManager;
    Bundle bundle;
    String subCatId = "";
    String category = "";
    private String phoneNumber = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.category_job_details_layout, container, false);
        pageno = 1;
        ButterKnife.bind(this, view);
        getChildCount();

        BaseFragment baseFragment = (BaseFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.placeholder);
        if (baseFragment instanceof TabFragment) {
            ((TabFragment) baseFragment).setSearchVisibility(Enum.setSearch.SEARCH_FILTER);
        }
        bundle = getArguments();

        if (bundle != null) {
            if (bundle.getString("data") != null) {
                Debugger.e("bundle data::" + bundle.getString("data"));
                subCatId = bundle.getString("data");
            } else if (bundle.getString("subCat") != null) {
                subCatId = bundle.getString("subCat");
                category = bundle.getString("category");
            }
        }

        subCategoryLists = new ArrayList<>();
        categoryAdapter = new CategorySellerAdapter(getContext(), subCategoryLists, new ItemEventListener<String>() {
            @Override
            public void onItemEventFired(String data, String userId, String actualPos) {
                switch (data) {
                    case "phone":
                        phoneNumber = userId;
                        showCallMessage(userId);
                        break;
                    case "profile":
                        Debugger.e("USER ID" + userId);
                        if (userId.equals(((MainActivity) getActivity()).userId())) {
                            ProfileFragment profileFragment = new ProfileFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("profile", "own");
                            ((MainActivity) getActivity()).setProfileUserId(userId);
                            profileFragment.setArguments(bundle);
                            ((MainActivity) getActivity()).setSelectionPostion(0);
                            ((MainActivity) getActivity()).setFragment(profileFragment, false, "");
                            //childFragmentReplacement(profileFragment, "child");
                        } else {
                            ProfileFragment profileFragment = new ProfileFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("profile", "other");
                            ((MainActivity) getActivity()).setProfileUserId(userId);
                            profileFragment.setArguments(bundle);
                            ((MainActivity) getActivity()).setSelectionPostion(0);
                            ((MainActivity) getActivity()).setFragment(profileFragment, false, "");
                            //childFragmentReplacement(profileFragment, "child");
                        }

                        /*ProfileFragment profileFragment = new ProfileFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("profile", "other");
                        profileFragment.setArguments(bundle);
                        ((MainActivity) getActivity()).setSelectionPostion(0);
                        //((MainActivity) getActivity()).setFragment(profileFragment);
                        childFragmentReplacement(profileFragment, "child");*/
                        break;
                    case "map":
                        ((MainActivity) getActivity()).setProfileUserId(userId);
                        ((MainActivity) getActivity()).setFragment(new OtherMapPathFragment(), false, "");

                        break;
                    case "follow":
                        doFollowApiCall(userId, "follow");
                        break;
                    case "unfollow":
                        doFollowApiCall(userId, "unfollow");
                        break;
                }
            }
        });

        gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        categoryJobDetailsRecycler.setLayoutManager(gridLayoutManager);
        categoryJobDetailsRecycler.setAdapter(categoryAdapter);

        categoryJobDetailsRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (isPagination) {
                    int visibleItemCount = recyclerView.getChildCount();
                    int totalItemCount = gridLayoutManager.getItemCount();
                    int firstVisibleItemPosition = gridLayoutManager.findFirstVisibleItemPosition();
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount) {
                        isPagination = false;
                        callSellerApi(++pageno);
                    }
                }
            }
        });
        //callSellerApi(pageno);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Debugger.e("1");
        if (getActivity() != null) {
            getSellerApiCall();
        }
    }

    public void getChildCount() {
        Debugger.e(":::::IN THE DATA::::" + getChildFragmentManager().getBackStackEntryCount());
    }

    public void doFollowApiCall(final String userId, final String status) {
        new PostRequestHelper<Like>().pingToRequest(Constants.DO_FOLLOW
                , null
                , null
                , new RequestParameter().getHashMapForDoFollow(getDoFollowMap(userId, status))
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new Like()
                , new TaskCompleteListener<Like>() {
                    @Override
                    public void onSuccess(Like mObject) {
                        if (mObject.getStatus() == 1)
                            follow(userId, status);
                        Debugger.e("FOLLOW SUCCESSFULLY");
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, Like like) {
                        if (like != null) {
                            if (like.getStatus() == -1)
                                ((MainActivity) getActivity()).invalidToken();
                        }
                    }
                });
    }

    public void follow(String userId, String status) {
        for (int i = 0; i < subCategoryLists.size(); i++) {
            if (subCategoryLists.get(i).getId().equals(userId)) {
                if (status.equals("follow"))
                    subCategoryLists.get(i).setIsFollowing(1);
                else
                    subCategoryLists.get(i).setIsFollowing(0);
            }
        }
        categoryAdapter.notifyDataSetChanged();
    }

    public HashMap<String, String> getDoFollowMap(String userId, String status) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("follower_user_id", userId);
        if (status.equals("follow"))
            hashMap.put("status", "1");
        else
            hashMap.put("status", "0");

        return hashMap;
    }


    public void callSellerApi(final int pageno) {
        if (getActivity() != null && pageno == 1)
            ((MainActivity) getActivity()).startLoader();

        new PostRequestHelper<SellerWrapper>().pingToRequest(Constants.SEARCH_SELLER_DETAIL
                , null
                , null
                , new RequestParameter().getSellerList(getSellerHashMap(pageno))
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new SellerWrapper()
                , new TaskCompleteListener<SellerWrapper>() {
                    @Override
                    public void onSuccess(SellerWrapper sellerWrapper) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        if (pageno == 1)
                            subCategoryLists.clear();

                        if (sellerWrapper.getStatus() == 1) {
                            isPagination = true;
                            setSellerAdapter(sellerWrapper.getData());
                        } else if (sellerWrapper.getStatus() == 0) {
                            isPagination = false;
                            if (pageno == 1)
                               // ((MainActivity) getActivity()).messageAlert(sellerWrapper.getMessage());
                                Toast.makeText(getActivity(),sellerWrapper.getMessage(),Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, SellerWrapper sellerWrapper) {
                        isPagination = false;
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        if (sellerWrapper != null) {
                            if (sellerWrapper.getStatus() == -1) {
                                ((MainActivity) getActivity()).invalidToken();
                            }
                        }

                        if (exceptions == ExceptionType.ParameterException) {
                            isPagination = false;
                            if (pageno == 1)
                                //((MainActivity) getActivity()).messageAlert(sellerWrapper.getMessage());
                            Toast.makeText(getActivity(),sellerWrapper.getMessage(),Toast.LENGTH_LONG).show();
                        }

                    }
                });
    }

    public void getSellerApiCall() {
        pageno = 1;
        callSellerApi(pageno);
    }


    /*city_id,page	cat_id,subcat_id,search,flag,latitude,longitude*/
    public HashMap<String, String> getSellerHashMap(int pageno) {
        HashMap<String, String> hashMapSeller = new HashMap<>();


        String search = "";
        String filter = "";

        if (!DataToPref.getData(getContext(), "CATEGORY").equals("") && category.equals("")) {
            CategoryData categoryData = new Gson().fromJson(DataToPref.getData(getContext(), "CATEGORY"), CategoryData.class);
            hashMapSeller.put("cat_id", categoryData.getId());
        } else if (category.length() > 0) {
            hashMapSeller.put("cat_id", category);
        } else {
            hashMapSeller.put("cat_id", "");
        }

        hashMapSeller.put("city_id", ((MainActivity) getActivity()).getTopCitySelectionId());
        hashMapSeller.put("page", pageno + "");

        hashMapSeller.put("subcat_id", subCatId);

        BaseFragment baseFragment = (BaseFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.placeholder);
        if (baseFragment instanceof TabFragment) {
            search = ((TabFragment) baseFragment).getSearchData();

            if (((TabFragment) baseFragment).getFilterData().equals(getString(R.string.nearby)))
                filter = "nearby";
            else if (((TabFragment) baseFragment).getFilterData().equals(getString(R.string.delivery)))
                filter = "delivery";
            else if (((TabFragment) baseFragment).getFilterData().equals(getString(R.string.followersDrop)))
                filter = "followers";
            else if (((TabFragment) baseFragment).getFilterData().equals(getString(R.string.followingDrop)))
                filter = "following";
            else if (((TabFragment) baseFragment).getFilterData().equals(getString(R.string.qiverifieddrop)))
                filter = "verified";
            else if (((TabFragment) baseFragment).getFilterData().equals(getString(R.string.ratings)))
                filter = "rating";

        }
        LatLng latLng;
        if (getActivity() != null)
            latLng = ((MainActivity) getActivity()).getLatLng();
        else
            latLng = new LatLng(0, 0);

        hashMapSeller.put("latitude", latLng.latitude + "");
        hashMapSeller.put("longitude", latLng.longitude + "");
        hashMapSeller.put("search", search);
        hashMapSeller.put("flag", filter);

        return hashMapSeller;
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle("default", Enum.setNevigationIcon.BACK_CITY, true);
    }

    public void setSellerAdapter(List<SellerData> sellerAdapter) {
        subCategoryLists.addAll(sellerAdapter);
        if(subCategoryLists.size()==0){
            relative_empty.setVisibility(View.VISIBLE);
        }
        else
            relative_empty.setVisibility(View.GONE);
        categoryAdapter.notifyDataSetChanged();
    }


    public void showCallMessage(String number) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                checkForCallPermission(number);
            else
                call(number);
        } catch (Exception e) {
            e.printStackTrace();
            Debugger.e(":::::::ERROR WHILE DIALING NUMBER:::::");
        }
    }

    public void checkForCallPermission(String number) {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL_PERMISSION);
            //call(number);
        } else {
            try {
                call(number);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void call(final String phone) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle("" + getString(R.string.app_name));
        alertDialogBuilder
                .setMessage("" + Html.fromHtml(phone))
                .setPositiveButton("CALL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Intent.ACTION_CALL);
                        intent.setData(Uri.parse("tel:" + phone));
                        startActivity(intent);
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Debugger.e("HOME:::::::::ONPERMISSIONRESULT CALLED");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CALL_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    call(phoneNumber);
                    return;
                } else {
                    //  getActivity().finish();
                }
                break;
        }
    }
}
