package com.citisquare.app.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 6/6/16.
 */
public class AboutFragment extends BaseFragment {


    @BindView(R.id.layout_about_us_webview)
    WebView webView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.about_layout, container, false);
        ButterKnife.bind(this, view);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);


        WebSettings webSettings = webView.getSettings();
        //loads webpages in webview instead of launching browser

        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                if (getActivity() != null)
                    ((MainActivity) getActivity()).startLoader();

                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if (getActivity() != null)
                    ((MainActivity) getActivity()).stopLoader();

                super.onPageFinished(view, url);
            }
        });


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        webView.loadUrl(Constants.ABDOUT_US);
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle(getString(R.string.title_about), Enum.setNevigationIcon.MENU, false);
    }
}
