package com.citisquare.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;

import com.google.gson.Gson;
import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.pojo.response.RateReviewData;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by hlink56 on 31/1/17.
 */

public class RateAndReviewDetailFragment extends BaseFragment {

    @BindView(R.id.RatingOnRateReviewDetails)
    RatingBar RatingOnRateReviewDetails;
    @BindView(R.id.commentTextView)
    CustomTextView commentTextView;

    Bundle bundleString;
    @BindView(R.id.nameRateAndReviewDetails)
    CustomTextView nameRateAndReviewDetails;
    @BindView(R.id.reviewDetailsReview)
    CustomTextView reviewDetailsReview;
    @BindView(R.id.reviewDetailsTitle)
    CustomTextView reviewDetailsTitle;

    RateReviewData rateReviewData;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.review_detail_frag, container, false);
        ButterKnife.bind(this, view);

        bundleString = getArguments();

        if (bundleString != null) {

            rateReviewData = new Gson().fromJson(bundleString.getString("data"), RateReviewData.class);
            String name = rateReviewData.getFirstName() + " " + rateReviewData.getLastName();
            nameRateAndReviewDetails.setText(name);
            reviewDetailsTitle.setText(rateReviewData.getTitle());
            RatingOnRateReviewDetails.setRating(Float.parseFloat(rateReviewData.getRate()));
            reviewDetailsReview.setText(rateReviewData.getReview());
        }

        return view;
    }

    @OnClick(R.id.nameRateAndReviewDetails)
    public void nameClicked(){
        ProfileFragment profileFragment = new ProfileFragment();
        Bundle bundle = new Bundle();
        bundle.putString("profile", "other");
        ((MainActivity) getActivity()).setProfileUserId(rateReviewData.getUserId());
        profileFragment.setArguments(bundle);
        ((MainActivity) getActivity()).setSelectionPostion(0);
        ((MainActivity) getActivity()).setFragment(profileFragment, false, "");
    }
    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle(getString(R.string.review_details), Enum.setNevigationIcon.BACK, true);
    }
}
