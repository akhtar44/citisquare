package com.citisquare.app.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.MessageAdapter;
import com.citisquare.app.interfaces.Enum;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by hlink56 on 16/7/16.
 */
public class MessagesFragment extends BaseFragment implements ViewPager.OnPageChangeListener {

    @BindView(R.id.message_inbox)
    TextView messageInbox;
    @BindView(R.id.message_sent)
    TextView messageSent;
    @BindView(R.id.messageViewPager)
    ViewPager messageViewPager;
    @BindView(R.id.linearLayoutCommumities)
    LinearLayout linearLayoutCommumities;


    MessageAdapter messageAdapter;
    List<Fragment> fragmentList;
    int pos;
    InboxFragment inboxFragment = new InboxFragment();
    InboxFragment sentFragment = new InboxFragment();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_message_viewpager, container, false);
        ButterKnife.bind(this, view);

        messageInbox.setBackgroundResource(R.drawable.left_tab_selector);
        messageInbox.setTextColor(Color.WHITE);

        fragmentList = new ArrayList<>();
        fragmentList.add(inboxFragment);
        fragmentList.add(sentFragment);

        messageAdapter = new MessageAdapter(getChildFragmentManager(), fragmentList);
        messageViewPager.setAdapter(messageAdapter);
        messageViewPager.addOnPageChangeListener(this);


        return view;
    }

    @OnClick(R.id.message_inbox)
    public void messageInboxClicked() {
        messageViewPager.setCurrentItem(0);
    }

    @OnClick(R.id.message_sent)
    public void messageSentClicked() {
        messageViewPager.setCurrentItem(1);
    }

    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle(getString(R.string.message), Enum.setNevigationIcon.BACK_COMPOSE, true);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position == 0) {
            messageInbox.setBackgroundResource(R.drawable.left_tab_selector);
            messageInbox.setTextColor(Color.WHITE);
            messageSent.setBackgroundResource(0);
            messageSent.setTextColor(getResources().getColor(R.color.dark_blue));
                    /*MyCommunityFragment fragment = (MyCommunityFragment) pagerAdapter.getItem(0);
                    fragment.myCommunity();*/

        } else if (position == 1) {
            messageSent.setBackgroundResource(R.drawable.right_tab_selector);
            messageSent.setTextColor(Color.WHITE);
            messageInbox.setBackgroundResource(0);
            messageInbox.setTextColor(getResources().getColor(R.color.dark_blue));
                    /*OtherCommunityFragment fragment = (OtherCommunityFragment) pagerAdapter.getItem(1);
                    fragment.otherCommunity(((MainActivity) getActivity()).getRadius());*/
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}

