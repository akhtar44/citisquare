package com.citisquare.app.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.gson.Gson;
import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.async.MultipleRouteFetchTask;
import com.citisquare.app.async.RouteParserTask;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.controls.MapWrapperLayout;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.LocationCallback;
import com.citisquare.app.listener.OnInfoWindowElemTouchListener;
import com.citisquare.app.pojo.response.BranchData;
import com.citisquare.app.pojo.response.BranchWrapper;
import com.citisquare.app.utils.Debugger;
import com.citisquare.app.utils.LocationHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import serviceCalling.builder.KeyValuePair;
import serviceCalling.helper.GetRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 17/8/16.
 */
public class OtherMapPathFragment extends BaseFragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    final static int ZOOM_PADDING = 100;
    public OnInfoWindowElemTouchListener infoButtonListenerMail;
    @BindView(R.id.otherMap)
    MapView otherMap;
    @BindView(R.id.mapProfileWrapperLayout)
    MapWrapperLayout mapProfileWrapperLayout;
    LatLng location;
    @BindView(R.id.framelayoutPlaceholder)
    FrameLayout framelayoutPlaceholder;
    List<BranchData> branchData;
    HashMap<LatLng, String> latLngStringHashMap;
    Bundle bundle;
    String bundleString = "";
    private GoogleMap googleMap;
    private Polyline polyline;
    private LocationCallback locationCallback;
    private LocationHelper locationHelper;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;
        try {
            MapsInitializer.initialize(getActivity());
            view = inflater.inflate(R.layout.layout_other_map_path, container, false);
            //view = inflater.inflate(R.layout.map_tip_path_layout, container, false);
            ButterKnife.bind(this, view);
            otherMap.onCreate(savedInstanceState);
            bundle = getArguments();
            if (bundle != null) {
                bundleString = bundle.getString("from");
            }

            latLngStringHashMap = new HashMap<>();
            if (otherMap != null) {
                otherMap.getMapAsync(this);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        //markerList = new ArrayList<>();
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle("default", Enum.setNevigationIcon.BACK, true);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        if (googleMap != null) {
            this.googleMap = googleMap;
            setUpMap();

            /*location = new LatLng(25.2048, 55.2708);
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    location, 9));
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(location)      // Sets the center of the map to location user
                    .zoom(9)// Sets the zoom
                    .build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            drawMarker(location, "CURRENT", "", R.drawable.current_location);*/
            /*drawMarker(new LatLng(25.05, 56.00), "", "", R.drawable.job);
            drawMarker(new LatLng(25.10, 55.52), "", "", R.drawable.events);
            drawMarker(new LatLng(25.20, 55.57), "", "", R.drawable.shoppings);
            drawMarker(new LatLng(25.08, 55.80), "", "", R.drawable.realestate);
            drawMarker(new LatLng(25.64, 55.888), "", "", R.drawable.bank);
            drawMarker(new LatLng(25.74, 55.87), "", "", R.drawable.job);
            drawMarker(new LatLng(25.18, 55.88), "", "", R.drawable.events);
            drawMarker(new LatLng(24.92, 55.888), "", "", R.drawable.shoppings);
            drawMarker(new LatLng(25.20, 55.55), "", "", R.drawable.realestate);
            drawMarker(new LatLng(25.24, 55.60), "", "", R.drawable.bank);
            drawMarker(new LatLng(25.6, 55.87), "", "", R.drawable.job);
            drawMarker(new LatLng(25.68, 55.87), "", "", R.drawable.realestate);*/

        }
    }


    public Marker drawMarker(LatLng place, String title, String content, int icon) {
        MarkerOptions markerOptions = new MarkerOptions()
                .position(place)
                .infoWindowAnchor(0.5f, -0.0f)
                .title(title)
                .snippet(content)
                .icon(BitmapDescriptorFactory.fromResource(icon));

        Marker mMarker = googleMap.addMarker(markerOptions);


        return mMarker;
    }


    public void setUpMap() {
        /*if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Debugger.e("setUpMap method: permission require................");
            return;
        }*/
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.setOnMarkerClickListener(this);
        //googleMap.setInfoWindowAdapter(new InfoWindowAdapter(getContext()));
        googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(final Marker marker) {
                View view = LayoutInflater.from(getContext()).inflate(R.layout.map_tip_path_layout, null);
                ViewHolder holder = new ViewHolder(view);

                //holder.jobPost.setText(parentMapWrapper.getData().get(marker.getPosition()).getFirstName());
                if (latLngStringHashMap != null) {
                    final BranchData branchData = new Gson().fromJson(latLngStringHashMap.get(marker.getPosition()), BranchData.class);
                    if (branchData != null) {
                        holder.tipTitle.setText(branchData.getTitle());
                        holder.address.setText(branchData.getAddress());
                       /* userId = parentMapData.getId();
                        String name = parentMapData.getFirstName() + " " + parentMapData.getLastName();
                        holder.jobPost.setText(name);

                        ((MainActivity) getActivity()).picasso.load(parentMapData.getProfileImageThumb()).into(holder.jobImageView, new InfoWindowRefresher(marker));
                        *//*new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                ((MainActivity) getActivity()).setCircularImage(holder.jobImageView, parentMapData.getProfileImageThumb());
                            }
                        }, 50);*//*
                        holder.locationTab.setText(parentMapData.getCityName());*/
                    }
                }
                mapProfileWrapperLayout.setMarkerWithInfoWindow(marker, view);
                if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) ==
                        Configuration.SCREENLAYOUT_SIZE_XLARGE) {
                    // on a x-large screen device ...
                    Debugger.e("extra large screen");
                    mapProfileWrapperLayout.init(googleMap, getPixelsFromDp(getActivity(), 35 + 50));
                } else if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) ==
                        Configuration.SCREENLAYOUT_SIZE_LARGE) {
                    // on a large screen device ...
                    Debugger.e("large screen");
                    mapProfileWrapperLayout.init(googleMap, getPixelsFromDp(getActivity(), 35 + 50));
                } else {
                    Debugger.e("normal screen");
                    mapProfileWrapperLayout.init(googleMap, getPixelsFromDp(getActivity(), 35 + 20));
                }

                OtherMapPathFragment.this.infoButtonListenerMail = new OnInfoWindowElemTouchListener(holder.tipGetPath, 0, 0) {
                    @Override
                    protected void onClickConfirmed(View v, Marker m) {
                        /*drawPath(location, marker.getPosition());
                        final LatLngBounds.Builder builder = new LatLngBounds.Builder();
                        builder.include(marker.getPosition());
                        LatLngBounds latLngBounds = builder.build();
                        Debugger.e("SOURCE:::" + location.toString() + "::: DESTINATION:::" + marker.getPosition().toString());
                        zoomMapInitial(location, builder, ZOOM_PADDING, googleMap, false);
                        marker.hideInfoWindow();*/
                        Double latSource = location.latitude;
                        Double lngDouble = location.longitude;

                        Double latMarker = marker.getPosition().latitude;
                        Double lngMarker = marker.getPosition().longitude;

                        /*= new Intent(android.content.Intent.ACTION_VIEW,
                                Uri.parse("http://maps.google.com/maps?saddr=20.344,34.34&daddr=20.5666,45.345"));*/
                        /*Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                                Uri.parse("http://maps.google.com/maps?saddr=" + latSource + "," + lngDouble + "&daddr=" + latMarker + "," + lngMarker));

                        */
                      /*  Uri.parse("http://maps.google.com/maps?saddr=" + LocationService.CurrentLatitude + "," + LocationService.CurrentLongitude +
                                "&daddr=" + DestinationLatitude + "," + DestinationLongitude + ""));
                        startActivity(intent);*/
                        Intent intent = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("http://maps.google.com/maps?saddr=" + latSource + "," + lngDouble +
                                        "&daddr=" + latMarker + "," + lngMarker + ""));
                        startActivity(intent);
                    }
                };
                holder.tipGetPath.setOnTouchListener(infoButtonListenerMail);

                return view;
            }

            @Override
            public View getInfoContents(Marker marker) {
                return null;
            }
        });
    }


    public void branchApiCall() {

        List<KeyValuePair> branchKeyValuePair = new ArrayList<>();
        if (bundleString != null) {
            if (bundleString.equals("own")) {
                branchKeyValuePair.add(new KeyValuePair("user_id", ((MainActivity) getActivity()).userId()));
            } else {
                branchKeyValuePair.add(new KeyValuePair("user_id", ((MainActivity) getActivity()).getProfileUserId()));
            }
        }

        //new GetRequestHelper().pingToRequest(Constants.BRANCH_LIST, "image/*", branchKeyValuePair ,((MainActivity)getActivity()).getHeader(),new BranchWrapper(),new TaskCompleteListener<>());
        new GetRequestHelper<BranchWrapper>().pingToRequest(Constants.BRANCH_LIST_MAP
                , branchKeyValuePair
                , ((MainActivity) getActivity()).getHeader()
                , new BranchWrapper()
                , new TaskCompleteListener<BranchWrapper>() {
                    @Override
                    public void onSuccess(BranchWrapper branchWrapper) {
                        loadMapData(branchWrapper);
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, BranchWrapper branchWrapper) {
                        if (branchWrapper != null)
                            loadMapData(branchWrapper);
                    }
                });
    }

    private void loadMapData(BranchWrapper branchWrapper) {
        if (branchWrapper != null) {

            if (getActivity() != null)
                location = ((MainActivity) getActivity()).getLatLng();
            else
                location = new LatLng(0, 0);

            /*googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    location, 9));
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(location)      // Sets the center of the map to location user
                    .zoom(9)// Sets the zoom
                    .build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));*/
            drawMarker(location, "CURRENT", "", R.drawable.current_location);

            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(location);
            if (branchWrapper.getData() != null) {
                branchData = branchWrapper.getData();
                latLngStringHashMap.clear();
                for (int i = 0; i < branchData.size(); i++) {
                    LatLng latLng = new LatLng(Double.parseDouble(branchData.get(i).getLatitude()), Double.parseDouble(branchData.get(i).getLongitude()));
                    drawMarker(latLng, "", "", R.drawable.job);
                    builder.include(latLng);
                    latLngStringHashMap.put(latLng, new Gson().toJson(branchData.get(i)));
                }
            }
            LatLngBounds bounds = builder.build();
            int padding = 100; // offset from edges of the map in pixels
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            googleMap.animateCamera(cu);
        }
    }

    protected void zoomMapInitial(LatLng finalPlace
            , LatLngBounds.Builder bc
            , int padding
            , GoogleMap map
            , boolean isMapPadding) {


        try {
            int width = getResources().getDisplayMetrics().widthPixels;
            int height = getResources().getDisplayMetrics().heightPixels;

            if (isMapPadding) {
                int layoutHeight = framelayoutPlaceholder.getHeight();
                int layoutWidth = framelayoutPlaceholder.getWidth();
                googleMap.setPadding(0, layoutWidth, 0, layoutHeight);
            }

            bc.include(finalPlace);
            map.setPadding(0, 0, 0, 0);
            map.animateCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), padding));


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void drawPath(LatLng start, LatLng end) {
        MultipleRouteFetchTask routeFetchTask = new MultipleRouteFetchTask(googleMap, new RouteParserTask.ParseCallback() {
            @Override
            public void onRouteComplete(Polyline polyline) {
                if (polyline != null) {
                    clearPolyline(polyline);
                } else {
                    Toast.makeText(getContext(), "Directions are not available from this location", Toast.LENGTH_SHORT).show();
                }
            }
        });

        routeFetchTask.setPolyLineColor(ContextCompat.getColor(getContext(), R.color.dark_blue));
        routeFetchTask.setCurrent(start);
        routeFetchTask.setDestination(end);
        routeFetchTask.execute();
    }

    public void clearPolyline(Polyline polyline) {
        if (this.polyline != null)
            this.polyline.remove();
        this.polyline = polyline;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (otherMap != null)
            otherMap.onResume();
        startlocationlisting(new LocationCallback() {
            @Override
            public void onLocationError() {
            }

            @Override
            public void onLocationRecive(Location location) {
                if (getActivity() != null) {
                    ((MainActivity) getActivity()).setLatLng(new LatLng(location.getLatitude(), location.getLongitude()));
                    branchApiCall();
                }
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (otherMap != null)
            otherMap.onDestroy();
    }

    public void startlocationlisting(LocationCallback locationCallback) {
        this.locationCallback = locationCallback;
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    ) {
                requestPermissions(Constants.ACCESS_FINE_LOCATION, Constants.LOCATIONREQUESTCODE);
            } else {
                startLisning();
            }
        } else startLisning();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Debugger.e("PERMISSION ::" + requestCode);
        switch (requestCode) {
            case Constants.LOCATIONREQUESTCODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLisning();
                } else {

                }
                return;
            }
        }
    }

    private void startLisning() {
        locationHelper = new LocationHelper(this, new LocationHelper.ProvideLocation() {
            @Override
            public void currentLocation(final Location location) {
                locationCallback.onLocationRecive(location);
            }

            @Override
            public void onError() {
                locationCallback.onLocationError();
            }
        });
    }

    @Override
    public void onLowMemory() {
        if (otherMap != null)
            otherMap.onLowMemory();
        super.onLowMemory();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
        //googleMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(marker.getPosition().latitude + (double) 90 / Math.pow(2, 9), marker.getPosition().longitude)));
        //new LatLng(arg0.getPosition().latitude + (double)90/Math.pow(2, zoom), arg0.getPosition().longitude), zoom
        if (marker.getTitle().equals("CURRENT")) {
        } else {
            marker.showInfoWindow();
        }
        return true;
    }

    public int getPixelsFromDp(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    class ViewHolder {
        @BindView(R.id.tip_title)
        CustomTextView tipTitle;
        @BindView(R.id.address)
        CustomTextView address;
        @BindView(R.id.tipGetPath)
        CustomTextView tipGetPath;
        @BindView(R.id.maintipPath)
        LinearLayout maintipPath;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

}