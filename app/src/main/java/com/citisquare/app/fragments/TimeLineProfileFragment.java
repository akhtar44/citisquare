package com.citisquare.app.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.NpaGridLayoutManager;
import com.citisquare.app.adapter.TimelineAdapter;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.pojo.response.Like;
import com.citisquare.app.pojo.response.OfferData;
import com.citisquare.app.pojo.response.OfferWrapper;
import com.citisquare.app.utils.Debugger;
import com.citisquare.app.utils.RequestParameter;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 7/6/16.
 */
public class TimeLineProfileFragment extends BaseFragment {

    public static final String TAG = "NEWS_PROFILE";
    TimelineAdapter newsProfileAdapter;
    ArrayList<OfferData> quickInfoDatas;

    List<String> list;
    @BindView(R.id.newsRecyclerView)
    RecyclerView newsRecyclerView;
    NpaGridLayoutManager gridLayoutManager;
    int pageno = 1;

    boolean isPagination;
    Bundle bundleNews;
    RelativeLayout relative_empty;
    String bundleString;

    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            view = inflater.inflate(R.layout.news_profile_layout, container, false);
            relative_empty = (RelativeLayout) view.findViewById(R.id.relative_empty);
            ButterKnife.bind(this, view);


            pageno = 1;
            bundleNews = getArguments();
            if (bundleNews != null) {
                bundleString = bundleNews.getString("FROM");
            }
            quickInfoDatas = new ArrayList<>();

           // newsListApiCall(pageno);
            if (quickInfoDatas.size() > 0) {
                relative_empty.setVisibility(View.GONE);
            } else {
                relative_empty.setVisibility(View.VISIBLE);
            }

            newsProfileAdapter = new TimelineAdapter(getContext(), quickInfoDatas, new ItemEventListener<String>() {
                @Override
                public void onItemEventFired(String pos, String userId, final String actualPos) {
                    switch (pos) {
                        case "map":
                            ((MainActivity) getActivity()).setProfileUserId(userId);
                            ((MainActivity) getActivity()).setFragment(new OtherMapPathFragment(), false, "");
                            break;
                        case "share":
                            OfferData offerData = new Gson().fromJson(userId, OfferData.class);
                            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                            sharingIntent.putExtra(Intent.EXTRA_TEXT, offerData.getDescription() + "  " + offerData.getUrl());
                            sharingIntent.setType("text/plain");
                            startActivity(Intent.createChooser(sharingIntent, "Share application using"));
                            break;
                        case "Child":
                            DetailsHomeFragment detailsHomeFragment = new DetailsHomeFragment();
                            Bundle bundleDetails = new Bundle();
                            bundleDetails.putString("data", userId);
                            bundleDetails.putString("details", "News");
                            detailsHomeFragment.setArguments(bundleDetails);
                            ((MainActivity) getActivity()).setFragment(detailsHomeFragment, false, "");
                            //((MainActivity) getActivity()).setFragment(new DetailsHomeFragment());
                            break;
                        case "comment":
                            if (((MainActivity) getActivity()).getUserForWholeApp().equals(Enum.setUser.GUEST)) {
                                ((MainActivity) getActivity()).messageAlert(getString(R.string.message_Guest));
                            } else {
                                CommentFragment commentFragment = new CommentFragment();

                                commentFragment.setCommentListener(new CommentFragment.CommentListener() {
                                    @Override
                                    public void commentCount(int count) {
                                        quickInfoDatas.get(Integer.parseInt(actualPos)).setCommentCount(count);
                                        newsProfileAdapter.notifyDataSetChanged();
                                    }
                                });
                                Bundle bundleComment = new Bundle();
                                bundleComment.putString("postId", userId);
                                bundleComment.putString("FROM", "T");
                                commentFragment.setArguments(bundleComment);
                                ((MainActivity) getActivity()).setFragment(commentFragment, false, "");
                            }
                            break;
                        case "otherProfile":
                            if (userId.equals(((MainActivity) getActivity()).userId())) {
                                ProfileFragment profileFragment = new ProfileFragment();
                                Bundle bundle = new Bundle();
                                bundle.putString("profile", "own");
                                ((MainActivity) getActivity()).setProfileUserId(userId);
                                profileFragment.setArguments(bundle);
                                ((MainActivity) getActivity()).setSelectionPostion(0);
                                ((MainActivity) getActivity()).setFragment(profileFragment, false, "");
                            } else {
                                ProfileFragment profileFragment = new ProfileFragment();
                                Bundle bundle = new Bundle();
                                bundle.putString("profile", "other");
                                ((MainActivity) getActivity()).setProfileUserId(userId);
                                profileFragment.setArguments(bundle);
                                ((MainActivity) getActivity()).setSelectionPostion(0);
                                ((MainActivity) getActivity()).setFragment(profileFragment, false, "");
                                //childFragmentReplacement(profileFragment, "child");
                            }
                            break;
                        case "Video":
                            PlayVideoFragment playVideoFragment = new PlayVideoFragment();
                            Bundle playBundle = new Bundle();
                            playBundle.putString("video", userId);
                            playVideoFragment.setArguments(playBundle);
                            ((MainActivity) getActivity()).setFragment(playVideoFragment, false, "");
                            break;

                        case "Follow":
                            doFollowApiCall(userId, "follow");
                            break;
                        case "Unfollow":
                            doFollowApiCall(userId, "unfollow");
                            break;
                        case "Like":
                            likeApiCall(userId);
                            break;
                    }
                }
            });
            gridLayoutManager = new NpaGridLayoutManager(getActivity(), 1);
            newsRecyclerView.setLayoutManager(gridLayoutManager);
            newsRecyclerView.setAdapter(newsProfileAdapter);


            newsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);

                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    try {
                        if (isPagination) {
                            int visibleItemCount = recyclerView.getChildCount();
                            int totalItemCount = gridLayoutManager.getItemCount();
                            int firstVisibleItemPosition = gridLayoutManager.findFirstVisibleItemPosition();
                            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount) {
                                isPagination = false;
                                newsListApiCall(++pageno);
                            }
                        }
                    } catch (IndexOutOfBoundsException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        return view;

    }

    public void newsApiCall() {
        pageno = 1;
        newsListApiCall(pageno);
    }

    public void likeApiCall(String postId) {
        new PostRequestHelper<Like>().pingToRequest(Constants.LIKE
                , null
                , null
                , new RequestParameter().getHashMapLike(getLikeHashMap(postId))
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new Like()
                , new TaskCompleteListener<Like>() {
                    @Override
                    public void onSuccess(Like mObject) {
                        if (mObject.getStatus() == 1)
                            Debugger.e("LIKE");

                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, Like like) {
                        if (like != null) {
                            if (like.getStatus() == -1)
                                ((MainActivity) getActivity()).invalidToken();
                        }
                    }
                });
    }

    public void doFollowApiCall(final String userId, final String status) {
        new PostRequestHelper<Like>().pingToRequest(Constants.DO_FOLLOW
                , null
                , null
                , new RequestParameter().getHashMapForDoFollow(getDoFollowMap(userId, status))
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new Like()
                , new TaskCompleteListener<Like>() {
                    @Override
                    public void onSuccess(Like mObject) {
                        if (mObject.getStatus() == 1)
                            follow(userId, status);
                        Debugger.e("FOLLOW SUCCESSFULLY");
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, Like like) {
                        if (like != null) {
                            if (like.getStatus() == -1)
                                ((MainActivity) getActivity()).invalidToken();
                        }
                    }
                });
    }

    public void follow(String userId, String status) {
        try {
            for (int i = 0; i < quickInfoDatas.size(); i++) {
                if (quickInfoDatas.get(i).getUserId().equals(userId)) {
                    if (status.equals("follow"))
                        quickInfoDatas.get(i).setIsFollowing(1);
                    else
                        quickInfoDatas.get(i).setIsFollowing(0);
                }
            }
            newsProfileAdapter.notifyDataSetChanged();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public HashMap<String, String> getLikeHashMap(String postId) {
        HashMap<String, String> likeHashMap = new HashMap<>();
        try {
            likeHashMap.put("post_id", postId);
            likeHashMap.put("status", "1");
            likeHashMap.put("flag", "T");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return likeHashMap;
    }

    public HashMap<String, String> getDoFollowMap(String userId, String status) {
        HashMap<String, String> hashMap = new HashMap<>();
        try {
            hashMap.put("follower_user_id", userId);
            if (status.equals("follow"))
                hashMap.put("status", "1");
            else
                hashMap.put("status", "0");
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        return hashMap;
    }


    public void newsListApiCall(final int pageno) {
        try {
            new PostRequestHelper<OfferWrapper>().pingToRequest(Constants.TIMELINE_LIST
                    , null
                    , null
                    , new RequestParameter().getHashMapForNewsApi(getHashMapNews(pageno))
                    , ((MainActivity) getActivity()).getParamWholeParameters()
                    , new OfferWrapper()
                    , new TaskCompleteListener<OfferWrapper>() {
                        @Override
                        public void onSuccess(OfferWrapper offerWrapper) {
                            isPagination = true;

                            if (pageno == 1)
                                quickInfoDatas.clear();

                            if (offerWrapper.getStatus() == 1) {
                                setNewsData(offerWrapper.getData());
                            } else {
                                if (pageno == 1)
                                    ((MainActivity) getActivity()).messageAlert(offerWrapper.getMessage());
                            }
                        }

                        @Override
                        public void onFailure(ExceptionType exceptions, OfferWrapper offerWrapper) {
                            isPagination = false;
                            if (pageno == 1)
                                quickInfoDatas.clear();

                            setPage();
                        }
                    });
        }catch (Exception ex)
        {
            ex.printStackTrace();
        }


    }

    public void setPage() {
        pageno = 1;
    }

    public HashMap<String, String> getHashMapNews(int pageno) {
        HashMap<String, String> hashMapNews = new HashMap<>();
        try {

            hashMapNews.put("type", bundleString);
            hashMapNews.put("page", pageno + "");
            hashMapNews.put("user_id", ((MainActivity) getActivity()).getProfileUserId());
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        return hashMapNews;
    }

    public void setNewsData(List<OfferData> offerDatas) {
        try {
            quickInfoDatas.addAll(offerDatas);
            newsProfileAdapter.notifyDataSetChanged();
            if (quickInfoDatas.size() > 0) {
                relative_empty.setVisibility(View.GONE);
            } else {
                relative_empty.setVisibility(View.VISIBLE);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    /* @OnClick(R.id.textViewLogin)
     public void onChildEnter() {
         childFragmentReplacement(new ChildHomeFragment(), "child");

     }
 */
    @Override
    public void setHeaderTitle() {

    }
}
