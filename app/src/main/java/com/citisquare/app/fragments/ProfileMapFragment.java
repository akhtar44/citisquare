package com.citisquare.app.fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.controls.MapWrapperLayout;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.LocationCallback;
import com.citisquare.app.utils.CircleTransformation;
import com.citisquare.app.utils.Debugger;
import com.citisquare.app.utils.LocationHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 25/7/16.
 */
public class ProfileMapFragment extends BaseFragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    final static int ZOOM_PADDING = 250;
    @BindView(R.id.map)
    MapView mapView;
    Bundle bundleMap;
    String bundleString = "";
    @BindView(R.id.framelayoutPlaceholder)
    FrameLayout framelayoutPlaceholder;
    @BindView(R.id.mapWrapperLayout)
    MapWrapperLayout mapWrapperLayout;
    private GoogleMap googleMap;


    private LocationCallback locationCallback;
    private LocationHelper locationHelper;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;
        try {
            MapsInitializer.initialize(getActivity());
            view = inflater.inflate(R.layout.map_layout, container, false);
            ButterKnife.bind(this, view);
            mapView.onCreate(savedInstanceState);

            if (mapView != null) {
                mapView.getMapAsync(this);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        //markerList = new ArrayList<>();
        ButterKnife.bind(this, view);
        bundleMap = getArguments();
        if (bundleMap != null) {
            if (bundleMap.getString("MAP") != null) {
                bundleString = bundleMap.getString("MAP");
            }
        }
        return view;
    }


    @Override
    public void setHeaderTitle() {
        ((MainActivity) getActivity()).setTitle("default", Enum.setNevigationIcon.BACK_CITY, true);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        if (googleMap != null) {
            this.googleMap = googleMap;
            setUpMap();

            LatLng locationStart = null;

            if (getActivity() != null) {
                locationStart = ((MainActivity) getActivity()).getLatLng();
            } else {
                locationStart = new LatLng(0, 0);
            }


            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    locationStart, 11));
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(locationStart)      // Sets the center of the map to location user
                    .zoom(11)// Sets the zoom
                    .build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            drawMarker(locationStart, "CURRENT", "", R.drawable.current_location);
        }
    }


    public Marker drawMarker(LatLng place, String title, String content, int icon) {
        MarkerOptions markerOptions = new MarkerOptions()
                .position(place)
                .infoWindowAnchor(0.5f, -0.0f)
                .title(title)
                .snippet(content)
                .icon(BitmapDescriptorFactory.fromResource(icon));


        Marker mMarker = googleMap.addMarker(markerOptions);
        return mMarker;
    }


    public void setUpMap() {
        /*if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Debugger.e("setUpMap method: permission require................");
            return;
        }*/
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.setOnMarkerClickListener(this);
        googleMap.setInfoWindowAdapter(new InfoWindowAdapter(getContext()));
    }


    @Override
    public void onResume() {
        super.onResume();
        startlocationlisting(new LocationCallback() {
            @Override
            public void onLocationError() {

            }

            @Override
            public void onLocationRecive(Location location) {
                if (getActivity() != null)
                    ((MainActivity) getActivity()).setLatLng(new LatLng(location.getLatitude(), location.getLongitude()));
            }
        });
        if (mapView != null)
            mapView.onResume();


    }

    public void startlocationlisting(LocationCallback locationCallback) {
        this.locationCallback = locationCallback;
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    ) {
                requestPermissions(Constants.ACCESS_FINE_LOCATION, Constants.LOCATIONREQUESTCODE);
            } else {
                startLisning();
            }
        } else startLisning();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Debugger.e("PERMISSION ::" + requestCode);
        switch (requestCode) {
            case Constants.LOCATIONREQUESTCODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLisning();
                } else {

                }
                return;
            }
        }
    }

    private void startLisning() {
        locationHelper = new LocationHelper(this, new LocationHelper.ProvideLocation() {
            @Override
            public void currentLocation(final Location location) {
                locationCallback.onLocationRecive(location);
            }

            @Override
            public void onError() {
                locationCallback.onLocationError();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mapView != null)
            mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        if (mapView != null)
            mapView.onLowMemory();
        super.onLowMemory();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
        //googleMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(marker.getPosition().latitude + (double) 90 / Math.pow(2, 11), marker.getPosition().longitude)));
        //new LatLng(arg0.getPosition().latitude + (double)90/Math.pow(2, zoom), arg0.getPosition().longitude), zoom
        if (marker.getTitle().equals("CURRENT")) {

        } else {
            marker.showInfoWindow();
        }
        return true;
    }


    public class InfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private Context context;

        public InfoWindowAdapter(Context context) {
            this.context = context;
        }

        @Override
        public View getInfoWindow(final Marker marker) {
            View view = LayoutInflater.from(context).inflate(R.layout.map_tip_popup, null);
            ViewHolder holder = new ViewHolder(view);


            holder.bind(marker);
            return view;
        }

        @Override
        public View getInfoContents(Marker marker) {
            if (marker != null &&
                    marker.isInfoWindowShown()) {
                //marker.hideInfoWindow();
                marker.showInfoWindow();
            }
            return null;
        }

        public class ViewHolder {
            @BindView(R.id.jobImageView)
            ImageView jobImageView;
            @BindView(R.id.jobPost)
            TextView jobPost;
            @BindView(R.id.jobCompanyName)
            TextView jobCompanyName;
            @BindView(R.id.jobExperience)
            TextView jobExperience;
            @BindView(R.id.tipViewDetails)
            TextView tipViewDetails;

            public ViewHolder(View v) {
                ButterKnife.bind(this, v);
                ((MainActivity) getActivity()).picasso.load(R.drawable.zac_afron)
                        .transform(new CircleTransformation())
                        .into(jobImageView);
            }

            public void bind(final Marker marker) {
            }
        }
    }
}