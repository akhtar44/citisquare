package com.citisquare.app.dialog;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Navigation;
import com.citisquare.app.pojo.response.Like;
import com.citisquare.app.utils.Debugger;
import com.citisquare.app.utils.RequestParameter;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 2/6/16.
 */
public class ForgotDialog extends DialogFragment {
    @BindView(R.id.editTextEmail)
    EditText editTextEmail;
    @BindView(R.id.forgotPasswordSubmit)
    Button forgotPasswordSubmit;
    @BindView(R.id.forgotPasswordClose)
    ImageView forgotPasswordClose;
    private HashMap<String, String> forgotPassword;

    private Pattern pattern;
    private Matcher matcher;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_forgot_password, container, false);
        getDialog().requestWindowFeature(STYLE_NO_TITLE);
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        getDialog().setCanceledOnTouchOutside(true);
        ButterKnife.bind(this, view);

        pattern = Pattern.compile(Constants.englishOnly);
        forgotPasswordClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager im = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                im.hideSoftInputFromWindow(forgotPasswordClose.getWindowToken(), 0);
                dismiss();
            }
        });
        return view;
    }

    @OnClick(R.id.forgotPasswordSubmit)
    public void forgotPasswordSubmitClicked() {
        if (isValid()) {
            forgotPasswordCalledApi();
            InputMethodManager im = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(forgotPasswordSubmit.getWindowToken(), 0);
        }
    }

    public void forgotPasswordCalledApi() {
        if (getActivity() != null)
            ((MainActivity) getActivity()).startLoader();
        new PostRequestHelper<Like>().pingToRequest(Constants.FORGOT_PASSWORD
                , null
                , null
                , new RequestParameter().getHashMapForForgotPassword(getForgotPassword())
                , ((MainActivity) getActivity()).getHeader()
                , new Like()
                , new TaskCompleteListener<Like>() {
                    @Override
                    public void onSuccess(Like like) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        Debugger.e("like is called activity not");
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();

                            Debugger.e("like is called");
                            if (like.getStatus() == 1) {
                                ((MainActivity) getActivity()).messageAlert(like.getMessage());
                            } else if (like.getStatus() == 0) {
                                ((MainActivity) getActivity()).messageAlert(like.getMessage());
                            }
                            dismiss();
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, Like like) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).stopLoader();
                            ((MainActivity) getActivity()).messageAlert(like.getMessage());
                        }
                        dismiss();
                    }
                });
    }

    public void resultShow(boolean result, Like like) {
        if (getActivity() != null) {
            if (result) {
                ((MainActivity) getActivity()).stopLoader();

                Debugger.e("like is called");
                if (like.getStatus() == 1)
                    ((MainActivity) getActivity()).messageAlert(like.getMessage());
                else if (like.getStatus() == 0)
                    ((MainActivity) getActivity()).messageAlert(like.getMessage());

            } else {
                ((MainActivity) getActivity()).stopLoader();
                ((MainActivity) getActivity()).messageAlert(like.getMessage());
            }
        }
    }

    public boolean isValid() {
        if (!validEmail(editTextEmail.getText().toString())) {
            editTextEmail.requestFocus();
            return false;
        }
        return true;
    }

    public boolean validEmail(String email) {
        int length = email.trim().length();
        if (length <= 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterEmail));
            return false;
        }
        Pattern pattern = Pattern.compile(Navigation.EMAIL_REGEX);
        Matcher matcher = pattern.matcher(email);
        if (!(email.charAt(0) + "").matches("\\p{L}") || !matcher.matches()) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterValidEmailId));
            return false;
        }
        return true;
    }

    @Override
    public int show(FragmentTransaction transaction, String tag) {
        return super.show(transaction, tag);
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        if (manager.findFragmentByTag(tag) == null) {
            super.show(manager, tag);
        }
    }

    public HashMap<String, String> getForgotPassword() {
        HashMap<String, String> hashmapForgotPassword = new HashMap<>();
        hashmapForgotPassword.put("email", editTextEmail.getText().toString());
        return hashmapForgotPassword;
    }
}
