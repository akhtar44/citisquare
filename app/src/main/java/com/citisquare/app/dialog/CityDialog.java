package com.citisquare.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.CityAdapter;
import com.citisquare.app.interfaces.ItemEventListener;

import java.util.List;

/**
 * Created by hlink56 on 2/8/16.
 */
public class CityDialog {
    public static void openCityDialog(Context context, final ItemEventListener<String> stringItemEventListener) {
        final Dialog dialog = new Dialog(context);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setContentView(R.layout.layout_picker_dialog);
        List<com.citisquare.app.pojo.response.City> cityArrayList = ((MainActivity) context).cityArrayList();
        dialog.getWindow().setContentView(R.layout.category_selection_dialog);
        Button button = (Button) dialog.findViewById(R.id.ok);
        button.setVisibility(View.GONE);
        ListView cateGorySelection = (ListView) dialog.findViewById(R.id.cateGorySelection);
        CityAdapter countryAdapter;
        countryAdapter = new CityAdapter(context, R.layout.raw_dialog_category, cityArrayList, new ItemEventListener<String>() {
            @Override
            public void onItemEventFired(String s, String t2, String actualPos) {
                stringItemEventListener.onItemEventFired(s, t2, "");
                dialog.dismiss();
            }
        });
        cateGorySelection.setAdapter(countryAdapter);
        dialog.show();
    }
}
