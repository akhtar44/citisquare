package com.citisquare.app.dialog;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.citisquare.app.R;
import com.citisquare.app.interfaces.ImagePickerDialogListener;
import com.citisquare.app.utils.Utility;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by hlink16 on 28/9/15.
 */
public class PickerDialog extends DialogFragment {

    private static final int CAMERA_PERMISSION = 94;
    private static final int STORAGE_PERMISSION = 95;
    private static final int RESULT_LOAD_IMAGE = 93;
    private static final String[] permission = new String[]{Manifest.permission.CAMERA
            , Manifest.permission.READ_EXTERNAL_STORAGE
            , Manifest.permission.RECORD_AUDIO};
    private static final int REQUEST_IMAGE_AND_VIDEO = 96;

    @BindView(R.id.raw_profile_chooser_string_chooseFromGallery)
    ImageView rawProfileChooserStringChooseFromGallery;
    @BindView(R.id.raw_profile_chooser_string_take_photo)
    ImageView rawProfileChooserStringTakePhoto;
    String mediaPath;
    private ImagePickerDialogListener callback;
    private boolean isclicked;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.raw_profile_chooser, container, false);
        ButterKnife.bind(this, view);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        isclicked = false;
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == AppCompatActivity.RESULT_OK) {

            if (data != null) {

                Uri selectedUri = data.getData();
                String[] columns = {MediaStore.Images.Media.DATA,
                        MediaStore.Images.Media.MIME_TYPE};

                Cursor cursor = getActivity().getContentResolver().query(selectedUri, columns, null, null, null);

                if (cursor != null) {
                    cursor.moveToFirst();
                    int pathColumnIndex = cursor.getColumnIndex(columns[0]);
                    int mimeTypeColumnIndex = cursor.getColumnIndex(columns[1]);

                    String contentPath = cursor.getString(pathColumnIndex);
                    String mimeType = cursor.getString(mimeTypeColumnIndex);
                    cursor.close();

                    if (contentPath == null)
                        contentPath = Utility.getPath(getContext(), selectedUri);

                    this.mediaPath = contentPath;

                    if (mimeType.startsWith("image")) {
                        if (callback != null)
                            callback.getResultFromGallery(data, "image");
                    } else if (mimeType.startsWith("video")) {
                        //It's a video

                        if (mediaPath != null) {
                            if (callback != null)
                                callback.getResultFromGallery(data, "video");
                        }

                    } //else showMessage("Invalid media file");

                } //else showMessage("Invalid media file");


            } else {
                // show error or do nothing
            }
        }
        /*} else {
            isclicked = false;
            if (requestCode == RESULT_LOAD_IMAGE) {
                if (resultCode == Activity.RESULT_OK) {
                    if (requestCode == RESULT_LOAD_IMAGE) {
                        String path = data.getData().getPath();
                        if (path.contains("images")) {
                            if (data != null) {
                                if (callback != null) {
                                    callback.getResultFromGallery(data, "image");
                                }
                            }
                        } else if (path.contains("video")) {
                            //handle video
                            if (data != null) {
                                if (callback != null) {
                                    callback.getResultFromGallery(data, "video");
                                }
                            }
                        }
                    }
                } else {
                    if (getCallback() != null) {
                        Log.e(this.getClass().getName(), ":::: NOTHING SET IS CALLED :::::");
                        getCallback().nothingSet();
                    }
                }
            } else {

                if (getCallback() != null) {
                    Log.e(this.getClass().getName(), ":::: NOTHING SET IS CALLED :::::");
                    getCallback().nothingSet();
                }

            }
        }*/
        getDialog().dismiss();
    }


    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    private void checkCameraPermission() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(getContext(), Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(permission, CAMERA_PERMISSION);
            //((MainActivity) getActivity()).requestPermissions(permission, CAMERA_PERMISSION);
        } else {
            captureImage();
        }
    }

    public void checkStoragePermissions() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION);
        } else {
            selectImage();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case CAMERA_PERMISSION:
                if (grantResults.length == 3 && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    captureImage();
                    return;
                } else {
                    getActivity().finish();
                }
                break;
            case STORAGE_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    selectImage();
                    return;
                } else {
                    getActivity().finish();
                }
                break;
        }
    }

    public void captureImage() {
        if (callback != null) {
            callback.getResultFromCamera("", "");
        }
        getDialog().dismiss();
    }

    public void selectImage() {
        if (!isclicked) {
            Intent mediaChooser = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

            if (Build.VERSION.SDK_INT < 19) {
                mediaChooser.setType("image/* video/*");
            } else {
                mediaChooser.setType("*/*");
                mediaChooser.setAction(Intent.ACTION_GET_CONTENT);
            }
            startActivityForResult(mediaChooser, RESULT_LOAD_IMAGE);
            /*Intent mediaChooser = new Intent(Intent.ACTION_GET_CONTENT);
            mediaChooser.setType("video*//*, images*//*");
            startActivityForResult(mediaChooser, RESULT_LOAD_IMAGE);*/
        }
        isclicked = true;
    }

    public ImagePickerDialogListener getCallback() {
        return callback;
    }

    public void setCallback(ImagePickerDialogListener callback) {
        this.callback = callback;
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        if (manager.findFragmentByTag(tag) == null) {
            super.show(manager, tag);
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    @OnClick(R.id.raw_profile_chooser_string_take_photo)
    public void onCameraClicked() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkCameraPermission();
        } else {
            captureImage();
        }
    }

    @OnClick(R.id.raw_profile_chooser_string_chooseFromGallery)
    public void onGalleryClicked() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkStoragePermissions();
        } else {
            selectImage();
        }
    }
}
