package com.citisquare.app.dialog;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.controls.CustomButton;
import com.citisquare.app.controls.CustomEditText;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.pojo.response.Like;

import java.util.HashMap;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 29/12/16.
 */

public class ChangePasswordDialog extends DialogFragment {

    @BindView(R.id.changePasswordClose)
    ImageView changePasswordClose;
    @BindView(R.id.changePasswordOldPassword)
    CustomEditText changePasswordOldPassword;
    @BindView(R.id.changePasswordNewPassword)
    CustomEditText changePasswordNewPassword;
    @BindView(R.id.changePasswordConfirmPassword)
    CustomEditText changePasswordConfirmPassword;
    @BindView(R.id.changePasswordSave)
    CustomButton changePasswordSave;
    private HashMap<String, String> forgotPassword;

    private Pattern pattern;
    private Pattern passwordPattern;
    private Matcher matcher;
    private Matcher password;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_change_password, container, false);
        getDialog().requestWindowFeature(STYLE_NO_TITLE);
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        getDialog().setCanceledOnTouchOutside(true);
        ButterKnife.bind(this, view);
        passwordPattern = Pattern.compile(Constants.passwordRegex);

        pattern = Pattern.compile(Constants.englishOnly);
        changePasswordClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager im = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                im.hideSoftInputFromWindow(changePasswordClose.getWindowToken(), 0);
                dismiss();
            }
        });
        return view;
    }

    @OnClick(R.id.changePasswordSave)
    public void forgotPasswordSubmitClicked() {
        if (isValid()) {
            //  forgotPasswordCalledApi();
            InputMethodManager im = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(changePasswordSave.getWindowToken(), 0);
            changePasswordApiCall();
        /* dismiss();*/
        }
    }

    private void changePasswordApiCall() {
        if (getActivity() != null)
            ((MainActivity) getActivity()).startLoader();

        new PostRequestHelper<Like>().pingToRequest(Constants.CHANGE_PASSWORD, null, null, getHashMapForPassword(), ((MainActivity) getActivity()).getParamWholeParameters(), new Like(), new TaskCompleteListener<Like>() {
            @Override
            public void onSuccess(Like mObject) {
                if (getActivity() != null)
                    ((MainActivity) getActivity()).stopLoader();

                if (mObject.getStatus() == 1) {
                    ((MainActivity) getActivity()).messageAlert(mObject.getMessage());
                    dismiss();
                }
            }

            @Override
            public void onFailure(ExceptionType exceptions, Like like) {
                if (getActivity() != null)
                    ((MainActivity) getActivity()).stopLoader();

                if (like != null) {
                    ((MainActivity) getActivity()).messageAlert(like.getMessage());
                }

            }
        });
    }

    private HashMap<String, String> getHashMapForPassword() {
        HashMap<String, String> hashMapPassword = new HashMap<>();
        hashMapPassword.put("old_password", changePasswordOldPassword.getText().toString());
        hashMapPassword.put("new_password", changePasswordNewPassword.getText().toString());
        return hashMapPassword;
    }


    public boolean isValid() {

        if (changePasswordOldPassword.getText().toString().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterOldPassword));
            changePasswordOldPassword.requestFocus();
            return false;
        }

        if (changePasswordNewPassword.getText().toString().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterNewPassword));
            changePasswordNewPassword.requestFocus();
            return false;
        } else {
            if (!Locale.getDefault().getDisplayLanguage().equals("العربية")) {
                matcher = pattern.matcher(changePasswordNewPassword.getText().toString());
                password = passwordPattern.matcher(changePasswordNewPassword.getText().toString());
                if (!password.matches()) {
                    changePasswordNewPassword.setText("");
                    ((MainActivity) getActivity()).messageAlert(getString(R.string.messagePasswordMustContains));
                    changePasswordNewPassword.requestFocus();
                    return false;
                }
            }
        }

        if (changePasswordNewPassword.getText().toString().length() < 3) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterMinimumThreeCharacterForPassword));
            changePasswordNewPassword.requestFocus();
            return false;
        }

        if (changePasswordConfirmPassword.getText().toString().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterConfirmPassword));
            changePasswordConfirmPassword.requestFocus();
            return false;
        }

        if (changePasswordNewPassword.getText().toString().compareTo(changePasswordConfirmPassword.getText().toString()) != 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_passwordmismatch));
            changePasswordNewPassword.requestFocus();
            return false;
        }

        return true;
    }


    @Override
    public int show(FragmentTransaction transaction, String tag) {
        return super.show(transaction, tag);
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        if (manager.findFragmentByTag(tag) == null) {
            super.show(manager, tag);
        }
    }
}