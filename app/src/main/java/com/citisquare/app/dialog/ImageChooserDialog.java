package com.citisquare.app.dialog;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.citisquare.app.R;
import com.citisquare.app.interfaces.ImageChooserListener;
import com.citisquare.app.utils.Debugger;
import com.citisquare.app.utils.FileUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by hlink16 on 28/9/15.
 */
public class ImageChooserDialog extends DialogFragment {
    private static final int PICK_Camera_IMAGE = 2;
    private static final int SELECT_FILE1 = 1;
    private static final int STORAGE_PERMISSION = 3;
    private static final int CAMERA_PERMISSION = 4;
    @BindView(R.id.raw_profile_chooser_string_take_photo)
    ImageView rawProfileChooserStringTakePhoto;
    @BindView(R.id.raw_profile_chooser_string_chooseFromGallery)
    ImageView rawProfileChooserStringChooseFromGallery;
    Uri selectedImage;
    private ImageChooserListener callback;
    private File file;
    private boolean isclicked;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.raw_profile_chooser, container, false);
        ButterKnife.bind(this, view);

        rawProfileChooserStringTakePhoto.setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
        rawProfileChooserStringChooseFromGallery.setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        isclicked = false;
        switch (requestCode) {
            case SELECT_FILE1:
                if (resultCode == Activity.RESULT_OK) {
                    if (requestCode == SELECT_FILE1) {
                        selectedImage = data.getData();
                        String path = FileUtils.getPath(getActivity(), selectedImage);
                        // Now we need to set the GUI ImageView data with data read from the picked file.
                        Debugger.e(this.getClass().getName(), " ::::: Image path is  ::::: " + path);
                        if (getCallback() != null)
                            getCallback().getResultFromGallery(path);
                    }
                } else {
                    if (getCallback() != null) {
                        Debugger.e(this.getClass().getName(), ":::: NOTHING SET IS CALLED :::::");
                        getCallback().nothingSet();
                    }
                }
                break;
            case PICK_Camera_IMAGE:

                if (resultCode == Activity.RESULT_OK) {
                    Bitmap thumbnail = null;
                    try {
                        if (data.getExtras().get("data") != null) {
                            Date dateTime = new Date();

                            thumbnail = (Bitmap) data.getExtras().get("data");
                            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                            String filenamePath = "tmp1" + dateTime.toString() + ".jpg";
                            thumbnail.compress(Bitmap.CompressFormat.JPEG, 55, bytes);
                            File outputDir = getActivity().getCacheDir();
                            file = new File(outputDir.getPath() + "/" + filenamePath);
                            file.createNewFile();
                            FileOutputStream fo = new FileOutputStream(file);
                            fo.write(bytes.toByteArray());
                            fo.close();

                        }
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    String path = file.getAbsoluteFile() + "";

                    if (getCallback() != null) {
                        getDialog().dismiss();
                        getCallback().getResultFromCamera(path);
                    }

                } else {
                    if (getCallback() != null) {
                        Debugger.e(this.getClass().getName(), ":::: NOTHING SET IS CALLED :::::");
                        getCallback().nothingSet();
                    }
                }
                break;
            default:
                if (getCallback() != null) {
                    Debugger.e(this.getClass().getName(), ":::: NOTHING SET IS CALLED :::::");
                    getCallback().nothingSet();
                }
                break;
        }
        getDialog().dismiss();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    private void checkCameraPermission() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION);
        } else {
            captureImage();
        }
    }

    public void checkStoragePermissions() {

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION);
        } else {
            selectImage();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case STORAGE_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    selectImage();
                    return;
                } else {
                    getActivity().finish();
                }
                break;

            case CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    captureImage();
                    return;
                } else {
                    getActivity().finish();
                }
                break;

        }
    }

    public void captureImage() {
        if (!isclicked) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, PICK_Camera_IMAGE);
        }
        isclicked = true;
    }


    public void selectImage() {
        if (!isclicked) {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, SELECT_FILE1);
        }
        isclicked = true;
    }

    public ImageChooserListener getCallback() {
        return callback;
    }

    public void setCallback(ImageChooserListener callback) {
        this.callback = callback;
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        if (manager.findFragmentByTag(tag) == null) {
            super.show(manager, tag);
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick({R.id.raw_profile_chooser_string_take_photo, R.id.raw_profile_chooser_string_chooseFromGallery})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.raw_profile_chooser_string_take_photo:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    checkCameraPermission();
                } else {
                    captureImage();
                }

                break;
            case R.id.raw_profile_chooser_string_chooseFromGallery:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    checkStoragePermissions();
                } else {
                    selectImage();
                }
                break;
        }
    }
}