package com.citisquare.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.CategoryMultipleSelectAdapter;
import com.citisquare.app.interfaces.CategoryListener;
import com.citisquare.app.interfaces.RenderingString;
import com.citisquare.app.pojo.response.CategoryData;
import com.citisquare.app.utils.Debugger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hlink56 on 15/7/16.
 */
public class CategorySelectionDialog extends DialogFragment {

    public static void openCategorySelectionDialog(Context context, int max, String catId, final CategoryListener categoryListener) {
        final Dialog dialog = new Dialog(context);
        final ArrayList<CategoryData> categoryDatas;
        final CategoryMultipleSelectAdapter categoryMultipleSelectAdapter;

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setContentView(R.layout.layout_picker_dialog);
        dialog.getWindow().setContentView(R.layout.category_selection_dialog);
        Button okButton = (Button) dialog.findViewById(R.id.ok);
        ListView cateGorySelection = (ListView) dialog.findViewById(R.id.cateGorySelection);

        categoryDatas = ((MainActivity) context).getCategoryList();
        categoryMultipleSelectAdapter = new CategoryMultipleSelectAdapter(context, max, catId, R.layout.raw_dialog_category, categoryDatas, new RenderingString() {
            @Override
            public void renderString(String data) {
            }
        });
        cateGorySelection.setAdapter(categoryMultipleSelectAdapter);

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CategoryData> string = categoryMultipleSelectAdapter.getStringList();

                categoryListener.onCategorySelected(string);
                Debugger.e("DATA RECEIVED OK ::: " + string);
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
