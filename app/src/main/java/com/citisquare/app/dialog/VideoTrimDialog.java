package com.citisquare.app.dialog;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.ffmpeg.MJPEGFFMPEGTest;
import com.citisquare.app.listener.ClickListener;
import com.citisquare.app.listener.TrimListener;
import com.citisquare.app.utils.Debugger;
import com.citisquare.app.utils.RangeSeekBar;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.bumptech.glide.gifdecoder.GifHeaderParser.TAG;

/**
 * Created by hlink16 on 7/11/15.
 */
public class VideoTrimDialog extends DialogFragment {

    @BindView(R.id.video_view)
    public VideoView mVideoView;
    @BindView(R.id.tv_min)
    public TextView min;
    @BindView(R.id.tv_max)
    public TextView max;
    @BindView(R.id.ll_container)
    public LinearLayout llContainer;
    @BindView(R.id.linearLayoutFrames)
    public LinearLayout linearLayoutFrames;
    HashMap<Integer, Bitmap> hashMapBitmap = new HashMap<>();
    String url;
    ProgressDialog progressDoalog;
    MJPEGFFMPEGTest mjpegffmpegTest;
    TrimListener trimListener;
    String trimmedFileName;
    private RangeSeekBar<Integer> range;
    private Handler rangeSeekerHandler, videoPlayerHandler;
    private Runnable rangeSeekerWork, videoPlayerWork;
    private Uri mVideoURI = null;
    private MediaMetadataRetriever metadata;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.raw_videotrim, container, false);
        ButterKnife.bind(this, view);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getDialog().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }

        if (getArguments() != null) {
            if (getArguments().getString("key") != null) {
                url = getArguments().getString("key");
                initialize();
                prepareVideo(Uri.parse(url));
                metadata = new MediaMetadataRetriever();
                metadata.setDataSource(getActivity(), mVideoURI);
                setFramming(metadata, Uri.parse(url));
            }
        }
        return view;
    }


    private void initialize() {

        rangeSeekerHandler = new Handler();
        videoPlayerHandler = new Handler();
        videoPlayerWork = new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                if (mVideoView.getCurrentPosition() >= range.getSelectedMaxValue() * 1000) {
                    Debugger.e("current position: " + mVideoView.getCurrentPosition() +
                            " max value: " + range.getSelectedMaxValue());
                    mVideoView.pause();
                } else {
                    videoPlayerHandler.postDelayed(this, 100);
                }
            }
        };
    }

    public void setFramming(MediaMetadataRetriever metadata, Uri uri) {
        MediaPlayer mp = MediaPlayer.create(getActivity().getApplicationContext(), uri);
        int millis = mp.getDuration();
        millis *= 1000;
        millis = millis / 7;
        //double seconds = millis / 1000.0;
        for (int j = 0; j <= 7; j++) {
            Bitmap bitmap = metadata.getFrameAtTime((millis * j), MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
            hashMapBitmap.put(j, bitmap);
            Debugger.e(this.getClass().getName(), "::::: Image id for Fetch is :::::: " + millis * j);
        }

        for (int i = 0; i < 7; i++) {
            View view = getActivity().getLayoutInflater().inflate(R.layout.raw_slider_imageview, linearLayoutFrames, false);
            ViewHolder viewHolder = new ViewHolder(view);
            viewHolder.imageView.setImageBitmap(hashMapBitmap.get(i));
            linearLayoutFrames.addView(view);
        }
    }

    private void prepareVideo(Uri uri) {

        mVideoURI = uri;
        mVideoView.setVideoURI(mVideoURI);
        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                // TODO Auto-generated method stub
                int milis = mp.getDuration();
                int seconds = (milis / 1000);
                int millis2 = 0;
                Cursor cursor = MediaStore.Video.query(getActivity().getContentResolver(), mVideoURI, new String[]{MediaStore.Video.VideoColumns.DURATION});
                if (cursor != null)
                    if (cursor.moveToFirst()) {
                        millis2 = Integer.parseInt(cursor.getString(0));
                    }
                //Toast.makeText(CustomSlider.this, "query millis: " + millis2 + " mpMilis: " + milis + " seconds: " + seconds, Toast.LENGTH_LONG).show();
                range = new RangeSeekBar<Integer>(0, seconds, getActivity());
                range.setNotifyWhileDragging(true);
                min.setText("" + 0);
                max.setText("" + seconds);

                if (seconds > 15) {
                    max.setText("" + 15);
                    range.setSelectedMaxValue(15);
                }

                range.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
                    @Override
                    public void onRangeSeekBarValuesChanged(
                            RangeSeekBar<?> bar, Integer minValue,
                            Integer maxValue, RangeSeekBar.Thumb selectedThumb) {
                        // TODO Auto-generated method stub
                            if (maxValue - minValue > 15) {

                            if (selectedThumb != null) {

                                if (selectedThumb == RangeSeekBar.Thumb.MIN) {
                                    range.setSelectedMaxValue(minValue + 15);
                                } else {
                                    range.setSelectedMinValue(maxValue - 15);
                                }
                            }
                        }

                        Log.d(TAG, "onRangeSeekBarValuesChanged() called with: bar = [" + bar + "], value gap= [" + (maxValue-minValue) + "], selectedThumb = [" + selectedThumb + "]");
                        min.setText("" + minValue);
                        max.setText("" + maxValue);

                        final int previewStartTime = minValue;

                        mVideoView.pause();
                        rangeSeekerHandler.removeCallbacks(rangeSeekerWork);
                        videoPlayerHandler.removeCallbacks(videoPlayerWork);
                        rangeSeekerHandler.postDelayed(rangeSeekerWork = new Runnable() {
                            @Override
                            public void run() {
                                // TODO Auto-generated method stub
                                mVideoView.seekTo(previewStartTime * 1000);
                                mVideoView.start();
                                videoPlayerHandler.post(videoPlayerWork);
                            }
                        }, 800);
                    }
                });

                llContainer.addView(range, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                mVideoView.start();
                videoPlayerHandler.post(videoPlayerWork);
            }
        });

        mVideoView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                // TODO Auto-generated method stub
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        if (!mVideoView.isPlaying()) {
                            mVideoView.seekTo(range.getSelectedMinValue() * 1000);
                            mVideoView.start();
                            videoPlayerHandler.post(videoPlayerWork);
                        } else {
                            videoPlayerHandler.removeCallbacks(videoPlayerWork);
                            mVideoView.pause();
                        }
                        break;
                    }
                }
                return true;
            }
        });
    }

    @OnClick(R.id.buttonTrim)
    public void onTrimCalled() {
        progressDoalog = new ProgressDialog(getActivity());
        progressDoalog.setCancelable(false);
        progressDoalog.setMessage("Loading....");
        progressDoalog.show();

        if (getActivity() != null) {
            trimmedFileName = ((MainActivity) getActivity()).toCreateDirectory() +
                    "/" + System.currentTimeMillis() + ((MainActivity) getActivity()).generateRandomNumber() + ".mp4";
            mjpegffmpegTest = new MJPEGFFMPEGTest(trimmedFileName);
            mjpegffmpegTest.setCallback(new ClickListener() {
                @Override
                public void onOkClick(int position) {
                    progressDoalog.dismiss();
                    if (trimListener != null) {
                        trimListener.onTrimmingDone(trimmedFileName);
                        dismiss();
                    }
                }
            });
            int seconds = Integer.parseInt(max.getText().toString()) - Integer.parseInt(min.getText().toString());
            mjpegffmpegTest.toStartTrim(getActivity(), min.getText().toString(), seconds, url);
        }
    }


    public void setCallback(TrimListener trimListener) {
        this.trimListener = trimListener;
    }

    public class ViewHolder {
        @BindView(R.id.imageview)
        public ImageView imageView;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
