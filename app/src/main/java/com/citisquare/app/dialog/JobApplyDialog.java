package com.citisquare.app.dialog;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.ImageChooserListener;
import com.citisquare.app.interfaces.Navigation;
import com.citisquare.app.pojo.response.JobListData;
import com.citisquare.app.pojo.response.Like;
import com.citisquare.app.pojo.response.LoginWrapper;
import com.citisquare.app.utils.RequestParameter;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import serviceCalling.builder.KeyValuePair;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 1/6/16.
 */
public class JobApplyDialog extends DialogFragment {


    File uploadAttachment;
    @BindView(R.id.jobApplyClose)
    ImageView jobApplyClose;
    @BindView(R.id.jobApplyfirstname)
    EditText jobApplyfirstname;
    @BindView(R.id.jobApplyLastName)
    EditText jobApplyLastName;
    @BindView(R.id.jobApplyBackground)
    EditText jobApplyBackground;
    @BindView(R.id.jpbApplyCountryCode)
    TextView jpbApplyCountryCode;
    @BindView(R.id.jobApplyPhoneNumber)
    TextView jobApplyPhoneNumber;
    @BindView(R.id.jobUploadResume)
    TextView jobUploadResume;
    @BindView(R.id.jobApplyButtonSubmit)
    Button jobApplyButtonSubmit;
    @BindView(R.id.title)
    TextView title;

    Bundle bundle;
    JobListData jobListData;
    @BindView(R.id.applyJobemail)
    TextView applyJobemail;

    private Pattern pattern;
    private Matcher matcher;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.job_apply, container, false);
        ButterKnife.bind(this, view);

        bundle = getArguments();

        pattern = Pattern.compile(Constants.englishOnly);

        if (bundle != null) {
            if (bundle.getString("jobData") != null) {
                jobListData = new Gson().fromJson(bundle.getString("jobData"), JobListData.class);
            }
        }

        if (jobListData != null) {
            LoginWrapper loginWrapper = ((MainActivity) getActivity()).getData(getContext());
            jobApplyfirstname.setText(loginWrapper.getData().getFirstName());
            jobApplyLastName.setText(loginWrapper.getData().getLastName());
            jpbApplyCountryCode.setText(loginWrapper.getData().getCountryCode());
            jobApplyPhoneNumber.setText(loginWrapper.getData().getPhoneNo());

            applyJobemail.setText(loginWrapper.getData().getEmail());
        }
        getDialog().requestWindowFeature(STYLE_NO_TITLE);
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        getDialog().setCanceledOnTouchOutside(true);

        return view;
    }


    @OnClick(R.id.jobApplyClose)
    public void jobApplyCloseClicked() {
        InputMethodManager im = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        im.hideSoftInputFromWindow(jobApplyClose.getWindowToken(), 0);
        //((MainActivity) getActivity()).hideKeyboard();
        dismiss();
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        if (manager.findFragmentByTag(tag) == null) {
            super.show(manager, tag);
        }
    }


    public boolean isValid() {
        if (jobApplyfirstname.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterFirstName));
            jobApplyfirstname.requestFocus();
            return false;
        }
        if (jobApplyLastName.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterLastName));
            jobApplyLastName.requestFocus();
            return false;
        }


        if (jobApplyBackground.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterBackgroud));
            jobApplyBackground.requestFocus();
            return false;
        }


        if (jpbApplyCountryCode.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterCountryCode));
            jpbApplyCountryCode.requestFocus();
            return false;
        }

        if (jobApplyPhoneNumber.getText().toString().trim().length() == 0) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterPhoneNumber));
            jobApplyPhoneNumber.requestFocus();
            return false;
        }


        if (jobApplyPhoneNumber.getText().toString().length() < 9) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterValidPhoneNumber));
            jobApplyPhoneNumber.requestFocus();
            return false;
        }


        if (!validEmail(applyJobemail.getText().toString())) {
            applyJobemail.requestFocus();
            return false;
        }

        if (jobUploadResume.getText().toString().equals(getResources().getString(R.string.uploadResume))) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseSelectResume));
            jobUploadResume.requestFocus();
            return false;
        }
        return true;
    }

    public boolean validEmail(String email) {
        int length = email.trim().length();

        // Patterns.WEB_URL.matcher(potentialUrl).matches()
        Pattern pattern = Pattern.compile(Navigation.EMAIL_REGEX);
        Matcher matcher = pattern.matcher(email);
        if (!(email.charAt(0) + "").matches("\\p{L}") || !matcher.matches()) {
            ((MainActivity) getActivity()).messageAlert(getString(R.string.message_pleaseEnterValidEmailId));
            return false;
        }
        return true;
    }

    @OnClick(R.id.jobApplyButtonSubmit)
    public void setButtonSubmit() {
        if (isValid()) {
            InputMethodManager im = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(jobApplyButtonSubmit.getWindowToken(), 0);
            applyJob();
        }
    }

    public void applyJob() {

        if (getActivity() != null)
            ((MainActivity) getActivity()).startLoader();

        List<KeyValuePair> keyValuePairs = new ArrayList<>();
        keyValuePairs.add(new KeyValuePair("resume_file", uploadAttachment.getAbsolutePath()));


        new PostRequestHelper<Like>().pingToRequest(Constants.APPLY_JOB
                , "image/*"
                , keyValuePairs
                , new RequestParameter().getHashMapApplyJob(getHashMapApplyJob())
                , ((MainActivity) getActivity()).getParamWholeParameters()
                , new Like()
                , new TaskCompleteListener<Like>() {
                    @Override
                    public void onSuccess(Like mObject) {

                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        ((MainActivity) getActivity()).messageAlert(mObject.getMessage());
                        dismiss();
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, Like like) {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).stopLoader();

                        if(like!=null) {
                            if (like.getStatus() == -1)
                                ((MainActivity) getActivity()).invalidToken();
                        }

                        ((MainActivity) getActivity()).messageAlert(like.getMessage());
                        dismiss();
                    }
                });
    }

    @OnClick(R.id.jobUploadResume)
    public void jobUploadResume() {
        ImageChooserDialog imageChooserDialog = new ImageChooserDialog();
        imageChooserDialog.show(getActivity().getSupportFragmentManager(), "imageChooserDialog");
        imageChooserDialog.setCallback(new ImageChooserListener() {
            @Override
            public void getResultFromCamera(String result) {
                uploadAttachment = new File(result);
                jobUploadResume.setText(uploadAttachment.getName());
                //((MainActivity) getActivity()).setCircularImage(image, imageViewBusinessImage);
            }

            @Override
            public void getResultFromGallery(String result) {
                if (result != null) {
                    if (!(result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("jpeg") ||
                            result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("jpg") ||
                            result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("png") ||
                            result.substring(result.lastIndexOf(".") + 1).equalsIgnoreCase("bmp"))) {
                        //isInvalidImage = true;
                        ((MainActivity) getActivity()).messageAlert(getString(R.string.message_thisImageIsNotSupported));
                    } else {
                        uploadAttachment = new File(result);
                        jobUploadResume.setText(uploadAttachment.getName());
                        // ((MainActivity) getActivity()).setCircularImage(image, imageViewBusinessImage);
                        //layoutAddItemImageViewAdd.setBackgroundColor(getResources().getColor(R.color.backColor));
                        //isInvalidImage = false;
                    }
                }
            }

            @Override
            public void nothingSet() {

            }
        });
    }

    public HashMap<String, String> getHashMapApplyJob() {
        HashMap<String, String> hashMapApplyJob = new HashMap<>();
        hashMapApplyJob.put("job_id", jobListData.getId());
        hashMapApplyJob.put("first_name", jobApplyfirstname.getText().toString().trim());
        hashMapApplyJob.put("last_name", jobApplyLastName.getText().toString().trim());
        hashMapApplyJob.put("background", jobApplyBackground.getText().toString().trim());
        hashMapApplyJob.put("country_code", jpbApplyCountryCode.getText().toString().trim());
        hashMapApplyJob.put("phoneno", jobApplyPhoneNumber.getText().toString().trim());
        hashMapApplyJob.put("email", applyJobemail.getText().toString().trim());

        return hashMapApplyJob;
    }
}
