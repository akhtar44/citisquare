package com.citisquare.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.adapter.SubCategoryMultipleSelection;
import com.citisquare.app.interfaces.RenderingString;
import com.citisquare.app.interfaces.SubCategoryListener;
import com.citisquare.app.pojo.response.CategoryData;
import com.citisquare.app.pojo.response.SubCategory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by hlink56 on 26/8/16.
 */
public class SubCategorySelectionDialog extends DialogFragment {

    public static void openCategorySelectionDialog(Context context, String category, final SubCategoryListener subCategoryListener) {
        final Dialog dialog = new Dialog(context);
        List<CategoryData> categoryDatas;
        List<SubCategory> subCategories = new ArrayList<>();
        List<String> categoryId = Arrays.asList(category.split(","));
        final SubCategoryMultipleSelection categoryMultipleSelectAdapter;

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setContentView(R.layout.layout_picker_dialog);
        dialog.getWindow().setContentView(R.layout.category_selection_dialog);
        Button okButton = (Button) dialog.findViewById(R.id.ok);
        ListView cateGorySelection = (ListView) dialog.findViewById(R.id.cateGorySelection);

        categoryDatas = ((MainActivity) context).getCategoryList();
        for (int i = 0; i < categoryDatas.size(); i++) {
            for (int j = 0; j < categoryId.size(); j++) {
                if (categoryDatas.get(i).getId().equals(categoryId.get(j))) {
                    for (int k = 0; k < categoryDatas.get(i).getSubCategory().size(); k++)
                        subCategories.add(categoryDatas.get(i).getSubCategory().get(k));
                }
            }
        }
        categoryMultipleSelectAdapter = new SubCategoryMultipleSelection(context, R.layout.raw_dialog_category, subCategories, new RenderingString() {
            @Override
            public void renderString(String data) {
            }
        });
        cateGorySelection.setAdapter(categoryMultipleSelectAdapter);

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ItemEventListener
                List<SubCategory> string = categoryMultipleSelectAdapter.getStringList();
                subCategoryListener.onSubCategoryListener(string);
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}