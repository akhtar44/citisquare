package com.citisquare.app.model;

/**
 * Created by BookMEds on 05-02-2018.
 */

public class UserKeyDetailsModel {


    private String id;
    private String DeviceUniqueID;
    private String ProfilePicture;
    public String ThemeColorCode;
    public boolean IsDefaultTheme;

    private String username;
    private String email;
    private String phone_no;
    private String profile_image;
    private String branch_name;
    private String user_session_id;
    private String category_count;
    private String language;
    private String building_no;
    private String street;
    private String city;
    private String country;
    private String zipcode;
    private String first_name;
    private String last_name;
    private String device_token;

    public void setIsDefaultTheme(boolean defaultTheme) {
        IsDefaultTheme = defaultTheme;
    }

    public boolean getIsDefaultTheme() {
        return IsDefaultTheme;
    }

    public void setThemeColorCode(String themeColorCode) {
        ThemeColorCode = themeColorCode;
    }

    public String getThemeColorCode() {
        return ThemeColorCode;
    }

    public void setPhoneNumber(String phoneNumber) {
        phone_no = phoneNumber;
    }

    public void setDeviceUniqueID(String deviceUniqueID) {
        DeviceUniqueID = deviceUniqueID;
    }

    public void setProfilePicture(String profilePicture) {
        ProfilePicture = profilePicture;
    }

    public void setUserGuid(String userGuid) {
        id = userGuid;
    }

    public String getUserGuid() {
        return id;
    }

    public String getPhoneNumber() {
        return phone_no;
    }

    public String getDeviceUniqueID() {
        return DeviceUniqueID;
    }

    public String getProfilePicture() {
        return ProfilePicture;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setBranch_name(String branch_name) {
        this.branch_name = branch_name;
    }

    public void setBuilding_no(String building_no) {
        this.building_no = building_no;
    }

    public void setCategory_count(String category_count) {
        this.category_count = category_count;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setUser_session_id(String user_session_id) {
        this.user_session_id = user_session_id;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }

    public String getCity() {
        return city;
    }

    public String getBranch_name() {
        return branch_name;
    }

    public String getBuilding_no() {
        return building_no;
    }

    public String getCategory_count() {
        return category_count;
    }

    public String getCountry() {
        return country;
    }

    public String getLanguage() {
        return language;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public String getStreet() {
        return street;
    }

    public String getUser_session_id() {
        return user_session_id;
    }

    public String getZipcode() {
        return zipcode;
    }
}
