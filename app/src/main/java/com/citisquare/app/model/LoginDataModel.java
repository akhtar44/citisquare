package com.citisquare.app.model;

public class LoginDataModel {

    /*{
	"status": 1,
	"message": "Login successfully",
	"data": {
		"id": "1811",
		"category_id": "",
		"sub_cat_id": "",
		"title": null,
		"first_name": null,
		"last_name": null,
		"username": "Qwerasdf",
		"email": "qwerasdf@gmail.com",
		"alternate_email": null,
		"password": "d90d87941927cb49e545af4f1f1ce99c",
		"building_no": null,
		"street": null,
		"city": "",
		"country": null,
		"country_code": null,
		"phone_no": "",
		"cell_country_code": null,
		"cell_phoneno": null,
		"zipcode": null,
		"profile_image": "https:\/\/s3-eu-west-1.amazonaws.com\/citisquare2\/user_profile\/default-user.png",
		"cover_image": null,
		"company_name": null,
		"company_name_ar": null,
		"website": null,
		"bio": null,
		"business_hours": null,
		"business_licence_id": "",
		"branch_name": "No Branch",
		"branch_id": "1",
		"device_type": "",
		"device_token": "358982079165301",
		"role": "V",
		"login_type": null,
		"latitude": null,
		"longitude": null,
		"seller_type": "1",
		"plan_expiry_date": "2019-01-11",
		"is_category_pending": "1",
		"is_delivery": "1",
		"notification": "1",
		"rate": "0.0",
		"language": "english",
		"keyword": null,
		"offer_count": "20",
		"is_active": "1",
		"insertdate": "2018-12-11 08:48:43",
		"is_visible": "zero",
		"offer_used_count": "0",
		"banner_count": "0",
		"banner_used_count": "0",
		"wallet": "0",
		"wallet_payment": "0",
		"salesman_id": null,
		"is_email_show": null,
		"is_mobile_number_show": "",
		"city_name": null,
		"country_name": null,
		"category_count": "3",
		"profile_image_thumb": "https:\/\/s3-eu-west-1.amazonaws.com\/citisquare2\/user_profile\/thumb\/default-user.png",
		"business_id_image_thumb": "",
		"user_session_id": "1ecdf3e1738b6129c941cfedf2388c29"
	}
}*/

    private String id;
    private String username;
    private String email;
    private String profile_image;
    private String branch_name;
    private String user_session_id;
    private String category_count;
    private String language;
    private String building_no;
    private String street;
    private String city;
    private String country;
    private String phone_no;
    private String zipcode;
    private String first_name;
    private String last_name;
    private String device_token;

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setBranch_name(String branch_name) {
        this.branch_name = branch_name;
    }

    public void setBuilding_no(String building_no) {
        this.building_no = building_no;
    }

    public void setCategory_count(String category_count) {
        this.category_count = category_count;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setUser_session_id(String user_session_id) {
        this.user_session_id = user_session_id;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }

    public String getCity() {
        return city;
    }

    public String getBranch_name() {
        return branch_name;
    }

    public String getBuilding_no() {
        return building_no;
    }

    public String getCategory_count() {
        return category_count;
    }

    public String getCountry() {
        return country;
    }

    public String getId() {
        return id;
    }

    public String getLanguage() {
        return language;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public String getStreet() {
        return street;
    }

    public String getUser_session_id() {
        return user_session_id;
    }

    public String getZipcode() {
        return zipcode;
    }
}
