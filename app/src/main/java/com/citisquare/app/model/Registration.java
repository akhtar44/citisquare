package com.citisquare.app.model;

import android.app.Activity;
import android.os.AsyncTask;

import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.utils.Connectivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by ahmed on 10/27/2016.
 */

public class Registration extends AsyncTask<String, String, String> {

    StringBuilder s;
    String medicineName;
    Activity activity;
    LogInEntity loginEntity;
    Gson gson;
    String RegistrationApi=null,LogInApi;
    String JsonData = null;
    public Registration(Activity act, LogInEntity getlogInEntity) {
       // medicineName = medicinename;
        activity = act;
        loginEntity=getlogInEntity;
gson=new GsonBuilder().disableHtmlEscaping().create();
        JsonData=gson.toJson(loginEntity);
    }

    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
    }

    @SuppressWarnings("deprecation")
    @Override
    protected String doInBackground(String... params) {
        String result = "";
      String	urll = Constants.URL;
urll=urll+"customer";
        HttpURLConnection urlConnection;
            BufferedReader reader = null;
            try {

       /* URL   url = new URL(urll+JsonDATA);
                HttpURLConnection conn = (HttpURLConnection) url
                        .openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Accept", "application/json");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                OutputStream os = conn.getOutputStream();

                os.close();
*/






              URL  url = new URL(urll);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setDoOutput(true);
                // is output buffer writter
                urlConnection.setRequestProperty("Accept", "application/json");
//set headers and method

                OutputStream os = urlConnection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
                        os, "UTF-8"));
                // writer.write(getPostData(postDataParams));
                writer.write(JsonData);

                writer.flush();
                writer.close();
                os.close();


                final int responseCode = urlConnection.getResponseCode();
              /*  runOnUiThread(new Runnable() {
                    public void run() {
                                   Toast.makeText(activity, "Response code" + responseCode, Toast.LENGTH_SHORT).show();
                    }
                });*/
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            urlConnection.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        result += line;
                    }
                } else {
                    if( !Connectivity.isConnectedFast(activity)) {
                        result = "Network slow";
                    }else{
                        result = "";
                    }


                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        return result;

    }


    protected void onPostExecute(String result) {

    }
  /*  public static String POST(String url, HealthRecordEntity person){
       InputStream inputStream = null;
      String result = "";
       try {











           // 1. create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

           // 2. make POST request to the given URL
            HttpPost httpPost = new HttpPost(url);

         String json = "";
            // 3. build jsonObject
           JSONObject jsonObject = new JSONObject();
             jsonObject.accumulate("name", person.getName());
            jsonObject.accumulate("country", person.getCountry());
            jsonObject.accumulate("twitter", person.getTwitter());

           // 4. convert JSONObject to JSON to String
          json = jsonObject.toString();

             // ** Alternative way to convert Person object to JSON string usin Jackson Lib
           // ObjectMapper mapper = new ObjectMapper();
             // json = mapper.writeValueAsString(person);

           // 5. set json to StringEntity
           StringEntity se = new StringEntity(json);

             // 6. set httpPost Entity
            httpPost.setEntity(se);

            // 7. Set some headers to inform server about the type of the content   
           httpPost.setHeader("Accept", "application/json");
             httpPost.setHeader("Content-type", "application/json");
          // 8. Execute POST request to the given URL
            HttpResponse httpResponse = httpclient.execute(httpPost);

           // 9. receive response as inputStream
         inputStream = httpResponse.getEntity().getContent();
           // 10. convert inputstream to string
            if(inputStream != null)
             result = convertInputStreamToString(inputStream);
             else
            result = "Did not work!";

           } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
           }

      // 11. return result
        return result;
        }
    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
       String line = "";
        String result = "";
       while((line = bufferedReader.readLine()) != null)
        result += line;

       inputStream.close();
        return result;
       }*/
}