package com.citisquare.app.model;

import com.citisquare.app.pojo.response.LoginData;

public class LoginResponseModel {

    private int status;
    private String message;

    private LoginDataModel data;

    public void setData(LoginDataModel data) {
        this.data = data;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public LoginDataModel getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }
}
