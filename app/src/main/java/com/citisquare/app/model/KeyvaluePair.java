package com.citisquare.app.model;

import android.graphics.drawable.Drawable;

/**
 * Created by hlink16 on 25/5/16.
 */
public class KeyvaluePair {
    private Drawable key;
    private String value;
    private int badge;


    public KeyvaluePair(Drawable key, String value, int badge) {
        this.key = key;
        this.value = value;
        this.badge = badge;
    }

    public Drawable getKey() {
        return key;
    }

    public void setKey(Drawable key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getBadge() {
        return badge;
    }

    public void setBadge(int badge) {
        this.badge = badge;
    }
}
