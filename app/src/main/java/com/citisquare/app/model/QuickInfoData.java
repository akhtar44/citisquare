package com.citisquare.app.model;

/**
 * Created by hlink56 on 20/6/16.
 */
public class QuickInfoData {
    String name;
    String followed;
    String location;
    String date;
    boolean isFollowed;

    public QuickInfoData(String name, String followed, String location, String date) {
        this.name = name;
        this.followed = followed;
        this.location = location;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFollowed() {
        return followed;
    }

    public void setFollowed(String followed) {
        this.followed = followed;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isFollowed() {
        return isFollowed;
    }

    public void setFollowed(boolean followed) {
        isFollowed = followed;
    }

    @Override
    public String toString() {
        return "QuickInfoData{" +
                "name='" + name + '\'' +
                ", followed='" + followed + '\'' +
                ", location='" + location + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
