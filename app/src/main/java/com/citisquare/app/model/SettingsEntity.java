package com.citisquare.app.model;

public class SettingsEntity {
    public boolean IsAllNotificationsOn;
    public boolean IsMuteFor1Day;
    public boolean IsMuteFor8Hours;
    public boolean IsMuteFor1Hour;
}
