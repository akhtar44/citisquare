package com.citisquare.app.model;

import java.io.Serializable;

public class SpinnerModel implements Serializable {

    private int id;
    private String title;

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }
}
