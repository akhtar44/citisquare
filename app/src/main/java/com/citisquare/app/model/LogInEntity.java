package com.citisquare.app.model;

/**
 * Created by ahmed on 11/4/2016.
 */

public class LogInEntity {
    private String username;
    private String email;
    private String password;
    private String phone_no;
    private String device_type;
    private String device_token;
    private String role;
    private String login_type;
    private String ReferralCode;
    private String city;

    public void setCity(String city) {
        this.city = city;
    }

    public void setDeviceType(String deviceType) {
        device_type = deviceType;
    }
    public void setDeviceToken(String deviceToken) {
        device_token = deviceToken;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setLogin_type(String login_type) {
        this.login_type = login_type;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPhoneNumber(String phoneNumber) {
        phone_no = phoneNumber;
    }

    public void setReferralCode(String referralCode) {
        ReferralCode = referralCode;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public String getCity() {
        return city;
    }

    public String getDeviceType() {
        return device_type;
    }

    public String getDevice_token() {
        return device_token;
    }

    public String getLogin_type() {
        return login_type;
    }

    public String getPassword() {
        return password;
    }

    public String getPhoneNumber() {
        return phone_no;
    }

    public String getReferralCode() {
        return ReferralCode;
    }

    public String getRole() {
        return role;
    }

    public String getUsername() {
        return username;
    }
}
