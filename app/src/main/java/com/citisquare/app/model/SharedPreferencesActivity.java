package com.citisquare.app.model;

import android.content.Context;
import android.content.SharedPreferences;

import com.citisquare.app.R;
import com.google.gson.Gson;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahmed on 11/14/2017.
 */

public class SharedPreferencesActivity {
    private static final String TAG = "SharedPreferences";
    public SharedPreferences sharedPreferences;
    public static SharedPreferencesActivity instance;

    public static SharedPreferencesActivity getInstance(Context context)

    {
        if (instance == null)
            instance = new SharedPreferencesActivity(context);
        return instance;
    }

    public SharedPreferencesActivity(Context context) {
        sharedPreferences = context.getSharedPreferences(context.getResources().getString(R.string.shared_prefs), Context.MODE_PRIVATE);
    }


    public int getStepCount() {

        return sharedPreferences.getInt("StepCount", 0);
    }

    public void setStepCount(int stepCount) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("StepCount", stepCount);
        editor.apply();
    }

    public void setIndonesianAvg(int indonesianAvg) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("IndonesianAvg", indonesianAvg);
        editor.apply();
    }

    public int getIndonesianAvg() {

        return sharedPreferences.getInt("IndonesianAvg", 0);
    }

    public void setMyAvg(int indonesianAvg) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("MyAvg", indonesianAvg);
        editor.apply();
    }

    public int getMyAvg() {

        return sharedPreferences.getInt("MyAvg", 0);
    }

    public void setMalaysianAvg(int MalaysianAvg) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("MalaysianAvg", MalaysianAvg);
        editor.apply();
    }

    public int getMalaysianAvg() {

        return sharedPreferences.getInt("MalaysianAvg", 0);
    }

    public void setMyTotal(int indonesianAvg) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("MyTotal", indonesianAvg);
        editor.apply();
    }

    public int getMyTotal() {

        return sharedPreferences.getInt("MyTotal", 0);
    }

    public void setLastUpdateValue(int LastUpdateValue) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("LastUpdateValue", LastUpdateValue);
        editor.apply();
    }

    public int getLastUpdateValue() {

        return sharedPreferences.getInt("LastUpdateValue", 0);
    }

    public void setIsJoined(boolean isJoined) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("IsStepJoined", isJoined);
        editor.apply();
    }

    public boolean getIsJoined() {

        return sharedPreferences.getBoolean("IsStepJoined", false);
    }

    public void setDailyCount(int dailyCount) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("DailyCount", dailyCount);
        editor.apply();
    }

    public int getDailyCount() {

        return sharedPreferences.getInt("DailyCount", 0);
    }

    public void setTarget(int target) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("Target", target);
        editor.apply();
    }

    public int getTarget() {

        return sharedPreferences.getInt("Target", 0);
    }

    public void setJoinedDays(String JoinDays) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("JoinDays", JoinDays);
        editor.apply();
    }

    public String getJoinedDays() {

        return sharedPreferences.getString("JoinDays", "0");
    }

    public void setDate(String DateTime) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("DateTime", DateTime);
        editor.apply();
    }

    public String getDate() {

        return sharedPreferences.getString("DateTime", "0");
    }

    public void setLastLeaderBoardUpdateTime(long timeMillis) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong("LastLeaderBoardUpdateTime", timeMillis);
        editor.apply();
    }

    public long getLastLeaderBoardUpdateTime() {

        return sharedPreferences.getLong("LastLeaderBoardUpdateTime", 0);
    }

    public void saveLeaderboardData(String result) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("SaveLeaderBoardData", result);
        editor.apply();
    }

    public String getsavedLeaderboardData() {

        return sharedPreferences.getString("SaveLeaderBoardData", "");
    }

    public void setMyPosition(String position) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("LeaderBoardPosition", position);
        editor.apply();
    }

    public String getLeaderboardPosition() {

        return sharedPreferences.getString("LeaderBoardPosition", "");
    }

    public void setGroupDetailsUpdateTime(long timeMillis, String groupId) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong("GroupDetailsUpdateTime" + groupId, timeMillis);
        editor.apply();
    }

    public long getGroupDetailsUpdateTime(String groupId) {

        return sharedPreferences.getLong("GroupDetailsUpdateTime" + groupId, 0);
    }

    public void setUpdateDb(int updateDb) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("UpdateDb", updateDb);
        editor.apply();
    }

    public int getUpdateDb() {


        return sharedPreferences.getInt("UpdateDb", 0);
    }

    public void setRequestedChallenge(boolean ChallengeRequested) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("ChallengeRequested", ChallengeRequested);
        editor.apply();
    }

    public boolean getRequestedChallenge() {


        return sharedPreferences.getBoolean("ChallengeRequested", false);
    }

    public void setLoginAppVersion(String appVersion) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("LoginAppVersion", appVersion);
        editor.apply();
    }

    public String getLoginAppVersion() {

        return sharedPreferences.getString("LoginAppVersion", "");
    }

    public void setCurrentAppVersion(String appVersion) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("CurrentAppVersion", appVersion);
        editor.apply();
    }

    public String getCurrentAppVersion() {

        return sharedPreferences.getString("CurrentAppVersion", "");
    }

    public void setGroupLastTimeTicks(long appVersion) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong("GroupLastTimeTicks", appVersion);
        editor.apply();
    }

    public long getGroupLastTimeTicks() {

        return sharedPreferences.getLong("GroupLastTimeTicks", 0);
    }





    public void setLastSavedStep(int lastSavedStep) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("lastSavedStep", lastSavedStep);
        editor.apply();
    }

    public int getLastSavedStep() {

        return sharedPreferences.getInt("lastSavedStep", 0);
    }

    public void setGroupCategeryId(int categoryId, int GroupId) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("GroupCategeryId" + GroupId, categoryId);
        editor.apply();
    }

    public int getGroupCategeryId(int GroupId) {

        return sharedPreferences.getInt("GroupCategeryId" + GroupId, 0);
    }

    public void setInternet(boolean isInternet) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("isInternet", isInternet);
        editor.apply();
    }

    public boolean getInternet() {
        return sharedPreferences.getBoolean("isInternet", false);
    }

    public void setlastStepSync(String gmtTime) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("LastSync", gmtTime);
        editor.apply();
    }

    public String getlastStepSync() {
        return sharedPreferences.getString("LastSync", "");
    }

    public void setIsStepBackendCallRunning(boolean isRunning) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("IsStepBackendCallRunning", isRunning);
        editor.apply();
    }

    public boolean getIsStepBackendCallRunning() {

        return sharedPreferences.getBoolean("IsStepBackendCallRunning", false);
    }



}
