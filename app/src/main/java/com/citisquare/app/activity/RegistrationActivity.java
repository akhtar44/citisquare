package com.citisquare.app.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.citisquare.app.R;
import com.citisquare.app.model.CommonResponseModel;
import com.citisquare.app.model.UserKeyDetailsModel;
import com.citisquare.app.utils.Constant;
import com.citisquare.app.utils.Utility;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by BookMEds on 05-02-2018.
 */

public class RegistrationActivity extends AppCompatActivity {


    ProgressDialog progressDialog;
    EditText enterNameET, enterEmailET;

    TextView submitTV;

    UserKeyDetailsModel userKeyDetailsModel;

    Utility utility = Utility.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        setToolBar();

        InitializeIDS();

        progressDialog = Utility.getProgressDialog(this, getString(R.string.pleaseWait));
        userKeyDetailsModel = Utility.getUserKeyDetails(this);

        submitTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    OnSubmitPressed();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        });
    }

    private void OnSubmitPressed() {
        String nickName, email;
        try {

            nickName = enterNameET.getText().toString().trim();
            email = enterEmailET.getText().toString().trim();

            if (StringUtils.isBlank(nickName)) {
                Utility.ShowToast(this, getString(R.string.enterName));
                return;
            } else if (StringUtils.isBlank(email)) {
                Utility.ShowToast(this, getString(R.string.enterEmail));
                return;
            } else if (!utility.isValidEmail(email)) {
                Utility.ShowToast(this, getString(R.string.pleasEnterValidEmail));
                return;
            } else {

                userKeyDetailsModel.setFirst_name(nickName);
                userKeyDetailsModel.setEmail(email);
                Utility.saveUserDetails(RegistrationActivity.this, userKeyDetailsModel);
                MainActivity.goToMainActivity(RegistrationActivity.this);
                //updateProfile(nickName, email);

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void updateProfile(final String name, final String email) {

        Utility.showDialog(progressDialog);

        StringRequest stringRequest = new StringRequest(Request.Method.PATCH, userKeyDetailsModel.getUserGuid(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            if (!StringUtils.isBlank(response)) {
                                CommonResponseModel commonResponseModel = Utility.getGson().fromJson(response, CommonResponseModel.class);
                                if (StringUtils.equalsIgnoreCase(commonResponseModel.getMessage(), "Profile Updated")) {
                                    Utility.saveUserDetails(RegistrationActivity.this, userKeyDetailsModel);
                                    MainActivity.goToMainActivity(RegistrationActivity.this);
                                }

                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        Utility.dismissDialog(progressDialog);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utility.dismissDialog(progressDialog);
                        Utility.onErrorResponseMessage(RegistrationActivity.this, error);

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", name);
                params.put("email", email);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params = Utility.getHeaderOnlyContentType();
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                500000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(AppController.getInstance());
        requestQueue.add(stringRequest);

    }


    private void InitializeIDS() {
        try {

            enterNameET = (EditText) findViewById(R.id.enterNameET);
            enterEmailET = (EditText) findViewById(R.id.enterEmailET);

            submitTV = (TextView) findViewById(R.id.doneTV);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void gotoRegistrationActivity(Context context) {
        try {
            Intent intent = new Intent(context, RegistrationActivity.class);
            context.startActivity(intent);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void setToolBar() {
        Toolbar toolbar;
        TextView toolbar_title;
        try {
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar_title = (TextView) findViewById(R.id.toolbar_title);
            setSupportActionBar(toolbar);
            Utility.getInstance().SupportActionBar(RegistrationActivity.this, getSupportActionBar(), Constant.THEMECOLOR, toolbar_title, getString(R.string.register), false);

            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        try {
            getMenuInflater().inflate(R.menu.menu_common_activity, menu);
            MenuItem moreIcon = menu.findItem(R.id.homeIcon);
            moreIcon.setVisible(false);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onBackPressed() {
        try {
            Utility.minimizeActivity(RegistrationActivity.this);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


}
