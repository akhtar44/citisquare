package com.citisquare.app.activity;

import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.RelativeLayout;

import com.citisquare.app.R;
import com.citisquare.app.model.UserKeyDetailsModel;
import com.citisquare.app.utils.Utility;

import org.apache.commons.lang3.StringUtils;



public class SplashActivity extends AppCompatActivity {


    private String TAG = "SplashActivity";

    UserKeyDetailsModel userKeyDetailsModel = new UserKeyDetailsModel();
    boolean isSignup;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.splash);

       // SessionStore.saveInformationMode(SplashActivity.this, SessionStore.USER_ID, "2");//only comment this line for dynamic data
        userKeyDetailsModel = Utility.getUserKeyDetails(this);

//        if (!userKeyDetailsModel.getIsDefaultTheme() && !StringUtils.isBlank(userKeyDetailsModel.getThemeColorCode()))
//            Utility.setThemeColor(userKeyDetailsModel.getThemeColorCode());

        transparentToolbar();

        startAnimating();
    }

    // Helper method to start the animation on the splash screen

    private void startAnimating() {
        try {
            // Load animations for all views within the TableLayout
            Animation spinin = AnimationUtils.loadAnimation(this, R.anim.fade_in);
            LayoutAnimationController controller = new LayoutAnimationController(spinin);
            RelativeLayout table = (RelativeLayout) findViewById(R.id.splashLayout);
            table.setLayoutAnimation(controller);
            // Transition to QiblaActivity Menu when bottom title finishes animating
            spinin.setAnimationListener(new AnimationListener() {

                @Override
                public void onAnimationEnd(Animation animation) {
                    // The animation has ended, transition to the QiblaActivity Menu screen
                   /* if (!StringUtils.isBlank(userKeyDetailsModel.getUserGuid())) {
                        MainActivity.goToMainActivity(SplashActivity.this);
                    } else {
                        LogInActivity.goToLogInActivity(SplashActivity.this, false);
                    }*/
                    MainActivity.goToMainActivity(SplashActivity.this);
                    //PhoneAuthActivity.goToPhoneAuthActivity(SplashActivity.this);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }

                @Override
                public void onAnimationStart(Animation animation) {
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            /*finish();
            System.exit(0);*/
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void transparentToolbar() {
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }


}
