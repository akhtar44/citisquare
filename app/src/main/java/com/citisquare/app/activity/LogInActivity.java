package com.citisquare.app.activity;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.citisquare.app.R;
import com.citisquare.app.application.QuickInfo;
import com.citisquare.app.fragments.CityDropDownSeller;
import com.citisquare.app.fragments.TabFragment;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.mediapermission.PermissionsChecker;
import com.citisquare.app.mediapermission.PickMediaActivity;
import com.citisquare.app.model.CommonResponseModel;
import com.citisquare.app.model.LogInEntity;
import com.citisquare.app.model.LoginDataModel;
import com.citisquare.app.model.LoginResponseModel;
import com.citisquare.app.model.SharedPreferencesActivity;
import com.citisquare.app.model.UserKeyDetailsModel;
import com.citisquare.app.pojo.response.City;
import com.citisquare.app.pojo.response.Country;
import com.citisquare.app.pojo.response.LoginData;
import com.citisquare.app.pojo.response.LoginWrapper;
import com.citisquare.app.utils.Constant;
import com.citisquare.app.utils.InitializeMenu;
import com.citisquare.app.utils.RobotoTextView;
import com.citisquare.app.utils.Utility;
import com.facebook.CallbackManager;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


import static com.citisquare.app.activity.MainActivity.mainActivity;


public class LogInActivity extends AppCompatActivity {
    EditText et_username, et_password, nickName, password_signUp, confirm_password, et_phNo,
            email_signUp, et_referral_signIn, et_referral_signUp, et_selectcity;
    RobotoTextView txt_forgot_password, showHidePasswordTV;
    RelativeLayout signIn_btn_layout, signUpLayout, signUp_fulllayout, logIn_fulllayout, signUpLayout_signup, bottomLayout, signUp_btn_layout;
    String userName_logIn, password_logIn, txt_email_Signup, txt_passwordSignUp, txt_confirm_password, txt_phNo, txt_nickName, txt_city;
    String regId;
    RadioButton radio_visitor, radio_seller;
    private String deviceID = "";
    GoogleCloudMessaging gcm;
    String deviceUniqueID;
    SharedPreferences app_preference = null;
    public static final String MyPREFERENCES = null;
    ProgressDialog alert;
    ImageView showHidePasswordImage;
    LoginButton loginButton;
    private CallbackManager callbackManager;
    JSONObject facebookObject = null;
    RelativeLayout language;
    MainActivity mainactivity = new MainActivity();
    SharedPreferencesActivity sharedPreferencesActivity;
    RobotoTextView loginText, signUp_btn;
    ImageView setting_url;
    ProgressDialog progressDialog;
    boolean isLogin = false;
    Intent intent;
    String[] PERMISSIONS = {"android.permission.READ_PHONE_STATE", "android.permission.ACCESS_FINE_LOCATION",
            "android.permission.ACCESS_COARSE_LOCATION"};

    private static final int PERMISSION_REQUEST_CODE = 1;
    private AlertDialog alert11;
    private String ON_RESUME_CALLED_PREFERENCE_KEY = "onResumeCalled";
    AppBarLayout appBarLayout;
    boolean isGPSEnabled = false, isLive = false, isGPSDenied = false;
    Geocoder geocoder;
    LatLng coordinate;
    private LocationManager locationManager;
    List<Address> addresses;
    ArrayList<Country> countries;
    Country country;
    CityDropDownSeller cityDropDown;
    City city;
    InitializeMenu initializeMenu;
    String detailAddress = null, CityName = null, currentLocationPincode = null;

    PickMediaActivity pickMediaActivity = PickMediaActivity.getInstance();


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.log_in);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        app_preference = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = app_preference.edit();


        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

                if (Math.abs(verticalOffset) - appBarLayout.getTotalScrollRange() == 0) {
                    toolbar.setBackgroundResource(R.drawable.gradient_background_home);


                } else {
                    toolbar.setBackgroundResource(R.drawable.border_transparent);


                }
            }
        });

        try {
            progressDialog = Utility.getProgressDialog(LogInActivity.this, getString(R.string.pleaseWait));
            UserKeyDetailsModel userKeyDetailsModel = new UserKeyDetailsModel();
            if (StringUtils.isBlank(userKeyDetailsModel.getUserGuid())) {
                locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                if (!isGPSEnabled) {
                    if (isGPSDenied) {
//                        startAnimating();

                    } else {

                        buildAlertMessageNoGps();

                    }

                } else {
               /* if (currentLocationPincode == null || currentLocationPincode.equalsIgnoreCase("null"))
                    GetDeviceCurrentLocation();
                else if (currentLocationPincode != null && isGPSEnabled && isLive)*/
                    if (currentLocationPincode == null || currentLocationPincode.equalsIgnoreCase("null")) {
                        GetDeviceCurrentLocation();
                    }

//                    startAnimating();
                }
            }


        } catch (Exception ex) {

        }
        sharedPreferencesActivity = SharedPreferencesActivity.getInstance(LogInActivity.this);
        callbackManager = CallbackManager.Factory.create();

        et_username = (EditText) findViewById(R.id.et_username);
        et_password = (EditText) findViewById(R.id.et_password);
        et_referral_signIn = (EditText) findViewById(R.id.et_reffaral_code_signIn);
        showHidePasswordTV = (RobotoTextView) findViewById(R.id.showHidePassword);
        showHidePasswordImage = (ImageView) findViewById(R.id.showHidePasswordImage);
        txt_forgot_password = (RobotoTextView) findViewById(R.id.txt_forgot_password);
        signIn_btn_layout = (RelativeLayout) findViewById(R.id.signIn_btn_layout);
        signUpLayout = (RelativeLayout) findViewById(R.id.signUpLayout);
        signUp_fulllayout = (RelativeLayout) findViewById(R.id.signUp_fulllayout);
        logIn_fulllayout = (RelativeLayout) findViewById(R.id.logIn_full_layout);
        signUpLayout_signup = (RelativeLayout) findViewById(R.id.signUpLayout_signup);
        //bottomLayout = (RelativeLayout) findViewById(R.id.footerLayout);
        signUp_btn = (RobotoTextView) findViewById(R.id.signUp_btn);
        loginText = (RobotoTextView) findViewById(R.id.loginText);
        nickName = (EditText) findViewById(R.id.user_name);
        password_signUp = (EditText) findViewById(R.id.et_password_signup);
        confirm_password = (EditText) findViewById(R.id.et_confirm_password_signup);
//        phNo = (EditText) findViewById(R.id.edit_phNo);
        email_signUp = (EditText) findViewById(R.id.et_email_signup);
        signUp_btn_layout = (RelativeLayout) findViewById(R.id.signUp_btn_layout);
        loginButton = (LoginButton) findViewById(R.id.login_button);
        language = (RelativeLayout) findViewById(R.id.language);
        setting_url = (ImageView) findViewById(R.id.setting_url);
        et_selectcity = (EditText) findViewById(R.id.et_selectcity_code_signup);
        //et_phNo = (EditText) findViewById(R.id.et_phonenumber_code_signup);
        radio_visitor = (RadioButton) findViewById(R.id.radio_visitor);
        radio_seller = (RadioButton) findViewById(R.id.radio_seller);


       /* isLogin = getIntent().getExtras().getBoolean("isLogin");
        if (isLogin) {
            signUp_fulllayout.setVisibility(View.GONE);
            logIn_fulllayout.setVisibility(View.VISIBLE);
        } else {
            signUp_fulllayout.setVisibility(View.VISIBLE);
            logIn_fulllayout.setVisibility(View.GONE);
        }*/


        edit.putBoolean("IsLogInScreen", true);
        edit.commit();
       /* loginButton.setReadPermissions(Arrays.asList(
                 "email", "user_birthday", "user_photos"));*/

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.hyper.quickInfo",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                //   Toast.makeText(getApplicationContext(),Base64.encodeToString(md.digest(), Base64.DEFAULT),Toast.LENGTH_LONG).show();
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        setting_url.setVisibility(View.GONE);
        setting_url.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    //showDialogForUrlChange();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        });


        language.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup();
            }
        });
        showHidePasswordImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String hideText = showHidePasswordTV.getText().toString().trim();
                try {
                    if (!hideText.equalsIgnoreCase("") && hideText.equalsIgnoreCase(getString(R.string.showPasswordText))) {
                        showHidePasswordTV.setText(getString(R.string.hidePasswordText));
                        et_password.setTransformationMethod(null);
                        showHidePasswordImage.setBackgroundResource(R.drawable.hide_pass_icon);
                    } else {
                        showHidePasswordTV.setText(getString(R.string.showPasswordText));
                        et_password.setTransformationMethod(new PasswordTransformationMethod().getInstance());
                        showHidePasswordImage.setBackgroundResource(R.drawable.show_pass_icon);
                    }
                    et_password.setSelection(et_password.length());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        });
        txt_forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String recoveryLink;
                /*if (MedicineMainActivity.TEST_API.equalsIgnoreCase("http://3221-16730.el-alt.com/api/")) {
                    recoveryLink = MedicineMainActivity.PasswordRecoveryLinkProd;
                } else {
                    recoveryLink = MedicineMainActivity.PasswordRecoveryLinkTest;
                }
                Uri uri = Uri.parse(recoveryLink); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);*/
            }
        });

        et_selectcity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                int[] location = new int[2];
                et_selectcity.getLocationOnScreen(location);
                ArrayList<City> cityList = cityArrayList();
                if (cityList != null) {
                    cityDropDown = new CityDropDownSeller(LogInActivity.this, et_selectcity.getHeight(), location, cityList, new ItemEventListener<String>() {
                        @Override
                        public void onItemEventFired(String integer, String t2, String actualPos) {
                            city = new Gson().fromJson(t2, City.class);
                            et_selectcity.setText(city.getName());

                        }
                    });
                    cityDropDown.show();
                }
            }

        });
        radio_visitor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                // TODO Auto-generated method stub
                if (radio_visitor.isChecked()) {
                    radio_visitor.setChecked(true);
                    radio_visitor.setTextColor(getResources().getColor(R.color.colorPrimary));
                    radio_seller.setChecked(false);
                    radio_seller.setTextColor(getResources().getColor(R.color.black));
                } else {
                    radio_visitor.setChecked(false);
                }

            }
        });
        radio_seller.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                // TODO Auto-generated method stub
                if (radio_seller.isChecked()) {
                    radio_seller.setChecked(true);
                    radio_seller.setTextColor(getResources().getColor(R.color.colorPrimary));
                    radio_visitor.setChecked(false);
                    radio_visitor.setTextColor(getResources().getColor(R.color.black));
                } else {
                    radio_seller.setChecked(false);
                }

            }
        });
        signIn_btn_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    userName_logIn = et_username.getText().toString().trim();
                    password_logIn = et_password.getText().toString().trim();

                    if (StringUtils.isBlank(userName_logIn)) {
                        Utility.ShowToast(LogInActivity.this, getResources().getString(R.string.enterEmailOrmobile));
                    } else if (StringUtils.isBlank(password_logIn)) {
                        Utility.ShowToast(LogInActivity.this, getResources().getString(R.string.enterPassword));
                    } else {
                        Utility.hideKeyboard(LogInActivity.this);
                        boolean isLive = MainActivity.getMainActivityInstance().isInternetConnected(LogInActivity.this);
                        boolean isPhoneNumeric = StringUtils.isNumeric(password_logIn);

                        LogInEntity logInEntity = new LogInEntity();
                        logInEntity.setLogin_type(userName_logIn);
                        logInEntity.setPassword(password_logIn);
                        logInEntity.setDeviceToken(deviceUniqueID);
                        logInEntity.setDeviceType("Android");

                        if (isLive) {
                            Utility.hideKeyboard(LogInActivity.this);
                            login(logInEntity);
                        } else {
                            Utility.ShowToast(LogInActivity.this, getResources().getString(R.string.checkInternetCon));
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        signUpLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logIn_fulllayout.setVisibility(View.GONE);
                //  bottomLayout.setVisibility(View.GONE);
                signUp_fulllayout.setVisibility(View.VISIBLE);

            }
        });
        signUpLayout_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUp_fulllayout.setVisibility(View.GONE);
                logIn_fulllayout.setVisibility(View.VISIBLE);
                //  bottomLayout.setVisibility(View.GONE);

            }
        });

        signUp_btn_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String json = "";
                txt_nickName = nickName.getText().toString().trim();
                txt_email_Signup = email_signUp.getText().toString().trim();
                txt_passwordSignUp = password_signUp.getText().toString();
                txt_confirm_password = confirm_password.getText().toString();
                txt_city = et_selectcity.getText().toString();

                boolean isEmailValid = isValidEmail(txt_email_Signup);
                boolean isPhoneNumeric = StringUtils.isNumeric(txt_email_Signup);
                boolean isPhoneValid = isValidPhoneNumberr(txt_email_Signup);

                if (StringUtils.isBlank(txt_nickName) && StringUtils.isBlank(txt_email_Signup)
                        && StringUtils.isBlank(txt_passwordSignUp) && StringUtils.isBlank(txt_city)) {
                    Utility.ShowToast(LogInActivity.this, getResources().getString(R.string.signuperrortext));
                } else if (StringUtils.isBlank(txt_nickName)) {
                    Utility.ShowToast(LogInActivity.this, getResources().getString(R.string.enterName));
                } else if (StringUtils.isBlank(txt_email_Signup)) {
                    Utility.ShowToast(LogInActivity.this, getResources().getString(R.string.enterEmailOrmobile));
                } else if (isPhoneNumeric && !isPhoneValid) {
                    Utility.ShowToast(LogInActivity.this, getResources().getString(R.string.enterValidMobile));
                } else if (!isPhoneNumeric && !isEmailValid) {
                    Utility.ShowToast(LogInActivity.this, getResources().getString(R.string.enterValidEmail));
                } else if (StringUtils.isBlank(txt_passwordSignUp)) {
                    Utility.ShowToast(LogInActivity.this, getResources().getString(R.string.enterPassword));
                } else if (StringUtils.length(txt_passwordSignUp) < 6) {
                    Utility.ShowToast(LogInActivity.this, "Password length should be greater than 5");
                } else if (StringUtils.isBlank(txt_confirm_password)) {
                    Utility.ShowToast(LogInActivity.this, getResources().getString(R.string.confirmyourPass));
                } else if (!StringUtils.equals(txt_passwordSignUp, txt_confirm_password)) {
                    Utility.ShowToast(LogInActivity.this, "Password didn't match");
                } else {

                    Utility.hideKeyboard(LogInActivity.this);
                    boolean isLive = MainActivity.getMainActivityInstance().isInternetConnected(LogInActivity.this);

                    LogInEntity logInEntity = new LogInEntity();
                    logInEntity.setPassword(txt_passwordSignUp);
                    logInEntity.setCity(txt_city);
                    logInEntity.setUsername(txt_nickName);

                    if (isPhoneNumeric) {
                        logInEntity.setEmail("");
                        logInEntity.setPhoneNumber(txt_email_Signup);
                    } else {
                        logInEntity.setPhoneNumber("");
                        logInEntity.setEmail(txt_email_Signup);
                    }

                    if (radio_seller.isChecked())
                        logInEntity.setRole("S");
                    else
                        logInEntity.setRole("V");

                    if (isLive) {
                        signUp(logInEntity);
                    } else {
                        Utility.ShowToast(LogInActivity.this, getResources().getString(R.string.checkInternetCon));
                    }
                }
            }
        });
    }

    private void signUp(final LogInEntity logInEntity) {
        Utility.showDialog(progressDialog);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.POST_SIGN_UP, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                String finalResp = "";
                try {
                    if (!StringUtils.isBlank(response)) {

                        if (response.contains("{") && response.contains("}")) {
                            finalResp = getMessage(response);
                        }

                        CommonResponseModel commonResponseModel = Utility.getGson().fromJson(finalResp, CommonResponseModel.class);
                        if (commonResponseModel.getStatus() == 1) {
                            //saveUserDetails(logInEntity);
                            Utility.ShowToast(LogInActivity.this, "Signup Successfull, Please login");
                            //isLogin = true;
                            LogInActivity.goToLogInActivity(LogInActivity.this, true);
                        } else {
                            Utility.ShowToast(LogInActivity.this, "Sign up failed");
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                Utility.dismissDialog(progressDialog);
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String resp = "";
                        try {
                            Utility.dismissDialog(progressDialog);
                            NetworkResponse response = error.networkResponse;
                            String json = new String(response.data);
                            if (json.contains("{") && json.contains("}")) {
                                resp = getMessage(json);
                            }
                            JSONObject jsonObject = new JSONObject(resp);
                            String message = jsonObject.getString("message");
                            if (message != null)
                                Utility.showAlertDialogSingleButton(LogInActivity.this, message);
                            else
                                Utility.showVolleyAlertDialogSingleButton(LogInActivity.this, error);
                            //Utility.onErrorResponseMessage(LogInActivity.this, error);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("API-KEY", Constants.X_API_KEY);
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("city", logInEntity.getCity());
                headers.put("email", logInEntity.getEmail());
                headers.put("password", logInEntity.getPassword());
                headers.put("phone_no", logInEntity.getPhoneNumber());
                headers.put("username", logInEntity.getUsername());
                headers.put("role", logInEntity.getRole());
                return headers;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                600000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void login(final LogInEntity logInEntity) {
        try {

            Utility.showDialog(progressDialog);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.POST_LOGIN, new Response.Listener<String>() {
                @SuppressLint("ResourceType")
                @Override
                public void onResponse(String response) {
                    String finalResp = "";
                    try {
                        if (!StringUtils.isBlank(response)) {

                            if (response.contains("{") && response.contains("}")) {
                                //finalResp = getMessage(response);
                                finalResp =response.substring(2, response.length());
                            }

                            LoginWrapper responseModelData = Utility.getGson().fromJson(finalResp, LoginWrapper.class);
                            if (responseModelData.getStatus() == 1) {

                                //saveUserDetails(responseModelData.getData());

                                Utility.ShowToast(LogInActivity.this, responseModelData.getMessage());

                                if (!StringUtils.isBlank(responseModelData.getData().getId())) {
                                    //MainActivity.goToMainActivity(LogInActivity.this);
                                    MainActivity.getMainActivityInstance().replaceParentFragment(new TabFragment(), "parent");
                                    MainActivity.getMainActivityInstance().setData(LogInActivity.this, new Gson().toJson(responseModelData));
                                    MainActivity.getMainActivityInstance().setUserSession(LogInActivity.this, responseModelData.getData().getUserSessionId());
                                }
                                else {
                                    LogInActivity.goToLogInActivity(LogInActivity.this, isLogin);
                                }
                            } else {
                                Utility.ShowToast(LogInActivity.this, "Login failed");
                            }
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    Utility.dismissDialog(progressDialog);
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            String resp = "";
                            try {
                                Utility.dismissDialog(progressDialog);
                                NetworkResponse response = error.networkResponse;
                                String json = new String(response.data);
                                if (json.contains("{") && json.contains("}")) {
                                    resp = getMessage(json);
                                }
                                JSONObject jsonObject = new JSONObject(resp);
                                String message = jsonObject.getString("message");
                                if (message != null)
                                    Utility.showAlertDialogSingleButton(LogInActivity.this, message);
                                else
                                    Utility.showVolleyAlertDialogSingleButton(LogInActivity.this, error);
                                //Utility.onErrorResponseMessage(LogInActivity.this, error);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<String, String>();
                    headers.put("API-KEY", Constants.X_API_KEY);
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<String, String>();
                    headers.put("login_type", logInEntity.getLogin_type());
                    headers.put("password", logInEntity.getPassword());
                    headers.put("device_type", logInEntity.getDeviceType());
                    headers.put("device_token", logInEntity.getDevice_token());
                    return headers;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    500000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            // requestQueue.add(stringRequest);
            QuickInfo.getInstance().addToRequestQueue(stringRequest);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public String getMessage(String input) {

        String message = "";
        try {
            int open = input.indexOf("{");
            int close = input.indexOf("}");
            if (open != -1 && close != -1) {
                message = "{" + input.substring(open + 1, close) + "}";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return message;
    }

    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public ArrayList<City> cityArrayList() {
        return initializeMenu.toGetCityCode();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void showToastMsg(String response) {
        Toast.makeText(getApplicationContext(), "Oops.. Time out! It's not you, its us.. Please give another try?", Toast.LENGTH_LONG).show();
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target)
                && android.util.Patterns.EMAIL_ADDRESS.matcher(target)
                .matches();
    }

    public final static boolean isValidPhoneNumberr(String target) {
        if (target == null) {
            return false;
        } else {
            if (target.length() < 10 || target.length() > 10) {
                return false;
            } else {
                return android.util.Patterns.PHONE.matcher(target).matches();
            }
        }
    }

    public void showPopup() {
        try {
            PopupMenu popup = new PopupMenu(this, language);
            MenuInflater inflater = popup.getMenuInflater();
            inflater.inflate(R.menu.language, popup.getMenu());

            Menu menu = popup.getMenu();

            Typeface face = Typeface.createFromAsset(getAssets(), "fonts/Roboto_Regular.ttf");    //  THIS
            SpannableStringBuilder englishtitle = new SpannableStringBuilder(getResources().getString(R.string.english));
            SpannableStringBuilder arabicTitle = new SpannableStringBuilder(getResources().getString(R.string.arabic_english));
            arabicTitle.setSpan(face, 0, arabicTitle.length(), 0);


            final MenuItem english = (MenuItem) menu.findItem(R.id.english);
            english.setTitle(englishtitle);
            final MenuItem arabic = (MenuItem) menu.findItem(R.id.arabic);
            arabic.setTitle(arabicTitle);

            if (app_preference.getString("language", "").equalsIgnoreCase("Arabic")) {
                english.setIcon(R.drawable.ic_done_transparent);
                arabic.setIcon(R.drawable.ic_done_black_24dp);
            } else {
                english.setIcon(R.drawable.ic_done_black_24dp);
                arabic.setIcon(R.drawable.ic_done_transparent);
            }

            english.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {

                    mainActivity.saveLanguage(LogInActivity.this, false, app_preference);
                    refreshResource();
                    english.setIcon(R.drawable.ic_done_black_24dp);
                    arabic.setIcon(R.drawable.ic_done_transparent);
                    return true;
                }
            });

            arabic.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {

                    mainActivity.saveLanguage(LogInActivity.this, true, app_preference);
                    refreshResource();
                    english.setIcon(R.drawable.ic_done_transparent);
                    arabic.setIcon(R.drawable.ic_done_black_24dp);
                    return true;
                }
            });
            mainActivity.setForceShowIconForPopup(popup);
            popup.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void refreshResource() {

        startActivity(new Intent(LogInActivity.this, LogInActivity.class));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (alert != null && alert.isShowing()) {
            alert.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (alert != null && alert.isShowing()) {
            alert.dismiss();
        }
    }


    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                try {
                    boolean isPhoneStateDone = false,
                            isAccessFineDone = false, isAccessCoarseDone = false,
                            isShowRationalePhoneState = false,
                            isShowRationaleAccessFine = false, isShowRationaleAccessCoarse = false;
                    for (int i = 0; i < permissions.length; i++) {
                        String permission = permissions[i];
                        int grantResult = grantResults[i];
                        if (permission.equals(android.Manifest.permission.READ_PHONE_STATE)) {
                            isShowRationalePhoneState = ActivityCompat.shouldShowRequestPermissionRationale(this, permission);
                            if (grantResult == PackageManager.PERMISSION_GRANTED) {
                                isPhoneStateDone = true;
                            } else {
                                isPhoneStateDone = false;
                            }
                        } else if (permission.equals(android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                            isShowRationaleAccessFine = ActivityCompat.shouldShowRequestPermissionRationale(this, permission);
                            if (grantResult == PackageManager.PERMISSION_GRANTED) {
                                isAccessFineDone = true;
                            } else {
                                isAccessFineDone = false;
                            }
                        } else if (permission.equals(android.Manifest.permission.ACCESS_COARSE_LOCATION)) {
                            isShowRationaleAccessCoarse = ActivityCompat.shouldShowRequestPermissionRationale(this, permission);
                            if (grantResult == PackageManager.PERMISSION_GRANTED) {
                                isAccessCoarseDone = true;
                            } else {
                                isAccessCoarseDone = false;
                            }
                        }
                    }

                    boolean isRationale = (isShowRationalePhoneState || isShowRationaleAccessFine ||
                            isShowRationaleAccessCoarse);
                    if (!isRationale && (!isPhoneStateDone
                            || !isAccessFineDone || !isAccessCoarseDone)) {
                        showSettingsDialog();
                    } else {
                        refreshResource();
                    }
                    break;
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void showSettingsDialog() {
        android.support.v7.app.AlertDialog.Builder builder1 = new android.support.v7.app.AlertDialog.Builder(LogInActivity.this);

        builder1.setMessage(getResources().getString(R.string.permissionStatement));

        builder1.setPositiveButton(getResources().getString(R.string.settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


                try {
                    marshmallowSetting();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                alert11.dismiss();

            }
        });

        alert11 = builder1.create();
        alert11.show();
        alert11.getButton(alert11.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.AppThemeColor));
        alert11.getButton(alert11.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.AppThemeColor));
    }

    private void marshmallowSetting() {
        try {
            Intent intent = new Intent();
            intent.setAction(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", getPackageName(), null);
            intent.setData(uri);
            startActivity(intent);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        PermissionsChecker checker = new PermissionsChecker(LogInActivity.this);
        if (!checker.lacksPermissions(Constant.PERMISSIONS_LOCATION)) {
            pickMediaActivity.SetToSharePreference(LogInActivity.this, getString(R.string.locationNeverAskAgain), false);
        }
        boolean isPhoneStateAllow = pickMediaActivity.checkPermission(LogInActivity.this, Constant.PERMISSIONS_LOCATION, getString(R.string.locationNeverAskAgain));

        if (isPhoneStateAllow) {
            TelephonyManager mTelephonyMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            deviceUniqueID = mTelephonyMgr.getDeviceId();
            if (StringUtils.isBlank(deviceUniqueID)) {
                deviceUniqueID = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
            }
            if (StringUtils.isBlank(deviceUniqueID))
                deviceUniqueID = "";
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // Save current onResumeCalledAlready state
        super.onSaveInstanceState(outState);
    }

    private void GetDeviceCurrentLocation() {

        try {
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

            if (Build.VERSION.SDK_INT >= 23 && checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && checkSelfPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }


// finally require updates at -at least- the desired rate
            long minTimeMillis = 600000; // 600,000 milliseconds make 10 minutes
            //  locationManager.requestLocationUpdates(myProvider,minTimeMillis,0,locListener);

            Location location1 = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (location != null) {
                onLocationChanged(new LatLng(location.getLatitude(), location.getLongitude()));
            } else if (location1 != null) {
                onLocationChanged(new LatLng(location1.getLatitude(), location1.getLongitude()));

            }
            // locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 20000, 0, locListener);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void onLocationChanged(LatLng latLng) {

        String detailAddressShowing = null, localArea = null, fullAddress;
        double latitude = latLng.latitude;
        double longitude = latLng.longitude;
        SharedPreferences.Editor edit;
        coordinate = new LatLng(latitude, longitude);
        geocoder = new Geocoder(LogInActivity.this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            //String area = addresses.get(0).getSubLocality();
            localArea = addresses.get(0).getAddressLine(0);
            CityName = addresses.get(0).getLocality();
            String stateName = addresses.get(0).getAdminArea();
            String countryCode = addresses.get(0).getCountryCode();
            String countryName = addresses.get(0).getCountryName();
            String pincode = addresses.get(0).getPostalCode();
            double getlatitude = addresses.get(0).getLatitude();
            double getlongitude = addresses.get(0).getLongitude();

            if (localArea == null)
                localArea = addresses.get(0).getAddressLine(1);
            String colony = addresses.get(0).getSubLocality();
            fullAddress = localArea;
            if (CityName != null && !CityName.equalsIgnoreCase("null"))
                fullAddress = fullAddress + ", " + CityName;
            if (stateName != null && !stateName.equalsIgnoreCase("null"))
                fullAddress = fullAddress + ", " + stateName;
            if (countryName != null && !countryName.equalsIgnoreCase("null"))
                fullAddress = fullAddress + ", " + countryName;


            edit = app_preference.edit();
            edit.putString("CountryName", countryName);
            edit.putString("CountryCode", countryCode);
            edit.putString("LocalName", localArea);
            edit.putString("StateName", stateName);
            edit.putString("NewCityName", CityName);
            edit.putString("NewStateName", stateName);
            edit.putString("NewLandMark", colony);
            edit.putString("NewLocalName", localArea);
            edit.putString("Longitude", String.valueOf(getlongitude));
            edit.putString("Latitude", String.valueOf(getlatitude));
            if (StringUtils.isBlank(app_preference.getString("CurrentAddId", null))) {
                edit.putString("CurrentAddId", "gps");
            }
            edit.commit();

            if (pincode != null) {
                fullAddress = fullAddress + " - " + pincode;
                edit = app_preference.edit();
                edit.putString("Pincode", pincode);
                edit.putString("NewPincode", pincode);
                edit.putString("FullAddress", fullAddress);
                edit.commit();
            } else {
                for (int i = 0; i < 10; i++) {
                    String addressDetails = addresses.get(0).getAddressLine(i);
                    if (addressDetails != null) {
                        String pinCode = mainActivity.containsPinCode(addressDetails);
                        if (pinCode != null && !pinCode.equalsIgnoreCase("")) {
                            fullAddress = fullAddress + " - " + pinCode;
                            edit = app_preference.edit();
                            edit.putString("Pincode", pinCode);
                            edit.putString("NewPincode", pinCode);
                            edit.putString("FullAddress", fullAddress);
                            edit.commit();
                            break;
                        }

                    } else
                        break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void buildAlertMessageNoGps() {
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this, R.style.MyAlertDialogStyle);
        builder.setMessage(
                getResources().getString(R.string.enableGps))
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    @SuppressWarnings("unused") final DialogInterface dialog,
                                    @SuppressWarnings("unused") final int id) {
                                startActivity(new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                /*if (isLive)
                                    GetDeviceCurrentLocation();*/
                            }
                        })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog,
                                                @SuppressWarnings("unused") final int id) {
                                dialog.cancel();
                                isGPSDenied = true;
                                onResume();
                            }
                        }
                );
        final android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    public static void goToLogInActivity(Context context, boolean isSignup) {
        Intent intent = new Intent(context, LogInActivity.class);
        intent.putExtra("isLogin", isSignup);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    private void saveUserDetails(LoginDataModel loginDataModel) {
        try {

            UserKeyDetailsModel userKeyDetailsModel = new UserKeyDetailsModel();

            userKeyDetailsModel.setUserGuid(loginDataModel.getId());
            userKeyDetailsModel.setUsername(loginDataModel.getUsername());
            userKeyDetailsModel.setEmail(loginDataModel.getEmail());
            userKeyDetailsModel.setPhoneNumber(loginDataModel.getPhone_no());
            userKeyDetailsModel.setProfile_image(loginDataModel.getProfile_image());

            userKeyDetailsModel.setFirst_name(loginDataModel.getFirst_name());
            userKeyDetailsModel.setLast_name(loginDataModel.getLast_name());

            userKeyDetailsModel.setBranch_name(loginDataModel.getBranch_name());
            userKeyDetailsModel.setUser_session_id(loginDataModel.getUser_session_id());
            userKeyDetailsModel.setCategory_count(loginDataModel.getCategory_count());
            userKeyDetailsModel.setLanguage(loginDataModel.getLanguage());
            userKeyDetailsModel.setDevice_token(loginDataModel.getDevice_token());

            userKeyDetailsModel.setBuilding_no(loginDataModel.getBuilding_no());
            userKeyDetailsModel.setStreet(loginDataModel.getStreet());
            userKeyDetailsModel.setCity(loginDataModel.getCity());
            userKeyDetailsModel.setZipcode(loginDataModel.getZipcode());
            userKeyDetailsModel.setCountry(loginDataModel.getCountry());


            Utility.saveUserDetails(LogInActivity.this, userKeyDetailsModel);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
