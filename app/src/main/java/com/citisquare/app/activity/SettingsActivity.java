package com.citisquare.app.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.citisquare.app.R;

/**
 * Created by Akhtar on 7/2/2018.
 */

public class SettingsActivity extends AppCompatActivity {
    public static MainActivity mainActivityInstance;
    Bundle bundle;

    public static MainActivity getMainActivityInstance() {
        return mainActivityInstance;
    }

    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_setting);
    }
}
