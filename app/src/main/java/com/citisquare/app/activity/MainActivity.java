package com.citisquare.app.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.citisquare.app.R;
import com.citisquare.app.adapter.MenuListAdapter;
import com.citisquare.app.application.QuickInfo;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.dialog.ChangePasswordDialog;
import com.citisquare.app.dialog.ForgotDialog;
import com.citisquare.app.dialog.JobApplyDialog;
import com.citisquare.app.error.ExceptionHandler;
import com.citisquare.app.fragments.AccountSettings;
import com.citisquare.app.fragments.AddBranchFragment;
import com.citisquare.app.fragments.BaseFragment;
import com.citisquare.app.fragments.BranchPlanFragment;
import com.citisquare.app.fragments.CategorySelectionFragment;
import com.citisquare.app.fragments.ChatFragment;
import com.citisquare.app.fragments.ChildCategoriesFragment;
import com.citisquare.app.fragments.CityDropDown;
import com.citisquare.app.fragments.CommentFragment;
import com.citisquare.app.fragments.ContactUsFragment;
import com.citisquare.app.fragments.InboxFragment;
import com.citisquare.app.fragments.LoginFragment;
import com.citisquare.app.fragments.NetworkConnectionFragment;
import com.citisquare.app.fragments.NotificationFragment;
import com.citisquare.app.fragments.PostAdsFragment;
import com.citisquare.app.fragments.PostOnTimeFragment;
import com.citisquare.app.fragments.PostSliderFragment;
import com.citisquare.app.fragments.PostTodayOfferFragment;
import com.citisquare.app.fragments.ProfileFragment;
import com.citisquare.app.fragments.SellerFragment;
import com.citisquare.app.fragments.SendMessageFragment;
import com.citisquare.app.fragments.SendQueryFragment;
import com.citisquare.app.fragments.SettingFragment;
import com.citisquare.app.fragments.SubscriptionPlanFragment;
import com.citisquare.app.fragments.TabFragment;
import com.citisquare.app.fragments.VisitorFragment;
import com.citisquare.app.gcm.RegistrationIntentService;
import com.citisquare.app.interfaces.ConnectionListener;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.interfaces.Navigation;
import com.citisquare.app.interfaces.PaymentCallBack;
import com.citisquare.app.interfaces.Rendering;
import com.citisquare.app.model.KeyvaluePair;
import com.citisquare.app.pojo.response.BadgeCounterData;
import com.citisquare.app.pojo.response.BadgeCounterWrapper;
import com.citisquare.app.pojo.response.CategoryData;
import com.citisquare.app.pojo.response.City;
import com.citisquare.app.pojo.response.Country;
import com.citisquare.app.pojo.response.Like;
import com.citisquare.app.pojo.response.LoginData;
import com.citisquare.app.pojo.response.LoginWrapper;
import com.citisquare.app.pojo.response.LogoutWrapper;
import com.citisquare.app.pojo.response.PushNotification;
import com.citisquare.app.pojo.response.SubCategory;
import com.citisquare.app.utils.CircleTransformation;
import com.citisquare.app.utils.DataToPref;
import com.citisquare.app.utils.Debugger;
import com.citisquare.app.utils.InitializeMenu;
import com.citisquare.app.utils.NetworkStateListener;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.payfort.fort.android.sdk.base.FortSdk;
import com.payfort.fort.android.sdk.base.callbacks.FortCallBackManager;
import com.payfort.fort.android.sdk.base.callbacks.FortCallback;
import com.payfort.sdk.android.dependancies.base.FortInterfaces;
import com.payfort.sdk.android.dependancies.models.FortRequest;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import serviceCalling.builder.KeyValuePair;
import serviceCalling.builder.ServiceParameter;
import serviceCalling.helper.GetRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

import static com.citisquare.app.R.id.framelayoutContainer;

public class MainActivity extends AppCompatActivity implements Navigation, ConnectionListener {


    private static final String TAG = "MAINACTIVITY";
    public static MainActivity mainActivityInstance;
    static MainActivity mainActivity;
    static int TIME_OUT = 3000;
    public Picasso picasso = QuickInfo.providePicasso();
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.listViewMenu)
    ExpandableListView listViewMenu;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.menuImage)
    ImageView menuImage;
    @BindView(R.id.backImage)
    ImageView backImage;
    @BindView(R.id.toolBarTitle)
    TextView toolBarTitle;
    @BindView(R.id.message)
    ImageView message;
    @BindView(R.id.setting)
    ImageView setting;
    @BindView(R.id.settingMessageLayout)
    LinearLayout settingMessageLayout;
    @BindView(R.id.save)
    TextView save;
    @BindView(R.id.linearLayoutToolbar)
    FrameLayout linearLayoutToolbar;
    @BindView(R.id.placeholder)
    FrameLayout placeholder;
    @BindView(R.id.groupMessageSend)
    ImageView groupMessageSend;
    List<String> strings;
    float lastTranslate = 0.0f;
    @BindView(R.id.topCitySelection)
    TextView topCitySelection;
    HashMap<String, List<String>> listDataChild;
    ArrayList<City> cityList;
    MenuHeader menuHeader;
    CityDropDown cityDropDown;
    MenuListAdapter menuListAdapter;
    int selectionPostion;
    int catePos = -1;
    int filterPos = -1;
    @BindView(R.id.send)
    TextView send;
    ProgressDialog getProgressDialog;
    NetworkStateListener networkStateListener;
    InitializeMenu initializeMenu;
    @BindView(R.id.saveLayout)
    LinearLayout saveLayout;
    @BindView(R.id.sendLayout)
    LinearLayout sendLayout;
    List<KeyvaluePair> menuList;
    String actionBarCityId = "";
    LatLng latLng;
    Enum.setUser userForWholeApp;
    String otherProfileUserId = "";
    String setChatDateTime = "";
    boolean isComeFirst;
    Handler handler;
    Runnable runnable;
    String cityName = "", citylatitude, citylongitude;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    ServiceParameter param;
    List<KeyValuePair> keyValuePairList;
    @BindView(R.id.profileImage)
    ImageView profileImage;
    @BindView(R.id.addBranch)
    ImageView addBranch;
    View headerView;
    @BindView(R.id.titleLogo)
    ImageView titleLogo;
    @BindView(R.id.badgeCountToolbar)
    ImageView badgeCountToolbar;
    @BindView(R.id.bageCounterToolbar)
    CustomTextView bageCounterToolbar;
    BadgeCounterData badgeCounterData;
    @BindView(R.id.review)
    CustomTextView review;
    @BindView(R.id.writeReview)
    LinearLayout writeReview;
    NavigationView nav_view;
    private ProgressDialog progressDialog;
    private int branchPos = -1;
    private FortCallBackManager fortCallback = null;
    private PaymentCallBack paymentCallBack;

    private String currentFragment = "";


    public static MainActivity getMainActivityInstance() {
        if (mainActivityInstance == null) {
            mainActivityInstance = new MainActivity();
        }
        return mainActivityInstance;
    }

    public static MainActivity getInstance() {
        if (mainActivity == null) {
            mainActivity = (MainActivity) new Activity();
        }
        return mainActivity;
    }

    public void setPaymentCallBack(PaymentCallBack paymentCallBack) {
        this.paymentCallBack = paymentCallBack;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        Debugger.e(":::->->ACTIVITY CREATED:::");

        Intent intent = new Intent(MainActivity.this, RegistrationIntentService.class);
        startService(intent);

        MobileAds.initialize(this, getString(R.string.admob_app_id));
        fortCallback = FortCallback.Factory.create();

        mainActivityInstance = this;

        //To register error crash log report handler
        ExceptionHandler.register(this, Constants.ERRORLINK);
        initializeMenu = new InitializeMenu();
        initializeMenu.init(this);

        progressDialog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(R.string.loading));

        listDataChild = new HashMap<String, List<String>>();
        List<String> nowShowing = new InitializeMenu().toGetSubMenu(this);

        strings = new ArrayList<>();
        setMenuBar(new InitializeMenu().toGetSubMenu(this), new InitializeMenu().toGetMenuList(this));
        ExpandableListView navigationView = (ExpandableListView) findViewById(R.id.listViewMenu);

        headerView = View.inflate(this, R.layout.menu_profile_header, null);
        menuHeader = new MenuHeader(headerView);
        listViewMenu.addHeaderView(headerView, null, false);
        //listViewMenu.setAdapter(menuListAdapter);

        listViewMenu.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                hideKeyboard();
                if (groupPosition == 7) {
                    //listViewMenu.collapseGroup(4);
                    drawerToggle();
                    switch (childPosition) {
                        /*case 0:
                            if (getUserForWholeApp().equals(Enum.setUser.GUEST)) {
                                messageAlert(getString(R.string.message_Guest));

                            } else if (getUserForWholeApp().equals(Enum.setUser.VISITOR)) {
                                messageAlert(getString(R.string.message_visitor));
                            } else
                                setFragment(new PostJobFragment(), true, "");
                            break;*/
                        case 0:
                            if (getUserForWholeApp().equals(Enum.setUser.GUEST)) {
                                messageAlert(getString(R.string.message_Guest));
                            } else if (getUserForWholeApp().equals(Enum.setUser.VISITOR)) {
                                messageAlert(getString(R.string.message_visitor));
                            } else
                                setFragment(new PostOnTimeFragment(), true, "");
                            break;
                        case 1:
                            if (getUserForWholeApp().equals(Enum.setUser.GUEST)) {
                                messageAlert(getString(R.string.message_Guest));
                            } else if (getUserForWholeApp().equals(Enum.setUser.VISITOR)) {
                                messageAlert(getString(R.string.message_visitor));
                            } else
                                setFragment(new PostTodayOfferFragment(), true, "");
                            break;
                        case 2:
                            if (getUserForWholeApp().equals(Enum.setUser.GUEST)) {
                                messageAlert(getString(R.string.message_Guest));
                            } else if (getUserForWholeApp().equals(Enum.setUser.VISITOR)) {
                                messageAlert(getString(R.string.message_visitor));
                            } else
                                setFragment(new PostSliderFragment(), true, "");
                            break;
                    }
                }
                return false;
            }
        });

        BaseFragment baseFragmentData = (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.placeholder);
        if (baseFragmentData != null) {
            baseFragmentData.setHeaderTitle();
            Debugger.e(" ::::::::::::: " + baseFragmentData.getClass().getName());
        }


        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                BaseFragment baseFragment = (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.placeholder);
                if (baseFragment != null) {
                    baseFragment.setHeaderTitle();
                    Debugger.e(" ::::::::::::: " + baseFragment.getClass().getName());
                }
            }
        });

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @SuppressLint("NewApi")
            public void onDrawerSlide(View drawerView, float slideOffset) {
                float moveFactor = (drawerView.getWidth() * slideOffset);
                hideKeyboard();


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    toolbar.setTranslationX(moveFactor);
                    placeholder.setTranslationX(moveFactor);
                } else {
                    TranslateAnimation anim = new TranslateAnimation(lastTranslate, moveFactor, 0.0f, 0.0f);
                    anim.setDuration(0);
                    anim.setFillAfter(true);
                    toolbar.startAnimation(anim);
                    placeholder.startAnimation(anim);
                    lastTranslate = moveFactor;
                }
            }
        };
        drawer.setDrawerListener(toggle);


        listViewMenu.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                BaseFragment fragment = (BaseFragment) getSupportFragmentManager().
                        findFragmentById(R.id.placeholder);
                hideKeyboard();
                Debugger.e(":::GROUP POSITION:::" + groupPosition);
                setSelectionPostion(groupPosition);
                if (getUserForWholeApp().equals(Enum.setUser.GUEST)) {
                    if (groupPosition == 0) {
                        drawerToggle();
                        if (fragment instanceof TabFragment) {
                            Debugger.e("CALL HOME");
                            ((TabFragment) fragment).onPageSelected(0);
                        }
                    } else if (groupPosition == 1) {
                        drawerToggle();
                        setFragment(new SettingFragment(), false, "");
                    } else if (groupPosition == 2) {
                        drawerToggle();
                        setFragment(new ContactUsFragment(), false, "");
                    } else if (groupPosition == 3) {
                        drawerToggle();
                        //setFragment(new ContactUsFragment(), false, "");
                        SubscriptionPlanFragment subscriptionPlanFragment = new SubscriptionPlanFragment();
                        Bundle bundleSubscription = new Bundle();
                        bundleSubscription.putString("from", "menu");
                        bundleSubscription.putString("guest", "guest");
                        subscriptionPlanFragment.setArguments(bundleSubscription);
                        setFragment(subscriptionPlanFragment, false, "");
                    } else if (groupPosition == 4) {
                        drawerToggle();

                        if (getUserForWholeApp().equals(Enum.setUser.GUEST)) {
                            DataToPref.clear(MainActivity.this);
                            removeAllNotification();
                            replaceParentFragment(new LoginFragment(), "");
                            //Intent intent = new Intent(getApplicationContext(), LogInActivity.class);
                            //startActivity(intent);
                        } else {
                            alertTwoAction(getString(R.string.message_areYouSureYouWantToLogout), new Rendering() {
                                @Override
                                public void response(boolean isCofirm) {
                                    if (isCofirm) {

                                        logoutWS();
                                    }
                                }
                            });
                        }
                    }
                } else if (getUserForWholeApp().equals(Enum.setUser.VISITOR)) {
                    if (groupPosition == 0) {
                        drawerToggle();
                        if (fragment instanceof TabFragment) {
                            Debugger.e("CALL HOME");
                            ((TabFragment) fragment).onPageSelected(0);
                        }
                    } else if (groupPosition == 1) {
                        drawerToggle();

                        if (getUserForWholeApp().equals(Enum.setUser.GUEST)) {
                            messageAlertLogin(getString(R.string.message_Guest));
                        } else if (getUserForWholeApp().equals(Enum.setUser.SELLER_BASIC)) {
                            messageAlert(getString(R.string.message_visitor));
                        } else
                            setFragment(new NotificationFragment(), false, "");
                    } else if (groupPosition == 2) {
                        drawerToggle();
                        if (getUserForWholeApp().equals(Enum.setUser.GUEST)) {
                            messageAlertLogin(getString(R.string.message_Guest));
                        } else if (getUserForWholeApp().equals(Enum.setUser.SELLER_BASIC)) {
                            messageAlert(getString(R.string.message_visitor));
                        } else {
                            setFragment(new InboxFragment(), false, "");
                        }
                    } else if (groupPosition == 3) {
                        drawerToggle();
                        setFragment(new SettingFragment(), false, "");
                    } else if (groupPosition == 4) {
                        drawerToggle();
                        setFragment(new ContactUsFragment(), false, "");
                    } else if (groupPosition == 5) {
                        drawerToggle();
                        setFragment(new AccountSettings(), false, "");
                    } else if (groupPosition == 6) {
                        drawerToggle();

                        if (getUserForWholeApp().equals(Enum.setUser.GUEST)) {
                            DataToPref.clear(MainActivity.this);
                            removeAllNotification();
                            replaceParentFragment(new LoginFragment(), "");
                        } else {
                            alertTwoAction(getString(R.string.message_areYouSureYouWantToLogout), new Rendering() {
                                @Override
                                public void response(boolean isCofirm) {
                                    if (isCofirm) {

                                        logoutWS();
                                    }
                                }
                            });
                        }
                    }
                } else {
                    if (groupPosition == 0) {
                        drawerToggle();
                        if (fragment instanceof TabFragment) {
                            Debugger.e("CALL HOME");
                            ((TabFragment) fragment).onPageSelected(0);
                        }
                    } else if (groupPosition == 1) {
                        drawerToggle();

                        if (getUserForWholeApp().equals(Enum.setUser.GUEST)) {
                            messageAlertLogin(getString(R.string.message_Guest));
                        } else if (getUserForWholeApp().equals(Enum.setUser.SELLER_BASIC)) {
                            messageAlert(getString(R.string.message_visitor));
                        } else
                            setFragment(new NotificationFragment(), false, "");
                    } else if (groupPosition == 2) {
                        drawerToggle();
                        if (getUserForWholeApp().equals(Enum.setUser.GUEST)) {
                            messageAlertLogin(getString(R.string.message_Guest));
                        } else if (getUserForWholeApp().equals(Enum.setUser.SELLER_BASIC)) {
                            messageAlert(getString(R.string.message_visitor));
                        } else {
                            setFragment(new InboxFragment(), false, "");
                        }
                    } else if (groupPosition == 3) {
                        drawerToggle();
                        setFragment(new SettingFragment(), false, "");
                    } else if (groupPosition == 4) {
                        drawerToggle();
                        if (getUserForWholeApp().equals(Enum.setUser.GUEST)) {
                            messageAlertLogin(getString(R.string.message_Guest));
                        } else if (getUserForWholeApp().equals(Enum.setUser.VISITOR)) {
                            messageAlert(getString(R.string.message_visitor));
                        } else {
                            // setFragment(new CategorySelectionFragment(), false, "");
                            SubscriptionPlanFragment subscriptionPlanFragment = new SubscriptionPlanFragment();
                            Bundle bundleSubscription = new Bundle();
                            bundleSubscription.putString("from", "menu");
                            subscriptionPlanFragment.setArguments(bundleSubscription);
                            setFragment(subscriptionPlanFragment, false, "");
                        }
                    } else if (groupPosition == 5) {
                        drawerToggle();
                        setFragment(new ContactUsFragment(), false, "");
                    } else if (groupPosition == 6) {
                        drawerToggle();
                        setFragment(new AccountSettings(), false, "");
                    } else if (groupPosition == 7) {

                    } else if (groupPosition == 8) {
                        drawerToggle();

                        if (getUserForWholeApp().equals(Enum.setUser.GUEST)) {
                            DataToPref.clear(MainActivity.this);
                            removeAllNotification();
                            replaceParentFragment(new LoginFragment(), "");
                        } else {
                            alertTwoAction(getString(R.string.message_areYouSureYouWantToLogout), new Rendering() {
                                @Override
                                public void response(boolean isCofirm) {
                                    if (isCofirm) {

                                        logoutWS();
                                    }
                                }
                            });
                        }
                    }
                }
                return false;
            }
        });


        //cityList = new InitializeMenu().toGetCity();
        getSupportActionBar().hide();
        lockDrawer();
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                if (savedInstanceState == null) {
                    if (getData(MainActivity.this) == null) {
                        isComeFirst = true;
                        LoginWrapper loginWrapper = new LoginWrapper();
                        LoginData loginData = new LoginData("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "guest user", "", "", "", "", "", "", "", "", "", "", "");
                        loginWrapper.setData(loginData);
                        setData(MainActivity.this, new Gson().toJson(loginWrapper));
                        //((MainActivity) getActivity()).setUserSession(getContext(), login.getData().getUserSessionId());
                        replaceParentFragment(new TabFragment(), "parent");
                    } else {
                        isComeFirst = true;
                        if (getUserForWholeApp().equals(Enum.setUser.SELLER_BASIC) || getUserForWholeApp().equals(Enum.setUser.SELLER_PREMIUM)) {
                            sellerStatusApiCall();
                        }
                        replaceParentFragment(new TabFragment(), "parent");
                    }
                }
            }
        };

        handler.postDelayed(runnable, TIME_OUT);
    }

    private void sellerStatusApiCall() {

        new GetRequestHelper<LoginWrapper>().pingToRequest(Constants.SELLER_STATUS, new ArrayList<KeyValuePair>(), getParamWholeParameters(), new LoginWrapper(), new TaskCompleteListener<LoginWrapper>() {
            @Override
            public void onSuccess(LoginWrapper mObject) {
                if (mObject != null) {
                    if (mObject.getStatus() == 1) {
                        SubscriptionPlanFragment subscriptionPlanFragment = new SubscriptionPlanFragment();
                        Bundle bundleSubscription = new Bundle();
                        bundleSubscription.putString("from", "status_check");
                        subscriptionPlanFragment.setArguments(bundleSubscription);
                        setFragment(subscriptionPlanFragment, false, "");
                        messageAlert(mObject.getMessage());
                    } else if (mObject.getStatus() == 2) {
                        setFragment(new CategorySelectionFragment(), false, "");
                    }
                }
            }

            @Override
            public void onFailure(ExceptionType exceptions, LoginWrapper like) {
            }
        });
    }

    public String toCreateDirectory() {
        File file = new File(Environment.getExternalStorageDirectory() + "/citiSquareApp");
        if (!file.isDirectory())
            file.mkdir();
        return file.getAbsolutePath();
    }

    public long generateRandomNumber() {
        long number = (long) Math.floor(Math.random() * 9000L) + 1000L;
        return number;
    }

    public void logoutWS() {
        startLoader();

       /* keyValuePairList = new ArrayList<>();
        keyValuePairList.add(new KeyValuePair("User-Session", userSessionId()));
        keyValuePairList.add(new KeyValuePair("User-Id", userId()));*/


        new GetRequestHelper<LogoutWrapper>().pingToRequest(Constants.LOGOUT
                , new ArrayList<KeyValuePair>()
                , getParamWholeParameters()
                , new LogoutWrapper(), new TaskCompleteListener<LogoutWrapper>() {
                    @Override
                    public void onSuccess(LogoutWrapper logout) {
                        Debugger.e("SUCCESS CALL>>>>");
                        stopLoader();

                        if (logout.getStatus() == 1) {
                            setSelectionPostion(0);
                            DataToPref.clear(MainActivity.this);
                            removeAllNotification();
                            LoginWrapper loginWrapper = new LoginWrapper();
                            LoginData loginData = new LoginData("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "guest user", "", "", "", "", "", "", "", "", "", "", "");
                            loginWrapper.setData(loginData);
                            setData(MainActivity.this, new Gson().toJson(loginWrapper));
                            //((MainActivity) getActivity()).setUserSession(getContext(), login.getData().getUserSessionId());
                            replaceParentFragment(new TabFragment(), "parent");

                            //replaceParentFragment(new LoginFragment(), "");
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, LogoutWrapper logoutWrapper) {
                        stopLoader();
                        if (logoutWrapper != null) {
                            if (logoutWrapper.getStatus() == -1) {
                                invalidToken();
                            }
                        }
                    }
                });
    }

    public void invalidToken() {
        DataToPref.clear(MainActivity.this);
        removeAllNotification();
        LoginWrapper loginWrapper = new LoginWrapper();
        LoginData loginData = new LoginData("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "guest user", "", "", "", "", "", "", "", "", "", "", "");
        loginWrapper.setData(loginData);
        setData(MainActivity.this, new Gson().toJson(loginWrapper));
        //((MainActivity) getActivity()).setUserSession(getContext(), login.getData().getUserSessionId());
        replaceParentFragment(new TabFragment(), "parent");
    }


    /*PUSH NOTIFICATION*/

    public void removeAllNotification() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    public ServiceParameter getHeader() {
        param = new ServiceParameter();
        param.addHeader("API-KEY", Constants.X_API_KEY);

        Debugger.e("LANGUAGE DETECT ::??" + Locale.getDefault().getDisplayLanguage());
        if (Locale.getDefault().getDisplayLanguage().equals("العربية"))
            param.addHeader("Accept-Language", "arabic");
        else
            param.addHeader("Accept-Language", "english");
        return param;
    }


    public ServiceParameter getParamWholeParameters() {
        Debugger.e("LANGUAGE DETECT ::??WHOLE PARA>>" + Locale.getDefault().getDisplayLanguage());
        ServiceParameter serviceParameter = new ServiceParameter();
        serviceParameter.addHeader("API-KEY", Constants.X_API_KEY);
        serviceParameter.addHeader("User-Session", userSessionId());
        serviceParameter.addHeader("User-Id", userId());
        if (Locale.getDefault().getDisplayLanguage().equals("العربية"))
            serviceParameter.addHeader("Accept-Language", "arabic");
        else
            serviceParameter.addHeader("Accept-Language", "english");

        return serviceParameter;
    }


    public String getDeviceId() {
        if (DataToPref.getData(MainActivity.this, "deviceID") != null) {
            if (DataToPref.getData(MainActivity.this, "deviceID").equals(""))
                return "0";
            else
                return DataToPref.getData(MainActivity.this, "deviceID");
        } else
            return "0";
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null)
            fortCallback.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mainActivity = this;
        mainActivityInstance = this;

        Log.d(TAG, "onResume() called");
        if (!isComeFirst)
            handler.postDelayed(runnable, TIME_OUT);

        if (getData(this) != null)
            setData(this, new Gson().toJson(getData(this)));
        try {
            networkStateListener = new NetworkStateListener(this);
            IntentFilter filter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
            registerReceiver(networkStateListener, filter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onStop() {
        super.onStop();// ATTENTION: This was auto-generated to implement the App Indexing API.
// See https://g.co/AppIndexing/AndroidStudio for more information.

        handler.removeCallbacks(runnable);
        mainActivityInstance = null;
        mainActivity = null;
        try {
            unregisterReceiver(networkStateListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void replaceParentFragment(BaseFragment fragment, String tag) {
        hideKeyboard();
        Debugger.e("REPLACE FRAGMENT");
        //toolbar.setVisibility(View.VISIBLE);
        if (this != null) {
            if (tag.equals("parent")) {
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.placeholder, fragment, tag).addToBackStack(tag).commit();
            } else {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.placeholder, fragment, tag).addToBackStack(tag).commit();
            }
        }
    }

    public void addParentFragment(BaseFragment fragment, String tag) {
        hideKeyboard();
        Debugger.e("REPLACE FRAGMENT");
        //toolbar.setVisibility(View.VISIBLE);
        if (this != null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.placeholder, fragment, tag).addToBackStack(tag).commit();
        }
    }


    public void setFragment(BaseFragment baseFragment, boolean flag, String tag) {
        hideKeyboard();
        BaseFragment fragment = (BaseFragment) getSupportFragmentManager().
                findFragmentById(R.id.placeholder);
        if (fragment instanceof TabFragment)
            ((TabFragment) fragment).setFragment(baseFragment, flag, tag);
    }

    public void addFragment(Fragment fragment, boolean isBackStack) {
        hideKeyboard();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        fragmentTransaction.add(R.id.placeholder, fragment, fragment.getClass().getName());

        if (isBackStack) {
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
        }
        fragmentTransaction.commitAllowingStateLoss();
    }


    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void setLoginScreen() {
        toolbar.setVisibility(View.VISIBLE);
    }


    @Override
    public void onBackPressed() {

        hideKeyboard();
        BaseFragment fragment = (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.placeholder);

        if (fragment instanceof NetworkConnectionFragment) {
            finish();
            return;
        }
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (fragment instanceof TabFragment) {

               /* if (fragment.onBackPressed()) {
                    finish();
                    return;
                } else {
                    Debugger.e("FRAGMENT:::::" + fragment.getClass().getName());
                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }*/
            }/* else if (fragment instanceof LoginFragment) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                return;
            }*/ else if (fragment instanceof SubscriptionPlanFragment) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                return;
            } else {

                super.onBackPressed();
            }
        }
    }
    /*INITIALIZE DATA SET*/

    public ArrayList<Country> countryDatas() {
        return initializeMenu.toGetCountryCode();
    }

    public ArrayList<City> cityArrayList() {
        return initializeMenu.toGetCityCode();
    }

    public ArrayList<CategoryData> getCategoryList() {
        return initializeMenu.toGetCategory();
    }

    public ArrayList<SubCategory> getSubCategoryList(String categoryId) {
        ArrayList<SubCategory> subCategories = new ArrayList<>();
        if (!getCategoryList().isEmpty()) {
            ArrayList<CategoryData> categoryDatas = getCategoryList();
            for (int i = 0; i < categoryDatas.size(); i++) {
                if (categoryId.equals(categoryDatas.get(i).getId())) {
                    for (int k = 0; k < categoryDatas.get(i).getSubCategory().size(); k++)
                        subCategories.add(categoryDatas.get(i).getSubCategory().get(k));
                }
            }
        }
        return subCategories;
    }


    /*DATA SET*/
    public String getProfileUserId() {
        /*if (DataToPref.getData(this, "userId") != null)
            return DataToPref.getData(this, "userId");
        else
            return "";*/
        return otherProfileUserId;
    }


    public void setProfileUserId(String profileUserId) {
        otherProfileUserId = profileUserId;
        //DataToPref.setData(this, "userId", profileUserId);
    }

    public Enum.setUser getUserForWholeApp() {
        return userForWholeApp;
    }

    public void setUserForWholeApp(Enum.setUser userForWholeApp) {
        this.userForWholeApp = userForWholeApp;
        if (getData(this) != null) {
            LoginWrapper loginWrapper = getData(this);
            if (loginWrapper.getData().getRole().equalsIgnoreCase("guest user")) {
                menuHeader.imageViewProfile.setImageDrawable(getResources().getDrawable(R.drawable.add_photo_signup));
                menuHeader.textViewProfilePerson.setText(R.string.guestuser);
            } else {
                setCircularImage(menuHeader.imageViewProfile, loginWrapper.getData().getProfileImageThumb());
               // String name = loginWrapper.getData().getFirstName() + " " + loginWrapper.getData().getLastName();
                String name = loginWrapper.getData().getUsername();
                menuHeader.textViewProfilePerson.setText(name);
            }
        }
    }

    public String getChatDateTime() {
        return setChatDateTime;
    }

    public void setChatDateTime(String dateTime) {
        setChatDateTime = dateTime;
    }

    public void setUserSession(Context userSession, String data) {
        DataToPref.setData(userSession, "userSession", data);
    }

    public void setData(Context context, String data) {
        cityList = cityArrayList();
        DataToPref.setData(context, data);

        LoginWrapper loginWrapper = new Gson().fromJson(data, LoginWrapper.class);
        if (loginWrapper.getData().getRole().equalsIgnoreCase("V"))
            setUserForWholeApp(Enum.setUser.VISITOR);
        else if (loginWrapper.getData().getRole().equalsIgnoreCase("S") && loginWrapper.getData().getSellerType().equalsIgnoreCase("1"))
            setUserForWholeApp(Enum.setUser.SELLER_BASIC);
        else if (loginWrapper.getData().getRole().equalsIgnoreCase("S") && !loginWrapper.getData().getSellerType().equalsIgnoreCase("1"))
            setUserForWholeApp(Enum.setUser.SELLER_PREMIUM);
        else if (loginWrapper.getData().getRole().equalsIgnoreCase("guest user")) {
            setUserForWholeApp(Enum.setUser.GUEST);
        }
        setMenuBar(new InitializeMenu().toGetSubMenu(this), new InitializeMenu().toGetMenuList(this));
       /* if (getUserForWholeApp().equals(Enum.setUser.GUEST)) {
            menuList = new InitializeMenu().toGetMenuList(this);
            listDataChild.remove(menuList.get(4).getValue());
            menuListAdapter.notifyDataSetChanged();
        }*/
        callCity();
    }

    public void setMenuBar(List<String> nowShowingPara, List<KeyvaluePair> keyvaluePairs) {
        menuList = keyvaluePairs;
        List<String> nowShowing = nowShowingPara;
        listDataChild.clear();
        if (getUserForWholeApp() != null) {
            if (getUserForWholeApp().equals(Enum.setUser.GUEST)) {
                listDataChild.put(menuList.get(0).getValue(), strings);
                listDataChild.put(menuList.get(1).getValue(), strings);
                listDataChild.put(menuList.get(2).getValue(), strings);
                listDataChild.put(menuList.get(3).getValue(), strings);
                listDataChild.put(menuList.get(4).getValue(), strings);
            } else if (getUserForWholeApp().equals(Enum.setUser.VISITOR)) {
                listDataChild.put(menuList.get(0).getValue(), strings);
                listDataChild.put(menuList.get(1).getValue(), strings);
                listDataChild.put(menuList.get(2).getValue(), strings);
                listDataChild.put(menuList.get(3).getValue(), strings);
                listDataChild.put(menuList.get(4).getValue(), strings);
                listDataChild.put(menuList.get(5).getValue(), strings);
                listDataChild.put(menuList.get(6).getValue(), strings);
            } else if (getUserForWholeApp().equals(Enum.setUser.SELLER_BASIC) || getUserForWholeApp().equals(Enum.setUser.SELLER_PREMIUM)) {
                listDataChild.put(menuList.get(0).getValue(), strings);
                listDataChild.put(menuList.get(1).getValue(), strings);
                listDataChild.put(menuList.get(2).getValue(), strings);
                listDataChild.put(menuList.get(3).getValue(), strings);
                listDataChild.put(menuList.get(4).getValue(), strings);
                listDataChild.put(menuList.get(5).getValue(), strings);
                listDataChild.put(menuList.get(6).getValue(), strings);
                listDataChild.put(menuList.get(7).getValue(), nowShowing);
                listDataChild.put(menuList.get(8).getValue(), strings);
            }
        }
        menuListAdapter = new MenuListAdapter(this, menuList, listDataChild);
        listViewMenu.setAdapter(menuListAdapter);

        /*Set keys = listDataChild.keySet();

        for (Iterator i = keys.iterator(); i.hasNext(); ) {
            String key = (String) i.next();
            List<String> value = listDataChild.get(key);
            Debugger.e("KEY VALUE:::" + key);
        }*/
    }


    public void callCity() {
        Debugger.e("CITY IS CALLED");
        cityList = cityArrayList();
        if (!DataToPref.getData(MainActivity.this, "city").equals("")) {
            cityName = DataToPref.getData(MainActivity.this, "city");
            actionBarCityId = DataToPref.getData(MainActivity.this, "cityId");
            /*cityName = cityList.get(3).getName();
            actionBarCityId = cityList.get(3).getId();
            citylatitude = cityList.get(3).getLatitude();
            citylongitude = cityList.get(3).getLongitude();*/
            topCitySelection.setText(cityName);
        } else if (actionBarCityId.equals("") && !cityList.isEmpty()) {
            topCitySelection.setText(cityList.get(3).getName());
            actionBarCityId = cityList.get(3).getId();
            cityName = cityList.get(3).getName();
            citylatitude = cityList.get(3).getLatitude();
            citylongitude = cityList.get(3).getLongitude();

            if (Locale.getDefault().getDisplayLanguage().equals("العربية"))
                DataToPref.setData(MainActivity.this, "city", cityList.get(3).getArName());
            else
                DataToPref.setData(MainActivity.this, "city", cityList.get(3).getName());

            DataToPref.setData(MainActivity.this, "cityId", actionBarCityId);
        }
    }

    public LoginWrapper getData(Context context) {
        try {
            return new Gson().fromJson(DataToPref.getData(this), LoginWrapper.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getSession(Context context) {
        try {
            return DataToPref.getData(this, "userSession");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public String userSessionId() {
        if (getSession(this) != null) {
            return getSession(this);
        } else
            return "";
    }

    public String userId() {
        if (getData(this) != null)
            return getData(this).getData().getId();
        else
            return "";
    }


    /*Loader start stop*/
    public void startLoader() {
//        getProgressDialog.show();
        if (progressDialog != null) {
            progressDialog.show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mainActivityInstance = null;
    }

    public void stopLoader() {
        //   getProgressDialog.dismiss();
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    /*SET IMAGE USING PICASSO*/
    public void setImage(File image, ImageView imageViewResource) {
        picasso
                .load(image)
                .fit()
                .into(imageViewResource);
    }

    public void setImage(String image, ImageView imageViewResource) {
        picasso
                .load(image)
                .fit()
                .into(imageViewResource);
    }

    public void setCircularImage(File image, ImageView imageView) {
        picasso.load(image)
                .transform(new CircleTransformation())
                .fit()
                .into(imageView);
    }

    public void setCircularImage(ImageView circularImage, String image) {
        if (image != null) {
            if (image.equals("")) {
                picasso.load(R.drawable.zac_afron)
                        .fit()
                        .transform(new CircleTransformation())
                        .into(circularImage);
            } else {
                picasso.load(image)
                        .fit()
                        .transform(new CircleTransformation())
                        .into(circularImage);
            }
        } else {
            picasso.load(R.drawable.zac_afron)
                    .fit()
                    .transform(new CircleTransformation())
                    .into(circularImage);
        }

    }

    public void setCircularProfileImage(String image) {
        picasso.load(image)
                .fit()
                .transform(new CircleTransformation())
                .into(profileImage);
    }

    public void setTitle(String title, Enum.setNevigationIcon setNevigationIcon, boolean lockDrawer) {
        toolbar.setVisibility(View.VISIBLE);

        if (lockDrawer)
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        else
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

        if (title.equals("default")) {
            toolBarTitle.setAllCaps(false);
            toolBarTitle.setVisibility(View.INVISIBLE);
            titleLogo.setVisibility(View.VISIBLE);
            /*Spannable spannable = new SpannableString(getString(R.string.title_quicinfo));
            spannable.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.cityinfoTitle)), 4, spannable.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            toolBarTitle.setText(spannable);*/
        } else {
            titleLogo.setVisibility(View.INVISIBLE);
            toolBarTitle.setVisibility(View.VISIBLE);
            toolBarTitle.setAllCaps(true);
            toolBarTitle.setText(title);
        }
        menuImage.setVisibility(View.VISIBLE);
        setMenuOptions(setNevigationIcon);
    }


    public void messageAlertLogin(final String message) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(getString(R.string.alert_string_title))
                .setMessage(message)
                .setPositiveButton(getResources().getString(R.string.alert_message_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (getUserForWholeApp().equals(Enum.setUser.GUEST)) {
                            replaceParentFragment(new LoginFragment(), "");
                        } else
                            dialog.dismiss();

                    }
                });
        builder.show();
    }

    public void messageAlert(final String message) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(getString(R.string.alert_string_title))
                .setMessage(message)
                .setPositiveButton(getResources().getString(R.string.alert_message_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                });
        builder.show();
    }

    public void messageAlert(final String message, String s) {

        onBackPressed();
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(getString(R.string.alert_string_title))
                .setMessage(message)
                .setPositiveButton(getResources().getString(R.string.alert_message_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder.show();
    }


    public void alertTwoAction(String message, final Rendering callback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.alert_string_title));
        builder.setMessage(message);
        builder.setPositiveButton(getResources().getString(R.string.alert_message_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (callback != null)
                    callback.response(true);

            }
        });
        builder.setNegativeButton(getResources().getString(R.string.alert_message_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (callback != null)
                    callback.response(false);

            }
        });
        builder.show();
    }


    /*MENU,BACK,MENU_CITY,MENU_SETTING,BACK_SAVE,BACK_COMPOSE;*/
    public void setMenuOptions(Enum.setNevigationIcon menuOptions) {
        switch (menuOptions) {
            case MENU:
                menuImage.setVisibility(View.VISIBLE);
                backImage.setVisibility(View.INVISIBLE);
                settingMessageLayout.setVisibility(View.INVISIBLE);
                groupMessageSend.setVisibility(View.INVISIBLE);
                topCitySelection.setVisibility(View.INVISIBLE);
                profileImage.setVisibility(View.INVISIBLE);
                saveLayout.setVisibility(View.INVISIBLE);
                sendLayout.setVisibility(View.INVISIBLE);
                addBranch.setVisibility(View.INVISIBLE);
                writeReview.setVisibility(View.INVISIBLE);
                break;
            case BACK:
                menuImage.setVisibility(View.INVISIBLE);
                backImage.setVisibility(View.VISIBLE);
                settingMessageLayout.setVisibility(View.INVISIBLE);
                groupMessageSend.setVisibility(View.INVISIBLE);
                topCitySelection.setVisibility(View.INVISIBLE);
                saveLayout.setVisibility(View.INVISIBLE);
                profileImage.setVisibility(View.INVISIBLE);
                sendLayout.setVisibility(View.INVISIBLE);
                addBranch.setVisibility(View.INVISIBLE);
                writeReview.setVisibility(View.INVISIBLE);
                break;
            case MENU_CITY:
                menuImage.setVisibility(View.VISIBLE);
                backImage.setVisibility(View.INVISIBLE);
                settingMessageLayout.setVisibility(View.INVISIBLE);
                profileImage.setVisibility(View.INVISIBLE);
                groupMessageSend.setVisibility(View.INVISIBLE);
                topCitySelection.setVisibility(View.VISIBLE);
                saveLayout.setVisibility(View.INVISIBLE);
                sendLayout.setVisibility(View.INVISIBLE);
                addBranch.setVisibility(View.INVISIBLE);
                writeReview.setVisibility(View.INVISIBLE);
                break;
            case MENU_SETTING:
                if (badgeCounterData != null) {
                    if (badgeCounterData.getChatCount() != 0) {
                        badgeCountToolbar.setVisibility(View.VISIBLE);
                        bageCounterToolbar.setVisibility(View.VISIBLE);
                        bageCounterToolbar.setText(badgeCounterData.getChatCount() + "");
                    } else {
                        badgeCountToolbar.setVisibility(View.INVISIBLE);
                        bageCounterToolbar.setVisibility(View.INVISIBLE);
                    }
                } else {
                    badgeCountToolbar.setVisibility(View.INVISIBLE);
                    bageCounterToolbar.setVisibility(View.INVISIBLE);
                }
                menuImage.setVisibility(View.VISIBLE);
                backImage.setVisibility(View.INVISIBLE);
                settingMessageLayout.setVisibility(View.VISIBLE);
                groupMessageSend.setVisibility(View.INVISIBLE);
                profileImage.setVisibility(View.INVISIBLE);
                topCitySelection.setVisibility(View.INVISIBLE);
                saveLayout.setVisibility(View.INVISIBLE);
                writeReview.setVisibility(View.INVISIBLE);
                sendLayout.setVisibility(View.INVISIBLE);
                addBranch.setVisibility(View.INVISIBLE);
                break;
            case MENU_COMPOSE:
                menuImage.setVisibility(View.INVISIBLE);
                backImage.setVisibility(View.VISIBLE);
                settingMessageLayout.setVisibility(View.INVISIBLE);
                groupMessageSend.setVisibility(View.VISIBLE);
                profileImage.setVisibility(View.INVISIBLE);
                topCitySelection.setVisibility(View.INVISIBLE);
                saveLayout.setVisibility(View.INVISIBLE);
                sendLayout.setVisibility(View.INVISIBLE);
                writeReview.setVisibility(View.INVISIBLE);
                addBranch.setVisibility(View.INVISIBLE);
                break;
            case BACK_SAVE:
                menuImage.setVisibility(View.INVISIBLE);
                profileImage.setVisibility(View.INVISIBLE);
                backImage.setVisibility(View.VISIBLE);
                settingMessageLayout.setVisibility(View.INVISIBLE);
                groupMessageSend.setVisibility(View.INVISIBLE);
                topCitySelection.setVisibility(View.INVISIBLE);
                saveLayout.setVisibility(View.VISIBLE);
                sendLayout.setVisibility(View.INVISIBLE);
                writeReview.setVisibility(View.INVISIBLE);
                addBranch.setVisibility(View.INVISIBLE);
                break;
            case BACK_COMPOSE:
                menuImage.setVisibility(View.INVISIBLE);
                profileImage.setVisibility(View.INVISIBLE);
                backImage.setVisibility(View.VISIBLE);
                settingMessageLayout.setVisibility(View.INVISIBLE);
                groupMessageSend.setVisibility(View.VISIBLE);
                topCitySelection.setVisibility(View.INVISIBLE);
                saveLayout.setVisibility(View.INVISIBLE);
                sendLayout.setVisibility(View.INVISIBLE);
                addBranch.setVisibility(View.INVISIBLE);
                writeReview.setVisibility(View.INVISIBLE);
                break;
            case BACK_CITY:
                menuImage.setVisibility(View.INVISIBLE);
                profileImage.setVisibility(View.INVISIBLE);
                backImage.setVisibility(View.VISIBLE);
                settingMessageLayout.setVisibility(View.INVISIBLE);
                groupMessageSend.setVisibility(View.INVISIBLE);
                topCitySelection.setVisibility(View.VISIBLE);
                saveLayout.setVisibility(View.INVISIBLE);
                sendLayout.setVisibility(View.INVISIBLE);
                addBranch.setVisibility(View.INVISIBLE);
                writeReview.setVisibility(View.INVISIBLE);
                break;
            case BACK_SEND:
                menuImage.setVisibility(View.INVISIBLE);
                backImage.setVisibility(View.VISIBLE);
                settingMessageLayout.setVisibility(View.INVISIBLE);
                profileImage.setVisibility(View.INVISIBLE);
                groupMessageSend.setVisibility(View.INVISIBLE);
                topCitySelection.setVisibility(View.INVISIBLE);
                sendLayout.setVisibility(View.VISIBLE);
                saveLayout.setVisibility(View.INVISIBLE);
                addBranch.setVisibility(View.INVISIBLE);
                writeReview.setVisibility(View.INVISIBLE);
                break;
            case BACK_PROFILE:
                menuImage.setVisibility(View.INVISIBLE);
                backImage.setVisibility(View.VISIBLE);
                settingMessageLayout.setVisibility(View.INVISIBLE);
                profileImage.setVisibility(View.VISIBLE);
                groupMessageSend.setVisibility(View.INVISIBLE);
                topCitySelection.setVisibility(View.INVISIBLE);
                sendLayout.setVisibility(View.INVISIBLE);
                saveLayout.setVisibility(View.INVISIBLE);
                addBranch.setVisibility(View.INVISIBLE);
                writeReview.setVisibility(View.INVISIBLE);
                break;
            case BACK_BRANCH:
                menuImage.setVisibility(View.INVISIBLE);
                backImage.setVisibility(View.VISIBLE);
                settingMessageLayout.setVisibility(View.INVISIBLE);
                profileImage.setVisibility(View.INVISIBLE);
                groupMessageSend.setVisibility(View.INVISIBLE);
                topCitySelection.setVisibility(View.INVISIBLE);
                sendLayout.setVisibility(View.INVISIBLE);
                saveLayout.setVisibility(View.INVISIBLE);
                addBranch.setVisibility(View.VISIBLE);
                writeReview.setVisibility(View.INVISIBLE);
                break;
            case NOTHING:
                getSupportActionBar().hide();
                break;
            case ALLHIDE:
                menuImage.setVisibility(View.INVISIBLE);
                backImage.setVisibility(View.INVISIBLE);
                settingMessageLayout.setVisibility(View.INVISIBLE);
                groupMessageSend.setVisibility(View.INVISIBLE);
                profileImage.setVisibility(View.INVISIBLE);
                topCitySelection.setVisibility(View.INVISIBLE);
                saveLayout.setVisibility(View.INVISIBLE);
                sendLayout.setVisibility(View.INVISIBLE);
                addBranch.setVisibility(View.INVISIBLE);
                writeReview.setVisibility(View.INVISIBLE);
                break;
            case BACK_REVIEW:
                menuImage.setVisibility(View.INVISIBLE);
                backImage.setVisibility(View.VISIBLE);
                settingMessageLayout.setVisibility(View.INVISIBLE);
                profileImage.setVisibility(View.INVISIBLE);
                groupMessageSend.setVisibility(View.INVISIBLE);
                topCitySelection.setVisibility(View.INVISIBLE);
                sendLayout.setVisibility(View.INVISIBLE);
                saveLayout.setVisibility(View.INVISIBLE);
                addBranch.setVisibility(View.INVISIBLE);
                writeReview.setVisibility(View.VISIBLE);
                break;
        }
    }

    public void childFragmentReplacement(BaseFragment fragment, String tag) {
        BaseFragment baseFragment = (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.framelayoutPlaceholder);
        baseFragment.childFragmentReplacement(fragment, tag);
        baseFragment.getChildFragmentManager().beginTransaction().
                add(R.id.framelayoutPlaceholder, fragment, tag).
                addToBackStack(fragment.getClass().getName()).commit();
    }

    @OnClick(R.id.writeReview)
    public void writeReviewClicked() {
        if (getUserForWholeApp().equals(Enum.setUser.GUEST)) {
            messageAlert(getString(R.string.message_Guest));
        } else {
            BaseFragment fragment = (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.placeholder);
            if (fragment instanceof TabFragment) {
                ((TabFragment) fragment).writeRateAndReview();
            }

            //setFragment(new WriteReviewFragment(), false, "");
        }
    }


    @OnClick(R.id.topCitySelection)
    public void headerCitySelectionClicked() {
        hideKeyboard();
        int[] location = new int[2];
        topCitySelection.getLocationOnScreen(location);
        cityList = cityArrayList();
        if (cityList.size() == 0) {
            initializeMenu.toSetCountry(this);
        }

        cityDropDown = new CityDropDown(this, topCitySelection.getHeight(), location, cityList, new ItemEventListener<Integer>() {
            @Override
            public void onItemEventFired(Integer integer, Integer t2, String actualPos) {

                actionBarCityId = cityList.get(integer).getId();

                if (Locale.getDefault().getDisplayLanguage().equals("العربية")) {
                    DataToPref.setData(MainActivity.this, "city", cityList.get(integer).getArName());
                    topCitySelection.setText(cityList.get(integer).getArName());
                    cityName = cityList.get(integer).getArName();
                } else {
                    DataToPref.setData(MainActivity.this, "city", cityList.get(integer).getName());
                    cityName = cityList.get(integer).getName();
                    topCitySelection.setText(cityList.get(integer).getName());
                }


                DataToPref.setData(MainActivity.this, "cityId", cityList.get(integer).getId());
                BaseFragment baseFragment = (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.placeholder);
                if (baseFragment != null) {
                    if (baseFragment instanceof TabFragment)
                        ((TabFragment) baseFragment).apiCall();
                }



                /*framelayoutContainer*/
            }
        });
        cityDropDown.show();
    }

    public String getTopCitySelectionId() {
        if (actionBarCityId != null)
            return actionBarCityId;
        else
            return "";

    }

    public String getCityName() {
        if (cityName != null)
            return cityName;
        else
            return "";
    }

    public void actionBarShow() {
        getSupportActionBar().show();
    }


    public void actionBarHide() {
        getSupportActionBar().hide();
    }


    /*SET DRAWER LOCK UNLOCK*/
    public void lockDrawer() {
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    public void unlockDrawer() {
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    public void drawerToggle() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            drawer.openDrawer(GravityCompat.START);
            if (!userForWholeApp.equals(Enum.setUser.GUEST)) {
                callBadgeCounter();
            }
        }
    }

    private void callBadgeCounter() {
        new GetRequestHelper<BadgeCounterWrapper>().pingToRequest(Constants.BADGE_COUNTER, new ArrayList<KeyValuePair>(), getParamWholeParameters(), new BadgeCounterWrapper(), new TaskCompleteListener<BadgeCounterWrapper>() {
            @Override
            public void onSuccess(BadgeCounterWrapper badgeCounterWrapper) {
                if (badgeCounterWrapper.getStatus() == 1) {
                    badgeCounterData = badgeCounterWrapper.getData();
                    if (getUserForWholeApp().equals(Enum.setUser.GUEST)) {

                    } else if (getUserForWholeApp().equals(Enum.setUser.VISITOR)) {
                        menuList.get(1).setBadge(badgeCounterData.getNotificationCount());
                        menuList.get(2).setBadge(badgeCounterData.getChatCount());
                    } else {
                        menuList.get(1).setBadge(badgeCounterData.getNotificationCount());
                        menuList.get(2).setBadge(badgeCounterData.getChatCount());
                    }

                    menuListAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(ExceptionType exceptions, BadgeCounterWrapper badgeCounterWrapper) {
                if (badgeCounterWrapper != null) {
                    if (badgeCounterWrapper.getStatus() == -1)
                        invalidToken();
                }
            }
        });
    }

    public void setBadgedata(final BadgeCounterData badgedata) {
        if (badgedata != null) {
            if (badgedata.getChatCount() > 0) {
                bageCounterToolbar.setVisibility(View.VISIBLE);
                badgeCountToolbar.setVisibility(View.VISIBLE);
                badgeCounterData = badgedata;
                bageCounterToolbar.setText(badgedata.getChatCount() + "");
            }
        }
    }

    @Override
    public void jobApplyDialog() {
        JobApplyDialog jobApplyDialog = new JobApplyDialog();
        jobApplyDialog.show(getSupportFragmentManager(), jobApplyDialog.getClass().getName());
    }

    @Override
    public void visitorFragment() {
        //addFragment(new VisitorFragment(), true);
        replaceParentFragment(new VisitorFragment(), "");
    }

    @Override
    public void sellerFragment() {
        //addFragment(new SellerFragment(), true);
        replaceParentFragment(new SellerFragment(), "");
    }

    @Override
    public void profileFragment() {
        //addFragment(new ProfileFragment(), true);
    }

    @Override
    public void sendQuiries() {
        addFragment(new SendQueryFragment(), true);
    }

    @Override
    public void childCategoryFragment() {
        addFragment(new ChildCategoriesFragment(), true);
    }


    @Override
    public void forgotDialogFragment() {
        ForgotDialog forgotDialog = new ForgotDialog();
        forgotDialog.show(getSupportFragmentManager(), forgotDialog.getClass().getName());
    }

    @Override
    public void changePasswordDialog() {
        ChangePasswordDialog changePasswordDialog = new ChangePasswordDialog();
        changePasswordDialog.show(getSupportFragmentManager(), changePasswordDialog.getClass().getName());
    }


    @Override
    public void commentFragment() {
        //addFragment(new CommentFragment(), true);
        replaceParentFragment(new CommentFragment(), "parent");
    }

    @Override
    public void notificationFragment() {
        addFragment(new NotificationFragment(), true);
    }

    @Override
    public void postAJob() {
        //addFragment(new PostJobFragment(), true);
    }

    @Override
    public void postOnTimeLine() {
        addFragment(new PostOnTimeFragment(), true);
    }

    @Override
    public void postAds() {
        addFragment(new PostAdsFragment(), true);
    }

    @Override
    public void postTodaysOffer() {
        addFragment(new PostTodayOfferFragment(), true);
    }

    public void postSlider() {
        addFragment(new PostSliderFragment(), true);
    }

    @Override
    public void onFragmentChange(String TAG) {
        Debugger.e("TAG THAT IS LSDJFI :::" + TAG);
        currentFragment = TAG;
    }


    @OnClick(R.id.menuImage)
    public void menuImageClicked() {
        drawerToggle();
    }

    @OnClick(R.id.backImage)
    public void backImageClicked() {
        BaseFragment fragment = (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.placeholder);
        if (fragment instanceof TabFragment) {
            fragment.onBackPressed();
            return;
        }

        onBackPressed();
    }


    @OnClick(R.id.setting)
    public void settingClicked() {
        setSelectionPostion(4);
        setFragment(new SettingFragment(), false, "");
    }

    @OnClick(R.id.message)
    public void messageButtonClicked() {
        //addFragment(new InboxDetailsFragment(), true);
        if (getUserForWholeApp().equals(Enum.setUser.SELLER_BASIC)) {
            messageAlert(getString(R.string.message_visitor));
        } else {
            setSelectionPostion(2);
            setFragment(new InboxFragment(), false, "");
        }
    }

    @OnClick(R.id.groupMessageSend)
    public void composeMessageClicked() {
        //addFragment(new SendMessageFragment(), true);
        setFragment(new SendMessageFragment(), true, "");
    }

    @OnClick(R.id.addBranch)
    public void addBranchButtonClicked() {
        //callCheckBranchListing();
        moveToAddBranch("");
    }

    private void callCheckBranchListing() {
        startLoader();
        new GetRequestHelper<Like>().pingToRequest(Constants.CHECK_BRANCH
                , new ArrayList<KeyValuePair>()
                , getParamWholeParameters()
                , new Like()
                , new TaskCompleteListener<Like>() {
                    @Override
                    public void onSuccess(Like mObject) {
                        stopLoader();
                        if (mObject.getStatus() == 1) {
                            moveToAddBranch("asd");
                        } else if (mObject.getStatus() == 0) {
                            moveToBranchPlanList();
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, Like like) {
                        stopLoader();
                    }
                });
    }

    private void moveToBranchPlanList() {
        BranchPlanFragment branchPlanFragment = new BranchPlanFragment();
        setFragment(branchPlanFragment, true, "");
    }

    public void moveToAddBranch(String data) {
      /*  if (data.equals("")) {
            onBackPressed();
        }*/
        AddBranchFragment addBranchFragment = new AddBranchFragment();
        Bundle bundle = new Bundle();
        bundle.putString("Branch", "ADD");
        addBranchFragment.setArguments(bundle);
        setFragment(addBranchFragment, true, "");
    }

    @OnClick(R.id.save)
    public void saveClicked() {
        BaseFragment fragment = (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.placeholder);
        if (fragment instanceof TabFragment) {
            ((TabFragment) fragment).myInfoDataIsValid();
            /*if (((TabFragment) fragment).myInfoDataIsValid()) {
                onBackPressed();
                messageAlert(getString(R.string.message_profileUpdatedSuccessfully));
            }*/
        }
    }

    @OnClick(R.id.send)
    public void sendClicked() {
        BaseFragment fragment = (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.placeholder);
        if (fragment instanceof TabFragment) {
            if (((TabFragment) fragment).selectUserSend()) {
            } else {
                messageAlert(getString(R.string.message_pleaseSelectMember));
            }
        }
    }

    public void payment(Map<String, String> hashMapString) {

        String environment = FortSdk.ENVIRONMENT.TEST;
        FortRequest fortRequest = new FortRequest();
        fortRequest.setShowResponsePage(true);
        fortRequest.setRequestMap(hashMapString);

        try {
            FortSdk.getInstance().registerCallback(this, fortRequest, environment, 5,
                    fortCallback, new FortInterfaces.OnTnxProcessed() {

                        @Override
                        public void onCancel(Map<String, String> map, Map<String, String> map1) {

                            if (paymentCallBack != null) {
                                paymentCallBack.onCancel(map, map1);
                            }
                            Debugger.e("onCancel() called with: map = [" + map + "], map1 = [" + map1 + "]");
                        }

                        @Override
                        public void onSuccess(Map<String, String> map, Map<String, String> map1) {
                            if (paymentCallBack != null) {
                                paymentCallBack.onSucess(map, map1);
                            }
                            Debugger.e("onSuccess() called with: map = [" + map + "], map1 = [" + map1 + "]");
                        }

                        @Override
                        public void onFailure(Map<String, String> map, Map<String, String> map1) {

                            if (paymentCallBack != null) {
                                paymentCallBack.onFailureTransaction(map, map1);
                            }

                            Debugger.e("onFailure() called with: map = [" + map + "], map1 = [" + map1 + "]");
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getSelectionPostion() {
        return selectionPostion;
    }

    public void setSelectionPostion(int selectionPostion) {
        this.selectionPostion = selectionPostion;
        menuListAdapter.notifyDataSetChanged();
    }

    public int getPositionCategory() {
        return catePos;
    }

    public void setPositionCategory(int pos) {
        catePos = pos;
    }

    public void setFilterPostion(int pos) {
        filterPos = pos;
    }

    public int getFilterPosition() {
        return filterPos;
    }

    public int getBranchPosition() {
        return branchPos;
    }

    public void setBranchPosition(int pos) {
        branchPos = pos;
    }

    @Override
    public void onConnectionON() {
        try {
            Debugger.e(":::::::::::::::::: INTERNET CONNECTION IS ON ::::::::::::::::");
            //getToolbarLinearLayout().setVisibility(View.VISIBLE);
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.placeholder);
            NetworkConnectionFragment networkConnection = new NetworkConnectionFragment();
            if (fragment instanceof NetworkConnectionFragment) {
                getSupportFragmentManager().popBackStackImmediate(networkConnection.getClass().getName(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
                fragment = getSupportFragmentManager().findFragmentById(R.id.placeholder);

                initializeMenu.init(this);
                if (fragment instanceof LoginFragment) {
                    getSupportActionBar().hide();
                } else {
                    getSupportActionBar().show();


                    BaseFragment frag = (BaseFragment) getSupportFragmentManager().
                            findFragmentById(R.id.placeholder);
                    if (frag != null) {
                        if (frag instanceof TabFragment) {


                            // (BaseFragment) getChildFragmentManager().findFragmentById(R.id.framelayoutContainer);
                            BaseFragment baseFragment = (BaseFragment) ((TabFragment) frag).adapter.getRegisteredFragment(((TabFragment) frag).pager.getCurrentItem());
                            if (baseFragment != null) {
                                baseFragment.setHeaderTitle();
                            }

                            BaseFragment fragSub = (BaseFragment) frag.getChildFragmentManager().findFragmentById(framelayoutContainer);
                            if (fragSub != null) {
                                fragSub.setHeaderTitle();
                            }


                            //frag.setHeaderTitle();
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionOFF() {
        try {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.placeholder);
            if (!(fragment instanceof NetworkConnectionFragment)) {
                getSupportActionBar().hide();
                hideKeyboard();
                // getToolbarLinearLayout().setVisibility(View.GONE);
                Debugger.e(":::::::::::::::::: INTERNET CONNECTION IS OFF ::::::::::::::::");
                //   networkConnectionFragment();
                NetworkConnectionFragment networkConnection = new NetworkConnectionFragment();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.add(R.id.placeholder, networkConnection);
                transaction.addToBackStack("" + networkConnection.getClass().getName());
                transaction.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public LatLng getLatLng() {
        if (latLng != null)
            return latLng;
        else
            return new LatLng(0, 0);
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public void handleNewLocation(Location location) {
        Log.d("FUSED API", location.toString());

        double currentLatitude = location.getLatitude();
        double currentLongitude = location.getLongitude();

        Debugger.e("LAT ::::" + location.getLatitude());
        Debugger.e("LONG ::::" + location.getLongitude());

        latLng = new LatLng(currentLatitude, currentLongitude);
        if (this != null)
            setLatLng(latLng);
        //mMap.addMarker(new MarkerOptions().position(new LatLng(currentLatitude, currentLongitude)).title("Current Location"));
        /*MarkerOptions options = new MarkerOptions()
                .position(latLng)
                .title("I am here!");*/

        /*mMap.addMarker(options);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));*/
    }

    public void showUpdateMessage(PushNotification message) {
        int id = (int) System.currentTimeMillis();
        Intent intent = new Intent(this, MainActivity.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, id, intent,
                PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.appicon)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(message.getMessage())
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        /*BaseFragment fragment = (BaseFragment) getSupportFragmentManager().
                findFragmentById(R.id.placeholder);*/
        Debugger.e("MESSAGE IS :::::::::::::::::: " + message);
        BaseFragment fragement = (BaseFragment) getInstance().getSupportFragmentManager().
                findFragmentById(R.id.placeholder);
        if (fragement != null) {
            BaseFragment chatFragmet = (BaseFragment) fragement.getChildFragmentManager().findFragmentById(framelayoutContainer);
            if (chatFragmet != null) {
                if (chatFragmet instanceof ChatFragment) {
                    if (((ChatFragment) chatFragmet).chat(message)) {
                        notificationManager.notify(id, notificationBuilder.build());
                    }
                } else if (chatFragmet instanceof InboxFragment) {
                    //  notificationManager.notify(0, notificationBuilder.build());
                    ((InboxFragment) chatFragmet).chatMessage(message.getMessage(), message.getChatId());
                    if (badgeCounterData != null) {
                        int count = badgeCounterData.getChatCount();
                        badgeCounterData.setChatCount(++count);
                        menuListAdapter.notifyDataSetChanged();
                    }
               /* } else if (chatFragmet instanceof ProfileFragment) {
                    //  notificationManager.notify(0, notificationBuilder.build());
                    if (badgeCounterData != null) {
                        int count = badgeCounterData.getChatCount();
                        badgeCounterData.setChatCount(++count);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                bageCounterToolbar.setText(badgeCounterData.getChatCount() + "");
                            }
                        });

                    }
                    notificationManager.notify(id, notificationBuilder.build());*/
                } else {
                    notificationManager.notify(id, notificationBuilder.build());
                }
            }
        }


       /* if (fragement instanceof MyChatFragment) {
            //  notificationManager.notify(0, notificationBuilder.build());
            ((MyChatFragment) fragement).chatMessage(chatReplyId, chatId);
        }*/
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();

        Debugger.e("MEMORY IS TOO LOW.....");
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */


    class MenuHeader {

        @BindView(R.id.titleUserLogoName)
        LinearLayout titleUserLogoName;
        @BindView(R.id.imageViewProfile)
        ImageView imageViewProfile;
        @BindView(R.id.textViewProfilePerson)
        TextView textViewProfilePerson;

        public MenuHeader(View view) {
            ButterKnife.bind(this, view);
        }

        @OnClick(R.id.titleUserLogoName)
        public void userLogoNameClicked() {
            if (getUserForWholeApp().equals(Enum.setUser.GUEST)) {
                messageAlertLogin(getString(R.string.message_Guest));
            } else {
                drawerToggle();
                setSelectionPostion(0);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ProfileFragment profileFragment = new ProfileFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("profile", "own");
                        setProfileUserId(userId());
                        profileFragment.setArguments(bundle);

                        setFragment(profileFragment, false, "profileOwn");
                    }
                }, 200);
            }
        }
    }

    public boolean isInternetConnected(Context context) {
        boolean isConnected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        isConnected = true;
                    }
                }
            }
        }
        return isConnected;
    }

    public static void goToMainActivity(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    public void saveLanguage(Context mContext, boolean isArabic, SharedPreferences app_preference) {
        try {
            SharedPreferences.Editor edit = app_preference.edit();
            if (isArabic) {


                Locale locale = new Locale("ar");
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                mContext.getResources().updateConfiguration(config,
                        mContext.getResources().getDisplayMetrics());

                edit.putString("language", "Arabic");
                edit.commit();
            } else {
                Locale locale = new Locale("");
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                mContext.getResources().updateConfiguration(config, mContext.getResources().getDisplayMetrics());
                edit.putString("language", "English");
                edit.commit();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void setForceShowIconForPopup(PopupMenu popupMenu) {
        try {
            Field[] fields = popupMenu.getClass().getDeclaredFields();
            for (Field field : fields) {
                if ("mPopup".equals(field.getName())) {
                    field.setAccessible(true);
                    Object menuPopupHelper = field.get(popupMenu);
                    Class<?> classPopupHelper = Class.forName(menuPopupHelper
                            .getClass().getName());
                    Method setForceIcons = classPopupHelper.getMethod(
                            "setForceShowIcon", boolean.class);
                    setForceIcons.invoke(menuPopupHelper, true);
                    break;
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public String containsPinCode(String addressDetails) {
        String result = "";
        try {
            final Pattern p = Pattern.compile("(\\d{5,})");
            final Matcher m = p.matcher(addressDetails);
            if (m.find()) {
                return m.group(0);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }
}