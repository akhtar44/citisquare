package com.citisquare.app.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.citisquare.app.R;
import com.citisquare.app.adapter.SubscriptionPlanAdapter;
import com.citisquare.app.fragments.BaseFragment;
import com.citisquare.app.fragments.CategorySelectionFragment;
import com.citisquare.app.fragments.TabFragment;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.interfaces.PaymentCallBack;
import com.citisquare.app.pojo.PayfortStatus;
import com.citisquare.app.pojo.response.FortSdkResponse;
import com.citisquare.app.pojo.response.LoginData;
import com.citisquare.app.pojo.response.LoginWrapper;
import com.citisquare.app.pojo.response.SubscriptionPlanData;
import com.citisquare.app.pojo.response.SubscriptionPlanWrapper;
import com.citisquare.app.utils.DataToPref;
import com.citisquare.app.utils.Debugger;
import com.google.gson.Gson;
import com.payfort.fort.android.sdk.base.FortSdk;
import com.payfort.sdk.android.dependancies.models.FortRequest;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import serviceCalling.builder.KeyValuePair;
import serviceCalling.builder.ServiceParameter;
import serviceCalling.helper.GetRequestHelper;
import serviceCalling.helper.PostRequestHelper;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

import static com.citisquare.app.utils.DataToPref.setData;

/**
 * Created by hlink56 on 4/1/17.
 */

public class SubscriptionPlanActivity extends AppCompatActivity implements PaymentCallBack {


    @BindView(R.id.subscriptionRecyclerView)
    RecyclerView subscriptionRecyclerView;

    SubscriptionPlanAdapter subscriptionPlanAdapter;
    List<SubscriptionPlanData> subscriptionPlanDatas;
    String device_id = "";
    LoginData loginData;
    LoginWrapper loginWrapper;
    private ProgressDialog progressDialog;
    Bundle bundle;
    String merchant_id = "";
    String plan_id = "";
    String bundleString = "";
    String language = "";
    private GridLayoutManager gridLayoutManager;
    private String amount;
    ServiceParameter param;

    public static String encode(String base) {

        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frag_subscription_layout);


        ButterKnife.bind(this);
        subscriptionPlanDatas = new ArrayList<>();
        //bundle = getArguments();
        if (bundle != null) {
            bundleString = bundle.getString("from");
        }

        if (Locale.getDefault().getDisplayLanguage().equals("العربية"))
            language = "ar";
        else
            language = "en";

       /* loginWrapper = ((MainActivity) getActivity()).getData(this);
        loginData = loginWrapper.getData();
        ((MainActivity) getActivity()).setPaymentCallBack(this);*/

        device_id = FortSdk.getDeviceId(this);
        Debugger.e("DEVICE ID IS::::" + device_id);
        subscriptionPlanAdapter = new SubscriptionPlanAdapter(this, subscriptionPlanDatas, loginData.getSellerType(), new ItemEventListener<String>() {


            @Override
            public void onItemEventFired(String s, @Nullable String t2, String actualPos) {
                plan_id = t2;
                amount = s;
                wsCallGetSdkToken(s);
            }
        });

        gridLayoutManager = new GridLayoutManager(this, 1);
        subscriptionRecyclerView.setLayoutManager(gridLayoutManager);
        subscriptionRecyclerView.setAdapter(subscriptionPlanAdapter);
        subscriptionPlanApiCall();

        return;
    }

    private void wsCallGetSdkToken(final String amount) {

        startLoader();
        List<KeyValuePair> keyValuePairs = new ArrayList<>();
        keyValuePairs.add(new KeyValuePair("device_id", device_id));

        new GetRequestHelper<FortSdkResponse>().pingToRequest(Constants.GETSDK_TOKEN
                , keyValuePairs
                , getParamWholeParameters()
                , new FortSdkResponse()
                , new TaskCompleteListener<FortSdkResponse>() {
                    @Override
                    public void onSuccess(FortSdkResponse fortSdkResponse) {
                       stopLoader();
                        if (fortSdkResponse != null) {
                            if (!fortSdkResponse.getSdkToken().equalsIgnoreCase("")) {
                                next(fortSdkResponse.getSdkToken(), amount);
                            } else {
                                if (fortSdkResponse.getResponseCode() != null)
                                    messageAlert(fortSdkResponse.getResponseMessage());
                            }
                        }
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, FortSdkResponse subscriptionPlanWrapper) {
                           stopLoader();
                    }
                });

    }
    public void messageAlert(final String message) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(SubscriptionPlanActivity.this);
        builder.setTitle(getString(R.string.alert_string_title))
                .setMessage(message)
                .setPositiveButton(getResources().getString(R.string.alert_message_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                });
        builder.show();
    }
    public LoginWrapper getData(Context context) {
        try {
            return new Gson().fromJson(DataToPref.getData(this), LoginWrapper.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getSession(Context context) {
        try {
            return DataToPref.getData(this, "userSession");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public String userSessionId() {
        if (getSession(this) != null) {
            return getSession(this);
        } else
            return "";
    }

    public String userId() {
        if (getData(this) != null)
            return getData(this).getData().getId();
        else
            return "";
    }
    public void stopLoader() {
        //   getProgressDialog.dismiss();
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    public void startLoader() {
//        getProgressDialog.show();
        if (progressDialog != null) {
            progressDialog.show();
        }
    }

    public ServiceParameter getParamWholeParameters() {
        Debugger.e("LANGUAGE DETECT ::??WHOLE PARA>>" + Locale.getDefault().getDisplayLanguage());
        ServiceParameter serviceParameter = new ServiceParameter();
        serviceParameter.addHeader("API-KEY", Constants.X_API_KEY);
        serviceParameter.addHeader("User-Session", userSessionId());
        serviceParameter.addHeader("User-Id", userId());
        if (Locale.getDefault().getDisplayLanguage().equals("العربية"))
            serviceParameter.addHeader("Accept-Language", "arabic");
        else
            serviceParameter.addHeader("Accept-Language", "english");

        return serviceParameter;
    }


    private void next(String sdkToken, String amount) {
        try {
            merchant_id = "" + System.currentTimeMillis();
            FortRequest fortRequest = new FortRequest();
            Double aDouble = Double.parseDouble(amount);
            aDouble = aDouble * 100;
            long d = (new Double(aDouble)).longValue();
            Map<String, String> hashMapString = new HashMap<String, String>();
            hashMapString.put("amount", String.valueOf(d));
            hashMapString.put("command", "PURCHASE");
            hashMapString.put("currency", "SAR");
            hashMapString.put("customer_email", loginData.getEmail());
            hashMapString.put("customer_name", loginData.getFirstName() + loginData.getLastName());
            hashMapString.put("language", language);
            hashMapString.put("merchant_reference", merchant_id);
            hashMapString.put("sdk_token", sdkToken);
            fortRequest.setShowResponsePage(true);
            payment(hashMapString);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    private void payment(Map<String, String> hashMapString) {
    }

    public void subscriptionPlanApiCall() {
       startLoader();

        new GetRequestHelper<SubscriptionPlanWrapper>().pingToRequest(Constants.SUBSCRIPTION_PLAN
                , new ArrayList<KeyValuePair>()
                , getHeader()
                , new SubscriptionPlanWrapper()
                , new TaskCompleteListener<SubscriptionPlanWrapper>() {
                    @Override
                    public void onSuccess(SubscriptionPlanWrapper subscriptionPlanWrapper) {
                            stopLoader();

                        if (subscriptionPlanWrapper.getStatus() == 1)
                            notifyAdapter(subscriptionPlanWrapper.getData());
                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, SubscriptionPlanWrapper subscriptionPlanWrapper) {
                            stopLoader();

                        if (subscriptionPlanWrapper != null) {
                            if (subscriptionPlanWrapper.getStatus() == -1) {
                                invalidToken();
                            }
                        }
                    }
                });
    }

    public ServiceParameter getHeader() {
        param = new ServiceParameter();
        param.addHeader("API-KEY", Constants.X_API_KEY);

        Debugger.e("LANGUAGE DETECT ::??" + Locale.getDefault().getDisplayLanguage());
        if (Locale.getDefault().getDisplayLanguage().equals("العربية"))
            param.addHeader("Accept-Language", "arabic");
        else
            param.addHeader("Accept-Language", "english");
        return param;
    }

    private void notifyAdapter(List<SubscriptionPlanData> data) {
        subscriptionPlanDatas.clear();
        subscriptionPlanDatas.addAll(data);
        subscriptionPlanAdapter.notifyDataSetChanged();
    }

    public void setHeaderTitle() {
        if (bundleString.equalsIgnoreCase("menu")) {
           setTitle(getString(R.string.subscriptionPlan), Enum.setNevigationIcon.MENU, false);
        } else {
            setTitle(getString(R.string.subscriptionPlan), Enum.setNevigationIcon.ALLHIDE, true);
        }
    }

    private void setTitle(String string, Enum.setNevigationIcon allhide, boolean b) {
    }

    @Override
    public void onSucess(Map<String, String> map, Map<String, String> map1) {
        paymentCallApi(map1.get("fort_id"), "1");
    }

    @Override
    public void onFailureTransaction(Map<String, String> map, Map<String, String> map1) {
        if (map1.get("response_message").equalsIgnoreCase("Duplicate order number")) {
            getCheckStatus();
        } else {
            paymentCallApi("00", "0");
        }
    }

    private void getCheckStatus() {

            startLoader();
///check_payfortstatus/merchant_reference/12345

        ArrayList<KeyValuePair> keyValuePairs = new ArrayList<>();
        keyValuePairs.add(new KeyValuePair("merchant_reference", merchant_id));

        new GetRequestHelper<PayfortStatus>().pingToRequest(Constants.CHECK_STATUS
                , keyValuePairs
                , getHeader()
                , new PayfortStatus()
                , new TaskCompleteListener<PayfortStatus>() {
                    @Override
                    public void onSuccess(PayfortStatus subscriptionPlanWrapper) {

                           stopLoader();
                            if (subscriptionPlanWrapper.getStatus().equals("12") && subscriptionPlanWrapper.getTransactionCode().equals("14")) {
                                paymentCallApi(subscriptionPlanWrapper.getFortId(), "1");
                            } else {
                                wsCallGetSdkToken(amount);
                            }

                    }

                    @Override
                    public void onFailure(ExceptionType exceptions, PayfortStatus subscriptionPlanWrapper) {

                           stopLoader();

                    }
                });
    }

    @Override
    public void onCancel(Map<String, String> map, Map<String, String> map1) {

    }


    private void paymentCallApi(String fortId, String status) {

            startLoader();

        new PostRequestHelper<LoginWrapper>().pingToRequest(Constants.SELLER_PLAN_PAYMENT
                , null
                , null
                , paymentHashMap(fortId, status)
                , getParamWholeParameters()
                , new LoginWrapper()
                , new TaskCompleteListener<LoginWrapper>() {
                    @Override
                    public void onSuccess(LoginWrapper loginWrapperComplete) {


                            stopLoader();

                            if (loginWrapperComplete.getStatus() == 1) {
                                setData(SubscriptionPlanActivity.this, new Gson().toJson(loginWrapperComplete));
                               setFragment(new CategorySelectionFragment(), false, "");
                            }
                        }


                    @Override
                    public void onFailure(ExceptionType exceptions, LoginWrapper loginWrapperComplete) {
                          stopLoader();

                        if (loginWrapperComplete != null) {
                            if (loginWrapperComplete.getStatus() == -1)
                              invalidToken();
                        }
                    }
                });
    }

    public void setFragment(BaseFragment baseFragment, boolean flag, String tag) {
        //hideKeyboard();
        BaseFragment fragment = (BaseFragment) getSupportFragmentManager().
                findFragmentById(R.id.placeholder);
        if (fragment instanceof TabFragment)
            ((TabFragment) fragment).setFragment(baseFragment, flag, tag);
    }

    public void invalidToken() {
        DataToPref.clear(this);
        removeAllNotification();
        LoginWrapper loginWrapper = new LoginWrapper();
        LoginData loginData = new LoginData("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "guest user", "", "", "", "", "", "", "", "", "", "", "");
        loginWrapper.setData(loginData);
        setData(this, new Gson().toJson(loginWrapper));

    }

    private void removeAllNotification() {
    }

    public HashMap<String, String> paymentHashMap(String fortId, String status) {
        HashMap<String, String> paymentHashMap = new HashMap<>();
        paymentHashMap.put("plan_id", plan_id);
        paymentHashMap.put("transaction_id", fortId);
        paymentHashMap.put("status", status);
        paymentHashMap.put("type", "P");

        return paymentHashMap;
    }

    public void onBackPressed() {
        try {
            Intent intent = new Intent(SubscriptionPlanActivity.this, MainActivity.class);
            startActivity(intent);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
