package com.citisquare.app.controls;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.citisquare.app.R;


/**
 * Created by hlink16 on 9/9/15.
 */
public class CustomTextView extends TextView {

    private AutofitHelper mHelper;

    public CustomTextView(Context context) {
        super(context);
        init(null);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }


    public CustomTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }


    private void init(AttributeSet attrs) {
        try {
            if (attrs != null) {
                TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CustomTextView);
                String fontName = typedArray.getString(R.styleable.CustomEditText_fontName);
                if (fontName != null) {
                   // Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), fontName);
                    Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto_Regular.ttf");
                    setTypeface(myTypeface);
                }
                typedArray.recycle();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}
