package com.citisquare.app.controls;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

import com.citisquare.app.R;


/**
 * Created by hlink16 on 9/9/15.
 */
public class CustomEditText extends AppCompatEditText {
    public CustomEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }


    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);

    }


    public CustomEditText(Context context) {
        super(context);

    }

    private void init(AttributeSet attrs)
    {

        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.CustomEditText);
            try {


                String fontName = a.getString(R.styleable.CustomEditText_fontName);
                boolean boldornot = a.getBoolean(R.styleable.CustomEditText_boldornot, false);


                if (fontName != null) {
                    Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), fontName);
                    this.setTypeface(myTypeface);

                }

            } finally {
                a.recycle();
            }

        }

    }


}
/* {
    public CustomEditText(Context context) {
        super(context);
        init(null);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    Typeface myTypeface;

    private void init(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs,
                    R.styleable.CustomEditText);
            String fontName = a.getString(R.styleable.CustomEditText_fontName);
            if (fontName != null && !isInEditMode()) {
                myTypeface = Typeface.createFromAsset(getContext().getAssets(), fontName);
            }
            setTypeface(myTypeface);
        }
    }
}*/
