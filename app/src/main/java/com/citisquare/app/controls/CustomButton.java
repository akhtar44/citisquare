package com.citisquare.app.controls;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import com.citisquare.app.R;


/**
 * Created by hlink56 on 9/2/16.
 */
public class CustomButton extends Button {

    public CustomButton(Context context) {
        super(context);
        init(null);
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }


    public CustomButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }


    private void init(AttributeSet attrs) {
        try {
            if (attrs != null) {
                TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CustomButton);
                String fontName = typedArray.getString(R.styleable.CustomEditText_fontName);
                if (fontName != null) {
                  //  Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(),  fontName);
                    Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(),  "fonts/Roboto_Bold.ttf");
                    setTypeface(myTypeface);
                }
                typedArray.recycle();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

}
