package com.citisquare.app.controls;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.RadioButton;

import com.citisquare.app.R;

/**
 * Created by hlink56 on 2/6/16.
 */
@SuppressLint("AppCompatCustomView")
public class CustomRadioButton extends RadioButton {

    public CustomRadioButton(Context context) {
        super(context);
        init(null);
    }

    public CustomRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CustomRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }


    private void init(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CustomRadioButton);
            String fontName = typedArray.getString(R.styleable.CustomEditText_fontName);
            if (fontName != null) {
                Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(),fontName);
                setTypeface(myTypeface);
            }
            typedArray.recycle();
        }
    }


}
