package com.citisquare.app.controls;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

import com.citisquare.app.R;

/**
 * Created by hlink56 on 16/3/16.
 */
public class CustomAutoComplete extends AutoCompleteTextView {

    public CustomAutoComplete(Context context) {
        super(context);
        init(null);
    }

    public CustomAutoComplete(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }


    public CustomAutoComplete(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }


    private void init(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CustomEditText);
            String fontName = typedArray.getString(R.styleable.CustomEditText_fontName);
            if (fontName != null) {
                Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), fontName);
                setTypeface(myTypeface);
            }
            typedArray.recycle();
        }
    }
}