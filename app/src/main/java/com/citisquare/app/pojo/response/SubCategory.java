package com.citisquare.app.pojo.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by hlink56 on 9/8/16.
 */
public class SubCategory implements Serializable {
    boolean isSelected;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("parent_id")
    @Expose
    private String parentId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("ar_name")
    @Expose
    private String arName;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("is_parent")
    @Expose
    private String isParent;
    @SerializedName("is_active")
    @Expose
    private String isActive;
    @SerializedName("insertdate")
    @Expose
    private String insertdate;
    @SerializedName("subcategory_img_url")
    @Expose
    private String subcategoryImgUrl;
    @SerializedName("subcategory_img_thumb_url")
    @Expose
    private String subcategoryImgThumbUrl;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The parentId
     */
    public String getParentId() {
        return parentId;
    }

    /**
     * @param parentId The parent_id
     */
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    public String getArName() {
        return arName;
    }

    public void setArName(String arName) {
        this.arName = arName;
    }

    /**
     * @return The image
     */


    public String getImage() {
        return image;
    }

    /**
     * @param image The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * @return The isParent
     */
    public String getIsParent() {
        return isParent;
    }

    /**
     * @param isParent The is_parent
     */
    public void setIsParent(String isParent) {
        this.isParent = isParent;
    }

    /**
     * @return The isActive
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * @param isActive The is_active
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    /**
     * @return The insertdate
     */
    public String getInsertdate() {
        return insertdate;
    }

    /**
     * @param insertdate The insertdate
     */
    public void setInsertdate(String insertdate) {
        this.insertdate = insertdate;
    }

    /**
     * @return The subcategoryImgUrl
     */
    public String getSubcategoryImgUrl() {
        return subcategoryImgUrl;
    }

    /**
     * @param subcategoryImgUrl The subcategory_img_url
     */
    public void setSubcategoryImgUrl(String subcategoryImgUrl) {
        this.subcategoryImgUrl = subcategoryImgUrl;
    }

    /**
     * @return The subcategoryImgThumbUrl
     */
    public String getSubcategoryImgThumbUrl() {
        return subcategoryImgThumbUrl;
    }

    /**
     * @param subcategoryImgThumbUrl The subcategory_img_thumb_url
     */
    public void setSubcategoryImgThumbUrl(String subcategoryImgThumbUrl) {
        this.subcategoryImgThumbUrl = subcategoryImgThumbUrl;
    }

    @Override
    public String toString() {
        return "SubCategory{" +
                "name='" + name + '\'' +
                '}';
    }
}
