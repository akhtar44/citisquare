package com.citisquare.app.pojo.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hlink56 on 4/1/17.
 */

public class FortSdkResponse{

    @SerializedName("access_code")
    @Expose
    private String accessCode;
    @SerializedName("sdk_token")
    @Expose
    private String sdkToken;
    @SerializedName("response_message")
    @Expose
    private String responseMessage;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("response_code")
    @Expose
    private String responseCode;
    @SerializedName("device_id")
    @Expose
    private String deviceId;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("service_command")
    @Expose
    private String serviceCommand;
    @SerializedName("signature")
    @Expose
    private String signature;
    @SerializedName("merchant_identifier")
    @Expose
    private String merchantIdentifier;

    public String getAccessCode() {
        return accessCode;
    }

    public void setAccessCode(String accessCode) {
        this.accessCode = accessCode;
    }

    public String getSdkToken() {
        return sdkToken;
    }

    public void setSdkToken(String sdkToken) {
        this.sdkToken = sdkToken;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getServiceCommand() {
        return serviceCommand;
    }

    public void setServiceCommand(String serviceCommand) {
        this.serviceCommand = serviceCommand;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getMerchantIdentifier() {
        return merchantIdentifier;
    }

    public void setMerchantIdentifier(String merchantIdentifier) {
        this.merchantIdentifier = merchantIdentifier;
    }

}
