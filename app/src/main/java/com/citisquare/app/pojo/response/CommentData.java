package com.citisquare.app.pojo.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hlink56 on 23/8/16.
 */
public class CommentData {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("post_id")
    @Expose
    private String postId;
    @SerializedName("commented_user_id")
    @Expose
    private String commentedUserId;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("is_active")
    @Expose
    private String isActive;
    @SerializedName("insertdate")
    @Expose
    private String insertdate;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("profile_image_thumb")
    @Expose
    private String profileImageThumb;

    public CommentData(String id
            , String postId
            , String commentedUserId
            , String comment
            , String isActive
            , String insertdate
            , String username
            , String profileImage
            , String profileImageThumb) {
        this.id = id;
        this.postId = postId;
        this.commentedUserId = commentedUserId;
        this.comment = comment;
        this.isActive = isActive;
        this.insertdate = insertdate;
        this.username = username;
        this.profileImage = profileImage;
        this.profileImageThumb = profileImageThumb;
    }

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The postId
     */
    public String getPostId() {
        return postId;
    }

    /**
     * @param postId The post_id
     */
    public void setPostId(String postId) {
        this.postId = postId;
    }

    /**
     * @return The commentedUserId
     */
    public String getCommentedUserId() {
        return commentedUserId;
    }

    /**
     * @param commentedUserId The commented_user_id
     */
    public void setCommentedUserId(String commentedUserId) {
        this.commentedUserId = commentedUserId;
    }

    /**
     * @return The comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment The comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * @return The isActive
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * @param isActive The is_active
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    /**
     * @return The insertdate
     */
    public String getInsertdate() {
        return insertdate;
    }

    /**
     * @param insertdate The insertdate
     */
    public void setInsertdate(String insertdate) {
        this.insertdate = insertdate;
    }
    /**
     * @return The username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return The profileImage
     */
    public String getProfileImage() {
        return profileImage;
    }

    /**
     * @param profileImage The profile_image
     */
    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    /**
     * @return The profileImageThumb
     */
    public String getProfileImageThumb() {
        return profileImageThumb;
    }

    /**
     * @param profileImageThumb The profile_image_thumb
     */
    public void setProfileImageThumb(String profileImageThumb) {
        this.profileImageThumb = profileImageThumb;
    }

}
