package com.citisquare.app.pojo.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hlink56 on 6/8/16.
 */
public class ChatWrapper {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("chat_id")
    @Expose
    private String chatId;
    @SerializedName("page")
    @Expose
    private Integer page;
    @SerializedName("datetime")
    @Expose
    private String datetime;
    @SerializedName("data")
    @Expose
    private List<ChatData> data = new ArrayList<ChatData>();
    @SerializedName("chat_room_image")
    @Expose
    private String chatRoomImage;

    /**
     * @return The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The chatId
     */
    public String getChatId() {
        return chatId;
    }

    /**
     * @param chatId The chat_id
     */
    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    /**
     * @return The page
     */
    public Integer getPage() {
        return page;
    }

    /**
     * @param page The page
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     * @return The datetime
     */
    public String getDatetime() {
        return datetime;
    }

    /**
     * @param datetime The datetime
     */
    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    /**
     * @return The data
     */
    public List<ChatData> getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(List<ChatData> data) {
        this.data = data;
    }

    public String getChatRoomImage() {
        return chatRoomImage;
    }

    public void setChatRoomImage(String chatRoomImage) {
        this.chatRoomImage = chatRoomImage;
    }


}
