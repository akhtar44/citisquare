package com.citisquare.app.pojo.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hlink56 on 9/8/16.
 */
public class CategoryWrapper {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private Boolean message;
    @SerializedName("data")
    @Expose
    private List<CategoryData> data = new ArrayList<CategoryData>();

    /**
     * @return The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return The message
     */
    public Boolean getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(Boolean message) {
        this.message = message;
    }

    /**
     * @return The data
     */
    public List<CategoryData> getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(List<CategoryData> data) {
        this.data = data;
    }

}