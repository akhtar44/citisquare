package com.citisquare.app.pojo.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hlink56 on 19/1/17.
 */

public class BadgeCounterData{

    @SerializedName("notification_count")
    @Expose
    private Integer notificationCount;
    @SerializedName("chat_count")
    @Expose
    private Integer chatCount;

    public Integer getNotificationCount() {
        return notificationCount;
    }

    public void setNotificationCount(Integer notificationCount) {
        this.notificationCount = notificationCount;
    }

    public Integer getChatCount() {
        return chatCount;
    }

    public void setChatCount(Integer chatCount) {
        this.chatCount = chatCount;
    }

}
