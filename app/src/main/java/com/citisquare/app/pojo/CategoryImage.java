package com.citisquare.app.pojo;

/**
 * Created by hlink56 on 18/6/16.
 */
public class CategoryImage {
    String key;
    String name;

    public CategoryImage(String key, String name) {
        this.key = key;
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
