package com.citisquare.app.pojo;

/**
 * Created by hlink56 on 6/6/16.
 */
public class GalleryData {
    String galleryImage;

    public GalleryData(String galleryImage) {
        this.galleryImage = galleryImage;
    }

    public String getGalleryImage() {
        return galleryImage;
    }

    public void setGalleryImage(String galleryImage) {
        this.galleryImage = galleryImage;
    }

    @Override
    public String toString() {
        return "GalleryData{" +
                "galleryImage='" + galleryImage + '\'' +
                '}';
    }
}
