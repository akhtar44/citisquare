package com.citisquare.app.pojo.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hlink56 on 9/8/16.
 */
public class GalleryData {

    @SerializedName("id")
    public String id;
    @SerializedName("user_id")
    public String user_id;
    @SerializedName("image")
    public String image;
    @SerializedName("media_type")
    public String media_type;
    @SerializedName("is_active")
    public String is_active;
    @SerializedName("insertdate")
    public String insertdate;
    @SerializedName("media_name")
    public String media_name;
    @SerializedName("media_name_thumb")
    public String media_name_thumb;

    public GalleryData(String data) {
        setMedia_name_thumb(data);
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getMedia_type() {
        return media_type;
    }

    public void setMedia_type(String media_type) {
        this.media_type = media_type;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public String getInsertdate() {
        return insertdate;
    }

    public void setInsertdate(String insertdate) {
        this.insertdate = insertdate;
    }

    public String getMedia_name() {
        return media_name;
    }

    public void setMedia_name(String media_name) {
        this.media_name = media_name;
    }

    public String getMedia_name_thumb() {
        return media_name_thumb;
    }

    public void setMedia_name_thumb(String media_name_thumb) {
        this.media_name_thumb = media_name_thumb;
    }
}
