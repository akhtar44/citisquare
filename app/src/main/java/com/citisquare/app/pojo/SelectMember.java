package com.citisquare.app.pojo;

/**
 * Created by hlink56 on 18/7/16.
 */
public class SelectMember {
    String name;
    boolean isSelect;

    public SelectMember(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }
}
