package com.citisquare.app.pojo;

/**
 * Created by hlink56 on 1/6/16.
 */
public class Message {
    String mesage;
    String time;

    public Message(String mesage, String time) {
        this.mesage = mesage;
        this.time = time;
    }

    public String getMesage() {
        return mesage;
    }

    public void setMesage(String mesage) {
        this.mesage = mesage;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Message{" +
                "mesage='" + mesage + '\'' +
                ", time='" + time + '\'' +
                '}';
    }
}
