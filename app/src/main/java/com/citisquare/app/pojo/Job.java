package com.citisquare.app.pojo;

/**
 * Created by hlink56 on 1/6/16.
 */
public class Job {
    String companyName;
    String postName;
    String note;
    String experience;
    String timeAgo;
    String city;


    public Job() {

    }

    public Job(String companyName, String postName, String note, String experience, String timeAgo, String city) {
        this.companyName = companyName;
        this.postName = postName;
        this.note = note;
        this.experience = experience;
        this.timeAgo = timeAgo;
        this.city = city;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getTimeAgo() {
        return timeAgo;
    }

    public void setTimeAgo(String timeAgo) {
        this.timeAgo = timeAgo;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Job{" +
                "companyName='" + companyName + '\'' +
                ", postName='" + postName + '\'' +
                ", note='" + note + '\'' +
                ", experience='" + experience + '\'' +
                ", timeAgo='" + timeAgo + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
