package com.citisquare.app.pojo.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hlink56 on 4/1/17.
 */
public class PaymentDataFailure {

    @SerializedName("access_code")
    @Expose
    private String accessCode;
    @SerializedName("captured_amount")
    @Expose
    private String capturedAmount;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("fort_id")
    @Expose
    private String fortId;
    @SerializedName("response_code")
    @Expose
    private String responseCode;
    @SerializedName("transaction_message")
    @Expose
    private String transactionMessage;
    @SerializedName("authorized_amount")
    @Expose
    private String authorizedAmount;
    @SerializedName("refunded_amount")
    @Expose
    private String refundedAmount;
    @SerializedName("merchant_reference")
    @Expose
    private String merchantReference;
    @SerializedName("transaction_status")
    @Expose
    private String transactionStatus;
    @SerializedName("response_message")
    @Expose
    private String responseMessage;
    @SerializedName("query_command")
    @Expose
    private String queryCommand;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("transaction_code")
    @Expose
    private String transactionCode;
    @SerializedName("merchant_identifier")
    @Expose
    private String merchantIdentifier;
    @SerializedName("signature")
    @Expose
    private String signature;

    /**
     * @return The accessCode
     */
    public String getAccessCode() {
        return accessCode;
    }

    /**
     * @param accessCode The access_code
     */
    public void setAccessCode(String accessCode) {
        this.accessCode = accessCode;
    }

    /**
     * @return The capturedAmount
     */
    public String getCapturedAmount() {
        return capturedAmount;
    }

    /**
     * @param capturedAmount The captured_amount
     */
    public void setCapturedAmount(String capturedAmount) {
        this.capturedAmount = capturedAmount;
    }

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The fortId
     */
    public String getFortId() {
        return fortId;
    }

    /**
     * @param fortId The fort_id
     */
    public void setFortId(String fortId) {
        this.fortId = fortId;
    }

    /**
     * @return The responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode The response_code
     */
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * @return The transactionMessage
     */
    public String getTransactionMessage() {
        return transactionMessage;
    }

    /**
     * @param transactionMessage The transaction_message
     */
    public void setTransactionMessage(String transactionMessage) {
        this.transactionMessage = transactionMessage;
    }

    /**
     * @return The authorizedAmount
     */
    public String getAuthorizedAmount() {
        return authorizedAmount;
    }

    /**
     * @param authorizedAmount The authorized_amount
     */
    public void setAuthorizedAmount(String authorizedAmount) {
        this.authorizedAmount = authorizedAmount;
    }

    /**
     * @return The refundedAmount
     */
    public String getRefundedAmount() {
        return refundedAmount;
    }

    /**
     * @param refundedAmount The refunded_amount
     */
    public void setRefundedAmount(String refundedAmount) {
        this.refundedAmount = refundedAmount;
    }

    /**
     * @return The merchantReference
     */
    public String getMerchantReference() {
        return merchantReference;
    }

    /**
     * @param merchantReference The merchant_reference
     */
    public void setMerchantReference(String merchantReference) {
        this.merchantReference = merchantReference;
    }

    /**
     * @return The transactionStatus
     */
    public String getTransactionStatus() {
        return transactionStatus;
    }

    /**
     * @param transactionStatus The transaction_status
     */
    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    /**
     * @return The responseMessage
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * @param responseMessage The response_message
     */
    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    /**
     * @return The queryCommand
     */
    public String getQueryCommand() {
        return queryCommand;
    }

    /**
     * @param queryCommand The query_command
     */
    public void setQueryCommand(String queryCommand) {
        this.queryCommand = queryCommand;
    }

    /**
     * @return The language
     */
    public String getLanguage() {
        return language;
    }

    /**
     * @param language The language
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * @return The transactionCode
     */
    public String getTransactionCode() {
        return transactionCode;
    }

    /**
     * @param transactionCode The transaction_code
     */
    public void setTransactionCode(String transactionCode) {
        this.transactionCode = transactionCode;
    }

    /**
     * @return The merchantIdentifier
     */
    public String getMerchantIdentifier() {
        return merchantIdentifier;
    }

    /**
     * @param merchantIdentifier The merchant_identifier
     */
    public void setMerchantIdentifier(String merchantIdentifier) {
        this.merchantIdentifier = merchantIdentifier;
    }

    /**
     * @return The signature
     */
    public String getSignature() {
        return signature;
    }

    /**
     * @param signature The signature
     */
    public void setSignature(String signature) {
        this.signature = signature;
    }

}
