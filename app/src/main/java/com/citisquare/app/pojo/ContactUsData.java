package com.citisquare.app.pojo;

/**
 * Created by hlink56 on 27/12/16.
 */

public class ContactUsData {

    String companyName;
    String address;
    String businessHour;
    String phone;
    String fax;
    String email;
    String website;

    public ContactUsData(String companyName, String address, String businessHour, String phone, String fax, String email, String website) {
        this.companyName = companyName;
        this.address = address;
        this.businessHour = businessHour;
        this.phone = phone;
        this.fax = fax;
        this.email = email;
        this.website = website;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBusinessHour() {
        return businessHour;
    }

    public void setBusinessHour(String businessHour) {
        this.businessHour = businessHour;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    /*

Full Company Name: RMS Advertising Agency.
Address Al Khobar-Prince Bander Street. Suwekat Cross 15.

Business Hours 08:00 AM  To 05:00 PM

Phone: 00966138995594
Fax: 00966138995594

E-mail: info@rmsadv.com.sa
Website: www.rmsadv.com.sa*/
}
