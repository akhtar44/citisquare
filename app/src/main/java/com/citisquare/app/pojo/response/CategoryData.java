package com.citisquare.app.pojo.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hlink56 on 9/8/16.
 */
public class CategoryData implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("parent_id")
    @Expose
    private String parentId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("ar_name")
    @Expose
    private String arName;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("is_parent")
    @Expose
    private String isParent;
    @SerializedName("is_active")
    @Expose
    private String isActive;
    @SerializedName("insertdate")
    @Expose
    private String insertdate;
    @SerializedName("sub_category")
    @Expose
    private List<SubCategory> subCategory = new ArrayList<SubCategory>();
    @SerializedName("category_img_url")
    @Expose
    private String categoryImgUrl;
    @SerializedName("category_img_thumb_url")
    @Expose
    private String categoryImgThumbUrl;
    private String subcategoryImgThumbUrl;
    boolean isSelected;

    boolean isFinallySelected;


    public CategoryData(String id) {
        this.id = id;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The parentId
     */
    public String getParentId() {
        return parentId;
    }

    /**
     * @param parentId The parent_id
     */
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    public String getArName() {
        return arName;
    }

    public void setArName(String arName) {
        this.arName = arName;
    }

    /**
     * @return The image
     */


    public String getImage() {
        return image;
    }

    /**
     * @param image The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * @return The isParent
     */
    public String getIsParent() {
        return isParent;
    }

    /**
     * @param isParent The is_parent
     */
    public void setIsParent(String isParent) {
        this.isParent = isParent;
    }

    /**
     * @return The isActive
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * @param isActive The is_active
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    /**
     * @return The insertdate
     */
    public String getInsertdate() {
        return insertdate;
    }

    /**
     * @param insertdate The insertdate
     */
    public void setInsertdate(String insertdate) {
        this.insertdate = insertdate;
    }

    /**
     * @return The subCategory
     */
    public List<SubCategory> getSubCategory() {
        return subCategory;
    }

    /**
     * @param subCategory The sub_category
     */
    public void setSubCategory(List<SubCategory> subCategory) {
        this.subCategory = subCategory;
    }

    /**
     * @return The categoryImgUrl
     */
    public String getCategoryImgUrl() {
        return categoryImgUrl;
    }

    /**
     * @param categoryImgUrl The category_img_url
     */
    public void setCategoryImgUrl(String categoryImgUrl) {
        this.categoryImgUrl = categoryImgUrl;
    }

    /**
     * @return The categoryImgThumbUrl
     */
    public String getCategoryImgThumbUrl() {
        return categoryImgThumbUrl;
    }
    public String getSubcategoryImgThumbUrl() {
        return subcategoryImgThumbUrl;
    }
    /**
     * @param categoryImgThumbUrl The category_img_thumb_url
     */
    public void setCategoryImgThumbUrl(String categoryImgThumbUrl) {
        this.categoryImgThumbUrl = categoryImgThumbUrl;
    }

    @Override
    public String toString() {
        return "CategoryData{" +
                "name='" + name + '\'' +
                '}';
    }
}
