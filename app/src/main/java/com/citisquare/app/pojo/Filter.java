package com.citisquare.app.pojo;

/**
 * Created by hlink56 on 2/6/16.
 */
public class Filter {
    String name;

    public Filter(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Filter{" +
                "name='" + name + '\'' +
                '}';
    }
}


