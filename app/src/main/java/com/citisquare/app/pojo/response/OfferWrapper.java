package com.citisquare.app.pojo.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hlink56 on 22/8/16.
 */
public class OfferWrapper {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<OfferData> data = new ArrayList<OfferData>();
    @SerializedName("is_hurryup")
    @Expose
    private String isHurryup;

    /**
     * @return The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    public String getIsHurryup() {
        return isHurryup;
    }

    public void setIsHurryup(String isHurryup) {
        this.isHurryup = isHurryup;
    }

    /**
     * @return The data
     */
    public List<OfferData> getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(List<OfferData> data) {
        this.data = data;
    }
}
