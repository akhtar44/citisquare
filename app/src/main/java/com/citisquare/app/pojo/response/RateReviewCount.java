package com.citisquare.app.pojo.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hlink56 on 31/1/17.
 */

public class RateReviewCount{

    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("one_count")
    @Expose
    private String oneCount;
    @SerializedName("two_count")
    @Expose
    private String twoCount;
    @SerializedName("three_count")
    @Expose
    private String threeCount;
    @SerializedName("four_count")
    @Expose
    private String fourCount;
    @SerializedName("five_count")
    @Expose
    private String fiveCount;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getOneCount() {
        return oneCount;
    }

    public void setOneCount(String oneCount) {
        this.oneCount = oneCount;
    }

    public String getTwoCount() {
        return twoCount;
    }

    public void setTwoCount(String twoCount) {
        this.twoCount = twoCount;
    }

    public String getThreeCount() {
        return threeCount;
    }

    public void setThreeCount(String threeCount) {
        this.threeCount = threeCount;
    }

    public String getFourCount() {
        return fourCount;
    }

    public void setFourCount(String fourCount) {
        this.fourCount = fourCount;
    }

    public String getFiveCount() {
        return fiveCount;
    }

    public void setFiveCount(String fiveCount) {
        this.fiveCount = fiveCount;
    }

}
