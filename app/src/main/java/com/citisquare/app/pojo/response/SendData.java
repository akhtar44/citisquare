package com.citisquare.app.pojo.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hlink56 on 10/8/16.
 */
public class SendData {
    @SerializedName("chat_id")
    @Expose
    private String chatId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("insertdate")
    @Expose
    private String insertdate;

    /**
     * @return The chatId
     */
    public String getChatId() {
        return chatId;
    }

    /**
     * @param chatId The chat_id
     */
    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    /**
     * @return The userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId The user_id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The insertdate
     */
    public String getInsertdate() {
        return insertdate;
    }

    /**
     * @param insertdate The insertdate
     */
    public void setInsertdate(String insertdate) {
        this.insertdate = insertdate;
    }


    @Override
    public String toString() {
        return "SendData{" +
                "chatId='" + chatId + '\'' +
                ", userId='" + userId + '\'' +
                ", message='" + message + '\'' +
                ", insertdate='" + insertdate + '\'' +
                '}';
    }
}
