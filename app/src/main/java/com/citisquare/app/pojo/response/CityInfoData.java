package com.citisquare.app.pojo.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hlink56 on 27/9/16.
 */
public class CityInfoData {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("web_view_id")
    @Expose
    private String webViewId;
    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("is_active")
    @Expose
    private String isActive;
    @SerializedName("insertdate")
    @Expose
    private String insertdate;
    @SerializedName("ar_title")
    @Expose
    private String arTitle;
    @SerializedName("ar_content")
    @Expose
    private String arContent;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("web_view_image")
    @Expose
    private String webViewImage;
    @SerializedName("web_view_image_thumb")
    @Expose
    private String webViewImageThumb;

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The webViewId
     */
    public String getWebViewId() {
        return webViewId;
    }

    /**
     * @param webViewId The web_view_id
     */
    public void setWebViewId(String webViewId) {
        this.webViewId = webViewId;
    }

    /**
     * @return The cityId
     */
    public String getCityId() {
        return cityId;
    }

    /**
     * @param cityId The city_id
     */
    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    /**
     * @return The content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content The content
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * @return The isActive
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * @param isActive The is_active
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    /**
     * @return The insertdate
     */
    public String getInsertdate() {
        return insertdate;
    }

    /**
     * @param insertdate The insertdate
     */
    public void setInsertdate(String insertdate) {
        this.insertdate = insertdate;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * @return The webViewImage
     */
    public String getWebViewImage() {
        return webViewImage;
    }

    /**
     * @param webViewImage The web_view_image
     */
    public void setWebViewImage(String webViewImage) {
        this.webViewImage = webViewImage;
    }

    /**
     * @return The webViewImageThumb
     */
    public String getWebViewImageThumb() {
        return webViewImageThumb;
    }

    /**
     * @param webViewImageThumb The web_view_image_thumb
     */
    public void setWebViewImageThumb(String webViewImageThumb) {
        this.webViewImageThumb = webViewImageThumb;
    }

    public String getArTitle() {
        return arTitle;
    }

    public void setArTitle(String arTitle) {
        this.arTitle = arTitle;
    }

    public String getArContent() {
        return arContent;
    }

    public void setArContent(String arContent) {
        this.arContent = arContent;
    }

    @Override
    public String toString() {
        return "CityInfoData{" +
                "id='" + id + '\'' +
                ", webViewId='" + webViewId + '\'' +
                ", cityId='" + cityId + '\'' +
                ", content='" + content + '\'' +
                ", isActive='" + isActive + '\'' +
                ", insertdate='" + insertdate + '\'' +
                ", arTitle='" + arTitle + '\'' +
                ", arContent='" + arContent + '\'' +
                ", title='" + title + '\'' +
                ", image='" + image + '\'' +
                ", webViewImage='" + webViewImage + '\'' +
                ", webViewImageThumb='" + webViewImageThumb + '\'' +
                '}';
    }
}
