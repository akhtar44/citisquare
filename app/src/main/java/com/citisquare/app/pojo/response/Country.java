package com.citisquare.app.pojo.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hlink56 on 2/8/16.
 */
public class Country {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("short_code")
    @Expose
    private String shortCode;
    @SerializedName("dial_code")
    @Expose
    private String dialCode;
    @SerializedName("is_active")
    @Expose
    private String isActive;
    @SerializedName("insertdate")
    @Expose
    private String insertdate;

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country The country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return The shortCode
     */
    public String getShortCode() {
        return shortCode;
    }

    /**
     * @param shortCode The short_code
     */
    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    /**
     * @return The dialCode
     */
    public String getDialCode() {
        return dialCode;
    }

    /**
     * @param dialCode The dial_code
     */
    public void setDialCode(String dialCode) {
        this.dialCode = dialCode;
    }

    /**
     * @return The isActive
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * @param isActive The is_active
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    /**
     * @return The insertdate
     */
    public String getInsertdate() {
        return insertdate;
    }

    /**
     * @param insertdate The insertdate
     */
    public void setInsertdate(String insertdate) {
        this.insertdate = insertdate;
    }

    @Override
    public String toString() {
        return "Country{" +
                "id='" + id + '\'' +
                ", country='" + country + '\'' +
                ", shortCode='" + shortCode + '\'' +
                ", dialCode='" + dialCode + '\'' +
                ", isActive='" + isActive + '\'' +
                ", insertdate='" + insertdate + '\'' +
                '}';
    }
}
