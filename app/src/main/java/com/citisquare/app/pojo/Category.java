package com.citisquare.app.pojo;

/**
 * Created by hlink56 on 25/5/16.
 */
public class Category {
    String category;
    boolean isSelected;

    public Category(String category) {
        this.category = category;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }


    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
