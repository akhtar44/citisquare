package com.citisquare.app.pojo;

/**
 * Created by hlink56 on 1/6/16.
 */
public class Following {

    String followingName;
    String image;
    boolean isSelected;



    public Following(){

    }

    public Following(String followingName, String image) {
        this.followingName = followingName;
        this.image = image;
    }

    public String getFollowingName() {
        return followingName;
    }

    public void setFollowingName(String followingName) {
        this.followingName = followingName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public String toString() {
        return "Following{" +
                "followingName='" + followingName + '\'' +
                ", image='" + image + '\'' +
                '}';
    }
}
