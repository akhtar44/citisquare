package com.citisquare.app.pojo;

/**
 * Created by hlink56 on 20/6/16.
 */
public class SubCategoryList {
    String companyName;
    String location;
    String data;
    String image;
    boolean isFollowed;

    public SubCategoryList(String companyName, String location, String data, String image) {
        this.companyName = companyName;
        this.location = location;
        this.data = data;
        this.image = image;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isFollowed() {
        return isFollowed;
    }

    public void setFollowed(boolean followed) {
        isFollowed = followed;
    }

    @Override
    public String toString() {
        return "SubCategoryList{" +
                "companyName='" + companyName + '\'' +
                ", location='" + location + '\'' +
                ", data='" + data + '\'' +
                ", image='" + image + '\'' +
                ", isFollowed=" + isFollowed +
                '}';
    }
}
