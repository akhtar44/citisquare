package com.citisquare.app.pojo.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hlink56 on 5/7/16.
 */
public class LoginData {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("sub_cat_id")
    @Expose
    private String subCatId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("alternate_email")
    @Expose
    private String alternateEmail;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("building_no")
    @Expose
    private String buildingNo;
    @SerializedName("street")
    @Expose
    private String street;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("country_code")
    @Expose
    private String countryCode;
    @SerializedName("phone_no")
    @Expose
    private String phoneNo;
    @SerializedName("cell_country_code")
    @Expose
    private String cellCountryCode;
    @SerializedName("cell_phoneno")
    @Expose
    private String cellPhoneno;
    @SerializedName("zipcode")
    @Expose
    private String zipcode;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("cover_image")
    @Expose
    private String coverImage;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("company_name_ar")
    @Expose
    private String companyNameAr;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("bio")
    @Expose
    private String bio;
    @SerializedName("business_licence_id")
    @Expose
    private String businessLicenceId;
    @SerializedName("branch_name")
    @Expose
    private String branchName;
    @SerializedName("branch_id")
    @Expose
    private String branchId;
    @SerializedName("device_type")
    @Expose
    private String deviceType;
    @SerializedName("device_token")
    @Expose
    private String deviceToken;
    @SerializedName("role")
    @Expose
    private String role;
    @SerializedName("login_type")
    @Expose
    private String loginType;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("seller_type")
    @Expose
    private String sellerType;
    @SerializedName("plan_expiry_date")
    @Expose
    private String planExpiryDate;
    @SerializedName("is_delivery")
    @Expose
    private String isDelivery;
    @SerializedName("is_active")
    @Expose
    private String isActive;
    @SerializedName("insertdate")
    @Expose
    private String insertdate;
    @SerializedName("city_name")
    @Expose
    private String cityName;
    @SerializedName("country_name")
    @Expose
    private String countryName;
    @SerializedName("profile_image_thumb")
    @Expose
    private String profileImageThumb;
    @SerializedName("business_id_image_thumb")
    @Expose
    private String businessIdImageThumb;
    @SerializedName("user_session_id")
    @Expose
    private String userSessionId;
    @SerializedName("category_count")
    @Expose
    private String categoryCount;
    @SerializedName("notification")
    @Expose
    private String notification;
    @SerializedName("business_hours")
    @Expose
    private String businessHours;
    @SerializedName("language")
    @Expose
    private String language;

    @SerializedName("cover_image_thumb")
    @Expose
    private String coverImageThumb;


    public LoginData(String id
            , String categoryId
            , String subCatId
            , String firstName
            , String lastName
            , String username
            , String email
            , String password
            , String buildingNo
            , String street
            , String city
            , String country
            , String countryCode
            , String phoneNo
            , String cellCountryCode
            , String cellPhoneno
            , String profileImage
            , String coverImage
            , String website
            , String bio
            , String businessLicenceId
            , String branchName
            , String deviceType
            , String deviceToken
            , String role
            , String loginType
            , String latitude
            , String longitude
            , String sellerType
            , String isDelivery
            , String isActive
            , String insertdate
            , String profileImageThumb
            , String businessIdImageThumb
            , String userSessionId, String language) {
        this.id = id;
        this.categoryId = categoryId;
        this.subCatId = subCatId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.email = email;
        this.password = password;
        this.buildingNo = buildingNo;
        this.street = street;
        this.city = city;
        this.country = country;
        this.countryCode = countryCode;
        this.phoneNo = phoneNo;
        this.cellCountryCode = cellCountryCode;
        this.cellPhoneno = cellPhoneno;
        this.profileImage = profileImage;
        this.coverImage = coverImage;
        this.website = website;
        this.bio = bio;
        this.businessLicenceId = businessLicenceId;
        this.branchName = branchName;
        this.deviceType = deviceType;
        this.deviceToken = deviceToken;
        this.role = role;
        this.loginType = loginType;
        this.latitude = latitude;
        this.longitude = longitude;
        this.sellerType = sellerType;
        this.isDelivery = isDelivery;
        this.isActive = isActive;
        this.insertdate = insertdate;
        this.profileImageThumb = profileImageThumb;
        this.businessIdImageThumb = businessIdImageThumb;
        this.userSessionId = userSessionId;
        this.language = language;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getSubCatId() {
        return subCatId;
    }

    public void setSubCatId(String subCatId) {
        this.subCatId = subCatId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAlternateEmail() {
        return alternateEmail;
    }

    public void setAlternateEmail(String alternateEmail) {
        this.alternateEmail = alternateEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBuildingNo() {
        return buildingNo;
    }

    public void setBuildingNo(String buildingNo) {
        this.buildingNo = buildingNo;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getCellCountryCode() {
        return cellCountryCode;
    }

    public void setCellCountryCode(String cellCountryCode) {
        this.cellCountryCode = cellCountryCode;
    }

    public String getCellPhoneno() {
        return cellPhoneno;
    }

    public void setCellPhoneno(String cellPhoneno) {
        this.cellPhoneno = cellPhoneno;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getBusinessLicenceId() {
        return businessLicenceId;
    }

    public void setBusinessLicenceId(String businessLicenceId) {
        this.businessLicenceId = businessLicenceId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getSellerType() {
        return sellerType;
    }

    public void setSellerType(String sellerType) {
        this.sellerType = sellerType;
    }

    public String getPlanExpiryDate() {
        return planExpiryDate;
    }

    public void setPlanExpiryDate(String planExpiryDate) {
        this.planExpiryDate = planExpiryDate;
    }

    public String getIsDelivery() {
        return isDelivery;
    }

    public void setIsDelivery(String isDelivery) {
        this.isDelivery = isDelivery;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getInsertdate() {
        return insertdate;
    }

    public void setInsertdate(String insertdate) {
        this.insertdate = insertdate;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getProfileImageThumb() {
        return profileImageThumb;
    }

    public void setProfileImageThumb(String profileImageThumb) {
        this.profileImageThumb = profileImageThumb;
    }

    public String getBusinessIdImageThumb() {
        return businessIdImageThumb;
    }

    public void setBusinessIdImageThumb(String businessIdImageThumb) {
        this.businessIdImageThumb = businessIdImageThumb;
    }

    public String getUserSessionId() {
        return userSessionId;
    }

    public void setUserSessionId(String userSessionId) {
        this.userSessionId = userSessionId;
    }

    public String getCategoryCount() {
        return categoryCount;
    }

    public void setCategoryCount(String categoryCount) {
        this.categoryCount = categoryCount;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public String getBusinessHours() {
        return businessHours;
    }

    public void setBusinessHours(String businessHours) {
        this.businessHours = businessHours;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCompanyNameAr() {
        return companyNameAr;
    }

    public void setCompanyNameAr(String companyNameAr) {
        this.companyNameAr = companyNameAr;
    }

    public String getCoverImageThumb() {
        return coverImageThumb;
    }

    public void setCoverImageThumb(String coverImageThumb) {
        this.coverImageThumb = coverImageThumb;
    }

    @Override
    public String toString() {
        return "LoginData{" +
                "id='" + id + '\'' +
                ", categoryId='" + categoryId + '\'' +
                ", subCatId='" + subCatId + '\'' +
                ", title='" + title + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", alternateEmail='" + alternateEmail + '\'' +
                ", password='" + password + '\'' +
                ", buildingNo='" + buildingNo + '\'' +
                ", street='" + street + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", phoneNo='" + phoneNo + '\'' +
                ", cellCountryCode='" + cellCountryCode + '\'' +
                ", cellPhoneno='" + cellPhoneno + '\'' +
                ", zipcode='" + zipcode + '\'' +
                ", profileImage='" + profileImage + '\'' +
                ", coverImage='" + coverImage + '\'' +
                ", companyName='" + companyName + '\'' +
                ", companyNameAr='" + companyNameAr + '\'' +
                ", website='" + website + '\'' +
                ", bio='" + bio + '\'' +
                ", businessLicenceId='" + businessLicenceId + '\'' +
                ", branchName='" + branchName + '\'' +
                ", branchId='" + branchId + '\'' +
                ", deviceType='" + deviceType + '\'' +
                ", deviceToken='" + deviceToken + '\'' +
                ", role='" + role + '\'' +
                ", loginType='" + loginType + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", sellerType='" + sellerType + '\'' +
                ", planExpiryDate='" + planExpiryDate + '\'' +
                ", isDelivery='" + isDelivery + '\'' +
                ", isActive='" + isActive + '\'' +
                ", insertdate='" + insertdate + '\'' +
                ", cityName='" + cityName + '\'' +
                ", countryName='" + countryName + '\'' +
                ", profileImageThumb='" + profileImageThumb + '\'' +
                ", businessIdImageThumb='" + businessIdImageThumb + '\'' +
                ", userSessionId='" + userSessionId + '\'' +
                ", categoryCount='" + categoryCount + '\'' +
                ", notification='" + notification + '\'' +
                ", businessHours='" + businessHours + '\'' +
                ", language='" + language + '\'' +
                '}';
    }
}