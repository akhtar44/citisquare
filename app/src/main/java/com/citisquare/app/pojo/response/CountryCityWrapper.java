package com.citisquare.app.pojo.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hlink56 on 2/8/16.
 */
public class CountryCityWrapper {
    @SerializedName("country")
    @Expose
    private List<Country> country = new ArrayList<Country>();
    @SerializedName("city")
    @Expose
    private List<com.citisquare.app.pojo.response.City> city = new ArrayList<com.citisquare.app.pojo.response.City>();

    /**
     * @return The country
     */
    public List<Country> getCountry() {
        return country;
    }

    /**
     * @param country The country
     */
    public void setCountry(List<Country> country) {
        this.country = country;
    }

    /**
     * @return The city
     */
    public List<com.citisquare.app.pojo.response.City> getCity() {
        return city;
    }

    /**
     * @param city The city
     */
    public void setCity(List<com.citisquare.app.pojo.response.City> city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "CountryCityWrapper{" +
                "country=" + country +
                ", city=" + city +
                '}';
    }
}
