package com.citisquare.app.pojo;

/**
 * Created by hlink56 on 31/5/16.
 */
public class City {
    String name;
    String image;

    public City() {

    }

    public City(String name, String image) {
        this.name = name;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "City{" +
                "image='" + image + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
