package com.citisquare.app.pojo.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hlink56 on 31/8/16.
 */
public class SellerData {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("company_name")
    @Expose
    private String companyName;

    @SerializedName("company_name_ar")
    @Expose
    private String companyNameArabic;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("cell_country_code")
    @Expose
    private String cellCountryCode;
    @SerializedName("cell_phoneno")
    @Expose
    private String cellPhoneno;
    @SerializedName("building_no")
    @Expose
    private String buildingNo;
    @SerializedName("street")
    @Expose
    private String street;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("bio")
    @Expose
    private String bio;
    @SerializedName("rate")
    @Expose
    private String rate;
    @SerializedName("is_active")
    @Expose
    private String isActive;
    @SerializedName("city_name")
    @Expose
    private String cityName;
    @SerializedName("profile_image_thumb")
    @Expose
    private String profileImageThumb;
    @SerializedName("is_following")
    @Expose
    private Integer isFollowing;
    @SerializedName("rating_count")
    @Expose
    private Integer ratingCount;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getCellCountryCode() {
        return cellCountryCode;
    }

    public void setCellCountryCode(String cellCountryCode) {
        this.cellCountryCode = cellCountryCode;
    }

    public String getCellPhoneno() {
        return cellPhoneno;
    }

    public void setCellPhoneno(String cellPhoneno) {
        this.cellPhoneno = cellPhoneno;
    }

    public String getBuildingNo() {
        return buildingNo;
    }

    public void setBuildingNo(String buildingNo) {
        this.buildingNo = buildingNo;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getProfileImageThumb() {
        return profileImageThumb;
    }

    public void setProfileImageThumb(String profileImageThumb) {
        this.profileImageThumb = profileImageThumb;
    }

    public Integer getIsFollowing() {
        return isFollowing;
    }

    public void setIsFollowing(Integer isFollowing) {
        this.isFollowing = isFollowing;
    }

    public Integer getRatingCount() {
        return ratingCount;
    }

    public void setRatingCount(Integer ratingCount) {
        this.ratingCount = ratingCount;
    }


    public String getCompanyNameArabic() {
        return companyNameArabic;
    }

    public void setCompanyNameArabic(String companyNameArabic) {
        this.companyNameArabic = companyNameArabic;
    }
}