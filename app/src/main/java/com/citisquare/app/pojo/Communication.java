package com.citisquare.app.pojo;

/**
 * Created by hlink56 on 26/7/16.
 */
public class Communication {
    private String text;
    private boolean isLeft;

    public Communication(String text, boolean isLeft) {
        this.text = text;
        this.isLeft = isLeft;
    }

    public boolean isLeft() {
        return isLeft;
    }

    public void setIsLeft(boolean isLeft) {
        this.isLeft = isLeft;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}