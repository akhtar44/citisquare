package com.citisquare.app.pojo;

/**
 * Created by hlink56 on 4/6/16.
 */
public class NotificationApp {
    String name;
    String notificationFor;
    String notificationType;

    public NotificationApp(String name, String notificationFor, String notificationType) {
        this.name = name;
        this.notificationFor = notificationFor;
        this.notificationType = notificationType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNotificationFor() {
        return notificationFor;
    }

    public void setNotificationFor(String notificationFor) {
        this.notificationFor = notificationFor;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    @Override
    public String toString() {
        return "NotificationApp{" +
                "name='" + name + '\'' +
                ", notificationFor='" + notificationFor + '\'' +
                ", notificationType='" + notificationType + '\'' +
                '}';
    }
}

