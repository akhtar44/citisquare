package com.citisquare.app.pojo.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hlink56 on 20/1/17.
 */

public class SliderData {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("slide_no")
    @Expose
    private String slideNo;
    @SerializedName("is_active")
    @Expose
    private String isActive;
    @SerializedName("insertdate")
    @Expose
    private String insertdate;
    @SerializedName("slider_image")
    @Expose
    private String sliderImage;
    @SerializedName("slider_image_thumb")
    @Expose
    private String sliderImageThumb;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSlideNo() {
        return slideNo;
    }

    public void setSlideNo(String slideNo) {
        this.slideNo = slideNo;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getInsertdate() {
        return insertdate;
    }

    public void setInsertdate(String insertdate) {
        this.insertdate = insertdate;
    }

    public String getSliderImage() {
        return sliderImage;
    }

    public void setSliderImage(String sliderImage) {
        this.sliderImage = sliderImage;
    }

    public String getSliderImageThumb() {
        return sliderImageThumb;
    }

    public void setSliderImageThumb(String sliderImageThumb) {
        this.sliderImageThumb = sliderImageThumb;
    }

    @Override
    public String toString() {
        return "SliderData{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", image='" + image + '\'' +
                ", slideNo='" + slideNo + '\'' +
                ", isActive='" + isActive + '\'' +
                ", insertdate='" + insertdate + '\'' +
                ", sliderImage='" + sliderImage + '\'' +
                ", sliderImageThumb='" + sliderImageThumb + '\'' +
                '}';
    }
}
