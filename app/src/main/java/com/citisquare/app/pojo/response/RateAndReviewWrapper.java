package com.citisquare.app.pojo.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by hlink56 on 31/1/17.
 */

public class RateAndReviewWrapper{

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("user_detail")
    @Expose
    private RateUserDetail userDetail;
    @SerializedName("count")
    @Expose
    private RateReviewCount count;
    @SerializedName("data")
    @Expose
    private List<RateReviewData> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public RateUserDetail getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(RateUserDetail userDetail) {
        this.userDetail = userDetail;
    }

    public RateReviewCount getCount() {
        return count;
    }

    public void setCount(RateReviewCount count) {
        this.count = count;
    }

    public List<RateReviewData> getData() {
        return data;
    }

    public void setData(List<RateReviewData> data) {
        this.data = data;
    }

}
