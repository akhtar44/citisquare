package com.citisquare.app.pojo;

/**
 * Created by hlink56 on 31/5/16.
 */
public class Inbox {
    String name;
    String data;
    String image;


    public Inbox() {

    }

    public Inbox(String name, String data, String image) {
        this.name = name;
        this.data = data;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Inbox{" +
                "name='" + name + '\'' +
                ", data='" + data + '\'' +
                ", image='" + image + '\'' +
                '}';
    }
}
