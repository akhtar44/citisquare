package com.citisquare.app.pojo.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by hlink56 on 9/9/16.
 */
public class ProfileData implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("sub_cat_id")
    @Expose
    private String subCatId;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("company_name_ar")
    @Expose
    private String companyNameAr;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("building_no")
    @Expose
    private String buildingNo;
    @SerializedName("street")
    @Expose
    private String street;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("country_code")
    @Expose
    private String countryCode;
    @SerializedName("phone_no")
    @Expose
    private String phoneNo;
    @SerializedName("cell_country_code")
    @Expose
    private String cellCountryCode;
    @SerializedName("cell_phoneno")
    @Expose
    private String cellPhoneno;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("cover_image")
    @Expose
    private String coverImage;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("bio")
    @Expose
    private String bio;
    @SerializedName("business_licence_id")
    @Expose
    private String businessLicenceId;
    @SerializedName("branch_name")
    @Expose
    private String branchName;
    @SerializedName("device_type")
    @Expose
    private String deviceType;
    @SerializedName("device_token")
    @Expose
    private String deviceToken;
    @SerializedName("role")
    @Expose
    private String role;
    @SerializedName("login_type")
    @Expose
    private String loginType;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("seller_type")
    @Expose
    private String sellerType;
    @SerializedName("is_delivery")
    @Expose
    private String isDelivery;
    @SerializedName("is_active")
    @Expose
    private String isActive;
    @SerializedName("insertdate")
    @Expose
    private String insertdate;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("is_following")
    @Expose
    private Integer isFollowing;
    @SerializedName("profile_image_thumb")
    @Expose
    private String profileImageThumb;
    @SerializedName("business_id_image_thumb")
    @Expose
    private String businessIdImageThumb;
    @SerializedName("cover_image_thumb")
    @Expose
    private String coverImageThumb;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("business_hours")
    @Expose
    private String businessHours;
    @SerializedName("zipcode")
    @Expose
    private String zipcode;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The categoryId
     */
    public String getCategoryId() {
        return categoryId;
    }

    /**
     * @param categoryId The category_id
     */
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * @return The subCatId
     */
    public String getSubCatId() {
        return subCatId;
    }

    /**
     * @param subCatId The sub_cat_id
     */
    public void setSubCatId(String subCatId) {
        this.subCatId = subCatId;
    }

    /**
     * @return The firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName The first_name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return The lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName The last_name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return The username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return The password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password The password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return The buildingNo
     */
    public String getBuildingNo() {
        return buildingNo;
    }

    /**
     * @param buildingNo The building_no
     */
    public void setBuildingNo(String buildingNo) {
        this.buildingNo = buildingNo;
    }

    /**
     * @return The street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street The street
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return The city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return The country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country The country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return The countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * @param countryCode The country_code
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    /**
     * @return The phoneNo
     */
    public String getPhoneNo() {
        return phoneNo;
    }

    /**
     * @param phoneNo The phone_no
     */
    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    /**
     * @return The cellCountryCode
     */
    public String getCellCountryCode() {
        return cellCountryCode;
    }

    /**
     * @param cellCountryCode The cell_country_code
     */
    public void setCellCountryCode(String cellCountryCode) {
        this.cellCountryCode = cellCountryCode;
    }

    /**
     * @return The cellPhoneno
     */
    public String getCellPhoneno() {
        return cellPhoneno;
    }

    /**
     * @param cellPhoneno The cell_phoneno
     */
    public void setCellPhoneno(String cellPhoneno) {
        this.cellPhoneno = cellPhoneno;
    }

    /**
     * @return The profileImage
     */
    public String getProfileImage() {
        return profileImage;
    }

    /**
     * @param profileImage The profile_image
     */
    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    /**
     * @return The coverImage
     */
    public String getCoverImage() {
        return coverImage;
    }

    /**
     * @param coverImage The cover_image
     */
    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    /**
     * @return The website
     */
    public String getWebsite() {
        return website;
    }

    /**
     * @param website The website
     */
    public void setWebsite(String website) {
        this.website = website;
    }

    /**
     * @return The bio
     */
    public String getBio() {
        return bio;
    }

    /**
     * @param bio The bio
     */
    public void setBio(String bio) {
        this.bio = bio;
    }

    /**
     * @return The businessLicenceId
     */
    public String getBusinessLicenceId() {
        return businessLicenceId;
    }

    /**
     * @param businessLicenceId The business_licence_id
     */
    public void setBusinessLicenceId(String businessLicenceId) {
        this.businessLicenceId = businessLicenceId;
    }

    /**
     * @return The branchName
     */
    public String getBranchName() {
        return branchName;
    }

    /**
     * @param branchName The branch_name
     */
    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    /**
     * @return The deviceType
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * @param deviceType The device_type
     */
    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    /**
     * @return The deviceToken
     */
    public String getDeviceToken() {
        return deviceToken;
    }

    /**
     * @param deviceToken The device_token
     */
    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    /**
     * @return The role
     */
    public String getRole() {
        return role;
    }

    /**
     * @param role The role
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * @return The loginType
     */
    public String getLoginType() {
        return loginType;
    }

    /**
     * @param loginType The login_type
     */
    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    /**
     * @return The latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * @param latitude The latitude
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * @return The longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * @param longitude The longitude
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     * @return The sellerType
     */
    public String getSellerType() {
        return sellerType;
    }

    /**
     * @param sellerType The seller_type
     */
    public void setSellerType(String sellerType) {
        this.sellerType = sellerType;
    }

    /**
     * @return The isDelivery
     */
    public String getIsDelivery() {
        return isDelivery;
    }

    /**
     * @param isDelivery The is_delivery
     */
    public void setIsDelivery(String isDelivery) {
        this.isDelivery = isDelivery;
    }

    /**
     * @return The isActive
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * @param isActive The is_active
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    /**
     * @return The insertdate
     */
    public String getInsertdate() {
        return insertdate;
    }

    /**
     * @param insertdate The insertdate
     */
    public void setInsertdate(String insertdate) {
        this.insertdate = insertdate;
    }

    /**
     * @return The userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId The user_id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return The isFollowing
     */
    public Integer getIsFollowing() {
        return isFollowing;
    }

    /**
     * @param isFollowing The is_following
     */
    public void setIsFollowing(Integer isFollowing) {
        this.isFollowing = isFollowing;
    }

    /**
     * @return The profileImageThumb
     */
    public String getProfileImageThumb() {
        return profileImageThumb;
    }

    /**
     * @param profileImageThumb The profile_image_thumb
     */
    public void setProfileImageThumb(String profileImageThumb) {
        this.profileImageThumb = profileImageThumb;
    }

    /**
     * @return The businessIdImageThumb
     */
    public String getBusinessIdImageThumb() {
        return businessIdImageThumb;
    }

    /**
     * @param businessIdImageThumb The business_id_image_thumb
     */
    public void setBusinessIdImageThumb(String businessIdImageThumb) {
        this.businessIdImageThumb = businessIdImageThumb;
    }

    /**
     * @return The coverImageThumb
     */
    public String getCoverImageThumb() {
        return coverImageThumb;
    }

    /**
     * @param coverImageThumb The cover_image_thumb
     */
    public void setCoverImageThumb(String coverImageThumb) {
        this.coverImageThumb = coverImageThumb;
    }

    public String getBusinessHours() {
        return businessHours;
    }

    public void setBusinessHours(String businessHours) {
        this.businessHours = businessHours;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyNameAr() {
        return companyNameAr;
    }

    public void setCompanyNameAr(String companyNameAr) {
        this.companyNameAr = companyNameAr;
    }
}
