package com.citisquare.app.pojo.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hlink56 on 15/9/16.
 */
public class DetailsData {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("media_name")
    @Expose
    private String mediaName;
    @SerializedName("video_thumb")
    @Expose
    private String videoThumb;
    @SerializedName("media_type")
    @Expose
    private String mediaType;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("is_active")
    @Expose
    private String isActive;
    @SerializedName("insertdate")
    @Expose
    private String insertdate;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("city_name")
    @Expose
    private String cityName;
    @SerializedName("profile_image_thumb")
    @Expose
    private String profileImageThumb;
    @SerializedName("media_name_thumb")
    @Expose
    private String mediaNameThumb;
    @SerializedName("is_following")
    @Expose
    private Integer isFollowing;
    @SerializedName("is_like")
    @Expose
    private Integer isLike;
    @SerializedName("like_count")
    @Expose
    private Integer likeCount;
    @SerializedName("comment_count")
    @Expose
    private Integer commentCount;
    @SerializedName("url")
    @Expose
    private String url;

    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("company_name_ar")
    @Expose
    private String companyNameAr;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyNameAr() {
        return companyNameAr;
    }

    public void setCompanyNameAr(String companyNameAr) {
        this.companyNameAr = companyNameAr;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId The user_id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return The cityId
     */
    public String getCityId() {
        return cityId;
    }

    /**
     * @param cityId The city_id
     */
    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The mediaName
     */
    public String getMediaName() {
        return mediaName;
    }

    /**
     * @param mediaName The media_name
     */
    public void setMediaName(String mediaName) {
        this.mediaName = mediaName;
    }

    /**
     * @return The videoThumb
     */
    public String getVideoThumb() {
        return videoThumb;
    }

    /**
     * @param videoThumb The video_thumb
     */
    public void setVideoThumb(String videoThumb) {
        this.videoThumb = videoThumb;
    }

    /**
     * @return The mediaType
     */
    public String getMediaType() {
        return mediaType;
    }

    /**
     * @param mediaType The media_type
     */
    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    /**
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The isActive
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * @param isActive The is_active
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    /**
     * @return The insertdate
     */
    public String getInsertdate() {
        return insertdate;
    }

    /**
     * @param insertdate The insertdate
     */
    public void setInsertdate(String insertdate) {
        this.insertdate = insertdate;
    }

    /**
     * @return The profileImage
     */
    public String getProfileImage() {
        return profileImage;
    }

    /**
     * @param profileImage The profile_image
     */
    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    /**
     * @return The firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName The first_name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return The lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName The last_name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return The username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return The cityName
     */
    public String getCityName() {
        return cityName;
    }

    /**
     * @param cityName The city_name
     */
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    /**
     * @return The profileImageThumb
     */
    public String getProfileImageThumb() {
        return profileImageThumb;
    }

    /**
     * @param profileImageThumb The profile_image_thumb
     */
    public void setProfileImageThumb(String profileImageThumb) {
        this.profileImageThumb = profileImageThumb;
    }

    /**
     * @return The mediaNameThumb
     */
    public String getMediaNameThumb() {
        return mediaNameThumb;
    }

    /**
     * @param mediaNameThumb The media_name_thumb
     */
    public void setMediaNameThumb(String mediaNameThumb) {
        this.mediaNameThumb = mediaNameThumb;
    }

    /**
     * @return The isFollowing
     */
    public Integer getIsFollowing() {
        return isFollowing;
    }

    /**
     * @param isFollowing The is_following
     */
    public void setIsFollowing(Integer isFollowing) {
        this.isFollowing = isFollowing;
    }

    /**
     * @return The isLike
     */
    public Integer getIsLike() {
        return isLike;
    }

    /**
     * @param isLike The is_like
     */
    public void setIsLike(Integer isLike) {
        this.isLike = isLike;
    }

    /**
     * @return The likeCount
     */
    public Integer getLikeCount() {
        return likeCount;
    }

    /**
     * @param likeCount The like_count
     */
    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }

    /**
     * @return The commentCount
     */
    public Integer getCommentCount() {
        return commentCount;
    }

    /**
     * @param commentCount The comment_count
     */
    public void setCommentCount(Integer commentCount) {
        this.commentCount = commentCount;
    }
}
