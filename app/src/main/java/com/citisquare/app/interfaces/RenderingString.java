package com.citisquare.app.interfaces;

/**
 * Created by hlink56 on 15/7/16.
 */
public interface RenderingString {
    void renderString(String data);
}
