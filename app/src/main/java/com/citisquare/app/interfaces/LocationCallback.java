package com.citisquare.app.interfaces;

import android.location.Location;

/**
 * Created by hlink56 on 31/8/16.
 */
public interface LocationCallback {
    void onLocationError();
    void onLocationRecive(Location location);
}
