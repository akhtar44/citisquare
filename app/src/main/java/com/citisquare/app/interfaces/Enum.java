package com.citisquare.app.interfaces;

/**
 * Created by hlink56 on 1/6/16.
 */
public interface Enum {

    enum setNevigationIcon {
        MENU, MENU_CITY, MENU_SETTING, BACK_SAVE, BACK_COMPOSE, BACK_CITY, BACK_SEND, BACK, BACK_PROFILE, NOTHING, MENU_COMPOSE, BACK_BRANCH, ALLHIDE, BACK_REVIEW;
    }

    enum setSearch {
        SEARCH, SEARCH_CATEGORY, SEARCH_FILTER, NOTHING, WHOLE_SEARCH,ONLYSEARCHHIDE;
    }

    enum setUser {
        VISITOR, SELLER_BASIC, SELLER_PREMIUM, GUEST
    }
}
