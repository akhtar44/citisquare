package com.citisquare.app.interfaces;

/**
 * Created by hlink56 on 16/3/17.
 */

public interface SearchAutoCompleteListener {
    public void moveToCategoryList();

    public void moveToProfile();
}
