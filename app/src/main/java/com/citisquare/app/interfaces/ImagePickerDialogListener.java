package com.citisquare.app.interfaces;

import android.content.Intent;

/**
 * Created by hlink56 on 19/9/16.
 */
public interface ImagePickerDialogListener {
    public void getResultFromCamera(String result, String from);

    public void getResultFromGallery(Intent result, String from);

    public void nothingSet();
}
