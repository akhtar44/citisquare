package com.citisquare.app.interfaces;

import android.support.annotation.Nullable;

/**
 * Created by hlink56 on 20/2/16.
 */
public interface ItemEventListener<T> {
    void onItemEventFired(T t, @Nullable T t2, String actualPos);
}
