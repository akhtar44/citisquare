package com.citisquare.app.interfaces;

import com.citisquare.app.pojo.response.SubCategory;

import java.util.List;

/**
 * Created by hlink56 on 5/1/17.
 */
public interface SubCategoryListener {
    void onSubCategoryListener(List<SubCategory> subCategories);
}
