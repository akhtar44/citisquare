package com.citisquare.app.interfaces;

/**
 * Created by hlink56 on 1/6/16.
 */
public interface Navigation {
    String EMAIL_REGEX = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    //String WEBSITE_REGEX="^(https|http)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
    String WEBSITE_REGEX="^[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
    void jobApplyDialog();

    void visitorFragment();

    void sellerFragment();

    void profileFragment();

    void sendQuiries();

    void childCategoryFragment();

    void forgotDialogFragment();

    void changePasswordDialog();

    void commentFragment();

    void notificationFragment();

    void postAJob();

    void postOnTimeLine();

    void postAds();

    void postTodaysOffer();

    void onFragmentChange(String TAG);
}