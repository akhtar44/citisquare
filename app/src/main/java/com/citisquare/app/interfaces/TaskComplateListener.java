package com.citisquare.app.interfaces;

/**
 * Created by hlink16 on 24/10/15.
 */
public interface TaskComplateListener<T> {
    public void onSuccess(T mObject);
    public void onFailure();
}
