package com.citisquare.app.interfaces;

/**
 * Created by hlink16 on 10/7/15.
 */
public interface ImageChooserListener {

    public void getResultFromCamera(String result);
    public void getResultFromGallery(String result);
    public void nothingSet();
}
