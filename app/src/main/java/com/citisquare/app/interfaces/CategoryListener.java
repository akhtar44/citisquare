package com.citisquare.app.interfaces;

import com.citisquare.app.pojo.response.CategoryData;

import java.util.List;

/**
 * Created by hlink56 on 5/1/17.
 */

public interface CategoryListener {
    void onCategorySelected(List<CategoryData> categoryDatas);
}
