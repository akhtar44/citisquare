package com.citisquare.app.interfaces;

import java.util.Map;

/**
 * Created by hlink43 on 12/9/16.
 */
public interface PaymentCallBack {
    void onSucess(Map<String, String> map, Map<String, String> map1);
    void onFailureTransaction(Map<String, String> map, Map<String, String> map1);
    void onCancel(Map<String, String> map, Map<String, String> map1);
}
