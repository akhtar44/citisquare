package com.citisquare.app.interfaces;

import android.Manifest;

/**
 * Created by hlink56 on 5/7/16.
 */

public interface Constants {
    public static final String englishOnly = "\\p{InArabic}";
    public static final String passwordRegex = "^\\S*(?=\\S{3,})(?=\\S*[a-z])(?=\\S*[A-Z])(?=\\S*[\\d])\\S*$";
    /*DATE CONSTANS*/
    String YAHOO_DATE = "EEE, dd MMM yyyy HH:mm a Z";
    String WS_DATE = "yyyy-MM-dd HH:mm:ss";
    String DATE_DISPLAY = "dd MMM yyyy, hh:mm a";
    String DATE_RATE = "dd MMM yyyy";
    /*CRASH LOG LINK*/
    String ERRORLINK = "http://hyperlinkserver.com/wconnect/ws/crashcall.php?";
    String GCM_KEY = "AIzaSyDw8XVkeB3C0prFuPSKXYwdjkLQEJiDmCE";
    /*API CALLING*/
    String HEADER_AUTHORIZATION = "API-KEY";
    String X_API_KEY = "Q17U21I9C3K11I9N14F6O15";
    String User_Session = "User-Session";
    String User_Id = "User-Id";
    //String URL = "http://www.citisquare.com/api/v1/"; //LIVE
    String URL = "http://produ.rpfz3fp4d5.us-east-1.elasticbeanstalk.com/api/v1/"; // AWS NEW SERVER
    // production version code 4,5,6,7,8,9,10,11,12,13,14
    //String URL = "http://e3ed4f20.ngrok.io/citisquare/api/v1/"; // LOCAL
    String ABDOUT_US = URL + "about_us.html";
    String SELLER = "seller/";
    String SELLER_EDIT = URL + SELLER + "edit";
    String SELLER_DETAILS = URL + SELLER + "user_details";
    String SELLER_SIGNUP = URL + SELLER + "signup/";
    String BRANCH_LIST = URL + SELLER + "branch_listing/";
    String BRANCH_LIST_MAP = URL + SELLER + "branch_listing_map";
    String VISITOR = "visitor/";
    String VISITOR_SIGNUP = URL + VISITOR + "signup/";
    String VISITOR_EDIT = URL + VISITOR + "edit";
    String CHAT = "chat/";
    String CHAT_LIST = URL + CHAT + "chat_list";
    String MESSAGE_LIST = URL + CHAT + "messages_list";
    String MEMBER_LIST = URL + CHAT + "member_list";
    String SERVICE = "service/";
    String REGISTER = "register/";
    String SLIDER = "slider/";
    String POST_SIGN_UP = URL +REGISTER+ "signup";
    String POST_LOGIN = URL +REGISTER+ "login";
    String LOGIN = URL + SERVICE + "login/";
    String LOGIN_FIREBASE = URL + REGISTER + "otp_login";
    String LOGOUT = URL + SERVICE + "logout";
    String CATEGORY = URL + SERVICE + "category/";
    String SUB_CATEGORY = URL + SERVICE + "sub_category/";
    String COUNTRY = URL + SERVICE + "country/";
    String SEND_MESSAGE = URL + CHAT + "send_message";
    String DO_FOLLOW = URL + SERVICE + "do_follow";
    String LIKE = URL + SERVICE + "like";
    String COMMENT_LISTING = URL + SERVICE + "comment_listing/";
    String COMMENT = URL + SERVICE + "comment";
    String FOLLOWING_LIST = URL + SERVICE + "following";
    String FORGOT_PASSWORD = URL + SERVICE + "forgot_password";
    String NOTIFICATION_LIST = URL + SERVICE + "notification_list";
    String FOLLOWERS_LIST = URL + SERVICE + "followers";
    String MAP_SELLER_DETAIL = URL + SERVICE + "map_seller_detail";
    String OFFER_DETAILS = URL + SERVICE + "timeline_offer_detail";
    /*http://hyperlinkinfosystem.in/quickinfo/api/v1/seller/add_branch*/
    String ADD_BRANCH = URL + SELLER + "add_branch";
    String EDIT_BRANCH = URL + SELLER + "edit_branch";
    String DELETE_BRANCH = URL + SELLER + "delete_branch";
    String CITY_INFO = URL + SERVICE + "city_info";
    String ADD_SELLER_CATEGORY = URL + SELLER + "seller_category";
    String JOB = "job/";
    String POST_ADD = URL + JOB + "add";
    String JOB_LISTING = URL + JOB + "job_listing";
    String JOB_FROM_MENU_LISTING = URL + JOB + "job_listing_new";
    String APPLY_JOB = URL + JOB + "apply_job";
    String SUBSCRIPTION_PLAN = URL + SERVICE + "plan_list";
    String POST_SLIDER = URL + SLIDER + "add_category_slider";
    String SEARCH_SELLER_DETAIL = URL + SERVICE + "search_seller_detail";
    String GALLERY = "gallery/";
    String GALLERY_LISTING = URL + GALLERY + "gallery_listing";
    String GALLERY_PHOTO_ADD = URL + GALLERY + "add";
    String TIMELINE = "timeline/";
    String TIMELINE_ADD = URL + TIMELINE + "add";
    String TIMELINE_LIST = URL + TIMELINE + "timeline_list";
    String OFFER = "offer/";
    String POST_OFFER = URL + OFFER + "add";
    String OFFER_LISTING = URL + OFFER + "offer_listing";
    String SEND_QUERY = URL + SERVICE + "send_query";
    int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    String[] ACCESS_FINE_LOCATION = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
    int LOCATIONREQUESTCODE = 45;
    String GETSDK_TOKEN = URL + SELLER + "getsdktoken";
    String SELLER_PLAN_PAYMENT = URL + SELLER + "plan_payment";
    String CHECK_STATUS = URL + SELLER + "check_payfortstatus";
    String ADVERTISEMENT = URL + SERVICE + "advertisement";
    String SELLER_STATUS = URL + SELLER + "seller_status";
    String CHANGE_PASSWORD = URL + SERVICE + "change_password";
    String NEWTIMELINE_LIST = URL + TIMELINE + "new_timeline_list";
    String NOTIFICATIO_ON_OFF = URL + SERVICE + "notification_onoff";
    String CMS_API_WEB_VIEW = URL + SERVICE + "cms_page";
    String CHECK_FOR_UPDATE = URL + SERVICE + "check_version";

    String BADGE_COUNTER = URL + SERVICE + "notification_chat_count";
    String SLIDER_IMAGE = URL + SERVICE + "slider_image";
    String CATEGORY_SLIDER_IMAGE = URL + SLIDER + "category_slider_image";
    String OFFER_SLIDER_IMAGE = URL + SLIDER + "offer_slider_image";
    String CHECK_BRANCH = URL + SERVICE + "check_branch";
    String BRANCH_PLAN_LIST = URL + SERVICE + "branch_plan";
    String UPDATE_BRANCH = URL + SELLER + "update_branch";
    String RATE_LISTING = URL + SERVICE + "rating_listing";
    String RATE_REVIEW = URL + SERVICE + "give_rating";
    String DELETE_REVIEW = URL + SERVICE + "delete_review";
    String UPLOAD_CHAT_IMAGE = URL + CHAT + "upload_chat_image";
    String ALL_SEARCH_DETAIL = URL + SERVICE + "all_search_detail";
    String DELETE_ACCOUNT = URL + SELLER + "delete_user";
    String DEACTIVATE_ACCOUNT = URL + SELLER + "deactive_user";
    String SUBSCRIPTION_DETAILS = URL + SELLER + "user_package_details";

    public enum forLanguage {
        ENGLISH {
            @Override
            public String toString() {
                return "english";
            }
        },
        ARABIC {
            @Override
            public String toString() {
                return "arabic";
            }
        }
    }
}

