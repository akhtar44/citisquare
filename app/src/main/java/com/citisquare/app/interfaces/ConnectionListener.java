package com.citisquare.app.interfaces;

/**
 * Created by hlink16 on 16/6/15.
 */
public interface ConnectionListener {
    public void onConnectionON();
    public void onConnectionOFF();
}
