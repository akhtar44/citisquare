package com.citisquare.app.interfaces;

/**
 * Created by hlink54 on 17/6/16.
 */
public interface Result<T> {
    void onSuccess(T t);
}
