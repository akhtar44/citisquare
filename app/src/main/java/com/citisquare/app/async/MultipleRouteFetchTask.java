package com.citisquare.app.async;

import android.os.AsyncTask;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.citisquare.app.utils.Debugger;
import com.citisquare.app.utils.HttpConnection;

import java.util.List;

/**
 * Created by hlink54 on 23/2/16.
 */
public class MultipleRouteFetchTask extends AsyncTask<Void, Void, String> {
    private LatLng current;
    private LatLng destination;
    private List<LatLng> waypoints;
    private GoogleMap mMap;
    private RouteParserTask.ParseCallback parseCallback;
    private int polyLineColor;

    public MultipleRouteFetchTask(GoogleMap mMap, RouteParserTask.ParseCallback parseCallback) {
        this.parseCallback = parseCallback;
        this.mMap = mMap;
    }

    @Override
    protected String doInBackground(Void... v) {
        String data = "";
        try {
            String url = getMapsApiDirectionsUrl(current, destination);
            HttpConnection http = new HttpConnection();
            data = http.readUrl(url);
        } catch (Exception e) {
            Debugger.e("Read Task", e.toString());
        }
        return data;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        RouteParserTask routeParserTask = new RouteParserTask(mMap, polyLineColor, parseCallback);
        routeParserTask.setCurrent(current);
        routeParserTask.setDestination(destination);
        routeParserTask.execute(result);
    }

    public void setWaypoints(List<LatLng> waypoints) {
        this.waypoints = waypoints;
    }

    public void setCurrent(LatLng current) {
        this.current = current;
    }

    public void setDestination(LatLng destination) {
        this.destination = destination;
    }

    private String getMapsApiDirectionsUrl(LatLng current, LatLng destination) {
        StringBuilder route = new StringBuilder();
        String url = "https://maps.googleapis.com/maps/api/directions/json";
        route.append(url);
        route.append("?");
        String origin = "origin="
                + current.latitude + "," + current.longitude
                + "&"
                + "destination=" + destination.latitude + ","
                + destination.longitude;
        route.append(origin);
        route.append("&");
        String mode = "mode=driving";
        route.append(mode);
        route.append("&");
        route.append("units=imperial");

        //For multiple routes only, remove when necessary
        //route.append("&alternatives=true");
        //route.append("&avoid=highways");
        if (waypoints != null && waypoints.size() > 0) {

            route.append("&waypoints=");

            route.append("optimize:true|");

            int total_size = waypoints.size();
            for (int i = 0; i < total_size; i++) {
                route.append(waypoints.get(i).latitude);
                route.append(",");
                route.append(waypoints.get(i).longitude);
                if (i != total_size - 1)
                    route.append("|");
            }
        }
        url = route.toString();
        Debugger.e("generated url is = " + url);
        return url;
    }

    public void setPolyLineColor(int polyLineColor) {
        this.polyLineColor = polyLineColor;
    }
}