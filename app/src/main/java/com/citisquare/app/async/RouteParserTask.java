package com.citisquare.app.async;

import android.os.AsyncTask;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Created by hlink54 on 23/2/16.
 */
public class RouteParserTask extends
        AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
    private GoogleMap mMap;
    private Polyline polyline;
    private ParseCallback parseCallback;
    private LatLng current, destination;
    private int polyLineColor;

    public RouteParserTask(GoogleMap mMap, int polyLineColor , ParseCallback parseCallback) {
        this.mMap = mMap;
        this.parseCallback = parseCallback;
        this.polyLineColor = polyLineColor;
    }

    @Override
    protected List<List<HashMap<String, String>>> doInBackground(
            String... jsonData) {

        List<List<HashMap<String, String>>> routes = null;
        try {
            PathParser parser = new PathParser();
            //  Debug.e("path response(json) : " + jsonData[0]);
            routes = parser.parse(jsonData[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return routes;
    }

    @Override
    protected void onPostExecute(List<List<HashMap<String, String>>> routes) {
        if (routes != null && routes.size() > 0) {
            // traversing through routes
            for (int i = 0; i < routes.size(); i++) {
                ArrayList<LatLng> points = null;
                PolylineOptions polyLineOptions = null;
                points = new ArrayList<>();
                polyLineOptions = new PolylineOptions();

                List<HashMap<String, String>> path = routes.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);
                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);
                    points.add(position);
                }

                if (current != null)
                    polyLineOptions.add(current);
                polyLineOptions.addAll(points);
                if (destination != null)
                    polyLineOptions.add(destination);
                polyLineOptions.width(8);
                polyLineOptions.zIndex(5);
                polyLineOptions.clickable(true);

                polyLineOptions.color(polyLineColor);
                if (mMap != null) {
                    polyline = mMap.addPolyline(polyLineOptions);
                    if (parseCallback != null) {
                        parseCallback.onRouteComplete(polyline);
                    }
                }
                polyLineOptions = null;
            }
        }
    }

    public interface ParseCallback {
        void onRouteComplete(Polyline polyline);
    }

    public void setCurrent(LatLng current) {
        this.current = current;
    }

    public void setDestination(LatLng destination) {
        this.destination = destination;
    }
}