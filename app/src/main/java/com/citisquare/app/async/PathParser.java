package com.citisquare.app.async;

/**
 * Created by hlink54 on 23/2/16.
 */

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.citisquare.app.pojo.route.Route;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class PathParser {
    private static final String TAG = "PathParser";

    public List<List<HashMap<String, String>>> parse(String jObject) {
        List<List<HashMap<String, String>>> routes = new ArrayList<>();
        List<HashMap<String, String>> path = new ArrayList<>();
        try {
            Gson gson = new Gson();
            Route route = gson.fromJson(jObject, Route.class);

            //To show multiple routes, traverse through route list and add in route hashmap
            // for (int i = 0; i < route.getRoutes().size(); i++) {
            String polyline = route.getRoutes().get(0).getOverviewPolyline().getPoints();
            List<LatLng> list = decodePoly(polyline);
//            Log.e(TAG, "route points : " + polyline);

            /** Traversing all points */
            for (int l = 0; l < list.size(); l++) {
                HashMap<String, String> hm = new HashMap<>();
                hm.put("lat", Double.toString((list.get(l)).latitude));
                hm.put("lng", Double.toString((list.get(l)).longitude));
                path.add(hm);
            }
            routes.add(path);

            //    }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return routes;
    }

    private List<LatLng> decodePoly(String poly) {
        int len = poly.length();
        int index = 0;
        List<LatLng> decoded = new LinkedList<>();
        int lat = 0;
        int lng = 0;

        while (index < len) {
            int b;
            int shift = 0;
            int result = 0;
            do {
                b = poly.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = poly.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            decoded.add(new LatLng((lat / 1E5), (lng / 1E5)));
        }

        return decoded;
    }
}
