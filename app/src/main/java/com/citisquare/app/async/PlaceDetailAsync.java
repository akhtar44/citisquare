package com.citisquare.app.async;

import android.os.AsyncTask;

import com.google.gson.Gson;
import com.citisquare.app.pojo.place.PlaceDetailWrapper;
import com.citisquare.app.utils.Debugger;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by hlink54 on 3/10/16.
 */
public class PlaceDetailAsync extends AsyncTask<String, Void, String> {
    StringBuilder jsonResults = new StringBuilder();
    private HttpURLConnection conn = null;
    private PlaceDetailListener placeDetailListener;

    public PlaceDetailAsync(PlaceDetailListener placeDetailListener) {
        this.placeDetailListener = placeDetailListener;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            StringBuilder sb = new StringBuilder(params[0]);

            Debugger.e(":::::PLACE DETAIL URL:::::" + sb.toString());

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Debugger.e(":::::MALFORMED URL EXCEPTION:::::");
            // return resultList;
        } catch (IOException e) {
            Debugger.e(":::::IO EXCEPTION:::::");
            // return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
        return jsonResults.toString();
    }

    @Override
    protected void onPostExecute(String s) {
        PlaceDetailWrapper placeDetailWrapper = new Gson().fromJson(s, PlaceDetailWrapper.class);
        if (placeDetailWrapper != null) {

            if (placeDetailListener != null)
                placeDetailListener.pickupAddress(placeDetailWrapper);

           /* Result result = placeDetailWrapper.getResult();
            if (result != null) {
                Geometry geometry = result.getGeometry();
                if (geometry != null) {
                    Location location = geometry.getLocationAndPayment();
                    if (location != null) {
                        if (placeDetailListener != null)
                            placeDetailListener.pickupAddress(new LatLng(location.getLat(), location.getLng()));
                    }
                }
            }*/
        }
    }

    public interface PlaceDetailListener {
        void pickupAddress(PlaceDetailWrapper location);
    }
}
