package com.citisquare.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.pojo.Filter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 2/6/16.
 */
public class FilterDropDownAdapter extends ArrayAdapter<Filter> {

    List<Filter> radiusList;
    Context context;
    int resourse;


    public FilterDropDownAdapter(Context context, int resource, List<Filter> radiusList) {
        super(context, resource, radiusList);
        this.context = context;
        this.radiusList = radiusList;
        this.resourse = resource;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.raw_filter_dropdown, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (position == ((MainActivity) context).getFilterPosition()) {
            viewHolder.filterText.setTextColor(getContext().getResources().getColor(R.color.colorPrimary));
        } else {
            viewHolder.filterText.setTextColor(getContext().getResources().getColor(R.color.dark_blue));
        }

        viewHolder.filterText.setText(radiusList.get(position).getName());
        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.filterText)
        CustomTextView filterText;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
