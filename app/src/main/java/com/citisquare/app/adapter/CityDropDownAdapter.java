package com.citisquare.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.citisquare.app.R;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.pojo.response.City;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 7/6/16.
 */
public class CityDropDownAdapter extends ArrayAdapter<City> {

    List<City> radiusList;
    Context context;
    int resourse;


    public CityDropDownAdapter(Context context, int resource, List<City> radiusList) {
        super(context, resource, radiusList);
        this.context = context;
        this.radiusList = radiusList;
        this.resourse = resource;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.raw_category_dropdown, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (Locale.getDefault().getDisplayLanguage().equals("العربية"))
            viewHolder.categoryText.setText(radiusList.get(position).getArName());
        else
            viewHolder.categoryText.setText(radiusList.get(position).getName());

        return convertView;
    }


    static class ViewHolder {
        @BindView(R.id.categoryText)
        CustomTextView categoryText;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
