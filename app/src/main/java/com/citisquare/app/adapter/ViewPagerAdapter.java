package com.citisquare.app.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.citisquare.app.fragments.ParentCategoryFragment;
import com.citisquare.app.fragments.ParentCityInfoFragment;
import com.citisquare.app.fragments.ParentHomeFragment;
import com.citisquare.app.fragments.ParentMapFragment;
import com.citisquare.app.fragments.ParentToDaysPostFragment;


/**
 * Created by shahabuddin on 6/6/14.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private final Resources resources;

    Fragment result = null;
    SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

    ParentCityInfoFragment parentCityInfoFragment;
    ParentCategoryFragment parentCategoryFragment;
    ParentToDaysPostFragment parentQuickInfoFragment;
    ParentHomeFragment parentHomeFragment;
    Context context;

    /**
     * Create pager adapter
     *
     * @param resources
     * @param fm
     */
    public ViewPagerAdapter(final Resources resources, FragmentManager fm, Context context) {
        super(fm);
        this.resources = resources;

        this.context = context;
        parentCityInfoFragment = new ParentCityInfoFragment();
        parentCategoryFragment = new ParentCategoryFragment();
        parentQuickInfoFragment = new ParentToDaysPostFragment();
        parentHomeFragment = new ParentHomeFragment();
    }

    @Override
    public Fragment getItem(int position) {
        result = null;
        switch (position) {
            case 0:
                // First Fragment of Fifth Tab
                //result = new ParentJobFragment();
                if (parentHomeFragment != null)
                    result =parentHomeFragment;
                else
                    result = new ParentHomeFragment();
                break;

            case 1:
                // First Fragment of Second Tab
                if (parentCategoryFragment != null)
                    result = parentCategoryFragment;

                else
                    result = new ParentCategoryFragment();
                break;
            case 2:
                // First Fragment of Third Tab
                if (parentQuickInfoFragment != null)
                    result = parentQuickInfoFragment;
                else
                    result = new ParentToDaysPostFragment();
                break;
            case 3:
                // First Fragment of Fourth Tab
                ParentMapFragment parentMapFragment = new ParentMapFragment();
                Bundle bundleMap = new Bundle();
                bundleMap.putString("MAP", "MAIN_MAP");
                parentMapFragment.setArguments(bundleMap);
                result = parentMapFragment;
                break;
            case 4:
                // First Fragment of First Tab
                if (parentCityInfoFragment != null)
                    result = parentCityInfoFragment;
                else
                    result = new ParentCityInfoFragment();
                break;

        }

        return result;
    }


    @Override
    public int getCount() {
        return 5;
    }


    /**
     * On each Fragment instantiation we are saving the reference of that Fragment in a Map
     * It will help us to retrieve the Fragment by position
     *
     * @param container
     * @param position
     * @return
     */
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    /**
     * Remove the saved reference from our Map on the Fragment destroy
     *
     * @param container
     * @param position
     * @param object
     */
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }


    /**
     * Get the Fragment by position
     *
     * @param position tab position of the fragment
     * @return
     */
    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }
}
