package com.citisquare.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.pojo.Message;
import com.citisquare.app.utils.Debugger;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by hlink56 on 1/6/16.
 */
public class SendMessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private Context context;

    private static final int HEADER = 1;
    private static final int NORMAL = 2;
    private static final int FOOTER = 3;
    List<Message> messageList;


    public SendMessageAdapter(Context context, List<Message> messageList) {

        this.context = context;
        this.messageList = messageList;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        RecyclerView.ViewHolder viewHolder;
        if (viewType == HEADER) {
            view = View.inflate(context, R.layout.send_message_header, null);
            viewHolder = new HeaderSendMessage(view);
            return viewHolder;
        } else {
            view = View.inflate(context, R.layout.raw_sendmessage, null);
            viewHolder = new SendMsgDetilasViewHolder(view);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        try {
            if (holder instanceof SendMsgDetilasViewHolder) {
                ((SendMsgDetilasViewHolder) holder).bindView(position - 1);
            } else if (holder instanceof HeaderSendMessage) {
                HeaderSendMessage vh = (HeaderSendMessage) holder;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Debugger.e(e.getMessage());
        }
    }

    public boolean isPositionFooter(int position) {
        return position == messageList.size();
    }

    @Override
    public int getItemCount() {
        if (messageList == null)
            return 0;
        if (messageList.size() == 0)
            return 0;

        return messageList.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            // This is where we'll add HEADER.
            return HEADER;
        }
        return NORMAL;

    }


    class HeaderSendMessage extends RecyclerView.ViewHolder {
        @BindView(R.id.sendMessageEditText)
        EditText sendMessageEditText;
        @BindView(R.id.layout_send_message_header_button_submit)
        Button layoutSendMessageHeaderButtonSubmit;

        public HeaderSendMessage(View view) {
            super(view);
            ButterKnife.bind(this, view);
            sendMessageEditText.setMovementMethod(new ScrollingMovementMethod());

            sendMessageEditText.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (v.getId() == R.id.sendMessageEditText) {
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        switch (event.getAction() & MotionEvent.ACTION_MASK) {
                            case MotionEvent.ACTION_UP:
                                v.getParent().requestDisallowInterceptTouchEvent(false);
                                break;
                        }
                    }
                    return false;
                }
            });
        }

        @OnClick(R.id.layout_send_message_header_button_submit)
        public void sendMessageButtonClicked() {
            if (isValid()) {
                sendMessageEditText.setText("");
            }
        }

        public boolean isValid() {
            if (sendMessageEditText.getText().toString().trim().length() == 0) {
                ((MainActivity) context).messageAlert(context.getString(R.string.message_pleaseEnterMessage));
                sendMessageEditText.requestFocus();
                return false;
            }
            return true;
        }
    }


    class SendMsgDetilasViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.sendMessageUserImage)
        ImageView sendMessageUserImage;
        @BindView(R.id.sendMessageUsername)
        TextView sendMessageUsername;
        @BindView(R.id.sendImageTimeAgo)
        TextView sendImageTimeAgo;
        @BindView(R.id.sendMessageData)
        TextView sendMessageData;

        public SendMsgDetilasViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bindView(int position) {
            //((MainActivity) context).setCircularImage(sendMessageUserImage, "");
        }
    }
}