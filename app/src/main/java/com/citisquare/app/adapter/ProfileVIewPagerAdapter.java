package com.citisquare.app.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.citisquare.app.fragments.FollowersFragment;
import com.citisquare.app.fragments.FollowingFragment;
import com.citisquare.app.fragments.GalleryFragment;
import com.citisquare.app.fragments.TimeLineProfileFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hlink56 on 1/6/16.
 */
public class ProfileVIewPagerAdapter extends FragmentStatePagerAdapter {

    private final Resources resources;

    SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();
    TimeLineProfileFragment timeLineProfileFragment;
    GalleryFragment galleryFragment;
    FollowersFragment followersFragment;
    FollowingFragment followingFragment;
    Fragment result = null;
    Context context;
    private List<Fragment> fragments;
    String profile;

    /**
     * Create pager adapter
     *
     * @param resources
     * @param fm
     */
    public ProfileVIewPagerAdapter(final Resources resources, FragmentManager fm, String profile, Context context) {
        super(fm);
        this.resources = resources;
        this.profile = profile;
        this.context = context;
        this.fragments = new ArrayList<Fragment>();
        fragments.add(new TimeLineProfileFragment());
        fragments.add(new GalleryFragment());
        fragments.add(new FollowersFragment());
        fragments.add(new FollowingFragment());
    }

    public int getCount() {
        return fragments.size();
    }

    @Override
    public Fragment getItem(int position) {
        result = null;
        switch (position) {
            case 0:
                // First Fragment of First Tab
               /* TimeLineProfileFragment newsProfileFragment = new TimeLineProfileFragment();
                Bundle bundleNews = new Bundle();
                bundleNews.putString("FROM", "N");
                newsProfileFragment.setArguments(bundleNews);
                result = newsProfileFragment;*/
                TimeLineProfileFragment timeLineProfileFragment = new TimeLineProfileFragment();
                if (timeLineProfileFragment != null) {
                    Bundle bundleNews = new Bundle();
                    bundleNews.putString("FROM", "N");
                    timeLineProfileFragment.setArguments(bundleNews);
                    result = timeLineProfileFragment;
                } else
                    result = new TimeLineProfileFragment();

                break;

            case 1:
                // First Fragment of Second Tab
                GalleryFragment galleryFragment = new GalleryFragment();
                Bundle bundle = new Bundle();
                bundle.putString("Profile", profile);
                galleryFragment.setArguments(bundle);
                result = galleryFragment;
                break;
            case 2:
                // First Fragment of Third Tab
                FollowersFragment followersFragment = new FollowersFragment();
                Bundle bundleFollowerFragment = new Bundle();
                bundleFollowerFragment.putString("Profile", profile);
                followersFragment.setArguments(bundleFollowerFragment);
                result = followersFragment;
                break;
            case 3:
                // First Fragment of Third Tab
                FollowingFragment followingFragment = new FollowingFragment();
                Bundle bundleFollowingFragment = new Bundle();
                bundleFollowingFragment.putString("Profile", profile);
                followingFragment.setArguments(bundleFollowingFragment);
                result = followingFragment;
                break;
           /* case 4:
                result = new ProfileFragment();*/
            default:
                result = null;
                break;
        }

        return result;
    }


    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }


}



