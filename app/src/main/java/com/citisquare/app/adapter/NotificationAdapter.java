package com.citisquare.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.pojo.response.NotificationData;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 4/6/16.
 */
public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder> {

    View view;
    Context context;
    List<NotificationData> notificationApps;
    ItemEventListener<String> itemEventListener;


    public NotificationAdapter(Context context, List<NotificationData> notificationApps, ItemEventListener<String> itemEventListener) {
        this.context = context;
        this.notificationApps = notificationApps;
        this.itemEventListener = itemEventListener;
    }

    @Override
    public NotificationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = View.inflate(context, R.layout.raw_notification, null);

        // ViewHolderSearch holderFriendList = new ViewHolderSearch(view);
        // view.setTag(holderFriendList);
        //return holderFriendList;
        return new NotificationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NotificationViewHolder holder, int position) {
        ((MainActivity) context).setCircularImage(holder.notificationImage, notificationApps.get(position).getProfileImageThumb());
        holder.notificationUserName.setText(notificationApps.get(position).getMessage());
        DateFormat utcFormat = new SimpleDateFormat(Constants.WS_DATE);
        utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date date = null;
        try {
            date = utcFormat.parse(notificationApps.get(position).getInsertdate());
        } catch (Exception e) {
            e.printStackTrace();
        }

        DateFormat pstFormat = new SimpleDateFormat(Constants.DATE_DISPLAY);
        pstFormat.setTimeZone(TimeZone.getDefault());
        holder.dateNotification.setText(pstFormat.format(date));
    }


    @Override
    public int getItemCount() {
        return notificationApps.size();
    }


    class NotificationViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.dateNotification)
        CustomTextView dateNotification;
        @BindView(R.id.notificationImage)
        ImageView notificationImage;
        @BindView(R.id.notificationUserName)
        CustomTextView notificationUserName;

        public NotificationViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemEventListener.onItemEventFired(new Gson().toJson(notificationApps.get(getAdapterPosition())), "", "");
                }
            });
        }
    }
}