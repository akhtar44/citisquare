package com.citisquare.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.pojo.response.CommentData;
import com.citisquare.app.utils.Debugger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 1/6/16.
 */
public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.CommentViewHolder> {

    View view;
    Context context;
    List<CommentData> commentDatas;

    public CommentAdapter(Context context, List<CommentData> commentDatas) {
        this.context = context;
        this.commentDatas = commentDatas;
    }

    public static String getTimeAgo(Date date, Context ctx) {

        DateFormat pstFormat = new SimpleDateFormat(Constants.DATE_DISPLAY);
        pstFormat.setTimeZone(TimeZone.getDefault());

        if (date == null) {
            return null;
        }

        long time = date.getTime();

        Date curDate = currentDate();
        long now = curDate.getTime();
        if (time > now || time <= 0) {
            return null;
        }

        int dim = getTimeDistanceInMinutes(time);

        String timeAgo = null;

        if (dim == 0) {
            timeAgo = ctx.getResources().getString(R.string.justNow);
            //timeAgo = ctx.getResources().getString(R.string.date_util_term_less) + " " + ctx.getResources().getString(R.string.date_util_term_a) + " " + ctx.getResources().getString(R.string.date_util_unit_minute);
        } else if (dim == 1) {
            return "1 " + ctx.getResources().getString(R.string.date_util_unit_minute);
        } else if (dim >= 2 && dim <= 44) {
            timeAgo = dim + " " + ctx.getResources().getString(R.string.date_util_unit_minutes);
        } else if (dim >= 45 && dim <= 89) {
            timeAgo = ctx.getResources().getString(R.string.date_util_unit_hour);
        } else if (dim >= 90 && dim <= 1439) {
            timeAgo = (Math.round(dim / 60)) + " " + ctx.getResources().getString(R.string.date_util_unit_hours);
        } else if (dim >= 1440 && dim <= 2519) {
            timeAgo = "1 " + ctx.getResources().getString(R.string.date_util_unit_day);
        } else if (dim >= 2520 && dim <= 5040) {//43199
            timeAgo = (Math.round(dim / 1440)) + " " + ctx.getResources().getString(R.string.date_util_unit_days);
        } else {
            return pstFormat.format(date);
        }/*else if (dim >= 43200 && dim <= 86399) {
            timeAgo = ctx.getResources().getString(R.string.date_util_term_a) + " " + ctx.getResources().getString(R.string.date_util_unit_month);
        } else if (dim >= 86400 && dim <= 525599) {
            timeAgo = (Math.round(dim / 43200)) + " " + ctx.getResources().getString(R.string.date_util_unit_months);
        } else if (dim >= 525600 && dim <= 655199) {
            timeAgo = ctx.getResources().getString(R.string.date_util_term_a) + " " + ctx.getResources().getString(R.string.date_util_unit_year);
        } else if (dim >= 655200 && dim <= 914399) {
            timeAgo = ctx.getResources().getString(R.string.date_util_term_a) + " " + ctx.getResources().getString(R.string.date_util_unit_year);
        } else if (dim >= 914400 && dim <= 1051199) {
            timeAgo = " 2 " + ctx.getResources().getString(R.string.date_util_unit_years);
        } else {
            timeAgo = (Math.round(dim / 525600)) + " " + ctx.getResources().getString(R.string.date_util_unit_years);
        }*/


        Debugger.e("::::AGO ::::" + timeAgo);
        return timeAgo;
    }

    public static Date currentDate() {
        Calendar calendar = Calendar.getInstance();
        return calendar.getTime();
    }

    private static int getTimeDistanceInMinutes(long time) {
        long timeDistance = currentDate().getTime() - time;
        return Math.round((Math.abs(timeDistance) / 1000) / 60);
    }

    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = View.inflate(context, R.layout.raw_comment, null);

        // ViewHolderSearch holderFriendList = new ViewHolderSearch(view);
        // view.setTag(holderFriendList);
        //return holderFriendList;
        return new CommentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CommentViewHolder holder, int position) {
        //String name = commentDatas.get(position).getFirstName() + commentDatas.get(position).getLastName();
        String name = commentDatas.get(position).getUsername();
        holder.commentUsername.setText(name);
        holder.commentData.setText(commentDatas.get(position).getComment());
        ((MainActivity) context).setCircularImage(holder.commentUserImage, commentDatas.get(position).getProfileImageThumb());

        String dateStr = commentDatas.get(position).getInsertdate();
        if (dateStr.equals("0")) {
            holder.commentTime.setText(R.string.justNow);
        } else {
            DateFormat utcFormat = new SimpleDateFormat(Constants.WS_DATE);
            utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date date = null;
            try {
                date = utcFormat.parse(commentDatas.get(position).getInsertdate());
            } catch (Exception e) {
                e.printStackTrace();
            }

            DateFormat pstFormat = new SimpleDateFormat(Constants.DATE_DISPLAY);
            pstFormat.setTimeZone(TimeZone.getDefault());

            holder.commentTime.setText(getTimeAgo(date, context));
        }
    }

    @Override
    public int getItemCount() {
        return commentDatas.size();
    }

    class CommentViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.commentUserImage)
        ImageView commentUserImage;
        @BindView(R.id.commentUsername)
        TextView commentUsername;
        @BindView(R.id.commentTime)
        TextView commentTime;
        @BindView(R.id.commentData)
        TextView commentData;

        public CommentViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}