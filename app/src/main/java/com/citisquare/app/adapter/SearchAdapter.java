package com.citisquare.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.citisquare.app.R;
import com.citisquare.app.pojo.response.SearchData;

import java.util.List;

import static com.citisquare.app.R.id.searchListingtextViewSymbols;

/**
 * Created by hlink56 on 15/3/17.
 */

public class SearchAdapter extends ArrayAdapter<SearchData> {

    Context context;
    private int resource;
    private List<SearchData> searchDatas;



    public SearchAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<SearchData> objects) {
        super(context, resource, objects);

        this.context = context;
        this.resource = resource;
        this.searchDatas = objects;
    }

    public int getCount() {
        return searchDatas.size();
    }

    public SearchData getItem(int position) {
        return searchDatas.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            if (convertView == null) {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                convertView = inflater.inflate(resource, parent, false);
            }
            SearchData searchData = getItem(position);
            TextView name = (TextView) convertView.findViewById(R.id.searchListingTextViewName);
            TextView sign = (TextView) convertView.findViewById(searchListingtextViewSymbols);
            if (searchData.getIsFlag().equalsIgnoreCase("U"))
                sign.setText(">");
            else
                sign.setText("+");

            name.setText(searchData.getName());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }


}
