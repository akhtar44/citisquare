package com.citisquare.app.adapter;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.pojo.response.CityInfoData;
import com.citisquare.app.utils.ColorFilterTransformation;
import com.citisquare.app.utils.Debugger;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 31/5/16.
 */
public class ParentCityInfoAdapter extends RecyclerView.Adapter<ParentCityInfoAdapter.CityDataViewHolder> {
    View view;
    Context context;
    List<CityInfoData> cityList;
    ItemEventListener<String> itemEventListener;


    public ParentCityInfoAdapter(Context context, List<CityInfoData> cityList, ItemEventListener<String> itemEventListener) {
        Debugger.e("ADAPTER CALLED::::::");
        this.context = context;
        this.cityList = cityList;
        this.itemEventListener = itemEventListener;
    }

    @Override
    public CityDataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = View.inflate(context, R.layout.raw_city, null);
        return new CityDataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CityDataViewHolder holder, int position) {
        holder.bind(position);

    }


    @Override
    public int getItemCount() {
        return cityList.size();
    }


    class CityDataViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.cityImage)
        ImageView cityImage;
        @BindView(R.id.cityText)
        CustomTextView cityText;
        @BindView(R.id.cityInfoLayout)
        FrameLayout cityInfoLayout;

        public CityDataViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(final int position) {
            if (Locale.getDefault().getDisplayLanguage().equals("العربية")) {
                cityText.setText(cityList.get(position).getArTitle());
            } else {
                cityText.setText(cityList.get(position).getTitle());
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ((MainActivity) context).picasso.load(cityList
                            .get(position).getWebViewImageThumb())
                            .transform(new ColorFilterTransformation(context.getResources().getColor(R.color.transparent_blue))).into(cityImage);
                /*Picasso.with(getActivity()).load(timeLineImage).transform(new ColorFilterTransformation(getResources().getColor(R.color.transparent_blue))).into(myInfoBackTimelineImage);*/
                }
            }, 50);

            cityInfoLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemEventListener.onItemEventFired("webview", new Gson().toJson(cityList.get(position)), "");
                }
            });
        }
    }
}