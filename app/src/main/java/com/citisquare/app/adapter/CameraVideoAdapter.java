package com.citisquare.app.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.citisquare.app.utils.Debugger;

import java.util.List;

/**
 * Created by hlink56 on 1/9/16.
 */
public class CameraVideoAdapter extends FragmentPagerAdapter {
    List<Fragment> fragmentList;
    List<String> stringsList;

    public CameraVideoAdapter(FragmentManager fm, List<Fragment> fragmentList, List<String> stringsList) {
        super(fm);
        this.fragmentList = fragmentList;
        this.stringsList = stringsList;
        Debugger.e("ADAPTER SIZE " + fragmentList);
    }

    @Override
    public Fragment getItem(int position) {
        Debugger.e("GET ITEM POSITION ::::: " + position);
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

   /* @Override
    public CharSequence getPageTitle(int position) {
        return stringsList.get(position);
    }*/
}

