package com.citisquare.app.adapter;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.citisquare.app.R;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.interfaces.IconPagerAdapter;
import com.citisquare.app.pojo.response.SliderData;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


///http://stackoverflow.com/questions/11736953/viewpager-does-not-redraw-content-remains-turns-blank
///https://www.google.co.in/?gfe_rd=cr&ei=odwLVoKcCrHG8Afrioto&gws_rd=ssl#q=android+viewpager+sometimes+get+null+view
///https://github.com/MatthieuLJ/ViewPagerParallax
///http://stackoverflow.com/questions/10849552/update-viewpager-dynamically?lq=1

/**
 * Created by hlink16 on 29/9/15.
 */
public class NewspaperPictureSlideAdapter extends PagerAdapter implements IconPagerAdapter {


    LayoutInflater mLayoutInflater;
    //ClickListener callback;
    Context context;

    private List<SliderData> list;

    public NewspaperPictureSlideAdapter(FragmentManager fm, Context context, List<SliderData> listImages) {

        this.list = listImages;
        this.context = context;
        //this.callback = callback;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void startUpdate(ViewGroup container) {
        super.startUpdate(container);
    }

    @Override
    public int getIconResId(int index) {
        return 0;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return list.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        View itemView = mLayoutInflater.inflate(R.layout.quickinfo_picture_slider, container, false);
        ViewHolder viewHolder = new ViewHolder(itemView);

        //int weight = (int)context.getResources().getDimension(R.dimen.dp_210);
        int height = (int) context.getResources().getDimension(R.dimen.dp_300);

        //((MainActivity) context).setImage(viewHolder.slideImges);

        /*((MainActivity) context)
                .picasso
                .load(list.get(position).toString())
                .placeholder(R.drawable.james)
                .resize(0, height)
                .into(viewHolder.dropDown);*/

        //((MainActivity) context).picasso.load(list.get(position).getSliderImageThumb()).into(viewHolder.slideImges);
        Glide.with(context).load(list.get(position).getSliderImageThumb()).diskCacheStrategy(DiskCacheStrategy.NONE).into(viewHolder.slideImges);
        viewHolder.sliderTextName.setText(list.get(position).getTitle());
        viewHolder.sliderTextJust.setText(list.get(position).getDescription());

        /*viewHolder.dropDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                *//*if (callback != null) {
                    callback.onOkClick(position);
                }*//*
            }
        });*/
        container.addView(itemView, 0);
        return itemView;
    }


    @Override
    public void destroyItem(View arg0, int arg1, Object arg2) {
        ((ViewPager) arg0).removeView(arg0);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((View) object);

    }

    public void refresh() {
        notifyDataSetChanged();
    }

    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
    public class ViewHolder {
        @BindView(R.id.slideImges)
        ImageView slideImges;
        @BindView(R.id.sliderTextName)
        CustomTextView sliderTextName;
        @BindView(R.id.sliderTextJust)
        CustomTextView sliderTextJust;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
