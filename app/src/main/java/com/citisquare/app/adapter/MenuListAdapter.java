package com.citisquare.app.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.model.KeyvaluePair;
import com.citisquare.app.pojo.response.BadgeCounterData;
import com.citisquare.app.utils.Debugger;

import java.util.HashMap;
import java.util.List;

/**
 * Created by hlink16 on 25/5/16.
 */
public class MenuListAdapter extends BaseExpandableListAdapter {



    private Context _context;
    private List<KeyvaluePair> listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<String>> _listDataChild;

    public MenuListAdapter(Context context, List<KeyvaluePair> listDataHeader,
                           HashMap<String, List<String>> listChildData) {
        this._context = context;
        this.listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this.listDataHeader.get(groupPosition).getValue())
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.sub_menu_drawer, null);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.subMenuText);

        txtListChild.setText(childText);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this.listDataHeader.get(groupPosition).getValue()).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }


    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
//        String headerTitle = (String) getGroup(groupPosition);
        String headerTitle = listDataHeader.get(groupPosition).getValue();
        Drawable drawable = listDataHeader.get(groupPosition).getKey();
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.raw_menu, null);
        }


        TextView badgeText = (TextView) convertView.findViewById(R.id.bageCounter);
        ImageView badge = (ImageView) convertView.findViewById(R.id.badgeCount);
        ImageView dropDown=(ImageView) convertView.findViewById(R.id.dropDown);
        TextView header = (TextView) convertView.findViewById(R.id.textViewMenuTitle);
        ImageView imageMenu = (ImageView) convertView.findViewById(R.id.imageViewMenu);

        imageMenu.setImageDrawable(drawable);
        //badgeText.setText(listDataHeader.get(groupPosition).getBadge() + "");

        if (listDataHeader.get(groupPosition).getBadge() > 0) {
            badgeText.setVisibility(View.VISIBLE);
            badge.setVisibility(View.VISIBLE);
            badgeText.setText(listDataHeader.get(groupPosition).getBadge() + "");
        } else {
            badgeText.setVisibility(View.INVISIBLE);
            badge.setVisibility(View.INVISIBLE);
        }

        if (groupPosition == 7) {
            dropDown.setVisibility(View.VISIBLE);
        } else
            dropDown.setVisibility(View.INVISIBLE);

        if (groupPosition == ((MainActivity) _context).getSelectionPostion()) {
            header.setTextColor(ContextCompat.getColor(_context, R.color.colorPrimary));
            imageMenu.setColorFilter(_context.getResources().getColor(R.color.colorPrimary));
        } else {
            header.setTextColor(Color.WHITE);
            imageMenu.setColorFilter(Color.WHITE);
        }

        header.setText(headerTitle);
        Debugger.e("POSITION:::" + groupPosition + "TEXT " + headerTitle);

        return convertView;
    }
    public void hideView(int pos)
    {
        listDataHeader.remove(pos);
        notifyDataSetChanged();
    }

    public void showView(int pos)
    {
        listDataHeader.add(pos, listDataHeader.get(pos));
        notifyDataSetChanged();
    }
    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void setData(BadgeCounterData data) {

    }

}