package com.citisquare.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.pojo.response.GalleryData;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 6/6/16.
 */
public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.GalleryViewHolder> {
    View view;
    Context context;
    ArrayList<GalleryData> galleryDataList;
    int width;
    ItemEventListener<String> rendering;

    public GalleryAdapter(Context context, ArrayList<GalleryData> galleryDataList, ItemEventListener<String> rendering) {
        this.context = context;
        this.galleryDataList = galleryDataList;
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        width = displayMetrics.widthPixels;
        this.rendering = rendering;
    }

    @Override
    public GalleryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = View.inflate(context, R.layout.raw_gallery, null);
        return new GalleryViewHolder(view);
    }


    @Override
    public void onBindViewHolder(GalleryViewHolder holder, final int position) {
        if (galleryDataList.get(position).getMedia_name_thumb().equals("DATA") && position == 0) {
            //holder.galleryRaw.setBackground(context.getResources().getDrawable(R.drawable.dashed_blue_line));
            //holder.galleryRaw.setImageDrawable(context.getResources().getDrawable(R.drawable.camera_addphoto_gallery));
            holder.galleryRaw.setImageResource(R.drawable.cam_icn_gallery);
            holder.galleryRaw.setPadding(15, 15, 15, 15);
        } else {
            if (galleryDataList.get(position).getMedia_type().equals("V")) {
                holder.videoImage.setVisibility(View.VISIBLE);
            } else {
                holder.videoImage.setVisibility(View.INVISIBLE);
            }
            //((MainActivity) context).picasso.load(R.drawable.city).fit().into(holder.galleryRaw);
            ((MainActivity) context).picasso.load(galleryDataList.get(position).getMedia_name_thumb()).fit().into(holder.galleryRaw);
        }


        holder.galleryRaw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (galleryDataList.get(position).getMedia_name_thumb().equals("DATA") && position == 0)
                    rendering.onItemEventFired("gallery", "", "");
                else {
                    if (galleryDataList.get(position).getMedia_type().equalsIgnoreCase("V")) {
                        rendering.onItemEventFired("video", galleryDataList.get(position).getMedia_name(), "");
                    } else {
                        rendering.onItemEventFired("photo", new Gson().toJson(galleryDataList.get(position)), "");
                    }

                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return galleryDataList.size();
    }


    class GalleryViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.galleryRaw)
        ImageView galleryRaw;
        @BindView(R.id.videoImage)
        ImageView videoImage;


        public GalleryViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            galleryRaw.setMinimumHeight(width / 4);
        }
    }
}

