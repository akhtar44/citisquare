package com.citisquare.app.adapter;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.interfaces.Rendering;
import com.citisquare.app.pojo.response.OfferData;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 7/6/16.
 */
public class TimelineAdapter extends RecyclerView.Adapter<TimelineAdapter.NewsProfileViewHolder> {

    View view;
    Context context;
    ArrayList<OfferData> quickInfoDatas;
    ItemEventListener<String> itemEventListener;


    public TimelineAdapter(Context context, ArrayList<OfferData> quickInfoDatas, ItemEventListener<String> itemEventListener) {
        this.context = context;
        this.quickInfoDatas = quickInfoDatas;
        this.itemEventListener = itemEventListener;
    }

    @Override
    public NewsProfileViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = View.inflate(context, R.layout.raw_quickinfo, null);
        return new NewsProfileViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NewsProfileViewHolder holder, final int position) {
        final NewsProfileViewHolder quickInfoHomeData = (NewsProfileViewHolder) holder;
        quickInfoHomeData.bindView(position);
    }


    @Override
    public int getItemCount() {
        return quickInfoDatas.size();
    }


    class NewsProfileViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.quickInfoUserLogo)
        ImageView quickInfoUserLogo;
        @BindView(R.id.homeUsername)
        TextView homeUsername;
        @BindView(R.id.homeLocation)
        TextView homeLocation;
        @BindView(R.id.homeDate)
        TextView homeDate;
        @BindView(R.id.followQuickInfo)
        Button followQuickInfo;
        @BindView(R.id.quickInfoImage)
        ImageView quickInfoImage;
        @BindView(R.id.commentQuickInfo)
        TextView commentQuickInfo;
        @BindView(R.id.likeQuickInfo)
        TextView likeQuickInfo;
        @BindView(R.id.shareQuickInfo)
        TextView shareQuickInfo;
        @BindView(R.id.quickInfoText)
        CustomTextView quickInfoText;
        @BindView(R.id.topInfo)
        LinearLayout topInfo;
        @BindView(R.id.cardView)
        CardView cardView;
        @BindView(R.id.videoicon)
        ImageView videoicon;
        @BindView(R.id.relativeLayoutLikeDetailsPage)
        RelativeLayout relativeLayout;

        @BindView(R.id.QuickInfoTopLayout)
        FrameLayout QuickInfoTopLayout;
        @BindView(R.id.likeImage)
        ImageView likeImage;


        public NewsProfileViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bindView(final int position) {
            followQuickInfo.setVisibility(View.GONE);

            try {

                if (quickInfoDatas.size() > 0) {
                    ((MainActivity) context).setCircularImage(quickInfoUserLogo, quickInfoDatas.get(position).getProfileImageThumb());
                    //String userName = quickInfoDatas.get(position).getFirstName() + quickInfoDatas.get(position).getLastName();
                    homeUsername.setText(quickInfoDatas.get(position).getCompanyName());
                    homeLocation.setText(quickInfoDatas.get(position).getCityName());
                    DateFormat utcFormat = new SimpleDateFormat(Constants.WS_DATE);
                    utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

                    Date date = null;
                    try {
                        date = utcFormat.parse(quickInfoDatas.get(position).getInsertdate());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    DateFormat pstFormat = new SimpleDateFormat(Constants.DATE_DISPLAY);
                    pstFormat.setTimeZone(TimeZone.getDefault());
                    homeDate.setText(pstFormat.format(date));


                    ((MainActivity) context).setImage(quickInfoDatas.get(position).getMediaNameThumb(), quickInfoImage);
                    if (quickInfoDatas.get(position).getMediaType() != null) {
                        if (quickInfoDatas.get(position).getMediaType().equalsIgnoreCase("")) {
                            quickInfoImage.setVisibility(View.GONE);
                            videoicon.setVisibility(View.GONE);
                        } else if (quickInfoDatas.get(position).getMediaType().equalsIgnoreCase("V")) {
                            quickInfoImage.setVisibility(View.VISIBLE);
                            videoicon.setVisibility(View.VISIBLE);
                        } else if (quickInfoDatas.get(position).getMediaType().equalsIgnoreCase("P")) {
                            quickInfoImage.setVisibility(View.VISIBLE);
                            videoicon.setVisibility(View.INVISIBLE);
                        }
                    } else {
                        quickInfoImage.setVisibility(View.GONE);
                        videoicon.setVisibility(View.GONE);
                    }

                    quickInfoText.setText(quickInfoDatas.get(position).getDescription());

                    if(String.valueOf(quickInfoDatas.get(position).getLikeCount()).equals("null")){
                        likeQuickInfo.setText("0");
                    }
                    if(String.valueOf(quickInfoDatas.get(position).getCommentCount()).equals("null")){
                        commentQuickInfo.setText("0");
                    }
                    //likeQuickInfo.setText(String.valueOf(quickInfoDatas.get(position).getLikeCount()));
                    //commentQuickInfo.setText(String.valueOf(quickInfoDatas.get(position).getCommentCount()));
                    if (quickInfoDatas.get(position).getIsFollowing() != null) {
                        if (quickInfoDatas.get(position).getIsFollowing() == 1) {
                            followQuickInfo.setText(context.getString(R.string.following));
                            followQuickInfo.setTextColor(Color.WHITE);
                            followQuickInfo.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
                        } else {
                            followQuickInfo.setText(context.getString(R.string.FOLLOW));
                            followQuickInfo.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                            followQuickInfo.setBackground(context.getResources().getDrawable(R.drawable.border_blue));
                        }
                    }

                    followQuickInfo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (((MainActivity) context).getUserForWholeApp().equals(Enum.setUser.GUEST)) {
                                ((MainActivity) context).messageAlert(context.getString(R.string.message_Guest));
                            } else if (((MainActivity) context).getUserForWholeApp().equals(Enum.setUser.SELLER_BASIC)) {
                                ((MainActivity) context).messageAlert(context.getString(R.string.message_visitor));
                            } else {
                                if (quickInfoDatas.get(position).getIsFollowing() == 1) {
                                    ((MainActivity) context).alertTwoAction(context.getString(R.string.message_DoYouWantToUnFollow), new Rendering() {
                                        @Override
                                        public void response(boolean isCofirm) {
                                            if (isCofirm) {
                                                quickInfoDatas.get(position).setIsFollowing(0);
                                                notifyDataSetChanged();
                                                itemEventListener.onItemEventFired("Unfollow", quickInfoDatas.get(position).getUserId(), "");
                                            }
                                        }
                                    });
                                } else {
                                    quickInfoDatas.get(position).setIsFollowing(1);
                                    itemEventListener.onItemEventFired("Follow", quickInfoDatas.get(position).getUserId(), "");
                                    notifyDataSetChanged();
                                }
                            }
                        }
                    });

                    relativeLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (((MainActivity) context).getUserForWholeApp().equals(Enum.setUser.GUEST)) {
                                ((MainActivity) context).messageAlert(context.getString(R.string.message_Guest));
                            } else {
                                if (quickInfoDatas.get(position).getIsLike() == 0) {

                                    Animator zoomAnimation = (AnimatorSet) AnimatorInflater.loadAnimator(context, R.animator.zoom_in_out);
                                    zoomAnimation.setTarget(likeImage);
                                    zoomAnimation.start();

                                    quickInfoDatas.get(position).setIsLike(1);
                                    quickInfoDatas.get(position).setLikeCount(quickInfoDatas.get(position).getLikeCount() + 1);
                                    notifyDataSetChanged();
                                    itemEventListener.onItemEventFired("Like", quickInfoDatas.get(position).getId(), "");
                                }
                            }
                        }
                    });


                    quickInfoImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            itemEventListener.onItemEventFired("Child", new Gson().toJson(quickInfoDatas.get(position)), "");
                        }
                    });

                    topInfo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            itemEventListener.onItemEventFired("otherProfile", quickInfoDatas.get(position).getUserId(), "");
                        }
                    });

                    commentQuickInfo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            itemEventListener.onItemEventFired("comment", quickInfoDatas.get(position).getId(), getAdapterPosition() + "");
                        }
                    });

                    shareQuickInfo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            itemEventListener.onItemEventFired("share", new Gson().toJson(quickInfoDatas.get(position)), "");
                        }
                    });


                    videoicon.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            itemEventListener.onItemEventFired("Video", quickInfoDatas.get(position).getMediaName(), "");
                        }
                    });

                    homeLocation.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            itemEventListener.onItemEventFired("map", quickInfoDatas.get(getAdapterPosition()).getUserId(), "");
                        }
                    });
                }
            }catch (Exception ex)
            {
                ex.printStackTrace();
            }



        }
    }
}
