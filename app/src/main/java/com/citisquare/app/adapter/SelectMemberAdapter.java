package com.citisquare.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.pojo.response.FollowerData;
import com.citisquare.app.utils.Debugger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 18/7/16.
 */
public class SelectMemberAdapter extends ArrayAdapter<FollowerData> {

    List<FollowerData> followerDataList;
    Context context;
    int resourse;
    HashMap<Integer, ViewHolder> hashMap;
    int counter;
    ArrayList<String> stringList;


    public SelectMemberAdapter(Context context, int resource, List<FollowerData> followerDataList) {
        super(context, resource, followerDataList);
        this.context = context;
        this.followerDataList = followerDataList;
        this.resourse = resource;
        hashMap = new HashMap<>();
        stringList = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return super.getCount();
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.raw_selectmember, parent, false);
        }
        final ViewHolder vh = new ViewHolder(convertView);

        ((MainActivity) context).setCircularImage(vh.selectMemberImage, followerDataList.get(position).getProfileImageThumb());
        String name = followerDataList.get(position).getFirstName() + " " + followerDataList.get(position).getLastName();
        vh.selectMemberName.setText(name);
        vh.selectMemberLayout.setSelected(followerDataList.get(position).isSelect());

        if (hashMap.get(position) == null)
            hashMap.put(position, vh);


        if (hashMap.get(position) != null) {
            if (followerDataList.get(position).isSelect()) {
                vh.selectMemberSelection.setBackgroundResource(R.drawable.check);
                vh.isSelected = true;
                stringList.add(followerDataList.get(position).getId());
            } else {
                vh.selectMemberSelection.setBackgroundResource(R.drawable.uncheck);
                vh.isSelected = false;
                stringList.remove(followerDataList.get(position).getId());
            }
            hashMap.put(position, vh);
        }

        vh.selectMemberLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewHolder viewHolder = hashMap.get(position);
                viewHolder.isSelected = !viewHolder.isSelected;
                //hashMap.put(position, viewHolder);
                if (viewHolder.isSelected) {
                    vh.selectMemberLayout.setSelected(!vh.selectMemberLayout.isSelected());
                    followerDataList.get(position).setSelect(true);
                    viewHolder.isSelected = true;
                    stringList.add(followerDataList.get(position).getId());
                    Debugger.e("SELECT:: > "+stringList.toString());
                    vh.selectMemberSelection.setBackgroundResource(R.drawable.check);
                    counter++;
                } else {
                    vh.selectMemberLayout.setSelected(!vh.selectMemberLayout.isSelected());
                    followerDataList.get(position).setSelect(false);
                    viewHolder.isSelected = false;
                    stringList.remove(followerDataList.get(position).getId());
                    Debugger.e("SELECT:: > "+stringList.toString());
                    vh.selectMemberSelection.setBackgroundResource(R.drawable.uncheck);
                    counter--;
                }

                hashMap.put(position, viewHolder);
            }
        });
        return convertView;
    }

    public int getCounter() {
        return counter;
    }


    public void setCountZero() {
        counter = 0;
    }

    public ArrayList<String> getReceiverId() {
        return stringList;
    }

    static class ViewHolder {
        @BindView(R.id.selectMemberImage)
        ImageView selectMemberImage;
        @BindView(R.id.selectMemberName)
        TextView selectMemberName;
        @BindView(R.id.selectMemberSelection)
        ImageView selectMemberSelection;
        @BindView(R.id.selectMemberLayout)
        LinearLayout selectMemberLayout;
        boolean isSelected;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}