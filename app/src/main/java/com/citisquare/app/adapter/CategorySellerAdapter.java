package com.citisquare.app.adapter;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.controls.CustomButton;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.fragments.RateAndReviewFragment;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.interfaces.Rendering;
import com.citisquare.app.pojo.response.SellerData;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by hlink56 on 6/6/16.
 */
public class CategorySellerAdapter extends RecyclerView.Adapter<CategorySellerAdapter.CategoryJobViewHolder> {
    View view;
    Context context;
    List<SellerData> sellerDatas;
    ItemEventListener<String> renderingString;
    private final int REQUEST_CALL_PERMISSION = 88;
    String phoneNumber;


    public CategorySellerAdapter(Context context, List<SellerData> sellerDatas, ItemEventListener<String> renderingString) {
        this.context = context;
        this.sellerDatas = sellerDatas;
        this.renderingString = renderingString;
    }

    @Override
    public CategoryJobViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = View.inflate(context, R.layout.raw_sub_category, null);
        return new CategoryJobViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CategoryJobViewHolder holder, final int position) {
        ((MainActivity) context).setCircularImage(holder.categoryJobImageView, sellerDatas.get(position).getProfileImageThumb());
        String companyName = sellerDatas.get(position).getCompanyName();
        holder.categoryJobPost.setText(companyName);
        if (sellerDatas.get(position).getCompanyNameArabic() != null) {
            if (sellerDatas.get(position).getCompanyNameArabic().length() > 0) {
                holder.arabicCompanyName.setVisibility(View.VISIBLE);
                holder.arabicCompanyName.setText(sellerDatas.get(position).getCompanyNameArabic());
            } else {
                holder.arabicCompanyName.setVisibility(View.GONE);
            }
        } else {
            holder.arabicCompanyName.setVisibility(View.GONE);
        }

        holder.categoryJobCompanyName.setText(sellerDatas.get(position).getStreet());

        holder.categoryJobDescription.setText(sellerDatas.get(position).getBio());
        if (sellerDatas.get(position).getIsFollowing().equals(1)) {
            holder.categoryJobFollow.setTextColor(Color.WHITE);
            holder.categoryJobFollow.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
            holder.categoryJobFollow.setText(context.getResources().getString(R.string.following));
        } else {
            holder.categoryJobFollow.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            holder.categoryJobFollow.setBackground(context.getResources().getDrawable(R.drawable.border_blue));
            holder.categoryJobFollow.setText(context.getResources().getString(R.string.FOLLOW));
        }

        holder.categoryJobFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((MainActivity) context).getUserForWholeApp() != null) {
                    if (((MainActivity) context).getUserForWholeApp().equals(Enum.setUser.GUEST)) {
                        ((MainActivity) context).messageAlert(context.getString(R.string.message_Guest));
                    } /*else if (((MainActivity) context).getUserForWholeApp().equals(Enum.setUser.SELLER_BASIC)) {
                        ((MainActivity) context).messageAlert(context.getString(R.string.message_visitor));
                    }*/ else if (sellerDatas.get(position).getIsFollowing().equals(1)) {
                        ((MainActivity) context).alertTwoAction(context.getString(R.string.message_DoYouWantToUnFollow), new Rendering() {
                            @Override
                            public void response(boolean isCofirm) {
                                if (isCofirm) {
                                    sellerDatas.get(position).setIsFollowing(0);
                                    renderingString.onItemEventFired("unfollow", sellerDatas.get(position).getId(), "");
                                    notifyDataSetChanged();
                                }
                            }
                        });
                    } else {
                        sellerDatas.get(position).setIsFollowing(1);
                        renderingString.onItemEventFired("follow", sellerDatas.get(position).getId(), "");
                        notifyDataSetChanged();
                    }
                }
            }
        });
        holder.categoryJobImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                renderingString.onItemEventFired("profile", sellerDatas.get(position).getId(), "");
            }
        });

        holder.linearlayout_full.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                renderingString.onItemEventFired("profile", sellerDatas.get(position).getId(), "");
            }
        });
        if (sellerDatas.get(position).getRate() != null) {
            holder.sellerRatingBar.setRating(Float.parseFloat(sellerDatas.get(position).getRate()));
        } else
            holder.sellerRatingBar.setRating(Float.parseFloat("0.0"));
        holder.sellerRatingAverage.setText(sellerDatas.get(position).getRatingCount() + "");
        phoneNumber = sellerDatas.get(position).getCellCountryCode() + sellerDatas.get(position).getCellPhoneno();
        holder.phoneNumberSellerListing.setText(underLine(phoneNumber));
        holder.phoneNumberSellerListing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                phoneNumber = sellerDatas.get(position).getCellCountryCode() + sellerDatas.get(position).getCellPhoneno();
                call(phoneNumber);
            }
        });
        holder.sellerRatingBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) context).setFragment(new RateAndReviewFragment(), true, "");
            }
        });

        if (sellerDatas.get(position).getIsActive().equals("2")) {
            holder.profileVerifiedInsellerList.setVisibility(View.VISIBLE);
        } else {
            holder.profileVerifiedInsellerList.setVisibility(View.INVISIBLE);
        }

    }

    public void call(final String phone) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("" + context.getString(R.string.app_name));
        alertDialogBuilder
                .setMessage("" + Html.fromHtml(phone))
                .setPositiveButton("CALL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Intent.ACTION_CALL);
                        intent.setData(Uri.parse("tel:" + phone));
                        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        context.startActivity(intent);
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public SpannableString underLine(String data) {
        SpannableString content = new SpannableString(data);
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        return content;
    }


    @Override
    public int getItemCount() {
        return sellerDatas.size();
    }


    class CategoryJobViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.categoryJobImageView)
        ImageView categoryJobImageView;
        @BindView(R.id.profileVerifiedInsellerList)
        CustomTextView profileVerifiedInsellerList;
        @BindView(R.id.categoryJobPost)
        CustomTextView categoryJobPost;
        @BindView(R.id.categoryJobCompanyMap)
        CustomTextView categoryJobCompanyName;
        @BindView(R.id.categoryJobFollow)
        CustomButton categoryJobFollow;
        @BindView(R.id.sellerRatingBar)
        RatingBar sellerRatingBar;
        @BindView(R.id.sellerRatingAverage)
        CustomTextView sellerRatingAverage;
        @BindView(R.id.phoneNumberSellerListing)
        CustomTextView phoneNumberSellerListing;
        @BindView(R.id.categoryJobDescription)
        CustomTextView categoryJobDescription;
        @BindView(R.id.arabicCompanyName)
        CustomTextView arabicCompanyName;
        @BindView(R.id.linearlayout_full)
        LinearLayout linearlayout_full;

        public CategoryJobViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        /*@OnClick(R.id.categoryJobImageView)
        public void categoryJobImageViewClicked() {
            itemEventListener.onItemEventFired("profile", "");
        }*/

        /*@OnClick(R.id.categoryJobPost)
        public void categoryJobPostClicked() {
            itemEventListener.onItemEventFired("profile", "");
        }*/

        @OnClick(R.id.phoneNumberSellerListing)
        public void phoneNumberClicked() {
            
        }

        @OnClick(R.id.categoryJobCompanyMap)
        public void categoryJobCompanyNameClicked() {
            renderingString.onItemEventFired("map", sellerDatas.get(getAdapterPosition()).getId(), "");
        }
    }
}

