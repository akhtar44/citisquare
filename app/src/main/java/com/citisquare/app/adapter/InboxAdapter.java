package com.citisquare.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.RenderingString;
import com.citisquare.app.pojo.response.InboxData;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 31/5/16.
 */
public class InboxAdapter extends RecyclerView.Adapter<InboxAdapter.InboxViewHolder> {

    View view;
    Context context;
    List<InboxData> inboxArrayList;
    RenderingString renderingString;


    public InboxAdapter(Context context, List<InboxData> inboxArrayList, RenderingString renderingString) {
        this.context = context;
        this.inboxArrayList = inboxArrayList;
        this.renderingString = renderingString;
    }

    @Override
    public InboxViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = View.inflate(context, R.layout.raw_inbox, null);
        return new InboxViewHolder(view);
    }

    @Override
    public void onBindViewHolder(InboxViewHolder holder, int position) {
        ((MainActivity) context).setCircularImage(holder.inboxUserImage, inboxArrayList.get(position).getProfileImageThumb());
        String name = inboxArrayList.get(position).getFirstName() + " " + inboxArrayList.get(position).getLastName();
        holder.inboxUsername.setText(name);
        holder.inboxData.setText(inboxArrayList.get(position).getMessage());
        if (!inboxArrayList.get(position).getCount().equals("0")) {
            holder.badgeCountMessage.setVisibility(View.VISIBLE);
            holder.bageCounterMessage.setVisibility(View.VISIBLE);
            holder.bageCounterMessage.setText(inboxArrayList.get(position).getCount());
        } else {
            holder.badgeCountMessage.setVisibility(View.INVISIBLE);
            holder.bageCounterMessage.setVisibility(View.INVISIBLE);
        }

        DateFormat utcFormat = new SimpleDateFormat(Constants.WS_DATE);
        utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date date = null;
        try {
            date = utcFormat.parse(inboxArrayList.get(position).getInsertdate());
        } catch (Exception e) {
            e.printStackTrace();
        }

        DateFormat pstFormat = new SimpleDateFormat(Constants.DATE_DISPLAY);
        pstFormat.setTimeZone(TimeZone.getDefault());
        holder.dateNTime.setText(pstFormat.format(date));
    }


    @Override
    public int getItemCount() {
        return inboxArrayList.size();
    }


    class InboxViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.inboxUserImage)
        ImageView inboxUserImage;
        @BindView(R.id.inboxUsername)
        CustomTextView inboxUsername;
        @BindView(R.id.dateNTime)
        CustomTextView dateNTime;
        @BindView(R.id.inboxData)
        CustomTextView inboxData;
        @BindView(R.id.badgeCountMessage)
        ImageView badgeCountMessage;
        @BindView(R.id.bageCounterMessage)
        CustomTextView bageCounterMessage;


        public InboxViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    renderingString.renderString(getAdapterPosition() + "");
                    //((MainActivity) context).inboxDialogFragment();
                }
            });
        }
    }
}
