package com.citisquare.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.controls.CustomTextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 19/8/16.
 */
public class BranchDropDownAdapter extends ArrayAdapter<String> {

    List<String> radiusList;
    Context context;
    int resourse;
    private boolean flag;

    public BranchDropDownAdapter(Context context, int resource, List<String> radiusList, boolean flag) {
        super(context, resource, radiusList);
        this.context = context;
        this.radiusList = radiusList;
        this.resourse = resource;
        this.flag = flag;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.raw_branch_drop_down, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (flag) {
            if (position == ((MainActivity) context).getBranchPosition()) {
                viewHolder.branchText.setTextColor(getContext().getResources().getColor(R.color.colorPrimary));
            } else {
                viewHolder.branchText.setTextColor(getContext().getResources().getColor(R.color.searchGray));
            }
        } else {
            viewHolder.branchText.setTextColor(getContext().getResources().getColor(R.color.searchGray));
        }
        viewHolder.branchText.setText(radiusList.get(position));
        return convertView;
    }


    static class ViewHolder {
        @BindView(R.id.branchText)
        CustomTextView branchText;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}

