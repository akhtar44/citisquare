package com.citisquare.app.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.interfaces.Rendering;
import com.citisquare.app.pojo.response.ProfileFollowerData;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 1/6/16.
 */
public class FollowersAdapter extends RecyclerView.Adapter<FollowersAdapter.FollowingViewHolder> {

    View view;
    Context context;

    List<ProfileFollowerData> followerDatas;
    ItemEventListener<String> itemEventListener;


    public FollowersAdapter(Context context, List<ProfileFollowerData> followerDatas, ItemEventListener<String> itemEventListener) {
        this.context = context;
        this.itemEventListener = itemEventListener;
        this.followerDatas = followerDatas;
    }

    @Override
    public FollowingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = View.inflate(context, R.layout.raw_followers, null);
        return new FollowingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final FollowingViewHolder holder, final int position) {
        holder.bindView(position);


    }


    @Override
    public int getItemCount() {
        return followerDatas.size();
    }


    class FollowingViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.followersImage)
        ImageView followersImage;
        @BindView(R.id.followersName)
        TextView followersName;
        @BindView(R.id.followersButton)
        Button followersButton;

        public FollowingViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bindView(final int pos) {
            if (followerDatas.get(pos).getIsFollowing() == 1) {
                followersButton.setText(context.getString(R.string.following));
                followersButton.setTextColor(Color.WHITE);
                followersButton.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
            } else {
                followersButton.setText(context.getString(R.string.FOLLOW));
                followersButton.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                followersButton.setBackground(context.getResources().getDrawable(R.drawable.border_blue));
            }

            ((MainActivity) context).setCircularImage(followersImage, followerDatas.get(pos).getProfileImageThumb());
            //String name = followerDatas.get(pos).getFirstName() + " " + followerDatas.get(pos).getLastName();
            String name = followerDatas.get(pos).getUsername();
            followersName.setText(name);

            if (((MainActivity) context).userId().equals(followerDatas.get(pos).getUserId())) {
                followersButton.setVisibility(View.INVISIBLE);
            } else if (followerDatas.get(pos).getRole().equalsIgnoreCase("V") && ((MainActivity) context).getData(context).getData().getRole().equalsIgnoreCase("V")) {
                followersButton.setVisibility(View.INVISIBLE);
            } else {
                followersButton.setVisibility(View.VISIBLE);
            }


            followersButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (((MainActivity) context).getUserForWholeApp().equals(Enum.setUser.GUEST)) {
                        ((MainActivity) context).messageAlert(context.getString(R.string.message_Guest));
                    } else if (((MainActivity) context).getUserForWholeApp().equals(Enum.setUser.SELLER_BASIC)) {
                        ((MainActivity) context).messageAlert(context.getString(R.string.message_visitor));
                    } else {

                        if (followerDatas.get(pos).getIsFollowing() == 1) {
                            ((MainActivity) context).alertTwoAction(context.getString(R.string.message_DoYouWantToUnFollow), new Rendering() {
                                @Override
                                public void response(boolean isCofirm) {
                                    if (isCofirm) {
                                        followerDatas.get(pos).setIsFollowing(0);
                                        notifyDataSetChanged();
                                        itemEventListener.onItemEventFired("Unfollow", followerDatas.get(pos).getUserId(), "");
                                    }
                                }
                            });
                        } else {
                            followerDatas.get(pos).setIsFollowing(1);
                            notifyDataSetChanged();
                            itemEventListener.onItemEventFired("Follow", followerDatas.get(pos).getUserId(), "");
                        }
                    }

                }
            });

            /*view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemEventListener.onItemEventFired("otherprofile", followerDatas.get(pos).getUserId(), "");
                }
            });*/
        }
    }
}
