package com.citisquare.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.pojo.response.CategoryData;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 1/6/16.
 */
public class CategoryDropDownAdapter extends ArrayAdapter<CategoryData> {

    List<CategoryData> radiusList;
    Context context;
    int resourse;
    private final boolean flag;


    public CategoryDropDownAdapter(Context context, int resource, List<CategoryData> radiusList, boolean flag) {
        super(context, resource, radiusList);
        this.context = context;
        this.radiusList = radiusList;
        this.resourse = resource;
        this.flag = flag;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.raw_category_dropdown, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (flag) {

        } else {
            if (position == ((MainActivity) context).getPositionCategory()) {
                viewHolder.categoryText.setTextColor(getContext().getResources().getColor(R.color.colorPrimary));
            } else {
                viewHolder.categoryText.setTextColor(getContext().getResources().getColor(R.color.dark_blue));
            }
        }
        if (Locale.getDefault().getDisplayLanguage().equals("العربية")) {
            viewHolder.categoryText.setText(radiusList.get(position).getArName());
        } else {
            viewHolder.categoryText.setText(radiusList.get(position).getName());
        }
        return convertView;
    }


    static class ViewHolder {
        @BindView(R.id.categoryText)
        CustomTextView categoryText;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
