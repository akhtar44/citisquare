package com.citisquare.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.citisquare.app.R;
import com.citisquare.app.model.SpinnerModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by hasib on 21/05/2018.
 */
public class CustomSpinnerAdapter extends ArrayAdapter {
    public LayoutInflater inflater;
    List<SpinnerModel> arrayList = new ArrayList<>();
    Context context;
    boolean isFromSearchSpinner;

    public CustomSpinnerAdapter(Context cont, int resource, List<SpinnerModel> arrayList, boolean isFromSearchSpinner) {
        super(cont, resource, arrayList);
        try {
            this.arrayList = arrayList;
            this.isFromSearchSpinner = isFromSearchSpinner;
            context = cont;
            inflater = LayoutInflater.from(getContext());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        try {
            SpinnerModel spinnerModel = arrayList.get(position);
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = inflater.inflate(R.layout.spinner_item, null);
                viewHolder.itemTextV = (TextView) convertView.findViewById(R.id.itemTextV);
                //viewHolder.color = (RobotoTextView) convertView.findViewById(R.id.colorcodeText);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.itemTextV.setText("");
            viewHolder.itemTextV.setText(spinnerModel.getTitle());

       /* if (isFromSearchSpinner) {
            if (position == 0 && StringUtils.startsWith(spinnerModel.getName(),context.getString(R.string.select)))
                convertView.setVisibility(View.INVISIBLE);
            else
                convertView.setVisibility(View.VISIBLE);
        }*/

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return convertView;
    }

    private static class ViewHolder {
        TextView itemTextV;
    }

    public void updateList(List<SpinnerModel> newList) {
        try {
            // arrayList = newList;
            this.arrayList.clear();
            this.arrayList.addAll(newList);
            notifyDataSetChanged();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
