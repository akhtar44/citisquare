package com.citisquare.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.citisquare.app.R;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.pojo.response.BranchPlanData;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 30/1/17.
 */

public class BranchPlanAdapter extends RecyclerView.Adapter<BranchPlanAdapter.BranchPlanViewHolder> {

    View view;
    Context context;
    List<BranchPlanData> branchPlanDatas;


    private ItemEventListener<String> itemEventListener;


    public BranchPlanAdapter(Context context, List<BranchPlanData> branchPlanDatas, ItemEventListener<String> itemEventListener) {
        this.context = context;
        this.branchPlanDatas = branchPlanDatas;
        this.itemEventListener = itemEventListener;
    }


    @Override
    public BranchPlanViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = View.inflate(context, R.layout.raw_branch_plan, null);
        return new BranchPlanViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BranchPlanViewHolder holder, int position) {
        holder.branchPlanName.setText(branchPlanDatas.get(position).getName());
        holder.branchPrice.setText(branchPlanDatas.get(position).getPrice());
    }

    @Override
    public int getItemCount() {
        return branchPlanDatas.size();
    }

    class BranchPlanViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.branchPlanName)
        CustomTextView branchPlanName;
        @BindView(R.id.branchPrice)
        CustomTextView branchPrice;
        @BindView(R.id.viewPlan)
        View viewPlan;
        @BindView(R.id.mainLayoutSeller)
        LinearLayout mainLayoutSeller;

        BranchPlanViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemEventListener.onItemEventFired(branchPlanDatas.get(getAdapterPosition()).getPrice(), branchPlanDatas.get(getAdapterPosition()).getId(), "");
                }
            });
        }
    }
}