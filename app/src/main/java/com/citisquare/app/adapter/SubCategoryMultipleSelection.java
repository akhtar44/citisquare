package com.citisquare.app.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;

import com.citisquare.app.R;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.interfaces.RenderingString;
import com.citisquare.app.pojo.response.SubCategory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 26/8/16.
 */
public class SubCategoryMultipleSelection extends ArrayAdapter<SubCategory> {

    List<SubCategory> subCategories;
    Context context;
    HashMap<Integer, ViewHolder> hashMap;
    List<String> stringList;
    HashMap<Integer, String> integerStringHashMap;
    int resourse;
    RenderingString renderingString;
    private int counter;


    public SubCategoryMultipleSelection(Context context, int resource, List<SubCategory> subCategories, RenderingString renderingString) {
        super(context, resource, subCategories);
        this.context = context;
        this.subCategories = subCategories;
        this.resourse = resource;
        this.renderingString = renderingString;
        integerStringHashMap = new HashMap<>();
        stringList = new ArrayList<>();
        hashMap = new HashMap<>();
        for (SubCategory subCategory : subCategories) {
            subCategory.setSelected(false);
        }
    }

    @Override
    public int getCount() {
        return super.getCount();
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder vh;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.raw_dialog_category, parent, false);
        }
        vh = new ViewHolder(convertView);
        if (Locale.getDefault().getDisplayLanguage().equals("العربية")) {
            vh.categoryName.setText(subCategories.get(position).getArName());
        } else {
            vh.categoryName.setText(subCategories.get(position).getName());
        }

        if (getItem(position).isSelected()) {
            vh.categoryName.setTextColor(Color.BLACK);
        } else {
            vh.categoryName.setTextColor(getContext().getResources().getColor(R.color.searchGray));
        }


        vh.viewCategoryDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getItem(position).isSelected()) {
                    getItem(position).setSelected(false);
                    notifyDataSetChanged();
                    counter++;
                } else {
                    getItem(position).setSelected(true);
                    notifyDataSetChanged();
                    counter--;
                }
            }
        });

        return convertView;
    }

    public List<SubCategory> getStringList() {
        return subCategories;
    }


    static class ViewHolder {
        @BindView(R.id.categoryName)
        CustomTextView categoryName;
        @BindView(R.id.viewCategoryDialog)
        RelativeLayout viewCategoryDialog;
        boolean isSelected;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}