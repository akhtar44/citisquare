package com.citisquare.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.citisquare.app.R;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.pojo.ContactUsData;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 27/12/16.
 */

public class ContactUsAdapter extends RecyclerView.Adapter<ContactUsAdapter.ContactUsHolder> {

    View view;
    Context context;
    List<ContactUsData> contactUsData;
    ItemEventListener<String> stringItemEventListener;


    public ContactUsAdapter(Context context, List<ContactUsData> contactUsData, ItemEventListener<String> stringItemEventListener) {
        this.context = context;
        this.contactUsData = contactUsData;
        this.stringItemEventListener = stringItemEventListener;
    }

    @Override
    public ContactUsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = View.inflate(context, R.layout.raw_contact_us, null);

        return new ContactUsHolder(view);
    }

    @Override
    public void onBindViewHolder(ContactUsHolder holder, int position) {
        holder.companyName.setText(contactUsData.get(position).getCompanyName());
        if (contactUsData.get(position).getAddress().equals("")) {
            holder.fullAddress.setVisibility(View.GONE);
        } else {
            holder.fullAddress.setVisibility(View.VISIBLE);
            holder.fullAddress.setText(contactUsData.get(position).getAddress());
        }
        if (contactUsData.get(position).getBusinessHour().equals("")) {
            holder.businessLayout.setVisibility(View.GONE);
        } else {
            holder.businessLayout.setVisibility(View.VISIBLE);
            holder.businessHour.setText(contactUsData.get(position).getBusinessHour());
        }
        holder.phoneNumber.setText(underLine(contactUsData.get(position).getPhone()));

        holder.fax.setText(contactUsData.get(position).getFax());
        if (contactUsData.get(position).getEmail().equals("")) {
            holder.emailLayout.setVisibility(View.GONE);
        } else {
            holder.emailLayout.setVisibility(View.VISIBLE);
            holder.emailId.setText(contactUsData.get(position).getEmail());
        }
        holder.website.setText(contactUsData.get(position).getWebsite());
    }

    public SpannableString underLine(String data) {
        SpannableString content = new SpannableString(data);
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        return content;
    }

    @Override
    public int getItemCount() {
        return contactUsData.size();
    }

    class ContactUsHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.companyName)
        CustomTextView companyName;
        @BindView(R.id.fullAddress)
        CustomTextView fullAddress;
        @BindView(R.id.businessHour)
        CustomTextView businessHour;
        @BindView(R.id.businessLayout)
        LinearLayout businessLayout;
        @BindView(R.id.phoneNumber)
        CustomTextView phoneNumber;
        @BindView(R.id.fax)
        CustomTextView fax;
        @BindView(R.id.email_id)
        CustomTextView emailId;
        @BindView(R.id.emailLayout)
        LinearLayout emailLayout;
        @BindView(R.id.website)
        CustomTextView website;

        public ContactUsHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            phoneNumber.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    stringItemEventListener.onItemEventFired("phone", contactUsData.get(getAdapterPosition()).getPhone(), "");
                }
            });
        }
    }
}