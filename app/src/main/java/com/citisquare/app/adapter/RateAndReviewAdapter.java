package com.citisquare.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.pojo.response.RateReviewData;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 31/1/17.
 */
public class RateAndReviewAdapter extends RecyclerView.Adapter<RateAndReviewAdapter.RateAndReviewViewHolder> {

    View view;
    Context context;
    List<RateReviewData> rateReviewDatas;
    ItemEventListener<String> itemEventListener;


    public RateAndReviewAdapter(Context context, List<RateReviewData> rateReviewDatas, ItemEventListener<String> itemEventListener) {
        this.context = context;
        this.rateReviewDatas = rateReviewDatas;
        this.itemEventListener = itemEventListener;
    }

    @Override
    public RateAndReviewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = View.inflate(context, R.layout.raw_rate_review, null);
        return new RateAndReviewViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RateAndReviewViewHolder holder, int position) {
        String name = rateReviewDatas.get(position).getFirstName() + " " + rateReviewDatas.get(position).getLastName();
        holder.rateAndReviewGivenByName.setText(name);
        holder.rateAndReviewTitle.setText(rateReviewDatas.get(position).getTitle());
        holder.rateReviewDescription.setText(rateReviewDatas.get(position).getReview());
        holder.rateReviewRatingBar.setRating(Integer.parseInt(rateReviewDatas.get(position).getRate()));
        if (rateReviewDatas.get(position).getUserId().equals(((MainActivity) context).getData(context).getData().getId())) {
            holder.imageDelete.setVisibility(View.VISIBLE);
        } else {
            holder.imageDelete.setVisibility(View.INVISIBLE);
        }

        DateFormat utcFormat = new SimpleDateFormat(Constants.WS_DATE);
        utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date date = null;
        try {
            date = utcFormat.parse(rateReviewDatas.get(position).getInsertdate());
        } catch (Exception e) {
            e.printStackTrace();
        }

        DateFormat pstFormat = new SimpleDateFormat(Constants.DATE_RATE);
        pstFormat.setTimeZone(TimeZone.getDefault());
        holder.rateAndReviewdate.setText(pstFormat.format(date));
    }


    @Override
    public int getItemCount() {
        return rateReviewDatas.size();
    }

    class RateAndReviewViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.rateAndReviewGivenByName)
        CustomTextView rateAndReviewGivenByName;
        @BindView(R.id.rateAndReviewdate)
        CustomTextView rateAndReviewdate;
        @BindView(R.id.rateAndReviewTitle)
        CustomTextView rateAndReviewTitle;
        @BindView(R.id.rateReviewDescription)
        CustomTextView rateReviewDescription;
        @BindView(R.id.rateReviewRatingBar)
        RatingBar rateReviewRatingBar;
        @BindView(R.id.imageDelete)
        ImageView imageDelete;

        RateAndReviewViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemEventListener.onItemEventFired("moveTo", getAdapterPosition() + "", "");
                }
            });

            rateAndReviewGivenByName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemEventListener.onItemEventFired("profile", rateReviewDatas.get(getAdapterPosition()).getUserId(), "");
                }
            });
            imageDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemEventListener.onItemEventFired("delete", getAdapterPosition() + "", "");
                }
            });
        }
    }
}
