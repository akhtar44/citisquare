package com.citisquare.app.adapter;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.controls.AutoScrollViewPager;
import com.citisquare.app.controls.CircleIndicator;
import com.citisquare.app.interfaces.Result;
import com.citisquare.app.pojo.response.CategoryData;
import com.citisquare.app.pojo.response.SliderData;
import com.citisquare.app.utils.Debugger;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 25/5/16.
 */
public class ParentCategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<CategoryData> categories;
    Result<CategoryData> categoryDataResult;
    private static final int HEADER = 1;
    private static final int NORMAL = 2;
    private static final int FOOTER = 3;
    AutoScrollViewPager dummyAutoScroll;
    private List<SliderData> sliderDataList=new ArrayList<>();
    NewspaperPictureSlideAdapter pictureSlideAdapter;
    FragmentManager manager;


    public ParentCategoryAdapter(Context context, List<CategoryData> categories, Result<CategoryData> categoryDataResult) {
        this.context = context;
        this.categories = categories;
        this.categoryDataResult = categoryDataResult;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        View view;

        if (viewType == HEADER) {
            view = View.inflate(context, R.layout.headerview_categories, null);
            viewHolder = new HeaderQuickInfo(view);
            return viewHolder;
        }
        else
        {
            view = View.inflate(context, R.layout.raw_parent_category, null);
            viewHolder = new QuickInfoHomeData(view);
        }
        return viewHolder;
    }

   public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
       try {
           if (holder instanceof QuickInfoHomeData) {
               final QuickInfoHomeData quickInfoHomeData = (QuickInfoHomeData) holder;
               quickInfoHomeData.bindView(position);

           } else if (holder instanceof HeaderQuickInfo) {
               HeaderQuickInfo vh = (HeaderQuickInfo) holder;
               vh.bindView(HEADER);
           }
       } catch (Exception e) {
           e.printStackTrace();
           Debugger.e(e.getMessage());
       }
   }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public int getItemViewType(int position) {
        /*if (position == 0) {
            // This is where we'll add HEADER.
            return HEADER;
        }*/
        return NORMAL;
    }

    class HeaderQuickInfo extends RecyclerView.ViewHolder {
        @BindView(R.id.quickInfoViewPager)
        AutoScrollViewPager quickInfoViewPager;
        @BindView(R.id.quickInfoCircleIndicator)
        CircleIndicator quickInfoCircleIndicator;

        public HeaderQuickInfo(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
        public void bindView(final int position) {
            pictureSlideAdapter = new NewspaperPictureSlideAdapter(manager, context, sliderDataList);
            quickInfoViewPager.startAutoScroll(2);
            dummyAutoScroll = quickInfoViewPager;
            quickInfoViewPager.setInterval(1500);
            quickInfoViewPager.setAdapter(pictureSlideAdapter);
            quickInfoCircleIndicator.setViewPager(quickInfoViewPager);

        }
    }
    class QuickInfoHomeData extends RecyclerView.ViewHolder {
        @BindView(R.id.imageViewParentCategoryImage)
        ImageView imageViewParentCategoryImage;
        @BindView(R.id.textViewParentCategory)
        TextView textViewParentCategory;
        Spannable spannableQuickInfo = new SpannableString(context.getResources().getString(R.string.MORE));
        public QuickInfoHomeData(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    categoryDataResult.onSuccess(categories.get(getAdapterPosition()));
                }
            });
            spannableQuickInfo.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.colorPrimary)), 0, spannableQuickInfo.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        public void bindView(final int position) {
            ((MainActivity) context).picasso.load(categories.get(position).getCategoryImgThumbUrl()).fit().into(imageViewParentCategoryImage);
            if (Locale.getDefault().getDisplayLanguage().equals("العربية")) {
                textViewParentCategory.setText(categories.get(position).getArName());
            }else {
                textViewParentCategory.setText(categories.get(position).getName());
            }
        }
    }
}
