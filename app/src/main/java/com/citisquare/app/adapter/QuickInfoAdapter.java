package com.citisquare.app.adapter;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.citisquare.app.error.G;
import com.google.gson.Gson;
import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.controls.AutoScrollViewPager;
import com.citisquare.app.controls.CircleIndicator;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.interfaces.Rendering;
import com.citisquare.app.pojo.response.CategoryData;
import com.citisquare.app.pojo.response.OfferData;
import com.citisquare.app.pojo.response.SliderData;
import com.citisquare.app.pojo.response.SubCategory;
import com.citisquare.app.utils.Debugger;
import com.citisquare.app.utils.QuickCategorySelection;
import com.citisquare.app.utils.QuickSubCatSelection;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by hlink56 on 2/6/16.
 */
public class QuickInfoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int HEADER = 1;
    private static final int NORMAL = 2;
    private static final int FOOTER = 3;
    FragmentManager manager;
    List<OfferData> quickInfoDatas;
    AutoScrollViewPager dummyAutoScroll;
    List<CategoryData> categories;
    List<SubCategory> subCategories;
    ItemEventListener<String> itemEventListener;
    NewspaperPictureSlideAdapter pictureSlideAdapter;
    QuickCategorySelection categoryDropDown;
    QuickSubCatSelection quickSubCatSelection;
    String categoryId = "";
    String subCategoryId = "";
    private List<SliderData> sliderDataList;
    private Context context;
    private boolean isCategory;


    public QuickInfoAdapter(Context context, FragmentManager manager, List<OfferData> quickInfoDatas,
                            ItemEventListener<String> itemEventListener, List<SliderData> sliderDataList,boolean isCategory) {
        this.context = context;
        this.quickInfoDatas = quickInfoDatas;
        this.manager = manager;
        this.itemEventListener = itemEventListener;
        categories = ((MainActivity) context).getCategoryList();
        this.sliderDataList = sliderDataList;
        this.isCategory=isCategory;
        subCategories = new ArrayList<>();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        RecyclerView.ViewHolder viewHolder;
        if (viewType == HEADER) {
            view = View.inflate(context, R.layout.quickinfo_header, null);
            viewHolder = new HeaderQuickInfo(view);
            return viewHolder;
        } else {

            view = View.inflate(context, R.layout.raw_quickinfo, null);
            viewHolder = new QuickInfoHomeData(view);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        try {
            if (holder instanceof QuickInfoHomeData) {
                final QuickInfoHomeData quickInfoHomeData = (QuickInfoHomeData) holder;

                quickInfoHomeData.bindView(position - 1);

            } else if (holder instanceof HeaderQuickInfo) {
                HeaderQuickInfo vh = (HeaderQuickInfo) holder;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Debugger.e(e.getMessage());
        }
    }

    /*public boolean isPositionFooter(int position) {
        return position == quickInfoDatas.size();
    }*/

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        if (quickInfoDatas == null)
            return 0;
        if (quickInfoDatas.size() == 0)
            return 1;
        return quickInfoDatas.size() + 1;
    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            // This is where we'll add HEADER.
            return HEADER;
        }
        return NORMAL;
    }


    public String selectedCategoryId() {
        if (categoryId != null)
            return categoryId;
        else
            return "";
    }

    public String selectedSubCategoryId() {
        if (categoryId != null)
            return subCategoryId;
        else
            return "";
    }


    public void notifySlider() {
        if (pictureSlideAdapter != null && dummyAutoScroll != null) {
            dummyAutoScroll.removeAllViews();
            pictureSlideAdapter.notifyDataSetChanged();
        }
    }

    class HeaderQuickInfo extends RecyclerView.ViewHolder {
        @BindView(R.id.quickInfoViewPager)
        AutoScrollViewPager quickInfoViewPager;
        @BindView(R.id.quickInfoCircleIndicator)
        CircleIndicator quickInfoCircleIndicator;
        @BindView(R.id.quickInfoCitySelcection)
        TextView quickInfoCitySelcection;
        @BindView(R.id.quickInfosubCatSelection)
        TextView quickInfosubCatSelection;
        @BindView(R.id.todaysoffertext)
        TextView todaysoffertext;

        public HeaderQuickInfo(View view) {
            super(view);
            ButterKnife.bind(this, view);
            pictureSlideAdapter = new NewspaperPictureSlideAdapter(manager, context, sliderDataList);
            quickInfoViewPager.startAutoScroll(2);
            dummyAutoScroll = quickInfoViewPager;
            quickInfoViewPager.setInterval(1500);
            quickInfoViewPager.setAdapter(pictureSlideAdapter);
            quickInfoCircleIndicator.setViewPager(quickInfoViewPager);

            final int[] location = new int[2];
            quickInfoCitySelcection.getLocationOnScreen(location);

            if(isCategory){
                todaysoffertext.setVisibility(View.GONE);
                quickInfoCitySelcection.setVisibility(View.GONE);
            }else{
                todaysoffertext.setVisibility(View.VISIBLE);
                quickInfoCitySelcection.setVisibility(View.VISIBLE);
            }


            quickInfoCitySelcection.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    categoryDropDown = new QuickCategorySelection(context, quickInfoCitySelcection.getHeight(), location, categories, new ItemEventListener<Integer>() {
                        @Override
                        public void onItemEventFired(Integer integer, Integer t2, String actualPos) {
                            if (Locale.getDefault().getDisplayLanguage().equals("العربية")) {
                                quickInfoCitySelcection.setText(categories.get(integer).getArName());
                            } else {
                                quickInfoCitySelcection.setText(categories.get(integer).getName());
                            }

                            categoryId = categories.get(integer).getId();
                            subCategories = categories.get(integer).getSubCategory();
                            quickInfosubCatSelection.setVisibility(View.VISIBLE);
                            quickInfosubCatSelection.setText(context.getString(R.string.select_sub_categor));
                            subCategoryId = "";
                            itemEventListener.onItemEventFired("category", categoryId, "");
                        }
                    });
                    categoryDropDown.show();
                }
            });

            final int[] locationCat = new int[2];
            quickInfosubCatSelection.getLocationOnScreen(locationCat);


            quickInfosubCatSelection.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    quickSubCatSelection = new QuickSubCatSelection(context
                            , quickInfosubCatSelection.getHeight()
                            , locationCat
                            , subCategories
                            , new ItemEventListener<Integer>() {
                        @Override
                        public void onItemEventFired(Integer integer, Integer t2, String actualPos) {
                            if (Locale.getDefault().getDisplayLanguage().equals("العربية")) {
                                quickInfosubCatSelection.setText(subCategories.get(integer).getArName());
                            } else {
                                quickInfosubCatSelection.setText(subCategories.get(integer).getName());
                            }
                            subCategoryId = subCategories.get(integer).getId();
                            itemEventListener.onItemEventFired("category", categoryId, "");
                        }
                    });
                    quickSubCatSelection.show();
                }
            });
        }
    }

    class QuickInfoHomeData extends RecyclerView.ViewHolder {
        private static final int MAX_LINE_COUNT = 2;
        @BindView(R.id.homeUsername)
        TextView homeUsername;
        @BindView(R.id.homeLocation)
        TextView homeLocation;
        @BindView(R.id.homeDate)
        TextView homeDate;
        @BindView(R.id.followQuickInfo)
        Button followQuickInfo;
        @BindView(R.id.commentQuickInfo)
        TextView commentQuickInfo;
        @BindView(R.id.likeQuickInfo)
        TextView likeQuickInfo;
        @BindView(R.id.relativeLayoutLikeDetailsPage)
        RelativeLayout relativeLayout;
        @BindView(R.id.shareQuickInfo)
        TextView shareQuickInfo;
        @BindView(R.id.quickInfoUserLogo)
        ImageView quickInfoUserLogo;
        @BindView(R.id.quickInfoImage)
        ImageView quickInfoImage;
        @BindView(R.id.topInfo)
        LinearLayout topInfo;
        @BindView(R.id.quickInfoText)
        TextView quickInfoText;
        @BindView(R.id.cardView)
        CardView cardView;
        @BindView(R.id.QuickInfoTopLayout)
        FrameLayout QuickInfoTopLayout;

        @BindView(R.id.likeImage)
        ImageView likeImage;
        @BindView(R.id.videoicon)
        ImageView videoicon;
        Spannable spannableQuickInfo = new SpannableString(context.getResources().getString(R.string.MORE));


        public QuickInfoHomeData(View view) {
            super(view);
            ButterKnife.bind(this, view);

            spannableQuickInfo.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.colorPrimary)), 0, spannableQuickInfo.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        public void bindView(final int position) {
            // ((MainActivity) context).setCircularImage(quickInfoUserLogo, "");

            ((MainActivity) context).setCircularImage(quickInfoUserLogo, quickInfoDatas.get(position).getProfileImageThumb());
            //String userName = quickInfoDatas.get(position).getFirstName() + quickInfoDatas.get(position).getLastName();
            homeUsername.setText(quickInfoDatas.get(position).getCompanyName());
            homeLocation.setText(quickInfoDatas.get(position).getCityName());
            DateFormat utcFormat = new SimpleDateFormat(Constants.WS_DATE);
            utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date date = null;
            try {
                date = utcFormat.parse(quickInfoDatas.get(position).getInsertdate());
            } catch (Exception e) {
                e.printStackTrace();
            }

            DateFormat pstFormat = new SimpleDateFormat(Constants.DATE_DISPLAY);
            pstFormat.setTimeZone(TimeZone.getDefault());
            homeDate.setText(pstFormat.format(date));


            ((MainActivity) context).setImage(quickInfoDatas.get(position).getMediaNameThumb(), quickInfoImage);


            if (quickInfoDatas.get(position).getMediaType().equalsIgnoreCase("")) {
                quickInfoImage.setVisibility(View.GONE);
                videoicon.setVisibility(View.GONE);
            } else if (quickInfoDatas.get(position).getMediaType().equalsIgnoreCase("V")) {
                quickInfoImage.setVisibility(View.VISIBLE);
                videoicon.setVisibility(View.VISIBLE);
            } else if (quickInfoDatas.get(position).getMediaType().equalsIgnoreCase("P")) {
                quickInfoImage.setVisibility(View.VISIBLE);
                videoicon.setVisibility(View.INVISIBLE);
            }
            //quickInfoText.setText(quickInfoDatas.get(position).getDescription());
            if (quickInfoDatas.get(position).getMediaType().equalsIgnoreCase(""))
                quickInfoText.setText(quickInfoDatas.get(position).getDescription());
            else {
                quickInfoText.setText(quickInfoDatas.get(position).getDescription());
            }


            likeQuickInfo.setText(String.valueOf(quickInfoDatas.get(position).getLikeCount()));
            commentQuickInfo.setText(String.valueOf(quickInfoDatas.get(position).getCommentCount()));

            if (quickInfoDatas.get(position).getUserId().equals(((MainActivity) context).getData(context).getData().getId())) {
                followQuickInfo.setVisibility(View.GONE);
            } else {
                followQuickInfo.setVisibility(View.VISIBLE);
                if (quickInfoDatas.get(position).getIsFollowing() == 1) {
                    followQuickInfo.setText(context.getString(R.string.following));
                    followQuickInfo.setTextColor(Color.WHITE);
                    followQuickInfo.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
                } else {
                    followQuickInfo.setText(context.getString(R.string.FOLLOW));
                    followQuickInfo.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    followQuickInfo.setBackground(context.getResources().getDrawable(R.drawable.border_blue));
                }
            }
            commentQuickInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemEventListener.onItemEventFired("comment", quickInfoDatas.get(position).getId(), getAdapterPosition() - 1 + "");
                }
            });

            quickInfoUserLogo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemEventListener.onItemEventFired("otherProfile", quickInfoDatas.get(position).getUserId(), "");
                }
            });

            homeUsername.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemEventListener.onItemEventFired("otherProfile", quickInfoDatas.get(position).getUserId(), "");
                }
            });

            followQuickInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (((MainActivity) context).getUserForWholeApp().equals(Enum.setUser.GUEST)) {
                        ((MainActivity) context).messageAlert(context.getString(R.string.message_Guest));
                    } else if (((MainActivity) context).getUserForWholeApp().equals(Enum.setUser.SELLER_BASIC)) {
                        ((MainActivity) context).messageAlert(context.getString(R.string.message_visitor));
                    } else {
                        if (quickInfoDatas.get(position).getIsFollowing() == 1) {
                            ((MainActivity) context).alertTwoAction(context.getString(R.string.message_DoYouWantToUnFollow), new Rendering() {
                                @Override
                                public void response(boolean isCofirm) {
                                    if (isCofirm) {
                                        quickInfoDatas.get(position).setIsFollowing(0);
                                        notifyDataSetChanged();
                                        itemEventListener.onItemEventFired("Unfollow", quickInfoDatas.get(position).getUserId(), "");
                                    }
                                }
                            });
                        } else {
                            quickInfoDatas.get(position).setIsFollowing(1);
                            itemEventListener.onItemEventFired("Follow", quickInfoDatas.get(position).getUserId(), "");
                            notifyDataSetChanged();
                        }
                    }
                }
            });

            videoicon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemEventListener.onItemEventFired("video", quickInfoDatas.get(position).getMediaName(), "");
                }
            });


            quickInfoImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemEventListener.onItemEventFired("Child", new Gson().toJson(quickInfoDatas.get(position)), "");
                }
            });

            relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (((MainActivity) context).getUserForWholeApp().equals(Enum.setUser.GUEST)) {
                        ((MainActivity) context).messageAlert(context.getString(R.string.message_Guest));
                    } else {
                        Debugger.e("QUICK INFO DATA POSITION:::" + quickInfoDatas.get(position).getIsLike());
                        if (quickInfoDatas.get(position).getIsLike() == 0) {

                            Animator zoomAnimation = (AnimatorSet) AnimatorInflater.loadAnimator(context, R.animator.zoom_in_out);
                            zoomAnimation.setTarget(likeImage);
                            zoomAnimation.start();


                            quickInfoDatas.get(position).setIsLike(1);
                            quickInfoDatas.get(position).setLikeCount(quickInfoDatas.get(position).getLikeCount() + 1);
                            notifyDataSetChanged();
                            itemEventListener.onItemEventFired("Like", quickInfoDatas.get(position).getId(), "");
                        }
                    }
                }
            });

            homeLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemEventListener.onItemEventFired("map", quickInfoDatas.get(position).getUserId(), "");
                }
            });

            shareQuickInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemEventListener.onItemEventFired("share", new Gson().toJson(quickInfoDatas.get(position)), "");
                }
            });
        }
    }
}