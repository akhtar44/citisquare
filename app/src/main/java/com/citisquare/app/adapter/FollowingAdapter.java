package com.citisquare.app.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.interfaces.Rendering;
import com.citisquare.app.pojo.response.ProfileFollowingData;
import com.citisquare.app.utils.Debugger;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 1/6/16.
 */
public class FollowingAdapter extends RecyclerView.Adapter<FollowingAdapter.FollowingViewHolder> {

    View view;
    Context context;

    List<ProfileFollowingData> followingDatas;
    ItemEventListener<String> itemEventListener;


    public FollowingAdapter(Context context, List<ProfileFollowingData> followingDatas, ItemEventListener<String> itemEventListener) {

        this.context = context;
        this.followingDatas = followingDatas;
        this.itemEventListener = itemEventListener;
    }

    @Override
    public FollowingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = View.inflate(context, R.layout.raw_following, null);

        return new FollowingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FollowingViewHolder holder, int position) {
        holder.bindView(position);
    }


    @Override
    public int getItemCount() {
        return followingDatas.size();
    }


    class FollowingViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.followingsImage)
        ImageView followingsImage;
        @BindView(R.id.followingUsername)
        TextView followingUsername;
        @BindView(R.id.followingsButton)
        Button followingsButton;

        public FollowingViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bindView(final int pos) {
            ((MainActivity) context).setCircularImage(followingsImage, followingDatas.get(pos).getProfileImageThumb());
            //String name = followingDatas.get(pos).getFirstName() + " " + followingDatas.get(pos).getLastName();
            String name = followingDatas.get(pos).getUsername();
            followingUsername.setText(name);
            Debugger.e("BEFORE :::" + followingsButton.getVisibility() + "::ROLE:::" + followingDatas.get(pos).getRole().equalsIgnoreCase("V"));

            if (((MainActivity) context).userId().equals(followingDatas.get(pos).getFollowerUserId())) {
                Debugger.e("1");
                followingsButton.setVisibility(View.INVISIBLE);
            } else if (followingDatas.get(pos).getRole().equalsIgnoreCase("V") && ((MainActivity) context).getData(context).getData().getRole().equalsIgnoreCase("V")) {
                Debugger.e("2");
                followingsButton.setVisibility(View.INVISIBLE);
            } else {
                followingsButton.setVisibility(View.VISIBLE);
            }

            if (followingDatas.get(pos).getIsFollowing() == 1) {
                followingsButton.setText(context.getString(R.string.following));
                followingsButton.setTextColor(Color.WHITE);
                followingsButton.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
            } else {
                followingsButton.setText(context.getString(R.string.FOLLOW));
                followingsButton.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                followingsButton.setBackground(context.getResources().getDrawable(R.drawable.border_blue));
            }


            Debugger.e("AFTER :::" + followingsButton.getVisibility() + "::ROLE:::" + followingDatas.get(pos).getRole().equalsIgnoreCase("V"));


            followingsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (((MainActivity) context).getUserForWholeApp().equals(Enum.setUser.GUEST)) {
                        ((MainActivity) context).messageAlert(context.getString(R.string.message_Guest));
                    } else if (((MainActivity) context).getUserForWholeApp().equals(Enum.setUser.SELLER_BASIC)) {
                        ((MainActivity) context).messageAlert(context.getString(R.string.message_visitor));
                    } else {
                        if (followingDatas.get(pos).getIsFollowing() == 1) {
                            ((MainActivity) context).alertTwoAction(context.getString(R.string.message_DoYouWantToUnFollow), new Rendering() {
                                @Override
                                public void response(boolean isCofirm) {
                                    if (isCofirm) {
                                        followingDatas.get(pos).setIsFollowing(0);
                                        notifyDataSetChanged();
                                        itemEventListener.onItemEventFired("Unfollow", followingDatas.get(pos).getFollowerUserId(), "");
                                    }
                                }
                            });
                        } else {
                            followingDatas.get(pos).setIsFollowing(1);
                            notifyDataSetChanged();
                            itemEventListener.onItemEventFired("Follow", followingDatas.get(pos).getFollowerUserId(), "");
                        }
                    }
                }
            });

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemEventListener.onItemEventFired("otherprofile", followingDatas.get(pos).getFollowerUserId(), "");
                }
            });
        }
    }
}
