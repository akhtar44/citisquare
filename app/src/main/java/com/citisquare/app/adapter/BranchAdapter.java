package com.citisquare.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.interfaces.Rendering;
import com.citisquare.app.pojo.response.BranchData;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 16/8/16.
 */
public class BranchAdapter extends RecyclerView.Adapter<BranchAdapter.BranchViewHolder> {

    View view;
    Context context;
    ArrayList<BranchData> branchDatas;
    ItemEventListener<String> positionListener;


    public BranchAdapter(Context context, ArrayList<BranchData> branchDatas, ItemEventListener<String> positionListener) {
        this.context = context;
        this.branchDatas = branchDatas;

        this.positionListener = positionListener;
    }


    @Override
    public BranchViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = View.inflate(context, R.layout.raw_branch, null);
        return new BranchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BranchViewHolder holder, int position) {
        holder.bindView(position);
    }

    @Override
    public int getItemCount() {
        return branchDatas.size();
    }


    class BranchViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.branchOfficeName)
        CustomTextView branchOfficeName;
        @BindView(R.id.branchLocation)
        CustomTextView branchLocation;
        @BindView(R.id.details)
        LinearLayout details;
        @BindView(R.id.editBranch)
        ImageView editBranch;
        @BindView(R.id.deleteBranch)
        ImageView deleteBranch;
        @BindView(R.id.deleteEdit)
        LinearLayout deleteEdit;

        BranchViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bindView(final int position) {
            branchOfficeName.setText(branchDatas.get(position).getTitle());
            branchLocation.setText(branchDatas.get(position).getAddress());

            branchLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    positionListener.onItemEventFired("LOCATION", new Gson().toJson(branchDatas.get(position)), "");
                }
            });

            editBranch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    positionListener.onItemEventFired("EDIT", new Gson().toJson(branchDatas.get(position)), "");
                }
            });

            deleteBranch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ((MainActivity) context).alertTwoAction(context.getString(R.string.doYouwantTodeleteBranch), new Rendering() {
                        @Override
                        public void response(boolean isCofirm) {
                            if (isCofirm) {
                                positionListener.onItemEventFired("DELETE", new Gson().toJson(branchDatas.get(position)), "");
                            }else {

                            }
                        }
                    });

                }
            });
        }
    }
}