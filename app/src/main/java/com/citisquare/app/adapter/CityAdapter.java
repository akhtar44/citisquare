package com.citisquare.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.citisquare.app.R;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.interfaces.ItemEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 2/8/16.
 */
public class CityAdapter extends ArrayAdapter<com.citisquare.app.pojo.response.City> {

    List<com.citisquare.app.pojo.response.City> cityList;
    Context context;
    HashMap<Integer, ViewHolder> hashMap;
    List<String> stringList;
    int resourse;
    ItemEventListener<String> renderingString;
    private int counter;


    public CityAdapter(Context context, int resource, List<com.citisquare.app.pojo.response.City> cityList, ItemEventListener<String> renderingString) {
        super(context, resource, cityList);
        this.context = context;
        this.cityList = cityList;
        this.resourse = resource;
        this.renderingString = renderingString;
        stringList = new ArrayList<>();
        hashMap = new HashMap<>();
    }

    @Override
    public int getCount() {
        return super.getCount();
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder vh;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.raw_dialog_category, parent, false);
        }
        vh = new ViewHolder(convertView);
        vh.categoryName.setText(cityList.get(position).getName());


        if (hashMap.get(position) == null)
            hashMap.put(position, vh);


        vh.viewCategoryDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String city = new Gson().toJson(cityList.get(position));
                renderingString.onItemEventFired("", city, "");
            }
        });

        return convertView;
    }

    public List<String> getStringList() {
        return stringList;
    }


    static class ViewHolder {
        @BindView(R.id.categoryName)
        CustomTextView categoryName;
        @BindView(R.id.viewCategoryDialog)
        RelativeLayout viewCategoryDialog;
        boolean isSelected;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
