package com.citisquare.app.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;

import com.citisquare.app.R;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.interfaces.RenderingString;
import com.citisquare.app.pojo.response.CategoryData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 15/7/16.
 */
public class CategoryMultipleSelectAdapter extends ArrayAdapter<CategoryData> {

    List<CategoryData> categoryDatas;
    Context context;
    HashMap<Integer, ViewHolder> hashMap;
    List<String> stringList;
    HashMap<Integer, String> integerStringHashMap;
    int resourse;
    RenderingString renderingString;
    String catId;
    List<String> stringsCat;
    private int counter = 0;
    private int max;


    public CategoryMultipleSelectAdapter(Context context, int max, String catId, int resource, List<CategoryData> categoryDatas, RenderingString renderingString) {
        super(context, resource, categoryDatas);
        this.context = context;
        this.categoryDatas = categoryDatas;
        this.max = max;
        this.catId = catId;
        stringsCat = Arrays.asList(catId.split(","));
        this.resourse = resource;
        this.renderingString = renderingString;
        integerStringHashMap = new HashMap<>();
        stringList = new ArrayList<>();
        hashMap = new HashMap<>();
        for (CategoryData categoryData : categoryDatas) {
            categoryData.setSelected(false);
        }
    }

    @Override
    public int getCount() {
        return super.getCount();
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder vh;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.raw_dialog_category, parent, false);
        }
        vh = new ViewHolder(convertView);
        if (Locale.getDefault().getDisplayLanguage().equals("العربية")) {
            vh.categoryName.setText(categoryDatas.get(position).getArName());
        } else {
            vh.categoryName.setText(categoryDatas.get(position).getName());
        }
        vh.categoryName.setSelected(categoryDatas.get(position).isSelected());
        if (getItem(position).isSelected()) {
            vh.categoryName.setTextColor(Color.BLACK);
        } else {
            vh.categoryName.setTextColor(getContext().getResources().getColor(R.color.searchGray));
        }

        vh.viewCategoryDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (counter > max - 1 && !getItem(position).isSelected()) {
                    return;
                }
                if (getItem(position).isSelected()) {
                    getItem(position).setSelected(false);
                    notifyDataSetChanged();
                    counter--;
                } else {
                    getItem(position).setSelected(true);
                    notifyDataSetChanged();
                    counter++;
                }
            }
        });

        return convertView;
    }

    public List<CategoryData> getStringList() {
        return categoryDatas;
    }


    static class ViewHolder {
        @BindView(R.id.categoryName)
        CustomTextView categoryName;
        @BindView(R.id.viewCategoryDialog)
        RelativeLayout viewCategoryDialog;
        boolean isSelected;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
