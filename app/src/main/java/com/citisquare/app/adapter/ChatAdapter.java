package com.citisquare.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.interfaces.Constants;
import com.citisquare.app.pojo.response.ChatData;
import com.citisquare.app.utils.Debugger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hlink56 on 26/7/16.
 */
public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {


    private View view;
    private Context context;
    private List<ChatData> chatDatas;
    private boolean flag = false;
    private HashMap<Integer, RecyclerView.ViewHolder> integerViewHolderHashMap;


    public ChatAdapter(Context context, List<ChatData> chatDatas) {
        this.context = context;
        this.chatDatas = chatDatas;
        integerViewHolderHashMap = new HashMap<>();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = View.inflate(context, R.layout.chat_data, null);
        ViewHolder viewHolderItem = new ViewHolder(view);
        view.setTag(viewHolderItem);
        return viewHolderItem;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindView(position);
        if (integerViewHolderHashMap.get(position) != null)
            integerViewHolderHashMap.put(position, holder);
        Debugger.e("POSITION:::" + position);
    }


    @Override
    public int getItemCount() {
        return chatDatas.size();
    }

    public void setText(String chatId, String userId, String msg, String date) {
        chatDatas.add(0, new ChatData(chatId, userId, msg, date));
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.raw_chat_text_leftCommunication)
        CustomTextView rawChatTextLeftCommunication;
        @BindView(R.id.leftDateTime)
        CustomTextView leftDateTime;
        @BindView(R.id.leftChat)
        LinearLayout leftChat;
        @BindView(R.id.raw_chat_text_rightCommunication)
        CustomTextView rawChatTextRightCommunication;
        @BindView(R.id.rightChat)
        LinearLayout rightChat;
        @BindView(R.id.rightDateTime)
        CustomTextView rightDateTime;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bindView(int position) {


            DateFormat utcFormat = new SimpleDateFormat(Constants.WS_DATE);
            utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date date = null;
            try {
                date = utcFormat.parse(chatDatas.get(position).getInsertdate());
            } catch (Exception e) {
                e.printStackTrace();
            }

            DateFormat pstFormat = new SimpleDateFormat(Constants.DATE_DISPLAY);
            pstFormat.setTimeZone(TimeZone.getDefault());
            //homeDate.setText(pstFormat.format(date));

            if (chatDatas.get(position).getUserId().equals(((MainActivity) context).userId())) {
                leftChat.setVisibility(View.GONE);
                leftDateTime.setVisibility(View.GONE);
                rawChatTextLeftCommunication.setVisibility(View.GONE);
                rightChat.setVisibility(View.VISIBLE);
                rawChatTextRightCommunication.setVisibility(View.VISIBLE);
                rawChatTextRightCommunication.setText(chatDatas.get(position).getMessage());
                rightDateTime.setVisibility(View.VISIBLE);
                rightDateTime.setText(pstFormat.format(date));
            } else {
                rightChat.setVisibility(View.GONE);
                rightDateTime.setVisibility(View.GONE);
                rawChatTextRightCommunication.setVisibility(View.GONE);
                leftChat.setVisibility(View.VISIBLE);
                rawChatTextLeftCommunication.setVisibility(View.VISIBLE);
                rawChatTextLeftCommunication.setText(chatDatas.get(position).getMessage());
                leftDateTime.setVisibility(View.VISIBLE);
                leftDateTime.setText(pstFormat.format(date));
            }
        }
    }
}