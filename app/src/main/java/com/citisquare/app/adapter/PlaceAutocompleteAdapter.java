package com.citisquare.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import com.google.gson.Gson;
import com.citisquare.app.R;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.pojo.place.PlaceWrapper;
import com.citisquare.app.pojo.place.Prediction;
import com.citisquare.app.utils.Debugger;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PlaceAutocompleteAdapter extends ArrayAdapter implements Filterable {
    private final static String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private final static String TYPE_AUTOCOMPLETE = "/autocomplete";
    private final static String OUT_JSON = "/json";
    private List<Prediction> resultList;
    private int resourceId;
    private Gson gson;
    private String API_KEY = "";

    public PlaceAutocompleteAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        resourceId = textViewResourceId;
        gson = new Gson();
        API_KEY = context.getString(R.string.google_maps_server_key);
    }

    @Override
    public int getCount() {
        if (resultList == null)
            return 0;
        return resultList.size();
    }

    @Override
    public String getItem(int index) {
        return resultList.get(index).getDescription();
    }

    public List<Prediction> getResultList() {
        return resultList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(resourceId, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.bind(position);
        return convertView;
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    // Retrieve the autocomplete results.
                    resultList = autocomplete(constraint.toString());

                    filterResults.values = resultList;
                    filterResults.count = resultList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
    }

    public List<Prediction> autocomplete(String input) {
        List<Prediction> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&sensor=false");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            //Debug.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            //Debug.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
        try {

            Debugger.e(":::::PREDICTIONS:::::" + jsonResults.toString());

            PlaceWrapper placeWrapper = gson.fromJson(jsonResults.toString(), PlaceWrapper.class);
            return placeWrapper.getPredictions();
        } catch (Exception e) {
            Debugger.e(":::::ERROR WHILE PARSING PREDICTIONS:::::");
        }
        return resultList;
    }

    class ViewHolder {
        @BindView(R.id.textViewRawText)
        CustomTextView textViewRawText;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        public void bind(int position) {
            try {
                if (resultList != null && resultList.size() > 0)
                    textViewRawText.setText(getItem(position));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}