package com.citisquare.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.controls.CustomButton;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.pojo.response.SubscriptionPlanData;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by hlink56 on 4/1/17.
 */

public class SubscriptionPlanAdapter extends RecyclerView.Adapter<SubscriptionPlanAdapter.SubscriptionPlanViewHolder> {

    View view;
    Context context;
    List<SubscriptionPlanData> subscriptionPlanDatas;
    ItemEventListener<String> stringItemEventListener;
    String subsctriptionId;


    public SubscriptionPlanAdapter(Context context, List<SubscriptionPlanData> subscriptionPlanDatas, String subsctriptionId, ItemEventListener<String> stringItemEventListener) {
        this.context = context;
        this.subsctriptionId = subsctriptionId;
        this.stringItemEventListener = stringItemEventListener;
        this.subscriptionPlanDatas = subscriptionPlanDatas;
    }


    @Override
    public SubscriptionPlanViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = View.inflate(context, R.layout.raw_subscription, null);
        // ViewHolderSearch holderFriendList = new ViewHolderSearch(view);
        // view.setTag(holderFriendList);
        //return holderFriendList;
        return new SubscriptionPlanViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SubscriptionPlanViewHolder holder, int position) {
        holder.planName.setText(subscriptionPlanDatas.get(position).getName());
        ((MainActivity) context).setImage(subscriptionPlanDatas.get(position).getImage(), holder.planImage);
        String planDesc = subscriptionPlanDatas.get(position).getCategoryCount() + " CATEGORIES. " + subscriptionPlanDatas.get(position).getDescription();
        holder.planDescription.setText(planDesc);
        String price = "SAR " + subscriptionPlanDatas.get(position).getPrice();
        if (subscriptionPlanDatas.get(position).getId().equalsIgnoreCase(subsctriptionId)) {
            holder.planSubscribe.setText(context.getString(R.string.activated));
        } else {
            holder.planSubscribe.setText(price);
        }
    }

    @Override
    public int getItemCount() {
        return subscriptionPlanDatas.size();
    }

    class SubscriptionPlanViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.planImage)
        ImageView planImage;
        @BindView(R.id.planName)
        CustomTextView planName;
        @BindView(R.id.planDescription)
        CustomTextView planDescription;
        @BindView(R.id.planSubscribe)
        CustomButton planSubscribe;

        public SubscriptionPlanViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, view);
        }

        @OnClick(R.id.planSubscribe)
        public void planSubScribeCLicked() {
                if (!planSubscribe.getText().toString().equalsIgnoreCase(context.getString(R.string.activated)))
                stringItemEventListener.onItemEventFired(subscriptionPlanDatas.get(getAdapterPosition()).getPrice(), subscriptionPlanDatas.get(getAdapterPosition()).getId(), "");
        }
    }
}