package com.citisquare.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.citisquare.app.R;
import com.citisquare.app.activity.MainActivity;
import com.citisquare.app.controls.CustomTextView;
import com.citisquare.app.interfaces.Enum;
import com.citisquare.app.interfaces.ItemEventListener;
import com.citisquare.app.pojo.JobMenuData;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by hlink56 on 21/4/17.
 */

public class JobsFrimMenuAdapter extends RecyclerView.Adapter<JobsFrimMenuAdapter.JobViewHolder> {

    View view;
    Context context;
    List<JobMenuData> jobList;

    String note = "NOTE: ";
    ItemEventListener<String> itemEventListener;


    public JobsFrimMenuAdapter(Context context, List<JobMenuData> jobList, ItemEventListener<String> itemEventListener) {
        this.context = context;
        this.itemEventListener = itemEventListener;
        this.jobList = jobList;
    }

    @Override
    public JobViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = View.inflate(context, R.layout.raw_job_bottom, null);
        return new JobViewHolder(view);
    }

    @Override
    public void onBindViewHolder(JobViewHolder holder, final int position) {
        if (((MainActivity) context).getUserForWholeApp() != null) {
            if (((MainActivity) context).getUserForWholeApp().equals(Enum.setUser.VISITOR)) {
                holder.jobApply.setVisibility(View.VISIBLE);
            } else {
                holder.jobApply.setVisibility(View.INVISIBLE);
            }
        }

        holder.jobLocation.setText(jobList.get(position).getLocation());
        ((MainActivity) context).setCircularImage(holder.jobImageView, jobList.get(position).getProfileImageThumb());
        holder.jobExperience.setText(jobList.get(position).getExperience());

        holder.jobPost.setText(jobList.get(position).getTitle());
        holder.jobCompanyName.setText(jobList.get(position).getCompanyName());
        holder.companyNameArabic.setText(jobList.get(position).getCompanyNameAr());


        String description = note + jobList.get(position).getDescription();
        Spannable wordtoSpan = new SpannableString(description);
        wordtoSpan.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.colorPrimary)), 0, note.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        holder.jobDescription.setText(wordtoSpan);

        holder.jobApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemEventListener.onItemEventFired(new Gson().toJson(jobList.get(position)), "JOBAPPLY", "");
            }
        });
        holder.jobTopProfileLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemEventListener.onItemEventFired(jobList.get(position).getUserId(), "profile", "");
            }
        });
    }


    @Override
    public int getItemCount() {
        return jobList.size();
    }


    class JobViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.jobImageView)
        ImageView jobImageView;
        @BindView(R.id.jobPost)
        TextView jobPost;
        @BindView(R.id.jobCompanyName)
        TextView jobCompanyName;
        @BindView(R.id.companyNameArabic)
        TextView companyNameArabic;
        @BindView(R.id.jobExperience)
        TextView jobExperience;
        @BindView(R.id.jobApply)
        Button jobApply;
        @BindView(R.id.jobLocation)
        CustomTextView jobLocation;
        @BindView(R.id.jobDescription)
        CustomTextView jobDescription;
        @BindView(R.id.jobTopProfileLayout)
        LinearLayout jobTopProfileLayout;

        public JobViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }


        @OnClick(R.id.jobLocation)
        public void jobLocationClicked() {
            itemEventListener.onItemEventFired("", "map", "");
        }
    }
}