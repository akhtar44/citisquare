package com.citisquare.app.adapter;

import android.content.Context;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.citisquare.app.R;
import com.citisquare.app.interfaces.ItemEventListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by hlink56 on 3/10/16.
 */

public class BottomSheetAdapter<T> extends RecyclerView.Adapter<BottomSheetAdapter.ViewHolder> {

    private final Context context;
    private final List<T> eList;
    private final List<String> strings;
    private final ItemEventListener<T> stringItemEventListener;
    private View view;
    private BottomSheetDialog mBottomSheetDialog;


    public BottomSheetAdapter(Context context, BottomSheetDialog mBottomSheetDialog, List<T> eList, List<String> strings, ItemEventListener<T> stringItemEventListener) {
        this.context = context;
        this.mBottomSheetDialog = mBottomSheetDialog;
        this.eList = eList;
        this.strings = strings;
        this.stringItemEventListener = stringItemEventListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(context, R.layout.raw_filter_dropdown, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BottomSheetAdapter.ViewHolder holder, int position) {
        holder.filterText.setText(strings.get(position));
    }

    @Override
    public int getItemCount() {
        return eList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.filterText)
        TextView filterText;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    stringItemEventListener.onItemEventFired(eList.get(getAdapterPosition()), null, "");
                    mBottomSheetDialog.dismiss();
                }
            });
        }
    }
}
