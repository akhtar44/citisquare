package com.citisquare.app.ffmpeg;

/**
 * Created by hlink16 on 3/9/15.
 */
public interface ShellCallback
{
    public void shellOut(String shellLine);

    public void processComplete(int exitValue);
}
