package com.citisquare.app.ffmpeg;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;

import com.citisquare.app.listener.ClickListener;
import com.citisquare.app.utils.Debugger;

import java.io.File;

public class MJPEGFFMPEGTest {

    ClickListener callback;
    private String fileName;

    public MJPEGFFMPEGTest(String fileName) {
        this.fileName = fileName;
    }

    public void toStartTrim(final Context context, final String startTime, final int duration, final String filePath) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {

                Debugger.e("DURATION IS :::::::::::::::::::::: " + duration);
                Debugger.e("START TIME IS ::::::::::::::::::::::: " + startTime);

                //mClip.videoBitrate = 4000;
                //mClip.videoFps = "35";

                try {
                    FfmpegController fc = new FfmpegController(context, new File(Environment.getExternalStorageDirectory() + "/temp"));
                    Clip mClip = new Clip(filePath);
                    mClip.startTime = startTime;
                    mClip.duration = duration;

                    fc.trim(mClip, true, fileName, new ShellCallback() {
                        @Override
                        public void shellOut(String shellLine) {
                            Debugger.e("aaa", shellLine);
                            //Toast.makeText(context, "TRIMMING IS DONE", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void processComplete(int exitValue) {
                            Debugger.e("aaa", exitValue + "");
                            if (callback != null)
                                callback.onOkClick(1);
                            //Toast.makeText(context, "TRIMMING IS DONE", Toast.LENGTH_LONG).show();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }


    public void setCallback(ClickListener callback) {
        this.callback = callback;
    }
}