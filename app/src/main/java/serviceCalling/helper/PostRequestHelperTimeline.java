package serviceCalling.helper;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.util.HashMap;
import java.util.List;

import serviceCalling.builder.KeyValuePair;
import serviceCalling.builder.ServiceParameter;
import serviceCalling.service.PostRequestAsync;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink16 on 15/6/16.
 */
public class PostRequestHelperTimeline<T> {
    public void pingToRequest(@NonNull String url
            , @Nullable String mimeType
            , @Nullable List<KeyValuePair> valuePairList
            , @NonNull HashMap<String, String> hashMap
            , @NonNull ServiceParameter param
            , final T t
            , final TaskCompleteListener<T> listener) {
        new PostRequestAsync(url
                , mimeType
                , valuePairList
                , hashMap
                , param
                , new TaskCompleteListener<String>() {
            @Override
            public void onSuccess(String mObject) {
                try {
                    String re = removeUTF8BOM(mObject);
                    if (re.startsWith("\uFEFF")) {
                        re = re.substring(1);
                    }
                    listener.onSuccess(new Gson().fromJson(re, ((Class<T>) t.getClass())));
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                    listener.onFailure(ExceptionType.ParseException, null);
                }
            }

            @Override
            public void onFailure(ExceptionType exceptions, String msg) {
                try {
                    listener.onFailure(exceptions, new Gson().fromJson(msg, ((Class<T>) t.getClass())));
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                    listener.onFailure(ExceptionType.ParseException, null);
                }
            }
        }).execute();
    }

    private static String removeUTF8BOM(String s) {
        if (s.startsWith("\uFEFF")) {
            s = s.substring(1);
        }
        return s;
    }

}
