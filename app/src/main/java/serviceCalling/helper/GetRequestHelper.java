package serviceCalling.helper;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.util.List;

import serviceCalling.builder.KeyValuePair;
import serviceCalling.builder.ServiceParameter;
import serviceCalling.service.GetRequestAsync;
import serviceCalling.utils.EnumContainer;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink16 on 15/6/16.
 */
public class GetRequestHelper<T> {
    public void pingToRequest(@NonNull String url
            , @NonNull List<KeyValuePair> keyValuePairList
            , @NonNull ServiceParameter param
            , @NonNull final T t
            , @NonNull final TaskCompleteListener<T> listener) {
        new GetRequestAsync(url
                , keyValuePairList
                , param
                , new TaskCompleteListener<String>() {
            @Override
            public void onSuccess(String mObject) {
                try {
                    Log.e(this.getClass().getName(), ":::: GET RESPONSE IS :::: " + mObject);
                    String result = removeUTF8BOM(mObject);
                    listener.onSuccess(new Gson().fromJson(result, ((Class<T>) t.getClass())));
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                    listener.onFailure(ExceptionType.ParseException, null);
                }
            }

            @Override
            public void onFailure(ExceptionType exceptions, String msg) {
                try {
                    listener.onFailure(exceptions, new Gson().fromJson(msg, ((Class<T>) t.getClass())));
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                    listener.onFailure(ExceptionType.ParseException, null);
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, EnumContainer.RequestType.GET);
    }

    private static String removeUTF8BOM(String s) {
        if (s.startsWith("\uFEFF")) {
            s = s.substring(1);
        }
        return s;
    }
}
