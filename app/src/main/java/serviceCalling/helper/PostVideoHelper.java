package serviceCalling.helper;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.util.List;
import java.util.Map;

import serviceCalling.builder.KeyValuePair;
import serviceCalling.builder.ServiceParameter;
import serviceCalling.service.PostVideoRequestAsync;
import serviceCalling.utils.EnumContainer;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 11/8/16.
 */
public class PostVideoHelper<T> {
    public void pingToRequest(@NonNull String url
            , @Nullable String mimeTypeImage
            , @Nullable String mimeTypeVideo
            , @Nullable List<KeyValuePair> valuePairList
            , @Nullable List<KeyValuePair> videoPairList
            , @NonNull Map<String, String> hashMap
            , @NonNull ServiceParameter param
            , final T t
            , final TaskCompleteListener<T> listener) {
        new PostVideoRequestAsync(url
                , mimeTypeImage
                , mimeTypeVideo
                , valuePairList
                , videoPairList
                , hashMap
                , param
                , new TaskCompleteListener<String>() {
            @Override
            public void onSuccess(String mObject) {
                try {
                    listener.onSuccess(new Gson().fromJson(mObject, ((Class<T>) t.getClass())));
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                    listener.onFailure(ExceptionType.ParseException, null);
                }
            }

            @Override
            public void onFailure(ExceptionType exceptions, String msg) {
                try {
                    listener.onFailure(exceptions, new Gson().fromJson(msg, ((Class<T>) t.getClass())));
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                    listener.onFailure(ExceptionType.ParseException, null);
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, EnumContainer.RequestType.POSTVIDEO);
    }
}