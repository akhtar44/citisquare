package serviceCalling.builder;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Pair;

import com.citisquare.app.utils.Debugger;
import com.citisquare.app.utils.Filename;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import serviceCalling.utils.ExceptionType;

/**
 * Created by hlink16 on 15/6/16.
 */
public class RequestBuilder {
    OkHttpClient client;
    int cacheSize = 10 * 1024 * 1024; // 10 MiB
    private Context context;
    private Pair<ExceptionType, String> pair;

    public void toIntializeObjects() {
        client = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();
    }

    public Pair<ExceptionType, String> createGetRequest(@NonNull String url, @NonNull List<KeyValuePair> valuePairList,
                                                        @NonNull ServiceParameter param) {
        String stringResponse = null;
        url += valuePairList.toString().substring(1, valuePairList.toString().length() - 1).
                replace(", ", "");
        Debugger.e(this.getClass().getName(), "::: URL IS :::: " + url);
        Request.Builder builder = new Request.Builder();
        builder.url(url).get();
        for (KeyValuePair pair : param.getKeyValuePairList())
            builder.addHeader(pair.getKey(), pair.getValue());
        Request request = builder.build();
        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful()) {
                String res = response.body().string();
                Debugger.e(this.getClass().getName(), " :::: GET RESPONSE STATUS FAIL :::: " + res);
                //listener.onFailure(ExceptionType.ParameterException, null);
                pair = new Pair<>(ExceptionType.ParameterException, res);
            } else {
                String res = response.body().string();
                Debugger.e(this.getClass().getName(), " :::: GET RESPONSE STATUS SUCCESS:::: " + res);
                //listener.onSuccess(res);
                pair = new Pair<>(ExceptionType.WithOutException, res);
            }
        } catch (IOException e) {
            Debugger.e(this.getClass().getName(), ":::::EXCEPTION THROW GET::::" + e.getMessage());
            e.printStackTrace();
            if (e.getClass().getName().equalsIgnoreCase("java.net.SocketTimeoutException"))
                //listener.onFailure(ExceptionType.SocketTimeoutException, null);
                pair = new Pair<>(ExceptionType.SocketTimeoutException, null);
            else
                //listener.onFailure(ExceptionType.IoException, null);
                pair = new Pair<>(ExceptionType.IoException, null);
        }
        return pair;
    }

    public Pair<ExceptionType, String> createPostRequest(@NonNull String url, @NonNull String mimeType,
                                                         @Nullable List<KeyValuePair> fileList, @NonNull HashMap<String, String> hashMap,
                                                         @NonNull ServiceParameter param) {
        try {
            final String IMGUR_CLIENT_ID = "111";
            MediaType MEDIA_TYPE_PNG;
            if (mimeType != null)
                MEDIA_TYPE_PNG = MediaType.parse(mimeType);
            else
                MEDIA_TYPE_PNG = MediaType.parse("");

            MultipartBody.Builder build = new MultipartBody.Builder();
            build.setType(MultipartBody.FORM);

            Debugger.e(this.getClass().getName(), " ::: REQUEST IS BUILT ::: ");
            Debugger.e(this.getClass().getName(), "::: URL IS :::: " + url);

            if (fileList != null) {
                for (KeyValuePair obj : fileList) {
                    Debugger.e("OBJECT KEY::" + obj.getKey() + " VALUE:::" + obj.getValue());
                    build.addFormDataPart(obj.getKey(), obj.getValue().substring(obj.getValue().lastIndexOf("/")),
                            RequestBody.create(MEDIA_TYPE_PNG, new File(obj.getValue())));
                }
            }

            for (Map.Entry<String, String> entry : hashMap.entrySet()) {
                Debugger.e(this.getClass().getName(), " ::: HASH MAP DATA KEY:::" + entry.getKey() + " ::: HASH MAP DATA VALUE:::" + entry.getValue());
                build.addFormDataPart(entry.getKey(), entry.getValue());
            }

            RequestBody requestBody = build.build();
            Request.Builder builder = new Request.Builder()
                    .header("Authorization", "Client-ID " + IMGUR_CLIENT_ID)
                    .url(url)
                    .post(requestBody);

            for (KeyValuePair pair : param.getKeyValuePairList()) {
                Debugger.e("Value::" + param.getKeyValuePairList().toString());
                builder.addHeader(pair.getKey(), pair.getValue());
            }

            Request request = builder.build();
            try {
                Response response = client.newCall(request).execute();
                if (!response.isSuccessful()) {
                    String stringResponse = response.body().string();
                    Debugger.e(this.getClass().getName(), " :::: POST RESPONSE STATUS FAIL:::: " + stringResponse);
                    //listener.onFailure(ExceptionType.ParameterException, stringResponse);
                    pair = new Pair<>(ExceptionType.ParameterException, stringResponse);
                } else {
                    String res = response.body().string();
                    Debugger.e(this.getClass().getName(), " :::: POST RESPONSE STATUS SUCCESS:::: " + res);
                    //listener.onSuccess(res);
                    pair = new Pair<>(ExceptionType.WithOutException, res);
                }
            } catch (IOException e) {

                Debugger.e(this.getClass().getName(), ":::::EXCEPTION THROW POST::::" + e.getMessage());

                if (e.getClass().getName().equalsIgnoreCase("java.net.SocketTimeoutException"))
                    //listener.onFailure(ExceptionType.SocketTimeoutException, null);
                    pair = new Pair<>(ExceptionType.SocketTimeoutException, null);
                else
                    //listener.onFailure(ExceptionType.IoException, null);
                    pair = new Pair<>(ExceptionType.IoException, null);
            }
        }catch (Exception ex)
        {
            ex.printStackTrace();
        }


        return pair;
    }

    public Pair<ExceptionType, String> createPostVideoRequest(String url, String mimeType,
                                                              String mimeTypeVideo, List<KeyValuePair> fileList, List<KeyValuePair> videoPair,
                                                              Map<String, String> hashMap, ServiceParameter param) {
        final String IMGUR_CLIENT_ID = "111";
        MediaType MEDIA_TYPE_PNG;
        MediaType MEDIA_TYPE_VIDEO;

        if (mimeType != null)
            MEDIA_TYPE_PNG = MediaType.parse(mimeType);
        else
            MEDIA_TYPE_PNG = MediaType.parse("");

        if (mimeTypeVideo != null)
            MEDIA_TYPE_VIDEO = MediaType.parse(mimeTypeVideo);
        else
            MEDIA_TYPE_VIDEO = MediaType.parse("");


        MultipartBody.Builder build = new MultipartBody.Builder();
        build.setType(MultipartBody.FORM);

        Debugger.e(this.getClass().getName(), " ::: REQUEST IS BUILT ::: ");
        Debugger.e(this.getClass().getName(), "::: URL IS :::: " + url);

        /* Filename filename= new Filename(path,'/','.');
            //HYPERLINK SERVER....
            //multipartBuilder.addFormDataPart(imageTitle, filename.extension(), RequestBody.create(MEDIA_TYPE_PNG, new File(path)));
            // LIVE SERVER....
            multipartBuilder.addFormDataPart(imageTitle, path, RequestBody.create(MEDIA_TYPE_PNG, new File(path)));*/


        for (Map.Entry<String, String> entry : hashMap.entrySet()) {
            Debugger.e(this.getClass().getName(), " ::: HASH MAP DATA KEY:::" + entry.getKey() + " ::: HASH MAP DATA VALUE:::" + entry.getValue());
            build.addFormDataPart(entry.getKey(), entry.getValue());
        }
        if (fileList != null) {
            for (KeyValuePair obj : fileList) {
                Filename filename = new Filename(obj.getValue(), '/', '.');
                Debugger.e("OBJECT KEY::IMAGE:::" + obj.getKey() + " VALUE:::IMAGE:::" + obj.getValue() + " FILE NAME EXTENSION" + filename.extension());
                Debugger.e("FILE NAME IS ::::::::::: " + obj.getValue().substring(obj.getValue().lastIndexOf("/") + 1));

                build.addFormDataPart(obj.getKey(), obj.getValue(),
                        RequestBody.create(MEDIA_TYPE_PNG, new File(obj.getValue())));
            }
        }

        if (videoPair != null) {
            for (KeyValuePair obj : videoPair) {
                Filename filename = new Filename(obj.getValue(), '/', '.');
                Debugger.e("OBJECT KEY::VIDEO:::" + obj.getKey() + " VALUE:::VIDEO:::" + obj.
                        getValue() + " FILE NAME EXTENSION" + filename.extension());
                Debugger.e("FILE NAME IS ::::::::::: " + obj.getValue().substring(obj.getValue().lastIndexOf("/") + 1));

                /*.addFormDataPart("ext",file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf(".")))
                .addFormDataPart("fileTypeName","img")*/

                /*HYPERLINK INFOSYSTEM SERVER....*/
                File file = new File(obj.getValue());
                build.addFormDataPart(obj.getKey(), obj.getValue(), RequestBody.create(MEDIA_TYPE_VIDEO, new File(obj.getValue())))
                        .addFormDataPart("ext", file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf(".")));
            }
        }


        RequestBody requestBody = build.build();
        Request.Builder builder = new Request.Builder()
                .header("Authorization", "Client-ID " + IMGUR_CLIENT_ID)
                .url(url)
                .post(requestBody);

        for (KeyValuePair pair : param.getKeyValuePairList())
            builder.addHeader(pair.getKey(), pair.getValue());

        Request request = builder.build();
        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful()) {
                String stringResponse = response.body().string();
                Debugger.e(this.getClass().getName(), " :::: POST RESPONSE STATUS :::: " + stringResponse);
                //listener.onFailure(ExceptionType.ParameterException, stringResponse);
                pair = new Pair<>(ExceptionType.ParameterException, stringResponse);
            } else {
                String res = response.body().string();
                //listener.onSuccess(res);
                pair = new Pair<>(ExceptionType.WithOutException, res);
            }
        } catch (IOException e) {
            e.printStackTrace();
            if (e.getClass().getName().equalsIgnoreCase("java.net.SocketTimeoutException"))
                //listener.onFailure(ExceptionType.SocketTimeoutException, null);
                pair = new Pair<>(ExceptionType.SocketTimeoutException, null);
            else
                //listener.onFailure(ExceptionType.IoException, null);
                pair = new Pair<>(ExceptionType.IoException, null);
        }

        return pair;
    }
}
