package serviceCalling.builder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hlink16 on 15/6/16.
 */
public class ServiceParameter {

    private List<KeyValuePair> keyValuePairList;

    public ServiceParameter() {
        keyValuePairList = new ArrayList<>();
    }

    public void addHeader(String key, String value) {
        keyValuePairList.add(new KeyValuePair(key, value));
    }

    public void removeHeader(String key) {

    }

    public List<KeyValuePair> getKeyValuePairList() {
        return keyValuePairList;
    }

    public void setKeyValuePairList(List<KeyValuePair> keyValuePairList) {
        this.keyValuePairList = keyValuePairList;
    }

}
