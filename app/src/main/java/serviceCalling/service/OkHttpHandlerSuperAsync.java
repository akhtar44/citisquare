package serviceCalling.service;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Pair;

import com.citisquare.app.application.QuickInfo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import serviceCalling.builder.KeyValuePair;
import serviceCalling.builder.RequestBuilder;
import serviceCalling.builder.ServiceParameter;
import serviceCalling.utils.EnumContainer;
import serviceCalling.utils.ExceptionType;

class OkHttpHandlerSuperAsync extends
        AsyncTask<EnumContainer.RequestType, Void, Pair<ExceptionType, String>> {

    private RequestBuilder requestBuilder;
    private String url;
    private List<KeyValuePair> keyValuePairList;
    private ServiceParameter param;
    private HashMap<String, String> hashMap;
    private Map<String,String> mapVideo;
    private String mimeType;
    private String mimeTypeVideo;
    private List<KeyValuePair> valuePairList;
    private List<KeyValuePair> videoPair;
    private Pair<ExceptionType, String> pair;


    OkHttpHandlerSuperAsync(@NonNull String url
            , @NonNull List<KeyValuePair> keyValuePairList
            , @NonNull ServiceParameter param) {
        this.url = url;
        this.keyValuePairList = keyValuePairList;
        this.param = param;
        setCommon();
    }

    OkHttpHandlerSuperAsync(@NonNull String url
            , @Nullable String mimeType
            , @Nullable List<KeyValuePair> valuePairList
            , @NonNull HashMap<String, String> hashMap
            , @NonNull ServiceParameter param) {
        this.url = url;
        this.hashMap = hashMap;
        this.param = param;
        this.mimeType = mimeType;
        this.valuePairList = valuePairList;
        setCommon();
    }

    OkHttpHandlerSuperAsync(@NonNull String url
            , @Nullable String mimeType
            , @Nullable String mimeTypeVideo
            , @Nullable List<KeyValuePair> valuePairList
            , @Nullable List<KeyValuePair> videoPair
            , @NonNull Map<String, String> hashMap
            , @NonNull ServiceParameter param) {
        this.url = url;
        this.mapVideo = hashMap;
        this.param = param;
        this.mimeType = mimeType;
        this.mimeTypeVideo = mimeTypeVideo;
        this.valuePairList = valuePairList;
        this.videoPair = videoPair;
        setCommon();
    }


    @Override
    protected Pair<ExceptionType, String> doInBackground(EnumContainer.RequestType... params) {
        if (params[0].equals(EnumContainer.RequestType.GET))
            pair = requestBuilder.createGetRequest(url, keyValuePairList, param);
        else if (params[0].equals(EnumContainer.RequestType.POST))
            pair = requestBuilder.createPostRequest(url, mimeType, valuePairList, hashMap, param);
        else if (params[0].equals(EnumContainer.RequestType.POSTVIDEO))
            pair = requestBuilder.createPostVideoRequest(url, mimeType, mimeTypeVideo, valuePairList, videoPair, mapVideo, param);

        return pair;
    }

    /*public void returnException(IOException e) {
        if (e.getClass().getName().equalsIgnoreCase("java.net.SocketTimeoutException")) {
            try {
                throw new ExceptionType(ExceptionType.SocketTimeoutException);
            } catch (ExceptionType e1) {
                e1.printStackTrace();
                Log.e(this.getClass().getName(), "::: NEW EXCEPTION IS THROWN :::");
            }
        }
    }*/


    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(Pair<ExceptionType, String> response) {
        super.onPostExecute(response);
    }

    private void setCommon() {
        requestBuilder = QuickInfo.getRequestBuilder();
        if (requestBuilder == null)
            toIntializeBuilder();
    }

    private void toIntializeBuilder() {
        requestBuilder = new RequestBuilder();
        requestBuilder.toIntializeObjects();
    }
}

