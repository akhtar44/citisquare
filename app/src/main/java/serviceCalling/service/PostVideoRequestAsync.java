package serviceCalling.service;

import android.util.Pair;

import java.util.List;
import java.util.Map;

import serviceCalling.builder.KeyValuePair;
import serviceCalling.builder.ServiceParameter;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink56 on 11/8/16.
 */
public class PostVideoRequestAsync extends OkHttpHandlerSuperAsync {
    private TaskCompleteListener<String> listener;

    public PostVideoRequestAsync(String url
            , String mimeType
            , String mimeTypeVideo
            , List<KeyValuePair> valuePairList
            , List<KeyValuePair> videoPair
            , Map<String, String> hashMap
            , ServiceParameter param
            , TaskCompleteListener<String> listener) {
        super(url, mimeType, mimeTypeVideo, valuePairList, videoPair, hashMap, param);
        this.listener = listener;
    }




    @Override
    protected void onPostExecute(Pair<ExceptionType, String> response) {
        super.onPostExecute(response);
        if (response.first != ExceptionType.WithOutException) {
            listener.onFailure(response.first, response.second);
        } else {
            listener.onSuccess(response.second);
        }
    }


}
