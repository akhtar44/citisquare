package serviceCalling.service;

import android.support.annotation.NonNull;
import android.util.Pair;

import java.util.List;

import serviceCalling.builder.KeyValuePair;
import serviceCalling.builder.ServiceParameter;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink16 on 15/6/16.
 */
public class GetRequestAsync extends OkHttpHandlerSuperAsync {
    private TaskCompleteListener<String> listener;

    public GetRequestAsync(@NonNull String url, @NonNull List<KeyValuePair> keyValuePairList, @NonNull ServiceParameter param, @NonNull TaskCompleteListener<String> listener) {
        super(url, keyValuePairList, param);
        this.listener = listener;
    }

    @Override
    protected void onPostExecute(Pair<ExceptionType, String> response) {
        super.onPostExecute(response);

        if (response.first != ExceptionType.WithOutException) {
            listener.onFailure(response.first, response.second);
        } else {
            listener.onSuccess(response.second);
        }
    }
}
