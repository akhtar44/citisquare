package serviceCalling.service;

import android.util.Pair;

import java.util.HashMap;
import java.util.List;

import serviceCalling.builder.KeyValuePair;
import serviceCalling.builder.ServiceParameter;
import serviceCalling.utils.ExceptionType;
import serviceCalling.utils.TaskCompleteListener;

/**
 * Created by hlink16 on 15/6/16.
 */
public class PostRequestAsync extends OkHttpHandlerSuperAsync {
    private TaskCompleteListener<String> listener;

    public PostRequestAsync(String url, String mimeType, List<KeyValuePair> valuePairList, HashMap<String, String> hashMap, ServiceParameter param, TaskCompleteListener<String> listener) {
        super(url, mimeType, valuePairList, hashMap, param);
        this.listener = listener;
    }

    @Override
    protected void onPostExecute(Pair<ExceptionType, String> response) {
        super.onPostExecute(response);

        if (response.first != ExceptionType.WithOutException) {
            listener.onFailure(response.first, response.second);
        } else {
            listener.onSuccess(response.second);
        }
    }
}
