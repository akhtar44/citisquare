package serviceCalling.utils;

/**
 * Created by hlink16 on 24/10/15.
 */
public interface TaskCompleteListener<T> {
    void onSuccess(T mObject);
    void onFailure(ExceptionType exceptions, T t);
}
