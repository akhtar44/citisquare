package serviceCalling.utils;

/**
 * Created by hlink16 on 7/7/16.
 */
public enum ExceptionType {
    SocketTimeoutException, ParseException, IoException, ParameterException, WithOutException
}
