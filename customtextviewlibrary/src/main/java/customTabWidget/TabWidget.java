package customTabWidget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by hlink16 on 22/2/16.
 */
public class TabWidget extends LinearLayout {
    Typeface typeface;
    ImageView imageView;
    TextView textView;
    TextView blinkView;


    public TabWidget(Context context) {
        super(context);
        init(context, null);
    }

    public TabWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public void init(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.UnderlineControls, 0, 0);
        Drawable valueColor = a.getDrawable(R.styleable.UnderlineControls_valueResource);
        String text = a.getString(R.styleable.UnderlineControls_headerText);
        String fontName = a.getString(R.styleable.UnderlineControls_fontNameTab);
        float textSize = a.getDimension(R.styleable.UnderlineControls_fontSize, 0.0f);
        boolean blinkVisible = a.getBoolean(R.styleable.UnderlineControls_blink, false);
        String blinkText = a.getString(R.styleable.UnderlineControls_blinkViewText);

        a.recycle();


        LayoutInflater inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_underline, this, true);


        textView = (TextView) findViewById(R.id.textViewTopTitle);
        imageView = (ImageView) findViewById(R.id.imageViewUnderline);
        blinkView = (TextView) findViewById(R.id.blinkView);
        textView.setText(text);
        if (textSize != 0.0f)
            textView.setTextSize(textSize);


        blinkView.setText(blinkText);


        imageView.setImageDrawable(valueColor);
        if (fontName != null)
            typeface = Typeface.createFromAsset(getContext().getAssets(),
                    fontName);
        textView.setTypeface(typeface);
    }


    public void changeColor(int color, int unselect) {
        this.setBackgroundResource(unselect);
        textView.setTextColor(color);
        imageView.setColorFilter(color);
    }


    public void visibleBlink(boolean blinkVisible) {
        if (blinkVisible) {
            blinkView.setVisibility(VISIBLE);
            blinkView.setTextColor(Color.RED);
        } else {
            blinkView.setVisibility(GONE);
        }
    }

    public void selectedTab(int color, int tab) {
        this.setBackgroundResource(tab);
        textView.setTextColor(color);
        imageView.setColorFilter(color);
    }

    public void setUnderLineVisibility(boolean isVisible) {
        imageView.setVisibility(isVisible ? VISIBLE : GONE);
    }

    public void setBackColor(int color) {
        imageView.setBackgroundColor(color);
    }

    public void setHurryUp(boolean value) {
        blinkView.setVisibility((value) ? VISIBLE : GONE);
        blinkView.setTextColor(Color.RED);
    }
}
